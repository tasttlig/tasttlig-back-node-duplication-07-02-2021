# Tasttlig Front-End [React (Web)]

The purpose of this README.md file is to show how to set up the development environment on your computer to run the repository.

## Getting Started

1. Login to bitbucket.org and go to this link <https://bitbucket.org/tasttlig/tasttlig-festival/src/master/>.
2. From the top right, click on the "Clone" button and get your Git clone URL (`git clone https://username@bitbucket.org/tasttlig/tasttlig-festival.git`).
3. If you have a Git command compatible shell, open it or get one from here <https://git-scm.com/downloads> (skip this step if you already have one).
4. Open shell and execute the command from Step 2.
5. Go to tasttlig-festival --> evnia-react --> client.
6. Download Node.js at <https://nodejs.org/en/download/> (skip this step if you already have one).
7. Install dependencies at the client folder: `npm ci`.
9. Create a `.env.local` file in the client folder.
9. Copy the `.env.example` content to the `.env.local` file.
10. Create a new Stripe account at <https://dashboard.stripe.com/register> (skip this step if you already have one).
11. Sign in to your Stripe account at <https://dashboard.stripe.com/login>.
12. Go to API keys at <https://dashboard.stripe.com/test/apikeys>.
13. Copy and paste the Publishable key to the `.env.local` file.
14. Start the development server at the client folder: `npm start`.
15. Go to <http://localhost:3000> in your browser.
