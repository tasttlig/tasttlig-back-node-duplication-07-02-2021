// How it Works data
const howItWorksData = [
  {
    image: 'fas fa-7x fa-ticket-alt',
    text: 'Get your festival pass',
  },
  {
    image: 'fas fa-7x fa-compass',
    text: 'Explore festival',
  },
  {
    image: 'fas fa-7x fa-check-circle',
    text: 'Reserve free samples',
  },
];

export default howItWorksData;
