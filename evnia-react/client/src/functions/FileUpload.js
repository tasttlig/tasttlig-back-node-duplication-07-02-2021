// Libraries
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';

function fileUpload(file, dir_name, callback) {
  let fileParts = file.name.split('.');
  let fileName = `${dir_name}/${uuidv4()} ${fileParts[0]}.${fileParts[1]}`;
  let fileType = fileParts[1];

  axios
    .post('/s3_signed_url', { fileName, fileType })
    .then((response) => {
      let signedRequest = response.data.signedRequest;
      let url = response.data.url;
      let options = {
        headers: {
          'Content-Type': fileType,
        },
      };

      axios
        .put(signedRequest, file, options)
        .then(() => {
          callback(url);
        })
        .catch((error) => {
          console.log(`ERROR ${JSON.stringify(error)}`);
        });
    })
    .catch((error) => {
      console.log(JSON.stringify(error));
    });
}

async function fileUploadAsync(file, dir_name) {
  let fileParts = file.name.split('.');
  let fileName = `${dir_name}/${uuidv4()} ${fileParts[0]}.${fileParts[1]}`;
  let fileType = fileParts[1];

  try {
    const response = await axios.post('/s3_signed_url', { fileName, fileType });

    let signedRequest = response.data.signedRequest;
    let url = response.data.url;
    let options = {
      headers: {
        'Content-Type': fileType,
      },
    };

    try {
      await axios.put(signedRequest, file, options);
      return {
        success: true,
        url,
      };
    } catch (error) {
      console.log(JSON.stringify(error));
      return {
        success: false,
        error: error.message,
      };
    }
  } catch (error) {
    console.log(JSON.stringify(error));
    return {
      success: false,
      error: error.message,
    };
  }
}

export default fileUpload;
export { fileUploadAsync };
