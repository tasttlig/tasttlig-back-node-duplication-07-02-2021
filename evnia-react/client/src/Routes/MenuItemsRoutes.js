// Libraries
import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
import YourMenuItems from '../components/MenuItems/YourMenuItems';
import ConvertMenuToSample from '../components/MenuItems/ConvertMenuToSample/ConvertMenuToSample';
import CreateNewMenuItem from '../components/Apply/CreateNewMenuItems';

const MenuItemsRoutes = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div>
      {userRole &&
        (userRole.includes('ADMIN') ||
          userRole.includes('RESTAURANT') ||
          userRole.includes('RESTAURANT_PENDING')) && (
          <div>
            <Route
              exact
              path={'/become-a-food-provider'}
              render={(props) => <YourMenuItems {...props} />}
            />
            <Route
              exact
              path={'/convert-menu-to-sample/:menuItemId'}
              render={(props) => <ConvertMenuToSample {...props} />}
            />
            <Route
              exact
              path={'/create-menu-items'}
              render={(props) => <CreateNewMenuItem {...props} />}
            />
          </div>
        )}
    </div>
  );
};

export default MenuItemsRoutes;
