// Libraries
import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
import Ticket from '../components/ticket/Ticket';
import TicketConfirmation from '../components/ticket/TicketConfirmationPage/TicketConfirmationPage';

const TicketRoutes = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div>
      {userRole && (
        <div>
          <Route exact path={'/ticket'} render={(props) => <Ticket {...props} />} />
          <Route
            exact
            path={'/ticket/:ticketId'}
            render={(props) => <TicketConfirmation {...props} />}
          />
        </div>
      )}
    </div>
  );
};

export default TicketRoutes;
