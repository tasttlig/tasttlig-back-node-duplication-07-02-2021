// Libraries0
import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
import Admin from '../components/Admin/Admin';
// import CreateNewUser from "../components/Apply/CreateNewUser";
import AllHostApplicants from '../components/Admin/AllHostApplicants/AllHostApplicants';
import AllVendorApplicants from '../components/Admin/AllVendorApplicants/AllVendorApplicants';
import AllSponsorApplicants from '../components/Admin/AllSponsorApplicants/AllSponsorApplicants';
// import AllExperiencesCreated from "../components/Admin/AllExperiencesCreated/AllExperiencesCreated";
import HostApplicant from '../components/Admin/HostApplicant/HostApplicant';
import VendorApplicant from '../components/Admin/AllVendorApplicants/VendorApplicant';
import SponsorApplicant from '../components/Admin/AllSponsorApplicants/SponsorApplicant';
import HostDetails from '../components/Admin/AllHostApplicants/AllHostApplicantsCard/HostDetails';
import AllBusinessApplicants from '../components/Admin/AllBusinessMemberApplications/AllBusinessMemberApplications';
import BusinessApplicant from '../components/Admin/AllBusinessMemberApplications/BusinessApplicant';
import CreateFestival from '../components/CreateFestival/CreateFestival';
import EditFestival from '../components/CreateFestival/CreateFestival';
import AllGuestAmbassadorApplicants from '../components/Admin/AllGuestAmbassadorApplicants/AllGuestAmbassadorApplicants';
import GuestAmbassadorApplicant from '../components/Admin/GuestAmbassadorApplicant/GuestAmbassadorApplicant';
import Restaurants from '../components/Restaurants/Restaurants';

const AdminRoutes = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div>
      {userRole && userRole.includes('ADMIN') && (
        <div>
          <Route exact path={'/admin'} render={(props) => <Admin {...props} />} />
          {/* <Route
            exact
            path={"/admin/create-user"}
            render={props => <CreateNewUser {...props} />}
          /> */}
          <Route
            exact
            path={'/admin/all-host-applicants'}
            render={(props) => <AllHostApplicants {...props} />}
          />
          <Route
            exact
            path={'/admin/host-application/:id'}
            render={(props) => <HostDetails {...props} />}
          />
          <Route
            exact
            path={'/admin/all-vendor-applicants'}
            render={(props) => <AllVendorApplicants {...props} />}
          />
          <Route
            exact
            path={'/admin/vendor-application/:id'}
            render={(props) => <VendorApplicant {...props} />}
          />
          <Route
            exact
            path={'/admin/all-sponsor-applicants'}
            render={(props) => <AllSponsorApplicants {...props} />}
          />
          <Route
            exact
            path={'/admin/sponsor-application/:id'}
            render={(props) => <SponsorApplicant {...props} />}
          />
          <Route
            exact
            path={'/admin/all-guest-ambassador-applicants'}
            render={(props) => <AllGuestAmbassadorApplicants {...props} />}
          />
          <Route
            exact
            path={'/admin/all-host-applicants/:id'}
            render={(props) => <HostApplicant {...props} />}
          />
          <Route
            exact
            path={'/admin/all-guest-ambassador-applicants/:id'}
            render={(props) => <GuestAmbassadorApplicant {...props} />}
          />
          <Route
            exact
            path={'/admin/all-business-member-applicants'}
            render={(props) => <AllBusinessApplicants {...props} />}
          />
          <Route
            exact
            path={'/admin/all-business-member-applicants/:id'}
            render={(props) => <BusinessApplicant {...props} />}
          />
          {/* <Route
            exact
            path={"/admin/all-experiences-created"}
            render={props => <AllExperiencesCreated {...props} />}
          /> */}
          <Route
            exact
            path={'/create-festival'}
            render={(props) => <CreateFestival {...props} />}
          />
          <Route
            exact
            path={'/edit-festival'}
            render={(props) => <EditFestival {...props} heading="Edit Festival" />}
          />
          <Route exact path={'/restaurants'} render={(props) => <Restaurants {...props} />} />
        </div>
      )}
    </div>
  );
};

export default AdminRoutes;
