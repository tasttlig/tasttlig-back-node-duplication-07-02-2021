// Libraries
import React from 'react';
import { Route } from 'react-router-dom';

// Components
import CartPayment from '../components/Payment/CartPayment';
import Payment from '../components/Payment/Payment';
import PaymentSuccess from '../components/Payment/PaymentSuccess';
import VendorPricing from '../components/VendorPricing/VendorPricing';

const PaymentRoutes = () => {
  return (
    <div>
      <Route exact path={'/cart/checkout'} render={(props) => <CartPayment {...props} />} />
      <Route
        exact
        path={'/payment/:item_type/:item_id'}
        render={(props) => <Payment {...props} />}
      />
      <Route exact path={'/payment/success'} render={(props) => <PaymentSuccess {...props} />} />
      <Route exact path={'/vendor-pricing'} render={(props) => <VendorPricing {...props} />} />
    </div>
  );
};

export default PaymentRoutes;
