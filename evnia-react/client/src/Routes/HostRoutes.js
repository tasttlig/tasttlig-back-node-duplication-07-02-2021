// Libraries0
import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
import AllVendorApplicants from '../components/Host/AllVendorApplicants/AllVendorApplicants';
import VendorApplicant from '../components/Host/AllVendorApplicants/VendorApplicant';

import AllHostSponsors from '../components/Host/AllHostSponsors/AllHostSponsors';
import AllHostGuests from '../components/Host/AllHostGuests/AllHostGuests';
import AllHostVendors from '../components/Host/AllHostVendors/AllHostVendors';
import HostRevenue from '../components/Host/HostRevenue/HostRevenue';
import AllHostFestivals from '../components/Host/AllHostFestivals/AllHostFestivals';
import HostAdmin from '../components/Dashboard/HostPanel/HostPanel';
import CreateFestival from '../components/CreateFestival/CreateFestival';

const HostRoutes = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div>
      {userRole && userRole.includes('HOST') && (
        <div>
            <Route exact path={'/host-admin'} render={(props) => <HostAdmin {...props} />} />

          <Route
            exact
            path={'/hosts/all-host-sponsors'}
            render={(props) => <AllHostSponsors {...props} />}
          />
            <Route
            exact
            path={'/host-review/all-vendor-applicants'}
            render={(props) => <AllVendorApplicants {...props} />}
          />
      
          <Route
            exact
            path={'/hosts/all-host-vendors'}
            render={(props) => <AllHostVendors {...props} />}
          />
          <Route
            exact
            path={'/hosts/all-host-guests'}
            render={(props) => <AllHostGuests {...props} />}
          />
          <Route
            exact
            path={'/hosts/revenue'}
            render={(props) => <HostRevenue {...props} />}
          />
          <Route
            exact
            path={'/hosts/host-festivals'}
            render={(props) => <AllHostFestivals {...props} />}
          />

          <Route
            exact
            path={'/create-festival'}
            render={(props) => <CreateFestival {...props} />}
          />
          <Route
            exact
            path={'/host-review/:festivalId/vendor-application/:id'}
            render={(props) => <VendorApplicant {...props} />}
          />
          
        </div>
      )}
    </div>
  );
};

export default HostRoutes;
