// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
import CreateExperience from '../components/CreateExperience/CreateExperience';
import Explore from '../components/Dashboard/ExperiencesCreated/ExperiencesCreated';
import ExperiencesArchived from '../components/Dashboard/ExperiencesArchived/ExperiencesArchived';

const ExperienceRoutes = () => {
  // Set initial state
  const [hostApplicants, setHostApplicants] = useState([]);
  let isHost = false;

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Check to see if current user can host experiences
  Array.isArray(hostApplicants) &&
    hostApplicants.map((item) => {
      if (appContext.state.user.id === parseInt(item.user_id) && item.is_host) {
        isHost = true;
      }
      return null;
    });

  // Mount list of host applicants
  useEffect(() => {
    // const url = "/applications";
    //
    // const fetchHostApplicants = async () => {
    //   const response = await axios({ method: "get", url });
    //   setHostApplicants(response.data.applications);
    // };
    //
    // fetchHostApplicants();
  }, []);

  return (
    <div>
      {userRole &&
        (userRole.includes('HOST') ||
          userRole.includes('HOST_PENDING') ||
          userRole.includes('ADMIN') ||
          userRole.includes('RESTAURANT') ||
          userRole.includes('RESTAURANT_PENDING')) && (
          <div>
            <Route
              exact
              path={'/my-current-experiences'}
              render={(props) => <Explore {...props} />}
            />
            <Route
              exact
              path={'/create-experience'}
              render={(props) => <CreateExperience {...props} />}
            />
            <Route
              exact
              path={'/edit-experience'}
              render={(props) => <CreateExperience {...props} heading="Edit Experience" />}
            />
            <Route
              exact
              path={'/my-archived-experiences'}
              render={(props) => <ExperiencesArchived {...props} />}
            />
          </div>
        )}
    </div>
  );
};

export default ExperienceRoutes;
