// Libraries
import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
// import CreateFoodSamples from "../components/Apply/CreateFoodSamples";
import CreateFoodSamples from '../components/CreateFoodSample/CreateFoodSampleForm/CreateFoodSampleForm';
import EditFoodSample from '../components/CreateFoodSample/CreateFoodSample';
// import EditFoodSample from '../components/CreateSampleSaleDonation/CreateSampleSaleDonation';
import FoodSamplesCreated from '../components/Dashboard/FoodSamplesCreated/FoodSamplesCreated';
import ArchivedFoodSamples from '../components/Dashboard/ArchivedFoodSamples/ArchivedFoodSamples';
import FoodSamplesDashboard from '../components/Dashboard/FoodSamplesDashboard/FoodSamplesDashboard';

const FoodSampleRoutes = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div>
      {userRole &&
        (userRole.includes('RESTAURANT') ||
          userRole.includes('RESTAURANT_PENDING') ||
          userRole.includes('HOST') ||
          userRole.includes('BUSINESS_MEMBER') ||
          userRole.includes('HOST_PENDING') ||
          userRole.includes('BUSINESS_MEMBER_PENDING') ||
          userRole.includes('VENDOR') ||
          userRole.includes('VENDOR_PENDING') ||
          userRole.includes('ADMIN')) && (
          <div>
            {/* <Route
              exact
              path={"/my-current-food-samples"}
              render={(props) => <FoodSamplesCreated {...props} />}
            /> */}
            <Route
              exact
              path={'/my-current-food-samples'}
              render={(props) => <FoodSamplesDashboard {...props} />}
            />
            <Route
              exact
              path={'/create-food-samples'}
              render={(props) => <CreateFoodSamples {...props} />}
            />
            <Route
              exact
              path={'/edit-food-sample'}
              render={(props) => <EditFoodSample {...props} heading="Edit Product Details" />}
            />
            <Route
              exact
              path={'/my-archived-food-samples'}
              render={(props) => <ArchivedFoodSamples {...props} />}
            />
          </div>
        )}
    </div>
  );
};

export default FoodSampleRoutes;
