// Libraries
import * as actionTypes from './actionTypes';

const initialState = {
  cartVisibility: false,
  cartItems: [],
  totalQuantity: 0,
  totalPrice: 0.0,
};

export const ShoppingReducer = (state = initialState, action) => {
  let itemPrice = 0;
  let itemQuantity = 0;
  switch (action.type) {
    case actionTypes.ADD_TO_CART:
      return {
        ...state,
        cartItems: [
          ...state.cartItems,
          {
            itemType: action.itemType,
            itemId: action.itemId,
            itemImgUrl: action.itemImgUrl,
            itemTitle: action.itemTitle,
            price: action.itemPrice,
            quantity: 1,
          },
        ],
        totalQuantity: state.totalQuantity + 1,
        totalPrice: state.totalPrice + parseFloat(action.itemPrice),
      };
    case actionTypes.REMOVE_FROM_CART:
      return {
        ...state,
        cartItems: state.cartItems.filter((item) => {
          if (item.itemId === action.itemId && item.itemType === action.itemType) {
            itemPrice = item.price;
            itemQuantity = item.quantity;
            return false;
          }
          return true;
        }),
        totalQuantity: state.totalQuantity - itemQuantity,
        totalPrice: state.totalPrice - parseFloat(itemPrice) * parseFloat(itemQuantity),
      };
    case actionTypes.ADD_QUANTITY:
      return {
        ...state,
        cartItems: state.cartItems.map((item) => {
          if (item.itemId === action.itemId && item.itemType === action.itemType) {
            itemPrice = item.price;
            return {
              ...item,
              quantity: item.quantity + 1,
            };
          } else {
            return item;
          }
        }),
        totalQuantity: state.totalQuantity + 1,
        totalPrice: state.totalPrice + parseFloat(itemPrice),
      };
    case actionTypes.SUB_QUANTITY:
      return {
        ...state,
        cartItems: state.cartItems.map((item) => {
          if (item.itemId === action.itemId && item.itemType === action.itemType) {
            itemPrice = item.price;
            return {
              ...item,
              quantity: item.quantity !== 1 ? item.quantity - 1 : 1,
            };
          } else {
            return item;
          }
        }),
        totalQuantity: state.totalQuantity - 1,
        totalPrice: state.totalPrice - parseFloat(itemPrice),
      };
    case actionTypes.EMPTY_CART:
      return initialState;
    case actionTypes.ALTERNATE_CART_VISIBILITY:
      return {
        ...state,
        cartVisibility: !state.cartVisibility,
      };
    default:
      return state;
  }
};
