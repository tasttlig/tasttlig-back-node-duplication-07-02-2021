// Libraries
import * as actionTypes from './actionTypes';

export const addToCart = (itemType, itemId, itemImgUrl, itemTitle, itemPrice) => {
  return {
    type: actionTypes.ADD_TO_CART,
    itemType,
    itemId,
    itemImgUrl,
    itemTitle,
    itemPrice,
  };
};

export const removeFromCart = (itemType, itemId) => {
  return {
    type: actionTypes.REMOVE_FROM_CART,
    itemType,
    itemId,
  };
};

export const subtractQuantity = (itemType, itemId) => {
  return {
    type: actionTypes.SUB_QUANTITY,
    itemType,
    itemId,
  };
};

export const addQuantity = (itemType, itemId) => {
  return {
    type: actionTypes.ADD_QUANTITY,
    itemType,
    itemId,
  };
};

export const emptyCart = () => {
  return {
    type: actionTypes.EMPTY_CART,
  };
};

export const alternateCartVisibility = () => {
  return {
    type: actionTypes.ALTERNATE_CART_VISIBILITY,
  };
};
