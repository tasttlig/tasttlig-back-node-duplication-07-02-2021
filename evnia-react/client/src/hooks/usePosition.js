// Libraries
import { useState, useEffect } from 'react';

const usePosition = () => {
  // Set initial state
  const [position, setPosition] = useState({});
  const [positionChanged, setPositionChanged] = useState(false);
  const [geoError, setGeoError] = useState(null);

  // Mount geolocation
  useEffect(() => {
    const geo = navigator.geolocation;

    if (!geo) {
      setGeoError('Geolocation is not supported.');

      return;
    }

    const onChange = ({ coords }) => {
      if (coords.accuracy < 5000) {
        setPosition({
          latitude: coords.latitude,
          longitude: coords.longitude,
        });
      } else {
        setGeoError('Location Accuracy is too low.');
      }

      setPositionChanged(true);
    };

    const onError = (error) => {
      setGeoError(error.message);
      setPositionChanged(true);
    };

    const watcher = geo.watchPosition(onChange, onError);

    return () => geo.clearWatch(watcher);
  }, []);

  return { ...position, positionChanged, geoError };
};

export default usePosition;
