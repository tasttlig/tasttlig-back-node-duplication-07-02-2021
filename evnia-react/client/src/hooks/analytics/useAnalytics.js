// Libraries
import React from 'react';
import { useLocation } from 'react-router-dom';
import analytics from './analytics';

export default function useAnalytics() {
  const loc = useLocation();

  React.useEffect(() => {
    analytics.init();
  }, []);

  React.useEffect(() => {
    const currentPath = loc.pathname + loc.search;
    analytics.sendPageView(currentPath);
  }, [loc]);
}
