// Libraries
import ReactGA from 'react-ga';

const TRACKING_ID =
  process.env.REACT_APP_ENVIRONMENT === 'production'
    ? process.env.REACT_APP_GOOGLE_ANALYTICS_ID
    : process.env.REACT_APP_TEST_GOOGLE_ANALYTICS_ID;

function init() {
  // Enable debug mode on the local development environment
  const isDev = process.env.REACT_APP_ENVIRONMENT === 'development';
  const isTest = process.env.REACT_APP_ENVIRONMENT === 'test';
  ReactGA.initialize(TRACKING_ID, {
    debug: isDev,
    testMode: isTest,
  });
}

function sendEvent(payload) {
  ReactGA.event(payload);
}

function sendPageView(path) {
  ReactGA.set({ page: path });
  ReactGA.pageview(path);
}

export default {
  init,
  sendEvent,
  sendPageView,
};
