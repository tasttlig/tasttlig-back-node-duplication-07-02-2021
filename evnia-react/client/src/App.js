import './assets/css/bootstrap.min.css';
import './assets/css/style.scss';
import './assets/css/responsive.scss';
import './assets/css/icofont.min.css';
import './assets/css/animate.min.css';

// Libraries
import React, { useState, useEffect, useContext, Fragment } from 'react';
import axios from 'axios';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import AdminRoutes from './Routes/AdminRoutes';
import HostRoutes from './Routes/HostRoutes';
import TicketRoutes from './Routes/TicketRoutes';
import PassportRoutes from './Routes/PassportRoutes';
import DashboardRoutes from './Routes/DashboardRoutes';
import ExperienceRoutes from './Routes/ExperienceRoutes';
import FoodSampleRoutes from './Routes/FoodSampleRoutes';
import MenuItemsRoutes from './Routes/MenuItemsRoutes';
import ProfileRoutes from './Routes/ProfileRoutes';
// import MemberUpgradeRoutes from "./Routes/MemberUpgradeRoutes";
import PaymentRoutes from './Routes/PaymentRoutes';
import { AppContext, AppProvider } from './ContextProvider/AppProvider';
import AuthModalProvider from './ContextProvider/AuthModalProvider';
import useAnalytics from './hooks/analytics/useAnalytics';
import PrivacyPolicy from './components/PrivacyPolicy/PrivacyPolicy';

// Components
import Home from './components/Home/Home';
import ComingSoon from './components/ComingSoon/ComingSoon';
import LandingPage from './components/LandingPage/LandingPage';
import HostLanding from './components/LandingPage/HostLandingPage/HostLandingPage';
import Festivals from './components/Festivals/Festivals';
import About from './components/About/About';
import PlansBusiness from './components/Plans/Business/Business';
import SignUp from './components/SignUp/SignUp';
import Login from './components/Login/Login';
import ForgotPassword from './components/ForgotPassword/ForgotPassword';
import Sponsor from './components/Apply/Sponsor';
import VendButtonApply from './components/Apply/VendButtonApply';
import VendButtonFestival from './components/Apply/VendButtonFestival';
import VendButtonFestivalTwo from './components/Apply/VendButtonFestivalTwo';
import BecomeAHost from './components/BecomeAHost/BecomeAHost';
import HostFoodSampleInquiry from './components/BecomeAHost/HostFoodSampleInquiries/HostFoodSampleInquiry';

import HostCustomerService from './components/BecomeAHost/HostCustomerService/HostCustomerService';
import CreateHostFoodSampleForm from './components/CreateFoodSample/CreateHostFoodSampleForm/CreateHostFoodSampleForm';
// import HostOnlyApply from "./components/Apply/HostOnlyApply"
// import HostExperiences from "./components/MultiStepForm/Apply";
// import ApplyToHost from "./components/ApplyToHost/ApplyToHost";
// import ApplyToCook from "./components/ApplyToCook/ApplyToCook";
// import ApplyToList from "./components/ApplyToList/ApplyToList";
// import PassportInformation from "./components/PassportInformation/PassportInformation";
// import Experiences from "./components/Experiences/Experiences";
import ExperienceDetails from './components/ExperienceDetails/ExperienceDetails';
import EditExperience from './components/Dashboard/BusinessDashboard/productAndServiceDetails/EditExperience';
import EditService from './components/Dashboard/BusinessDashboard/productAndServiceDetails/EditService';
import Restaurants from './components/Restaurants/Restaurants';
// import ReviewExperience from "./components/ReviewExperience/ReviewExperience";
import Passport from './components/Passport/Passport';
// import UpcomingFestival from "./components/Festivals/Upcoming/Upcoming";
import FoodSampleOwner from './components/FoodSampleOwner/FoodSampleOwner';
import HostProfile from './components/HostProfile/HostProfile'
import BusinessOwner from './components/BusinessOwner/BusinessOwner';
import ReviewFoodSample from './components/ReviewFoodSample/ReviewFoodSample';
import ConfirmFoodSample from './components/ConfirmFoodSample/ConfirmFoodSample';
import FoodSamplePage from './components/FoodSamplePage/FoodSamplePage';
import FestivalDetails from './components/FestivalPage/FestivalDetailsPage';
// import FestivalDetails from './components/FestivalPage/FestivalDetailsPageDuplicate';
// import TicketDetails from "./components/ticket/TicketConfirmationPage/TicketConfirmationPage";
// import Marketplace from "./components/Marketplace/Marketplace";
import EmailVerification from './components/EmailVerification/EmailVerification';
import EnterNewPassword from './components/EnterNewPassword/EnterNewPassword';
// import HostApplicationAction from "./components/Apply/HostApplicationAction";
// import FestivalToronto from "./components/Festivals/FestivalToronto/FestivalToronto";
// import FestivalHamilton from "./components/Festivals/FestivalHamilton/FestivalHamilton";
// import FestivalLearnMore from "./components/Festivals/FestivalLearnMore/FestivalLearnMore";
import PublicProfile from './components/Profile/PublicProfile';

// Styling
import './App.scss';

import SponsorshipPackageForm from './components/Profile/Sponsor/SponsorshipPackageForm';
import SponsorProductsAndServices from './components/Profile/Sponsor/SponsorProductsAndServices/SponsorProductsAndServices';
import SponsorPlan from './components/Profile/Sponsor/SponsorPlan';
import BusinessPassportStart from './components/Festivals/FestivalCard/BusinessPassportStart';
import SponsorInKindConfirmation from './components/BusinessPassport/SponsorInKindConfirmation';
import Subscriptions from './components/Pricing/Subscriptions/Subscriptions';
import GuestAmbassadorApplication from './components/Apply/ApplyForm/GuestAmbassador/GuestAmbassadorApplication';
import CreateFestival from './components/CreateFestival/CreateFestival';
import EditFestival from './components/CreateFestival/CreateFestival';
import AllHostApplicants from './components/Admin/AllHostApplicants/AllHostApplicants';
import AllVendorApplicants from './components/Admin/AllVendorApplicants/AllVendorApplicants';
import AllSponsorApplicants from './components/Admin/AllSponsorApplicants/AllSponsorApplicants';

// Environment
if (process.env.REACT_APP_ENVIRONMENT === 'production') {
  axios.defaults.baseURL = process.env.REACT_APP_PROD_API_BASE_URL;
} else {
  axios.defaults.baseURL = process.env.REACT_APP_TEST_API_BASE_URL;
}

axios.interceptors.request.use(async (config) => {
  if (config.url.startsWith('/')) {
    const acc_token = await localStorage.getItem('access_token');
    if (acc_token) {
      config.headers.Authorization = `Bearer ${acc_token}`;
    }
  }
  return config;
});

function Routes({ signedInStatus }) {
  useAnalytics();
  const NotFoundRedirect = () => <Redirect to="/" />;

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <Switch>
      {/* <Route
        exact
        path={"/"}
        render={(props) => <Home {...props} />}
      /> */}
      {/* <Route exact path={"/"} render={(props) => <ComingSoon {...props} />} /> */}
      {/* <Route
        exact
        path={"/"}
        render={(props) => (
          <LandingPage {...props} festival_name="December 2020 Festival" />
        )}
      /> */}
      {/* <Route exact path={"/festivals"} render={(props) => <Festivals {...props} />} />  */}
      {!signedInStatus ? (
        <Route exact path={'/'} render={(props) => <LandingPage {...props} />} />
      ) : (
        <Route exact path={'/'} render={(props) => <Festivals {...props} />} />
      )}
      {/* <Route exact path={"/about"} render={(props) => <About {...props} />} /> */}
      {userRole && (userRole.includes('RESTAURANT') || userRole.includes('RESTAURANT_PENDING')) ? (
        <Route exact path={'/plans/business'} render={(props) => <PlansBusiness {...props} />} />
      ) : null}
      {!signedInStatus &&
      (window.location.href ===
        `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-basic` ||
        window.location.href ===
          `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-basic` ||
        window.location.href ===
          `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-basic`) ? (
        <Redirect to="/login" />
      ) : signedInStatus &&
        userRole &&
        userRole.includes('MEMBER_BUSINESS_BASIC') &&
        (window.location.href ===
          `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-basic` ||
          window.location.href ===
            `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-basic` ||
          window.location.href ===
            `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-basic`) ? (
        <Redirect to="/plans/business" />
      ) : null}
      {!signedInStatus &&
      (window.location.href ===
        `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-intermediate` ||
        window.location.href ===
          `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-intermediate` ||
        window.location.href ===
          `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-intermediate`) ? (
        <Redirect to="/login" />
      ) : signedInStatus &&
        userRole &&
        userRole.includes('MEMBER_BUSINESS_INTERMEDIATE') &&
        (window.location.href ===
          `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-intermediate` ||
          window.location.href ===
            `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-intermediate` ||
          window.location.href ===
            `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-intermediate`) ? (
        <Redirect to="/plans/business" />
      ) : null}
      {!signedInStatus &&
      (window.location.href ===
        `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-advanced` ||
        window.location.href ===
          `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-advanced` ||
        window.location.href ===
          `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-advanced`) ? (
        <Redirect to="/login" />
      ) : signedInStatus &&
        userRole &&
        userRole.includes('MEMBER_BUSINESS_ADVANCED') &&
        (window.location.href ===
          `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-advanced` ||
          window.location.href ===
            `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-advanced` ||
          window.location.href ===
            `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-advanced`) ? (
        <Redirect to="/plans/business" />
      ) : null}
      {!signedInStatus &&
      (window.location.href ===
        `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-pro` ||
        window.location.href ===
          `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-pro` ||
        window.location.href ===
          `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-pro`) ? (
        <Redirect to="/login" />
      ) : signedInStatus &&
        userRole &&
        userRole.includes('MEMBER_BUSINESS_PRO') &&
        (window.location.href ===
          `${process.env.REACT_APP_PROD_BASE_URL}/payment/plan/business-pro` ||
          window.location.href ===
            `${process.env.REACT_APP_TEST_BASE_URL}/payment/plan/business-pro` ||
          window.location.href ===
            `${process.env.REACT_APP_DEV_BASE_URL}/payment/plan/business-pro`) ? (
        <Redirect to="/plans/business" />
      ) : null}
      {signedInStatus &&
      (window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/sign-up` ||
        window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/sign-up` ||
        window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/sign-up`) ? (
        <Redirect to="/" />
      ) : (
        <Route exact path={'/sign-up'} render={(props) => <SignUp {...props} />} />
      )}
      {signedInStatus &&
      (window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/login` ||
        window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/login` ||
        window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/login`) ? (
        <Redirect to="/" />
      ) : (
        <Route exact path={'/login'} render={(props) => <Login {...props} />} />
      )}
      {/*       {signedInStatus &&
      (window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/forgot-password` ||
        window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/forgot-password` ||
        window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/forgot-password`) ? (
        <Redirect to="/" />
      ) : ( */}
      <Route exact path={'/forgot-password'} render={(props) => <ForgotPassword {...props} />} />
      {/* )} */}
      <Route exact path={'/sponsor'} render={(props) => <Sponsor {...props} />} />

      {/*SponsorshipPackageForm*/}
      <Route
        exact
        path={'/sponsorship-package'}
        render={(props) => <SponsorshipPackageForm {...props} />}
      />
      <Route exact path={'/festivals'} render={(props) => <Festivals {...props} />} />
      <Route
        exact
        path={'/sponsor-products-and-services'}
        render={(props) => <SponsorProductsAndServices {...props} />}
      />

      <Route exact path={'/sponsor-plan'} render={(props) => <SponsorPlan {...props} />} />

      <Route
        exact
        path={'/get-business-passport'}
        render={(props) => <BusinessPassportStart {...props} />}
      />

      <Route exact path={'/host-landing'} render={(props) => <HostLanding {...props} />} />

      <Route
        exact
        path={'/sponsor-in-kind-confirmation'}
        render={(props) => <SponsorInKindConfirmation {...props} />}
      />

      <Route exact path={'/guest-vendor-plan'} render={(props) => <Subscriptions {...props} />} />
      <Route
        exact
        path={'/guest-ambassador-form'}
        render={(props) => <GuestAmbassadorApplication {...props} />}
      />

      {/* <Route
        exact
        path={"/host"}
        render={(props) => <HostOnlyApply {...props} />}
      /> */}

      {/* <Route
        exact
        path={"/festivals"}
        render={(props) => <Festivals {...props} />}
      /> */}
      {/* <Route
        exact
        path={"/09-2020-festival"}
        render={(props) => (
          <Passport {...props} festival_name="September 2020 Festival" />
        )}
      /> */}
      {/* <Route
        exact
        path={"/festival"}
        render={(props) => (
          <Passport {...props} festival_name="December 2020 Festival" />
        )}
      /> */}
      <Route
        exact
        path={'/festival/owner/:id'}
        render={(props) => <FoodSampleOwner {...props} />}
      />
      <Route
        exact
        path={'/festival/:id/business-owner/:id'}
        render={(props) => <BusinessOwner {...props} />}
      />
      <Route
        exact
        path={'/host/:id'}
        render={(props) => <HostProfile {...props} />}
      />
      {/* <Route
                exact
                path={"/resources"}
                render={props => <Resources {...props} />}
                />
                <Route
                  exact
                  path={"/resources/publisher/:id"}
                  render={props => <ResourcesPublisher {...props} />}
                /> */}
      {/* <Route
                  exact
                  path={"/experiences"}
                  render={(props) => <Experiences {...props} />}
                /> */}
      <Route exact path={'/experiences/:id'} render={(props) => <ExperienceDetails {...props} />} />

      <Route
        exact
        path={'/edit-experience'}
        render={(props) => <EditExperience {...props}  />}
      />

      <Route
        exact
        path={'/edit-service'}
        render={(props) => <EditService {...props}  />}
      />

      {/* <Route
        exact
        path={"/restaurants"}
        render={(props) => <Restaurants {...props} />}
      /> */}
      {/* <Route
                  exact
                  path={"/apply-to-host"}
                  render={(props) => <ApplyToHost {...props} />}
                /> */}
      {/* <Route
                  exact
                  path={"/apply-to-cook"}
                  render={(props) => <ApplyToCook {...props} />}
                /> */}
      {/* <Route
                  exact
                  path={"/apply-to-list"}
                  render={(props) => <ApplyToList {...props} />}
                /> */}
      {/* <Route
                  exact
                  path={"/review-experience/:id/:token"}
                  render={(props) => <ReviewExperience {...props} />}
                /> */}
      <Route
        exact
        path={'/review-food-sample/:id/:token'}
        render={(props) => <ReviewFoodSample {...props} />}
      />
      <Route
        exact
        path={'/confirm-food-sample/:token'}
        render={(props) => <ConfirmFoodSample {...props} />}
      />
      <Route
        exact
        path={'/user/verify/:token'}
        render={(props) => <EmailVerification {...props} />}
      />
      <Route
        exact
        path={'/forgot-password/:token/:email'}
        render={(props) => <EnterNewPassword {...props} />}
      />
       <Route
        exact
        path={'/privacy'}
        render={(props) => <PrivacyPolicy {...props} />}
      />
      {/* <Route
                  exact
                  path={"/user/upgrade/action/:token"}
                  render={(props) => <HostApplicationAction {...props} />}
                /> */}
      {/* <Route
                  exact
                  path={"/festival/toronto"}
                  render={(props) => <FestivalToronto {...props} />}
                /> */}
      {/* <Route
                  exact
                  path={"/festival/hamilton"}
                  render={(props) => <FestivalHamilton {...props} />}
                /> */}
      {/* <Route
                  exact
                  path={"/festival/learn-more"}
                  render={(props) => <FestivalLearnMore {...props} />}
                /> */}
      {/* <Route
        exact
        path={"/passports"}
        render={(props) => <PassportInformation {...props} />}
      /> */}
      <Route
        exact
        path={'/business/:business_name'}
        render={(props) => <PublicProfile {...props} />}
      />
      <Route
        exact
        path={'/food-sample/:foodSampleId'}
        render={(props) => <FoodSamplePage {...props} />}
      />
      <Route
        exact
        path={'/festival/:festivalId'}
        render={(props) => <FestivalDetails {...props} />}
      />

      <Route
        exact
        path={'/complete-profile/welcome-form'}
        render={(props) => <VendButtonApply {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/festival-vendor'}
        render={(props) => <VendButtonFestival {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/festival-vendor-2'}
        render={(props) => <VendButtonFestivalTwo {...props} />}
      />

      {/* <Route
        exact
        path={"/ticket/:ticketId"}
        render={(props) => <TicketDetails {...props} />}
      /> */}

      {/*<Route*/}
      {/*  exact*/}
      {/*  path={"/marketplace"}*/}
      {/*  render={(props) => <Marketplace {...props} />}*/}
      {/*/>*/}
      <Route exact path={'/become-a-host'} render={(props) => <BecomeAHost {...props} />} />
      <Route
        exact
        path={'/become-a-host/create-food-sample'}
        render={(props) => <CreateHostFoodSampleForm {...props} />}
      />
      <Route
        exact
        path={'/become-a-host/food-sample-inquiries'}
        render={(props) => <HostFoodSampleInquiry {...props} />}
      />

      <Route
        exact
        path={'/become-a-host/customer-service-inquiries'}
        render={(props) => <HostCustomerService {...props} />}
      />
    
    {/* {userRole && userRole.includes('HOST') && (
      <div>
        <Route
          exact
          path={'/create-festival'}
          render={(props) => <CreateFestival {...props} />}
        />
        <Route
          exact
          path={'/admin/all-vendor-applicants'}
          render={(props) => <AllVendorApplicants {...props} />}
        />
      </div>)
      } */}

      {/*<Route*/}
      {/*  exact*/}
      {/*  path={"/passes"}*/}
      {/*  render={(props) => <PassportPricing {...props} />}*/}
      {/*/>*/}
      {/* To render the private routes conditionally */}
      {signedInStatus ? (
        <>
          <AdminRoutes />
          <TicketRoutes />
          <HostRoutes />
          <PassportRoutes />
          <DashboardRoutes />
          <ExperienceRoutes />
          <FoodSampleRoutes />
          <MenuItemsRoutes />
          {/* <MemberUpgradeRoutes /> */}
          <PaymentRoutes />
          <ProfileRoutes />
        </>
      ) : null}
      <Route path="*" component={NotFoundRedirect} />
    </Switch>
  );
}

export default function App(props) {
  const [loading, setLoading] = useState(true);
  const [signedInStatus, setSignedInStatus] = useState(false);

  useEffect(() => {
    setSignedInStatus(localStorage.getItem('signedInStatus'));
    setLoading(false);
  }, []);

  return (
    <div className="App">
      <BrowserRouter>
        <AppProvider>
          <AuthModalProvider>
            {!loading && <Routes signedInStatus={signedInStatus} />}
          </AuthModalProvider>
        </AppProvider>
      </BrowserRouter>
    </div>
  );
}
