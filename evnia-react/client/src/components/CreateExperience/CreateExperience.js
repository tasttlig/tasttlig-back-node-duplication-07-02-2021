// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import CreateExperienceForm from './CreateExperienceForm/CreateExperienceForm';

const CreateExperience = (props) => {
  // Mount Create Experience page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Create Experience page
  return (
    <div>
      <Nav />

      <CreateExperienceForm update={props.location.state} heading={props.heading} />
    </div>
  );
};

export default CreateExperience;
