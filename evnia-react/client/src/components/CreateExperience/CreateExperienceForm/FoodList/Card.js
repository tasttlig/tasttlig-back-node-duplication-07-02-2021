// Libraries
import React, { useState } from 'react';
// import StripeCheckout from "react-stripe-checkout";
// import { toast } from "react-toastify";

// Components
import ImagesModal from './ImagesModal';

// Styling
import './Card.scss';
import 'react-toastify/dist/ReactToastify.css';

// toast.configure();

export default (props) => {
  const [showModal, setShowModal] = useState(false);

  const { item } = props;

  const { id, name, first_name, last_name, Experience_ethnicity, city, description, price } = item;

  const handleOpenModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  // Stripe token helper function
  // const handleToken = async token => {
  //   const url = "/charge";

  //   const acc_token = await localStorage.getItem("access_token");

  //   const headers = {
  //     Authorization: `Bearer ${acc_token}`
  //   };

  //   const data = {
  //     token: token.id,
  //     amount: price * 100,
  //     description: name
  //   };

  //   axios({ method: "POST", url, headers, data })
  //     .then(response => {
  //       if (response) {
  //         toast("Success! Thank you for purchasing Experience!", {
  //           type: "success"
  //         });
  //       }
  //     })
  //     .catch(error => {
  //       console.log(error);
  //       toast("Error! Something went wrong!", { type: "error" });
  //     });
  // };

  return (
    <div className="card Experiencelist-card mr-4">
      <div className="card-body">
        <div>
          <h5 className="card-title">
            <span className="text-muted float-left">
              {name} by {first_name} {last_name}
            </span>
          </h5>
        </div>
        <br></br>
        <div>
          <h6 className="card-subtitle mb-3 text-muted">
            <span className="float-left">
              {Experience_ethnicity.charAt(0).toUpperCase() + Experience_ethnicity.slice(1)} culture
            </span>
          </h6>
        </div>
        <br></br>
        <div>
          <h6 className="card-subtitle mb-3 text-muted">
            <span className="float-left">{city}</span>
          </h6>
        </div>
        <br></br>
        <div>
          <p className="card-text pb-3 Experience-card-description">{description}</p>
        </div>
        <br></br>
        <div>
          <button
            type="button"
            onClick={handleOpenModal}
            className="card-link btn btn-primary float-left"
          >
            View Images
          </button>
          <span className="card-link extra-padding float-right select-Experience">
            {`CAD $${(Math.round(price * 100) / 100).toFixed(2)}`}{' '}
            <span className="select-Experience-btn">Select</span>
            {/* <StripeCheckout
              stripeKey={process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY}
              token={handleToken}
              label="Select"
              amount={price * 100}
              currency="CAD"
              description={name}
            /> */}
          </span>
        </div>
        {showModal && (
          <ImagesModal
            ExperienceId={id}
            showModal={showModal}
            handleCloseModal={handleCloseModal}
          />
        )}
      </div>
    </div>
  );
};
