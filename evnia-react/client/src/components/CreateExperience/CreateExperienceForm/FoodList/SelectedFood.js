// Libraries
import React from 'react';

// Components
import Card from './Card';

export default (props) => {
  const {
    field,
    form: { setFieldValue },
  } = props;

  const items = field.value;
  const removeItem = (item) => {
    const filteredItems = items.filter(({ id }) => id !== item.id);
    setFieldValue('menuItems', filteredItems);
  };

  return (
    <div id="ExperienceList">
      <div className="container-fluid scroll">
        {items.map((item) => (
          <Card key={item.id} item={item} cardType={'remove'} handleClick={removeItem} />
        ))}
      </div>
    </div>
  );
};
