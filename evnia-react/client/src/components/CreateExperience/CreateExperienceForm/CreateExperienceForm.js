// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import {
  // Checkbox,
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
  RestaurantSelector,
  NationalitySelector,
} from '../../EasyForm';
import { canadaProvincesTerritories } from '../../Functions/Functions';

// Styling
import './CreateExperienceForm.scss';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const CreateExperienceForm = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const data = props.update ? props.update : {};

  // Set initial state
  const [nationalities, setNationalities] = useState([]);
  const [successMessage, setSuccessMessage] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Submit experience helper function
  const submitExperience = async (data) => {
    window.scrollTo(0, 0);

    try {
      const url = props.update
        ? `/experience/update/${props.update.experienceId}`
        : '/experience/add';

      const response = await axios({
        method: props.update ? 'PUT' : 'POST',
        url,
        data,
      });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.href = '/dashboard';
        }, 2000);

        toast(`Success! Thank you for ${props.update ? 'updating' : 'creating'} an experience!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSuccessMessage('Your experience will be shown as soon as your application is approved.');
        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    const { addressLine1, addressLine2, provinceTerritory, ...rest } = values;

    let address = addressLine1;

    if (addressLine2 && addressLine2.length > 0) {
      address = `${address}, ${addressLine2}`;
    }

    let data = {
      ...rest,
      address,
      state: provinceTerritory,
      country: 'Canada',
    };

    data.start_time = values.start_time.toString().substring(16, 21);
    data.end_time = values.end_time.toString().substring(16, 21);

    if (props.update) {
      data = {
        experience_update_data: data,
      };
    }

    await submitExperience(data);
  };

  // Mount Create Experience page
  useEffect(() => {
    window.scrollTo(0, 0);

    const fetchNationalities = async () => {
      const response = await axios({
        method: 'GET',
        url: '/nationalities',
      });

      setNationalities(response.data.nationalities);
    };

    fetchNationalities();
  }, []);

  // Render Create Experience page
  return (
    <div className="create-experience">
      {successMessage &&
        (appContext.state.user.role.includes('RESTAURANT_PENDING') ||
          appContext.state.user.role.includes('HOST_PENDING')) && (
          <div className="btn btn-success mb-3">{successMessage}</div>
        )}

      <div className="create-experience-title">
        {props.heading ? props.heading : 'Create Experience'}
      </div>

      <Form data={data} onSubmit={onSubmit}>
        <MultiImageInput
          name="images"
          label="Images"
          dropbox_label="Click or drag-and-drop to upload one or more images"
          disabled={submitAuthDisabled}
          required
        />

        <Input
          name="title"
          label="Name of your Experience"
          placeholder="Include the cultural identity of the establishment (i.e. Italian Experience)"
          disabled={submitAuthDisabled}
          required
        />

        <NationalitySelector nationalities={nationalities} disabled={submitAuthDisabled} />

        <Input name="price" label="Price" step="0.01" disabled={submitAuthDisabled} required />

        <Select name="style" label="Style" disabled={submitAuthDisabled} required>
          <option value="">--Select--</option>
          <option value="Drop In">Drop In</option>
          <option value="Fine Dining">Fine Dining</option>
          <option value="Buffet">Buffet</option>
          <option value="Family Dining">Family Dining</option>
          <option value="A La Carte">A La Carte</option>
          <option value="First Come, First Serve">First Come, First Serve</option>
        </Select>

        <div className="row">
          <div className="col-md-6 create-experience-start-date">
            <DateInput
              name="start_date"
              label="Start Date"
              dateFormat="yyyy/MM/dd"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-6 create-experience-end-date">
            <DateInput
              name="end_date"
              label="End Date"
              dateFormat="yyyy/MM/dd"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              disabled={submitAuthDisabled}
              required
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6 create-experience-start-time">
            <DateInput
              name="start_time"
              label="Start Time"
              showTimeSelect
              showTimeSelectOnly
              dateFormat="h:mm aa"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-6 create-experience-end-time">
            <DateInput
              name="end_time"
              label="End Time"
              showTimeSelect
              showTimeSelectOnly
              dateFormat="h:mm aa"
              disabled={submitAuthDisabled}
              required
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6 create-experience-capacity">
            <Input
              name="capacity"
              label="Capacity"
              type="number"
              min="1"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-6 create-experience-dress-code">
            <Select name="dress_code" label="Dress Code" disabled={submitAuthDisabled}>
              <option value="">--Select--</option>
              <option value="Formal">Formal</option>
              <option value="Informal">Informal</option>
            </Select>
          </div>
        </div>

        {props.update ? (
          <RestaurantSelector
            name="is_restaurant_location"
            is_restaurant_location={props.update.is_restaurant_location}
            disabled={submitAuthDisabled}
          />
        ) : (
          <RestaurantSelector name="is_restaurant_location" disabled={submitAuthDisabled} />
        )}

        <Input name="addressLine1" label="Street Address" disabled={submitAuthDisabled} required />
        <Input name="addressLine2" label="Unit Address" disabled={submitAuthDisabled} />
        <Input name="city" label="City" disabled={submitAuthDisabled} required />
        <Select
          name="provinceTerritory"
          label="Province or Territory"
          disabled={submitAuthDisabled}
          required
        >
          {canadaProvincesTerritories()}
        </Select>
        <Input
          name="postal_code"
          label="Postal Code"
          maxLength="7"
          disabled={submitAuthDisabled}
          required
        />

        <Textarea
          name="food_description"
          label="Food Description"
          placeholder="Describe clearly what guests will be eating and drinking"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="game_description"
          label="Game Description"
          placeholder="Describe clearly what type of games guest will be playing"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="entertainment_description"
          label="Entertainment Description"
          placeholder="Describe clearly the type of entertainment selected for this experience"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="feature_description"
          label="Feature Description"
          placeholder="Describe any special features including VIP, celebrities or artists"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="transport_description"
          label="Transportation Description"
          placeholder="Describe how easy is it to get to the experience"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="parking_description"
          label="Parking Description"
          placeholder="Describe how easy is it to find parking"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="accessibility_description"
          label="Accessibility Description"
          placeholder="Describe any mobility or special accommodations available"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="environmental_consideration_description"
          label="Environmental Consideration Description"
          placeholder="Describe any provisions made to accommodate climate issues"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="value_description"
          label="Value Description"
          placeholder="Describe the unique value offering of this experience"
          disabled={submitAuthDisabled}
          required
        />
        <Textarea
          name="other_description"
          label="Other Description"
          placeholder="Other information about location, food, host, restaurants, unit number, etc."
          disabled={submitAuthDisabled}
          required
        />

        {/* <Checkbox
          name="is_food_service_requested"
          label="Would you like to request additional food from our suppliers?"
          disabled={submitAuthDisabled}
        />
        <Checkbox
          name="is_entertainment_service_requested"
          label="Would you like to request for entertainment from our service providers?"
          disabled={submitAuthDisabled}
        />
        <Checkbox
          name="is_venue_service_requested"
          label="Would you like to request for venue from our service providers?"
          disabled={submitAuthDisabled}
        />
        <Checkbox
          name="is_transport_service_requested"
          label="Would you like to request for transport from our service providers?"
          disabled={submitAuthDisabled}
        /> */}

        <div>
          <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
            Submit
          </button>
        </div>
      </Form>
    </div>
  );
};

export default CreateExperienceForm;
