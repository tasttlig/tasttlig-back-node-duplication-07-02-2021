import React from 'react';

const VenueList = (props) => {
  return (
    <>
      {props.venueList.map((venue, index) => (
        <div key={index} className="card m-3">
          <img className="card-img-top" src={venue.image} alt={venue.name} />
          <div className="card-body">
            <h5 className="card-title">{venue.name}</h5>
            <p className="card-text">{venue.description}</p>
            <p className="card-text">{venue.location}</p>
            <div className="row">
              <div className="col">
                <span className="btn btn-primary">${venue.price}</span>
              </div>
              <div className="col d-flex justify-content-end">
                <button
                  className="btn btn-primary"
                  type="button"
                  data-venue={JSON.stringify(venue.location)}
                  onClick={props.handleClick}
                >
                  Select
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default VenueList;
