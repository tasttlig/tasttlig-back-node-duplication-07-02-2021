import React from 'react';

const ExperienceSrcList = (props) => {
  return (
    <>
      {props.ExperienceSrcList.map((ExperienceSrc, index) => (
        <div key={index} className="card text-center m-3">
          <div className="card-header">
            {ExperienceSrc.type} - <small>{ExperienceSrc.culture}</small>
          </div>
          <div className="card-body">
            <h5 className="card-title">{ExperienceSrc.name}</h5>
            <p className="card-text">
              {ExperienceSrc.items.reduce(
                (acc, cur, idx, src) => acc + cur.name + (idx !== src.length - 1 ? ', ' : '.'),
                '',
              )}
            </p>
            <p className="card-text">Location: {ExperienceSrc.location}</p>
            <p className="card-text">
              Hours: {ExperienceSrc.openHour} - {ExperienceSrc.closeHour}
            </p>
            <button
              className="btn btn-primary"
              type="button"
              data-toggle="modal"
              data-target="#Experience-src-modal"
            >
              Select
            </button>
            <ExperienceSrcItems items={ExperienceSrc.items} handleClick={props.handleClick} />
          </div>
          <div className="card-footer text-muted">
            Contact: {ExperienceSrc.owner} {ExperienceSrc.contactNumber}
          </div>
        </div>
      ))}
    </>
  );
};

export default ExperienceSrcList;

//Experience Src Items
const ExperienceSrcItems = (props) => {
  return (
    <div
      className="modal fade"
      id="Experience-src-modal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Menu items</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Item name</th>
                  <th scope="col">Price</th>
                  <th scope="col">Select</th>
                </tr>
              </thead>
              <tbody>
                {/* display items  */}
                {props.items.map((item, index) => (
                  <tr key={index}>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                      <button
                        type="button"
                        data-item={JSON.stringify(item)}
                        className="btn btn-primary"
                        onClick={props.handleClick}
                      >
                        Add
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export const SelectedMenuItems = (props) => (
  <ul className="list-group">
    <label>Selected menu items</label>
    {props.items.map((item) => (
      <li className="list-group-item ">
        {item.name}
        <span className="badge badge-primary badge-pill ml-5">${item.price}</span>
        <span>
          <i
            data-name={item.name}
            class="fas fa-trash-alt text-danger ml-5"
            onClick={props.handleRemoveItem}
          ></i>
        </span>
      </li>
    ))}
  </ul>
);
