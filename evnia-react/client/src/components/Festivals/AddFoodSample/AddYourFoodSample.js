// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import PassportCard from '../../Passport/PassportCard/PassportCard';

// Styling
import '../Festivals.scss';

const AddYourFoodSample = (props) => {
  // Set initial state
  const [foodSamples, setFoodSamples] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Fetch user food samples created helper function
  const fetchFoodSamples = async () => {
    try {
      const url = '/food-sample/festival/user/all';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };
      const params = {
        festival_name: props.festival_name,
      };

      return await axios({
        method: 'GET',
        url,
        params,
        headers,
      });
    } catch (error) {
      return error.response;
    }
  };

  // Render Passport Cards (Food Samples) helper function
  const renderPassportCards = (arr) => {
    return arr.map((card, index) => (
      <PassportCard
        key={index}
        business_name={card.business_name}
        foodSampleId={card.food_sample_id}
        passportId={
          appContext.state && appContext.state.user && appContext.state.user.passport_id
            ? appContext.state.user.passport_id
            : ''
        }
        images={card.image_urls}
        title={card.title}
        price={card.price}
        address={card.address}
        city={card.city}
        provinceTerritory={card.state}
        postalCode={card.postal_code}
        startDate={card.start_date}
        endDate={card.end_date}
        startTime={card.start_time}
        endTime={card.end_time}
        description={card.description}
        sample_size={card.sample_size}
        is_available_on_monday={card.is_available_on_monday}
        is_available_on_tuesday={card.is_available_on_tuesday}
        is_available_on_wednesday={card.is_available_on_wednesday}
        is_available_on_thursday={card.is_available_on_thursday}
        is_available_on_friday={card.is_available_on_friday}
        is_available_on_saturday={card.is_available_on_saturday}
        is_available_on_sunday={card.is_available_on_sunday}
        foodSampleOwnerId={card.food_sample_creater_user_id}
        foodSampleOwnerPicture={card.profile_image_link}
        isEmailVerified={card.is_email_verified}
        firstName={card.first_name}
        lastName={card.last_name}
        email={card.email}
        phoneNumber={card.phone_number}
        facebookLink={card.facebook_link}
        twitterLink={card.twitter_link}
        instagramLink={card.instagram_link}
        youtubeLink={card.youtube_link}
        linkedinLink={card.linkedin_link}
        websiteLink={card.website_link}
        bioText={card.bio_text}
        nationality={card.nationality}
        alpha_2_code={card.alpha_2_code}
        frequency={card.frequency}
        foodSampleType={card.food_sample_type}
        quantity={card.quantity}
        numOfClaims={card.num_of_claims}
        history={props.history}
        festival_name={props.festival_name}
        NotInFestival={true}
      />
    ));
  };

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchFoodSamples().then((foodsamples) => {
      if (foodsamples.data && foodsamples.data.details) {
        setFoodSamples(foodsamples.data.details);
      }
    });
  }, []);

  // Render empty page
  const Empty = () => {
    return (
      <strong className="no-food-samples-found">
        All of your food samples are part of this festival.
      </strong>
    );
  };

  // Render Festivals page
  return (
    <div>
      <div className="passport-cards mb-5">
        {foodSamples.length !== 0 ? renderPassportCards(foodSamples) : <Empty />}
      </div>
    </div>
  );
};

export default AddYourFoodSample;
