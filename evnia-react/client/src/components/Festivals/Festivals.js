// Libraries
import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import { useForm, Controller } from 'react-hook-form';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../ContextProvider/AppProvider';
import CreateFoodSampleForm from '../../components/CreateFoodSample/CreateFoodSampleForm/CreateFoodSampleForm';
import Modal from 'react-modal';
import LoadingBar from 'react-top-loading-bar';
// Components
import Nav from '../Navbar/Nav';
import NationalitySelector from '../Passport-Old/NationalitySelector';
import FestivalCard from './FestivalCard/FestivalCard';
import ExpiredFestivalCard from './FestivalCard/ExpiredFestivalCard';
import Footer from '../Footer/Footer';
import GoTop from '../Shared/GoTop';
import ExampleImage from '../../assets/images/Kelewele.jpeg';
import { toast } from 'react-toastify';
import { scroller } from 'react-scroll';
// import LoadingBar from 'react-top-loading-bar';

// Styling
import './Festivals.scss';
import '../LandingPage/Explore/Explore.scss';
import 'react-datepicker/dist/react-datepicker.css';
import { Link } from 'react-router-dom';
import { combineReducers } from 'redux';

const Festivals = (props) => {
  console.log("props coming from festivals:", props)
  
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [navBackground, setNavBackground] = useState('nav-transparent');
  const [festivalItems, setFestivalItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [dayOfWeek, setDayOfWeek] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectMultipleFestivals, setSelectMultipleFestivals] = useState(false);
  const [hostFestivalList, setHostFestivalList] = useState([]);
  const [festivalList, setFestivalList] = useState([]);
  const [emailVerifyOpened, setEmailVerifyOpened] = useState(false);
  const [width, setWidth] = useState(window.innerWidth);
  const breakpoint = 780;
  const ref = useRef(null);


  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'emailverify-details') {
      setEmailVerifyOpened(true);
    }
  };
  const [userReviewOpened, setUserReviewOpened] = useState(false);
  const [foodTaste, setFoodTaste] = useState(0);
  const [authenticity, setAuthenticity] = useState(0);
  const [service, setService] = useState(0);
  const [transit, setTransit] = useState(0);
  const [appearance, setAppearance] = useState(0);
  const [finalScore, setFinalScore] = useState(0);
  const [additionalInfo, setAdditionalInfo] = useState('');
  const [modalInformation, setModalInformation] = useState([]);
  // const ref = useRef(null);

  const { register, handleSubmit, control, setValue, errors } = useForm({});
  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'emailverify-details') {
      setEmailVerifyOpened(false);
      localStorage.removeItem('verificationStatusOnLogin');
    }
  };

  const listenScrollEvent = (e) => {
    if (window.scrollY > 200) {
      return setNavBackground('nav-white');
    } else {
      return setNavBackground('nav-transparent');
    }
  };
  
  
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  //const { latitude, longitude, geoError } = usePosition();
  console.log("appcontext:", appContext)
  localStorage.removeItem('dataFromFestival');
  localStorage.removeItem('valueFromTicketDashboard');
  localStorage.removeItem('festivalPayments');
  localStorage.removeItem('signUp');
  localStorage.removeItem('festivalId');
  localStorage.removeItem('valueFromFestivalTicket');
  localStorage.removeItem("buyFestivalFromNotSignup");
  localStorage.removeItem('festival_id');
  localStorage.removeItem('VendFromSignOut')
  localStorage.removeItem('host_id')
  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  const acc_token = localStorage.getItem('access_token');

  // Host multiple festivals helper function
  const handleHostMultipleFestivals = () => {
    setSelectMultipleFestivals(true);
  };

  // Render festival cards helper function
  const renderFestivalCards = (arr) => {
    const d3 = new Date();
    return arr.map((card, index) => (
      // new Date(card.festival_end_date) > d3 ?
      // (
      <FestivalCard
        key={index}
        festivalId={card.festival_id}
        images={card.image_urls}
        title={card.festival_name}
        type={card.festival_type}
        price={card.festival_price}
        city={card.festival_city}
        startDate={card.festival_start_date}
        endDate={card.festival_end_date}
        startTime={card.festival_start_time}
        endTime={card.festival_end_time}
        description={card.festival_description}
        selectMultipleFestivals={selectMultipleFestivals}
        // hostFestivalList={card.festival_restaurant_host_id}
        festivalSponsorList={card.festival_business_sponsor_id}
        hostFestivalList={card.festival_vendor_id}
        vendorApplicantList={card.vendor_request_id}
        guestFestivalList={card.festival_user_guest_id}
        history={props.history}
      />
      // ) : null
    ));
  };

  const locationOptions = (arr) => {
    let loc = [];
    arr.map((location) => {
      loc.push(location.festival_city);
    });
    let uniqueArr = [...new Set(loc)];
    return uniqueArr.map((location) => <option value={location}>{location}</option>);
  };

  let data = {};

  const weekDay = (d) => {
    let dt = new Date(d).getDay();
    let weekday = new Array(7);
    weekday[0] = 'Sunday';
    weekday[1] = 'Monday';
    weekday[2] = 'Tuesday';
    weekday[3] = 'Wednesday';
    weekday[4] = 'Thursday';
    weekday[5] = 'Friday';
    weekday[6] = 'Saturday';
    return weekday[dt];
  };

  const filterDay = (day, arr) => {
    let f = arr.filter((item) => weekDay(item.festival_start_date) === day);
    setFestivalItems(f);
    console.log('d', f);
  };

  // Toggle nationality helper function
  const toggleNationality = (nationalities) => {
    let nationalities_list = [];
    nationalities.map((nationality) => {
      nationalities_list.push(nationality.value.nationality);
    });
    setSelectedNationalities(nationalities_list);
  };

  // Load next set of festivals helper functions
  const loadNextPage = async (page) => {
    const url = '/festival/all?';
    return axios({
      method: 'GET',
      url,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
        nationalities: selectedNationalities,
        radius: filterRadius,
        startDate,
        startTime,
        cityLocation,
        dayOfWeek,
      },
    });
  };

  const handleLoadMore = (page = currentPage, festivals = festivalItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);
      if (!newPage) {
        return false;
      }
      const pagination = newPage.data.details.pagination;
      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }
      setHasNextPage(currentPage < pagination.lastPage);
      setFestivalItems(festivals.concat(newPage.data.details.data));
    });
  };
  console.log("festival items from the festivals", festivalItems)

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
    ref.current.complete();
    
  };


  // Mount Festivals page
  useEffect(() => {
    ref.current.continuousStart();
    window.scrollTo(0, 0);
    window.addEventListener('resize', () => setWidth(window.innerWidth));
    ref.current.continuousStart();
    fetchFestivalList();
    handleLoadMore();
  }, []);

  useEffect(() => {
    window.addEventListener('scroll', listenScrollEvent);
    return () => {
      window.removeEventListener('scroll', listenScrollEvent);
    };
  }, []);


  useEffect(() => {
    handleLoadMore(0, []);
    
  }, [
    props.location.state.keyword,
    selectedNationalities,
    startDate,
    startTime,
    cityLocation,
    filterRadius,
    dayOfWeek,
  ]);

  useEffect(() => {
    if((localStorage.getItem('verificationStatusOnLogin') !== 'undefined' &&  localStorage.getItem('verificationStatusOnLogin') === 'SignedIn') && appContext.state.user.verified === false){
      setEmailVerifyOpened(true);
      openModal('emailverify-details')
    }
  }, [appContext]);
  


  // Render empty festival page
  const FestivalEmpty = () => <strong className="no-festivals-found">No festivals found.</strong>;

  const renderFestivalItems = function () {
    return festivalItems.map((item) => {
      return item.festival_name;
    });
  };

  // Render Festivals page
  return (
    <Fragment>
      <LoadingBar color="#88171a" ref={ref} shadow={true} />
      <Nav
        handleHostMultipleFestivals={handleHostMultipleFestivals}
        selectMultipleFestivals={selectMultipleFestivals}
        hostFestivalList={hostFestivalList}
        background={navBackground}
      />

        <Modal
          isOpen={emailVerifyOpened}
          onRequestClose={closeModal('emailverify-details')}
          ariaHideApp={false}
          className="passport-details-modal"
        >
            <p className="text-dark">Please check your email to verify your Account to enjoy our services.</p>
       
          <div className="close-modal-bottom-content">
            <span onClick={closeModal('emailverify-details')} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

      <div className="festivals-banner">
        <span className="festivals-banner-value-proposition">
          <h1 className="festivals-banner-title">
            Taste the World in Your Neighbourhood
          </h1>
        </span>
      </div>

    {/* <header className="festival-page__header"> */}
      {/* <div className="row header-row">
        <div
          className={
            width < breakpoint && width > 480 ? 'col align-self-center' : 'col-12 col-md-8'
          }
        >
          <h1 className="header__main-heading">Experience Every Nationality</h1>
        </div>
      </div> */}
    {/* </header> */}

      <section className="container festival-details-page">
        <div className="festivals-page">
          <div className="row">
            {/* <div className="col-lg-3 festivals-filter-content">
            <div className="festivals-filter-name">
              Select Festival Nationality
            </div>
            <div>
              <span className="fas fa-search festivals-filter-search-icon" />
              <NationalitySelector
                toggleNationality={toggleNationality}
                selectedItem={selectedItem}
                setSelectedItem={setSelectedItem}
              />
            </div>
          </div> */}
            
            <div className="col-md-6 festivals-filter-content">
              {/* <div className="festivals-filter-name">Select Festival Day</div> */}
              <div>
                <span className="fas fa-calendar-day festivals-filter-day-icon" />
                <Controller
                  control={control}
                  name="select_date"
                  ref={register({ required: true })}
                  render={(props) => (
                    <DatePicker
                      className="form-control"
                      placeholderText="Select Date"
                      onChange={(date) => setFilterStartDate(date)}
                      startDate={startDate}
                      /* minDate={new Date()} */
                      dateFormat="yyyy/MM/dd"
                      className="form-control festivals-filter-date"
                      selected={startDate}
                    />
                  )}
                />
                {/* <Controller
                  control={control}
                  name="select_date"
                  render={(props) => (
                    <DatePicker
                      selected={startDate}
                      placeholderText="Select Date"
                      onChange={(date) => setFilterStartDate(date)}
                      startDate={startDate}
                      dateFormat="yyyy/MM/dd"
                      className="form-control festivals-filter-date"
                    />
                  )}
                /> */}
              </div>
            </div>
            {/* <div className="col festivals-filter-content"> */}
            {/*            <div className="festivals-filter-name">Select Festival Time</div> */}
            {/* <div> */}
            {/* <span className="fas fa-clock festivals-filter-day-icon" />
                <select
                  value={dayOfWeek}
                  onChange={(e) => {
                    setDayOfWeek(e.target.value);
                    //filterDay(e.target.value, festivalItems);
                  }}
                  className="custom-select festivals-filter-location"
                >
                  <option value="">Select Day</option>
                  <option value="Sunday">Sunday</option>
                  <option value="Monday">Monday</option>
                  <option value="Tuesday">Tuesday</option>
                  <option value="Wednesday">Wednesday</option>
                  <option value="Thursday">Thursday</option>
                  <option value="Friday">Friday</option>
                  <option value="Saturday">Saturday</option>
                </select> */}
            {/* <DatePicker
                  selected={startTime}
                  placeholderText="Select Time"
                  onChange={(time) => setFilterStartTime(time)}
                  showTimeSelect
                  showTimeSelectOnly
                  dateFormat="h:mm aa"
                  className="form-control festivals-filter-time"
                /> */}
            {/* </div>
            </div> */}
            <div className="col-md-6 festivals-filter-content">
                          {/* <div className="festivals-filter-name">
              Select Festival Location
            </div> */}
              <div>
                <span className="fas fa-map-marker-alt festivals-filter-location-icon" />
                <select
                  value={cityLocation}
                  onChange={(e) => setCityLocation(e.target.value)}
                  className="custom-select festivals-filter-location"
                >
                  <option value="">Select Location</option>
                  {festivalList.length !== 0 ? locationOptions(festivalList) : null}
                </select>
              </div>
            </div>

            <button className={'clear-btn'} onClick={() => {setCityLocation(''); setFilterStartDate('')}}>
              Clear
              </button>
              
          </div>

          <section className="festivals-page-section explore-tasttlig">
            <div className="container">
              <div className="row">
                <div className="festivals__explore-cards festivals-page__cards" ref={infiniteRef}>
                  {festivalItems.length !== 0 ? (
                    renderFestivalCards(festivalItems)
                  ) : (
                    <FestivalEmpty />
                  )}
                </div>
              </div>
            </div>
          </section>

          {/* <div className="row">
            <div className="col-md-12 p-0">
              <div className="landing-page-cards" ref={infiniteRef}>
                {festivalItems.length !== 0 ? (
                  renderFestivalCards(festivalItems)
                ) : (
                  <FestivalEmpty />
                )}
              </div>
            </div>
          </div> */}
        </div>
      </section>

      <Footer />

      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </Fragment>
  );
};

export default Festivals;
