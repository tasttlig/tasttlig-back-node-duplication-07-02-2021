// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Components
import Nav from '../../Navbar/Nav';
import BannerFooter from '../../Home/BannerFooter/BannerFooter';

// Styling
import './Upcoming.scss';

const Upcoming = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [navbarEffect, setNavbarEffect] = useState(true);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;

  // Mount Upcoming Festival page
  useEffect(() => {
    window.scrollTo(0, 0);

    // Mount navbar styling effect
    window.addEventListener('scroll', () => {
      const isTop = window.scrollY < 1;

      if (isTop && <Link exact="true" to="/festival"></Link>) {
        setNavbarEffect(true);
      } else {
        setNavbarEffect(false);
      }
    });

    // Unmount navbar styling effect at scroll
    window.removeEventListener('scroll', () => {});
  }, []);

  // Render Upcoming Festival page
  return (
    <div>
      <Nav navbarEffect={navbarEffect} />

      <div className="banner-slide upcoming-festival-banner">
        <span className="festival-banner-value-proposition">
          <h1 className="festival-banner-title">Multi-National Food Festival</h1>
          <div className="festival-banner-date">December 01 - 31st, 2020</div>
          <div className="festival-banner-location">Toronto, Ontario, Canada</div>
          {!appContext.state.user.id ? (
            <>
              <Link
                onClick={authModalContext.openModal('log-in')}
                className="festival-banner-add-food-sample-btn"
              >
                Add Food Sample
              </Link>
            </>
          ) : userRole &&
            !userRole.includes('RESTAURANT') &&
            !userRole.includes('RESTAURANT_PENDING') ? (
            <>
              <Link to="/add-restaurant" className="festival-banner-add-food-sample-btn">
                Add Food Sample
              </Link>
            </>
          ) : (
            <>
              <Link to="/create-food-sample" className="festival-banner-add-food-sample-btn">
                Add Food Sample
              </Link>
            </>
          )}
          <Link exact="true" to="/09-2020-festival" className="festival-banner-previous-btn">
            See Previous Festival
          </Link>
        </span>
      </div>

      <BannerFooter />
    </div>
  );
};

export default Upcoming;
