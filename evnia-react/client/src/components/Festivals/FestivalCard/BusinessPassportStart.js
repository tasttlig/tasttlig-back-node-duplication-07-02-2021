// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Styling
import './BusinessPassportStart.scss';

const BusinessPassportStart = (props) => {
  return (
    <div
      className="business-modal row justify-content-md-center 
        align-items-center text-center"
    >
      <div
        className="col-4 bg-danger text-center
        border border-bottom-0 border-danger rounded-top rounded py-5 text-white"
      >
        <div className="float-left">
          <img
            src={require('../../../assets/images/tasttlig-logo-black.png')}
            className="tasttlig-logo"
            alt="Tasttlig"
          />
        </div>
        Do you have a business?
      </div>
      <div class="w-100 d-none d-md-block"></div>
      <div
        className="col-4 bg-white border border-top-0 
        border-danger py-5"
      >
        <div className="px-5 btn btn-danger rounded-pill">
          <Link
            exact="true"
            to={{
              pathname: '/business-passport',
              //is_sponsor:true,
            }}
            className="text-white "
          >
            Yes
          </Link>
        </div>
        <div className="ml-5 py-3 px-5 text-center btn btn-danger rounded-pill">
          <Link
            exact="true"
            to={{
              pathname: '/sponsor-plan',
            }}
            className=" text-white"
          >
            No
          </Link>
        </div>
      </div>
    </div>
  );
};

export default BusinessPassportStart;
