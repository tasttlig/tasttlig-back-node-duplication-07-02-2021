// Libraries
import React, { useState, useEffect } from 'react';

// Components
import Nav from '../../Navbar/Nav';

// Styling
import './FestivalToronto.scss';

const FestivalToronto = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [days, setDays] = useState('');
  const [hours, setHours] = useState('');
  const [minutes, setMinutes] = useState('');

  // Countdown helper function
  const makeTimer = () => {
    let endTime = new Date('December 1, 2020 10:00:00 EDT');
    let endTimeParse = Date.parse(endTime) / 1000;
    let now = new Date();
    let nowParse = Date.parse(now) / 1000;
    let timeLeft = endTimeParse - nowParse;
    let days = Math.floor(timeLeft / 86400);
    let hours = Math.floor((timeLeft - days * 86400) / 3600);
    let minutes = Math.floor((timeLeft - days * 86400 - hours * 3600) / 60);
    let seconds = Math.floor(timeLeft - days * 86400 - hours * 3600 - minutes * 60);
    if (hours < '10') {
      hours = '0' + hours;
    }
    if (minutes < '10') {
      minutes = '0' + minutes;
    }
    if (seconds < '10') {
      seconds = '0' + seconds;
    }
    setDays(days);
    setHours(hours);
    setMinutes(minutes);
  };

  // Mount Festival Toronto page
  useEffect(() => {
    window.scrollTo(0, 0);

    const myInterval = setInterval(() => {
      makeTimer();
    }, 1000);

    return () => clearInterval(myInterval);
  }, [days, hours, minutes]);

  return (
    <div>
      <Nav />

      <div className="festival-toronto">
        <div className="festival-toronto-coming-soon">Coming Soon!</div>

        <div className="row">
          <div className="col-sm-4 p-0">
            <div className="festival-countdown-days">
              {days} <span className="festival-countdown-text">Days</span>
            </div>
          </div>
          <div className="col-sm-4 p-0">
            <div className="festival-countdown-hours">
              {hours} <span className="festival-countdown-text">Hours</span>
            </div>
          </div>
          <div className="col-sm-4 p-0">
            <div className="festival-countdown-minutes">
              {minutes} <span className="festival-countdown-text">Minutes</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FestivalToronto;
