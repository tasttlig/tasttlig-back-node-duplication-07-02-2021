import React, { useEffect, useContext, useState } from 'react';
import { Fragment } from 'react';
import { Modal, Container, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';
import './TermsAndConditions.scss';
import { toast } from 'react-toastify';
import axios from 'axios';


const HostingConditions = (props) => {

const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const user= appContext.state.user;
  const userRole= appContext.state.user.role;
  const history = useHistory();
  const user_id = appContext.state.user.id;

//   console.log('USER', user);
  

 
  
 
  const [businessPreference, setBusinessPreference] = useState([]);

  

  const handleContinue = async(e) => {
    localStorage.setItem('business-preference','Host');
    var response;
    var data = {
        "businessPreference": 'Host',
        "user_id": user_id
    }
    console.log('DATAAA', data);

      if(userRole && !userRole.includes('HOST') && !userRole.includes('HOST_PENDING')) {

        if((userRole && userRole.includes('VENDOR'))){
            
          window.location.href = '/become-a-host';
          // const url = `/upgrade/vendor-to-host`;
            
            // try {
            //     response = await axios({ method: 'POST', url, data });
        
            //     setTimeout(3000);
            //     if (response && response.data && response.data.success) {
            //       toast('Success! Your host application is in progress!', {
            //         type: 'success',
            //         autoClose: 4000,
            //       });
            //       localStorage.removeItem('business-preference');
            //     } else {
            //       toast(response.data.message, {
            //         type: 'error',
            //         autoClose: 4000,
            //       });
            //     }
            //   } catch (error) {
            //     toast('Error! Something went wrong!'.concat(error.message), {
            //       type: 'error',
            //       autoClose: 4000,
            //     });
            //   }
          }
          else if ((userRole && userRole.includes('BUSINESS_MEMBER'))&&(userRole && !userRole.includes('VENDOR'))){
            window.location.href = '/become-a-host';
            // const url = `/upgrade/business-to-host`;
            // try {
            //     response = await axios({ method: 'POST', url, data });
        
            //     setTimeout(3000);
            //     if (response && response.data && response.data.success) {
            //       toast('Success! You host application is in progress!', {
            //         type: 'success',
            //         autoClose: 4000,
            //       });
            //       localStorage.removeItem('business-preference');
            //     } else {
            //       toast(response.data.message, {
            //         type: 'error',
            //         autoClose: 4000,
            //       });
            //     }
            //   } catch (error) {
            //     toast('Error! Something went wrong!'.concat(error.message), {
            //       type: 'error',
            //       autoClose: 4000,
            //     });
            //   }

          }
          else if (((userRole && userRole.includes('GUEST'))))
        {
          window.location.href = '/business-passport';
          localStorage.setItem('application-type', 'HostApplication');

        }
      }      
      else {
        console.log('DATAAA', data);
        window.location.href = '/sign-up';
        localStorage.setItem('application-type', 'HostApplication');
      }
    // console.log(businessPreference);
  };

  const handleDisagree = () => {
    props.changeHostingConditions(false)
  }

  console.log(businessPreference);



  

  return (
    <div className="termsAndConditions1">
      <div className="bg-image" />
      
      <div className="row proceed-button">
                <div>
                    <div className="conditions-title-card"> Host - Terms and Conditions </div>
                    <div className="conditions-content-card"> 
                      <ul>
                      If you are approved as a host:
                      <li>You can provide free sample products in festivals </li>
                      <li>You agree to adhere to the standards of Tasttlig </li>
                      <li>You should maintain an overall average user rating score of 2.5/5 over the course of a year</li>
                      <li>If you choose to submit your CRA business number, you can provide your products to guests in Tasttlig festivals, if you choose not to, then you can only 
                      promote your products in Tasttlig festivals </li>
                      <li>Upon failing to comply with the above conditions, you will be removed as a host and remain as a business member in Tasttlig</li>
                      </ul>
                    </div>
                </div>
                <div className="align-vend-button">
                    <button
                      type="button"
                      className="agree-disagree-button"
                      onClick={(e) => {
                        // localStorage.setItem('business-preference','HOST');
                        localStorage.setItem('dataFromFestival', 'Host');
                        handleContinue();
                        }}
                    >
                      Agree
                    </button>
                    <button
                      type="button"
                      className="agree-disagree-button"
                      onClick={() => handleDisagree(false)}
                    >
                      Disagree
                    </button>
                </div>
              </div>
          </div>
      
  );
};

export default HostingConditions;
