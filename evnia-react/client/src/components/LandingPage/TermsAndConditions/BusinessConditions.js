import React, { useEffect, useContext, useState } from 'react';
import { Fragment } from 'react';
import { Modal, Container, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';
import './TermsAndConditions.scss';
import { toast } from 'react-toastify';
import axios from 'axios';

const BusinessConditions = (props) => {
  console.log("props coming from business conditions:", props)
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const user = appContext.state.user;
  const userRole = appContext.state.user.role;
  const history = useHistory();
  const user_id = appContext.state.user.id;

  // console.log('USER', user);

  const [businessPreference, setBusinessPreference] = useState([]);

  const handleContinue = async (e) => {
    localStorage.setItem('business-preference', 'Business');
    var response;
    var data = {
      businessPreference: 'Business',
      user_id: user_id,
    };
    console.log('DATAAA', data);

     if (userRole && userRole.includes('GUEST')) {
        window.location.href = '/business-passport';
      
    } else {
      // console.log('DATAAA', data);
      window.location.href = '/sign-up';
    }
    // console.log(businessPreference);
  };

  const handleDisagree = () => {
    props.changeBusinessConditions(false);
  };

  console.log(businessPreference);

  return (
    <div className="termsAndConditions1">
      <div className="bg-image" />

      <div className="row proceed-button">
        <div>
          <div className="conditions-title-card"> Business - Terms and Conditions </div>
          <div className="conditions-content-card">
            If you dont agree to Vending conditions you can apply as a normal business member:
            <ul>
              <li>You can upload your products, services and experiences in your dashboard.</li>
              <li>You can sponsor festivals.</li>
              <li>You can still apply to become host or vendor.</li>
            </ul>
          </div>
        </div>
        <row className="align-vend-button">
          <button
            type="button"
            className="agree-disagree-button"
            onClick={(e) => {
              handleContinue();
            }}
          >
            Agree
          </button>
          <button
            type="button"
            className="agree-disagree-button"
            onClick={() => {
              handleDisagree(false)
            }}
          >
            Disagree
          </button>
        </row>
      </div>
    </div>
  );
};

export default BusinessConditions;
