import React, { useEffect, useContext, useState } from 'react';
import { Fragment } from 'react';
import { Modal, Container, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';
import './TermsAndConditions.scss';
import { toast } from 'react-toastify';
import axios from 'axios';
import BusinessConditions from './BusinessConditions'


const VendingConditions = (props) => {
  console.log("props from vending conditions:", props)
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const user = appContext.state.user;
  const userRole = appContext.state.user.role;
  const history = useHistory();
  const user_id = appContext.state.user.id;

  // console.log('USER', user);
  const [businessPreference, setBusinessPreference] = useState([]);
  const [businessConditions, setBusinessConditions] = useState(false);

  const handleContinue = async (e) => {
    localStorage.setItem('business-preference', 'Vend');
    var response;
    var data = {
      businessPreference: 'Vend',
      user_id: user_id,
    };

    if (userRole && !userRole.includes('VENDOR')) {
      if (userRole && userRole.includes('BUSINESS_MEMBER')) {
        const url = `/upgrade/business-to-vend`;
        try {
          response = await axios({ method: 'POST', url, data });

          setTimeout(3000);
          if (response && response.data && response.data.success) {
            setTimeout(() => {
              window.location = "/";
            }, 2000);
            toast('Success! Your vendor application is now in progress', {
              type: 'success',
              autoClose: 4000,
            });
            localStorage.removeItem('business-preference');
          } else {
            toast(response.data.message, {
              type: 'error',
              autoClose: 4000,
            });
          }
        } catch (error) {
          toast('Error! Something went wrong!'.concat(error.message), {
            type: 'error',
            autoClose: 4000,
          });
        }
      } else if (userRole && userRole.includes('GUEST')) {
        window.location.href = '/business-passport';
      }
    } else {
      // console.log('DATAAA', data);
      window.location.href = '/sign-up';
    }
    // console.log(businessPreference);
  };

  const handleDisagree = () => {
    // props.changeVendingConditions(false);
    props.changeBusinessConditions(true);
  };
console.log("props.selecte from vending conditions:", props.selected)

  const handleFestivalPayment = () => {

    if(props.selected.length === 0 ) {
      toast('Please Select one or more festivals to vend!', {
        type: 'error',
        autoClose: 2000,
      });
      } else {
      if(localStorage.getItem("VendFromSignOut")) {
        localStorage.removeItem('festivalPayments');
        localStorage.setItem('festivalPayments', props.selected);
        window.location.href = `/sign-up`;
      } else {
        localStorage.removeItem('festivalPayments');
        localStorage.setItem('festivalPayments',[ props.selected]);

        if(props.subscriptionType && props.subscriptionType === 'V_MOD') {
          window.location.href = '/payment/package/V_MOD';
        } else if (props.subscriptionType && props.subscriptionType === 'V_ULTRA') {
          window.location.href = '/payment/package/V_ULTRA';
        } else {
          window.location.href = '/payment/package/V_MIN';
        }
      }
    }
  };

  console.log(businessPreference);

  return (
    <div className="termsAndConditions1">
      <div className="bg-image" />

      <div className="row proceed-button">
        <div>
          <div className="conditions-title-card"> Vendor - Terms and Conditions </div>
          <div className="conditions-content-card">
            If you are approved as a Vendor:
            <ul>
              <li>You can sell your products, services and experiences in any Tasttlig festival</li>
              <li>You agree to adhere to the vending standards of Tasttlig</li>
            </ul>
          </div>
        </div>
        <row className="align-vend-button">
          <button
            type="button"
            className="agree-disagree-button"
            onClick={(e) => {
              // localStorage.setItem('dataFromFestival', 'Vendor');
              // handleContinue();
              handleFestivalPayment();
            }}
          >
            Agree
          </button>
         {!userRole ? (<button
            type="button"
            className="agree-disagree-button"
            onClick={() => props.changeBusinessConditions(true)}
          >
            Disagree
          </button>): userRole && !userRole.includes('BUSINESS_MEMBER') || !userRole.includes('BUSINESS_MEMBER_PENDING') ?
            <button
            type="button"
            className="agree-disagree-button"
            onClick={(e) => {
              props.changeVendingConditions(false)
              // setBusinessConditions(true)
              
            }}
          >
            Disagree
          </button> : null}
        </row>
      </div>
    </div>
  );
};

export default VendingConditions;
