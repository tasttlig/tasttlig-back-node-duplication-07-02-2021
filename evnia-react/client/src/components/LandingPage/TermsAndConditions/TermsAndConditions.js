import React, { useEffect, useContext, useState } from 'react';
import { Fragment } from 'react';
import { Modal, Container, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';
import './TermsAndConditions.scss';
import { toast } from 'react-toastify';
import axios from 'axios';

const TermsAndConditions = (props) => {
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const user = appContext.state.user;
  const userRole = appContext.state.user.role;
  const history = useHistory();
  const user_id = appContext.state.user.id;

  console.log('USER', user);
  console.log();

  const [hostAgree, setHostAgree] = useState(false);
  const [vendAgree, setVendAgree] = useState(false);
  const [businessMemberAgree, setBusinessMemberAgree] = useState(false);
  const [businessPreference, setBusinessPreference] = useState([]);

  const handleContinue = async (e) => {
    localStorage.setItem('business-preference', businessPreference);
    var response;
    var data = {
      businessPreference: businessPreference,
      user_id: user_id,
    };
    if (userRole && !userRole.includes('HOST')) {
      if (userRole && userRole.includes('VENDOR') && businessPreference === 'Host') {
        window.location.href = '/become-a-host';

        // const url = `/upgrade/vendor-to-host`;

        // try {
        //     response = await axios({ method: 'POST', url, data });

        //     setTimeout(3000);
        //     if (response && response.data && response.data.success) {
        //       toast('Success! Your host application is in progress!', {
        //         type: 'success',
        //         autoClose: 4000,
        //       });
        //     } else {
        //       toast(response.data.message, {
        //         type: 'error',
        //         autoClose: 4000,
        //       });
        //     }
        //   } catch (error) {
        //     toast('Error! Something went wrong!'.concat(error.message), {
        //       type: 'error',
        //       autoClose: 4000,
        //     });
        //   }
      } else if (
        userRole &&
        userRole.includes('BUSINESS_MEMBER') &&
        userRole &&
        !userRole.includes('VENDOR') &&
        businessPreference == 'Host'
      ) {
        window.location.href = '/become-a-host';
        // const url = `/upgrade/business-to-host`;
        // try {
        //     response = await axios({ method: 'POST', url, data });

        //     setTimeout(3000);
        //     if (response && response.data && response.data.success) {
        //       toast('Success! You host application is in progress!', {
        //         type: 'success',
        //         autoClose: 4000,
        //       });
        //     } else {
        //       toast(response.data.message, {
        //         type: 'error',
        //         autoClose: 4000,
        //       });
        //     }
        //   } catch (error) {
        //     toast('Error! Something went wrong!'.concat(error.message), {
        //       type: 'error',
        //       autoClose: 4000,
        //     });
        //   }
      } else if (userRole && userRole.includes('BUSINESS_MEMBER') && businessPreference == 'Vend') {
        const url = `/upgrade/business-to-vend`;
        try {
          response = await axios({ method: 'POST', url, data });

          setTimeout(3000);
          if (response && response.data && response.data.success) {
            toast('Success! Your vendor application is now in progress', {
              type: 'success',
              autoClose: 4000,
            });
          } else {
            toast(response.data.message, {
              type: 'error',
              autoClose: 4000,
            });
          }
        } catch (error) {
          toast('Error! Something went wrong!'.concat(error.message), {
            type: 'error',
            autoClose: 4000,
          });
        }
      } else if (
        userRole &&
        userRole.includes('GUEST') &&
        (businessPreference === 'Vend' ||
          businessPreference === 'Host' ||
          businessPreference === 'Business Member')
      ) {
        window.location.href = '/business-passport';
      }
    } else {
      window.location.href = '/sign-up';
    }
    // console.log(businessPreference);
  };

  const handleDisagree = () => {
    props.changeViewTermsAndConditions(false);
  };

  console.log(businessPreference);

  //   const handleCancel = (e) => {
  //     return (
  //       <div>
  //         {hostAgree === false ? (
  //           <div> You can continue by the selecting the option to vend or be a business member </div>
  //         ) : vendAgree === false ? (
  //           <div> You can continue as a host or a business member</div>
  //         ) : businessMemberAgree === false ? (
  //           <div>You can continue</div>
  //         ) : (
  //           <div> You must select any one option to proceed </div>
  //         )}
  //       </div>
  //     );
  //   };

  return (
    <div className="termsAndConditions1">
      <div className="bg-image" />
      <div className="row">
        <div className="question-style">What do you want to do?</div>
      </div>
      <div className="row options-block" onChange={(e) => setBusinessPreference(e.target.value)}>
        <div className="options-radio-style">
          <input type="radio" value="Host" name="preference" /> Host
        </div>
        {(userRole && userRole.includes('VENDOR')) ||
        (userRole && userRole.includes('VENDOR_PENDING')) ? (
          <div />
        ) : (
          <div className="options-radio-style">
            <input type="radio" value="Vend" name="preference" /> Vend
          </div>
        )}
        {!appContext.state.user.id ? (
          <div className="options-radio-style">
            <input type="radio" value="Guest" name="preference" /> Continue as a Guest
          </div>
        ) : (
          <div />
        )}
      </div>
      <div className="row proceed-button">
        {businessPreference === 'Host' ? (
          <div>
            {hostAgree === true || vendAgree === true || businessPreference === 'Guest' ? (
              <div>
                <button
                  type="button"
                  className="agree-disagree-button "
                  onClick={(e) => handleContinue()}
                >
                  Continue
                </button>
              </div>
            ) : (
              <div className="container terms-and-conditions">
                <div className="container terms-and-conditions1">
                  <div className="conditions-title-card"> Host - Terms and Conditions </div>
                  <div className="conditions-content-card">
                    <ul>
                      If you are approved as a host:
                      <li>You can provide free sample products in festivals </li>
                      <li>You agree to adhere to the standards of Tasttlig </li>
                      <li>
                        You should maintain an overall average user rating score of 2.5/5 over the
                        course of a year
                      </li>
                      <li>
                        If you choose to submit your CRA business number, you can provide your
                        products to guests in Tasttlig festivals, if you choose not to, then you can
                        only promote your products in Tasttlig festivals{' '}
                      </li>
                      <li>
                        Upon failing to comply with the above conditions, you will be removed as a
                        host and remain as a business member in Tasttlig
                      </li>
                    </ul>
                  </div>
                </div>
                <button
                  type="button"
                  className="agree-disagree-button"
                  onClick={(e) => {
                    setVendAgree(false);
                    setHostAgree(true);
                    setBusinessMemberAgree(false);
                    localStorage.setItem('business-preference', 'HOST');
                    localStorage.setItem('dataFromFestival', 'Host');
                  }}
                >
                  Agree
                </button>
                <button
                  type="button"
                  className="agree-disagree-button"
                  onClick={() => handleDisagree(false)}
                >
                  Disagree
                </button>
              </div>
            )}
          </div>
        ) : businessPreference === 'Vend' ? (
          <div>
            {hostAgree === true || vendAgree === true || businessMemberAgree === true ? (
              <div>
                <button
                  type="button"
                  className="agree-disagree-button"
                  onClick={(e) => handleContinue()}
                >
                  Continue
                </button>
                {/* <button type="button" className="modal-button1" onClick={(e) => handleCancel()}>
                Cancel
              </button> */}
              </div>
            ) : (
              <div>
                <div>
                  <div className="conditions-title-card"> Vendor - Terms and Conditions </div>
                  <div className="conditions-content-card">
                    If you are approved as a vendor:
                    <ul>
                      <li>ucts in festivals</li>
                      <li>You agree to adhere to the standards of Tasttlig</li>
                      <li>You can upgrade to Host at any time later in Tasttlig</li>
                      <li>
                        If you choose to submit your CRA business number, you can sell your products
                        to guests in Tasttlig festivals, if you choose not to, then you can only
                        promote your products in Tasttlig festivals
                      </li>
                      <li>
                        Upon failing to comply with the above conditions, you may be removed as a
                        vendor but you may continue as a business member in Tasttlig
                      </li>
                    </ul>
                  </div>
                </div>
                <button
                  type="button"
                  className="agree-disagree-button"
                  onClick={(e) => {
                    setVendAgree(true);
                    setHostAgree(false);
                    setBusinessMemberAgree(false);
                    localStorage.setItem('business-preference', 'VEND');
                    localStorage.setItem('dataFromFestival', 'Vendor');
                  }}
                >
                  Agree
                </button>
                <button
                  type="button"
                  className="agree-disagree-button"
                  onClick={() => handleDisagree(false)}
                >
                  Disagree
                </button>
              </div>
            )}
          </div>
        ) : businessPreference === 'Guest' ? (
          <div>
            <button
              type="button"
              className="agree-disagree-button"
              onClick={(e) => handleContinue()}
            >
              Continue
            </button>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default TermsAndConditions;
