import React from 'react';
import { Link } from 'react-router-dom';
import './Sponsor.scss';

const Sponsor = (props) => {
  return (
    <section className="landing-page-section sponsor">
      <div className="container">
        <div className="row">
          <h1 className="landing-page__heading">About Tasttlig</h1>
        </div>
        <div className="row sponsor__layout">
          <div className="col">
            <h1 className="sponsor__heading">Our Mission</h1>
            <p className="sponsor__body">
              Tasttlig’s mission is to showcase the world in the best light. We do this by hosting
              multicultural food festivals in neighbourhoods. Experience Tasttlig by creating your
              own Tasttlig passport to travel through your neighbourhood and taste food from around
              the world in your local restaurant.
            </p>
            <button className="landing-page__button-primary-black-solid sponsor__call-to-action">
              <Link exact="true" to="/sign-up" className="join-button-sponsor-tasttlig">
                Join
              </Link>
            </button>
          </div>
          <div className="col">
            <img className="sponsor__picture" src="/img/OurMission.jpg" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Sponsor;
