// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../ContextProvider/AppProvider';
import { Link } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';
import Header from './Header/Header';
import Explore from './Explore/Explore';
import HowItWorks from './HowItWorks/HowItWorks';
import CreateYourPassport from './CreateYourPassport/CreateYourPassport';
import Pricing from '../Pricing/Subscriptions/Subscriptions';
import Experience from './Experience/Experience';
import Sponsor from './Sponsor/Sponsor';
import Footer from '../Footer/Footer';
import GoTop from '../Shared/GoTop';

// Styling
import './LandingPage.scss';

const LandingPage = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [navBackground, setNavBackground] = useState('nav-transparent');
  const [toggleGoTop, setToggleGoTop] = useState(false);
  const [festival_name] = useState(props.festival_name);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedItem, setSelectedItem] = useState('');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  console.log(appContext);

  //removing host value if returned back to landing page from sign-up page
  localStorage.removeItem('dataFromFestival');
  localStorage.removeItem('valueFromFestivalTicket');
  localStorage.removeItem('festivalPayments');
  localStorage.removeItem('VendFromSignOut');
  localStorage.removeItem('business-preference');
  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  const listenScrollEvent = (e) => {
    if (window.scrollY > 200) {
      return setNavBackground('nav-white');
    } else {
      return setNavBackground('nav-transparent');
    }
  };

  const listenGoTop = (e) => {
    if (window.scrollY > 400) {
      return setToggleGoTop(true);
    } else {
      return setToggleGoTop(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', listenScrollEvent);
    return () => {
      window.removeEventListener('scroll', listenScrollEvent);
    };
  }, []);

  useEffect(() => {
    window.addEventListener('scroll', listenGoTop);
    return () => {
      window.removeEventListener('scroll', listenGoTop);
    };
  }, []);

  return (
    <div>
      <Nav
        background={navBackground}
        keyword={props.location.state.keyword}
        url={
          window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/` ||
          window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/` ||
          window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/`
            ? '/'
            : '/09-2020-festival'
        }
      />
      <Header />
      <Explore />
      <CreateYourPassport />
      {/* <Pricing /> */}
      <Experience />
      <HowItWorks />
      <Sponsor />
      <Footer />
      <GoTop visible={toggleGoTop} />
    </div>
  );
};

export default LandingPage;
