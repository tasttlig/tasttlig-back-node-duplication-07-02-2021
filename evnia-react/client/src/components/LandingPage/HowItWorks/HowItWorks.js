import React, { useState, useEffect } from 'react';
import './HowItWorks.scss';

const HowItWorks = (props) => {
  const [width, setWidth] = useState(window.innerWidth);
  const breakpoint = 780;

  useEffect(() => {
    window.addEventListener('resize', () => setWidth(window.innerWidth));
  }, []);

  return (
    <section className="landing-page-section how-it-works">
      {/* <div className="container">
        <div className="row">
          <h1 className="landing-page__heading">How It Works</h1>
        </div>
        <div className="explore-cards">
          {width < breakpoint ? (
            <img alt="How Tasttlig Works" src="/img/vertical-how-it-works-new.png" />
          ) : (
            <img alt="How Tasttlig Works" src="/img/how-it-works-new.png" />
          )}
        </div>
      </div> */}
    </section>
  );
};

export default HowItWorks;
