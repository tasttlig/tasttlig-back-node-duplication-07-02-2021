import React, { useState, useEffect } from 'react';
import { scroller } from 'react-scroll';
import { Link } from 'react-router-dom';
import './Header.scss';
import TermsAndConditions from '../TermsAndConditions/TermsAndConditions';
import VendingConditions from '../TermsAndConditions/VendingConditions';
import Modal from 'react-modal';
import BusinessConditions from '../TermsAndConditions/BusinessConditions';

const Header = (props) => {
  const [width, setWidth] = useState(window.innerWidth);
  const breakpoint = 780;
  const [viewTermsAndConditions, setViewTermsAndConditions] = useState(false);
  const [vendingConditions, setVendingConditions] = useState(false);
  const [businessConditions, setBusinessConditions] = useState(false);

  const scrollToSection = () => {
    scroller.scrollTo('explore-tasttlig', {
      duration: 100,
      delay: 0,
      smooth: 'linear',
    });
  };

  useEffect(() => {
    window.addEventListener('resize', () => setWidth(window.innerWidth));
  }, []);

  return (
    <header className="landing-page__header">
      <div className="row header-row">
        <div
          className={
            width < breakpoint && width > 480 ? 'col align-self-center' : 'col-12 col-md-7'
          }
        >
          {/* <h3 className="header__main-heading">Attract More Customers. Increase Your Revenue. Host Our Festival In Your Restaurant.</h3> */}
          <h3 className="header__main-heading">PROMOTE YOUR RESTAURANT IN OUR FESTIVALS</h3>
        </div>
      </div>
      <div className="row header-row">
        <div className={width < breakpoint ? 'col align-self-center' : 'col-12 col-md-8'}>
          {/* <button className="landing-page__button-red-primary header__main-header-button"> */}
          {/* <button
            className="main-navbar-item nav-join-btn"
            // onClick={(e) => setVendingConditions(true)}
            onClick={() =>  {localStorage.setItem('business-preference', 'Business');
            window.location.href='/sign-up'}}
          >
            Add your Restaurant
          </button> */}
         <div>
            <Link exact="true" to="/complete-profile/festival-vendor">
                <button
                  type="button"
                  className="main-navbar-item nav-host-btn"
                  onClick={ () => localStorage.setItem('VendFromSignOut', 'true')}
                >
                    View Vendor Packages
                </button>
              </Link>
            </div>
      
          {vendingConditions ? (
              <Modal
                className="terms-conditions-modal"
                isOpen={vendingConditions}
                onRequestClose={() => setVendingConditions(false)}
              >
                <div className="terms-conditions-background-image" />
                <VendingConditions
                  user_id={props.user_id}
                  viewVendingConditionsState={vendingConditions}
                  changeVendingConditions={(e) => setVendingConditions(e)}
                  changeBusinessConditions={(e) => 
                    {setVendingConditions(false);
                    setBusinessConditions(e)}}
                />
              </Modal>
            ) : null}
             {businessConditions ? (
                  <Modal
                    className="terms-conditions-modal"
                    isOpen={businessConditions}
                    onRequestClose={() => setBusinessConditions(false)}
                  >
                    <div className="terms-conditions-background-image" />
                    <BusinessConditions
                      user_id={props.user_id}
                      viewHostingConditionsState={businessConditions}
                      changeBusinessConditions={(e) => setBusinessConditions(e)}
                    />
                  </Modal>
                ) : null}
          {/* <div>
                <button type="button"  className="main-navbar-item nav-join-btn" onClick={(e) => setVendingConditions(true)}>
                    VEND
                  </button>

                  {vendingConditions ? (
                <Modal className="terms-conditions-modal" 
                 isOpen ={vendingConditions} onRequestClose={() => setVendingConditions(false) }>
                  <div className = "terms-conditions-background-image"/>                  
                  <VendingConditions
                    user_id={props.user_id}
                    viewVendingConditionsState={vendingConditions}
                    changeVendingConditions={(e) => setVendingConditions(e)}
                    />
                </Modal>
                  ) : null} */}
        </div>
      </div>
      <div className="header__learn-more" onClick={scrollToSection}>
        <span className="learn-more-text">Learn More</span>
        <span className="learn-more-arrow">
          <i className="fas fa-angle-down"></i>
        </span>
      </div>
    </header>
  );
};

export default Header;
