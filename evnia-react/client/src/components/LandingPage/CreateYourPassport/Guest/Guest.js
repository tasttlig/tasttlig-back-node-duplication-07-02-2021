import React from 'react';
import { Link } from 'react-router-dom';
import './Guest.scss';

const Guest = (props) => {
  return (
    <div className="row create-your-passport__layout">
      <div className="col create-your-passport-content">
        <h1 className="create-your-passport__heading">Tasttlig Guest Passport</h1>
        <p className="create-your-passport__body">
          Your guest passport provides you <span className="red-text">access </span> to the food
          festival in your neighbourhood.
        </p>
        <Link exact="true" to="/sign-up" className="join-button-create-passport">
          <button className="landing-page__button-primary-black-solid create-your-passport__call-to-action">
            Join
          </button>
        </Link>
      </div>
      <div className="col">
        <img className="create-your-passport__picture" src="/img/guest.jpeg" />
      </div>
    </div>
  );
};

export default Guest;
