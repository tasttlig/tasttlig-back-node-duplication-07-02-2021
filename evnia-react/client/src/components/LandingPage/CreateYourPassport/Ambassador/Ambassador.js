import React from 'react';
import { Link } from 'react-router-dom';
import './Ambassador.scss';

const Ambassador = (props) => {
  return (
    <div className="row create-your-passport__layout">
      <div className="col">
        <img className="create-your-passport__picture" src="/img/member.jpeg" />
      </div>
      <div className="col create-your-passport-content">
        <h1 className="create-your-passport__heading">Tasttlig Ambassador Passport</h1>
        <p className="create-your-passport__body">
          Your ambassador passport provides you with{' '}
          <span className="red-text">free food tasting</span>, and{' '}
          <span className="red-text">exclusive deals</span> at Tasttlig festivals in your
          neighbourhood and around the world.
        </p>
        <Link exact="true" to="/sign-up" className="join-button-create-passport">
          <button className="landing-page__button-primary-black-solid create-your-passport__call-to-action">
            Apply
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Ambassador;
