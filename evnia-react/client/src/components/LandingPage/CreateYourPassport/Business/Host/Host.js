import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../../../ContextProvider/AppProvider';
import './Host.scss';

const Host = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div className="row create-your-passport__layout business-tab-content">
      <p className="create-your-passport__body">
        Host with <strong>Tasttlig</strong> for free and{' '}
        <span className="red-text">earn money</span>.
      </p>
      <button className="landing-page__button-primary-black-solid create-your-passport__call-to-action">
        {!appContext.state.user.id ? (
          <Link exact="true" to="/sign-up">
            Apply to Host
          </Link>
        ) : (
          <Link exact="true" to="/">
            Apply to Host
          </Link>
        )}
      </button>
    </div>
  );
};

export default Host;
