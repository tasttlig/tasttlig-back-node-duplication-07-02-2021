import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import axios from 'axios';
import { DateInput, Form, MultiImageInput } from '../../../EasyForm';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';

// Styling
import './Pricing.scss';
import { Fragment } from 'react';

toast.configure();

const Pricing = (props) => {
  const history = useHistory();
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  console.log('userRole', appContext);
  let role = userRole && userRole.includes('HOST') ? 'host' : 'guest';

  const { userSubscription, setUserSubscription } = props;

  const membershipPaymentFirst = async () => {
    await history.push({
      pathname: `/payment/package/G_MSHIP1`,
      /* title: props.title,
      festivalId: props.festivalId, */
    });
  };

  const membershipPaymentSecond = async () => {
    await history.push({
      pathname: `/payment/package/G_MSHIP2`,
      /* title: props.title,
      festivalId: props.festivalId, */
    });
  };

  const membershipPaymentThird = async () => {
    await history.push({
      pathname: `/payment/package/G_MSHIP3`,
      /* title: props.title,
      festivalId: props.festivalId, */
    });
  };

  const vendingHostPaymentFirst = async () => {
    await history.push({
      pathname: `/payment/package/H_VEND1`,
    });
  };

  const vendingHostPaymentSecond = async () => {
    await history.push({
      pathname: `/payment/package/H_VEND2`,
    });
  };

  const vendingHostPaymentThird = async () => {
    await history.push({
      pathname: `/payment/package/H_VEND3`,
    });
  };

  const getFreeGuest = async () => {
    await history.push({
      pathname: `/payment/package/G_BASIC`,
    });
  };
  const getFreeHost = async () => {
    await history.push({
      pathname: `/payment/package/H_BASIC`,
    });
  };
  const getAmbassadorGuest = async () => {
    await history.push({
      pathname: `/payment/package/G_AMB`,
    });
  };
  // const getAmbassadorHost = async () => {
  //   await history.push({
  //     pathname: `/payment/package/H_AMB`,
  //   });
  // };

  // const handlePayment = () => {
  //   if (userSubscription === 'host') {
  //     vendingHostPayment();
  //   } else {
  //     membershipPayment();
  //   }
  // };
  const handlePaymentFirstPackage = () => {
    if (userSubscription === 'host') {
      vendingHostPaymentFirst();
    } else {
      membershipPaymentFirst();
    }
  };
  const handlePaymentSecondPackage = () => {
    if (userSubscription === 'host') {
      vendingHostPaymentSecond();
    } else {
      membershipPaymentSecond();
    }
  };
  const handlePaymentThirdPackage = () => {
    if (userSubscription === 'host') {
      vendingHostPaymentThird();
    } else {
      membershipPaymentThird();
    }
  };
  const handleFree = () => {
    if (userSubscription === 'host') {
      getFreeHost();
    } else {
      getFreeGuest();
    }
  };
  // const handleAmbassador = () => {
  //   if (userSubscription === 'host') {
  //     getAmbassadorHost();
  //   } else {
  //     getAmbassadorGuest();
  //   }
  // };

  return (
    <section className="landing-page-section create-your-passport">
      <div className="container">
        <div className="row cards-row">
          {userSubscription === 'ambassador' ? (
            <Fragment>
              <div className="col-lg-4 col-md-12 col-sm-12 card">
                <div className="card-title">Ambassador</div>
                <div>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Attend Festivals.
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Free Food Tasting.
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Buy all Business contents for 30 promotion at retail prices.
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    25% Discounts
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Provided after approval from admin.
                  </p>
                </div>
                {(userRole && userRole.includes('HOST')) ||
                (userRole && userRole.includes('HOST_PENDING')) ? (
                  <div className="tab active-tab absolute-div text-center">Already Applied!</div>
                ) : (
                  // <Link exact="true" to="/become-a-host">
                  <div className="pt-5">
                    {/* <div className="tab active-tab absolute-div text-center" onClick={handleAmbassador}> */}
                    <button className="pricing-button-primary pricing-button-disabled">
                      Apply Now
                    </button>
                  </div>
                  // {/* </Link> */}
                )}
              </div>
              <div className="col-lg-4 col-md-12 col-sm-12 card">
                <div className="card-title">Ambassador Host</div>
                <div>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Attend Festivals.
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Free Food Tasting.
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Put all your Business contents for 30 promotions.
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    0% Commission.
                  </p>
                  <p>
                    <i className="fas fa-check-circle"></i>
                    Donated after approval by Admin.
                  </p>
                </div>
                {(userRole && userRole.includes('HOST')) ||
                (userRole && userRole.includes('HOST_PENDING')) ? (
                  <div className="tab active-tab absolute-div text-center">Already Applied!</div>
                ) : (
                  // <Link exact="true" to="/become-a-host">
                  <div className="pt-5">
                    {/* <div className="tab active-tab absolute-div text-center" onClick={handleAmbassador}> */}
                    <button className="pricing-button-primary pricing-button-disabled">
                      Apply Now
                    </button>
                  </div>
                  // </Link>
                )}
              </div>
            </Fragment>
          ) : (
            <Fragment>
              {/* card 1 */}
              <div className="col-lg-3 col-md-12 col-sm-12 card">
                <div className="card-title">
                  {userSubscription === 'host' ? 'Host Default' : 'Guest Default'}
                </div>
                {userSubscription === 'host' ? (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Put all your Business contents for 30 promotions.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      25% Commission.
                    </p>
                  </div>
                ) : (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Business contents for 30 promotion at retail prices.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      No Discounts
                    </p>
                  </div>
                )}
                <div className="pt-5">
                  <button className="pricing-button-primary" onClick={handleFree}>
                    Free
                  </button>
                </div>
              </div>
              {/* end card 1 */}
              {/* card 2 */}
              <div className="col-lg-3 col-md-12 col-sm-12 card">
                <div className="card-title">
                  {userSubscription === 'host' ? 'Host One' : 'Guest One'}
                </div>
                {userSubscription === 'host' ? (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Put all your Business contents for 30 promotions.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      5% Commission.
                    </p>
                  </div>
                ) : (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Buy all Business contents for 30 promotion at retail prices.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      5% Discounts
                    </p>
                  </div>
                )}
                <div className="pt-5">
                  <button
                    className="pricing-button-primary pricing-button-disabled"
                    onClick={handlePaymentFirstPackage}
                  >
                    {userSubscription === 'host' ? '$300.00' : '$10.00'}
                  </button>
                </div>
              </div>
              {/* end card 2 */}
              {/* card 3 */}
              <div className="col-lg-3 col-md-12 col-sm-12 card">
                <div className="card-title">
                  {userSubscription === 'host' ? 'Host Two' : 'Guest Two'}
                </div>
                {userSubscription === 'host' ? (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Put all your Business contents for 30 promotions.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      15% Commission.
                    </p>
                  </div>
                ) : (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Buy all Business contents for 30 promotion at retail prices.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      15% Discounts
                    </p>
                  </div>
                )}
                <div className="pt-5">
                  <button
                    className="pricing-button-primary pricing-button-disabled"
                    onClick={handlePaymentSecondPackage}
                  >
                    {userSubscription === 'host' ? '$200.00' : '$20.00'}
                  </button>
                </div>
              </div>
              {/* end card 3 */}
              {/* card 4 */}
              <div className="col-lg-3 col-md-12 col-sm-12 card">
                <div className="card-title">
                  {userSubscription === 'host' ? 'Host Three' : 'Guest Three'}
                </div>
                {userSubscription === 'host' ? (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Put all your Business contents for 30 promotions.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      20% Commission.
                    </p>
                  </div>
                ) : (
                  <div>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Attend Festivals.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Free Food Tasting.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      Buy all Business contents for 30 promotion at retail prices.
                    </p>
                    <p>
                      <i className="fas fa-check-circle"></i>
                      20% Discounts
                    </p>
                  </div>
                )}
                <div className="pt-5">
                  <button
                    className="pricing-button-primary pricing-button-disabled"
                    onClick={handlePaymentThirdPackage}
                  >
                    {userSubscription === 'host' ? '$100.00' : '$30.00'}
                  </button>
                </div>
              </div>
              {/* end card 4 */}
            </Fragment>
          )}
        </div>
      </div>
    </section>
  );
};

export default Pricing;
