import React, { useState, useContext } from 'react';
import moment from 'moment';
import { Link, useHistory } from 'react-router-dom';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { formatDate } from '../../../Functions/Functions';
import TermsAndConditions from '../../TermsAndConditions/TermsAndConditions';
import VendingConditions from '../../TermsAndConditions/VendingConditions';
import Modal from 'react-modal';

import './FestivalCard.scss';

const FestivalCard = (props) => {
  const [load, setLoad] = useState(false);
  const [viewTermsAndConditions, setViewTermsAndConditions] = useState(false);
  const [vendingConditions, setVendingConditions] = useState(false);

  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
  const history = useHistory();
  // Set date and time
  const startDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.startTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.endTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const truncateDescription = (description) => {
    if (description.length > 130) {
      if (description[description.length - 1] === '.') {
        description = description.substr(0, 130) + '..';
      } else {
        description = description.substr(0, 130) + '...';
      }
    }
    return description;
  };

  return (
    <div className="col main-festival-card">
      <div className="festival-card-photo">
        {upcomingDays > 0 ? (
          <span className="days-remaining">{`${upcomingDays} day${
            upcomingDays > 1 ? 's' : ''
          }`}</span>
        ) : moment().isSameOrBefore(props.endDate) ? (
          <span className="days-remaining">
            <strong>Happening Now</strong>
          </span>
        ) : (
          <span className="days-remaining">
            <strong>Done</strong>
          </span>
        )}
        <img
          className="festival-card-photo"
          src={props.images[0]}
          alt={props.title}
          onLoad={() => setLoad(true)}
        />
      </div>
      <div className="main-festival-info">
        <div className="festival-location">
          <i className="fas fa-map-marker-alt"></i> <strong>{props.city}</strong>
        </div>
        <div className="festival-date">
          <i className="fas fa-calendar"></i>{' '}
          <strong>{`${formatDate(props.startDate)} - ${formatDate(props.endDate)}`}</strong>
        </div>
        <div className="festival-name">{props.title}</div>
      </div>
      <div >
      
      </div>
      <div className="purchase-ticket-button">
        {!appContext.state.user.id ? (
          <div>
            <button className="nav-host-btn"
            onClick={(e) => window.location.href='/sign-up'} >
                Attend
            </button>
          </div>
        ) : (
          <Link exact="true" to={`/payment/festival/${props.festivalId}`}>
            Join
          </Link>
        )}
        {vendingConditions ? (
          <Modal className="terms-conditions-modal" 
            isOpen ={vendingConditions} onRequestClose={() => setVendingConditions(false) }>
            <div className = "terms-conditions-background-image"/>                  
            <VendingConditions
              user_id={props.user_id}
              viewVendingConditionsState={vendingConditions}
              changeVendingConditions={(e) => setVendingConditions(e)}
              />
          </Modal>
          ) : null}
      </div>
    </div>
  );
};

export default FestivalCard;
