import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './HostLandingPage.scss';
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer';
import TermsAndConditions from '../TermsAndConditions/TermsAndConditions';
// import { Modal, Button, Row, Col } from 'react-bootstrap';
import Modal from 'react-modal';
import { AppContext } from '../../../ContextProvider/AppProvider';

const Host = (props) => {
  const [navBackground, setNavBackground] = useState('nav-white');
  // const [viewTermsAndConditions, setViewTermsAndConditions] = useState(false);
  localStorage.removeItem('dataFromFestival');
  localStorage.removeItem('business-preference');

  const handleOnCLick = (e) => {
    // setViewTermsAndConditions(true);
    localStorage.setItem('dataFromFestival', 'Host');
    window.location.href = '/sign-up';
    // console.log('modal state',viewTermsAndConditions);
  };


  return (
    <div>
      <Nav
        background={navBackground}
  
      />
      <div className="container host-landing">
        <section className="landing-page-section experience host-landing-experience">
          <div className="col experience-content host-landing-experience-content">
         
            <h2 className="landing-page__heading-light experience-heading host-landing-experience-heading">
            Celebrate Your Culture, Bring our Festival to Your Restaurant!
            </h2>
            <h4 className="experience-message host-landing-experience-message">
              Accelerate the growth of your restaurants by hosting your food samples for free.
            </h4>

            {/* <button className="landing-page__button-red-primary header__main-header-button host-landing-cta"
            onClick={(e) => setViewTermsAndConditions(true)} > */}

            <button className="landing-page__button-red-primary header__main-header-button host-landing-cta"
            onClick={(e) => handleOnCLick(e)}>
              
                Add Your Restaurant
              
            </button>
            {/* {viewTermsAndConditions ? (
                <Modal className="terms-conditions-modal" 
                 isOpen ={viewTermsAndConditions} onRequestClose={() => setViewTermsAndConditions(false) }>
                  <div className = "terms-conditions-background-image"/>                  
                  <TermsAndConditions
                    user_id={props.user_id}
                    viewTermsAndConditionsState={viewTermsAndConditions}
                    changeViewTermsAndConditions={(e) => setViewTermsAndConditions(e)}
                    />
              </Modal>
            ) : null} */}
          </div>
          <div className="col experience-content-right">
            <div className="host-landing-point-cards">
              <i class="far fa-check-circle"></i>Increase your customer conversion rate.
            </div>
            <div className="host-landing-point-cards">
              <i class="far fa-check-circle"></i>Reduce your customer aquisition cost.
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </div>
  );
};

export default Host;
