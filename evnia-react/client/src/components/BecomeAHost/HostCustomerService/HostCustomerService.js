// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link, useHistory } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
// import ProductForm from "./Apply/ApplyForm/FormSteps/ProductForm";
// Components
import Nav from '../../Navbar/Nav';
import {
  listHostedEvents,
  countriesOnPassport,
} from '../../../../src/components/Functions/Functions';

// Styling
import './HostCustomerService.scss';
import 'react-toastify/dist/ReactToastify.css';
toast.configure();

const HostCustomerService = (props) => {
  console.log('props from become a host:', props);
  // Set initial state
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [video, setVideo] = useState('');
  const [description, setDescription] = useState('');
  const [governmentId, setGovernmentId] = useState([]);
  const [firstNameError, setFirstNameError] = useState('');
  const [lastNameError, setLastNameError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [videoError, setVideoError] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [openCompleteProfile, setOpenCompleteProfile] = useState(false);
  const [hostedEvents, setHostedEvents] = useState([]);
  const [hostedCountry, setHostedCountry] = useState([]);
  const [haveYouHostedCountry, setHaveYouHostedCountry] = useState('');
  const [descriptionError, setDescriptionError] = useState('');

  const [provideGames, setProvideGames] = useState('');
  const [noprovideGamesButton, setNoprovideGamesButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [provideGamesButton, setprovideGamesButton] = useState('do-you-have-a-restaurant-no1-btn');

  const [provideExcellentService, setProvideExcellentService] = useState('');
  const [noprovideExcellentServiceButton, setNoprovideExcellentServiceButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [provideExcellentServiceButton, setprovideExcellentServiceButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [hostedTasttligFestival, setHostedTasttligFestival] = useState('');
  const [nohostedTasttligFestivalButton, setNohostedTasttligFestivalButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [hostedTasttligFestivalButton, sethostedTasttligFestivalButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [subscriptionResponse, setSubscriptionResponse] = useState([]);
  const [abidehealth, setAbideHealth] = useState('');
  const [noabidehealthButton, setNoabidehealthButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [abidehealthButton, setabidehealthButton] = useState('do-you-have-a-restaurant-no1-btn');

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  //getting data from hostFoodsampleInquiry form
  const dataFromHostFoodSampleInquiry = JSON.parse(
    localStorage.getItem('dataFromHostFoodSampleInquiry'),
  );
  console.log('data from all previous', dataFromHostFoodSampleInquiry);

  const fetchSubscriptions = async () => {
    const response = await axios({
      method: 'GET',
      url: `/subscription/details`,
      params: {
        item_type: 'package',
        item_id: 'H_AMB'
      },
    });
    setSubscriptionResponse(response.data.item);
    return response;
  };

  // Handle file upload helper function
  const handleFileUpload = (input) => async (event) => {
    if (video.length < 1 || governmentId.length < 1) {
      let dir_name = '';
      // Set the directory name, depending on the input
      switch (input) {
        case 'video':
          dir_name = 'become-a-host-videos';
          break;
        default:
          dir_name = 'government-id-images';
      }

      let file = event.target.files[0];
      let fileParts = file.name.split('.');
      let fileName = `${dir_name}/${uuidv4()} ${fileParts[0]}.${fileParts[1]}`;
      let fileType = fileParts[1];

      axios
        .post('/s3_signed_url', { fileName, fileType })
        .then((response) => {
          let signedRequest = response.data.signedRequest;
          let url = response.data.url;
          let options = {
            headers: {
              'Content-Type': fileType,
            },
          };

          axios
            .put(signedRequest, file, options)
            .then(() => {
              console.log('im here');
              switch (input) {
                case 'video':
                  console.log('url for video', url);
                  setVideo(url);
                  break;
                default:
                  setGovernmentId((governmentId) => [...governmentId, url]);
              }
            })
            .catch((error) => {
              console.log(`ERROR ${JSON.stringify(error)}`);
            });
        })
        .catch((error) => {
          console.log(JSON.stringify(error));
        });
    }
  };

  /*   // Validate user input for Become a Host helper function
  const validateBecomeAHost = () => {
    window.scrollTo(0, 0);

    // Render first name error message
    if (!appContext.state.signedInStatus && !firstName) {
      setFirstNameError("First name is required.");
    } else {
      setFirstNameError("");
    }

    // Render last name error message
    if (!appContext.state.signedInStatus && !lastName) {
      setLastNameError("Last name is required.");
    } else {
      setLastNameError("");
    }

    // Render email error message
    if (!appContext.state.signedInStatus && !email) {
      setEmailError("Email is required.");
    } else if (
      !appContext.state.signedInStatus &&
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)
    ) {
      setEmailError("Please enter a valid email.");
    } else {
      setEmailError("");
    }


    // Render video error message
    if (!video[0]) {
      setVideoError("Video is required.");
    } else {
      setVideoError("");
    }

    // // Render description error message
    // if (!description) {
    //   setDescriptionError("Description is required.");
    // } else {
    //   setDescriptionError("");
    // }


    if (
      (!appContext.state.signedInStatus && !firstName) ||
      (!appContext.state.signedInStatus && !lastName) ||
      (!appContext.state.signedInStatus && !email) ||
      (!appContext.state.signedInStatus &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) 
        ||
        !video[0] 
      // !phoneNumber ||
      // phoneNumber.length < 14 ||
      // !description ||
      // !governmentId[0]
    ) {
      return false;
    }

    return true;
  }; */
  const history = useHistory();
  // Submit Become a Host helper function
  const handleSubmitBecomeAHost = async (event) => {
    event.preventDefault();
    console.log(JSON.parse(localStorage.getItem('dataFromBecomeHost')));

    //const isValid = validateBecomeAHost();

    //if (isValid) {
    const urlLoggedIn = '/hosts/request-host';

    const acc_token = await localStorage.getItem('access_token');

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      first_name: appContext.state.signedInStatus
        ? appContext.state.user.first_name
        : JSON.parse(localStorage.getItem('dataFromBecomeHost')).first_name,
      last_name: appContext.state.signedInStatus
        ? appContext.state.user.last_name
        : JSON.parse(localStorage.getItem('dataFromBecomeHost')).last_name,
      email: appContext.state.signedInStatus
        ? appContext.state.user.email
        : JSON.parse(localStorage.getItem('dataFromBecomeHost')).email,
      host_user_id: appContext.state.user.id,
      // role: appContext.state.user.role,
      host_video_url: video,
      host_description: description,

      //data from become-a-host page
      has_hosted_anything_before: dataFromHostFoodSampleInquiry.has_hosted_anything_before,
      have_a_restaurant: dataFromHostFoodSampleInquiry.have_a_restaurant,
      cuisine_type: dataFromHostFoodSampleInquiry.cuisine_type,
      seating_option: dataFromHostFoodSampleInquiry.seating_option,
      want_people_to_discover_your_cuisine:
        dataFromHostFoodSampleInquiry.want_people_to_discover_your_cuisine,
      able_to_provide_food_samples: dataFromHostFoodSampleInquiry.able_to_provide_food_samples,
      is_host: dataFromHostFoodSampleInquiry.is_host,
      has_hosted_other_things_before: dataFromHostFoodSampleInquiry.has_hosted_other_things_before,

      //data from /become-a-host/food-sample-inquiries
      able_to_explain_the_origins_of_tasting_samples:
        dataFromHostFoodSampleInquiry.able_to_explain_the_origins_of_tasting_samples,
      able_to_proudly_showcase_your_culture:
        dataFromHostFoodSampleInquiry.able_to_proudly_showcase_your_culture,
      able_to_provie_private_dining_experience:
        dataFromHostFoodSampleInquiry.able_to_provie_private_dining_experience,
      able_to_provide_3_or_more_course_meals_to_guests:
        dataFromHostFoodSampleInquiry.able_to_provide_3_or_more_course_meals_to_guests,
      able_to_provide_live_entertainment:
        dataFromHostFoodSampleInquiry.able_to_provide_live_entertainment,
      able_to_provide_other_form_of_entertainment:
        dataFromHostFoodSampleInquiry.able_to_provide_other_form_of_entertainment,

      //data from /become-a-host/customer-service-inquiries
      able_to_abide_by_health_safety_regulations: abidehealth,
      hosted_tasttlig_festival_before: hostedTasttligFestival,
      able_to_provide_excellent_customer_service: provideExcellentService,
      able_to_provide_games_about_culture_cuisine: provideGames,
      subscriptionResponse: subscriptionResponse,
    };
    console.log(data);
    try {
      let response;

      //if (appContext.state.signedInStatus) {
      response = await axios({
        method: 'POST',
        url: urlLoggedIn,
        headers,
        data,
      });
      // }
      console.log(response);

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          //window.location.href = '/become-a-host/create-food-sample';
          //window.location.href = '/dashboard';
          history.push('/dashboard');
        }, 2000);

        toast(`Success! Thank you for submitting Become a Host form!`, {
          type: 'success',
          autoClose: 2000,
        });

        localStorage.removeItem('application-type');

        setErrorMessage('');
        setSubmitAuthDisabled(true);
      } else {
        setErrorMessage(response.data.message);
      }
    } catch (error) {
      console.log(error);
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
    //}
  };

  // Mount Become a Host page
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchSubscriptions();
  }, []);

  // Render Become a Host page
  return (
    <div className="row">
      <Nav />
      <div className="col-lg-8 col-xl-6 px-0 sign-up-background-image-one" />
      <div className="col-lg-4 col-xl-4 px-0 become-a-host">
        <div className="become-a-host-title">Become a Host</div>

        <form onSubmit={handleSubmitBecomeAHost} noValidate>
          {/* {!appContext.state.signedInStatus && (
            <div>
              <div className="mb-3">
                <div className="input-title">First Name*</div>
                <input
                  type="text"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  disabled={submitAuthDisabled}
                  className="form-control"
                  required
                />
                {firstNameError && (
                  <div className="error-message">{firstNameError}</div>
                )}
              </div>
              <div className="mb-3">
                <div className="input-title">Last Name*</div>
                <input
                  type="text"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  disabled={submitAuthDisabled}
                  className="form-control"
                  required
                />
                {lastNameError && (
                  <div className="error-message">{lastNameError}</div>
                )}
              </div>
              <div className="mb-3">
                <div className="input-title">Email*</div>
                <input
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  disabled={submitAuthDisabled}
                  className="form-control"
                  required
                />
                {emailError ? (
                  <div className="error-message">{emailError}</div>
                ) : errorMessage ? (
                  <div className="error-message">{errorMessage}</div>
                ) : null}
              </div>
            </div>
          )} */}
          <div className="mb-3">
            <h6>Are you able to provide games about your cuisine and culture to our guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideGames('');
                      setProvideGames('Yes');
                      setNoprovideGamesButton('do-you-have-a-restaurant-no1-btn');
                      setprovideGamesButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={provideGamesButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideGames('');
                      setProvideGames('No');
                      setprovideGamesButton('do-you-have-a-restaurant-no1-btn');
                      setNoprovideGamesButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noprovideGamesButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Are you able to provide excellent customer service to our guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideExcellentService('');
                      setProvideExcellentService('Yes');
                      setNoprovideExcellentServiceButton('do-you-have-a-restaurant-no1-btn');
                      setprovideExcellentServiceButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={provideExcellentServiceButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideExcellentService('');
                      setProvideExcellentService('No');
                      setprovideExcellentServiceButton('do-you-have-a-restaurant-no1-btn');
                      setNoprovideExcellentServiceButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noprovideExcellentServiceButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Have you ever hosted a Tasttlig festival or experiences before?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setHostedTasttligFestival('');
                      setHostedTasttligFestival('Yes');
                      setNohostedTasttligFestivalButton('do-you-have-a-restaurant-no1-btn');
                      sethostedTasttligFestivalButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={hostedTasttligFestivalButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setHostedTasttligFestival('');
                      setHostedTasttligFestival('No');
                      sethostedTasttligFestivalButton('do-you-have-a-restaurant-no1-btn');
                      setNohostedTasttligFestivalButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={nohostedTasttligFestivalButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>
              Are you able to abide by the health and safety regulations of the government and
              Tasttlig platform?
            </h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setAbideHealth('');
                      setAbideHealth('Yes');
                      setNoabidehealthButton('do-you-have-a-restaurant-no1-btn');
                      setabidehealthButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={abidehealthButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setAbideHealth('');
                      setAbideHealth('No');
                      setabidehealthButton('do-you-have-a-restaurant-no1-btn');
                      setNoabidehealthButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noabidehealthButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <div className="mb-3">
              <div className="input-title">Description*</div>
              <textarea
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                disabled={submitAuthDisabled}
                className="become-a-host-description"
                required
              />
              {descriptionError && <div className="error-message">{descriptionError}</div>}
            </div>

            <div className="mb-3">
              <div className="input-title">
                Upload a video link of you, your business or anything that will help us approve you
                to host Tasttlig*
              </div>
              <label className="file-upload" htmlFor="host-selection-video">
                Upload the Video
              </label>
              <input
                id="host-selection-video"
                type="file"
                accept="video/*"
                onChange={handleFileUpload('video')}
                disabled={submitAuthDisabled}
                className="custom-file-input"
                required
              />
              {video[0] ? (
                <div className="file-upload-successful">File upload successful.</div>
              ) : videoError ? (
                <div className="error-message">{videoError}</div>
              ) : null}
            </div>
          </div>
          <div>
            <button type="submit" disabled={submitAuthDisabled} className="submit-button">
              Submit
            </button>
          </div>
        </form>
      </div>
      <div className="col-xl-2 px-0 sign-up-background-image-two" />
    </div>
  );
};

export default HostCustomerService;
