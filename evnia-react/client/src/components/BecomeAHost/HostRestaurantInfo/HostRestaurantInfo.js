// // Libraries
// import React, { useState, useEffect, useContext } from "react";
// import axios from "axios";
// import { Link } from "react-router-dom";
// import { v4 as uuidv4 } from "uuid";
// import { toast } from "react-toastify";
// import { AppContext } from "../../ContextProvider/AppProvider";
// // import ProductForm from "./Apply/ApplyForm/FormSteps/ProductForm";
// // Components
// import Nav from "../Navbar/Nav";
// import { formatPhoneNumber } from "../Functions/Functions";
// import HostButtonApply from "../../components/Apply/HostButtonApply";
// // Styling
// import "./BecomeAHost.scss";
// import "react-toastify/dist/ReactToastify.css";
// toast.configure();

// const BecomeAHost = (props) => {

// console.log("props from become a host:", props)
//   // Set initial state
//   const [firstName, setFirstName] = useState("");
//   const [lastName, setLastName] = useState("");
//   const [email, setEmail] = useState("");
//   const [phoneNumber, setPhoneNumber] = useState("");
//   const [video, setVideo] = useState([]);
//   const [description, setDescription] = useState("");
//   const [governmentId, setGovernmentId] = useState([]);
//   const [firstNameError, setFirstNameError] = useState("");
//   const [lastNameError, setLastNameError] = useState("");
//   const [emailError, setEmailError] = useState("");
//   const [phoneNumberError, setPhoneNumberError] = useState("");
//   const [videoError, setVideoError] = useState("");
//   const [descriptionError, setDescriptionError] = useState("");
//   const [governmentIdError, setGovernmentIdError] = useState("");
//   const [errorMessage, setErrorMessage] = useState("");
//   const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
//   const [openCompleteProfile, setOpenCompleteProfile] = useState(false)

//   // To use the JWT credentials
//   const appContext = useContext(AppContext);
//   // Handle file upload helper function
//   const handleFileUpload = (input) => async (event) => {
//     if (video.length < 1 || governmentId.length < 1) {
//       let dir_name = "";
//       // Set the directory name, depending on the input
//       switch (input) {
//         case "video":
//           dir_name = "become-a-host-videos";
//           break;
//         default:
//           dir_name = "government-id-images";
//       }

//       let file = event.target.files[0];
//       let fileParts = file.name.split(".");
//       let fileName = `${dir_name}/${uuidv4()} ${fileParts[0]}.${fileParts[1]}`;
//       let fileType = fileParts[1];

//       axios
//         .post("/s3_signed_url", { fileName, fileType })
//         .then((response) => {
//           let signedRequest = response.data.signedRequest;
//           let url = response.data.url;
//           let options = {
//             headers: {
//               "Content-Type": fileType,
//             },
//           };

//           axios
//             .put(signedRequest, file, options)
//             .then(() => {
//               switch (input) {
//                 case "video":
//                   setVideo((video) => [...video, url]);
//                   break;
//                 default:
//                   setGovernmentId((governmentId) => [...governmentId, url]);
//               }
//             })
//             .catch((error) => {
//               console.log(`ERROR ${JSON.stringify(error)}`);
//             });
//         })
//         .catch((error) => {
//           console.log(JSON.stringify(error));
//         });
//     }
//   };

//   // Validate user input for Become a Host helper function
//   const validateBecomeAHost = () => {
//     window.scrollTo(0, 0);

//     // Render first name error message
//     if (!appContext.state.signedInStatus && !firstName) {
//       setFirstNameError("First name is required.");
//     } else {
//       setFirstNameError("");
//     }

//     // Render last name error message
//     if (!appContext.state.signedInStatus && !lastName) {
//       setLastNameError("Last name is required.");
//     } else {
//       setLastNameError("");
//     }

//     // Render email error message
//     if (!appContext.state.signedInStatus && !email) {
//       setEmailError("Email is required.");
//     } else if (
//       !appContext.state.signedInStatus &&
//       !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)
//     ) {
//       setEmailError("Please enter a valid email.");
//     } else {
//       setEmailError("");
//     }

//     // Render phone number error message
//     if (!phoneNumber) {
//       setPhoneNumberError("Phone number is required.");
//     } else if (phoneNumber.length < 14) {
//       setPhoneNumberError("Please enter a valid phone number.");
//     } else {
//       setPhoneNumberError("");
//     }

//     // Render video error message
//     if (!video[0]) {
//       setVideoError("Video is required.");
//     } else {
//       setVideoError("");
//     }

//     // Render description error message
//     if (!description) {
//       setDescriptionError("Description is required.");
//     } else {
//       setDescriptionError("");
//     }

//     // Render government ID error message
//     if (!governmentId[0]) {
//       setGovernmentIdError("Government ID is required.");
//     } else {
//       setGovernmentIdError("");
//     }

//     if (
//       (!appContext.state.signedInStatus && !firstName) ||
//       (!appContext.state.signedInStatus && !lastName) ||
//       (!appContext.state.signedInStatus && !email) ||
//       (!appContext.state.signedInStatus &&
//         !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) ||
//       !phoneNumber ||
//       phoneNumber.length < 14 ||
//       !video[0] ||
//       !description ||
//       !governmentId[0]
//     ) {
//       return false;
//     }

//     return true;
//   };

//   // Submit Become a Host helper function
//   const handleSubmitBecomeAHost = async (event) => {
//     event.preventDefault();
//     console.log("appcontext from become a host: ", appContext)

//     const isValid = validateBecomeAHost();

//     if (isValid) {
//       const urlLoggedIn = "/hosts/request-host";
//       const urlNotLoggedIn = "/hosts/become-a-host-not-logged-in";

//       const acc_token = await localStorage.getItem("access_token");

//       const headers = { Authorization: `Bearer ${acc_token}` };

//       const data = {
//         first_name: appContext.state.signedInStatus
//           ? appContext.state.user.first_name
//           : firstName,
//         last_name: appContext.state.signedInStatus
//           ? appContext.state.user.last_name
//           : lastName,
//         email: appContext.state.signedInStatus
//           ? appContext.state.user.email
//           : email,
//         host_user_id: appContext.state.user.id,
//         // role: appContext.state.user.role[1],
//         // is_host: "yes",
//         host_video_url: video,
//         host_description: description,
//         host_government_id_url: governmentId,
//       };

//       try {
//         let response;

//         if (appContext.state.signedInStatus) {
//           response = await axios({
//             method: "POST",
//             url: urlLoggedIn,
//             headers,
//             data,
//           });
//         }

//         if (response && response.data && response.data.success) {
//           setTimeout(() => {
//             window.location.href = "/become-a-host/create-food-sample";
//           }, 2000);

//           toast(`Success! Thank you for submitting Become a Host form!`, {
//             type: "success",
//             autoClose: 2000,
//           });

//           setErrorMessage("");
//           setSubmitAuthDisabled(true);
//         } else {
//           setErrorMessage(response.data.message);
//         }
//       } catch (error) {
//         toast("Error! Something went wrong!", {
//           type: "error",
//           autoClose: 2000,
//         });
//       }
//     }
//   };

//   // Mount Become a Host page
//   useEffect(() => {
//     window.scrollTo(0, 0);
//   }, []);

//     // Render do you have a restaurant question
//     const DoYouHaveARestaurant = () => (
//       <div className="text-center">
//         <div className="do-you-have-a-restaurant-title">
//           Do you Have a Restaurant?
//         </div>

//         <div className="row">
//           <div className="col-md-6 do-you-have-a-restaurant-yes">
//           <div
//               onClick={() => setOpenCompleteProfile(!openCompleteProfile)}
//               className="do-you-have-a-restaurant-no-btn"
//             >
//               Yes
//             </div>

//           </div>
//           <div className="col-md-6 do-you-have-a-restaurant-no">
//           <Link
//               onClick={handleSubmitBecomeAHost}
//               exact="true"
//               to="/become-a-host/create-food-sample"
//               className="do-you-have-a-restaurant-yes-btn"
//             >
//               No
//             </Link>
//           </div>
//         </div>
//       </div>
//     );

//   // Render Become a Host page
//   return (
//     <div>
//       <Nav />

//       <div className="become-a-host">
//         <div className="become-a-host-title">Become a Host</div>

//         <form onSubmit={handleSubmitBecomeAHost} noValidate>
//           {!appContext.state.signedInStatus && (
//             <div>
//               <div className="mb-3">
//                 <div className="input-title">First Name*</div>
//                 <input
//                   type="text"
//                   value={firstName}
//                   onChange={(e) => setFirstName(e.target.value)}
//                   disabled={submitAuthDisabled}
//                   className="form-control"
//                   required
//                 />
//                 {firstNameError && (
//                   <div className="error-message">{firstNameError}</div>
//                 )}
//               </div>
//               <div className="mb-3">
//                 <div className="input-title">Last Name*</div>
//                 <input
//                   type="text"
//                   value={lastName}
//                   onChange={(e) => setLastName(e.target.value)}
//                   disabled={submitAuthDisabled}
//                   className="form-control"
//                   required
//                 />
//                 {lastNameError && (
//                   <div className="error-message">{lastNameError}</div>
//                 )}
//               </div>
//               <div className="mb-3">
//                 <div className="input-title">Email*</div>
//                 <input
//                   type="email"
//                   value={email}
//                   onChange={(e) => setEmail(e.target.value)}
//                   disabled={submitAuthDisabled}
//                   className="form-control"
//                   required
//                 />
//                 {emailError ? (
//                   <div className="error-message">{emailError}</div>
//                 ) : errorMessage ? (
//                   <div className="error-message">{errorMessage}</div>
//                 ) : null}
//               </div>
//             </div>
//           )}
//           <div className="mb-3">
//             <div className="input-title">Phone Number*</div>
//             <input
//               type="tel"
//               value={formatPhoneNumber(phoneNumber)}
//               onChange={(e) => setPhoneNumber(e.target.value)}
//               maxLength="14"
//               disabled={submitAuthDisabled}
//               className="form-control"
//               required
//             />
//             {phoneNumberError && (
//               <div className="error-message">{phoneNumberError}</div>
//             )}
//           </div>
//           <div className="mb-3">
//             <div className="input-title">
//               Upload 1 minute video on why you want to be selected*
//             </div>
//             <label className="file-upload" htmlFor="host-selection-video">
//               Upload the Video
//             </label>
//             <input
//               id="host-selection-video"
//               type="file"
//               accept="video/*"
//               onChange={handleFileUpload("video")}
//               disabled={submitAuthDisabled}
//               className="custom-file-input"
//               required
//             />
//             {video[0] ? (
//               <div className="file-upload-successful">
//                 File upload successful.
//               </div>
//             ) : videoError ? (
//               <div className="error-message">{videoError}</div>
//             ) : null}
//           </div>
//           <div className="mb-3">
//             <div className="input-title">Description*</div>
//             <textarea
//               value={description}
//               onChange={(e) => setDescription(e.target.value)}
//               disabled={submitAuthDisabled}
//               className="become-a-host-description"
//               required
//             />
//             {descriptionError && (
//               <div className="error-message">{descriptionError}</div>
//             )}
//           </div>

//           <div className="mb-3">
//             <div className="input-title">
//               Government ID Photo (Driver's Licence, Provincial Card, Passport)*
//             </div>
//             <label className="file-upload" htmlFor="government-id">
//               Upload the Government ID
//             </label>
//             <input
//               id="government-id"
//               type="file"
//               accept="image/x-png,image/gif,image/jpeg"
//               onChange={handleFileUpload("governmentId")}
//               disabled={submitAuthDisabled}
//               className="custom-file-input"
//               required
//             />

//             {governmentId[0] ? (
//               <div className="file-upload-successful">
//                 File upload successful.
//               </div>
//             ) : governmentIdError ? (
//               <div className="error-message">{governmentIdError}</div>
//             ) : null}

//           </div>

//              <DoYouHaveARestaurant />

//              {openCompleteProfile && (
//                     <HostButtonApply {...props}/>
//              )}

//           {/* <div className="mb-5">
//              <CreateFoodSampleForm
//              update={props.location.state}
//              heading={props.heading}
//              />
//             </div> */}

// {/* const onChangeHostedEvents = function (event) {
//     //takes the file and reads the file from buffer of array

//     if(hostedEvents.includes(event.target.value)) {
//       console.log("error")
//     }

//     else {

//       setHostedEvents(hostedEvents.concat(event.target.value))
//     } */}

//           <div>
//             {/* <button
//               type="submit"
//               disabled={submitAuthDisabled}
//               className="call-to-action-btn"
//             >
//               Submit
//             </button> */}
//           </div>
//         </form>
//       </div>
//     </div>
//   );
// };

// export default BecomeAHost;
