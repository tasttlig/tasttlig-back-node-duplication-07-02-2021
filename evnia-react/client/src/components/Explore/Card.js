// Libraries
import React, { useState, useContext } from 'react';
// import axios from "axios";
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
// import StripeCheckout from "react-stripe-checkout";
// import { toast } from "react-toastify";
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import { formatDate } from '../Functions/Functions';

// Styling
import './Card.scss';
// import "react-toastify/dist/ReactToastify.css";

// toast.configure();

const ExperienceCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [experienceDetailsOpened, setExperienceDetailsOpened] = useState(false);
  const [contactOpened, setContactOpened] = useState(false);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'experience-details') {
      setExperienceDetailsOpened(true);
    } else if (modalType === 'contact') {
      setContactOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'experience-details') {
      setExperienceDetailsOpened(false);
    } else if (modalType === 'contact') {
      setContactOpened(false);
    }
  };

  // Stripe token helper function
  // const handleToken = async token => {
  //   const url = "/charge";

  //   const acc_token = await localStorage.getItem("access_token");

  //   const headers = {
  //     Authorization: `Bearer ${acc_token}`
  //   };

  //   const data = {
  //     token: token.id,
  //     amount: props.price * 100,
  //     description: props.title,
  //     receipt_email: appContext.state.user.email
  //   };

  //   appContext.state.user.id &&
  //     axios({ method: "POST", url, headers, data })
  //       .then(response => {
  //         if (response) {
  //           toast("Success! Thank you for purchasing an experience!", {
  //             type: "success"
  //           });
  //         }
  //       })
  //       .catch(error => {
  //         console.log(error);
  //         toast("Error! Something went wrong!", { type: "error" });
  //       });
  // };

  // Contact modal
  const contactLink = (
    <div>
      <Modal
        isOpen={contactOpened}
        onRequestClose={closeModal('contact')}
        ariaHideApp={false}
        className="contact-modal"
      >
        <div className="form-group">
          <span onClick={closeModal('contact')} className="fas fa-times fa-2x close-modal"></span>
        </div>

        <div className="modal-title">{`Contact Information on ${props.title}`}</div>

        <div>
          Phone:{' '}
          <a href={`tel:${props.phoneNumber}`} className="external-link">
            {props.phoneNumber}
          </a>
        </div>
        <div>
          Email:{' '}
          <a href={`mailto:${props.email}`} className="external-link">
            {props.email}
          </a>
        </div>
      </Modal>

      <div onClick={openModal('contact')} className="experience-details-contact-btn">
        Contact
      </div>
    </div>
  );

  // Render explore card
  const {
    publisher,
    image,
    title,
    capacity,
    price,
    startDate,
    endDate,
    address,
    city,
    provinceTerritory,
    postalCode,
    description,
  } = props;

  // const buyLinks = this.context.state.signedInStatus ? (
  //   <StripeCheckout
  //     stripeKey={process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY}
  //     token={this.handleToken}
  //     label="Buy"
  //     amount={price * 100}
  //     currency="CAD"
  //     description={title}
  //     email={appContext.state.user.email}
  //   />
  // ) : null;

  return (
    <div className="col-xl-4 col-md-6 experience-card">
      <LazyLoad once>
        {/* Experiences Details Modal */}
        <Modal
          isOpen={experienceDetailsOpened}
          onRequestClose={closeModal('experience-details')}
          ariaHideApp={false}
          className="experience-details-modal"
        >
          <div className="form-group">
            <span
              onClick={closeModal('experience-details')}
              className="fas fa-times fa-2x close-modal"
            ></span>
          </div>

          <div className="experience-details-title">{title}</div>

          <div className="form-group">
            <img
              src={image}
              alt={title}
              onLoad={() => setLoad(true)}
              className={load ? 'experience-image' : 'loading-image'}
            />
          </div>

          <div className="text-left">
            <div className="form-group">
              <div>{`Price: $${price}`}</div>
              <div>{`Capacity: ${capacity}`}</div>
            </div>
            <div>{`Address: ${address}, ${city}, ${provinceTerritory} ${postalCode}`}</div>
            <div>{`Start Date: ${formatDate(startDate)}`}</div>
            <div>{`End Date: ${formatDate(endDate)}`}</div>
            <div className="experience-description">{description}</div>
          </div>

          {appContext.state.user.id !== parseInt(publisher) ? <div>{contactLink}</div> : null}

          <div className="close-modal-bottom-content">
            <span onClick={closeModal('experience-details')} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

        <div
          onClick={openModal('experience-details')}
          className="experience-card-content text-center"
        >
          <img
            src={image}
            alt={title}
            onLoad={() => setLoad(true)}
            className={load ? 'experience-image' : 'loading-image'}
          />
          <div className="experience-title">{title}</div>
          <div className="experience-price">{`$${price}`}</div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default ExperienceCard;
