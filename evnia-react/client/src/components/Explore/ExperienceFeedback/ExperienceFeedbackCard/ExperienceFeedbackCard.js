// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import Modal from 'react-modal';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Styling
import './ExperienceFeedbackCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class ExperienceFeedbackCard extends Component {
  // Experience Feedback card constructor
  constructor(props) {
    super(props);

    this.state = {
      reportOpened: false,
      report: '',
      submitAuthDisabled: false,
    };
  }

  // To use the JWT credentials
  static contextType = AppContext;

  // Date formatting helper function
  formatDate = (event) => {
    const options = { month: 'short', day: '2-digit', year: 'numeric' };
    return new Date(event).toLocaleDateString([], options);
  };

  // Open modal type helper function
  openModal = (modalType) => () => {
    if (modalType === 'report') {
      this.setState({ reportOpened: true });
    }
  };

  // Close modal type helper function
  closeModal = (modalType) => () => {
    if (modalType === 'report') {
      this.setState({ reportOpened: false });
    }
  };

  // User input change helper function
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate user input for reporting feedback helper function
  validateReport = () => {
    let reportError = '';

    // Render report error message
    if (!this.state.report) {
      reportError = 'Description is required.';
    }

    // Set validation error state
    if (reportError) {
      this.setState({ reportError });
      return false;
    }

    return true;
  };

  // Flag Experience feedback helper function
  handleSubmitFlagFeedback = async (event) => {
    event.preventDefault();

    const isValid = this.validateReport();

    if (isValid) {
      const url = '/flagged-feedbacks';

      const acc_token = localStorage.getItem('access_token');

      const headers = {
        Authorization: `Bearer ${acc_token}`,
      };

      const data = {
        Experience_id: this.props.currentExperienceId.toString(),
        feedback_id: this.props.id.toString(),
        flagged_profile_img_url: this.context.state.user.profile_img_url,
        flagged_first_name: this.context.state.user.first_name,
        flagged_body: this.state.report,
        feedback_body: this.props.body,
        feedback_profile_img_url: this.props.profileImage,
        feedback_first_name: this.props.firstName,
      };

      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for reporting ${this.props.firstName}'s feedback!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            reportError: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Experience Feedback card
  render = () => {
    const { reportOpened, report, reportError, submitAuthDisabled } = this.state;

    const {
      currentExperienceId,
      feedbackExperienceId,
      body,
      profileImage,
      firstName,
      createdAt,
    } = this.props;

    return (
      <div>
        {this.context.state.signedInStatus &&
        currentExperienceId.toString() === feedbackExperienceId ? (
          <div className="experience-feedback-card">
            <div>{`"${body}"`}</div>

            <div className="row experience-feedback-card-user">
              {profileImage ? (
                <img
                  src={profileImage}
                  alt={firstName}
                  className="experience-feedback-card-user-picture"
                />
              ) : (
                <span className="fas fa-user-circle fa-3x experience-feedback-card-default-picture"></span>
              )}
              <span>
                <div className="experience-feedback-card-user-name">{firstName}</div>
                <div className="experience-feedback-card-user-date">{`${this.formatDate(
                  createdAt,
                )}`}</div>
              </span>
            </div>

            <div>
              {/* Report Modal */}
              <Modal
                isOpen={reportOpened}
                onRequestClose={this.closeModal('report')}
                ariaHideApp={false}
                className="report-feedback-modal"
              >
                <div className="form-group">
                  <span
                    onClick={this.closeModal('report')}
                    className="fas fa-times fa-2x close-modal"
                  ></span>
                </div>

                <div className="modal-title">Report this Feedback</div>

                <form onSubmit={this.handleSubmitFlagFeedback} noValidate>
                  <div className="form-group">
                    <textarea
                      name="report"
                      placeholder="Why are your reporting this feedback?"
                      value={report}
                      onChange={this.handleChange}
                      disabled={submitAuthDisabled}
                      className="report-feedback-input"
                      required
                    />
                    <span className="fas fa-pencil-alt report-feedback-input-icon"></span>
                    {reportError ? <div className="error-message">{reportError}</div> : null}
                  </div>

                  <div>
                    <button type="submit" disabled={submitAuthDisabled} className="modal-btn">
                      Report
                    </button>
                  </div>
                </form>
              </Modal>

              <div onClick={this.openModal('report')} className="experience-feedback-card-flag">
                <span className="fas fa-flag"></span>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  };
}
