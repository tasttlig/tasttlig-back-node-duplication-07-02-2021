// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import ExperienceFeedbackCard from './ExperienceFeedbackCard/ExperienceFeedbackCard';

// Styling
import './ExperienceFeedback.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class ExperienceFeedback extends Component {
  // Experience Feedback constructor
  constructor(props) {
    super(props);

    this.state = {
      ExperienceId: this.props.ExperienceId,
      ExperienceFeedback: '',
      submitAuthDisabled: false,
      ExperienceFeedbackItems: [],
      numberOfFeedbacks: 0,
    };
  }

  // To use the JWT credentials
  static contextType = AppContext;

  // User input change helper function
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate user input for Experience Feedback helper function
  validateExperienceFeedback = () => {
    let ExperienceFeedbackError = '';

    // Render Experience feedback error message
    if (!this.state.ExperienceFeedback) {
      ExperienceFeedbackError = 'Description is required.';
    }

    // Set validation error state
    if (ExperienceFeedbackError) {
      this.setState({ ExperienceFeedbackError });
      return false;
    }

    return true;
  };

  // Submit Experience Feedback form helper function
  handleSubmitExperienceFeedback = async (event) => {
    event.preventDefault();

    const isValid = this.validateExperienceFeedback();

    if (isValid) {
      const urlFeedback = '/feedbacks';

      const urlExperience = `/api/experiences/feedback-count/${this.state.experienceId}`;

      const acc_token = localStorage.getItem('access_token');

      const headers = {
        Authorization: `Bearer ${acc_token}`,
      };

      const dataFeedback = {
        Experience_id: this.state.ExperienceId.toString(),
        body: this.state.ExperienceFeedback,
        profile_img_url: this.context.state.user.profile_img_url,
        first_name: this.context.state.user.first_name,
      };

      const dataExperience = {
        feedback_count: this.state.numberOfFeedbacks + 1,
      };

      try {
        const responseFeedback = await axios({
          method: 'POST',
          url: urlFeedback,
          headers,
          data: dataFeedback,
        });

        const responseExperience = await axios({
          method: 'PUT',
          url: urlExperience,
          data: dataExperience,
        });

        if (responseFeedback && responseExperience) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for submitting your feedback on ${this.props.title}!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            ExperienceFeedbackError: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Experience Feedback card helper function
  renderExperienceFeedbackCard = (arr) => {
    return arr.map((card, index) => (
      <ExperienceFeedbackCard
        key={index}
        id={card.id}
        currentExperienceId={this.state.ExperienceId}
        feedbackExperienceId={card.Experience_id}
        profileImage={card.profile_img_url}
        firstName={card.first_name}
        body={card.body}
        createdAt={card.created_at}
      />
    ));
  };

  // Mount All Experience Feedback
  componentDidMount = () => {
    const url = '/feedbacks';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          ExperienceFeedbackItems: [
            ...this.state.ExperienceFeedbackItems,
            ...response.data.feedbacks,
          ],
        });
      })
      .then(() => {
        this.setState({
          ExperienceFeedbackItems: this.state.ExperienceFeedbackItems.reverse(),
        });
      })
      .then(() => {
        this.state.ExperienceFeedbackItems.map((item) => {
          if (item.Experience_id === this.state.ExperienceId.toString()) {
            this.setState({
              numberOfFeedbacks: this.state.numberOfFeedbacks + 1,
            });
          }
          return null;
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render Experience Feedback component on Experience Details page
  render = () => {
    const {
      ExperienceFeedback,
      ExperienceFeedbackError,
      submitAuthDisabled,
      ExperienceFeedbackItems,
      numberOfFeedbacks,
    } = this.state;

    const { publisher, title } = this.props;

    return (
      <div>
        {this.context.state.signedInStatus &&
        this.context.state.user.verified &&
        parseInt(publisher) !== this.context.state.user.id ? (
          <div className="experience-feedback">
            <div className="form-group experience-feedback-title">Write a Feedback</div>

            <form onSubmit={this.handleSubmitExperienceFeedback} noValidate>
              <div className="form-group">
                <textarea
                  name="ExperienceFeedback"
                  placeholder={`Write a feedback on ${title}`}
                  value={ExperienceFeedback}
                  onChange={this.handleChange}
                  disabled={submitAuthDisabled}
                  className="experience-feedback-input"
                  required
                />
                <span className="fas fa-pencil-alt experience-feedback-input-icon"></span>
                {ExperienceFeedbackError ? (
                  <div className="error-message">{ExperienceFeedbackError}</div>
                ) : null}
              </div>

              <div>
                <button
                  type="submit"
                  disabled={submitAuthDisabled}
                  className="experience-feedback-submit-btn"
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        ) : null}

        {numberOfFeedbacks !== 0 ? (
          <div className="experience-feedback-quantity">
            {numberOfFeedbacks} Feedback
            {numberOfFeedbacks !== 1 ? <span>s</span> : null}
          </div>
        ) : this.context.state.signedInStatus ? (
          <div className="experience-feedback-quantity">No feedback yet.</div>
        ) : null}

        {this.renderExperienceFeedbackCard(ExperienceFeedbackItems)}
      </div>
    );
  };
}
