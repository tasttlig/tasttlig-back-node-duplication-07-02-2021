// Libraries
import React, { Component } from 'react';

// Components
import Nav from '../Navbar/Nav';
import Explore from './Explore';
import Footer from '../Home/Footer/Footer';

// Styling
import './Explore.scss';

export default class ExplorePage extends Component {
  // Render explore page
  render = () => {
    return (
      <div>
        <Nav />
        <Explore history={this.props.history} />
        <Footer />
      </div>
    );
  };
}
