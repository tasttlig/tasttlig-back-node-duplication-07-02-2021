// Libraries
import React, { Component } from 'react';

// Components
import Nav from '../Navbar/Nav';
import MiniHostForm from './ApplyForm/MiniHostForm';

// Styling
import './Apply.scss';

export default class Apply extends Component {
  // Set initial state
  constructor(props) {
    super(props);

    // Source tracking on new sign ups
    const source = new URLSearchParams(props.location.search).get('source');
    if (source) {
      if (!localStorage.getItem('source')) {
        localStorage.setItem('source', source);
      }
    }
  }

  // Mount Host page
  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  // Render Host page
  render = () => {
    return (
      <div>
        <div className="become-host-form">
          <Nav />
          <ApplyForm history={this.props.history} />
          {/*<MiniHostForm history={this.props.history}/>*/}
        </div>
      </div>
    );
  };
}
