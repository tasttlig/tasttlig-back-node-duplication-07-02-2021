// Libraries
import React, { useContext, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../../ContextProvider/AuthModalProvider';

// Components
import { Form, Input } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const PersonalInfoForm = (props) => {
  const { nextStep, update, prevStep, values, readMode } = props;

  const [existingUser, setExistingUser] = useState(null);

  const appContext = useContext(AppContext);
  const authContext = useContext(AuthModalContext);

  const checkExistingUser = async (email) => {
    const response = await axios({
      method: 'GET',
      url: `/user/check-email/${email}`,
    });

    return response.data;
  };

  const createNewUser = async (data) => {
    const response = await axios({
      method: 'POST',
      url: '/user/create-new-multi-step-user',
      data: data,
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    const response = await checkExistingUser(data.email);

    if (response.success) {
      setExistingUser(response.user);
    } else {
      update(data);
      await createNewUser(data);
      nextStep();
    }
  };

  const emailDisabled = values.submitAuthDisabled || appContext.state.signedInStatus;

  const UserExists = () => (
    <div>
      <div className="p-t-128 mw-700 px-3 py-3 pb-md-4 mx-auto text-center">
        <p className="lead">
          An account already exists for <strong>{existingUser.email}</strong>.
          <br />
          Please login to access your dashboard.
        </p>

        <Link exact="true" to="/login" className="log-in-btn">
          Login
        </Link>
      </div>
    </div>
  );

  return (
    <>
      {existingUser ? (
        <UserExists />
      ) : (
        <div>
          {/* {!readMode && <Progress />} */}
          <div className={!readMode ? 'apply-to-host' : ''}>
            {!readMode ? (
              <h1 className="apply-to-host-step-name">Personal Information</h1>
            ) : (
              <>
                <h6>Personal Information</h6>
                <hr />
              </>
            )}
            <Form data={values} onSubmit={onSubmit} readMode={readMode}>
              <Input name="first_name" required={true} label="First Name" />
              <Input name="last_name" required={false} label="Last Name" />
              <Input
                name="email"
                type="email"
                required={true}
                label="Email"
                disabled={emailDisabled}
              />
              <Input name="phone_number" type="tel" required={true} label="Phone Number" />

              {!readMode && (
                <div className="apply-to-host-navigation">
                  <span
                    onClick={prevStep}
                    disabled={values.submitAuthDisabled}
                    className="back-btn"
                  >
                    Back
                  </span>
                  <input
                    type="submit"
                    value="Continue"
                    disabled={values.submitAuthDisabled}
                    className="continue-btn"
                  />
                </div>
              )}
            </Form>
          </div>
        </div>
      )}
    </>
  );
};

export default PersonalInfoForm;
