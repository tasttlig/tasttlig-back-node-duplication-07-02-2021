// Libraries
import React from 'react';
// import { Progress } from "react-sweet-progress";

// Components
import { Form, Input, MultiImageInput, Textarea } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const VenuesProductForm = (props) => {
  const { values, prevStep, updateVenue, readMode } = props;

  const onSubmit = (data) => {
    updateVenue(data);
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      {!readMode ? (
        <h1 className="apply-to-host-step-name">Venue Information</h1>
      ) : (
        <>
          <h6>Venue Information</h6>
          <hr />
        </>
      )}

      <Form data={values} onSubmit={onSubmit} readMode={readMode}>
        <Input name="venue_name" label="Venue Name" required />
        <Textarea name="venue_description" label="Venue Description" required />

        <MultiImageInput
          name="venue_photos"
          label="Venue Photos"
          dropbox_label="Click or drag-and-drop to upload one or more images"
          dir="venue-images"
          required
        />

        {!readMode && (
          <div className="apply-to-host-navigation">
            <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
              Back
            </span>
            <button type="submit" disabled={values.submitAuthDisabled} className="continue-btn">
              Continue
            </button>
          </div>
        )}
      </Form>
    </div>
  );
};

export default VenuesProductForm;
