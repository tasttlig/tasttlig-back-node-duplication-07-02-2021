// Libraries
import React, { useContext } from 'react';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import PersonalInfoForm from './PersonalInfoForm';
import BusinessInfoForm from './BusinessInfoForm';
import ServicesForm from './ServicesForm';
import ApplyToHostForm from './ApplyToHostForm';
import PaymentInfoForm from './PaymentInfoForm';
import DocumentForm from './DocumentForm';
import ProductForm from './ProductForm';
import SocialProofForm from './SocialProofForm';
import ResidentialAddressForm from './ResidentialAddressForm';
import ApplyToCookForm from './ApplyToCook';
import AssetsForm from './AssetsForm';
import PromotionsForm from './PromotionsForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const FormReview = (props) => {
  const appContext = useContext(AppContext);

  const { handleSubmitApplication, prevStep, values, readMode } = props;

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode && <h1 className="apply-to-host-step-name">Preview</h1>}

        <div className="mx-auto text-left">
          {!appContext.state.signedInStatus || readMode ? (
            <div className="mb-5">
              <PersonalInfoForm values={values} readMode />
            </div>
          ) : null}

          <div className="mb-5">
            <ResidentialAddressForm values={values} readMode />
          </div>

          <div className="mb-5">
            <BusinessInfoForm values={values} readMode />
          </div>

          {values.has_business &&
          values.business_category !== 'Party Suppliers' &&
          values.business_category !== 'Transportation' ? (
            <div className="mb-5">
              <ProductForm values={values} readMode />
            </div>
          ) : null}

          <div className="mb-5">
            <PromotionsForm values={values} readMode />
          </div>

          {values.service_provider === 'Restaurant' && (
            <div className="mb-5">
              <AssetsForm values={values} readMode />
            </div>
          )}

          {values.business_category !== 'MC' && (
            <div className="mb-5">
              <ServicesForm values={values} readMode />
            </div>
          )}

          <div className="mb-5">
            <ApplyToHostForm values={values} readMode />
          </div>

          <div className="mb-5">
            <ApplyToCookForm values={values} readMode />
          </div>

          <div className="mb-5">
            <PaymentInfoForm values={values} readMode />
          </div>

          {values.business_category === 'Food' || values.business_category === 'Transportation' ? (
            <div className="mb-5">
              <DocumentForm values={values} readMode />
            </div>
          ) : null}

          <div className="mb-5">
            <SocialProofForm values={values} readMode />
          </div>

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <span
                onClick={handleSubmitApplication}
                disabled={values.submitAuthDisabled}
                className="apply-to-host-submit-btn"
              >
                Submit
              </span>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default FormReview;
