// Libraries
import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

// Components
import MenuItemsForm from './MenuItemsForm';
import EntertainersProductForm from './EntertainersProductForm';
import VenuesProductForm from './VenuesProductForm';

const ProductForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;

  const updateMenuItem = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateMenuItem',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const updateEntertainersProduct = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateEntertainersProduct',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const updateVenuesProduct = async (venue_name, venue_description, venue_photos) => {
    let data = {
      venue_name,
      venue_description,
      venue_photos,
    };

    const response = await axios({
      method: 'PUT',
      url: '/user/updateVenuesProduct',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const updateMenuList = (menuList) => {
    update({
      menu_list: menuList,
    });
  };

  const updateSampleLinks = async (sampleLinks) => {
    update({
      sample_links: sampleLinks,
    });
    await updateEntertainersProduct(sampleLinks);
    nextStep();
  };

  const updateVenue = async ({ venue_name, venue_description, venue_photos }) => {
    update({
      venue_name: venue_name,
      venue_description: venue_description,
      venue_photos: venue_photos,
    });
    await updateVenuesProduct(venue_name, venue_description, venue_photos);
    nextStep();
  };

  const submit = async () => {
    if (values.business_category === 'Food' && !values.menu_list.length) {
      toast('Please add at least one menu item.', {
        type: 'error',
        autoClose: 2000,
      });

      return;
    }

    await updateMenuItem(values.menu_list);
    nextStep();
  };

  return (
    <div>
      <div className={!readMode ? 'product-items-form' : ''}>
        {values.business_category === 'Food' && (
          <>
            <MenuItemsForm values={values} updateMenuList={updateMenuList} readMode={readMode} />
            {!readMode && (
              <div className="apply-to-host-navigation">
                <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                  Back
                </span>
                <span
                  onClick={submit}
                  disabled={values.submitAuthDisabled}
                  className="continue-btn"
                >
                  Continue
                </span>
              </div>
            )}
          </>
        )}

        {(values.business_category === 'Entertainment' || values.business_category === 'MC') && (
          <EntertainersProductForm
            values={values}
            prevStep={prevStep}
            updateSampleLinks={updateSampleLinks}
            readMode={readMode}
          />
        )}

        {values.business_category === 'Venues' && (
          <VenuesProductForm
            values={values}
            prevStep={prevStep}
            updateVenue={updateVenue}
            readMode={readMode}
          />
        )}
      </div>
    </div>
  );
};

export default ProductForm;
