// Libraries
import React, { useContext, useRef, useState } from 'react';
import axios from 'axios';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { Checkbox, Form, Input, Select } from '../../../EasyForm';
import { displayNationalitiesInSelect, nationalities } from '../../nationality';
import { canadaProvincesTerritories } from '../../../Functions/Functions';

// Styling
// import "react-sweet-progress/lib/style.css";

const BusinessInfoForm = (props) => {
  const context = useContext(AppContext);
  const formRef = useRef(null);

  const { nextStep, prevStep, update, values, readMode } = props;

  const [hasBusiness, setHasBusiness] = useState(values.has_business === 'yes');
  const [businessCategory, setBusinessCategory] = useState(values.business_category || '');
  const [serviceProvider, setServiceProvider] = useState(values.service_provider || '');

  const updateBusinessInfo = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/update-business-info',
      data: { ...data, email: values.email },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    update(data);

    if (hasBusiness) {
      await updateBusinessInfo(data);
    }

    nextStep();
  };

  const updateAddressInfo = (e) => {
    if (e.target.checked) {
      const currentValues = formRef.current.getValues();

      currentValues.address_line_1 = values.residential_address_line_1;
      currentValues.address_line_2 = values.residential_address_line_2;
      currentValues.business_city = values.residential_city;
      currentValues.state = values.residential_state;
      currentValues.postal_code = values.residential_postal_code;
      currentValues.country = values.residential_country;

      update(currentValues);
    }
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Business Information</h1>
        ) : (
          <>
            <h6>Business Information</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode} formRef={formRef}>
          <Select
            name="has_business"
            label="Do you have a business?"
            onChange={(e) => setHasBusiness(e.target.value === 'yes')}
            required
          >
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </Select>

          {hasBusiness && (
            <>
              <Select
                name="business_category"
                label="Business Type"
                onChange={(e) => setBusinessCategory(e.target.value)}
                required
              >
                <option value="">--Select--</option>
                <option value="Food">Food</option>
                <option value="Entertainment">Entertainment</option>
                <option value="Venues">Venues</option>
                <option value="Transportation">Transportation</option>
                <option value="Party Suppliers">Party Suppliers</option>
                <option value="MC">MC</option>
              </Select>

              <Select
                name="service_provider"
                label="Type of Service Provider"
                onChange={(e) => setServiceProvider(e.target.value)}
                required
              >
                <option value="">--Select--</option>
                {businessCategory === 'Food' && (
                  <>
                    <option value="Chef">Chef</option>
                    <option value="Caterer">Caterer</option>
                    <option value="Restaurant">Restaurant</option>
                    <option value="Food Truck">Food Truck</option>
                  </>
                )}

                {businessCategory === 'Entertainment' && (
                  <>
                    <option value="DJ">DJ</option>
                    <option value="Musician">Musician</option>
                    <option value="Dancer">Dancer</option>
                    <option value="Clown">Clown</option>
                  </>
                )}

                {businessCategory === 'Venues' && (
                  <>
                    <option value="Restaurant">Restaurant</option>
                    <option value="Banquet Hall">Banquet Hall</option>
                  </>
                )}

                {businessCategory === 'Transportation' && (
                  <>
                    <option value="Taxi">Taxi</option>
                    <option value="Limo">Limo</option>
                    <option value="Bus">Bus</option>
                  </>
                )}

                {businessCategory === 'Party Suppliers' && (
                  <>
                    <option value="Balloon Shop">Balloon Shop</option>
                    <option value="Decoration Shop">Decoration Shop</option>
                  </>
                )}

                {businessCategory === 'MC' && (
                  <>
                    <option value="Hosting">Hosting</option>
                  </>
                )}
              </Select>

              <Input name="business_name" label="Business Name" required />

              {serviceProvider === 'Restaurant' && (
                <Select name="culture" label="How do you identify your restaurant?" required>
                  <option value="">--Select--</option>
                  {displayNationalitiesInSelect()}
                </Select>
              )}

              {values.business_category === 'Transportation' && (
                <Input
                  name="transportation_rate"
                  label="Rate (charge per km)"
                  required
                  type="number"
                  step=".01"
                />
              )}

              {!context.state.signedInStatus && (
                <Checkbox name="use_residential" onClick={updateAddressInfo}>
                  Business address is same as residential address
                </Checkbox>
              )}

              <Input name="address_line_1" label="Business Street Address" required />
              <Input name="address_line_2" label="Business Unit Address (Optional)" />
              <Input name="business_city" label="Business City" required />
              <Select name="state" label="Business Province or Territory" required>
                {canadaProvincesTerritories()}
              </Select>
              <Input name="postal_code" label="Business Postal Code" maxLength="7" required />
              <Select
                name="country"
                required
                options={nationalities.map((n) => [n.en_short_name, n.en_short_name])}
              >
                Business Country
              </Select>
              <Input name="registration_number" label="Business Registration Number (Optional)" />
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default BusinessInfoForm;
