// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import DatePicker from 'react-datepicker';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class MiniHostForm extends Component {
  state = {
    foodHandlerCertificate: [],
    dateOfIssue: '',
    expiryDate: '',
    is_participating_in_festival: false,
    submitAuthDisabled: false,
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // Handle input change helper function
  handleChange = (input) => (event) => {
    if (input === 'is_participating_in_festival') {
      this.setState({
        is_participating_in_festival: !this.state.is_participating_in_festival,
      });
    } else {
      this.setState({ [input]: event.target.value });
    }
  };

  // Handle file upload helper function
  handleFileUpload = async (event) => {
    if (this.state.foodHandlerCertificate.length < 1) {
      let file = event.target.files[0];
      let dir_name = 'food-handler-certificates';
      let fileParts = file.name.split('.');
      let fileName = `${dir_name}/${uuidv4()} ${fileParts[0]}.${fileParts[1]}`;
      let fileType = fileParts[1];

      axios
        .post('/s3_signed_url', { fileName, fileType })
        .then((response) => {
          let signedRequest = response.data.signedRequest;
          let url = response.data.url;
          let options = {
            headers: {
              'Content-Type': fileType,
            },
          };

          axios
            .put(signedRequest, file, options)
            .then(() => {
              this.setState({
                foodHandlerCertificate: [...this.state.foodHandlerCertificate, url],
              });
            })
            .catch((error) => {
              console.log(`ERROR ${JSON.stringify(error)}`);
            });
        })
        .catch((error) => {
          console.log(JSON.stringify(error));
        });
    }
  };

  // Validate user input helper function
  validateFields = () => {
    let foodHandlerCertificateError = '';
    let dateOfIssueError = '';
    let expiryDateError = '';

    // Move to the top of the page
    window.scrollTo(0, 0);

    // Render Food Handler Certificate error message
    if (!this.state.foodHandlerCertificate) {
      foodHandlerCertificateError = 'Food Handler Certificate is required.';
    }

    // Render Food Handler Certificate Date of Issue error message
    if (!this.state.dateOfIssue) {
      dateOfIssueError = 'Select date of issue.';
    } else if (this.state.dateOfIssue > this.state.expiryDate) {
      dateOfIssueError = 'Date of issue is not valid.';
    }

    // Render Food Handler Certificate License Expiry Date error message
    if (!this.state.expiryDate) {
      expiryDateError = 'Select expiry date.';
    }

    if (foodHandlerCertificateError || dateOfIssueError || expiryDateError) {
      this.setState({
        foodHandlerCertificateError,
        dateOfIssueError,
        expiryDateError,
      });
      return false;
    }

    return true;
  };

  // Submit Apply to Host form helper function
  handleSubmitApplication = async (event) => {
    event.preventDefault();

    if (this.validateFields()) {
      const url = '/user/upgrade';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      const data = {
        upgrade_type: 'RESTAURANT',
        document_type: 'FOOD_HANDLER_CERTIFICATE',
        document_link: this.state.foodHandlerCertificate[0],
        issue_date: this.state.dateOfIssue,
        expiry_date: this.state.expiryDate,
        is_participating_in_festival: this.state.is_participating_in_festival,
      };

      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response) {
          setTimeout(() => {
            window.location.href = '/complete-profile';
          }, 2000);

          toast(`Success! Thank you for submitting your application for Upgrade a Host!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            foodHandlerCertificateError: '',
            dateOfIssueError: '',
            expiryDateError: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Host page
  render = () => {
    const {
      foodHandlerCertificate,
      dateOfIssue,
      expiryDate,
      is_participating_in_festival,
      foodHandlerCertificateError,
      dateOfIssueError,
      expiryDateError,
      submitAuthDisabled,
    } = this.state;

    return (
      <div className="apply-to-host">
        <h1 className="apply-to-host-title">Host Application</h1>
        <form className="mx-auto text-left" noValidate>
          <div className="form-group">
            <div className="input-title">
              Food Handler Certificate* (only .pdf file is accepted)
            </div>
            <div>
              <label htmlFor="food-handler-certificate" className="file-upload">
                Upload the Document
              </label>
              <input
                id="food-handler-certificate"
                type="file"
                accept="application/pdf"
                onChange={this.handleFileUpload}
                disabled={submitAuthDisabled}
                className="custom-file-input"
                required
              />
              {foodHandlerCertificateError && (
                <div className="error-message">{foodHandlerCertificateError}</div>
              )}
            </div>
            {foodHandlerCertificate[0] && (
              <div>
                <div className="input-title">File Upload Successful</div>
                <div className="row mt-3 mb-5">
                  <div className="col-md-6 date-of-issue-section">
                    <div className="input-title">Date of Issue*</div>
                    <DatePicker
                      dateFormat="yyyy/MM/dd"
                      selected={dateOfIssue}
                      onChange={(date) => this.setState({ dateOfIssue: date })}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      disabled={submitAuthDisabled}
                      className="date-of-issue"
                      required
                    />
                    {dateOfIssueError && <div className="error-message">{dateOfIssueError}</div>}
                  </div>
                  <div className="col-md-6 expiry-date-section">
                    <div className="input-title">Expiry Date*</div>
                    <DatePicker
                      dateFormat="yyyy/MM/dd"
                      selected={expiryDate}
                      onChange={(date) => this.setState({ expiryDate: date })}
                      minDate={new Date()}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      disabled={submitAuthDisabled}
                      className="expiry-date"
                      required
                    />
                    {expiryDateError && <div className="error-message">{expiryDateError}</div>}
                  </div>
                </div>
                <div className="row pt-3">
                  <label>
                    <input
                      type="checkbox"
                      defaultChecked={is_participating_in_festival}
                      onChange={this.handleChange('is_participating_in_festival')}
                      disabled={submitAuthDisabled}
                    />
                    &nbsp;&nbsp;I agree to participate in Tasttlig Festival
                  </label>
                </div>
                <div className="mt-3">
                  <button
                    onClick={this.handleSubmitApplication}
                    disabled={submitAuthDisabled}
                    className="btn btn-primary"
                  >
                    Submit
                  </button>
                </div>
              </div>
            )}
          </div>
        </form>
      </div>
    );
  };
}
