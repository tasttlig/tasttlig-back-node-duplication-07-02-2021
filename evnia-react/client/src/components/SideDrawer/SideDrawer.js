// Libraries
import React, { useState, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Styling
import './SideDrawer.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const SideDrawer = (props) => {
  // Set initial state
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  let isHost = false;

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;

  if (userRole && userRole.includes('RESTAURANT')) {
    isHost = true;
  }

  // Host festival helper function
  const handleHostFestival = async (event) => {
    event.preventDefault();

    const url = '/host-festival';

    const acc_token = localStorage.getItem('access_token');

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      festival_id: props.hostFestivalList,
      festival_restaurant_host_id: appContext.state.user.id,
    };

    localStorage.setItem('festival_id', JSON.stringify(props.hostFestivalList));

    try {
      const response = await axios({ method: 'POST', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for hosting!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  let drawerClasses = 'main-side-drawer';

  if (props.sideDrawerOpen) {
    drawerClasses = 'main-side-drawer open';
  }

  const sideDrawerLinks = appContext.state.signedInStatus ? (
    <div>
      <li>
        <Link
          exact="true"
          to={`/passport/${appContext.state.user.id}`}
          className="main-side-drawer-item"
        >
          Passport
        </Link>
      </li>
      {/* {userRole && userRole.includes('ADMIN') && (
        <li>
          <Link exact="true" to="/admin" className="main-side-drawer-item">
            Admin
          </Link>
        </li>
      )} */}
      <li>
        <Link exact="true" to="/dashboard" className="main-side-drawer-item">
          Dashboard
        </Link>
      </li>
      {/* <li>
        <Link exact="true" to="/ticket" className="main-side-drawer-item">
          Ticket
        </Link>
      </li> */}
      <li>
        <span onClick={authModalContext.handleSignOut} className="main-side-drawer-item">
          Logout
        </span>
      </li>
    </div>
  ) : (
    <div>
      <li>
        <Link exact="true" to="/login" className="main-side-drawer-item">
          Login
        </Link>
        {/* <span
          onClick={authModalContext.openModal("log-in")}
          className="main-side-drawer-item"
        >
          Login
        </span> */}
      </li>
      <li>
        <Link exact="true" to="/sign-up" className="main-side-drawer-item">
          Join
        </Link>
        {/* <span
          onClick={authModalContext.openModal("sign-up")}
          className="main-side-drawer-item"
        >
          Sign Up
        </span> */}
      </li>
    </div>
  );

  // Render Side Drawer
  return (
    <div>
      <nav className={drawerClasses}>
        <ul>
          {/* <li>
            <Link
              exact="true"
              to="/festival"
              className="main-side-drawer-item"
            >
              Festival
            </Link>
          </li> */}
          {/* <li>
            <Link
              exact="true"
              to="/experiences"
              className="main-side-drawer-item"
            >
              Experiences
            </Link>
          </li> */}
          {/* <li>
            <Link
              exact="true"
              to="/passports"
              className="main-side-drawer-item"
            >
              Passports
            </Link>
          </li> */}

          {/* <li>
            <Link exact="true" to="/about" className="main-side-drawer-item">
              About
            </Link>
          </li> */}

          {/* {appContext.state.signedInStatus &&
          window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/` &&
          window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/` &&
          window.location.href !==
            `${process.env.REACT_APP_DEV_BASE_URL}/` ? null : (
            <>
              {!props.selectMultipleFestivals && appContext.state.user.id ? (
                <li>
                  <div
                    onClick={props.handleHostMultipleFestivals}
                    className="main-side-drawer-item"
                  >
                    Host
                  </div>
                </li>
              ) : props.selectMultipleFestivals &&
                props.hostFestivalList.length === 0 &&
                appContext.state.user.id ? (
                <li>
                  <div className="main-side-drawer-item-hosting">Hosting</div>
                </li>
              ) : props.selectMultipleFestivals &&
                props.hostFestivalList.length > 0 &&
                userRole &&
                !userRole.includes("RESTAURANT") &&
                !userRole.includes("RESTAURANT_PENDING") &&
                !userRole.includes("ADMIN") &&
                appContext.state.user.id ? (
                <li>
                  <Link
                    exact="true"
                    to="/complete-profile/business"
                    className="main-side-drawer-item"
                  >
                    Host
                  </Link>
                </li>
              ) : props.selectMultipleFestivals &&
                props.hostFestivalList.length > 0 &&
                userRole &&
                (userRole.includes("RESTAURANT") ||
                  userRole.includes("RESTAURANT_PENDING") ||
                  userRole.includes("ADMIN")) &&
                appContext.state.user.id ? (
                <li>
                  <div
                    onClick={handleHostFestival}
                    disabled={submitAuthDisabled}
                    className="main-side-drawer-item"
                  >
                    Host
                  </div>
                </li>
              ) : null}
            </>
          )} */}

          {userRole &&
          (userRole.includes('RESTAURANT') || userRole.includes('RESTAURANT_PENDING')) ? (
            <li>
              <Link exact="true" to="/plans/business" className="main-side-drawer-item">
                Upgrade Your Membership
              </Link>
            </li>
          ) : null}

          {/* {!appContext.state.user.id ||
          (userRole &&
            !userRole.includes("SPONSOR") &&
            !userRole.includes("SPONSOR_PENDING")) ? (
            <li>
              <Link
                exact="true"
                to="/sponsor"
                className="main-side-drawer-item"
              >
                Sponsor
              </Link>
            </li>
          ) : null} */}

          {/* {!appContext.state.user.id ||
          (userRole &&
            !userRole.includes("RESTAURANT") &&
            !userRole.includes("RESTAURANT_PENDING")) ? (
            <li>
              <Link exact="true" to="/host" className="main-side-drawer-item">
                Host
              </Link>
            </li>
          ) : null} */}

          {/* {!appContext.state.user.id &&
          (window.location.href ===
            `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
            window.location.href ===
              `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
            window.location.href ===
              `${process.env.REACT_APP_DEV_BASE_URL}/festival`) ? (
            <li>
              <span
                onClick={authModalContext.openModal("log-in")}
                className="main-side-drawer-item"
              >
                Add Food Samples
              </span>
            </li>
          ) : userRole &&
            (userRole.includes("RESTAURANT") ||
              userRole.includes("RESTAURANT_PENDING")) &&
            (window.location.href ===
              `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
              window.location.href ===
                `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
              window.location.href ===
                `${process.env.REACT_APP_DEV_BASE_URL}/festival`) ? (
            <li>
              <Link
                exact="true"
                to="/create-food-samples"
                className="main-side-drawer-item"
              >
                Add Food Samples
              </Link>
            </li>
          ) : null} */}

          {/* {window.location.href !==
            `${process.env.REACT_APP_PROD_BASE_URL}/festival` &&
            window.location.href !==
              `${process.env.REACT_APP_TEST_BASE_URL}/festival` &&
            window.location.href !==
              `${process.env.REACT_APP_DEV_BASE_URL}/festival` && (
              <li>
                <Link exact="true" to="/host" className="main-side-drawer-item">
                  Host
                </Link>
              </li>
            )} */}

          {sideDrawerLinks}
        </ul>
      </nav>
    </div>
  );
};

export default SideDrawer;
