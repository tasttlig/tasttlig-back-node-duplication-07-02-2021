// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Modal from 'react-modal';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { toast } from 'react-toastify';
import * as shoppingCartActions from '../../redux/shoppingCart/actions';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Components
import Nav from '../Navbar/Nav';
import ImageSlider from '../ImageSlider/ImageSlider';
import BannerFooter from '../Home/BannerFooter/BannerFooter';
import GoToTop from '../Shared/GoTop';
import { formatDate, formatMilitaryToStandardTime } from '../Functions/Functions';

// Styling
import './ExperienceDetails.scss';
import 'react-toastify/dist/ReactToastify.css';
import food from '../../assets/images/food.png';
import games from '../../assets/images/games.png';
import fun from '../../assets/images/fun.png';
import special from '../../assets/images/special.png';
import transportation from '../../assets/images/transportation.png';
import parking from '../../assets/images/parking.png';
import accessibility from '../../assets/images/accessibility.png';
import environment from '../../assets/images/environment.png';
import value from '../../assets/images/value.png';
import other from '../../assets/images/other.png';

toast.configure();

const ExperienceDetails = (props) => {
  // Set initial state
  const [experienceDetails, setExperienceDetails] = useState([]);
  const [experienceId, setExperienceId] = useState();
  const [contactOpened, setContactOpened] = useState(false);
  const [, setCopied] = useState(false);
  const [itemInCart, setItemInCart] = useState(false);
  const [reduxItem, setReduxItem] = useState(false);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;

  // Link copied to clipboard helper function
  const handleCopyToClipboard = () => {
    toast('Link copied to clipboard!', {
      type: 'success',
      autoClose: 2000,
    });
  };

  // Shopping cart state management
  let cartItemFound = false;
  props.cartItems.map((item) => {
    if (item && item.itemType === 'experiences' && item.itemId === experienceId) {
      cartItemFound = true;
      if (!itemInCart) {
        setItemInCart(true);
      }
      if (reduxItem !== item) {
        setReduxItem(item);
      }
    }
  });
  if (!cartItemFound && itemInCart) {
    setItemInCart(false);
  }

  // Mount Experience Details page
  useEffect(() => {
    window.scrollTo(0, 0);

    const fetchExperienceDetails = async () => {
      try {
        const { data } = await axios({
          method: 'GET',
          url: `/experience/${props.match.params.id}`,
        });

        setExperienceDetails(data.details);
        setExperienceId(data.details[0].experience_id);
      } catch (error) {
        return error.response;
      }
    };

    fetchExperienceDetails();
  }, []);

  // Render Experience Details page
  return (
    <div>
      <Nav />

      <div className="experience-details">
        {experienceDetails.map((experienceDetail, index) => (
          <div key={index}>
            <div className="row">
              <div className="col-lg-7 experience-details-images-content">
                <div className="experience-details-images-section">
                  {experienceDetail.image_urls.length > 1 ? (
                    <ImageSlider images={experienceDetail.image_urls} />
                  ) : (
                    <img src={experienceDetail.image_urls[0]} alt={experienceDetail.title} />
                  )}

                  <CopyToClipboard text={window.location.href} onCopy={() => setCopied(true)}>
                    <button onClick={handleCopyToClipboard} className="share-btn">
                      Share&nbsp;<span className="fas fa-share"></span>
                    </button>
                  </CopyToClipboard>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={food} alt="What you will be eating and drinking" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">
                      What you will be eating and drinking
                    </div>
                    <div>{experienceDetail.food_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={games} alt="What games will you be playing" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">
                      What games will you be playing
                    </div>
                    <div>{experienceDetail.game_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={fun} alt="What entertainment is included" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">
                      What entertainment is included
                    </div>
                    <div>{experienceDetail.entertainment_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={special} alt="Special features" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">Special features</div>
                    <div>{experienceDetail.feature_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={transportation} alt="Transportation" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">Transportation</div>
                    <div>{experienceDetail.transport_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={parking} alt="Parking" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">Parking</div>
                    <div>{experienceDetail.parking_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={accessibility} alt="Accessibility" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">Accessibility</div>
                    <div>{experienceDetail.accessibility_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={environment} alt="Environmental considerations" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">
                      Environmental considerations
                    </div>
                    <div>{experienceDetail.environmental_consideration_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={value} alt="Value" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">Value</div>
                    <div>{experienceDetail.value_description}</div>
                  </div>
                </div>

                <div className="row experience-description-content">
                  <div className="col-lg-1 experience-description-icon-content">
                    <img src={other} alt="Other information" />
                  </div>
                  <div className="col-lg-11 experience-description">
                    <div className="experience-description-sub-title">Other information</div>
                    <div>{experienceDetail.other_description}</div>
                  </div>
                </div>
              </div>

              <div className="col-lg-5 experience-details-buy-content">
                <div className="experience-details-buy-section">
                  <div className="row product-details-owner-information">
                    <div className="col-25">
                      {experienceDetail.profile_image_link ? (
                        <img
                          src={experienceDetail.profile_image_link}
                          alt={`${experienceDetail.first_name} ${experienceDetail.last_name}`}
                          className="product-details-owner-profile-picture"
                        />
                      ) : (
                        <span className="fas fa-user-circle fa-3x product-details-owner-default-picture" />
                      )}
                    </div>
                    <div className="col-75">
                      <div className="product-details-title">{experienceDetail.title}</div>
                      {experienceDetail.business_name && (
                        <div className="text-left">
                          <span className="product-details-owner-by">by </span>
                          <Link
                            exact="true"
                            to={`/business/${experienceDetail.business_name}`}
                            className="product-details-owner-name"
                          >
                            {experienceDetail.business_name}
                          </Link>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="mt-3">
                    <div className="pb-2">
                      <div className="experience-details-sub-title">Address</div>
                      <div>{`${experienceDetail.address}, ${experienceDetail.city}, ${experienceDetail.state} ${experienceDetail.postal_code}`}</div>
                    </div>
                    <div className="pb-2">
                      <div className="experience-details-sub-title">Date</div>
                      <div>{`${formatDate(experienceDetail.start_date)} to ${formatDate(
                        experienceDetail.end_date,
                      )}`}</div>
                    </div>
                    <div className="pb-2">
                      <div className="experience-details-sub-title">Time</div>
                      <div>
                        {`${formatMilitaryToStandardTime(
                          experienceDetail.start_time,
                        )} to ${formatMilitaryToStandardTime(experienceDetail.end_time)}`}
                      </div>
                    </div>
                    <div className="pb-4">
                      <div className="experience-details-sub-title">Capacity</div>
                      <div>{experienceDetail.capacity}</div>
                    </div>

                    {(userRole &&
                      (userRole.includes('HOST') ||
                        userRole.includes('HOST_PENDING') ||
                        userRole.includes('ADMIN'))) ||
                    experienceDetail.experience_creator_user_id ===
                      appContext.state.user.id ? null : !appContext.state.user.id ? (
                      <Link exact="true" to="/login" className="buy-now-btn">
                        {`$${experienceDetail.price}`}&nbsp;Buy Now
                      </Link>
                    ) : (
                      <div>
                        <Link
                          exact="true"
                          to={`/payment/experience/${experienceDetail.experience_id}`}
                          className="buy-now-btn"
                        >
                          {`$${experienceDetail.price}`}&nbsp;Buy Now
                        </Link>
                      </div>
                    )}

                    {/* {itemInCart ? (
                      <div className="row">
                        <div className="col-sm-6 row pt-2 pb-2 pl-0 pr-0 text-center">
                          <div className="col p-0">
                            <div
                              className={
                                reduxItem.quantity > 1
                                  ? "border border-dark bg-dark text-white p-2 fas fa-minus"
                                  : "border border-dark bg-light p-2 fas fa-minus"
                              }
                              onClick={() =>
                                reduxItem.quantity > 1
                                  ? props.subtractQuantity(
                                      reduxItem.itemType,
                                      reduxItem.itemId
                                    )
                                  : null
                              }
                            />
                          </div>
                          <div className="col p-1">{reduxItem.quantity}</div>
                          <div className="col p-0">
                            <div
                              className="border border-dark bg-dark text-white p-2 fas fa-plus"
                              onClick={() =>
                                props.addQuantity(
                                  reduxItem.itemType,
                                  reduxItem.itemId
                                )
                              }
                            />
                          </div>
                        </div>
                        <div
                          className="col-sm-6 add-to-cart-btn"
                          onClick={() => props.alternateCartVisibility()}
                        >
                          <span className="fas fa-shopping-basket" />
                          &nbsp;Cart
                        </div>
                      </div>
                    ) : (
                      <div className="row">
                        <div
                          onClick={() => {
                            props.addToCart(
                              experienceDetail.experience_id,
                              experienceDetail.image_urls[0],
                              experienceDetail.title,
                              experienceDetail.price
                            );
                          }}
                          className="col-md-12 add-to-cart-btn"
                        >
                          <span className="fas fa-cart-plus" />
                          &nbsp;Add
                        </div>
                      </div>
                    )} */}

                    <div>
                      <Modal
                        isOpen={contactOpened}
                        onRequestClose={() => setContactOpened(false)}
                        ariaHideApp={false}
                        className="contact-modal"
                      >
                        <div className="text-right">
                          <button
                            aria-label="Close Contact Information Modal"
                            onClick={() => setContactOpened(false)}
                            className="fas fa-times fa-2x close-modal"
                          ></button>
                        </div>

                        <div className="modal-title">
                          {`Contact Information on ${experienceDetail.title}`}
                        </div>

                        <div>
                          Phone:{' '}
                          <a
                            href={`tel:${experienceDetail.phone_number}`}
                            className="external-link"
                          >
                            {experienceDetail.phone_number}
                          </a>
                        </div>
                        <div>
                          Email:{' '}
                          <a href={`mailto:${experienceDetail.email}`} className="external-link">
                            {experienceDetail.email}
                          </a>
                        </div>
                      </Modal>

                      {/* <div
                        onClick={() => setContactOpened(true)}
                        className="experience-details-contact-btn"
                      >
                        Contact
                      </div> */}
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">
                        What you will be eating and drinking
                      </div>
                      <div>{experienceDetail.food_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">
                        What games will you be playing
                      </div>
                      <div>{experienceDetail.game_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">
                        What entertainment is included
                      </div>
                      <div>{experienceDetail.entertainment_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Special features</div>
                      <div>{experienceDetail.feature_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Transportation</div>
                      <div>{experienceDetail.transport_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Parking</div>
                      <div>{experienceDetail.parking_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Accessibility</div>
                      <div>{experienceDetail.accessibility_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">
                        Environmental considerations
                      </div>
                      <div>{experienceDetail.environmental_consideration_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Value</div>
                      <div>{experienceDetail.value_description}</div>
                    </div>

                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Other information</div>
                      <div>{experienceDetail.other_description}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>

      <BannerFooter />

      <GoToTop />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartItems,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (experience_id, imageUrl, title, price) => {
      dispatch(shoppingCartActions.addToCart('experiences', experience_id, imageUrl, title, price));
    },
    addQuantity: (type, id) => {
      dispatch(shoppingCartActions.addQuantity(type, id));
    },
    subtractQuantity: (type, id) => {
      dispatch(shoppingCartActions.subtractQuantity(type, id));
    },
    alternateCartVisibility: () => {
      dispatch(shoppingCartActions.alternateCartVisibility());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExperienceDetails);
