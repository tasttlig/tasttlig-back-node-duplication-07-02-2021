// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import Nav from '../Navbar/Nav';
import ReviewExperienceForm from './ReviewExperienceForm/ReviewExperienceForm';

const ReviewExperience = (props) => {
  // Set initial state
  const [reviewExperience, setReviewExperience] = useState([]);

  // Fetch experience for review helper function
  const fetchReviewExperience = async () => {
    try {
      const experience_id = props.match.params.id;
      const response = await axios({
        method: 'GET',
        url: `/experience/${experience_id}`,
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Mount Review Experience page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchReviewExperience().then(({ data }) => {
      data.details.map((item) => {
        if (parseInt(props.match.params.id) === item.experience_id) {
          setReviewExperience((info) => [...info, item]);
        }
      });
    });
  }, []);

  // Render Review Experience page
  return (
    <div>
      <Nav />

      {reviewExperience.map((experience) => (
        <ReviewExperienceForm
          key={experience.experience_id}
          reviewExperience={experience}
          token={props.match.params.token}
        />
      ))}
    </div>
  );
};

export default ReviewExperience;
