// Libraries
import React, { useState } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

// Components
import { formatMilitaryToStandardTime } from '../../Functions/Functions';

// Styling
import './ReviewExperienceForm.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const ReviewExperienceForm = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [reason, setReason] = useState('');
  const [reasonError, setReasonError] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Validate user input for reason/suggestions helper function
  const validateReviewExperience = () => {
    if (!reason) {
      setReasonError('Reason/suggestions are required.');
    } else {
      setReasonError('');
    }

    if (!reason) {
      return false;
    }

    return true;
  };

  // Accept experience helper function
  const handleSubmitAcceptExperience = async (event) => {
    event.preventDefault();

    const isValid = validateReviewExperience();

    if (isValid) {
      const url = '/experience/review';

      const data = {
        token: props.token,
        id: props.reviewExperience.experience_id,
        experience_update_data: {
          status: 'ACTIVE',
          review_experience_reason: reason,
        },
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);

          toast('Success! Thank you for accepting this experience!', {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Reject experience helper function
  const handleSubmitRejectExperience = async (event) => {
    event.preventDefault();

    const isValid = validateReviewExperience();

    if (isValid) {
      const url = '/experience/review';

      const data = {
        token: props.token,
        id: props.reviewExperience.experience_id,
        experience_update_data: {
          status: 'ARCHIVED',
          review_experience_reason: reason,
        },
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);

          toast('Success! Thank you for rejecting this experience!', {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Destructuring
  const {
    image_urls,
    title,
    address,
    city,
    state,
    postal_code,
    start_time,
    end_time,
    price,
    dress_code,
    capacity,
    style,
    description,
  } = props.reviewExperience;

  return (
    <div className="review-experience">
      <div className="review-experience-title">Review Experience</div>

      <div className="row">
        <div className="col-md-6 review-experience-image-content">
          <img
            src={image_urls[0]}
            alt={title}
            onLoad={() => setLoad(true)}
            className={load ? 'review-experience-image' : 'loading-image'}
          />
        </div>
        <div className="col-md-6 review-experience-text-content">
          <div className="mb-3">
            <span className="review-experience-sub-title">Title: </span>
            <span>{title}</span>
          </div>
          <div className="mb-3">
            <span className="review-experience-sub-title">Address: </span>
            <span>{`${address}, ${city}, ${state} ${postal_code}`}</span>
          </div>
          <div className="mb-3">
            <span className="review-experience-sub-title">Time: </span>
            <span>{`${formatMilitaryToStandardTime(start_time)} to ${formatMilitaryToStandardTime(
              end_time,
            )}`}</span>
          </div>
          <div className="mb-3">
            <span className="review-experience-sub-title">Price: </span>
            <span>{`$${price}`}</span>
          </div>
          <div className="mb-3">
            <span className="review-experience-sub-title">Dress Code: </span>
            <span>{dress_code}</span>
          </div>
          <div className="mb-3">
            <span className="review-experience-sub-title">Capacity: </span>
            <span>{capacity}</span>
          </div>
          <div className="mb-3">
            <span className="review-experience-sub-title">Experience Type: </span>
            <span>{style}</span>
          </div>
          <div className="mb-3">
            <span className="review-experience-sub-title">Description: </span>
            <span>{description}</span>
          </div>
        </div>
      </div>

      <div className="review-experience-reason-suggestions-content">
        <div className="input-title">Reason/Suggestions*</div>
        <textarea
          value={reason}
          onChange={(e) => setReason(e.target.value)}
          disabled={submitAuthDisabled}
          className="review-experience-reason-suggestions"
          required
        />
        {reasonError && <div className="error-message">{reasonError}</div>}
      </div>

      <div className="row">
        <div className="col-md-6 review-experience-accept-content">
          <form onSubmit={handleSubmitAcceptExperience} noValidate>
            <button
              type="submit"
              disabled={submitAuthDisabled}
              className="btn btn-success review-experience-accept-btn"
            >
              Accept
            </button>
          </form>
        </div>
        <div className="col-md-6 review-experience-reject-content">
          <form onSubmit={handleSubmitRejectExperience} noValidate>
            <button
              type="submit"
              disabled={submitAuthDisabled}
              className="btn btn-danger review-experience-reject-btn"
            >
              Reject
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ReviewExperienceForm;
