// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import Nav from '../Navbar/Nav';

const ConfirmFoodSample = (props) => {
  // Set initial state
  const [confirmed, setConfirmed] = useState(false);
  const [error, setError] = useState(null);

  // Mount Confirm Food Sample page
  useEffect(async () => {
    window.scrollTo(0, 0);

    const token = props.match.params.token;
    const response = await axios.post('/food-sample-claim/confirm/', {
      token,
    });

    setConfirmed(response.data.success);
    setError(response.data.error);
  }, []);

  // Render Confirm Food Sample page
  return (
    <div>
      <Nav />

      <div className="passport">
        {confirmed ? (
          <div className="text-center">Food sample claim confirmed successfully.</div>
        ) : null}
        {error ? (
          <div className="text-center">
            An error occurred while attempting to confirm food sample claim. Please try again later.
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default ConfirmFoodSample;
