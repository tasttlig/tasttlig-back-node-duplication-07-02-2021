// Libraries
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import canadianCuisine from '../../../assets/images/canadian-cuisine.jpg';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';

// Styling
import './SponsorProductOrServiceCard.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../assets/images/live.png';

toast.configure();

const SponsorProductOrServiceCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;

  // Set date and time
  /*  const startDateTime = moment(
    new Date(props.startDate).toISOString().split("T")[0] +
    "T" +
    props.startTime +
    ".000Z"
  ).add(new Date().getTimezoneOffset(), "m");
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split("T")[0] +
    "T" +
    props.endTime +
    ".000Z"
  ).add(new Date().getTimezoneOffset(), "m"); */

  // To use the JWT credentials
  let divName;
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  const handleClaimItem = async () => {
    try {
      let url = '';
      let data = {};

      if (props.productId) {
        url = '/claim-product';
        data = {
          product_claim_user: appContext.state.user.id,
          product_id: props.productId,
        };
      } else if (props.serviceId) {
        url = '/claim-service';
        data = {
          service_claim_user: appContext.state.user.id,
          service_id: props.serviceId,
        };
      }

      const response = await axios({ method: 'POST', url, data });
      if (response && response.data && response.data.success) {
        toast(`Item claimed successfully!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  if (!props.classNameFalse) {
    divName = 'restaurant-card';
  } else {
    divName = 'col-md-6 col-xl-4 restaurant-card';
  }
  console.log(userRole);
  // Render Festival Card
  return (
    <div className="restaurant-card">
      <LazyLoad once>
        <div className="d-flex flex-column h-100">
          <div className="restaurant-information">
            <img src={canadianCuisine} className="restaurant-logo" />
            <div>
              <div className="restaurant-name">{props.restaurant_name}</div>
              <div className="restaurant-address">
                <span className="fas fa-map-marker-alt restaurant-address-location-icon" />
                <span>{props.address}</span>
              </div>
            </div>
          </div>
          <div className="restaurant-card-body">
            <div className="restaurant-card-top-section">
              {props.isSponsored ? (
                <div className="restaurant-card-title">
                  {props.productId ? 'Product ' : props.serviceId ? 'Service ' : null}Sponsored by{' '}
                  <strong>{props.title}</strong>
                </div>
              ) : (
                <div className="restaurant-card-title">{props.title}</div>
              )}
              <div className="festival-card-attendance"></div>
            </div>
            <div>
              {props.images.length > 1 ? (
                <ImageSlider images={props.images} isCard={true} />
              ) : (
                <img
                  src={props.images[0]}
                  alt={props.title}
                  onLoad={() => setLoad(true)}
                  className={load ? 'restaurant-card-image' : 'loading-image'}
                />
              )}
              <div className="festival-card-text">
                {/* <div className="festival-card-details">
                  <div className="row">
                    <div className="col-md-8 festival-card-time">
                      {`${moment(props.startTime).format("h:mm a")}`}
                    </div>
                    <div className="col-md-4 pr-0 pl-1 festival-card-location">
                      {props.startDate}
                    </div>
                  </div>
                </div> */}
                <div className="festival-card-details">{props.description}</div>
              </div>
            </div>

            <div className="mt-auto button-row">
              <div className="row pb-2 festival-card-button-section">
                {!appContext.state.user.id ? (
                  <div className="mx-auto p-2">
                    <Link exact="true" to="/login" className="festival-card-free-btn">
                      Claim
                    </Link>
                  </div>
                ) : props.claim ? (
                  <div className="mx-auto p-2">
                    <div
                      onClick={handleClaimItem}
                      disabled={submitAuthDisabled}
                      className="festival-card-free-btn"
                    >
                      Claim
                    </div>
                  </div>
                ) : null}
                {/* 
              {!appContext.state.user.id ? (
                <div className="col-md-6 p-2">
                  <Link
                    exact="true"
                    to="/login"
                    className="festival-card-buy-btn"
                  >
                    {`Buy: $${props.price}`}
                  </Link>
                </div>
              ) : (
                <div className="col-md-6 p-2">
                  <Link
                    exact="true"
                    to={`/payment/${
                      props.itemType === "product"
                        ? `product/${props.productId}`
                        : props.itemType === "service"
                        ? `service/${props.serviceId}`
                        : `experience/${props.experienceId}`
                    }`}
                    className="festival-card-buy-btn"
                  >
                    {`Buy: $${props.price}`}
                  </Link>
                </div>
              )} */}
              </div>
            </div>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default SponsorProductOrServiceCard;
