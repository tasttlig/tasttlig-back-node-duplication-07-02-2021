// Libraries
import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { slice, concat } from 'lodash';
// import Modal from "react-modal";
// import { CopyToClipboard } from "react-copy-to-clipboard";
import { toast } from 'react-toastify';
import { AppContext } from './../../ContextProvider/AppProvider';
import LoadingBar from 'react-top-loading-bar';

// Components
import Nav from '../Navbar/Nav';
import RestaurantCard from './RestaurantCard/RestaurantCard';
import SponsorProductOrServiceCard from './SponsorProductOrServiceCard/SponsorProductOrServiceCard';
import ImageSlider from '../ImageSlider/ImageSlider';
import Footer from '../Footer/Footer';
import GoToTop from '../Shared/GoTop';
import { formatDate, formatMilitaryToStandardTime } from '../Functions/Functions';
import { useHistory } from 'react-router-dom';
import { Select, Input, Form } from '../EasyForm';
// Styling
import './FestivalDetailsPage.scss';
import 'react-toastify/dist/ReactToastify.css';
// import Game from "../../assets/images/games.png";
// import Entertainment from "../../assets/images/entertainment.png";
import AttendanceIcon from '../../assets/images/attendance.png';
import HostIcon from '../../assets/images/host_icon.png';
import SponsorIcon from '../../assets/images/sponsor-icon.png';
import FoodSamplesCreatedCard from '../Dashboard/FoodSamplesCreated/FoodSamplesCreatedCard/FoodSamplesCreatedCard';
import BusinessCreatedCard from '../Dashboard/BusinessCreated/BusinessCreatedCard/BusinessCreatedCard';
import FreeSamplesCard from './FreePaidSponsorCards/FreeSamplesCard';
import PaidVendCard from './FreePaidSponsorCards/PaidVendingCards';
import SponsoredItemCard from './FreePaidSponsorCards/SponsoredItemCards';

toast.configure();

const FestivalDetailsPage = (props) => {

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  console.log("props coming from festival details page:", props)
  let data = {};
  // Set initial state
  const [navBackground, setNavBackground] = useState('nav-white');
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [festivalAttendants, setFestivalAttendants] = useState([]);
  const [festivalHosts, setFestivalHosts] = useState([]);
  const [hostsInFestival, setHostsInFestival] = useState([]);
  const [vendorsInFestival, setVendorsInFestival] = useState([]);
  const [festivalSponsors, setFestivalSponsors] = useState([]);
  const [festivalVendors, setFestivalVendors] = useState([]);
  const [loading, setLoading] = useState(true);
  const [productsInFestival, setProductsInFestival] = useState([]);
  const [servicesInFestival, setServicesInFestival] = useState([]);
  const [experiencesInFestival, setExperiencesInFestival] = useState([]);
  const [foodSamplesInFestival, setFoodSamplesInFestival] = useState([]);
  const [allProductsInFestival, setAllProductsInFestival] = useState([]);
  const [itemSelected, setItemSelected] = useState('all');
  const [selected, setSelected] = useState('all');
  const [filterPrice, setFilterPrice] = useState('none');
  const [filterQuantity, setFilterQuantity] = useState('none');
  const [filterSize, setFilterSize] = useState('none');
  const [dayOfWeek, setDayOfWeek] = useState('');
  const [filterSearch, setFilterSearch] = useState('');
  const [keyword, setKeyword] = useState('');
  const history = useHistory();
  const [tickets, setTickets] = useState([]);
  const [toggleState, setToggleState] = useState(1);
  const [discount, setDiscount] = useState(1);
  const [festivalDiscount, setFestivalDiscount] = useState(1);
  const ref = useRef(null);
  const [festivalTicketStatus, setFestivalTicketStatus] = useState(false);
  localStorage.removeItem("buyFestivalFromNotSignup")
  localStorage.removeItem('festival_id');

  const toggleTab = (index) => {
    setToggleState(index);
  };

  const handleInputChange = (event) => {
    //event.preventDefault();
    setKeyword(event.target.value);
  };
  const _handleKeyDown = (event) => {
    //event.preventDefault();
    if (event.key === 'Enter') {
      event.preventDefault();
      setFilterSearch(keyword);
    }
  };

  const [visibleProducts, setVisibleProducts] = useState(3);
  const [visibleService, setVisibleService] = useState(3);
  const [visibleExperience, setVisibleExperience] = useState(3);

  const handleClickProducts = () => {
    setVisibleProducts((prevVisibleProducts) => prevVisibleProducts + 3);
  };
  const handleClickServices = () => {
    setVisibleProducts((prevVisibleServices) => prevVisibleServices + 3);
  };
  const handleClickExperiences = () => {
    setVisibleProducts((prevVisibleExperiences) => prevVisibleExperiences + 3);
  };

  const valueFromTicket = localStorage.getItem('valueFromTicketDashboard');

  const user_id = () => {
   if (appContext.state.signedInStatus)
    console.log("/subscription-list/:id/:code", appContext.state.user.id);
  };

  // fetch user's subscription list and filter them, if user has level 1 or 2 subsciption set discount accordingly
  const fetchSubscriptionList = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/valid-user-subscriptions/${appContext.state.user.id}`,
      });
      const sub1 = response.data.user.filter(subscription => subscription.subscription_code === 'G_MSHIP2')
      const sub2 = response.data.user.filter(subscription => subscription.subscription_code === 'G_MSHIP3')
      const sub3 = response.data.user.filter(subscription => subscription.subscription_code === 'G_MSHIP2')
      const sub4 = response.data.user.filter(subscription => subscription.subscription_code === 'G_MSHIP3')

// console.log("sub1 coming from festival details page:", sub1)

// console.log("sub2 coming from festival details page:", sub2)


      if (sub2.length > 0 &&( sub2[0].suscribed_festivals.includes(parseInt(props.match.params.festivalId)))){
        setDiscount(0.85);
      }
      else if (sub1.length > 0 && (sub1[0].suscribed_festival !== null || sub1[0].suscribed_festivals.includes(parseInt(props.match.params.festivalId)))){
        setDiscount(0.85);
      }
      console.log("sub3 coming from festival details page:", sub3)

      console.log("sub4 coming from festival details page:", sub4)

    
      if (sub4.length > 0 ){
        setFestivalDiscount(0.85);
      }
      else if (sub3.length > 0){
        setFestivalDiscount(0.85);
      }
 
      //setFestivalAttendants(response.data);
      return response;
    } catch (error) {
      return error.response;
    }
  };
  console.log("discount coming from festival details page:", discount)

  console.log("festivalDiscount coming from festival details page:", festivalDiscount)

  localStorage.removeItem('festivalId');

  //set type of component to display product
  const chooseProductContentType = (product, index) => {
    //condition should be changed by content_type attribute
    if (product.product_creator_type === 'SPONSOR') {
      return (
        <SponsorProductOrServiceCard
          key={index}
          productId={product.product_id}
          businessId={product.product_business_id}
          images={product.image_urls}
          title={product.product_name}
          price={product.product_price}
          address={product.business_address_1}
          restaurant_name={product.business_name}
          city={product.city}
          startDate={product.product_expiry_date && formatDate(product.product_expiry_date)}
          startTime={
            product.product_expiry_time && formatMilitaryToStandardTime(product.product_expiry_time)
          }
          description={product.product_description}
          itemType="product"
          classNameFalse={true}
          product_creator_type={product.product_creator_type}
        />
      );
    } else {
      return (
        <RestaurantCard
          key={index}
          productId={product.product_id}
          images={product.image_urls}
          title={product.product_name}
          price={product.product_price}
          businessId={product.product_business_id}
          address={product.business_address_1}
          restaurant_name={product.business_name}
          quantity={product.product_quantity}
          size={product.product_size}
          city={product.city}
          startDate={product.product_expiry_date && formatDate(product.product_expiry_date)}
          startTime={
            product.product_expiry_time && formatMilitaryToStandardTime(product.product_expiry_time)
          }
          description={product.product_description}
          itemType="product"
          classNameFalse={true}
        />
      );
    }
  };

  //set type of component to display service
  const chooseServiceContentType = (service, index) => {
    if (service.service_creator_type === 'SPONSOR') {
      return (
        <SponsorProductOrServiceCard
          key={index}
          serviceId={service.service_id}
          businessId={service.service_business_id}
          images={service.image_urls}
          title={service.service_name}
          price={service.service_price}
          address={service.business_address_1}
          restaurant_name={service.business_name}
          description={service.service_description}
          itemType="service"
          classNameFalse={true}
          service_creator_type={service.service_creator_type}
        />
      );
    } else {
      return (
        <RestaurantCard
          key={index}
          serviceId={service.service_id}
          images={service.image_urls}
          title={service.service_name}
          businessId={service.service_business_id}
          price={service.service_price}
          address={service.business_address_1}
          quantity={service.service_capacity}
          size={service.service_size_scope}
          restaurant_name={service.business_name}
          description={service.service_description}
          itemType="service"
          classNameFalse={true}
        />
      );
    }
  };
  // Render product cards helper function
  const renderProductCards = (arr) => {
    return arr.map((product, index) => chooseProductContentType(product, index));
  };

  // Render service cards helper function
  const renderServiceCards = (arr) => {
    // console.log('services data', arr);
    return arr.map((service, index) => chooseServiceContentType(service, index));
  };

  // Render experience cards helper function
  const renderExperienceCards = (arr) => {
    return arr.map((experience, index) => (
      <RestaurantCard
        key={index}
        experienceId={experience.experience_id}
        businessId={experience.business_id}
        images={experience.image_urls}
        title={experience.experience_name}
        price={experience.experience_price}
        address={experience.business_address_1}
        restaurant_name={experience.business_name}
        city={experience.city}
        description={experience.experience_description}
        itemType="experience"
        classNameFalse={true}
      />
    ));
  };

  const renderFoodSampleCards = (item, ownerType) => {
    
    return (
      <div className="product">
        {((itemSelected && itemSelected === 'all') || itemSelected === 'food-samples') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {
         
                allProductsInFestival.length !== 0
                  ? allProductsInFestival.map((product) => (
                      <>
                        {
                        product.product_id === item.product_id ? (
                          <FreeSamplesCard
                            key={product.product_id}
                            foodSampleProps={product}
                            festivalId={props.match.params.festivalId}
                            festivalName={festivalDetails[0].festival_name}
                          />
                        ) : null}
                      </>
                    ))
                  : null
              }
            </div>
          </Fragment>
        )}
        {((itemSelected && itemSelected === 'all') || itemSelected === 'experiences') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {experiencesInFestival && experiencesInFestival.length !== 0
                ? experiencesInFestival.map((exp) => (
                    <>
                      {
                      exp.experience_id === item.experience_id ? (
                        <FreeSamplesCard
                          key={exp.product_id}
                          foodSampleProps={exp}
                          festivalId={props.match.params.festivalId}
                          festivalName={festivalDetails[0].festival_name}
                        />
                      ) : null}
                    </>
                  ))
                : null}
            </div>
          </Fragment>
        )}
        {((itemSelected && itemSelected === 'all') || itemSelected === 'services') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {servicesInFestival.length > 0
                ? servicesInFestival.map((service) => (
                    <>
                      {
                      service.service_id === item.service_id ? (
                        <FreeSamplesCard
                          key={service.service_id}
                          foodSampleProps={service}
                          festivalId={props.match.params.festivalId}
                          festivalName={festivalDetails[0].festival_name}
                        />
                      ) : null}
                    </>
                  ))
                : null}
            </div>
          </Fragment>
        )}
      </div>
    );

  };

  const renderPaidVendCards = (item, ownerType) => {
    console.log("renderFoodSampleCards from renderFoodSampleCards:", item)
    return (
      <div className="product">
        {((itemSelected && itemSelected === 'all') || itemSelected === 'food-samples') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {
         
                allProductsInFestival.length !== 0
                  ? allProductsInFestival.map((product) => (
                      <>
                        {
                        product.product_id === item.product_id ? (
                          <PaidVendCard
                            key={product.product_id}
                            foodSampleProps={product}
                            festivalId={props.match.params.festivalId}
                            festivalName={festivalDetails[0].festival_name}
                            discount={discount}
                          />
                        ) : null}
                      </>
                    ))
                  : null
              }
            </div>
          </Fragment>
        )}
        {((itemSelected && itemSelected === 'all') || itemSelected === 'experiences') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {experiencesInFestival && experiencesInFestival.length !== 0
                ? experiencesInFestival.map((exp) => (
                    <>
                      {
                      exp.experience_id === item.experience_id ? (
                        <PaidVendCard
                          key={exp.product_id}
                          foodSampleProps={exp}
                          festivalId={props.match.params.festivalId}
                          festivalName={festivalDetails[0].festival_name}
                          discount={discount}
                        />
                      ) : null}
                    </>
                  ))
                : null}
            </div>
          </Fragment>
        )}
        {((itemSelected && itemSelected === 'all') || itemSelected === 'services') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {servicesInFestival.length > 0
                ? servicesInFestival.map((service) => (
                    <>
                      {
                      service.service_id === item.service_id ? (
                        <PaidVendCard
                          key={service.service_id}
                          foodSampleProps={service}
                          festivalId={props.match.params.festivalId}
                          festivalName={festivalDetails[0].festival_name}
                          discount={discount}
                        />
                      ) : null}
                    </>
                  ))
                : null}
            </div>
          </Fragment>
        )}
      </div>
    );

  };

  const renderSponsoredItemCards = (item, ownerType) => {
    console.log("renderFoodSampleCards from renderFoodSampleCards:", item)
    return (
      <div className="product">
        {((itemSelected && itemSelected === 'all') || itemSelected === 'food-samples') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {
         
                allProductsInFestival.length !== 0
                  ? allProductsInFestival.map((product) => (
                      <>
                        {
                        product.product_id === item.product_id ? (
                          <SponsoredItemCard
                            key={product.product_id}
                            foodSampleProps={product}
                            festivalId={props.match.params.festivalId}
                            festivalName={festivalDetails[0].festival_name}
                          />
                        ) : null}
                      </>
                    ))
                  : null
              }
            </div>
          </Fragment>
        )}
        {((itemSelected && itemSelected === 'all') || itemSelected === 'experiences') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {experiencesInFestival && experiencesInFestival.length !== 0
                ? experiencesInFestival.map((exp) => (
                    <>
                      {
                      exp.experience_id === item.experience_id ? (
                        <SponsoredItemCard
                          key={exp.product_id}
                          foodSampleProps={exp}
                          festivalId={props.match.params.festivalId}
                          festivalName={festivalDetails[0].festival_name}
                        />
                      ) : null}
                    </>
                  ))
                : null}
            </div>
          </Fragment>
        )}
        {((itemSelected && itemSelected === 'all') || itemSelected === 'services') && (
          <Fragment>
            <div className="restaurant-cards-section">
              {servicesInFestival.length > 0
                ? servicesInFestival.map((service) => (
                    <>
                      {
                      service.service_id === item.service_id ? (
                        <SponsoredItemCard
                          key={service.service_id}
                          foodSampleProps={service}
                          festivalId={props.match.params.festivalId}
                          festivalName={festivalDetails[0].festival_name}
                        />
                      ) : null}
                    </>
                  ))
                : null}
            </div>
          </Fragment>
        )}
      </div>
    );

  };

  const renderRestaurantCards = (business, index) => (
    // console.log("business from render Restaurant Cards:", business)
    <div className="business">
      <RestaurantCard
        key={index}
        businessId={business.business_details_user_id}
        images={business.business_image_urls}
        title={business.business_name}
        address={business.business_address_1}
        restaurant_name={business.business_name}
        city={business.city}
        //size={restaurant.sample_size}
        //description={business.description}
        //itemType="foodSample"
      />
    </div>
  );

  // Fetch festival details helper function
  const fetchFestivalDetails = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/festival/${props.match.params.festivalId}`,
      });

      setFestivalDetails(response.data.details);
      ref.current.complete();
      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Payment authorization function
  const paymentApprovalSuccess = async () => {
    try {
      await history.push({
        pathname: `/payment/festival/${props.match.params.festivalId}`,
        festivalDetailsPage: `/festival/${props.match.params.festivalId}`,
        title: props.match.params.festivalId,
      });
      if (localStorage.getItem('isAttendFestivalPaymentSuccessful')) {
        //console.log("outcome iside the local storage",localStorage.getItem("isAttendFestivalPaymentSuccessful") )
      }
    } catch (error) {
      return error.response;
    }
  };

  //fetching tickets to see if the user is attending festival
  const fetchUserTickets = async () => {
    const response = await axios({
      method: 'GET',
      url: `/ticket/all`,
      data: {
        ticket_user_id: appContext.state.user.id,
      },
    });
    setTickets(response.data.details.data);
    return response;
  };
  const checkTicketFestivalId = function (ticketsFromDatabase) {
    let ticketFoundPrice = '';
    for (let ticket of ticketsFromDatabase) {
      if (ticket.festival_id === Number(props.match.params.festivalId)) {
        localStorage.setItem('valueFromFestivalTicket', 'Already Paid');
        ticketFoundPrice = (
          <button value="Already Paid!" className="festival-details-primary-button">
            Already Paid!
          </button>
        );
        break;
      } else if (Number(festivalDetails[0].festival_price) !== 0) {
        localStorage.setItem('valueFromFestivalTicket', festivalDetails[0].festival_price);
        ticketFoundPrice = (
          <button
            value={festivalDetails[0].festival_price}
            onClick={paymentApprovalSuccess}
            className="festival-details-primary-button"
          >
            {festivalDiscount === 1 ? '$' + festivalDetails[0].festival_price : null}

            <strong className = "crossed-out">{festivalDiscount < 1 ? '$' + festivalDetails[0].festival_price : null} </strong>
            {festivalDiscount < 1 ? '$' + ( festivalDetails[0].festival_price * festivalDiscount).toFixed(2) : null}
          </button>
        );
      } else {
        localStorage.setItem('valueFromFestivalTicket', 'Free');
        ticketFoundPrice = (
          <button valiue="Free" className="festival-details-primary-button">
            Free
          </button>
        );
      }
    }
    return ticketFoundPrice;
  };

  // Fetch festival attendants helper function
  const fetchFestivalAttendants = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/attendants/festival/${props.match.params.festivalId}`,
      });

      setFestivalAttendants(response.data);
      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Fetch festival hosts helper function
  const fetchFestivalHosts = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/hosts/festival/${props.match.params.festivalId}`,
      });

      setFestivalHosts(response.data);
      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Fetch festival vendors helper function
  const fetchFestivalVendors = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/vendors/festival/${props.match.params.festivalId}`,
      });

      setFestivalVendors(response.data);
      console.log('vendors coming from vendor fetch', festivalVendors.length === 0);
      return response;
    } catch (error) {
      console.log('error coming from vendor fetch:', error);
      return error.response;
    }
  };
  console.log("festivalVendors coming from festival details page:", festivalVendors)

  // Fetch festival sponsors helper function
  const fetchFestivalSponsors = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/sponsors/festival/${props.match.params.festivalId}`,
      });

      setFestivalSponsors(response.data);
      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Fetch products in festival helper function
  const fetchProductsInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/products/festival/${props.match.params.festivalId}`,
        params: {
          //keyword,
          keyword: filterSearch,
          price: filterPrice,
          quantity: filterQuantity,
          size: filterSize,
        },
      });
      setProductsInFestival(response.data.details);
      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Fetch services in festival helper function
  const fetchServicesInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/services/festival/${props.match.params.festivalId}`,
        params: {
          price: filterPrice,
          quantity: filterQuantity,
          size: filterSize,
          keyword: filterSearch,
        },
      });
      setServicesInFestival(response.data.details);
      return response;
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  // Fetch experiences in festival helper function
  const fetchExperiencesInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/experiences/festival/${props.match.params.festivalId}`,
      });

      setExperiencesInFestival(response.data.details);
      console.log('expssss', response.data.details);

      return response;
    } catch (error) {
      return error.response;
    }
  };

  console.log('experiencesInFestival', experiencesInFestival);

  const fetchallProductsInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/all-products/festival/${props.match.params.festivalId}`,
        params: {
          price: filterPrice,
          quantity: filterQuantity,
          size: filterSize,
          keyword: filterSearch,
          dayOfWeek,
        },
      });
       console.log('fud sm', response.data.details);

      setAllProductsInFestival(response.data.details);
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };
  const fetchRestaurantsInFestival = async () => {
    try {
      const vendorResponse = await axios({
        method: 'GET',
        url: `/vendors/festival/${props.match.params.festivalId}`,
      });
      const hostResponse = await axios({
        method: 'GET',
        url: `/hosts/festival/${props.match.params.festivalId}`,

      });
      setVendorsInFestival(vendorResponse.data);
      setHostsInFestival(hostResponse.data);
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  console.log("vendors in festival:", vendorsInFestival)

  // Get all fetches helper function
  const getAllFetches = async () => {
    await fetchFestivalDetails();
    await fetchFestivalAttendants();
    await fetchFestivalHosts();
    await fetchFestivalVendors();
    await fetchSubscriptionList();

    // await fetchFestivalSponsors();
    // await fetchProductsInFestival();
    await fetchServicesInFestival();
    await fetchExperiencesInFestival();
    await fetchallProductsInFestival();
    await fetchRestaurantsInFestival();
    if (appContext.state.signedInStatus === true) {
      await fetchUserTickets();
    }
    setLoading(false);
  };

  // Mount Festival Details page
  useEffect(() => {
    window.scrollTo(0, 0);
    getAllFetches();
  }, []);

  useEffect(() => {
    getAllFetches();
    ref.current.continuousStart();
  }, [
    //eyword,
    filterPrice,
    filterQuantity,
    filterSize,
    filterSearch,
    dayOfWeek,
  ]);
  // Render Festival Details page
  return (
    <Fragment>
      <LoadingBar color="#88171a" ref={ref} shadow={true} />
      <Nav background={navBackground} />
      {!loading && (
        <Fragment>
          <div className="container festival-details-container">
            <div className="festival-details">
              <div className="row">
                <div className="col-lg-4 festival-details-text-section">
                  <div className="d-flex flex-column h-100">
                    <div className="p-4">
                      <div className="mb-4">
                        <div className="festival-details-title">
                          {festivalDetails[0].festival_name}
                        </div>
                        <div>
                          <span>by </span>
                          {festivalDetails[0].business ? (
                              <Link
                              exact="true"
                              to={`/host/${festivalDetails[0].festival_host_admin_id}`}
                              className="text-center host-detail-btn">
                                {festivalDetails[0].business}
                            </Link>
                          ) : (
                            
                            <Link
                              exact="true"
                              to={`/host/${festivalDetails[0].festival_host_admin_id}`}
                              className="text-center host-detail-btn">
                                Tasttlig
                            </Link>
                          )}
                        </div>
                      </div>
                      <div className="mb-4">
                        <div className="festival-details-sub-title">Date</div>
                        <div>{`${formatDate(
                          festivalDetails[0].festival_start_date,
                        )} to ${formatDate(festivalDetails[0].festival_end_date)}`}</div>
                      </div>
                      <div className="mb-4">
                        <div className="festival-details-sub-title">Time</div>
                        {festivalDetails[0].festival_start_time &&
                          festivalDetails[0].festival_end_time && (
                            <div>
                              {`${formatMilitaryToStandardTime(
                                festivalDetails[0].festival_start_time,
                              )} to ${formatMilitaryToStandardTime(
                                festivalDetails[0].festival_end_time,
                              )}`}
                            </div>
                          )}
                      </div>
                      <div className="mb-4">
                        <div className="festival-details-sub-title">City</div>
                        <div>{`${festivalDetails[0].festival_city}`}</div>
                      </div>
                      {festivalDetails[0].sponsor_name && (
                        <div className="mb-4">
                          <div className="festival-details-sub-title">Sponsored by</div>
                          <div>{festivalDetails[0].sponsor_name}</div>
                        </div>
                      )}
                      {/* <div className="mb-4">
                        <div className="festival-details-sub-title">Description</div>
                        <div>{`${festivalDetails[0].festival_description}`}</div>
                      </div> */}
                    </div>
                    <div className="mt-auto">
                      <div>
                        {/* <Link
                          exact="true"
                          to={`/payment/festival/${festivalDetails[0].festival_id}`}
                          className="text-center festival-details-buy-btn"
                          >
                            ${festivalDetails[0].festival_price}
                        </Link> */}
                        {!appContext.state.signedInStatus ? (
                          <div className="festival-details-button-container">
                            {festivalDetails[0].festival_price &&
                            Number(festivalDetails[0].festival_price) !== 0 ? (
                              <button
                                onClick={() =>
                                  localStorage.setItem(
                                    'buyFestivalFromNotSignup',
                                    props.match.params.festivalId,
                                  )
                                }
                                className="festival-details-primary-button"
                              >
                                <Link
                                  className="festival-details-primary-button"
                                  exact="true"
                                  to={'/sign-up'}
                                >
                                  $ {festivalDetails[0].festival_price}
                                </Link>
                              </button>
                            ) : (
                              <button className="festival-details-primary-button">Free</button>
                            )}
                          </div>
                        ) : (
                          <>
                            {tickets.length !== 0 ? (
                              <div className="festival-details-primary-button-container">
                                {checkTicketFestivalId(tickets)}
                              </div>
                            ) : (
                              <div className="festival-details-button-container">
                                {festivalDetails[0].festival_price &&
                                Number(festivalDetails[0].festival_price) !== 0 ? (
                                  <button
                                  value={festivalDetails[0].festival_price}
                                  onClick={paymentApprovalSuccess}
                                  className="festival-details-primary-button"
                                >
                                  {festivalDiscount === 1 ? '$' + festivalDetails[0].festival_price : null}
                      
                                  <strong className = "crossed-out">{festivalDiscount < 1 ? '$' + festivalDetails[0].festival_price : null} </strong>
                                  {festivalDiscount < 1 ? '$' + ( festivalDetails[0].festival_price * festivalDiscount).toFixed(2) : null}
                                </button>
                                ) : (
                                  <button className="festival-details-primary-button">Free</button>
                                )}
                              </div>
                            )}
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-8 festival-details-images-section">
                  {festivalDetails[0].image_urls.length > 1 ? (
                    <ImageSlider images={festivalDetails[0].image_urls} isCard={false} />
                  ) : (
                    <img
                      src={[festivalDetails[0].image_urls][0]}
                      alt="Festival Details"
                      className="festival-details-images"
                    />
                  )}
                  <div className="festival-details-type">{festivalDetails[0].festival_type}</div>
                  <div className="row">
                    <div className="col-md-6 festival-details-people-info">
                      <i className="fas fa-users festival-details-icon"></i>
                      <span className="festival-details-icon-text">
                        {festivalAttendants.length > 0
                          ? `${festivalAttendants.length} Attendants`
                          : '0 Attendants'}
                      </span>
                    </div>
                    <div className="col-md-6 festival-details-people-info">
                      <i className="fas fa-utensils festival-details-icon"></i>
                      <span className="festival-details-icon-text">
                        {vendorsInFestival.length > 0 ? `${vendorsInFestival.length} Vendors` : '0 Vendors'}
                      </span>
                    </div>
                  </div>
                  <div className="row">
                  <div className="festival-details-sub-title">Description:</div>
                        <div>{`${festivalDetails[0].festival_description}`}</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="tab-row">
                <div className="tab-switcher">
                  <button
                    onClick={() => {
                      toggleTab(1);
                      setSelected('all');
                    }}
                    className={toggleState === 1 ? 'tab active-tab' : 'tab'}
                  >
                    All
                  </button>
                  <button
                    onClick={() => {
                      toggleTab(2);
                      setSelected('Hosts');
                    }}
                    className={toggleState === 2 ? 'tab active-tab' : 'tab'}
                  >
                    Free Samples
                  </button>
                  <button
                    onClick={() => {
                      toggleTab(3);
                      setSelected('vendors');
                    }}
                    className={toggleState === 3 ? 'tab active-tab' : 'tab'}
                  >
                    Buy Items
                  </button>
                  <button
                    onClick={() => {
                      toggleTab(4);
                      setSelected('sponsors');
                    }}
                    className={toggleState === 4 ? 'tab active-tab' : 'tab'}
                  >
                    Sponsored Items
                  </button>
                  <button
                    onClick={() => {
                      toggleTab(5);
                      setSelected('restaurants');
                    }}
                    className={toggleState === 5 ? 'tab active-tab' : 'tab'}
                  >
                    Restaurants
                  </button>
                </div>
              </div>
            </div>
            <div className="row search-field">
              <input
                name="card_search"
                placeholder="Search..."
                type="text"
                value={keyword}
                onChange={handleInputChange}
                onKeyDown={_handleKeyDown}
              ></input>
             {userRole && (userRole.includes("BUSINESS_MEMBER") && ((festivalDetails[0].festival_vendor_id  && !festivalDetails[0].festival_vendor_id.includes(appContext.state.user.id)) && ( festivalDetails[0].vendor_request_id && !festivalDetails[0].vendor_request_id.includes(appContext.state.user.id))) ) || 
             (festivalDetails[0].festival_vendor_id === null && festivalDetails[0].vendor_request_id === null) ||
             (festivalDetails[0].festival_vendor_id === null && festivalDetails[0].vendor_request_id && !festivalDetails[0].vendor_request_id.includes(appContext.state.user.id)) 
             
             ? ( <Link exact="true" to="/complete-profile/festival-vendor" onClick={() => localStorage.setItem("festival_id", props.match.params.festivalId)}
               className="modal-button2">
               Vend this Festival
              </Link>) 
              : null}
            </div>
            <div className="row">
              <div className="col sticky-dash-nav">
                <div className="filters-container">
                  <select
                    name="quantity_shown"
                    label="Products/Services/Experiences"
                    placeholder="Choose Order"
                    onChange={(event) => setItemSelected(event.target.value)}
                  >
                    <option value="all">All</option>
                    <option value="food-samples">Products</option>
                    <option value="services">Services</option>
                    <option value="experiences">Experiences</option>
                  </select>
                  <select
                    name="price_shown"
                    label="Prices"
                    placeholder="Choose Order"
                    onChange={(event) => {
                      setFilterPrice(event.target.value);
                    }}
                  >
                    <option value="">Price</option>
                    {/* <option value="free">Price (free)</option> */}
                    <option value="lowest_to_highest">Price (Lowest to Highest)</option>
                    <option value="highest_to_lowest">Price (Highest to Lowest)</option>
                  </select>
                  <select
                    name="quantity_shown"
                    label="Quantity/Capacity"
                    placeholder="Choose Order"
                    onChange={(event) => setFilterQuantity(event.target.value)}
                  >
                    <option value="">Quantity</option>
                    <option value="lowest_to_highest">Quantity (Lowest to Highest)</option>
                    <option value="highest_to_lowest">Quantity (Highest to Lowest)</option>
                  </select>
                  <select
                    name="size_shown"
                    label="Size/Scope"
                    placeholder="Choose"
                    onChange={(event) => setFilterSize(event.target.value)}
                  >
                    <option value="">Size</option>
                    <option value="bite_size">Bite size</option>
                    <option value="quarter">Quarter</option>
                    <option value="half">Half</option>
                    <option value="full">Full</option>
                    <option value="small">Small</option>
                    <option value="medium">Medium</option>
                    <option value="large">Large</option>
                  </select>
                </div>
              </div>
              <div className="product-container">
                {((selected && selected === 'all') || selected === 'Hosts') && (
                  <>
                    {allProductsInFestival && allProductsInFestival.length > 0
                      ? allProductsInFestival.map((product) => (
                          <>
                            {product.product_offering_type && product.product_offering_type.includes('Host')
                              ? renderFoodSampleCards(product, 'Host')
                              : null}
                          </>
                        ))
                      : null}
                    {experiencesInFestival && experiencesInFestival.length > 0
                      ? experiencesInFestival.map((experience) => (
                          <>
                            {experience.experience_offering_type && experience.experience_offering_type.includes('Host')
                              ? renderFoodSampleCards(experience, 'Host')
                              : null}
                          </>
                        ))
                      : null}
                    {servicesInFestival && servicesInFestival.length > 0
                      ? servicesInFestival.map((services) => (
                          <>
                            {services.service_offering_type && services.service_offering_type.includes('Host')
                              ? renderFoodSampleCards(services, 'Host')
                              : null}
                          </>
                        ))
                      : null}
                  </>
                )}
                {((selected && selected === 'all') || selected === 'sponsors') && (
                  <>
                    {allProductsInFestival && allProductsInFestival.length > 0
                      ? allProductsInFestival.map((product) => (
                          <>
                            {product.product_offering_type && product.product_offering_type.includes('Sponsor')
                              ? renderSponsoredItemCards(product, 'Sponsor')
                              : null}
                          </>
                        ))
                      : null}
                    {experiencesInFestival && experiencesInFestival.length > 0
                      ? experiencesInFestival.map((experience) => (
                          <>
                            {experience.experience_offering_type &&  experience.experience_offering_type.includes('Sponsor')
                              ? renderSponsoredItemCards(experience, 'Sponsor')
                              : null}
                          </>
                        ))
                      : null}
                    {servicesInFestival && servicesInFestival.length > 0
                      ? servicesInFestival.map((services) => (
                          <>
                            {services.service_offering_type && services.service_offering_type.includes('Sponsor')
                              ? renderSponsoredItemCards(services, 'Sponsor')
                              : null}
                          </>
                        ))
                      : null}
                  </>
                )}
                {((selected && selected === 'all') || selected === 'vendors') && (
                 // <Fragment>
                 //   <div className="vendor-cards-section">
                      <>
                        {allProductsInFestival && allProductsInFestival.length > 0
                          ? allProductsInFestival.map((product) => (
                              <>
                                {product.product_offering_type && product.product_offering_type.includes('Vendor')
                                  ? renderPaidVendCards(product, 'Vendor')
                                  : null}
                              </>
                            ))
                          : null}
                        {experiencesInFestival && experiencesInFestival.length > 0
                          ? experiencesInFestival.map((experience) => (
                              <>
                                {experience.experience_offering_type && experience.experience_offering_type.includes('Vendor')
                                  ? renderPaidVendCards(experience, 'Vendor')
                                  : null}
                              </>
                            ))
                          : null}
                        {servicesInFestival && servicesInFestival.length > 0
                          ? servicesInFestival.map((services) => (
                              <>
                                {services.service_offering_type && services.service_offering_type.includes('Vendor')
                                  ? renderPaidVendCards(services, 'Vendor')
                                  : null}
                              </>
                            ))
                          : null}
                      </>
               //     </div>
               //   </Fragment>
                )}
                {((selected && selected === 'all') || selected === 'restaurants') && (
                 // <Fragment>
                    //<div className="business-cards-section">
                      <>
                        {hostsInFestival && hostsInFestival.length > 0
                          ? hostsInFestival.map((host, index) => (
                              <>{renderRestaurantCards(host, index)}</>
                            ))
                          : null}
                        {vendorsInFestival && vendorsInFestival.length > 0
                          ? vendorsInFestival.map((vendor, index) => (
                              <>{renderRestaurantCards(vendor, index)}</>
                            ))
                          : null}
                      </>
                   // </div>
                //  </Fragment>
                )}
              </div>
            </div>
          </div>
        </Fragment>
      )}

      <Footer />

      <GoToTop scrollStepInPx="50" delayInMs="16.66" />
    </Fragment>
  );
};

export default FestivalDetailsPage;
