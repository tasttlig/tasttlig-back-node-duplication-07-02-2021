// Libraries
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';

// Styling
import './TicketCard.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../assets/images/live.png';

toast.configure();

const TicketCard = (props) => {
  const [load, setLoad] = useState(false);

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;

  // Set date and time
  const startDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.startTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.endTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Render Ticket Card
  return (
    <div className="col-md-6 col-xl-4 festival-card">
      <LazyLoad once>
        <div className="d-flex flex-column h-100 card-box">
          <Link exact="true" to={`/ticket/${props.ticketId}`} className="festival-card-link">
            {props.images.length > 1 ? (
              <ImageSlider images={props.images} />
            ) : (
              <img
                src={props.images[0]}
                alt={props.title}
                onLoad={() => setLoad(true)}
                className={load ? 'festival-card-image' : 'loading-image'}
              />
            )}
            {upcomingDays > 0 ? (
              <div className="festival-card-date-upcoming">
                <span className="festival-card-date-upcoming-text">Upcoming</span>
                <span className="festival-card-date-upcoming-days">{`in ${upcomingDays} day${
                  upcomingDays > 1 ? 's' : ''
                }`}</span>
              </div>
            ) : upcomingDays === 0 ? (
              <div className="festival-card-date-live">
                <img src={liveFestival} alt="Live Festival" />
              </div>
            ) : endingDays > 0 ? (
              <div className="festival-card-date-ending">
                <span className="festival-card-date-ending-text">Ending</span>
                <span className="festival-card-date-ending-days">{`in ${endingDays} day${
                  endingDays > 1 ? 's' : ''
                }`}</span>
              </div>
            ) : null}
            <div className="festival-card-text">
              <div className="festival-card-title">{props.title}</div>
              <div className="festival-card-details">
                <div className="row">
                  <div className="col-md-8 festival-card-time">
                    <span className="fas fa-clock pr-1 festival-card-time-icon"></span>
                    Starts At:{' '}
                    {`${moment(startDateTime).format('h:mm a')} - ${moment(endDateTime).format(
                      'h:mm a',
                    )}`}
                  </div>
                  <div className="col-md-4 pr-0 pl-1 festival-card-location">
                    <span className="fas fa-map-marker-alt pr-1 festival-card-location-icon"></span>
                    {props.city}
                  </div>
                </div>
              </div>
              <div className="festival-card-details">{props.description}</div>
            </div>
          </Link>
          <div className="festival-card-footer">
            <div className="row">
              <div className="col-md-8 festival-card-category">{props.type}</div>
              <div className="col-md-4 festival-card-price">Paid</div>
            </div>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default TicketCard;
