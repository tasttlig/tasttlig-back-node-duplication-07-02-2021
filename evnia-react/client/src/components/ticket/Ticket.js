// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../ContextProvider/AppProvider';
// Components
import Nav from '../Navbar/Nav';
import TicketCard from './TicketCard/TicketCard';
import Footer from '../Home/BannerFooter/BannerFooter';
import GoTop from '../Shared/GoTop';
// Styling
import './Ticket.scss';

const Ticket = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [ticketItems, setTicketItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Render ticket cards helper function
  const renderTicketCards = (arr) => {
    return arr.map((card, index) => (
      <TicketCard
        key={index}
        ticketId={card.ticket_id}
        festivalId={card.festival_id}
        title={card.festival_name}
        type={card.festival_type}
        price={card.festival_price}
        city={card.festival_city}
        startDate={card.festival_start_date}
        endDate={card.festival_end_date}
        startTime={card.festival_start_time}
        endTime={card.festival_end_time}
        description={card.festival_description}
        history={props.history}
        images={card.image_urls}
      />
    ));
  };

  // Toggle nationality helper function
  const toggleNationality = (nationalities) => {
    let nationalities_list = [];

    nationalities.map((nationality) => {
      nationalities_list.push(nationality.value.nationality);
    });

    setSelectedNationalities(nationalities_list);
  };

  //To fetch tickets when entered into new page
  const loadNextPage = async (page) => {
    try {
      const response = await axios({
        method: 'GET',
        url: '/ticket/all',
        params: {
          ticket_user_id: appContext.state.user.id,
          page: page + 1,
        },
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  const handleLoadMore = (page = currentPage, tickets = ticketItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      if (!newPage) {
        return false;
      }

      const pagination = newPage.data.details.pagination;
      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);
      setTicketItems(tickets.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);

    handleLoadMore();
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [selectedNationalities, startDate, startTime, cityLocation, filterRadius]);

  // Render empty ticket page
  const TicketEmpty = () => <strong className="no-festivals-found">No tickets found.</strong>;

  // Render Tickets page
  return (
    <div>
      <Nav />

      <div>
        <div className="dashboard">
          <div className="row">
            <div className="col-lg-8 px-0">
              <div className="dashboard-title">Dashboard</div>
            </div>
          </div>
          <div className="col-md-12 p-0">
            <div className="landing-page-cards" ref={infiniteRef}>
              {ticketItems.length !== 0 ? renderTicketCards(ticketItems) : <TicketEmpty />}
            </div>
          </div>
        </div>
      </div>
      <Footer />
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default Ticket;
