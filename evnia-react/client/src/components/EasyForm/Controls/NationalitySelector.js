// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';
import { Select } from '../index';

const NationalitySelector = (props) => {
  const { formData, setValue } = useContext(FormContext);
  const { nationalities, name, label, disabled, required, className, children, ...rest } = props;

  const [experienceType, setExperienceType] = useState('Single National');

  const changeExperienceType = (value) => {
    setValue('type', value);
    if (value !== 'Single National') {
      setValue('nationality_id', '');
    }
    setExperienceType(value);
  };

  useEffect(() => {
    setValue('type', formData['type'] || 'Single National');
    setValue('nationality_id', formData['nationality_id'] || '');
  }, [formData]);

  return (
    <>
      <Select
        name="type"
        label="Type"
        value={experienceType}
        onChange={(e) => changeExperienceType(e.target.value)}
        disabled={props.disabled}
        required
      >
        <option value="Single National">Single National Showcase</option>
        <option value="Multinational">Multinational Showcase</option>
      </Select>

      {experienceType === 'Single National' && nationalities.length && (
        <Select name="nationality_id" label="Nationality" disabled={props.disabled} required>
          <option value="">--Select--</option>
          {nationalities.map((n) => (
            <option key={n.id} value={n.id}>
              {n.nationality}
            </option>
          ))}
        </Select>
      )}
    </>
  );
};

export default NationalitySelector;
