// Libraries
import React, { useState, useContext } from 'react';

// Components
import { FormContext } from '../Form';

const Checkbox = (props) => {
  const { register, errors, formData, readMode } = useContext(FormContext);
  const { name, label, disabled, required, className, onClick, children, ...rest } = props;

  const fieldError = errors[name];
  const [value] = useState(
    (typeof formData[name] === 'boolean' && formData[name]) ||
      (typeof formData[name] === 'string' && formData[name] === 'true'),
  );

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            <input
              type="checkbox"
              name={name}
              defaultChecked={value}
              className={`${className || ''} mr-3`}
              disabled={disabled}
              onClick={onClick}
              ref={register}
              {...rest}
            />
            {label || children}
            {required ? '*' : ''}
          </div>
          {fieldError && <div className="error-message">This field is required.</div>}
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label}
              {required ? '*' : ''}
            </h6>
            <span>{value ? 'Yes' : 'No'}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default Checkbox;
