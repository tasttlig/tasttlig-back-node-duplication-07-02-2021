// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';
import { fileUploadAsync } from '../../../functions/FileUpload';

const MultiImageInput = (props) => {
  const { register, unregister, errors, setValue, formData, readMode } = useContext(FormContext);
  const {
    name,
    label,
    dropbox_label,
    disabled,
    required,
    className,
    dir,
    onChange,
    defaultValue,
    children,
    ...rest
  } = props;

  const fieldError = errors[name];
  const [fieldValues, setFieldValues] = useState(formData[name] || []);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    register(
      { name },
      {
        required,
        validate: (value) => {
          return !required || (value && value.length > 0);
        },
      },
    );
    return () => unregister(name);
  }, [register]);

  useEffect(() => {
    setFieldValues(formData[name] || []);
    setValue(name, formData[name] || []);
  }, [formData]);

  const handleChange = async (e) => {
    if (e.target.files.length) {
      setLoading(true);

      const result = await Promise.all(
        Array.from(e.target.files).map((f) => fileUploadAsync(f, dir)),
      );
      const urls = [...fieldValues, ...result.filter((r) => r.success).map((r) => r.url)];

        
      setFieldValues(urls);
      setValue(name, urls);
      setLoading(false);
      if(onChange) {
        onChange(urls)
      }
    }
  };

  console.log("images from the product form", fieldValues)
  const removeImage = (url) => {
    const urls = fieldValues.filter((f) => f !== url);

    setFieldValues(urls);
    setValue(name, urls);
  };

  const fieldLabel = () => {
    if (!fieldValues.length) {
      return 'Upload your file';
    }

    return fieldValues.split('/').pop().split(' ').slice(1);
  };

  const Loading = () => <div className="dropbox__loading">Loading ...</div>;

  const ImagePreview = ({ url }) => (
    
    <div className="dropbox__image-preview">
      {!readMode && <i className="fa fa-trash" onClick={() => removeImage(url)} />}
      <img src={url} alt={url} />
    </div>
  );

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label || children}
          </div>
          <div>
            <div className="dropbox">
              <div className="dropbox__dropzone">
                <input
                  type="file"
                  id={name}
                  name={name}
                  className={`${className || ''} input-file`}
                  disabled={disabled}
                  accept="image/*"
                  multiple
                  onChange={handleChange}
                  {...rest}
                />

                {loading ? (
                  <Loading />
                ) : (
                  <>
                    <div className="dropbox__label">
                      <i className="fas fa-image" />
                      <div>{dropbox_label}</div>
                    </div>
                  </>
                )}
              </div>

              {fieldValues.length ? (
                <div className="dropbox__image-list">
                  {fieldValues.map((f) => (
                    <ImagePreview key={f} url={f} />
                  ))}
                </div>
              ) : null}
            </div>
          </div>
          {fieldError && <div className="error-message">This field is required.</div>}
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label}
              {required ? '*' : ''}
            </h6>
            {fieldValues.length && (
              <div className="dropbox__image-list dropbox__image-list__readMode">
                {fieldValues.map((f) => (
                  <ImagePreview key={f} url={f} />
                ))}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default MultiImageInput;
