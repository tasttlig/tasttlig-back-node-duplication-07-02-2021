// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';

const Select = (props) => {
  const { register, errors, formData, setValue, readMode } = useContext(FormContext);
  const { name, label, disabled, required, className, options, children, ...rest } = props;

  const onChange = props.onChange ? props.onChange : () => {};

  const fieldError = errors[name];
  const [fieldValue, setFieldValue] = useState(formData[name] || '');

  useEffect(() => {
    setFieldValue(formData[name] || '');
    setValue(name, formData[name] || '');
  }, [formData]);

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label ? label : options ? children : ''}
            {/* {required ? "*" : ""} */}
          </div>
          <select
            name={name}
            defaultValue={fieldValue}
            className={`${className || ''} custom-select`}
            disabled={disabled}
            onChange={onChange}
            ref={register({ required })}
            {...rest}
          >
            {options ? (
              <>
                {options.map((o) => (
                  <option key={o[1]} value={o[1]}>
                    {o[0]}
                  </option>
                ))}
              </>
            ) : (
              <>{children}</>
            )}
          </select>
          {fieldError && <div className="error-message">This field is required.</div>}
        </>
      ) : (
        <div>
          <h6 className="ez-form__readMode-title">
            {label ? label : options ? children : ''}
            {required ? '*' : ''}
          </h6>
          <span>{fieldValue || 'N/A'}</span>
        </div>
      )}
    </div>
  );
};

export default Select;
