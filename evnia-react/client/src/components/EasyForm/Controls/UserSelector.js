// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Components
import { FormContext } from '../Form';

const UserSelector = (props) => {
  const { register, errors, setValue, formData, readMode } = useContext(FormContext);
  const { name, label, disabled, required, className, children, ...rest } = props;

  const fieldError = errors[name];
  const [fieldValue, setFieldValue] = useState(formData[name] || '');
  const [emailDoesNotExist, setEmailDoesNotExist] = useState('');

  const onChange = async (e) => {
    const response = await axios({
      method: 'GET',
      url: `/user/${e.target.value}`,
    });

    setEmailDoesNotExist(response.data.message);
  };

  useEffect(() => {
    setFieldValue(formData[name] || '');
    setValue(name, formData[name] || '');
  }, [formData]);

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label || children}
            {required ? '*' : ''}
          </div>
          <input
            type="email"
            name={name}
            defaultValue={fieldValue}
            onChange={onChange}
            className={`${className || ''} form-control`}
            disabled={disabled}
            ref={register({ required })}
            {...rest}
          />
          {fieldError || emailDoesNotExist ? (
            <div className="error-message">
              Email does not exist. Please create new user (
              <Link exact="true" to="/complete-profile/business" className="create-user-link">
                Link
              </Link>
              ).
            </div>
          ) : null}
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label || children}
              {required ? '*' : ''}
            </h6>
            <span>{fieldValue}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default UserSelector;
