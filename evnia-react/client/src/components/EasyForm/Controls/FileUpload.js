// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';
import fileUpload from '../../../functions/FileUpload';

const FileUpload = (props) => {
  const { register, unregister, errors, setValue, formData, readMode } = useContext(FormContext);
  const {
    name,
    label,
    disabled,
    required,
    className,
    dir,
    onChange,
    defaultValue,
    children,
    ...rest
  } = props;

  const fieldError = errors[name];
  const [fieldValue, setFieldValue] = useState(formData[name] || defaultValue || '');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    register({ name }, { required });
    return () => unregister(name);
  }, [register]);

  useEffect(() => {
    setFieldValue(formData[name] || defaultValue || '');
    setValue(name, formData[name] || defaultValue || '');
  }, [formData]);

  const handleChange = (e) => {
    if (e.target.files.length) {
      setLoading(true);
      fileUpload(e.target.files[0], dir, (url) => {
        setValue(name, url);
        setFieldValue(url);
        setLoading(false);
        if (onChange) {
          onChange(url);
        }
      });
    }
  };

  const removeFile = () => {
    setValue(name, null);
    setFieldValue(null);
    if (onChange) {
      onChange(null);
    }
  };

  const fieldLabel = () => {
    if (!fieldValue) {
      return 'Upload your file';
    }

    const label = fieldValue.split('/').pop().split(' ').slice(1).join(' ');

    return label || fieldValue;
  };

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label || children}
            {required ? '*' : ''}
          </div>
          <div className="ez-form__file-upload">
            <div className="ez-form__file-upload-wrapper">
              <label htmlFor={name} className="ez-form__file-upload-label">
                {fieldLabel()}
              </label>
              {(fieldValue || loading) && (
                <div className="ez-form__file-upload-remove">
                  <span className="separator"></span>
                  {loading ? (
                    <div className="spinner-border text-secondary" role="status">
                      <span className="sr-only">Loading...</span>
                    </div>
                  ) : (
                    <i className="fa fa-times" onClick={removeFile}></i>
                  )}
                </div>
              )}
            </div>
            <input
              type="file"
              id={name}
              name={name}
              className={`${className || ''} form-control-file`}
              disabled={disabled}
              onChange={handleChange}
              {...rest}
            />
          </div>
          {fieldError && <div className="error-message">This field is required.</div>}
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label}
              {required ? '*' : ''}
            </h6>
            {fieldValue && (
              <a href={fieldValue} target="_blank">
                {fieldLabel()}
              </a>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default FileUpload;
