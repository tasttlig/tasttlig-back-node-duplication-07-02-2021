// Libraries
import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';

export const FormContext = React.createContext({
  register: null,
  watch: null,
  errors: null,
  control: null,
  setValue: null,
  setError: null,
  formData: null,
  readMode: false,
});

const Form = ({ children, data, onSubmit, readMode, formRef }) => {
  const formMethods = useForm();

  const [formData, setFormData] = useState(data);

  useEffect(() => {
    setFormData(data);
  }, [data]);

  useEffect(() => {
    if (formRef && formRef.current) {
      formRef.current.getValues = formMethods.getValues;
    }
  }, [formRef]);

  const contextValue = {
    formData,
    readMode,
    ...formMethods,
  };

  return (
    <FormContext.Provider value={contextValue}>
      <form
        ref={formRef}
        className="mx-auto needs-validation"
        onSubmit={formMethods.handleSubmit(onSubmit)}
        noValidate
      >
        {children}
      </form>
    </FormContext.Provider>
  );
};

export default Form;
