// Libraries
import React, { useState } from 'react';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import Flag from 'react-world-flags';

// Components
import PassportDetails from '../../Passport-Old/PassportDetails/PassportDetails';

const FoodSampleOwnerItems = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);
  const [disableReserveButton] = useState(props.disableButton ? props.disableButton : false);

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(false);
    }
  };

  // Render Food Sample Owner Item Card
  return (
    <div className="col-sm-6 col-lg-4 col-xl-3 passport-card">
      <LazyLoad once>
        {/* Passport (Food Sample) Details Modal */}
        <Modal
          isOpen={passportDetailsOpened}
          onRequestClose={closeModal('passport-details')}
          ariaHideApp={false}
          className="passport-details-modal"
        >
          <div className="mb-3 text-right">
            <span
              onClick={closeModal('passport-details')}
              className="fas fa-times fa-2x close-modal"
            ></span>
          </div>

          <PassportDetails
            business_name={props.business_name}
            foodSampleId={props.foodSampleId}
            images={props.images}
            title={props.title}
            price={props.price}
            quantity={props.quantity}
            numOfClaims={props.numOfClaims}
            address={props.address}
            city={props.city}
            provinceTerritory={props.provinceTerritory}
            postalCode={props.postalCode}
            startDate={props.startDate}
            endDate={props.endDate}
            startTime={props.startTime}
            endTime={props.endTime}
            description={props.description}
            sample_size={props.sample_size}
            is_available_on_monday={props.is_available_on_monday}
            is_available_on_tuesday={props.is_available_on_tuesday}
            is_available_on_wednesday={props.is_available_on_wednesday}
            is_available_on_thursday={props.is_available_on_thursday}
            is_available_on_friday={props.is_available_on_friday}
            is_available_on_saturday={props.is_available_on_saturday}
            is_available_on_sunday={props.is_available_on_sunday}
            foodSampleOwnerId={props.foodSampleOwnerId}
            foodSampleOwnerPicture={props.foodSampleOwnerPicture}
            isEmailVerified={props.isEmailVerified}
            firstName={props.firstName}
            lastName={props.lastName}
            email={props.email}
            phoneNumber={props.phoneNumber}
            facebookLink={props.facebookLink}
            twitterLink={props.twitterLink}
            instagramLink={props.instagramLink}
            youtubeLink={props.youtubeLink}
            linkedinLink={props.linkedinLink}
            websiteLink={props.websiteLink}
            bioText={props.bioText}
            frequency={props.frequency}
            disableButton={disableReserveButton}
            history={props.history}
            passportId={props.passportId}
          />

          <div className="close-modal-bottom-content">
            <span onClick={closeModal('passport-details')} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

        <div className="d-flex flex-column h-100 card-box">
          <div
            onClick={openModal('passport-details')}
            className="passport-card-content mb-0 text-center"
          >
            <img
              src={props.images[0]}
              alt={props.title}
              onLoad={() => setLoad(true)}
              className={load ? 'passport-image' : 'loading-image'}
            />
            <div className="passport-card-flag-container">
              <Flag code={props.alpha_2_code} className="passport-card-flag" />
              <div className="passport-card-flag-country">{props.nationality}</div>
            </div>
            <div className="passport-card-text">
              <div className="mt-auto passport-title">{props.title}</div>
            </div>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default FoodSampleOwnerItems;
