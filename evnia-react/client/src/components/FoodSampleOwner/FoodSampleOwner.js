// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import FoodSampleOwnerItems from './FoodSampleOwnerItems/FoodSampleOwnerItems';

// Styling
import './FoodSampleOwner.scss';
import defaultProfilePicture from '../../assets/images/default-profile-picture.png';

const FoodSampleOwner = (props) => {
 
  // Set initial state
  const [load, setLoad] = useState(false);
  const [foodSampleOwner, setFoodSampleOwner] = useState([]);
  const [foodSampleOwnerItems, setFoodSampleOwnerItems] = useState([]);
  const [fetchedServices, setFetchServices] = useState([]);
  const [fetchedExperiences, setFetchedExperiences] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Render Food Sample Owner Items helper function
  const renderFoodSampleOwnerItems = (arr) => {
    return arr.map((card, index) => (
      <FoodSampleOwnerItems
        key={index}
        
        foodSampleId={card.product_id ? card.product_id : card.product_id ? card.product_id : card.service_id }
        
        images={card.image_urls}
        
        title={card.title ? card.title : card.experience_name ? card.experience_name : card.service_name}
        
        nationality={card.nationality ? card.nationality : card.experience_nationality_id ? card.experience_nationality_id : card.service_nationality_id}
       
        alpha_2_code={card.alpha_2_code ? card.alpha_2_code :null}
        
        frequency={card.frequency ? card.frequency : null}
        
        price={card.price ? card.price : card.experience_price ? card.experience_price : card.service_price }
        
        quantity={card.quantity ? card.quantity : card.experience_capacity ? card.experience_capacity : card.service_capacity}
        
        numOfClaims={card.claimed_total_quantity ? card.claimed_total_quantity :null}
        
        address={`${card.business_unit} - ${card.business_street_number}, ${card.business_street_name}`}
        
        city={card.city}
        
        provinceTerritory={card.state}
       
        postalCode={card.zip_postal_code}
        // startDate={card.start_date}
        // endDate={card.end_date}
        
        startTime={card.start_time}
        
        endTime={card.end_time}
        
        description={card.description ? card.description : card.experience_description ? card.experience_description : card.service_description}
        
        sample_size={card.product_size ? card.product_size : card.experience_size_scope ? card.experience_size_scope : card.service_size_scope}
        
        is_available_on_monday={card.is_available_on_monday ? card.is_available_on_monday :null}
        
        is_available_on_tuesday={card.is_available_on_tuesday ? card.is_available_on_tuesday :null}
        
        is_available_on_wednesday={card.is_available_on_wednesday ? card.is_available_on_wednesday :null}
       
        is_available_on_thursday={card.is_available_on_thursday ? card.is_available_on_thursday :null}
        
        is_available_on_friday={card.is_available_on_friday ? card.is_available_on_friday :null} 
        
        is_available_on_saturday={card.is_available_on_saturday ? card.is_available_on_saturday :null}
       
        is_available_on_sunday={card.is_available_on_sunday ? card.is_available_on_sunday :null}
        
        foodSampleOwnerBusinessId={card.business_details_id}

        // foodSampleOwnerPicture={foodSampleOwner.profile_image_link}
        isEmailVerified={foodSampleOwner.is_email_verified}
        firstName={foodSampleOwner.first_name}
        lastName={foodSampleOwner.last_name}
        email={foodSampleOwner.email}
        phoneNumber={foodSampleOwner.phone_number}
        facebookLink={foodSampleOwner.facebook_link}
        twitterLink={foodSampleOwner.twitter_link}
        instagramLink={foodSampleOwner.instagram_link}
        youtubeLink={foodSampleOwner.youtube_link}
        linkedinLink={foodSampleOwner.linkedin_link}
        websiteLink={foodSampleOwner.website_link}
        bioText={foodSampleOwner.bio_text}
        history={props.history}
        passportId={
          appContext.state && appContext.state.user && appContext.state.user.passport_id
            ? appContext.state.user.passport_id
            : ''
        }
      />
    ));
  };
  console.log("props from food sample owner's page:", props)
  console.log("fetchedServices from food sample owner's page:", fetchedServices)
  console.log("fetchedExperiences from food sample owner's page:", fetchedExperiences)


  const fetchFoodSamples = async () => {
    const url = `/food-sample/owner/${props.match.params.id}`;
    const response = await axios({ method: 'GET', url });
    console.log("115", `/food-sample/owner/${props.match.params.id}`); 
    setFoodSampleOwner(response.data.owner_user);
    setFoodSampleOwnerItems(response.data.food_samples);
  };

  const fetchUserExperiences = async () => {
    const response = await axios({
      method: 'GET',
      url: `/experiences/${props.match.params.id}`,
      data: {
        user_id: props.match.params.id,
      },
    });
    setFetchedExperiences(response.data.details);
    return response;
  };


  const fetchUserServices = async () => {
    const response = await axios({
      method: 'GET',
      url: `/services/details/${props.match.params.id}`,
     
    });
    setFetchServices(response.data.details);
    return response;
  };

  // Mount Passport (Food Samples) page
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchFoodSamples();
    fetchUserExperiences();
    fetchUserServices();
  }, []);


  // Food Sample Owner Banner Component
  const Banner = () => (
    <div>
      <div className="food-sample-owner-profile-banner">
        <div className="row food-sample-owner-profile-banner-simple">
          <div className="col-md-5 banner-content">
            <div className="mb-4">
              {foodSampleOwner.profile_image_link ? (
                <img
                  src={foodSampleOwner.profile_image_link}
                  alt={`${foodSampleOwner.first_name} ${foodSampleOwner.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'food-sample-owner-profile-picture' : 'loading-image'}
                />
              ) : (
                <img
                  src={defaultProfilePicture}
                  alt={`${foodSampleOwner.first_name} ${foodSampleOwner.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'food-sample-owner-default-picture' : 'loading-image'}
                />
              )}
            </div>
            <div className="food-sample-owner-profile-name">
              {`${foodSampleOwner.first_name} ${foodSampleOwner.last_name}`}
            </div>
            <div className="food-sample-owner-profile-promo">
              {foodSampleOwner.profile_tag_line && (
                <div className="pro mb-4 food-sample-owner-profile-tag-line">
                  {foodSampleOwner.profile_tag_line}
                </div>
              )}
              <address>
                {foodSampleOwner.business_name && (
                  <div>
                    <strong>{foodSampleOwner.business_name}</strong>
                    <br />
                  </div>
                )}
                {foodSampleOwner.business_address_1 && foodSampleOwner.business_address_1}
                {foodSampleOwner.business_address_2 && (
                  <span>, {foodSampleOwner.business_address_2}</span>
                )}
                <br />
                {foodSampleOwner.city && `${foodSampleOwner.city},`}{' '}
                {foodSampleOwner.state && foodSampleOwner.state}{' '}
                {foodSampleOwner.zip_postal_code && foodSampleOwner.zip_postal_code}
                <br />
                <div title="Email">E: {foodSampleOwner.email}</div>
                <div title="Phone">P: {foodSampleOwner.phone_number}</div>
              </address>
            </div>
            {appContext.state.user.id === foodSampleOwner.tasttlig_user_id && (
              <div>
                <Link to="/account" className="edit-profile-link">
                  Edit Profile
                </Link>
              </div>
            )}
          </div>
          <div className="col-md-7 banner-content">
            <div className="banner-images">
              <div className="banner-images-wrapper">
                <div className="banner-color-block-container">
                  <div className="banner-color-block"></div>
                </div>
                {foodSampleOwner.banner_image_link ? (
                  <img
                    src={foodSampleOwner.banner_image_link}
                    alt="Banner"
                    onLoad={() => setLoad(true)}
                    className={load ? 'main-banner-image' : 'loading-image'}
                  />
                ) : (
                  <div className="main-banner-image"></div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="fade-rule" />
    </div>
  );

  // Render Food Sample Owner page
  return (
    <div>
      <Nav />

      {/* Food Sample Owner section */}
      <div className="food-sample-owner">
        <Banner />

        {/* Food Sample Owner Items section */}
          <h2> Products </h2>
        <div className="food-sample-owner-items">
          {renderFoodSampleOwnerItems(foodSampleOwnerItems)}
        </div>

        <h2> Experiences </h2>
        <div className="food-sample-owner-items">
          {renderFoodSampleOwnerItems(fetchedExperiences)}
        </div>

        <h2> Services </h2>
        <div className="food-sample-owner-items">
          {fetchedServices && fetchedServices.length > 0 ? renderFoodSampleOwnerItems(fetchedServices) : null}
        </div>

      </div>
    </div>
  );
};

export default FoodSampleOwner;
