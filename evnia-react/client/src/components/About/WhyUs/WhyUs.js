// Libraries
import React, { Component } from 'react';
// import { Link } from "react-router-dom";

export default class WhyUs extends Component {
  // Render Why Us Component page
  render = () => {
    return (
      <section className="why-choose-us">
        <div className="row m-0">
          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="pb-3 content">
                    <div className="icon">
                      <i className="icofont-headphone-alt-1"></i>
                    </div>
                    <h3>Experiences</h3>
                    <p>
                      Must have on website: Nice 3-course meal food photos at affordable prices.
                      Travel through the world in Toronto - Sample and eat your way through.
                    </p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="pb-3 content">
                    <div className="icon">
                      <i className="icofont-network-tower"></i>
                    </div>
                    <h3>Hosts</h3>
                    <p>
                      Link Participant’s profiles to their websites and social handles with ratings,
                      stories and nice photos of the place they are going to be eating at. People
                      judge the place they are eating at.
                    </p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="pb-3 content">
                    <div className="icon">
                      <i className="icofont-users-social"></i>
                    </div>
                    <h3>Ratings</h3>
                    <p>
                      Tasttlig selects people to visit the applicant's location to taste their food
                      and vote on the best application to be selected to be part of the experience.
                    </p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="pb-3 content">
                    <div className="icon">
                      <i className="icofont-wink-smile"></i>
                    </div>
                    <h3>Guests</h3>
                    <p>
                      The public is invited to participate by buying tickets from Tasttlig.com as
                      guests.
                    </p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ul className="slideshow">
          <li>
            <span></span>
          </li>
          <li>
            <span></span>
          </li>
          <li>
            <span></span>
          </li>
          <li>
            <span></span>
          </li>
        </ul>
      </section>
    );
  };
}
