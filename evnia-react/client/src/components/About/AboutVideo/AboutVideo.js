// Libraries
import React from 'react';
import ReactPlayer from 'react-player/youtube';

const AboutVideo = () => {
  return (
    <div className="react-player-content">
      <ReactPlayer
        url="https://www.youtube.com/watch?v=NxA2sCM7gag"
        controls={true}
        className="react-player"
      />
    </div>
  );
};

export default AboutVideo;
