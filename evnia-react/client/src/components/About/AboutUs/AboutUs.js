// Libraries
import React, { Component } from 'react';

// Components
import AboutVideo from '../../Home/AboutVideo/AboutVideo';

// Styling
import './AboutUs.scss';

export default class AboutUs extends Component {
  // Render About Us Component page
  render = () => {
    return (
      <section className="about-area-three bg-image">
        <div className="about-us">
          <AboutVideo />

          <div className="row h-100 align-items-center">
            <div className="col-lg-6 about-us-section">
              <div className="about-image">
                <img
                  src={require('../../../assets/images/food-festival.jpg')}
                  className="about-img1"
                  alt="Food Festival"
                />
              </div>
            </div>

            <div className="col-lg-6 about-us-section">
              <div className="about-content">
                <span>About Us</span>
                <h2>Economic empowerment to multinational businesses.</h2>
                <h6>
                  Craft immersive customer experiences. Tasttlig would like to celebrate Toronto and
                  Canada as a hub for all these nationalities coming together in one place.
                </h6>
                <p>
                  Bring economic development and opportunities for multinational food
                  establishments. Tasttlig aims to showcase Toronto as a multinational food mecca.
                  People from around the world come to live and bring their talents and skill sets
                  and their unique experiences with respect to food from around the world to one
                  city. North America's fourth-largest city. Tasttlig is hoping to build
                  collaborations with the City of Toronto and its multinational food landscape. Make
                  reservations to Tasttlig participating restaurants. Get the booklet. It has over
                  200 participating restaurants. Contact so you can book your reservation and enjoy
                  Tasttlig experiences.
                </p>

                <ul>
                  <li>
                    <i className="icofont-long-arrow-right"></i>
                    Experiences
                  </li>
                  <li>
                    <i className="icofont-long-arrow-right"></i>
                    Hosts
                  </li>
                  <li>
                    <i className="icofont-long-arrow-right"></i>
                    Ratings
                  </li>
                  <li>
                    <i className="icofont-long-arrow-right"></i>
                    Guests
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  };
}
