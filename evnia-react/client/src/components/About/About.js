// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import AboutVideo from './AboutVideo/AboutVideo';
import Footer from '../Home/BannerFooter/BannerFooter';

// Styling
import './About.scss';

const About = () => {
  // Mount About page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render About page
  return (
    <div>
      <Nav />

      <div className="about">
        <AboutVideo />
      </div>

      <Footer />
    </div>
  );
};

export default About;
