// Libraries
import React, { useState, useEffect, useContext, Fragment } from 'react';
import axios from 'axios';
import { AppContext } from '../../ContextProvider/AppProvider';
import Modal from 'react-modal';
// Components
/* import Nav from '../Navbar/Nav';
import NationalitySelector from '../Passport-Old/NationalitySelector';
import Footer from '../Footer/Footer';
import GoTop from '../Shared/GoTop'; */
import ExampleImage from '../../assets/images/Kelewele.jpeg';
import { toast } from 'react-toastify';

// Styling
import './UserReviews.scss';
import '../LandingPage/Explore/Explore.scss';

// Set initial state

/*     const sendAmountOfPopups = async () => {
      console.log(modalInformation);
      const response = await axios({
        method: 'GET',
        url: `/reviews/pop-up`,
        params: { review_id: modalInformation.review_id },
      });
      console.log(response);
    };
  
     useEffect(() => {
       fetchUserReviews();
     }, [appContext]); */

const UserReview = (props) => {
  const [foodTaste, setFoodTaste] = useState(0);
  const [authenticity, setAuthenticity] = useState(0);
  const [service, setService] = useState(0);
  const [transit, setTransit] = useState(0);
  const [appearance, setAppearance] = useState(0);
  const [finalScore, setFinalScore] = useState(0);
  const [additionalInfo, setAdditionalInfo] = useState('');

  const appContext = useContext(AppContext);

  let data = {};

  const handleSubmitUserReview = async (event) => {
    event.preventDefault();
    //console.log(event);
    //api call to backend
    let data = {
      foodTaste,
      authenticity,
      service,
      transit,
      appearance,
      finalScore,
      additionalInfo,
    };

    const response = await axios({
      method: 'POST',
      url: `/reviews/add`,
      data: data,
      params: { review_id: props.modalInformation.review_id },
    });

    if (response && response.data && response.data.success) {
      setTimeout(() => {
        // window.location.reload();
        window.location.href = `/dashboard`;
      }, 2000);

      toast(`Success! Thank you for submitting a review!`, {
        type: 'success',
        autoClose: 2000,
      });
    } else {
      toast(`Error! Something went wrong!`, {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const calculateFinalScore = () => {
    let average =
      (Number(foodTaste) +
        Number(authenticity) +
        Number(service) +
        Number(transit) +
        Number(appearance)) /
      5;
    setFinalScore(average);
  };

  useEffect(() => {
    calculateFinalScore();
  }, [foodTaste, authenticity, service, transit, appearance]);

  // Fetch if pop-up should occur
  /*   const fetchUserReviews = async () => {
    const response = await axios({
      method: 'GET',
      url: `/reviews/user/${appContext.state.user.id}`,
      data: {
        productId: props.productId,
      },
    });
    console.log(response);
    if (response && response.data && response.data.success && response.data.details) {
      setModalInformation(response.data.details);
    } else {
    }
  };
  useEffect(() => {
    fetchUserReviews();
  }, [appContext]); */

  return (
    <div className="container user-review-container">
      <div className="top-section-user-review">
        <strong className="row justify-content-center user-review-title">Review</strong>
        <div className="user-review-information">
          <div>
            Thank you for visiting{' '}
            {props.modalInformation.business_name ? (
              <strong>{props.modalInformation.business_name}</strong>
            ) : (
              <strong>Example Restaurant</strong>
            )}{' '}
            and trying out their{' '}
            {props.modalInformation.title ? (
              <strong>{props.modalInformation.title}</strong>
            ) : (
              <strong>Example Product</strong>
            )}
            !
          </div>
          <div>
            <img
              className="user-review-image"
              src={
                props.modalInformation && props.modalInformation.image_urls
                  ? props.modalInformation.image_urls[0]
                  : ExampleImage
              }
            ></img>
          </div>
          <div>How was your experience?</div>
          <div>
            Please rate our T.A.S.T.E SCORE [1 means dissatisfaction, 5 means highly satisfied]
          </div>
        </div>
      </div>
      <form onSubmit={handleSubmitUserReview}>
        <div className="user-review-rating">
          <div className="row justify-content-center">
            <div className="col-8">
              Taste of the food [Does the tasting experience satisfy your tastebuds?]
            </div>
            <select
              name="food-taste"
              className="col-2 user-review-scores"
              onChange={(event) => {
                // console.log(event.target.value);
                setFoodTaste(event.target.value);
              }}
              value={foodTaste}
            >
              <option value="">--Select--</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">
              Authenticity of the food staff [Does the experience remind you of the origin of the
              Nationality?]
            </div>
            <select
              name="authenticity-food"
              onChange={(event) => {
                //console.log(event.target.value);
                setAuthenticity(event.target.value);
              }}
              value={authenticity}
              className="col-2 user-review-scores"
            >
              <option value="">--Select--</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">
              Service of the restaurant [Does the service experience make you feel loved and
              appreciated?]
            </div>
            <select
              name="service"
              className="col-2 user-review-scores"
              value={service}
              onChange={(event) => {
                console.log(event.target.value);
                setService(event.target.value);
              }}
            >
              <option value="">--Select--</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">
              Transit to get to the restaurant [Was it a relatively easy experience finding the
              location of the restaurant?]
            </div>
            <select
              name="transit"
              value={transit}
              className="col-2 user-review-scores"
              onChange={(event) => {
                //console.log(event.target.value);
                setTransit(event.target.value);
              }}
            >
              <option value="">--Select--</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">
              General appearance of restaurant [Does the ambiance, decor, cleanliness and look of
              the restaurant provide you a pleasant experience?]
            </div>
            <select
              name="appearance"
              value={appearance}
              className="col-2 user-review-scores"
              onChange={(event) => {
                //console.log(event.target.value);
                setAppearance(event.target.value);
              }}
            >
              <option value="">--Select--</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className="row justify-content-center">
            <div className="col-8">YOUR T.A.S.T.E SCORE FOR THIS RESTAURANT IS</div>
            <input
              name="final-score"
              className="col-1 user-review-scores"
              value={`${finalScore} / 5`}
              disabled
            ></input>
          </div>
        </div>
        <div className="additional-information-user-review">
          <div>Please feel free to add any additional comments.</div>
          <div>
            Restaurants will see this score with you anonymity to help them improve the experience
          </div>
          <textarea
            className="additional-info-review"
            name="additional-info"
            placeholder="Additional Information"
            value={additionalInfo}
            onChange={(event) => {
              setAdditionalInfo(event.target.value);
            }}
          ></textarea>
          <button className="user-review-submit-button" type="submit">
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default UserReview;
