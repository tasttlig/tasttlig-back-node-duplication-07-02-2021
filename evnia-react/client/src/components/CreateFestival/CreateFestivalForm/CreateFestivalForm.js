// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

// Components
import { DateInput, Form, Input, MultiImageInput, Select, Textarea } from '../../EasyForm';

// Styling
import './CreateFestivalForm.scss';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const CreateFestivalForm = (props) => {



  const data = props.update ? (props.update) : {};

  // const date1 = new Date();
  // console.log('NEW DATE', date1);

  // Set initial state
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Submit festival helper function
  const submitFestival = async (data) => {
    window.scrollTo(0, 0);

    try {
      const url = props.update ? `/festival/update/${props.update.festivalId}` : '/festival/add';

      const response = await axios({
        method: props.update ? 'PUT' : 'POST',
        url,
        data,
      });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.href = '/dashboard';
        }, 2000);

        toast(`Success! Thank you for ${props.update ? 'updating' : 'creating'} a festival!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    const { ...rest } = values;
    console.log("values from on submit:", values)
    let data = {
      ...rest,
    };
    
    data.festival_start_time = values.festival_start_time.toString().substring(16, 21);
    data.festival_end_time = values.festival_end_time.toString().substring(16, 21);
    
    if (props.update) {
      data = {
        festival_update_data: data,
      };
    }

    await submitFestival(data);
  };

  // Mount Create Festival page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Create Festival page
  return (
    <div className="create-festival">
      <div className="create-festival-title">
        {props.heading ? props.heading : 'Create Festival'}
      </div>

      <Form data={data} onSubmit={onSubmit}>
        <MultiImageInput
          name="images"
          label="Images"
          dropbox_label="Click or drag-and-drop to upload one or more images"
          disabled={submitAuthDisabled}
          required
        />

        <Input name="festival_name" label="Festival Name" disabled={submitAuthDisabled} required />

        <Select name="festival_type" label="Type" disabled={submitAuthDisabled} required>
          <option value="">--Select--</option>
          <option value="Spring Festival">Spring Festival</option>
          <option value="Summer Festival">Summer Festival</option>
          <option value="Fall Festival">Fall Festival</option>
          <option value="Winter Festival">Winter Festival</option>
        </Select>
        <div className="row">
          <div className="col-md-6  create-festival-start-date"> 
            <Input
              name="festival_price"
              label="Festival Price"
              type="number"
              step="0.01"
              min="0"
              disabled={submitAuthDisabled}
              required
            />
           </div> 
           <div className="col-md-6  create-festival-start-date"> 
            <Input
              name="festival_vendor_price"
              label="Food Vendor Price (optional)"
              type="number"
              step="0.01"
              min="0"
              disabled={submitAuthDisabled}
              required
            />
           </div> 
        </div>

        <Input name="festival_city" label="City" disabled={submitAuthDisabled} required />

        <div className="row">
          <div className="col-md-6 create-festival-start-date">
            <DateInput
              name="festival_start_date"
              label="Start Date"
              dateFormat="yyyy/MM/dd"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-6 create-festival-end-date">
            <DateInput
              name="festival_end_date"
              label="End Date"
              dateFormat="yyyy/MM/dd"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              disabled={submitAuthDisabled}
              required
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6 create-festival-start-time">
            <DateInput
              name="festival_start_time"
              label="Start Time"
              showTimeSelect
              showTimeSelectOnly
              dateFormat="h:mm aa"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-6 create-festival-end-time">
            <DateInput
              name="festival_end_time"
              label="End Time"
              showTimeSelect
              showTimeSelectOnly
              dateFormat="h:mm aa"
              disabled={submitAuthDisabled}
              required
            />
          </div>
        </div>

        <Textarea
          name="festival_description"
          label="Description"
          disabled={submitAuthDisabled}
          required
        />

        <div>
          <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
            Submit
          </button>
        </div>
      </Form>
    </div>
  );
};

export default CreateFestivalForm;
