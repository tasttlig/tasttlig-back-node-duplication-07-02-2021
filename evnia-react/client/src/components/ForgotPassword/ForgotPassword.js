// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Styling
import './ForgotPassword.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';

const ForgotPassword = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // To use the JWT credentials
  const authModalContext = useContext(AuthModalContext);

  // Mount Forgot Password page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Forgot Password page
  return (
    <div className="forgot-password forgot-password-background-image">
      <div className="mb-3 text-center">
        <Link exact="true" to="/">
          <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
        </Link>
      </div>

      <div className="forgot-password-content">
        <div className="forgot-password-title">Forgot Password?</div>

        <form onSubmit={authModalContext.handleSubmitForgotPassword} noValidate>
          <div className="mb-3">
            <input
              type="email"
              name="forgotPasswordEmail"
              placeholder="Email Address"
              value={authModalContext.state.forgotPasswordEmail}
              onChange={authModalContext.handleChange}
              disabled={authModalContext.state.submitAuthDisabled}
              className="email"
              required
            />
            <span className="far fa-envelope input-icon"></span>
            {authModalContext.state.forgotPasswordEmailError && (
              <div className="error-message">{authModalContext.state.forgotPasswordEmailError}</div>
            )}
          </div>

          <div className="mb-3">
            <button
              type="submit"
              disabled={authModalContext.state.submitAuthDisabled}
              className="forgot-password-btn"
            >
              Find Account
            </button>
          </div>
        </form>

        <div>
          <Link exact="true" to="/login" className="option log-in-switch">
            Login
          </Link>
          &nbsp;
          <Link exact="true" to="/sign-up" className="option sign-up-switch">
            Sign Up
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
