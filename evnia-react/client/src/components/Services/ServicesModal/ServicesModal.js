// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import './ServicesModal.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class ServicesModal extends Component {
  // Set the initial state
  state = {
    serviceType: '',
    description: '',
    submitAuthDisabled: false,
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // Handle user input change helper function
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate user input for services helper function
  validateServices = () => {
    let serviceTypeError = '';
    let descriptionError = '';

    // Render service type error message
    if (!this.state.serviceType) {
      serviceTypeError = 'Service type is required.';
    }

    // Render description error message
    if (!this.state.description) {
      descriptionError = 'Description is required.';
    }

    // Set validation error state
    if (serviceTypeError || descriptionError) {
      this.setState({ serviceTypeError, descriptionError });

      return false;
    }

    return true;
  };

  // Submit services form helper function
  handleSubmitServices = async (event) => {
    event.preventDefault();

    const isValid = this.validateServices();

    if (isValid) {
      // Set URL, depending on the service type
      const url =
        this.state.serviceType === 'Financial'
          ? '/financial-services'
          : this.state.serviceType === 'Marketing'
          ? '/marketing-services'
          : '/technology-services';

      // Get access token to identify current user
      const acc_token = localStorage.getItem('access_token');

      // Save current user to headers
      const headers = {
        Authorization: `Bearer ${acc_token}`,
      };

      // Service type data
      const data = {
        profile_img_url: this.context.state.user.profile_img_url,
        first_name: this.context.state.user.first_name,
        last_name: this.context.state.user.last_name,
        email: this.context.state.user.email,
        phone_number: this.context.state.user.phone_number,
        description: this.state.description,
      };

      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for submitting services form!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            serviceTypeError: '',
            descriptionError: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Services modal
  render = () => {
    const {
      serviceType,
      serviceTypeError,
      description,
      descriptionError,
      submitAuthDisabled,
    } = this.state;

    return (
      <div>
        <form onSubmit={this.handleSubmitServices} noValidate>
          <div className="form-group">
            <select
              name="serviceType"
              value={serviceType}
              onChange={this.handleChange}
              disabled={submitAuthDisabled}
              className="custom-select"
              required
            >
              <option value="">Select Service Type</option>
              <option value="Financial">Financial</option>
              <option value="Marketing">Marketing</option>
              <option value="Technology">Technology</option>
            </select>
            {serviceTypeError ? (
              <div className="error-message">Service type is required.</div>
            ) : null}
          </div>
          <div className="form-group">
            <textarea
              name="description"
              placeholder="Description"
              value={description}
              onChange={this.handleChange}
              disabled={submitAuthDisabled}
              className="services-input"
              required
            />
            <span className="fas fa-pencil-alt services-input-icon"></span>
            {descriptionError ? (
              <div className="error-message">Description is required.</div>
            ) : null}
          </div>

          <div>
            <button type="submit" disabled={submitAuthDisabled} className="services-submit-btn">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  };
}
