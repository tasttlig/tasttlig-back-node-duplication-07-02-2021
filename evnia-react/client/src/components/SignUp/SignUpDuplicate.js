// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Components
import { formatPhoneNumber } from '../Functions/Functions';

// Styling
import './SignUp.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';

const SignUp = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  const businessPreference = localStorage.getItem('business-preference');
  // console.log(businessPreference);

  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  console.log('source from authmodel', authModalContext);

  // Mount Sign Up page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Sign Up page
  return (
    <div className="row">
      <div className="col-lg-8 col-xl-6 px-0 sign-up-background-image-one" />
      <div className="col-lg-4 col-xl-4 px-0 sign-up-background">
        <div className="sign-up-content">
          <div className="mb-3 text-center">
            <Link exact="true" to="/">
              <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
            </Link>
          </div>
          <div className="mb-3 text-center"> Welcome! Let's get started...</div>

          {appContext.state.errorMessage && (
            <div className="mb-3 text-center invalid-message">{appContext.state.errorMessage}</div>
          )}

          <form onSubmit={authModalContext.handleSubmitSignUp} noValidate>
            <div className="mb-3">
              <input
                type="text"
                name="signUpFirstName"
                placeholder="First Name"
                value={authModalContext.state.signUpFirstName}
                onChange={authModalContext.handleChange}
                disabled={authModalContext.state.submitAuthDisabled}
                className="first-name"
                required
              />
              <span className="far fa-user input-icon" />
              {authModalContext.state.signUpFirstNameError && (
                <div className="error-message">{authModalContext.state.signUpFirstNameError}</div>
              )}
            </div>
            <div className="mb-3">
              <input
                type="text"
                name="signUpLastName"
                placeholder="Last Name"
                value={authModalContext.state.signUpLastName}
                onChange={authModalContext.handleChange}
                disabled={authModalContext.state.submitAuthDisabled}
                className="last-name"
                required
              />
              <span className="far fa-user input-icon" />
              {authModalContext.state.signUpLastNameError && (
                <div className="error-message">{authModalContext.state.signUpLastNameError}</div>
              )}
            </div>
            <div className="mb-3">
              <input
                type="email"
                name="signUpEmail"
                placeholder="Email Address"
                value={authModalContext.state.signUpEmail}
                onChange={authModalContext.handleChange}
                disabled={authModalContext.state.submitAuthDisabled}
                className="email"
                required
              />
              <span className="far fa-envelope input-icon" />
              {authModalContext.state.signUpEmailError && (
                <div className="error-message">{authModalContext.state.signUpEmailError}</div>
              )}
            </div>
            <div className="mb-3">
              <input
                type={authModalContext.state.signUpPasswordType}
                name="signUpPassword"
                placeholder="Password"
                value={authModalContext.state.signUpPassword}
                onChange={authModalContext.handleChange}
                disabled={authModalContext.state.submitAuthDisabled}
                className="password"
                required
              />
              <span onClick={authModalContext.handleClickSignUp} className="password-icon">
                {authModalContext.state.signUpPasswordType === 'password' ? (
                  <i className="far fa-eye-slash"></i>
                ) : (
                  <i className="far fa-eye"></i>
                )}
              </span>
              {authModalContext.state.signUpPasswordError && (
                <div className="error-message">{authModalContext.state.signUpPasswordError}</div>
              )}
              <div>
                <div className="password-less-char">
                  {authModalContext.state.signUpPasswordLessChar}
                </div>
                <div className="password-min-char">
                  {authModalContext.state.signUpPasswordMinChar}
                </div>
              </div>
            </div>
            <div className="mb-3">
              <input
                type="tel"
                name="signUpPhoneNumber"
                placeholder="Phone Number Ex: (999)-999-9999"
                value={formatPhoneNumber(authModalContext.state.signUpPhoneNumber)}
                onChange={authModalContext.handleChange}
                maxLength="14"
                disabled={authModalContext.state.submitAuthDisabled}
                className="phone-number"
                required
              />
              <span className="fas fa-phone-alt input-icon" />
              {authModalContext.state.signUpPhoneNumberError && (
                <div className="error-message">{authModalContext.state.signUpPhoneNumberError}</div>
              )}
            </div>

            <div className="mb-3">
              <button
                type="submit"
                disabled={authModalContext.state.submitAuthDisabled}
                className="sign-up-btn"
              >
                Sign Up
              </button>
            </div>
          </form>

          <div className="text-center">
            <span>Already have an account?</span>&nbsp;
            <Link exact="true" to="/login" className="option">
              Get started here
            </Link>
          </div>
        </div>
      </div>
      <div className="col-xl-2 px-0 sign-up-background-image-two" />
    </div>
  );
};

export default SignUp;
