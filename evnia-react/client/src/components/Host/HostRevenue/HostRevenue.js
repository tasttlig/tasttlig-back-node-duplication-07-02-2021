// Libraries
import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

import {formatDate} from '../../Functions/Functions';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer';
// import ExpiredFestivalCard from '../Festivals/FestivalCard/ExpiredFestivalCard.js';

// Styling
import './HostRevenue.scss';

const AllHostRevenue = (props) => {
 
  // Set initial state
  const [hostFestivals, setHostFestivals] = useState([]);
  const [hostTickets, setHostTickets] = useState([]);


  // To use the JWT credentials
  const appContext = useContext(AppContext);

  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  const fetchHostFestivals = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `host-festivals/${appContext.state.user.id}`,
        params: {
          user_id: appContext.state.user.id,
        },
      });

      setHostFestivals(response.data.details);
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  const fetchUserTickets = async () => {
    const response = await axios({
      method: 'GET',
      url: `/ticket-list`,
      
    });
    setHostTickets(response.data.ticket_list);
    return response;
  };

  const hostFestivalTickets = () => {
    let hostFestivalTicketArray = []

    if(hostTickets.length > 0 && hostFestivals.length > 0) {
      for(let festival of hostFestivals) {
        for(let ticket of hostTickets) {
          if(festival.festival_id === ticket.ticket_festival_id) {
            ticket["festival_name"] = festival.festival_name;
            ticket["festival_city"] = festival.festival_city;
            ticket["festival_type"] = festival.festival_type;
            hostFestivalTicketArray.push(ticket)
          }
        }
      }
    }
    return hostFestivalTicketArray
}

  const hostFestivalRevenue = (hostFestivalsTickets) => {
    let totalRevenue = 0;
    let revenueFestivalObject = {};
    let finalRevenueFestival = [];

    for(let festival of hostFestivalsTickets) {
     if(finalRevenueFestival.length === 0) {
        totalRevenue += Number(festival.ticket_price);
        revenueFestivalObject["festivalId"] = festival.ticket_festival_id;
        revenueFestivalObject["totalRevenue"] = totalRevenue;

        finalRevenueFestival.push(revenueFestivalObject)
     } else {

          for ( let revenueFestival = 0; revenueFestival < finalRevenueFestival.length; revenueFestival++) {

            if(finalRevenueFestival[revenueFestival].festivalId === festival.ticket_festival_id && festival.ticket_price !== null) {

              finalRevenueFestival[revenueFestival].totalRevenue = finalRevenueFestival[revenueFestival].totalRevenue + Number(festival.ticket_price);
            } else {

              totalRevenue += Number(festival.ticket_price);
              // revenueFestivalObject["festivalId"] = festival.ticket_festival_id;
              // revenueFestivalObject["totalRevenue"] = totalRevenue;
              // finalRevenueFestival.push(finalRevenueFestival[revenueFestival])
    
              finalRevenueFestival= {
                festivalId: festival.ticket_festival_id,
                totalRevenue: totalRevenue
              }
            }
            
          }

     }

    }

    return finalRevenueFestival;
  }

  const calculateTotalRevenue = (festivalPrices) => {
    let totalRevenue = 0;

    for(let festival of festivalPrices) {
      totalRevenue += Number(festival.ticket_price)
    }

    return totalRevenue.toFixed(2)

  }

  const totalCalculatedRevenue = calculateTotalRevenue(hostFestivalTickets());
 
  // Mount HostProfile page
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchHostFestivals();
    fetchUserTickets();

  }, []);

  // Render Host page
  return (
    <>
      <Nav />
        <div className="sponsor-container">
          <div className="all-host-applicants-navigation">
          <Link exact="true" to="/host-admin" className="all-host-applicants-navigation-content">
            <span>
              Host
            </span>
            </Link>
            <span className="arrow">&nbsp;&gt;&nbsp;</span>Revenue
          </div>
       
          {hostFestivalTickets().length ? (
                <div className="table-responsive mt-5">
                  <table className="table table-bordered">
                    <thead>
                      <tr>
                        <th>Festival Name</th>
                        <th>Festival Type</th>
                        <th>Festival Price</th>
                        <th>Festival City</th>
                      </tr>
                    </thead>
                    <tbody>
                      {hostFestivalTickets().map((a) => (
                        <tr key={a.festival_id}>
                          <td className="all-host-applicants-business-type">{a.festival_name}</td>
                          <td className="all-host-applicants-business-type">{a.festival_type}</td>
                          <td className="all-host-applicants-business-type">${a.ticket_price}</td>
                          <td className="all-host-applicants-business-type">{a.festival_city}</td>
                        </tr>
                      ))}
                    </tbody>
                    <tfoot>
                      <td><strong>Total Revenue: </strong></td>
                      <td>{totalCalculatedRevenue}</td>
                    </tfoot>
                  </table>
                </div>
              ) : (
                <div className="mt-5">
                  <strong>No revenue available.</strong>
                </div>
              )}
          </div>
      <Footer />
    </>
  );
};

export default AllHostRevenue;
