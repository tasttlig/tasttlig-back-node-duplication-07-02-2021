// Libraries
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';
import Footer from '../Home/BannerFooter/BannerFooter';

// Styling
import './Host.scss';
import festivals from '../../assets/images/festivals.jpg';
import experiences from '../../assets/images/experiences.jpg';
import parties from '../../assets/images/parties.jpg';

const Host = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Host page
  return (
    <div>
      <Nav />

      <div className="host">
        {/* <h1 className="host-title">Host</h1> */}
        <div className="row text-center">
          <div className="col-lg-4 host-content">
            <Link exact="true" to="/festival" className="host-section">
              <img src={festivals} alt="Festivals" className="host-image" />
              <span className="host-text">Festivals</span>
            </Link>
          </div>
          <div className="col-lg-4 host-content">
            <button className="host-coming-soon-content">
              <div className="host-section">
                <div>
                  <img src={experiences} alt="Experiences" className="host-image" />
                  <span className="host-text host-experiences">Experiences</span>
                  <span className="host-text host-coming-soon">Coming Soon!</span>
                </div>
              </div>
            </button>
          </div>
          <div className="col-lg-4 host-content">
            <button className="host-coming-soon-content">
              <div className="host-section">
                <div>
                  <img src={parties} alt="Parties" className="host-image" />
                  <span className="host-text host-parties">Parties</span>
                  <span className="host-text host-coming-soon">Coming Soon!</span>
                </div>
              </div>
            </button>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default Host;
