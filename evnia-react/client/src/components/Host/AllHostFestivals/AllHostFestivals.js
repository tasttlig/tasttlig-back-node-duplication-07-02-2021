// Libraries
import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

import {formatDate} from '../../Functions/Functions';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer';
// import ExpiredFestivalCard from '../Festivals/FestivalCard/ExpiredFestivalCard.js';

// Styling
import './AllHostFestivals.scss';

const AllHostFestivals = (props) => {
 
  // Set initial state
  const [hostFestivals, setHostFestivals] = useState([]);


  // To use the JWT credentials
  const appContext = useContext(AppContext);

  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  const fetchHostFestivals = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `host-festivals/${appContext.state.user.id}`,
        params: {
          user_id: appContext.state.user.id,
        },
      });

      setHostFestivals(response.data.details);
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  // Mount HostProfile page
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchHostFestivals();

  }, []);

  // Render Host page
  return (
    <>
      <Nav />
        <div className="sponsor-container">
          <div className="all-host-applicants-navigation">
          <Link exact="true" to="/host-admin" className="all-host-applicants-navigation-content">
            <span>
              Host
            </span>
            </Link>
            <span className="arrow">&nbsp;&gt;&nbsp;</span>All Festivals
          </div>

          {hostFestivals && hostFestivals.length ? (
                <div className="table-responsive mt-5">
                  <table className="table table-bordered">
                    <thead>
                      <tr>
                        <th>Festival Name</th>
                        <th>Festival Type</th>
                        <th>Festival Start Date</th>
                        <th>Festival End Date</th>
                        <th>Festival Price</th>
                        <th>Festival City</th>
                      </tr>
                    </thead>
                    <tbody>
                      {hostFestivals.map((a) => (
                        <tr key={a.festival_id}>
                          <td className="all-host-applicants-business-type">{a.festival_name} {a.last_name}</td>
                          <td className="all-host-applicants-business-type">{a.festival_type}</td>
                          <td className="all-host-applicants-business-type">{formatDate(a.festival_start_date)}</td>
                          <td className="all-host-applicants-business-type">{formatDate(a.festival_end_date)}</td>
                          <td className="all-host-applicants-business-type">${a.festival_price}</td>
                          <td className="all-host-applicants-business-type">{a.festival_city}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              ) : (
                <div className="mt-5">
                  <strong>No applications available.</strong>
                </div>
              )}
          </div>
      <Footer />
    </>
  );
};

export default AllHostFestivals;
