// Libraries
import React, { Component } from 'react';

// Components
import Nav from '../Navbar/Nav';

// Styling
import './ResourcesPublisher.scss';

export default class ResourcesPublisher extends Component {
  // Set initial state
  state = {
    load: false,
    profileImage: this.props.location.state.profileImage,
    verified: this.props.location.state.verified,
    firstName: this.props.location.state.firstName,
    lastName: this.props.location.state.lastName,
    phoneNumber: this.props.location.state.phoneNumber,
    email: this.props.location.state.email,
  };

  // Load image helper function
  onLoad = () => {
    this.setState({ load: true });
  };

  // Render resources publisher page
  render = () => {
    const { load, profileImage, verified, firstName, lastName, phoneNumber, email } = this.state;

    return (
      <div>
        <Nav />

        {/* Commercial Publisher Header section */}
        <div className="publisher">
          <div className="row publisher-header">
            <div className="col-md-5 publisher-header-picture">
              <div className="row publisher-photo-section">
                {profileImage ? (
                  <img
                    src={profileImage}
                    alt={`${firstName} ${lastName}`}
                    onLoad={this.onLoad}
                    className={load ? 'publisher-photo' : 'loading-image'}
                  />
                ) : (
                  <span className="fas fa-user-circle fa-7x default-publisher-photo"></span>
                )}
              </div>
              <div>
                {verified ? (
                  <div className="has-this-item">
                    <span className="far fa-check-circle check"></span> Email Verified
                  </div>
                ) : null}
              </div>
            </div>
            <div className="col-md-7 publisher-header-information">
              <div className="publisher-name">{`${firstName} ${lastName}`}</div>
              <div className="publisher-phone">
                <span className="fas fa-phone-alt publisher-phone-icon"></span>
                <a href={`tel:${phoneNumber}`} className="external-link">
                  {phoneNumber}
                </a>
              </div>
              <div className="publisher-email">
                <span className="far fa-envelope publisher-email-icon"></span>
                <a href={`mailto:${email}`} className="external-link">
                  {email}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
}
