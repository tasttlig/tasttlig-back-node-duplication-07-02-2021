// Libraries
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

// Components
import Footer from '../Home/BannerFooter/BannerFooter';

// Styling
import './ApplyToList.scss';
import '../Home/HowItWorks/HowItWorks.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import applyToListBanner from '../../assets/images/apply-to-list-banner.jpg';
import profile from '../../assets/images/profile.png';
import businessType from '../../assets/images/business-type.png';
import submitBusinessInformation from '../../assets/images/submit-business-information.png';
import products from '../../assets/images/products.png';
import services from '../../assets/images/services.png';
import describe from '../../assets/images/describe.png';
import price from '../../assets/images/price.png';

const ApplyToList = () => {
  // Mount Apply to List page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Apply to List page
  return (
    <div>
      <div className="navbar">
        <Link exact="true" to="/">
          <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
        </Link>
        <Link to="/apply" className="apply-to-list-navbar-btn">
          Get Started
        </Link>
      </div>

      <div className="row apply-to-list-header">
        <div className="col-lg-8 apply-to-list-header-image-content">
          <img src={applyToListBanner} alt="Apply to List" className="apply-to-list-header-image" />
        </div>
        <div className="col-lg-4 apply-to-list-header-text-content">
          <div className="apply-to-list-header-text">
            Attract more customers, promote your business on Tasttlig.
          </div>
          <Link to="/apply" className="apply-to-list-header-btn">
            Get Started
          </Link>
        </div>
      </div>

      <div className="text-center apply-to-list-body">
        <div className="apply-to-list-body-title">Apply to List</div>

        <div className="apply-to-list-body-section">
          <div className="apply-to-list-body-timeline"></div>

          <div className="apply-to-list-body-sub-title">Apply to List</div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <img src={profile} alt="Create an account" />
              <span className="apply-to-list-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <div>Create an account</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <div>Select your business type</div>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <span className="apply-to-list-body-timeline-circle-left"></span>
              <img src={businessType} alt="Select your business type" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <img src={submitBusinessInformation} alt="Submit business information" />
              <span className="apply-to-list-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <div>Submit business information</div>
            </div>
          </div>

          <div className="apply-to-list-body-sub-title">Products</div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <img src={products} alt="Add your products" />
              <span className="apply-to-list-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <div>Add your products (limit of 3)</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <div>Describe your products</div>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <span className="apply-to-list-body-timeline-circle-left"></span>
              <img src={describe} alt="Describe your products" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <img src={price} alt="Price your products" />
              <span className="apply-to-list-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <div>Price your products</div>
            </div>
          </div>

          <div className="apply-to-list-body-sub-title">Services</div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <img src={services} alt="Add your services" />
              <span className="apply-to-list-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <div>Add your services (limit of 3)</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <div>Describe your services</div>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <span className="apply-to-list-body-timeline-circle-left"></span>
              <img src={describe} alt="Describe your services" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-list-body-text">
              <img src={price} alt="Price your services" />
              <span className="apply-to-list-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-list-body-text">
              <div>Price your services</div>
            </div>
          </div>

          <Link exact="true" to="/apply" className="apply-to-list-body-btn">
            Get Started
          </Link>
        </div>

        {/* Responsive Design */}
        <div className="apply-to-list-body-section-responsive">
          <div className="apply-to-list-body-sub-title-responsive">Apply to List</div>

          <div className="mb-4">
            <img src={profile} alt="Create an account" />
            <div className="apply-to-list-body-text-responsive">Create an account</div>
          </div>

          <div className="mb-4">
            <img src={businessType} alt="Select your business type" />
            <div className="apply-to-list-body-text-responsive">Select your business type</div>
          </div>

          <div className="mb-4">
            <img src={submitBusinessInformation} alt="Submit business information" />
            <div className="apply-to-list-body-text-responsive">Submit business information</div>
          </div>

          <div className="apply-to-list-body-sub-title-responsive">Products</div>

          <div className="mb-4">
            <img src={products} alt="Add your products" />
            <div className="apply-to-list-body-text-responsive">Add your products (limit of 3)</div>
          </div>

          <div className="mb-4">
            <img src={describe} alt="Describe your products" />
            <div className="apply-to-list-body-text-responsive">Describe your products</div>
          </div>

          <div className="mb-4">
            <img src={price} alt="Price your products" />
            <div className="apply-to-list-body-text-responsive">Price your products</div>
          </div>

          <div className="apply-to-list-body-sub-title-responsive">Services</div>

          <div className="mb-4">
            <img src={services} alt="Add your services" />
            <div className="apply-to-list-body-text-responsive">Add your services (limit of 3)</div>
          </div>

          <div className="mb-4">
            <img src={describe} alt="Describe your services" />
            <div className="apply-to-list-body-text-responsive">Describe your services</div>
          </div>

          <div className="mb-4">
            <img src={price} alt="Price your services" />
            <div className="apply-to-list-body-text-responsive">Price your services</div>
          </div>

          <Link exact="true" to="/apply" className="apply-to-list-body-btn-responsive">
            Get Started
          </Link>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default ApplyToList;
