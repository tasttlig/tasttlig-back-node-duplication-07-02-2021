// Libraries
import React, { useState, useContext } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import Flag from 'react-world-flags';
import * as shoppingCartActions from '../../../redux/shoppingCart/actions';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';
import { formatDate, formatMilitaryToStandardTime } from '../../Functions/Functions';

// Styling
import './ExperiencesCard.scss';

const ExperiencesCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [experienceDetailsOpened, setExperienceDetailsOpened] = useState(false);
  const [contactOpened, setContactOpened] = useState(false);
  const [itemInCart, setItemInCart] = useState(false);
  const [reduxItem, setReduxItem] = useState(false);
  const [isFullPageView] = useState(props.isFullPageView);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;

  // Shopping cart state management
  let cartItemFound = false;
  props.cartItems.map((item) => {
    if (
      item &&
      item.itemType === 'experiences' &&
      item.itemId === props.experiencesCard.experience_id
    ) {
      cartItemFound = true;
      if (!itemInCart) {
        setItemInCart(true);
      }
      if (reduxItem !== item) {
        setReduxItem(item);
      }
    }
  });
  if (!cartItemFound && itemInCart) {
    setItemInCart(false);
  }

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'experience-details') {
      setExperienceDetailsOpened(true);
    } else if (modalType === 'contact') {
      setContactOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'experience-details') {
      setExperienceDetailsOpened(false);
    } else if (modalType === 'contact') {
      setContactOpened(false);
    }
  };

  // Render Experiences Card
  const {
    experience_id,
    image_urls,
    title,
    business_name,
    price,
    address,
    city,
    state,
    postal_code,
    start_date,
    end_date,
    start_time,
    end_time,
    capacity,
    description,
    phone_number,
    email,
    alpha_2_code,
    nationality,
  } = props.experiencesCard;

  // Contact modal
  const contactLink = (
    <div>
      <Modal
        isOpen={contactOpened}
        onRequestClose={closeModal('contact')}
        ariaHideApp={false}
        className="contact-modal"
      >
        <div className="text-right">
          <button
            aria-label="Close Contact Information Details Modal"
            onClick={closeModal('contact')}
            className="fas fa-times fa-2x close-modal"
          ></button>
        </div>

        <div className="modal-title">{`Contact Information on ${title}`}</div>

        <div>
          Phone:{' '}
          <a href={`tel:${phone_number}`} className="external-link">
            {phone_number}
          </a>
        </div>
        <div>
          Email:{' '}
          <a href={`mailto:${email}`} className="external-link">
            {email}
          </a>
        </div>
      </Modal>

      <div
        onClick={openModal('contact')}
        className="btn btn-secondary experience-details-contact-btn"
      >
        Contact
      </div>
    </div>
  );

  return (
    <div
      className={`${
        window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/` ||
        window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/` ||
        window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/`
          ? 'col-md-6 col-lg-4 col-xl-3'
          : 'col-md-6 col-xl-4'
      } experience-card`}
    >
      <LazyLoad once>
        {/* Experiences Details Modal */}
        <Modal
          isOpen={experienceDetailsOpened}
          onRequestClose={closeModal('experience-details')}
          ariaHideApp={false}
          className="experience-details-modal"
        >
          <div className="text-right">
            <button
              aria-label={`Close ${title} Details Modal`}
              onClick={closeModal('experience-details')}
              className="fas fa-times fa-2x close-modal"
            ></button>
          </div>

          <div className="experience-details-title">{title}</div>

          <div className="mb-3 experience-details-image">
            {image_urls.length > 1 ? (
              <ImageSlider images={image_urls} />
            ) : (
              <img src={image_urls[0]} alt={title} />
            )}
          </div>

          <div className="mb-3 product-text-default">
            <div className="pb-2">
              <div className="product-text-sub-title">Address</div>
              <div>{`${address}, ${city}, ${state} ${postal_code}`}</div>
            </div>
            <div className="pb-2">
              <div className="product-text-sub-title">Date</div>
              <div>{`${start_date && formatDate(start_date)} to ${
                end_date && formatDate(end_date)
              }`}</div>
            </div>
            <div className="pb-2">
              <div className="product-text-sub-title">Time</div>
              <div>
                {`${start_time && formatMilitaryToStandardTime(start_time)} to ${
                  end_time && formatMilitaryToStandardTime(end_time)
                }`}
              </div>
            </div>
            <div className="pb-2">
              <div className="product-text-sub-title">Capacity</div>
              <div>{`${capacity}`}</div>
            </div>
            <div className="experience-description">{description}</div>
          </div>
          <div className="row">
            <div className="col-md-6 p-0 text-left">{contactLink}</div>
            <div className="col-md-6 p-0 text-left">
              <div>
                <Link
                  to={`/payment/experience/${experience_id}`}
                  className="btn btn-primary experience-details-contact-btn"
                >
                  {`$${price} Buy Now`}
                </Link>
              </div>
            </div>
          </div>

          <div className="close-modal-bottom-content">
            <span onClick={closeModal('experience-details')} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

        <div className="d-flex flex-column h-100 card-box">
          <div className="d-flex flex-column h-100 text-center">
            <Link
              exact="true"
              to={`/experiences/${experience_id}`}
              className="experience-details-link"
            >
              <img
                src={image_urls[0]}
                alt={title}
                onLoad={() => setLoad(true)}
                className={load ? 'experience-card-image' : 'loading-image'}
              />
              <div className="passport-card-flag-container">
                <Flag code={alpha_2_code} className="passport-card-flag" />
                <div className="passport-card-flag-country">{nationality}</div>
              </div>
              <div className="experience-card-title">{title}</div>
            </Link>
            <div className="experience-card-business-name">
              {business_name ? business_name : ''}
            </div>
            {isFullPageView ? null : userRole &&
              (userRole.includes('HOST') ||
                userRole.includes('HOST_PENDING') ||
                userRole.includes('ADMIN')) ? (
              <div className="mt-auto btn btn-dark call-to-action-btn">
                {`$${price}`}&nbsp;Buy Now
              </div>
            ) : !appContext.state.user.id ? (
              <div onClick={authModalContext.openModal('log-in')} className="buy-now-btn">
                {`$${price}`}&nbsp;Buy Now
              </div>
            ) : (
              <div>
                <Link to={`/payment/experience/${experience_id}`} className="buy-now-btn">
                  {`$${price}`}&nbsp;Buy Now
                </Link>
              </div>
            )}
          </div>

          {/* {itemInCart ? (
            <div className="row">
              <div className="col-sm-6 row pt-2 pb-2 pl-0 pr-0 text-center">
                <div className="col p-0">
                  <div
                    className= {reduxItem.quantity > 1
                      ? "border border-dark bg-dark text-white p-2 fas fa-minus"
                      : "border border-dark bg-light p-2 fas fa-minus"}
                    onClick={() => reduxItem.quantity > 1
                      ? props.subtractQuantity(
                        reduxItem.itemType,
                        reduxItem.itemId)
                      : null}
                  />
                </div>
                <div className="col p-1">
                  {reduxItem.quantity}
                </div>
                <div className="col p-0">
                  <div
                    className="border border-dark bg-dark text-white p-2 fas fa-plus"
                    onClick={() => props.addQuantity(
                      reduxItem.itemType,
                      reduxItem.itemId)}
                  />
                </div>
              </div>
              <div
                className="col-sm-6 add-to-cart-btn"
                onClick={() => props.alternateCartVisibility()}
              >
                <span className="fas fa-shopping-basket"/>
                &nbsp;Cart
              </div>
            </div>
          ) : (
            <div className="row">
              <div
                onClick={() => {
                  props.addToCart(
                    props.experiencesCard.experience_id,
                    props.experiencesCard.image_urls[0],
                    props.experiencesCard.title,
                    props.experiencesCard.price);
                }}
                className="col-md-12 add-to-cart-btn"
              >
                <span className="fas fa-cart-plus"/>
                &nbsp;Add
              </div>
            </div>
          )} */}
        </div>
      </LazyLoad>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartItems,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (experience_id, imageUrl, title, price) => {
      dispatch(shoppingCartActions.addToCart('experiences', experience_id, imageUrl, title, price));
    },
    addQuantity: (type, id) => {
      dispatch(shoppingCartActions.addQuantity(type, id));
    },
    subtractQuantity: (type, id) => {
      dispatch(shoppingCartActions.subtractQuantity(type, id));
    },
    alternateCartVisibility: () => {
      dispatch(shoppingCartActions.alternateCartVisibility());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExperiencesCard);
