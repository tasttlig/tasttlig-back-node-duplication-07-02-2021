// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import useInfiniteScroll from 'react-infinite-scroll-hook';

// Components
import Nav from '../Navbar/Nav';
import SearchBar from '../Navbar/SearchBar';
import MenuItemCard from '../Discover/MenuItemCard';

// Styling
import '../Dashboard/Dashboard.scss';

const YourMenuItems = (props) => {
  // Set initial state
  const [menuItems, setMenuItems] = useState([]);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Fetch user menu items created helper function
  const fetchMenuItems = async () => {
    try {
      const url = '/menu-item/user/all';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Load next set of user menu items helper functions
  const loadNextPage = async (page) => {
    const url = '/menu-item/user/all?';
    const acc_token = localStorage.getItem('access_token');
    const headers = { Authorization: `Bearer ${acc_token}` };

    return axios({
      method: 'GET',
      url,
      headers,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
      },
    });
  };

  const handleLoadMore = (page = currentPage, menuItem = menuItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);
      setMenuItems(menuItem.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Your Current Menu Items page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchMenuItems().then(({ data }) => {
      setMenuItems(data.details.data);
    });
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [props.location.state.keyword]);

  // Render empty page
  const Empty = () => {
    return <strong className="no-food-samples-found">No menu items found.</strong>;
  };

  // Render search bar
  const Filters = () => (
    <div className="page-filters">
      <section className="mb-4">
        <SearchBar keyword={props.location.state.keyword} url="/my-menu-items" />
      </section>
      <hr />
    </div>
  );

  // Render Your Current Menu Items page
  return (
    <div>
      <Nav />

      <div className="your-current-menu-items">
        <div className="your-current-menu-items-title">Your Menu Items</div>
        <div className="row">
          <div className="col-md-3 p-0">
            <Filters />
          </div>
          <div className="col-md-9 p-0">
            <div className="menu-item-cards" ref={infiniteRef}>
              {menuItems && menuItems.length !== 0 ? (
                menuItems.map((menuItem) => (
                  <MenuItemCard key={menuItem.menu_item_id} {...menuItem}>
                    <div className="mt-auto">
                      <Link
                        exact="true"
                        to={`/convert-menu-to-sample/${menuItem.menu_item_id}`}
                        className="edit-btn"
                      >
                        Create Food Sample
                      </Link>
                    </div>
                  </MenuItemCard>
                ))
              ) : (
                <Empty />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default YourMenuItems;
