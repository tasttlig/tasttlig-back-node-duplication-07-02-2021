// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

// Components
import Nav from '../../Navbar/Nav';
import CreateFoodSampleForm from '../../CreateFoodSample/CreateFoodSampleForm/CreateFoodSampleForm';

const ConvertMenuToSample = (props) => {
  // Set initial state
  const menuItemId = props.match.params.menuItemId;
  const [menuItem, setMenuItem] = useState();

  // Food sample data from menu item helper function
  const foodSample = () => {
    const dietaryRestrictions = [];
    const daysAvailable = [];

    // Split address into address lines 1 and 2
    let addressParts = menuItem.address.split(', ');
    let addressLine1 = addressParts[0];
    let addressLine2 = addressParts[1];

    menuItem.is_vegetarian && dietaryRestrictions.push('vegetarian');
    menuItem.is_vegan && dietaryRestrictions.push('vegan');
    menuItem.is_gluten_free && dietaryRestrictions.push('glutenFree');
    menuItem.is_halal && dietaryRestrictions.push('halal');
    menuItem.is_available_on_monday && daysAvailable.push('available_on_monday');
    menuItem.is_available_on_tuesday && daysAvailable.push('available_on_tuesday');
    menuItem.is_available_on_wednesday && daysAvailable.push('available_on_wednesday');
    menuItem.is_available_on_thursday && daysAvailable.push('available_on_thursday');
    menuItem.is_available_on_friday && daysAvailable.push('available_on_friday');
    menuItem.is_available_on_saturday && daysAvailable.push('available_on_saturday');
    menuItem.is_available_on_sunday && daysAvailable.push('available_on_sunday');

    return {
      ...menuItem,
      images: menuItem.image_urls,
      addressLine1,
      addressLine2,
      provinceTerritory: menuItem.state,
      end_time: null,
      quantity: parseInt(menuItem.quantity),
      sample_size: menuItem.size,
      dietaryRestrictions,
      daysAvailable,
    };
  };

  // Menu item to food sample success helper function
  const onCreated = () => {
    setTimeout(() => {
      window.location.href = '/my-menu-items';
    }, 2000);

    toast(`Success! Food sample is created!`, {
      type: 'success',
      autoClose: 2000,
    });
  };

  // Mount Convert Menu Item to Food Sample page
  useEffect(() => {
    window.scrollTo(0, 0);

    const fetchMenuItem = async () => {
      const response = await axios.get(`/menu_items/${menuItemId}`);
      setMenuItem(response.data.menuItem);
    };

    fetchMenuItem();
  }, []);

  // Render Convert Menu Item to Food Sample page
  return (
    <div>
      <Nav />
      {menuItem && <CreateFoodSampleForm foodSample={foodSample()} onCreated={onCreated} />}
    </div>
  );
};

export default ConvertMenuToSample;
