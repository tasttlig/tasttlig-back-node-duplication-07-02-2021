// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Styling
import './Login.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';

const Login = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);


  // Mount Login page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Login page
  return (
    <div className="row">
      <div className="col-lg-8 col-xl-6 px-0 login-background-image-one" />
      <div className="col-lg-4 col-xl-4 px-0 login-background">
        <div className="login-content">
          <div className="mb-3 text-center">
            <Link exact="true" to="/">
              <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
            </Link>
          </div>

          {appContext.state.errorMessage && (
            <div className="mb-3 text-center invalid-message">{appContext.state.errorMessage}</div>
          )}

          <form onSubmit={authModalContext.handleSubmitLogIn} noValidate>
            <div className="mb-3">
              <input
                type="email"
                name="logInEmail"
                placeholder="Email Address"
                value={authModalContext.state.logInEmail}
                onChange={authModalContext.handleChange}
                disabled={authModalContext.state.submitAuthDisabled}
                className="email"
                required
              />
              <span className="far fa-envelope input-icon"></span>
              {authModalContext.state.logInEmailError && (
                <div className="error-message">{authModalContext.state.logInEmailError}</div>
              )}
            </div>
            <div className="mb-3">
              <input
                type={authModalContext.state.logInPasswordType}
                name="logInPassword"
                placeholder="Password"
                value={authModalContext.state.logInPassword}
                onChange={authModalContext.handleChange}
                disabled={authModalContext.state.submitAuthDisabled}
                className="password"
                required
              />
              <span onClick={authModalContext.handleClickLogIn} className="password-icon">
                {authModalContext.state.logInPasswordType === 'password' ? (
                  <i className="far fa-eye-slash"></i>
                ) : (
                  <i className="far fa-eye"></i>
                )}
              </span>
              {authModalContext.state.logInPasswordError && (
                <div className="error-message">{authModalContext.state.logInPasswordError}</div>
              )}
            </div>

            <div className="mb-3 text-right">
              <Link exact="true" to="/forgot-password" className="forgot-password-link">
                Forgot Password?
              </Link>
            </div>

            <div className="mb-3">
              <button
                type="submit"
                disabled={authModalContext.state.submitAuthDisabled}
                className="log-in-btn"
                onClick = {() => localStorage.setItem('verificationStatusOnLogin', "SignedIn")}
              >
                Sign in
              </button>
            </div>
          </form>

          <div className="text-center">
            <span>Are you New?</span>&nbsp;
            <Link exact="true" to="/sign-up" className="option">
              Create an Account
            </Link>
          </div>
        </div>
      </div>
      <div className="col-xl-2 px-0 login-background-image-two" />
    </div>
  );
};

export default Login;
