// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import comingSoonWelcome from '../../../assets/images/coming-soon-welcome.png';

const Welcome = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  return (
    <div className="coming-soon-page-welcome">
      <div className="coming-soon-page-sub-title">
        Starting from Restaurants, whatever your business is, welcome.
      </div>
      <div>
        <img src={comingSoonWelcome} alt="Coming Soon Welcome" className="coming-soon-page-image" />
      </div>
      {!appContext.state.user.id && (
        <div>
          <Link exact="true" to="/sign-up" className="coming-soon-page-sign-up-btn">
            Sign Up
          </Link>
        </div>
      )}
    </div>
  );
};

export default Welcome;
