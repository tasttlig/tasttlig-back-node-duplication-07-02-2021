// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import comingSoonVisitors from '../../../assets/images/coming-soon-visitors.png';

const Visitors = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  return (
    <div className="coming-soon-page-visitors">
      <div className="coming-soon-page-sub-title">
        We have over 21,000 visitors a month to bring to your business.
      </div>
      <div>
        <img
          src={comingSoonVisitors}
          alt="Coming Soon Visitors"
          className="coming-soon-page-image"
        />
      </div>
      {!appContext.state.user.id && (
        <div>
          <Link exact="true" to="/sign-up" className="coming-soon-page-sign-up-btn">
            Sign Up Now
          </Link>
        </div>
      )}
    </div>
  );
};

export default Visitors;
