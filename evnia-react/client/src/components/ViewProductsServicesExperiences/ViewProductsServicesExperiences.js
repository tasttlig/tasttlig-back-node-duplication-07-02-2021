import React, { useState, useEffect, useContext, Fragment } from 'react';
import { AppContext } from '../../ContextProvider/AppProvider';
import axios from 'axios';
import Select from 'react-select';
import { useHistory } from 'react-router-dom';
import FoodSamplesCreatedCard from '../Dashboard/FoodSamplesCreated/FoodSamplesCreatedCard/FoodSamplesCreatedCard';
import Radio from '../Shared/Radio';

import './ViewProductsServicesExperiences.scss';
import { toast } from 'react-toastify';

const ViewProductsServicesExperiences = (props) => {
  //console.log('prps from ViewProductsServicesExperiences', props);
  const [toggleState, setToggleState] = useState(1);
  const [toggleFoodFilterState, setToggleFoodFilterState] = useState('food');
  const [foodSamplesInFestival, setFoodSamplesInFestival] = useState([]);
  const [servicesInFestival, setServicesInFestival] = useState([]);
  const [experiencesInFestival, setExperiencesInFestival] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filterPrice, setFilterPrice] = useState('none');
  const [filterQuantity, setFilterQuantity] = useState('none');
  const [filterSize, setFilterSize] = useState('none');
  const [filterSearch, setFilterSearch] = useState('');
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [foodSampleSelected, setFoodSampleSelected] = useState([]);
  const [serviceSelected, setServiceSelected] = useState([]);
  const [experienceSelected, setExperienceSelected] = useState([]);
  const [foodNonFoodFilter, setFoodNonFoodFilter] = useState('all');
  const [paidNonPaidFilter, setPaidNonPaidFilter] = useState('all');

  const toggleTab = (index) => {
    setToggleState(index);
  };

  const toggleFoodFilterTab = (index) => {
    setToggleFoodFilterState(index);
  };

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  const history = useHistory();
  const products = [
    {
      name: 'product 1',
      price: '$14.99',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tellus sapien, sollicitudin nec ',
    },
    {
      name: 'product 2',
      price: '$30.00',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tellus sapien, sollicitudin nec ',
    },
    {
      name: 'product 3',
      price: '$29.99',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tellus sapien, sollicitudin nec ',
    },
  ];

  const fetchUserFoodSamples = async () => {
    //console.log('fetch route', filterSearch);
        
    const response = await axios({
      method: 'GET',
      url: '/products/user/:user_id',
      params: {
        user_id: appContext.state.user.id,
        keyword: filterSearch,
        // festival: props.festivalId,
      },
      
    });
    setFoodSamplesInFestival(response.data.details);
    // console.log('products fetched', response.data.details);
    return response;
  };

  
  const fetchUserServices = async () => {
    //console.log('fetch route', filterSearch);
    const response = await axios({
      method: 'GET',
      url: `/services/details/${appContext.state.user.id}`,
      params: {
        user_id: appContext.state.user.id,
        keyword: filterSearch,
        festival: props.festivalId,
      },
    });
    console.log("services", response.data);
    setServicesInFestival(response.data.details);
    return response;
  };
  const fetchUserExperiences = async () => {
    //console.log('fetch route', filterSearch);
    const response = await axios({
      method: 'GET',
      url: `/experiences/${appContext.state.user.id}`,
      params: {
        user_id: appContext.state.user.id,
        keyword: filterSearch,
        festival: props.festivalId,
      },
    });
    console.log('exp', response);
    setExperiencesInFestival(response.data.details);
    return response;
  };

  const handleAddToFestival = async (event) => {
    //console.log(foodSampleSelected);
    //console.log(serviceSelected);
    //console.log(experienceSelected);
    if (foodSampleSelected && foodSampleSelected.length > 0) {
      try {
        const response = await axios({
          method: 'POST',
          url: `/products/festival/:festivalId`,
          data: {
            festivalId: props.festivalId,
            ps: foodSampleSelected,
          },
        });
        //console.log(response);
        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);
          toast('Success! Added products to festival!', {
            type: 'success',
            autoClose: 2000,
          });
        } else {
          toast('Error! Unable to insert products into festival!', {
            type: 'error',
            autoClose: 2000,
          });
        }
      } catch (error) {
        //console.log(error);
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    } else if (serviceSelected && serviceSelected.length > 0) {
      try {
        const response = await axios({
          method: 'POST',
          url: `/services/festival/:festivalId`,
          data: {
            festivalId: props.festivalId,
            ps: serviceSelected,
          },
        });
        //console.log(response);
        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);
          toast('Success! Added service to festival!', {
            type: 'success',
            autoClose: 2000,
          });
        } else {
          toast('Error! Unable to insert service into festival!', {
            type: 'error',
            autoClose: 2000,
          });
        }
      } catch (error) {
        //console.log(error);
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    } else if (experienceSelected && experienceSelected.length > 0) {
      try {
        const response = await axios({
          method: 'POST',
          url: `/experiences/festival/:festivalId`,
          data: {
            festivalId: props.festivalId,
            ps: experienceSelected,
          },
        });
        //console.log(response);
        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);
          toast('Success! Added experience to festival!', {
            type: 'success',
            autoClose: 2000,
          });
        } else {
          toast('Error! Unable to insert experience into festival!', {
            type: 'error',
            autoClose: 2000,
          });
        }
      } catch (error) {
        //onsole.log(error);
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };
  // const fetchFoodSamplesInFestival = async () => {
  //   try {
  //     const response = await axios({
  //       method: 'GET',
  //       url: `/food-sample/festival/${props.festivalId}`,
  //       params: {
  //         price: filterPrice,
  //         quantity: filterQuantity,
  //         size: filterSize,
  //         keyword: filterSearch,
  //       },
  //     });

  //     setFoodSamplesInFestival(response.data.details);
  //   } catch (error) {
  //     console.log(error);
  //     return error.response;
  //   }
  // };
  //console.log('itsems from the product tble::', foodSamplesInFestival);
  // Get all fetches helper function
  const getAllFetches = async () => {
    await fetchUserFoodSamples();
    await fetchUserServices();
    await fetchUserExperiences();
    setLoading(false);
  };

  // Mount Festival Details page
  useEffect(() => {
    window.scrollTo(0, 0);
    getAllFetches();
  }, []);

  useEffect(() => {
    getAllFetches();
  }, [filterSearch]);

  return (
    <Fragment>
      <div className="row view-tab-heading">
        <div className="col tab-row">
          <div className="tab-switcher">
            <button
              onClick={() => {
                toggleTab(1);
                setServiceSelected([]);
                setExperienceSelected([]);
              }}
              className={toggleState === 1 ? 'tab active-tab' : 'tab'}
            >
              Products
            </button>
            <button
              onClick={() => {
                toggleTab(2);
                setFoodSampleSelected([]);
                setExperienceSelected([]);
              }}
              className={toggleState === 2 ? 'tab active-tab' : 'tab'}
            >
              Services
            </button>
            <button
              onClick={() => {
                toggleTab(3);
                setFoodSampleSelected([]);
                setServiceSelected([]);
              }}
              className={toggleState === 3 ? 'tab active-tab' : 'tab'}
            >
              Experiences
            </button>
          </div>
        </div>
      </div>
      <div className="row view-tab-heading">
        <div className="col search-input">
          <input
            placeholder="Search"
            name="card_search"
            label="Search for card"
            type="text"
            value={keyword}
            onChange={(event) => {
              setKeyword(event.target.value);
            }}
            onKeyDown={(event) => {
              if (event.key === 'Enter') {
                event.preventDefault();
                setFilterSearch(keyword);
              }
            }}
            className="col"
            placeholder="Search"
          ></input>
        </div>
        <div className="col add-to-festival-button">
          <button onClick={handleAddToFestival}>Add to Festival</button>
        </div>
      </div>
      {/* <div className="row view-tab-heading radio-filter-row">
        <div className="col food-toggle-col">
          <div className="food-non-food-filters">
            <button
              onClick={() => toggleFoodFilterTab('food')}
              className={toggleFoodFilterState === 'food' ? 'tab active-tab' : 'tab'}
            >
              Food
            </button>
            <button
              onClick={() => toggleFoodFilterTab('nonFood')}
              className={toggleFoodFilterState === 'nonFood' ? 'tab active-tab' : 'tab'}
            >
              Non-Food
            </button>
          </div>
        </div>
      </div> */}
      <div className="row">
        <div className="col">
          <div className="filter-subtitle">
            <strong>Paid/Non-Paid Filter</strong>
          </div>
          <div className="radio-filters">
            <Radio
              value="all"
              selected={paidNonPaidFilter}
              text="All"
              onChange={setPaidNonPaidFilter}
            />
            <Radio
              value="paid"
              selected={paidNonPaidFilter}
              text="Paid"
              onChange={setPaidNonPaidFilter}
            />
            <Radio
              value="nonPaid"
              selected={paidNonPaidFilter}
              text="Non-Paid"
              onChange={setPaidNonPaidFilter}
            />
          </div>
        </div>
      </div>
      <section className="product-service-experience-cards">
        {toggleState && toggleState === 1 ? (
          <div>
            {/* <div className="festival-card-type">Products</div> */}
            <div className="restaurant-cards-section">
              {
                // foodSamplesInFestival &&
                // renderFoodSampleCards(foodSamplesInFestival)
                foodSamplesInFestival && foodSamplesInFestival.length !== 0
                  ? foodSamplesInFestival.map((foodSample) => (
                      <FoodSamplesCreatedCard
                        key={foodSample.product_id}
                        foodSampleProps={foodSample}
                        festivalId={props.festivalId}
                        festivalName={props.festivalName}
                        selected={foodSampleSelected}
                        setSelected={setFoodSampleSelected}
                        isSelectable
                      />
                    ))
                  : null
              }
            </div>
          </div>
        ) : toggleState && toggleState === 2 ? (
          <div>
            {/* <div className="festival-card-type">Services</div> */}
            <div className="restaurant-cards-section">
              {
                // foodSamplesInFestival &&
                // renderFoodSampleCards(foodSamplesInFestival)
                servicesInFestival && servicesInFestival.length !== 0
                  ? servicesInFestival.map((service) => (
                      <FoodSamplesCreatedCard
                        key={service.service_id}
                        foodSampleProps={service}
                        festivalId={props.festivalId}
                        festivalName={props.festivalName}
                        selected={serviceSelected}
                        setSelected={setServiceSelected}
                        isSelectable
                      />
                    ))
                  : null
              }
            </div>
          </div>
        ) : toggleState && toggleState === 3 ? (
          <div>
            {/* <div className="festival-card-type">Experiences</div> */}
            <div className="restaurant-cards-section">
              {
                // foodSamplesInFestival &&
                // renderFoodSampleCards(foodSamplesInFestival)
                experiencesInFestival && experiencesInFestival.length !== 0
                  ? experiencesInFestival.map((experience) => (
                      <FoodSamplesCreatedCard
                        key={experience.service_id}
                        foodSampleProps={experience}
                        festivalId={props.festivalId}
                        festivalName={props.festivalName}
                        selected={experienceSelected}
                        setSelected={setExperienceSelected}
                        isSelectable
                      />
                    ))
                  : null
              }
            </div>
          </div>
        ) : null}
      </section>
    </Fragment>
  );
};

export default ViewProductsServicesExperiences;
