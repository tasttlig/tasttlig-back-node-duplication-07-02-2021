// Libraries
import React from 'react';
import axios from 'axios';
import AsyncSelect from 'react-select/async';

const NationalitySelector = (props) => {
  const toggleNationality = props.toggleNationality;
  const selectedItem = props.selectedItem;
  const setSelectedItem = props.setSelectedItem;

  const onChange = (selectedItem) => {
    toggleNationality(selectedItem);
    setSelectedItem(selectedItem);
  };

  const loadOptions = async (inputText, callback) => {
    const response = await axios({
      method: 'GET',
      url: `/food-sample/nationalities`,
      params: {
        keyword: inputText,
        selectedNationality: selectedItem,
      },
    });

    callback(
      response.data.nationalities.map((item) => ({
        label: `${item.nationality}`,
        value: item,
      })),
    );
  };

  return (
    <>
      <AsyncSelect
        isMulti
        placeholder="Select Nationality"
        loadOptions={loadOptions}
        defaultOptions
        value={selectedItem}
        onChange={onChange}
      />
    </>
  );
};

export default NationalitySelector;
