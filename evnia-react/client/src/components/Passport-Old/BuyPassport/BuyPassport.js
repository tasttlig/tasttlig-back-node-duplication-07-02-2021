// Libraries
import React, { useState } from 'react';

// Components
import Banner from '../../Home/Banner/Banner';

// Styling
import './BuyPassport.scss';
import festivalBanner from '../../../assets/images/festival-banner.jpg';

const BuyPassport = () => {
  // Set initial state
  const [load, setLoad] = useState(false);

  // Render Buy Passport Component page
  return (
    <div>
      <div className="row buy-passport">
        {/*<div className="col-lg-8 buy-passport-banner-image-section">*/}
        {/*  <img*/}
        {/*    src={festivalBanner}*/}
        {/*    alt="Festival Banner"*/}
        {/*    onLoad={() => setLoad(true)}*/}
        {/*    className={load ? "buy-passport-banner-image" : "loading-image"}*/}
        {/*  />*/}
        {/*</div>*/}
        {/* Main Banner Area */}
        <div className="col-lg-8 buy-passport-banner-image-section">
          <Banner noTimer={true} noButtons={true} />
        </div>
        <div className="col-lg-4 buy-passport-summary">
          <div className="buy-passport-summary-section">
            <div className="pb-3 buy-passport-title">Multi-National Food Festival 2020</div>
            <div className="pb-3">
              <div className="buy-passport-sub-title">Created by Tasttlig</div>
              <div className="buy-passport-text">$20</div>
            </div>
            <div className="pb-3">
              <div className="buy-passport-sub-title">Date</div>
              <div className="buy-passport-text">Wednesday, September 2 to 30, 2020</div>
            </div>
            <div>
              <div className="buy-passport-sub-title">Location</div>
              <div className="buy-passport-text">At Participating Restaurants Near You</div>
            </div>
          </div>
          <div className="btn btn-primary buy-passport-call-to-action">Buy Passport</div>
        </div>
      </div>

      {/* Responsive Design */}
      <div className="buy-passport-bottom">
        <div className="pb-2 buy-passport-text">$20</div>
        <div className="btn btn-primary buy-passport-call-to-action-bottom">Buy Passport</div>
      </div>
    </div>
  );
};

export default BuyPassport;
