// Libraries
import React, { useState, useContext } from 'react';
import { useHistory, Link } from 'react-router-dom';
import axios from 'axios';
import Modal from 'react-modal';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';
import { formatDate, formatMilitaryToStandardTime } from '../../Functions/Functions';

// Styling
import './PassportDetails.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const PassportDetails = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [claimFoodSampleOpened, setClaimFoodSampleOpened] = useState(false);
  const [passportIdOrEmail, setPassportIdOrEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [canClaim, setCanClaim] = useState(true);
  const [disableReserveButton] = useState(props.disableButton ? props.disableButton : false);
  const history = useHistory();

  // Set date and time
  // const startDateTime = moment(
  //   new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.startTime + '.000Z',
  // ).add(new Date().getTimezoneOffset(), 'm');

  // Set quantity available
  const quantityAvailable = Math.max(0, props.quantity - props.numOfClaims);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'claim-food-sample') {
      setClaimFoodSampleOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'claim-food-sample') {
      setClaimFoodSampleOpened(false);
    }
  };

  // See food sample owner profile helper function
  const handleFoodSampleOwner = () => {
    if (props.business_name) {
      history.push({
        pathname: `/business/${props.business_name}`,
      });
    }
  };
  const handleFoodSample = () => {
    if (props.foodSampleId) {
      history.push({
        pathname: `/food-sample/${props.foodSampleId}`,
      });
    }
  };

  // Validate user input for guest email helper function
  const validatePassportIdOrEmail = () => {
    if (!passportIdOrEmail) {
      setErrorMessage('Email is required.');

      return false;
    } else {
      setErrorMessage('');

      return true;
    }
  };

  // Claim food sample helper function
  const handleClaimFoodSample = async (event) => {
    if (!event) {
      event = window.event;
    }

    event.preventDefault();

    const isValid = validatePassportIdOrEmail();

    if (props.passportId || isValid) {
      const url = '/food-sample-claim';

      const acc_token = await localStorage.getItem('access_token');

      const headers = { Authorization: `Bearer ${acc_token}` };

      const data = {
        food_sample_claim_user: props.passportId ? props.passportId : passportIdOrEmail,
        food_sample_id: props.foodSampleId,
      };

      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for claiming ${props.title}!`, {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
          setErrorMessage('');
        } else if (response && response.data && !response.data.success && props.passportId) {
          toast(response.data.message, {
            type: 'error',
            autoClose: 2000,
          });
        } else {
          setErrorMessage(response.data.message);
          setCanClaim(response.data.canClaim);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Claim Food Sample Modal
  // const claimFoodSampleModal = (
  //   <Modal
  //     isOpen={claimFoodSampleOpened}
  //     onRequestClose={closeModal("claim-food-sample")}
  //     ariaHideApp={false}
  //     className="passport-details-modal"
  //   >
  //     <div className="mb-3 text-right">
  //       <span
  //         onClick={closeModal("claim-food-sample")}
  //         className="fas fa-times fa-2x close-modal"
  //       />
  //     </div>

  //     <div className="modal-title">{`Claim ${props.title} on Tasttlig`}</div>

  //     <form onSubmit={handleClaimFoodSample} noValidate>
  //       <div className="form-group">
  //         <input
  //           type="text"
  //           name="passportIdOrEmail"
  //           // placeholder="Enter Tasttlig Passport Id or Email"
  //           placeholder="Email"
  //           value={passportIdOrEmail}
  //           onChange={(e) => setPassportIdOrEmail(e.target.value)}
  //           disabled={submitAuthDisabled}
  //           className="email"
  //           required
  //         />
  //         <span className="far fa-envelope input-icon" />
  //         {errorMessage && <div className="error-message">{errorMessage}</div>}
  //       </div>

  //       <div>
  //         {/* {canClaim ? */}
  //         {/* {!disableReserveButton ? (
  //           <button
  //             type="submit"
  //             disabled={submitAuthDisabled}
  //             className="call-to-action-btn"
  //           >
  //             Reserve
  //             Reserve by {moment(startDateTime).subtract(2, "hours").format("hh:mm a")}
  //             {" "} {props.frequency}
  //           </button>
  //         ) : ""} */}
  //         {/* (
  //             <Link to={`/payment/food_sample/${props.foodSampleId}`} className="call-to-action-btn">
  //               Reserve for $2 by {moment(startDateTime).subtract(2, "hours").format("hh:mm a")}
  //               {" "} {props.frequency}
  //             </Link>
  //           ) */}
  //         {/* } */}
  //       </div>
  //     </form>
  //   </Modal>
  // );

  // Render Passport (Food Sample) details modal
  return (
    <div onClick={handleFoodSampleOwner} className="text-left">
      {/* {claimFoodSampleModal} */}

      <div className="row passport-details-owner-information">
        <div className="col-25">
          {props.foodSampleOwnerPicture ? (
            <img
              src={props.foodSampleOwnerPicture}
              alt={props.business_name}
              className="passport-details-owner-profile-picture"
            />
          ) : (
            <span className="fas fa-user-circle fa-3x passport-details-owner-default-picture"></span>
          )}
        </div>
        <div className="col-75">
          <div className="passport-details-title">{props.title}</div>
          {props.business_name && (
            <div>
              <span className="passport-details-owner-by">by</span>
              <span onClick={handleFoodSampleOwner} className="passport-details-owner-name">
                {' '}
                {props.business_name}
              </span>
            </div>
          )}
        </div>
      </div>

      <div className="mb-3 text-center passport-details-image">
        <ImageSlider images={props.images} />
      </div>

      <div onClick={handleFoodSample}>
        <span>
          <div className="mb-3">
            <div className="passport-details-sub-title">Address</div>
            <div>
              {`${props.address}, ${props.city}, ${props.provinceTerritory} ${props.postalCode}`}
            </div>
          </div>
          {props.startDate && (
            <div className="mb-3">
              <div className="passport-details-sub-title">Date</div>
              <div>{`${formatDate(props.startDate)} to ${formatDate(props.endDate)}`}</div>
            </div>
          )}
          {props.startTime && (
            <div className="mb-3">
              <div className="passport-details-sub-title">Time</div>
              <div>{`${formatMilitaryToStandardTime(
                props.startTime,
              )} to ${formatMilitaryToStandardTime(props.endTime)}`}</div>
            </div>
          )}
          {props.sample_size && (
            <div className="mb-3">
              <div className="passport-details-sub-title">Sample Size</div>
              <div>{props.sample_size}</div>
            </div>
          )}
          {props.is_available_on_monday ||
          props.is_available_on_tuesday ||
          props.is_available_on_wednesday ||
          props.is_available_on_thursday ||
          props.is_available_on_friday ||
          props.is_available_on_saturday ||
          props.is_available_on_sunday ? (
            <div className="mb-3">
              <div className="passport-details-sub-title">Days Available</div>
              {props.is_available_on_monday && <div>Monday</div>}
              {props.is_available_on_tuesday && <div>Tuesday</div>}
              {props.is_available_on_wednesday && <div>Wednesday</div>}
              {props.is_available_on_thursday && <div>Thursday</div>}
              {props.is_available_on_friday && <div>Friday</div>}
              {props.is_available_on_saturday && <div>Saturday</div>}
              {props.is_available_on_sunday && <div>Sunday</div>}
            </div>
          ) : null}
          {props.numOfClaims !== undefined && (
            <div className="mb-3">
              <div className="passport-details-sub-title">Quantity Available</div>
              <div>{quantityAvailable}</div>
            </div>
          )}
          <div className="passport-details-description">{props.description}</div>
        </span>
      </div>

      {disableReserveButton ||
      (window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/festival` &&
        window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/festival` &&
        window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/festival`) ? null : (
        <div
          // onClick={
          //   moment(new Date()).format("HH:mm a") <=
          //     moment(startDateTime).subtract(2, "hours").format("HH:mm a") &&
          //   quantityAvailable > 0 &&
          //   props.foodSampleOwnerId !== appContext.state.user.id
          //     ? props.passportId
          //       ? handleClaimFoodSample
          //       : authModalContext.openModal("log-in")
          //     : null
          // }
          onClick={
            quantityAvailable > 0 && props.foodSampleOwnerId !== appContext.state.user.id
              ? props.passportId
                ? handleClaimFoodSample
                : props.closeModal
              : null
          }
          disabled={submitAuthDisabled}
          // className={
          //   moment(new Date()).format("HH:mm A") <=
          //     moment(startDateTime).subtract(2, "hours").format("HH:mm A") &&
          //   quantityAvailable > 0 &&
          //   props.foodSampleOwnerId !== appContext.state.user.id
          //     ? "mt-auto call-to-action-btn"
          //     : "mt-auto btn btn-dark call-to-action-btn"
          // }
          className={
            quantityAvailable > 0 && props.foodSampleOwnerId !== appContext.state.user.id
              ? 'mt-3 call-to-action-btn'
              : 'mt-3 btn btn-dark call-to-action-btn'
          }
        >
          Reserve
        </div>
      )}
    </div>
  );
};

export default PassportDetails;
