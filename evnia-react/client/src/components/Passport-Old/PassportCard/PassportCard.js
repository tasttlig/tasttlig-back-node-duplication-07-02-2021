// Libraries
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import moment from 'moment';
import Flag from 'react-world-flags';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Components
import PassportDetails from '../PassportDetails/PassportDetails';

// Styling
import './PassportCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const PassportCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);
  const [claimFoodSampleOpened, setClaimFoodSampleOpened] = useState(false);
  const [addToFestivalOpened, setAddToFestivalOpened] = useState(false);
  const [passportIdOrEmail, setPassportIdOrEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [canClaim, setCanClaim] = useState(true);
  const [disableReserveButton] = useState(props.disableButton ? props.disableButton : false);
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [festivalPreference, setFestivalPreference] = useState([]);

  // Set date and time
  const startDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.startTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.endTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');
  const endDate = moment(new Date(props.endDate).toISOString());

  // Set quantity available
  const quantityAvailable = Math.max(0, props.quantity - props.numOfClaims);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;

  // Open modal type helper function
  const openModal = (modalType) => () => {
    setAddToFestivalOpened(true);
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    setAddToFestivalOpened(false);
  };

  //fetch all festivals
  const fetchFestivalDetails = async () => {
    const url = '/festival/allFestival';

    try {
      const response = await axios({ method: 'GET', url });
      setFestivalDetails(response.data.festival_list);
    } catch (error) {
      return error.response;
    }
  };

  const onChangeFestival = function (event) {
    //takes the file and reads the file from buffer of array

    if (festivalPreference.includes(event.target.value)) {
    } else {
      const matchedFestival = festivalDetails.find((value) => {
        return value.festival_id === Number(event.target.value);
      });
      setFestivalPreference(festivalPreference.concat(matchedFestival));
      //setFestivalPreferenceName(festivalPreferenceName.concat(matchedFestival.festival_name))
    }
  };

  const handleAddToFestival = async () => {
    const url = '/business/festival/add';
    const acc_token = localStorage.getItem('access_token');
    const headers = { Authorization: `Bearer ${acc_token}` };
    const data = {
      festival_id: festivalPreference.map((festival) => {
        return festival.festival_id;
      }),
      //festival_restaurant_host_id: appContext.state.user.id,
      business_details_id: props.businessDetailsId,
      user_id: props.userId,
    };

    try {
      const response = await axios({ method: 'POST', url, headers, data });
      if (response && response.data && response.data.success) {
        setTimeout(() => {
          // window.location.reload();
          //window.location.href = `/`;
        }, 2000);

        toast(`Success! Thank you for adding the restaurant to the festival(s)!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  //function to render over the selected options from dropdown and print them
  const getOptions = function (festivals) {
    return festivals.map((festival) => (
      <button
        key={festival.festival_id}
        className="festival-card-festival-attend-btn"
        onClick={(event) => {
          setFestivalPreference((stateFestivals) => {
            return stateFestivals.filter((stateFestival) => {
              if (stateFestival.festival_id === festival.festival_id) {
                return false;
              } else {
                return true;
              }
            });
          });
        }}
      >
        {festival.festival_name}
      </button>
    ));
  };

  // Validate user input for passport id or email
  const validatePassportIdOrEmail = () => {
    if (!passportIdOrEmail) {
      setErrorMessage('Passport Id or Email is required.');

      return false;
    } else {
      setErrorMessage('');

      return true;
    }
  };

  useEffect(() => {
    fetchFestivalDetails();
  }, []);

  // Claim Food Sample Modal
  // const claimFoodSampleModal = (
  //   <Modal
  //     isOpen={claimFoodSampleOpened}
  //     onRequestClose={closeModal("claim-food-sample")}
  //     ariaHideApp={false}
  //     className="passport-details-modal"
  //   >
  //     <div className="mb-3 text-right">
  //       <span
  //         onClick={closeModal("claim-food-sample")}
  //         className="fas fa-times fa-2x close-modal"
  //       />
  //     </div>

  //     <div className="modal-title">{`Claim ${props.title} on Tasttlig`}</div>

  //     <form onSubmit={handleClaimFoodSample} noValidate>
  //       <div className="form-group">
  //         <input
  //           type="text"
  //           name="passportIdOrEmail"
  //           // placeholder="Enter Tasttlig Passport Id or Email"
  //           placeholder="Email"
  //           value={passportIdOrEmail}
  //           onChange={(e) => setPassportIdOrEmail(e.target.value)}
  //           disabled={submitAuthDisabled}
  //           className="email"
  //           required
  //         />
  //         <span className="far fa-envelope input-icon" />
  //         {errorMessage && <div className="error-message">{errorMessage}</div>}
  //       </div>

  //       <div>
  //         {/* {canClaim ? */}
  //         {/* {!disableReserveButton ? (
  //           <button
  //             type="submit"
  //             disabled={submitAuthDisabled}
  //             className="call-to-action-btn"
  //           >
  //             Reserve
  //             Reserve by {moment(startDateTime).subtract(2, "hours").format("hh:mm a")}
  //             {" "} {props.frequency}
  //           </button>
  //         ) : ""} */}
  //         {/* (
  //             <Link to={`/payment/food_sample/${props.foodSampleId}`} className="call-to-action-btn">
  //               Reserve for $2 by {moment(startDateTime).subtract(2, "hours").format("hh:mm a")}
  //               {" "} {props.frequency}
  //             </Link>
  //           ) */}
  //         {/* } */}
  //       </div>
  //     </form>
  //   </Modal>
  // );

  // Passport (Food Sample) Details Modal
  /* const passportDetailsModal = (
    <Modal
      isOpen={passportDetailsOpened}
      onRequestClose={closeModal('passport-details')}
      ariaHideApp={false}
      className="passport-details-modal"
    >
      <div className="text-right">
        <button
          aria-label={`Close ${props.title} Details Modal`}
          onClick={closeModal('passport-details')}
          className="fas fa-times fa-2x close-modal"
        ></button>
      </div>

      <PassportDetails
        business_name={props.business_name}
        passportId={props.passportId}
        foodSampleId={props.foodSampleId}
        images={props.images}
        title={props.title}
        price={props.price}
        address={props.address}
        city={props.city}
        provinceTerritory={props.provinceTerritory}
        postalCode={props.postalCode}
        startDate={props.startDate}
        endDate={props.endDate}
        startTime={props.startTime}
        endTime={props.endTime}
        description={props.description}
        sample_size={props.sample_size}
        is_available_on_monday={props.is_available_on_monday}
        is_available_on_tuesday={props.is_available_on_tuesday}
        is_available_on_wednesday={props.is_available_on_wednesday}
        is_available_on_thursday={props.is_available_on_thursday}
        is_available_on_friday={props.is_available_on_friday}
        is_available_on_saturday={props.is_available_on_saturday}
        is_available_on_sunday={props.is_available_on_sunday}
        foodSampleOwnerId={props.foodSampleOwnerId}
        foodSampleOwnerPicture={props.foodSampleOwnerPicture}
        isEmailVerified={props.isEmailVerified}
        firstName={props.firstName}
        lastName={props.lastName}
        email={props.email}
        phoneNumber={props.phoneNumber}
        facebookLink={props.facebookLink}
        twitterLink={props.twitterLink}
        instagramLink={props.instagramLink}
        youtubeLink={props.youtubeLink}
        linkedinLink={props.linkedinLink}
        websiteLink={props.websiteLink}
        bioText={props.bioText}
        history={props.history}
        nationality={props.nationality}
        alpha_2_code={props.alpha_2_code}
        frequency={props.frequency}
        foodSampleType={props.foodSampleType}
        quantity={props.quantity}
        numOfClaims={props.numOfClaims}
        appContext={appContext}
        disableButton={props.disableButton}
        closeModal={closeModal('passport-details')}
      />

      <div className="close-modal-bottom-content">
        <span onClick={closeModal('passport-details')} className="close-modal-bottom">
          Close
        </span>
      </div>
    </Modal>
  ); */

  // Render Passport (Food Samples) Card
  return (
    <div
      className={`${
        window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/` ||
        window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/` ||
        window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/`
          ? 'col-md-4 col-lg-4 col-xl-3'
          : 'col-md-4 col-xl-4'
      } passport-card`}
    >
      <LazyLoad once>
        {/* {claimFoodSampleModal} */}

        <Modal
          isOpen={addToFestivalOpened}
          onRequestClose={closeModal()}
          ariaHideApp={false}
          className="passport-details-modal"
        >
          <div className="mb-3">
            <h5>Which festival would you like to add the restaurant to?</h5>
            <div className="input-title">Select all that apply</div>
            <select
              name="festival"
              label="List festival"
              onChange={onChangeFestival}
              disabled={submitAuthDisabled}
              // required
            >
              <option value="" name="">
                --Select--
              </option>
              {festivalDetails.map((n) => (
                <option key={n.festival_id} value={n.festival_id} name={n.festival_name}>
                  {n.festival_name}
                </option>
              ))}
            </select>
            {festivalPreference.length === 0 ? (
              <div className="col-md-4 p-2">
                <p></p>
              </div>
            ) : (
              <div className="col-md-4">{getOptions(festivalPreference)}</div>
            )}
          </div>

          <div
            onClick={handleAddToFestival}
            disabled={submitAuthDisabled}
            className={'mt-3 call-to-action-btn'}
          >
            Add to Festival
          </div>
          <div className="close-modal-bottom-content">
            <span onClick={closeModal()} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

        <div className="d-flex flex-column h-100 card-box">
          <div className="passport-card-content">
            <img
              src={props.images[0]}
              alt={props.title}
              onLoad={() => setLoad(true)}
              className={load ? 'passport-image' : 'loading-image'}
            />
            <div className="passport-card-flag-container">
              <Flag code={props.alpha_2_code} className="passport-card-flag" />
              <div className="passport-card-flag-country">{props.nationality}</div>
            </div>
            <div className="passport-card-text">
              <div className="passport-card-title">{props.title}</div>
              {props.business_name && (
                <div className="passport-card-details">{props.business_name}</div>
              )}
              <div className="passport-card-details">
                {`${props.address}, ${props.city}, ${props.provinceTerritory}
                ${props.postalCode}`}
              </div>
              {props.startDate && (
                <div className="passport-card-details">
                  {`${moment(startDateTime).format('MMM Do')} ${moment(startDateTime).format(
                    'hh:mm A',
                  )} - ${moment(endDateTime).format('hh:mm A')}`}
                  <br />
                  until {endDate.format('MMM Do')}
                </div>
              )}

              {props.numOfClaims !== undefined && (
                <div className="passport-card-details">Quantity Available: {quantityAvailable}</div>
              )}
            </div>
          </div>
          <div className="mt-auto btn btn-primary" onClick={openModal()}>
            Add to Festival
          </div>

          {disableReserveButton ||
          (window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/` &&
            window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/` &&
            window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/` &&
            window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/festival` &&
            window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/festival` &&
            window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/festival`) ||
          (userRole && userRole.includes('ADMIN')) ? null : props.foodSampleOwnerId ===
            appContext.state.user.id ? (
            <div className="mt-auto mb-3">You own this item</div>
          ) : !appContext.state.user.id ? (
            <Link exact="true" to="/login" className="mt-auto buy-now-btn">
              Reserve Free Tasting
            </Link>
          ) : (
            <div
            // onClick={
            //   moment(new Date()).format("HH:mm A") <=
            //     moment(startDateTime).subtract(2, "hours").format("HH:mm A") &&
            //   quantityAvailable > 0 &&
            //   props.foodSampleOwnerId !== appContext.state.user.id
            //     ? props.passportId
            //       ? handleClaimFoodSample
            //       : authModalContext.openModal("log-in")
            //     : null
            // }
            /* onClick={
                quantityAvailable > 0 && props.foodSampleOwnerId !== appContext.state.user.id
                  ? props.passportId
                    ? handleClaimFoodSample
                    : null
                  : null
              }
              disabled={submitAuthDisabled} */
            // className={
            //   moment(new Date()).format("HH:mm A") <=
            //     moment(startDateTime).subtract(2, "hours").format("HH:mm A") &&
            //   quantityAvailable > 0 &&
            //   props.foodSampleOwnerId !== appContext.state.user.id
            //     ? "mt-auto call-to-action-btn"
            //     : "mt-auto btn btn-dark call-to-action-btn"
            // }
            /* className={
                quantityAvailable > 0 && props.foodSampleOwnerId !== appContext.state.user.id
                  ? `${!props.NotInFestival ? 'mt-auto' : ''} call-to-action-btn`
                  : `${!props.NotInFestival ? 'mt-auto' : ''} btn btn-dark call-to-action-btn`
              } */
            >
              Reserve Free Tasting
            </div>
          )}
        </div>
      </LazyLoad>
    </div>
  );
};

export default PassportCard;
