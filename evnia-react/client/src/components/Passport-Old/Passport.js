// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { usePosition } from '../../hooks';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import SearchBar from '../Navbar/SearchBar';
import PassportCard from './PassportCard/PassportCard';
import BannerFooter from '../Home/BannerFooter/BannerFooter';
import GoTop from '../Shared/GoTop';
import NationalitySelector from './NationalitySelector';
import AddressLookup from '../AddressLookup/AddressLookup';

// Styling
import './Passport.scss';
import 'react-datepicker/dist/react-datepicker.css';

const Passport = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [festival_name] = useState(props.festival_name);
  const [foodSampleItems, setFoodSampleItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [filterStartDate, setFilterStartDate] = useState('');
  const [filterEndDate, setFilterEndDate] = useState('');
  const [filterQuantity, setFilterQuantity] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedItem, setSelectedItem] = useState('');
  const [restaurants, setRestaurants] = useState([]);
  const [lookupLat, setLookupLat] = useState('');
  const [lookupLng, setLookupLng] = useState('');
  const [addressLookup, setAddressLookup] = useState('');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  const { latitude, longitude, positionChanged, geoError } = usePosition();

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Render Passport Cards (Food Samples) helper function
  const renderPassportCards = (arr) => {
    return arr.map((card, index) => (
      <PassportCard
        key={index}
        business_name={card.business_name}
        foodSampleId={card.food_sample_id}
        passportId={
          appContext.state && appContext.state.user && appContext.state.user.passport_id
            ? appContext.state.user.passport_id
            : ''
        }
        images={card.image_urls}
        title={card.title}
        price={card.price}
        address={card.address}
        city={card.city}
        provinceTerritory={card.state}
        postalCode={card.postal_code}
        startDate={card.start_date}
        endDate={card.end_date}
        startTime={card.start_time}
        endTime={card.end_time}
        description={card.description}
        sample_size={card.sample_size}
        is_available_on_monday={card.is_available_on_monday}
        is_available_on_tuesday={card.is_available_on_tuesday}
        is_available_on_wednesday={card.is_available_on_wednesday}
        is_available_on_thursday={card.is_available_on_thursday}
        is_available_on_friday={card.is_available_on_friday}
        is_available_on_saturday={card.is_available_on_saturday}
        is_available_on_sunday={card.is_available_on_sunday}
        foodSampleOwnerId={card.food_sample_creater_user_id}
        foodSampleOwnerPicture={card.profile_image_link}
        isEmailVerified={card.is_email_verified}
        firstName={card.first_name}
        lastName={card.last_name}
        email={card.email}
        phoneNumber={card.phone_number}
        facebookLink={card.facebook_link}
        twitterLink={card.twitter_link}
        instagramLink={card.instagram_link}
        youtubeLink={card.youtube_link}
        linkedinLink={card.linkedin_link}
        websiteLink={card.website_link}
        bioText={card.bio_text}
        nationality={card.nationality}
        alpha_2_code={card.alpha_2_code}
        frequency={card.frequency}
        foodSampleType={card.food_sample_type}
        quantity={card.quantity}
        numOfClaims={card.num_of_claims}
        history={props.history}
      />
    ));
  };

  // Toggle nationality helper function
  const toggleNationality = (nationalities) => {
    let nationalities_list = [];

    nationalities.map((nationality) => {
      nationalities_list.push(nationality.value.nationality);
    });

    setSelectedNationalities(nationalities_list);
    // if (selectedNationalities.includes(nationality)) {
    //   setSelectedNationalities(
    //     selectedNationalities.filter((c) => c !== nationality)
    //   );
    // } else {
    //   setSelectedNationalities([nationality].concat(selectedNationalities));
    // }
  };

  // const isFiltered = () => {
  //   return (
  //     selectedNationalities.length ||
  //     filterStartDate !== "" ||
  //     filterEndDate !== ""
  //   );
  // };

  // Load next set of food samples helper functions
  const loadNextPage = async (page) => {
    const url = '/food-sample/all?';

    return axios({
      method: 'GET',
      url,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
        nationalities: selectedNationalities,
        radius: filterRadius,
        startDate: filterStartDate,
        endDate: filterEndDate,
        quantity: filterQuantity,
        latitude: latitude ?? lookupLat,
        longitude: longitude ?? lookupLng,
        festival_name,
      },
    });
  };

  const handleLoadMore = (page = currentPage, foods = foodSampleItems) => {
    if (latitude || lookupLat || geoError) {
      setLoading(true);
      loadNextPage(page).then((newPage) => {
        setLoading(false);

        const pagination = newPage.data.details.pagination;

        if (page < pagination.lastPage) {
          setCurrentPage(page + 1);
        }

        setHasNextPage(currentPage < pagination.lastPage);
        setFoodSampleItems(foods.concat(newPage.data.details.data));
      });
    }
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // const loadingElement = () => {
  //   if (loading && hasNextPage) {
  //     return <span className={"mt-6 mx-auto"}>Loading ...</span>;
  //   }
  //   if (!loading && !hasNextPage) {
  //     return <div id={"no_more_samples"}>This is the end of food samples</div>;
  //   }
  // };

  // Mount Passport (Food Samples) page
  useEffect(() => {
    window.scrollTo(0, 0);

    // const fetchDistinctNationalities = async () => {
    //   const response = await axios({
    //     method: "GET",
    //     url: "/food-sample/nationalities",
    //   });
    //   setAvailableNationalities(response.data.nationalities);
    // };
    //
    // fetchDistinctNationalities();
    handleLoadMore();
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [
    props.location.state.keyword,
    props.location.state.foodAdCode,
    selectedNationalities,
    filterStartDate,
    filterEndDate,
    filterQuantity,
    filterRadius,
    positionChanged,
    lookupLat,
  ]);

  // Render empty food sample page
  const FoodSampleEmpty = () => {
    return <strong className="no-food-samples-found">No food samples found.</strong>;
  };

  const Location = () => (
    <>
      <div>
        <hr />
        <section>
          <div className="pb-2 filter-section-header">Location</div>

          {!positionChanged || geoError ? (
            <div className="filter-section-categories mt-2 mb-4">
              <div className="mb-1">Address</div>
              <AddressLookup
                value={addressLookup}
                onSelect={(address, latlng) => {
                  setAddressLookup(address);
                  setLookupLng(latlng.lng);
                  setLookupLat(latlng.lat);
                }}
              />
            </div>
          ) : null}

          <div className="filter-section-categories">
            <div className="mb-1">Distance (km)</div>
            <select
              onChange={(e) => setFilterRadius(parseInt(e.target.value))}
              value={filterRadius}
              className="mb-3 custom-select"
            >
              <option value="25000000">--Select---</option>
              <option value="5000">5km</option>
              <option value="10000">10km</option>
              <option value="25000">25km</option>
              <option value="50000">50km</option>
            </select>
          </div>
        </section>
      </div>
    </>
  );

  // Render filters
  const Filters = () => (
    <div className="page-filters">
      {window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/09-2020-festival` ||
      window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/09-2020-festival` ||
      window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/09-2020-festival` ? (
        <div className="current-festival-link-content">
          <a href="/festival" className="current-festival-link">
            Go to Current Festival
          </a>
        </div>
      ) : null}
      {/* <section className="mb-4">
        <SearchBar
          keyword={props.location.state.keyword}
          url={
            window.location.href ===
              `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
            window.location.href ===
              `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
            window.location.href ===
              `${process.env.REACT_APP_DEV_BASE_URL}/festival`
              ? "/festival"
              : "/09-2020-festival"
          }
        />
      </section> */}
      <div className="">
        <div className="pb-2 filter-section-header">Cuisine</div>
        {/*<div className="filter-section-categories">*/}
        {/*  <ul>*/}
        {/*    {availableNationalities &&*/}
        {/*      availableNationalities.map((c) => (*/}
        {/*        <li key={c}>*/}
        {/*          <input*/}
        {/*            type="checkbox"*/}
        {/*            defaultChecked={selectedNationalities.includes(c)}*/}
        {/*            value={c}*/}
        {/*            onClick={toggleNationality}*/}
        {/*          />*/}
        {/*          {c}*/}
        {/*        </li>*/}
        {/*      ))}*/}
        {/*  </ul>*/}
        {/*</div>*/}
        <NationalitySelector
          toggleNationality={toggleNationality}
          selectedItem={selectedItem}
          setSelectedItem={setSelectedItem}
        />
      </div>
      <hr />
      <section>
        <div className="pb-2 filter-section-header">Date / Time</div>
        <div className="filter-section-categories">
          <div className="mb-1">From</div>
          <DatePicker
            selected={filterStartDate}
            onChange={(date) => setFilterStartDate(date)}
            minDate={new Date()}
            showTimeSelect
            dateFormat="MMMM d, yyyy h:mm aa"
            className="form-control passport-date-time"
          />
        </div>
      </section>

      <Location />
    </div>
  );

  // Render Passport (Food Samples) page
  return (
    <div>
      <Nav />

      {window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
      window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
      window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/festival` ? (
        <div>
          <section className="jumbotron landing-page-banner">
            <h1 className="landing-page-banner-statement">
              <div className="landing-page-banner-statement-yellow">Everything is free.</div>
              <div className="landing-page-banner-statement-red">Reserve free tasting.</div>
              <div className="landing-page-banner-statement-green">Free, no payment required.</div>
            </h1>
          </section>
        </div>
      ) : null}

      <div>
        <div
          className={
            window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
            window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
            window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/festival`
              ? 'landing-page'
              : 'passport'
          }
        >
          <div className="row">
            <div className="col-lg-3 passport-page-title">
              {`${
                window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
                window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
                window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/festival`
                  ? 'December'
                  : 'September'
              } 2020 Festival`}
            </div>
            <div className="col-lg-9 passport-search">
              <SearchBar
                keyword={props.location.state.keyword}
                url={
                  window.location.href === `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
                  window.location.href === `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
                  window.location.href === `${process.env.REACT_APP_DEV_BASE_URL}/festival`
                    ? '/festival'
                    : '/09-2020-festival'
                }
              />
            </div>
          </div>

          <div className="row">
            <div className="col-md-3 p-0">
              <Filters />
            </div>
            <div className="col-md-9 p-0">
              <div className="passport-cards" ref={infiniteRef}>
                {foodSampleItems.length !== 0 ? (
                  renderPassportCards(foodSampleItems)
                ) : (
                  <FoodSampleEmpty />
                )}
              </div>
            </div>
          </div>
        </div>

        {/* Footer Area */}
        <BannerFooter />
      </div>
      {/* ) : (
        <PassportPricing />
      )} */}

      {/* Back to Top */}
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default Passport;
