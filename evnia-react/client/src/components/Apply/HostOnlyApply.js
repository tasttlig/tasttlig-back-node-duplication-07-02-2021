// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import HostOnlyApplyForm from './ApplyForm/HostOnlyApplyForm';

// Styling
import './Apply.scss';

const Apply = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Tasttlig Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Tasttlig Host page
  return (
    <div>
      <div className="become-host-form">
        <Nav />
        <HostOnlyApplyForm history={props.history} />
      </div>
    </div>
  );
};

export default Apply;
