// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import Nav from '../Navbar/Nav';

const HostApplicationAction = (props) => {
  // Set initial state
  const [content, setContent] = useState('');

  // Verify email helper function
  const upgradeUser = async () => {
    let response_text = '';

    try {
      const token = props.match.params.token;
      const response = await axios.post(`/user/upgrade/action/${token}`);

      if (response.data.success) {
        response_text = `Status of Application to become Host is ${response.data.message}`;
      } else {
        response_text = `Error in Changing to the Application Status, Contact Support. Error: ${response.data.message}`;
      }
    } catch (error) {
      response_text = `Error in Changing to the Application Status, Contact Support. Error: ${error}`;
    }

    setContent(response_text);
  };

  // Mount Email Verification page
  useEffect(() => {
    window.scrollTo(0, 0);

    upgradeUser();
  }, []);

  return (
    <div>
      <div>
        <Nav />

        <div className="dashboard">
          <div className="row">
            <div>{content}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HostApplicationAction;
