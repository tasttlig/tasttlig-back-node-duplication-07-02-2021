import React from 'react';
import styled from 'styled-components';
import modal from 'react-modal';

import tasttligLogoBlack from '../../../../assets/images/tasttlig-logo-black.png';

const Background = styled.div`
    width: 100%
    height: 100%
    background: rgbal(0,0,0,0,8);
    position: fixed;
    display: flex;
    justify-content: center;
    align-items: center;
    `;

const ModalWrapper = styled.div`
  width: 800px;
  height: 500px;
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
  background: #fff;
  color: #000;
  display: grid;
  grid-template-columns: 1fr 1fr;
  position: relative;
  z-index: 10;
  border-radius: 10px;
`;

const ModalImg = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 10px 0 0 10px;
  background: #000;
`;

const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 1.8;
  color: #141414;

  p {
    margin-bottom: 1rem;
  }

  button {
    padding: 10px 24px;
    background: #141414;
    color: #fff;
    border: none;
  }
`;

export const PassportConfirmationModal = (props) => {
  console.log('modal entered');
  return (
    <>
      <Background>
        <ModalWrapper showModal={props.showModal}>
          {/* <ModalImg src = {tasttligLogoBlack} alt='camera' /> */}
          <ModalContent>
            <h1>Congratulations!!</h1>
            <h3>Your first festval is on us</h3>
            <button>Explore Tasttlig</button>
          </ModalContent>
        </ModalWrapper>
      </Background>
    </>
  );
};
