// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import ProductForm from './FormSteps/ProductForm';
import FormReview from './FormSteps/FormReview';

toast.configure();

export default class CreateMenuItemsForm extends Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);

    const currentTime = new Date();
    currentTime.setHours(currentTime.getHours() + 2);

    // Set initial state
    this.state = {
      step: 0,
      menu_list: [],
      has_business: true,
      business_category: 'Food',
      submitAuthDisabled: false,
    };
  }

  UNSAFE_componentWillMount() {
    const hostFormData = JSON.parse(localStorage.getItem('menuItemsFormData'));

    if (hostFormData) {
      Object.keys(this.state).forEach((key) => {
        const value = hostFormData[key];

        if (value) {
          this.setState({
            [key]: value,
          });
        }
      });
    }
  }

  // Proceed to next step
  nextStep = () => {
    window.scrollTo(0, 0);

    this.setState({
      step: this.state.step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    window.scrollTo(0, 0);

    this.setState({ step: this.state.step - 1 });
  };

  // Update menu items information
  update = (data) => {
    const hostFormData = JSON.parse(localStorage.getItem('menuItemsFormData')) || {};

    localStorage.setItem('menuItemsFormData', JSON.stringify({ ...hostFormData, ...data }));

    Object.keys(data).forEach((key) => {
      this.setState({ [key]: data[key] });
    });
  };

  // Submit Become a Food Provider form helper function
  handleSubmitApplication = async (event) => {
    event.preventDefault();

    const url = '/user/addMenuItems';

    let { submitAuthDisabled, use_residential, ...data } = this.state;
    data = {
      ...data,
      email: this.context.state.signedInStatus ? this.context.state.user.email : this.state.email,
    };

    try {
      const response = await axios({ method: 'POST', url, data });

      if (response && response.data && response.data.success) {
        localStorage.removeItem('menuItemsFormData');

        setTimeout(() => {
          window.location.href = '/dashboard';
        }, 2000);

        toast(`Success! Thank you for creating menu items!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  getSteps = () => {
    return [ProductForm, FormReview];
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  render = () => {
    const { step, ...values } = this.state;
    const Step = this.getSteps()[step];

    return (
      <Step
        nextStep={this.nextStep}
        prevStep={this.prevStep}
        update={this.update}
        values={values}
        handleSubmitApplication={this.handleSubmitApplication}
        standalone={true}
      />
    );
  };
}
