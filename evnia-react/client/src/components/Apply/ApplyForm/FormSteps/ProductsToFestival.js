// Libraries
import React, { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';

// Components
import { DateInput, Form, Input, MultiImageInput, Select, Textarea } from '../../../EasyForm';
import SponsorProductOrServiceCard from '../../../FestivalPage/SponsorProductOrServiceCard/SponsorProductOrServiceCard';
import {
  formatDate,
  formattedTimeToDateTime,
  formatMilitaryToStandardTime,
} from '../../../Functions/Functions';

import '../../../Profile/Sponsor/SponsorProductsAndServices/SponsorProductsAndServices.scss';

// Styling
// import "react-sweet-progress/lib/style.css";

let ButtonOrCheckmark = (props) => {
  const { values, readMode, product } = props;
  const [button, setButton] = useState(true);
  const [checkmark, setCheckmark] = useState(false);

  const addToFestival = async (data) => {
    console.log(product.product_id);
    const response = await axios({
      method: 'POST',
      url: `/products/festival/${values.festivalId}`,
      data: {
        festivalId: values.festivalId,
        productId: product.product_id,
      },
    });
    console.log(response);
    if (response.data.details === 'Success.') {
      setButton(false);
      setCheckmark(true);
    }
    /*     if (products[0].product_festivals_id.includes(values.festivalId)) {
          setButton(false)
          setCheckmark(true);
        } */
    return response;
  };
  useEffect(() => {
    if (product.product_festivals_id && product.product_festivals_id.includes(values.festivalId)) {
      setButton(false);
      setCheckmark(true);
    }
  }, []);
  return (
    <Form onSubmit={addToFestival}>
      {button && <input type="submit" className="" value="Add to Festival"></input>}

      {checkmark && (
        <span className="checkmark">
          <div className="checkmark_circle"></div>
          <div className="checkmark_stem"></div>
          <div className="checkmark_kick"></div>
        </span>
      )}
    </Form>
  );
};
const ProductsToFestival = (props) => {
  const appContext = useContext(AppContext);
  const { nextStep, update, values, readMode } = props;
  const userRole = appContext.state.user.role;
  // Set initial state
  const [existingUser, setExistingUser] = useState(null);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  let productInFestival;
  const [createFormsOpened, setCreateFormsOpened] = useState(false);
  const [nationalities, setNationalities] = useState([]);
  const [festivalList, setFestivalList] = useState([]);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [selected, setSelected] = useState('products');
  const [formDetails, setFormDetails] = useState({
    input_one: 'product_name',
    input_one_placeholder: 'Name Your Product',
    input_two: 'product_made_in_nationality_id',
    input_three: 'product_price',
    input_three_placeholder: 'Price',
    input_four: 'product_quantity',
    input_four_placeholder: 'Quantity',
    input_five: 'product_size',
    input_five_placeholder: 'Size',
    input_six: 'product_description',
    input_six_placeholder: 'Description',
    input_seven: 'product_expiry_date',
    input_seven_placeholder: 'Expiry Date',
    input_eight: 'product_expiry_time',
    input_eight_placeholder: 'Expiry Time',
    input_nine: 'product_images',
    input_ten: 'product_festival_id',
  });
  // To use the JWT credentials

  let data = {};

  const onSubmit = async (values) => {
    const { ...rest } = values;

    let data = {
      ...rest,
    };

    if (selected === 'products') {
      data.product_expiry_time = formattedTimeToDateTime(
        values.product_expiry_time.toString().substring(16, 21),
      );
      data.product_expiry_date = formatDate(values.product_expiry_date);

      await submitProductOrService(data);
    } else {
      await submitProductOrService(data);
    }
  };

  const submitProductOrService = async (data) => {
    window.scrollTo(0, 0);

    let url = '';
    if (selected === 'products') {
      url = '/products/add';
    } else if (selected === 'services') {
      url = '/services/add';
    }

    try {
      const response = await axios({
        method: 'POST',
        url,
        data,
      });
      console.log(response);
      if (
        response &&
        response.data &&
        response.data.success &&
        userRole &&
        (userRole.includes('VENDOR') || userRole.includes('ADMIN'))
      ) {
        defaultOnCreatedAction();
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };
  const defaultOnCreatedAction = () => {
    //setTimeout(() => {
    //window.location.href = `/festival/${props.location.festivalId}`;
    //}, 2000);

    toast(`Success! Thank you for creating ${selected}!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSubmitAuthDisabled(true);
  };

  const productSelected = () => {
    setFormDetails({
      input_one: 'product_name',
      input_one_placeholder: 'Name Your Product',
      input_two: 'product_made_in_nationality_id',
      input_three: 'product_price',
      input_three_placeholder: 'Price',
      input_four: 'product_quantity',
      input_four_placeholder: 'Quantity',
      input_five: 'product_size',
      input_five_placeholder: 'Size',
      input_six: 'product_description',
      input_six_placeholder: 'Description',
      input_seven: 'product_expiry_date',
      input_seven_placeholder: 'Expiry Date',
      input_eight: 'product_expiry_time',
      input_eight_placeholder: 'Expiry Time',
      input_nine: 'product_images',
      input_ten: 'product_festival_id',
    });
  };
  const changeForm = (event) => {
    setSelected('products');
    productSelected();
  };
  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(false);
    }
  };

  /*   const addToFestival = async (data) => {
      console.log(products[0].product_id);
      const response = await axios({
        method: "POST",
        url: `/products/festival/${values.festivalId}`,
        data: {
          festivalId: values.festivalId,
          productId: products[0].product_id
        },
      });
      console.log(response)
      if (response.data.details === "Success.") {
        input = false
      }
      return response;
    } */

  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
  };

  const fetchUserProducts = async () => {
    const response = await axios({
      method: 'GET',
      url: `/products/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    console.log(response.data.details);
    setProducts(response.data.details);
    return response;
  };

  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };
  useEffect(() => {
    fetchUserProducts();
    fetchNationalities();
    fetchFestivalList();
    setLoading(false);
  }, []);

  const renderProducts = (arr) => {
    return arr.map((product, index) => (
      /*       <div className="row product-row">
              <div className="col-8">
                <div>Product Name: {product.product_name}</div>
                <div>Product Description: {product.product_description}</div>
                <div>Product Quantity: {product.product_quantity}</div>
                <div>Product Size: {product.product_size}</div>
              </div>
              <div className="col-4">
                {<ButtonOrCheckmark
                  product={product}
                  update={update}
                  values={values}
                >
                </ButtonOrCheckmark>}
              </div>
            </div> */
      <div className="col-sm-4">
        <SponsorProductOrServiceCard
          key={index}
          productId={product.product_id}
          images={product.image_urls}
          title={product.product_name}
          price={product.product_price}
          address={product.business_address_1}
          restaurant_name={product.business_name}
          city={product.city}
          startDate={formatDate(product.product_expiry_date)}
          startTime={formatMilitaryToStandardTime(product.product_expiry_time)}
          description={product.product_description}
          itemType="product"
        />
        <ButtonOrCheckmark product={product} update={update} values={values}></ButtonOrCheckmark>
      </div>
    ));
  };
  // Render Welcome Paragraph part of multi-step form
  return (
    <div>
      {!readMode && !loading && (
        <div>
          <div>
            <Modal
              isOpen={createFormsOpened}
              onRequestClose={closeModal('create-forms')}
              ariaHideApp={false}
              className="dashboard-forms-modal"
            >
              <div>
                <div className="row">
                  <div className="col-md-6 my-auto dashboard-forms-input-content-col-1">
                    <div className="dashboard-forms-title mb-3">Add</div>
                  </div>
                  <div className="col-md-6 dashboard-forms-input-content-col-3">
                    <select
                      name="select-form"
                      selected="products"
                      onChange={changeForm}
                      disabled={submitAuthDisabled}
                      className="custom-select mb-3"
                      required
                    >
                      <option value="products">Products</option>
                      {/*                 <option value="services">Services</option> */}
                    </select>
                  </div>
                  <div className="dashboard-forms-close">
                    <button
                      aria-label={`Close Forms Modal`}
                      onClick={closeModal('create-forms')}
                      className="fas fa-times fa-2x close-modal"
                    ></button>
                  </div>
                </div>
                <Form data={data} onSubmit={onSubmit}>
                  <div className="row">
                    <div className="col-md-6 dashboard-forms-input-content-col-1">
                      <Input
                        name={formDetails.input_one}
                        placeholder={formDetails.input_one_placeholder}
                        disabled={submitAuthDisabled}
                        className="product-name-input"
                        required
                        label={null}
                      />
                    </div>
                    <div className="col-md-6 dashboard-forms-input-content-col-3">
                      {nationalities.length && (
                        <Select
                          name={formDetails.input_two}
                          placeholder={formDetails.input_two_placeholder}
                          className="product-name-input"
                          disabled={submitAuthDisabled}
                          required
                        >
                          <option value="">--Nationality--</option>
                          {nationalities.map((n) => (
                            <option key={n.id} value={n.id}>
                              {n.nationality}
                            </option>
                          ))}
                        </Select>
                      )}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-4 dashboard-forms-input-content-col-1">
                      <Input
                        name={formDetails.input_three}
                        step="0.01"
                        placeholder={formDetails.input_three_placeholder}
                        className="product-name-input"
                        disabled={submitAuthDisabled}
                        required
                      />
                    </div>
                    <div className="col-md-4 dashboard-forms-input-content-col-2">
                      <Input
                        name={formDetails.input_four}
                        type="number"
                        placeholder={formDetails.input_four_placeholder}
                        min="1"
                        className="product-name-input"
                        disabled={submitAuthDisabled}
                        required
                      />
                    </div>
                    <div className="col-md-4 dashboard-forms-input-content-col-3">
                      <Select
                        name={formDetails.input_five}
                        placeholder={formDetails.input_five_placeholder}
                        className="product-name-input"
                        disabled={submitAuthDisabled}
                        required
                      >
                        <option value="">{`--${
                          formDetails.input_five === 'product_size' ? 'Size' : 'Scope'
                        }--`}</option>
                        <option value="Bite Size">Bite Size</option>
                        <option value="Quarter">Quarter</option>
                        <option value="Half">Half</option>
                        <option value="Full">Full</option>
                      </Select>
                    </div>
                  </div>
                  {selected === 'products' && (
                    <div className="row">
                      <div className="col-md-6 dashboard-forms-input-content-col-1">
                        <DateInput
                          name={formDetails.input_seven}
                          placeholderText={formDetails.input_seven_placeholder}
                          dateFormat="yyyy/MM/dd"
                          minDate={new Date()}
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                          disabled={submitAuthDisabled}
                          className="product-name-input"
                          required
                        />
                      </div>
                      <div className="col-md-6 dashboard-forms-input-content-col-3">
                        <DateInput
                          name={formDetails.input_eight}
                          placeholderText={formDetails.input_eight_placeholder}
                          showTimeSelect
                          showTimeSelectOnly
                          dateFormat="h:mm aa"
                          disabled={submitAuthDisabled}
                          className="last-date-input"
                          required
                        />
                      </div>
                    </div>
                  )}
                  <div className="first-row">
                    <Textarea
                      name={formDetails.input_six}
                      placeholder={formDetails.input_six_placeholder}
                      disabled={submitAuthDisabled}
                      className="product-name-input"
                      required
                    />
                  </div>
                  <div className="first-row">
                    {selected === 'products' && (
                      <MultiImageInput
                        name="product_images"
                        dropbox_label="Click or drag-and-drop to upload one or more images"
                        disabled={submitAuthDisabled}
                        className="product-images"
                        required
                      />
                    )}
                    {selected === 'services' && (
                      <MultiImageInput
                        name="service_images"
                        dropbox_label="Click or drag-and-drop to upload one or more images"
                        disabled={submitAuthDisabled}
                        className="product-images"
                        required
                      />
                    )}
                  </div>
                  {festivalList.length ? (
                    <>
                      <div>{`Which festival does this ${
                        selected === 'products' ? 'product' : 'service'
                      } belong to?`}</div>
                      <div>
                        <Select
                          name={formDetails.input_ten}
                          className="product-name-input"
                          disabled={submitAuthDisabled}
                          required
                        >
                          <option value="">--Select--</option>
                          {/*                     <option value={props.location.title}>Selected Festival &#40;{props.location.title}&#41;</option>
                    <option value="uncreated">Future(uncreated) Festivals</option>
                    <option value="all_by_subscription">upcoming (created) festivals</option> */}

                          {festivalList.map((f) => (
                            <option key={f.festival_id} value={f.festival_id}>
                              {f.festival_name}
                            </option>
                          ))}
                        </Select>
                      </div>
                    </>
                  ) : null}

                  <div className="first-row">
                    <button type="submit" className="dashboard-forms-add-btn">
                      Add
                    </button>
                  </div>
                </Form>
              </div>
            </Modal>
          </div>
          <div className="dashboard">
            <div className="row">
              <div className="col-lg-8 px0">
                <div className="dashboard-title">Add Products To Festival</div>
              </div>
              {userRole &&
              (userRole.includes('ADMIN') ||
                userRole.includes('SPONSOR') ||
                userRole.includes('VENDOR')) ? (
                <div className="col-lg-4 px-0 dashboard-section dashboard-forms-btn-content">
                  <button onClick={openModal('create-forms')} className="dashboard-forms-btn">
                    <span className="fas fa-3x fa-plus-circle" />
                  </button>
                </div>
              ) : null}
            </div>
          </div>

          <div className="mb-5">
            <div className="dashboard-title text-center">Products</div>
            {/* <div className="row">
                <div className="col-6">
                  Product 1: {products[0].business_name}</div>
                <div className="col-6">
                  {input ? (<input type="submit" className="" value="Add to Festival"></input>) : (<span className="checkmark">
                  <div className="checkmark_circle"></div>
                    <div className="checkmark_stem"></div>
                    <div className="checkmark_kick"></div>
                  </span>)}
                </div>
              </div> */}
            <div className="row">{!loading && renderProducts(products)}</div>
          </div>
          <div>
            {!readMode && (
              <Form data={values} onSubmit={() => nextStep()}>
                <div /* className="apply-to-host-navigation"> */>
                  <span className="apply-to-host-navigation-spacing"></span>
                  <Link exact="true" to={'/'} className="continue-btn">
                    Finish
                  </Link>
                </div>
              </Form>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductsToFestival;
