// Libraries
import React, { useState, useEffect, useContext, useRef } from 'react';
import axios from 'axios';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import {
  CheckboxGroup,
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
  UserSelector,
} from '../../../EasyForm';
import { canadaProvincesTerritories } from '../../../Functions/Functions';

// Styling
// import "react-sweet-progress/lib/style.css";

const MenuItemsForm = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const { values, updateMenuList, readMode } = props;
  const standAloneStep = props.standAloneStep;

  const formRef = useRef();

  // Set initial state
  const [nationalities, setNationalities] = useState([]);
  const [menuItems, setMenuItems] = useState(values.menu_list || []);
  const [specials, setSpecials] = useState([]);
  const [selectedMenuItem, setSelectedMenuItem] = useState({});
  const [isAdmin, setIsAdmin] = useState(
    appContext.state.user &&
      appContext.state.user.role &&
      appContext.state.user.role.includes('ADMIN'),
  );
  const userRole = appContext.state.user.role;

  const updateMenuAddress = () => {
    if (formRef && formRef.current) {
      const formValues = formRef.current.getValues();

      if (formValues.menuLocation === 'business') {
        formValues.menuAddressLine1 = values.address_line_1;
        formValues.menuAddressLine2 = values.address_line_2;
        formValues.menuCity = values.business_city;
        formValues.menuProvinceTerritory = values.state;
        formValues.menuPostalCode = values.postal_code;
      } else if (formValues.menuLocation === 'residence') {
        formValues.menuAddressLine1 = values.residential_address_line_1;
        formValues.menuAddressLine2 = values.residential_address_line_2;
        formValues.menuCity = values.residential_city;
        formValues.menuProvinceTerritory = values.residential_state;
        formValues.menuPostalCode = values.residential_postal_code;
      }

      setSelectedMenuItem({
        ...formValues,
      });
    }
  };

  const updateState = (newList) => {
    updateMenuList(newList);
    setMenuItems(newList);
    setSelectedMenuItem({});
  };

  // Add menu item helper function
  const onSubmit = async (data) => {
    window.scrollTo(0, 0);

    let newList = [...menuItems];

    const response = await axios({
      method: 'GET',
      url: `/user/${data.userEmail}`,
    });

    if (
      (userRole && (userRole.includes('RESTAURANT') || userRole.includes('RESTAURANT_PENDING'))) ||
      !response.data.message
    ) {
      if (data.id) {
        newList = [...newList.filter((m) => parseInt(m.id) !== parseInt(data.id)), data];
      } else {
        data.id = menuItems.length + 1;
        newList.push(data);
      }

      updateState(newList);
    }
  };

  // Edit menu item helper function
  const editMenuItem = (m) => {
    setSelectedMenuItem(m);
  };

  // Remove menu item helper function
  const removeMenuItem = (menuItem) => {
    const newList = menuItems.filter((m) => m.id !== menuItem.id);

    updateState(newList);
  };

  // Fetch nationalities helper function
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };

  const fetchSpecialsList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/external_api/kodidi/specials_list',
    });

    setSpecials(response.data.specialsList);
  };

  useEffect(() => {
    window.scrollTo(0, 0);

    fetchNationalities();
    fetchSpecialsList();
  }, []);

  useEffect(() => {
    setIsAdmin(
      appContext.state.user &&
        appContext.state.user.role &&
        appContext.state.user.role.includes('ADMIN'),
    );
  }, [appContext]);

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      {!readMode ? (
        <h1 className="apply-to-host-step-name text-center">Create Menu Items</h1>
      ) : (
        <>
          <h6>Menu Items</h6>
          <hr />
        </>
      )}

      {!readMode ? (
        <div className="row">
          <div className="col-12 px-0">
            <Form data={selectedMenuItem} onSubmit={onSubmit} readMode={readMode} formRef={formRef}>
              <MultiImageInput
                name="menuImages"
                label="Images"
                dropbox_label="Click or drag-and-drop to upload one or more images"
                dir="menu-item-images"
                accept="image/x-png,image/gif,image/jpeg"
                required
              />

              {isAdmin && standAloneStep && (
                <UserSelector name="userEmail" label="User Email" required />
              )}

              <Input name="id" type="hidden" />
              <Input name="menuName" label="Food Name" required />
              <Select name="menuNationality" label="Food Nationality" required>
                <option value="">--Select--</option>
                {nationalities.map((n) => (
                  <option key={n.id} value={n.id}>
                    {n.nationality}
                  </option>
                ))}
              </Select>

              <Select name="menuType" label="Type" required>
                <option value="">--Select--</option>
                <option value="Appetizer">Appetizer</option>
                <option value="Main Course">Main Course</option>
                <option value="Dessert">Dessert</option>
                <option value="Drink">Drink</option>
              </Select>

              {/* <Input
                name="menuPrice"
                label="Price"
                type="number"
                step=".01"
                placeholder="Price (Ex. 14.99)"
                required
              /> */}

              {/* <Select name="menuQuantity" label="Quantity" required>
                <option value="">--Select--</option>
                <option value={1}>1 person</option>
                <option value={5}>5 people</option>
                <option value={10}>10 people</option>
                <option value={15}>15 people</option>
                <option value={20}>20 people</option>
                <option value={25}>25 people</option>
              </Select> */}

              <Input
                type="number"
                step="1"
                name="menuQuantity"
                label="Quantity Available"
                required
              />

              <Select name="menuSize" label="Size" required>
                <option value="">--Select--</option>
                <option value="Bite Size">Bite Size</option>
                <option value="Quarter Plate">Quarter Plate</option>
                <option value="Half Plate">Half Plate</option>
                <option value="Full Plate">Full Plate</option>
                <option value="Small Tray">Small Tray</option>
                <option value="Medium Tray">Medium Tray</option>
                <option value="Large Tray">Large Tray</option>
              </Select>

              <DateInput
                name="menuEndTime"
                label="Order by Time (Deadline to Reserve)"
                showTimeSelect
                showTimeSelectOnly
                dateFormat="h:mm aa"
                required
              />

              <CheckboxGroup
                name="dietaryRestrictions"
                label="Dietary Restrictions (Optional)"
                options={[
                  ['Vegetarian', 'vegetarian'],
                  ['Vegan', 'vegan'],
                  ['Gluten-Free', 'glutenFree'],
                  ['Halal', 'halal'],
                ]}
              />

              <CheckboxGroup
                name="daysAvailable"
                label="Days Available"
                options={[
                  ['Monday', 'available_on_monday'],
                  ['Tuesday', 'available_on_tuesday'],
                  ['Wednesday', 'available_on_wednesday'],
                  ['Thursday', 'available_on_thursday'],
                  ['Friday', 'available_on_friday'],
                  ['Saturday', 'available_on_saturday'],
                  ['Sunday', 'available_on_sunday'],
                ]}
                required
              />

              <Select name="menuSpiceLevel" label="Spice Level (Optional)">
                <option value="">--Select--</option>
                <option value="Mild">Mild</option>
                <option value="Medium">Medium</option>
                <option value="Hot">Hot</option>
              </Select>

              {/* <Select
                name="menuLocation"
                label="Where is the food located?"
                onChange={updateMenuAddress}
              >
                <option value="">--Select--</option>
                <option value="business">Business Address</option>
                <option value="residence">Residential Address</option>
              </Select> */}

              <Input name="menuAddressLine1" label="Street Address" required />
              <Input name="menuAddressLine2" label="Unit Address" />
              <Input name="menuCity" label="City" required />

              <Select name="menuProvinceTerritory" label="Province or Territory" required>
                {canadaProvincesTerritories()}
              </Select>
              <Input name="menuPostalCode" label="Postal Code" maxLength="7" required />

              <Textarea name="menuDescription" label="Description" required />

              {/* <Select
                name="special"
                label="What type of special is available for this item?"
              >
                <option value="">--Select--</option>
                {specials.map((s) => (
                  <option key={s.id} value={s.id}>
                    {s.special_name}
                  </option>
                ))}
              </Select> */}

              {/* {menuItems.length < 3 && ( */}
              <button type="submit" disabled={values.submitAuthDisabled} className="add-menu-btn">
                Add
              </button>
              {/* )} */}
            </Form>
          </div>
          {menuItems.length ? (
            <div className="col-12 px-0">
              {menuItems.map((m) => (
                <div className="menu-items-form__item" key={m.menuName}>
                  <img className="menu-items-form__img" src={m.menuImages[0]} alt={m.menuName} />
                  <div className="menu-items-form__ctrl">
                    <div className="menu-items-form__ctrl__title">{m.menuName}</div>
                    <div className="menu-items-form__ctrl__actions">
                      <i className="fa fa-edit" title="edit" onClick={() => editMenuItem(m)} />
                      <span> | </span>
                      <i className="fa fa-times" title="remove" onClick={() => removeMenuItem(m)} />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ) : null}
        </div>
      ) : (
        <div className="row justify-content-between">
          {menuItems.map((m, index) => (
            <div key={index} className="col-lg-6 col-xl-4 menu-items-preview-content">
              <img src={m.menuImages[0]} alt={m.menuName} className="menu-items-preview-image" />
              <div>{m.menuName}</div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default MenuItemsForm;
