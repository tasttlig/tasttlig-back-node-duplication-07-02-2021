import React, { useState } from 'react';

const SampleSaleDonation = () => {
  return (
    <div className="create-food-sample">
      {successMessage && userRole && appContext.state.user.role.includes('HOST_PENDING') && (
        <div className="btn btn-success mb-3">{successMessage}</div>
      )}

      <div className="create-food-sample-title">
        {heading ? heading : 'Create Product or Service'}
      </div>
      <div classname="container-lg">
        <CreateSampleSaleDonation></CreateSampleSaleDonation>
      </div>
    </div>
  );
};

export default SampleSaleDonation;
