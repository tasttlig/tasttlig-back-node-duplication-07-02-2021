// Libraries
import React, { useState, useContext } from 'react';
// import { Link } from "react-router-dom";
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { FileUpload, Form, Input, Select, Textarea } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const HostPersonalInfo = (props) => {
  console.log('props', props);
  const appContext = useContext(AppContext);

  const { nextStep, prevStep, update, values, readMode } = props;

  const [showOptions, setShowOptions] = useState(values.is_host === 'yes');

  const onSubmit = (data) => {
    update(data);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Apply To Host</h1>
        ) : (
          <>
            <h6>Host Personal Info</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          {showOptions && (
            <>
              <h7>Host First Name:</h7>
              <p>{values.first_name}</p>

              <h7>Host Last Name:</h7>
              <p>{values.last_name}</p>

              <h7>Host Email:</h7>
              <p>{values.email}</p>

              <h7>Street name of host:</h7>
              <p>{`${values.street_number} , ${values.street_name}`}</p>

              <h7>Postal code of host:</h7>
              <p>{values.user_zip_postal_code}</p>

              <h7>City of host:</h7>
              <p>{values.user_city}</p>

              <h7>State of host:</h7>
              <p>{values.user_state}</p>

              <h7>Country of host:</h7>
              <p>{values.user_country}</p>
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              {appContext.state.signedInStatus ? (
                <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                  Back
                </span>
              ) : (
                <span className="apply-to-host-navigation-spacing"></span>
              )}
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default HostPersonalInfo;
