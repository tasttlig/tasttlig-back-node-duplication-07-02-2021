// Libraries
import React, { useState, useContext } from 'react';
// import { Link } from "react-router-dom";
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { FileUpload, Form, Input, Select, Textarea } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const ApplyToHostForm = (props) => {
  console.log('props', props);
  const appContext = useContext(AppContext);

  const { nextStep, prevStep, update, values, readMode } = props;

  const [showOptions, setShowOptions] = useState(values.is_host === 'yes');

  const onSubmit = (data) => {
    update(data);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Apply To Host</h1>
        ) : (
          <>
            <h6>Apply To Host</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          {showOptions && (
            <>
              <div>
                <h7>Have you hosted anything before? [Party, celebration, festivals]</h7>
                <p>
                  {values.has_hosted_anything_before !== null ? (
                    values.has_hosted_anything_before.map((item) => {
                      return item + ', ';
                    })
                  ) : (
                    <div>
                      <p></p>
                      <h6>Anything else that you have hosted:</h6>
                      <p>{values.has_hosted_other_things_before}</p>
                      <p></p>
                    </div>
                  )}
                </p>
              </div>

              <h7>Do you have a restaurant, or work for a restaurant with a physical location?</h7>
              <p>{values.have_a_restaurant}</p>

              <h7>What kind of cuisine do you serve to your customers? [Nationalities]</h7>
              {values.has_hosted_anything_before !== null ? (
                <p>
                  {values.cuisine_type.map((item) => {
                    return item + ', ';
                  })}
                </p>
              ) : (
                <p></p>
              )}

              <h7>Do you have seating in your restaurant to host guests?</h7>
              <p>{values.seating_option}</p>

              <h7>Would you like more people to discover and experience your National cuisine?</h7>
              <p>{values.want_people_to_discover_your_cuisine}</p>

              <h7>Are you able to provide food tasting samples of your menu to our guests?</h7>
              <p>{values.able_to_provide_food_samples}</p>

              <h7>Are you able to explain the origins of your tasttlig samples to our guests?</h7>
              <p>{values.able_to_explain_the_origins_of_tasting_samples}</p>

              <h7>Are you able to proudly showcase your culture with food and entertainment?</h7>
              <p>{values.able_to_proudly_showcase_your_culture}</p>

              <h7>Are you able to provide a private dining experience for our guests?</h7>
              <p>{values.able_to_provie_private_dining_experience}</p>

              <h7>Are you able to provide 3 or more course meals to our guests if necessary?</h7>
              <p>{values.able_to_provide_3_or_more_course_meals_to_guests}</p>

              <h7>Are you able to provide live entertainment to our guests?</h7>
              <p>{values.able_to_provide_live_entertainment}</p>

              <h7>Are you able to provide any other form of entertainment to our guests?</h7>
              <p>{values.able_to_provide_other_form_of_entertainment}</p>

              <h7>Are you able to provide games about your cuisine and culture to our guests?</h7>
              <p>{values.able_to_provide_games_about_culture_cuisine}</p>

              <h7>Are you able to provide excellent customer service to our guests?</h7>
              <p>{values.able_to_provide_excellent_customer_service}</p>

              <h7>Have you ever hosted a Tasttlig festival or experiences before?</h7>
              <p>{values.hosted_tasttlig_festival_before}</p>

              <h7>
                Are you able to abide by the health and safety regulations of the government and
                Tasttlig platform?
              </h7>
              <p>{values.able_to_abide_by_health_safety_regulations}</p>

              <h7>Why do you want to be a Tasttlig Host?</h7>
              <p>{values.host_description}</p>

              {/* <FileUpload
                name="host_selection_resume"
                dir="host-selection-resume"
                accept="application/pdf"
                label="Upload your resume"
                required
              /> */}

              <FileUpload
                name="host_video_url"
                dir="host-selection-videos"
                accept="video/*"
                label="Upload 1 minute video on why you want to be selected (Optional)"
              />
              {/* <Input name="host_youtube_link" label="YouTube link (Optional)" /> */}
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              {appContext.state.signedInStatus ? (
                <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                  Back
                </span>
              ) : (
                <span className="apply-to-host-navigation-spacing"></span>
              )}
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default ApplyToHostForm;
