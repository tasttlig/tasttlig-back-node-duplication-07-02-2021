// Libraries
import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

// Components
import MenuItemsForm from './MenuItemsForm';
import EntertainersProductForm from './EntertainersProductForm';
import VenuesProductForm from './VenuesProductForm';
import FoodSamplesForm from './FoodSamplesForm';

// Styling
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const ProductForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;
  const standAloneStep = props.standalone;

  const updateMenuList = (menuList) => {
    update({
      menu_list: menuList,
    });
  };

  const updateSampleLinks = async (sampleLinks) => {
    update({
      sample_links: sampleLinks,
    });
    await updateEntertainersProduct(sampleLinks);
    nextStep();
  };

  const updateVenue = async ({ venue_name, venue_description, venue_photos }) => {
    update({
      venue_name: venue_name,
      venue_description: venue_description,
      venue_photos: venue_photos,
    });
    await updateVenuesProduct(venue_name, venue_description, venue_photos);
    nextStep();
  };

  const updateMenuItem = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateMenuItem',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  // Update food samples list helper function
  const updateFoodSampleList = (foodSampleList) => {
    update({ foodSampleList });
  };

  const updateEntertainersProduct = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateEntertainersProduct',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const updateVenuesProduct = async (venue_name, venue_description, venue_photos) => {
    let data = {
      venue_name,
      venue_description,
      venue_photos,
    };

    const response = await axios({
      method: 'PUT',
      url: '/user/updateVenuesProduct',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const submit = async () => {
    if (values.business_category === 'Food' && !values.menu_list.length) {
      toast('Please add at least one menu item.', {
        type: 'error',
        autoClose: 2000,
      });

      return;
    }

    if (!standAloneStep) {
      await updateMenuItem(values.menu_list);
    }

    nextStep();
  };

  // Add food sample helper function
  const submitFoodSample = async () => {
    if (values.isCreateFoodSamples && !values.foodSampleList.length) {
      toast('Please add at least one food sample.', {
        type: 'error',
        autoClose: 2000,
      });

      return;
    }

    nextStep();
  };

  // Render Product part of multi-step form
  return (
    <div>
      <div className={!readMode ? 'product-items-form' : ''}>
        {values.isCreateFoodSamples || values.isFoodSamples ? (
          <>
            <FoodSamplesForm
              values={values}
              updateFoodSampleList={updateFoodSampleList}
              readMode={readMode}
            />
          </>
        ) : null}

        {values.business_category === 'Food' && (
          <MenuItemsForm
            values={values}
            updateMenuList={updateMenuList}
            readMode={readMode}
            standAloneStep={standAloneStep}
          />
        )}

        {(values.business_category === 'Entertainment' || values.business_category === 'MC') && (
          <EntertainersProductForm
            values={values}
            prevStep={prevStep}
            updateSampleLinks={updateSampleLinks}
            readMode={readMode}
          />
        )}

        {values.business_category === 'Venues' && (
          <VenuesProductForm
            values={values}
            prevStep={prevStep}
            updateVenue={updateVenue}
            readMode={readMode}
          />
        )}

        {!readMode && (
          <div className="apply-to-host-navigation">
            {values.isFoodSamples && (
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
            )}
            <span className="apply-to-host-navigation-spacing"></span>
            <span
              onClick={values.isCreateFoodSamples ? submitFoodSample : submit}
              disabled={values.submitAuthDisabled}
              className={standAloneStep ? 'continue-btn pull-right' : 'continue-btn'}
            >
              Continue
            </span>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductForm;
