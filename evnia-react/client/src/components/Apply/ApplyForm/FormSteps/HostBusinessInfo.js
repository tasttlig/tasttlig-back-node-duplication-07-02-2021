// Libraries
import React, { useState, useContext } from 'react';
// import { Link } from "react-router-dom";
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { FileUpload, Form, Input, Select, Textarea } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const HostBusinessInfo = (props) => {
  const appContext = useContext(AppContext);

  const { nextStep, prevStep, update, values, readMode } = props;

  const [showOptions, setShowOptions] = useState(values.is_host === 'yes');

  const onSubmit = (data) => {
    update(data);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Apply To Host</h1>
        ) : (
          <>
            <h6>Host Food Sample</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          {showOptions && (
            <>
              <h7>Business Name:</h7>
              <p>{values.business_name}</p>

              <h7>Business Type:</h7>
              <p>{values.business_type}</p>

              <h7>Food Business Type:</h7>
              <p>{values.food_business_type}</p>

              <h7>Host Email:</h7>
              <p>{values.email}</p>

              <h7>Address of Business:</h7>
              <p>{`${values.business_unit} , ${values.business_street_number}, ${values.business_street_name}`}</p>

              <h7>Postal code of Business:</h7>
              <p>{values.zip_postal_code}</p>

              <h7>City of Business:</h7>
              <p>{values.city}</p>

              <h7>State of Business:</h7>
              <p>{values.state}</p>

              <h7>Country of Business:</h7>
              <p>{values.country}</p>

              <h7>City of Business registered:</h7>
              <p>{values.business_registered_location}</p>

              <h7>Business Logo: </h7>
              <div>
                <img src={values.business_details_logo} />
              </div>

              <h7>Food Handling Certificate: </h7>
              <div>
                <img src={values.food_handling_certificate} />
              </div>
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              {appContext.state.signedInStatus ? (
                <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                  Back
                </span>
              ) : (
                <span className="apply-to-host-navigation-spacing"></span>
              )}
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default HostBusinessInfo;
