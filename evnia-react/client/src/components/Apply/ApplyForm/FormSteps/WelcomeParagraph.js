// Libraries
import React, { useState, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { Checkbox, Form, Input } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const WelcomeParagraph = (props) => {
  const { nextStep, update, values, readMode } = props;

  // Set initial state
  const [existingUser, setExistingUser] = useState(null);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const onSubmit = (data) => {
    if (data.accept_checkbox) {
      nextStep();
    }
  };

  // Render Welcome Paragraph part of multi-step form
  return (
    <div>
      {!readMode && (
        <div className="apply-to-host">
          <div className="apply-to-host-step-name">Welcome To Tasttlig!</div>
          <div className="welcome-paragraph">
            Tasttlig is the largest multicultural food tasting festival platform in the world. We
            have over 40,000 visitors during the months of our festivals. We send our guests to you
            to taste your food. For each food sample you give out, we pay you $2.00. Your total
            samples sold are deposited to your bank account at the end of each festival. Tasttlig
            festival happens four times a year in your city. To become a vendor in our festival,
            please complete the form below.
          </div>
          <Form data={values} onSubmit={onSubmit}>
            <div>
              <Checkbox name="accept_checkbox" label="Do you agree?" required={true} />
            </div>
            <div>
              {!readMode && (
                <div className="apply-to-host-navigation">
                  <span className="apply-to-host-navigation-spacing"></span>
                  <input
                    type="submit"
                    value="Continue"
                    disabled={values.submitAuthDisabled}
                    className="continue-btn"
                  />
                </div>
              )}
            </div>
          </Form>
        </div>
      )}
    </div>
  );
};

export default WelcomeParagraph;
