// Libraries
import React, { useState, useContext } from 'react';
// import { Link } from "react-router-dom";
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { FileUpload, Form, Input, Select, Textarea } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const HostFoodSampleForm = (props) => {
  console.log('props', props);
  const appContext = useContext(AppContext);

  const { nextStep, prevStep, update, values, readMode } = props;

  const [showOptions, setShowOptions] = useState(values.is_host === 'yes');

  const onSubmit = (data) => {
    update(data);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Apply To Host</h1>
        ) : (
          <>
            <h6>Host Food Sample</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          {showOptions && (
            <>
              <h7>Food Sample name:</h7>
              <p>{values.title}</p>

              {/* <h7>Address of host:</h7>
              <p>{values.address}</p> */}
{/* 
              <h7>City of host:</h7>
              <p>{values.city}</p>

              <h7>State of host:</h7>
              <p>{values.state}</p> */}
{/* 
              <h7>Postal code of host:</h7>
              <p>{values.postal_code}</p> */}

              {/* <h7>Country of host:</h7>
              <p>{values.country}</p> */}

              <h7>quantity of product</h7>
              <p>{values.quantity}</p>

              <h7>Spice level of product: </h7>
              <p>{values.spice_level}</p>

              <h7>Sample size: </h7>
              <p>{values.sample_size}</p>

              <h7>Festivals applicant wants to apply: </h7>
            
                {values.festival_selected !== null ? (
                  <p>
                    {values.festival_selected.map((item) => {
                    return item + ', ';
                  })}
                  </p>
                  ) : null}
                

              <h7>Description of product: </h7>
              <p>{values.description}</p>

              <h7>Picture of product: </h7>
              <div>
                <img src={values.image_url} />
              </div>
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              {appContext.state.signedInStatus ? (
                <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                  Back
                </span>
              ) : (
                <span className="apply-to-host-navigation-spacing"></span>
              )}
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default HostFoodSampleForm;
