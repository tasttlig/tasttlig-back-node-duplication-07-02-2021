// Libraries
import React, { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';

// Components
import {
  Checkbox,
  Select,
  Form,
  Input,
  CheckboxGroup,
  Textarea,
  DateInput,
  MultiImageInput,
} from '../../../EasyForm';
import { formatDate, formattedTimeToDateTime } from '../../../Functions/Functions';
import BackgroundImage from '../../../../assets/images/background-vendor-image.png';
import BackgroundGirlImage from '../../../../assets/images/background-vendor-girl-image.png';
import tasttligLogoBlack from '../../../../assets/images/tasttlig-logo-black.png';
import CutoffImage from '../../../../assets/images/vendor-page-cutoff-image.png';
import SmallVendorImage from '../../../../assets/images/small-vendor-image.png';

const VendorProductsAndServices = (props) => {
  const { nextStep, update, values, readMode } = props;
  let data = {};
  // Set initial state
  const [existingUser, setExistingUser] = useState(null);
  const [formDetails, setFormDetails] = useState({
    input_one: 'product_name',
    input_one_placeholder: 'Name Your Product',
    input_two: 'product_made_in_nationality_id',
    input_three: 'product_price',
    input_three_placeholder: 'Price',
    input_four: 'product_quantity',
    input_four_placeholder: 'Quantity',
    input_five: 'product_size',
    input_five_placeholder: 'Size',
    input_six: 'product_manufacture_date',
    input_six_placeholder: 'Manufacture Date',
    input_seven: 'product_expiry_date',
    input_seven_placeholder: 'Expiry Date',
    input_eight: 'product_description',
    input_eight_placeholder: 'Description',
    input_nine: 'product_images',
    input_ten: 'product_festival_id',
  });
  const [selected, setSelected] = useState('products');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [nationalities, setNationalities] = useState([]);
  const [load, setLoad] = useState(false);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      nextStep();
      //window.location.href = `/festival/${festivalId}`;
    }, 2000);

    toast(`Success! Thank you for creating ${selected}!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSubmitAuthDisabled(true);
  };

  const submitProductOrService = async (data) => {
    window.scrollTo(0, 0);

    let url = '';
    if (selected === 'products') {
      data.product_creator_type = userRole[0];
      url = '/products/add';
    } else if (selected === 'services') {
      data.service_creator_type = userRole[0];
      url = '/services/add';
    }

    try {
      const response = await axios({
        method: 'POST',
        url,
        data,
      });

      if (response && response.data && response.data.success && userRole) {
        //defaultOnCreatedAction(data.product_festival_id);
        defaultOnCreatedAction();
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    const { ...rest } = values;

    let data = {
      ...rest,
    };

    if (selected === 'products') {
      /*       data.product_expiry_time = formattedTimeToDateTime(
        values.product_expiry_time.toString().substring(16, 21)
      ); */
      data.product_expiry_date = formatDate(values.product_expiry_date);
      data.product_manufacture_date = formatDate(values.product_manufacture_date);

      await submitProductOrService(data);
    } else {
      await submitProductOrService(data);
    }
  };

  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    fetchNationalities();
  }, []);

  // Select product form helper function
  const productSelected = () => {
    setFormDetails({
      input_one: 'product_name',
      input_one_placeholder: 'Name Your Product',
      input_two: 'product_made_in_nationality_id',
      input_three: 'product_price',
      input_three_placeholder: 'Price',
      input_four: 'product_quantity',
      input_four_placeholder: 'Quantity',
      input_five: 'product_size',
      input_five_placeholder: 'Size',
      input_six: 'product_manufacture_date',
      input_six_placeholder: 'Manufacture Date',
      input_seven: 'product_expiry_date',
      input_seven_placeholder: 'Expiry Date',
      input_eight: 'product_description',
      input_eight_placeholder: 'Description',
      input_nine: 'product_images',
      //input_ten: "product_festivals_id",
    });
  };

  // Select service form helper function
  const serviceSelected = () => {
    setFormDetails({
      input_one: 'service_name',
      input_one_placeholder: 'Name Your Service',
      input_two: 'service_nationality_id',
      input_three: 'service_price',
      input_three_placeholder: 'Price',
      input_four: 'service_capacity',
      input_four_placeholder: 'Capacity',
      input_five: 'service_size_scope',
      input_five_placeholder: 'Scope',
      input_six: null,
      input_six_placeholder: null,
      input_seven: null,
      input_seven_placeholder: null,
      input_eight: 'service_description',
      input_eight_placeholder: 'Description',
      input_nine: 'service_images',
      //input_ten: "service_festival_id",
    });
  };

  const changeForm = (event) => {
    if (event.target.value === 'services') {
      setSelected('services');
      serviceSelected();
    } else {
      setSelected('products');
      productSelected();
    }
  };

  return (
    <div className="container-xxl">
      <div className="row">
        <div className="col-4 left-side-vendor">
          <div className="row vendor-tasttlig-container">
            <img
              className="col-md-6 tasttlig-logo-vendor"
              onClick={() => (window.location.href = '/')}
              src={tasttligLogoBlack}
            ></img>
            <div
              className="col-4 tasttlig-vendor-title"
              onClick={() => (window.location.href = '/')}
            >
              Vendor
            </div>
          </div>
        </div>
        <div className="col-8 right-side-vendor">
          <div className="container vendor-details-container">
            <div className="vendor-profile-name-picture-container">
              <div className="user-full-name">
                {appContext.state.user.first_name} {appContext.state.user.last_name}
              </div>
              {appContext.state.user.profile_image_link ? (
                <img
                  src={appContext.state.user.profile_image_link}
                  alt={`${appContext.state.user.first_name} ${appContext.state.user.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'main-navbar-account-profile-picture' : 'loading-image'}
                />
              ) : (
                <span className="fas fa-user-circle fa-3x main-navbar-account-default-picture"></span>
              )}
            </div>
            <div className="container title-subtitle-pas-container">
              <div className="vendor-details-title">Products & Services</div>
              <div className="sub-title-vendor">
                Upload your products and services you would like to vend to Tasttlig
              </div>
              <div className="row vendor-dropdown">
                <div>Select the Type of product</div>
                <select
                  name="select-form"
                  selected="products"
                  className="product-service-dropdown"
                  onChange={changeForm}
                  disabled={submitAuthDisabled}
                  required
                >
                  <option value="products">Products</option>
                  <option value="services">Services</option>
                </select>
              </div>
            </div>
            <Form data={data} onSubmit={onSubmit}>
              <div className="row">
                <div className="col-md-6 dashboard-forms-input-content-col-1">
                  <Input
                    name={formDetails.input_one}
                    placeholder={formDetails.input_one_placeholder}
                    disabled={submitAuthDisabled}
                    className="product-name-input"
                    required
                    label={null}
                  />
                </div>
                <div className="col-md-6 dashboard-forms-input-content-col-3">
                  {nationalities.length && (
                    <Select
                      name={formDetails.input_two}
                      placeholder={formDetails.input_two_placeholder}
                      className="product-name-input"
                      disabled={submitAuthDisabled}
                      required
                    >
                      <option value="">--Nationality--</option>
                      {nationalities.map((n) => (
                        <option key={n.id} value={n.id}>
                          {n.nationality}
                        </option>
                      ))}
                    </Select>
                  )}
                </div>
              </div>
              <div className="row">
                <div className="col-md-4 dashboard-forms-input-content-col-1">
                  <Input
                    name={formDetails.input_three}
                    step="0.01"
                    placeholder={formDetails.input_three_placeholder}
                    className="product-name-input"
                    disabled={submitAuthDisabled}
                    required
                  />
                </div>
                <div className="col-md-4 dashboard-forms-input-content-col-2">
                  <Input
                    name={formDetails.input_four}
                    type="number"
                    placeholder={formDetails.input_four_placeholder}
                    min="1"
                    className="product-name-input"
                    disabled={submitAuthDisabled}
                    required
                  />
                </div>
                <div className="col-md-4 dashboard-forms-input-content-col-3">
                  <Select
                    name={formDetails.input_five}
                    placeholder={formDetails.input_five_placeholder}
                    className="product-name-input"
                    disabled={submitAuthDisabled}
                    required
                  >
                    <option value="">{`--${
                      formDetails.input_five === 'product_size' ? 'Size' : 'Scope'
                    }--`}</option>
                    <option value="Bite Size">Bite Size</option>
                    <option value="Quarter">Quarter</option>
                    <option value="Half">Half</option>
                    <option value="Full">Full</option>
                  </Select>
                </div>
              </div>
              {selected === 'products' && (
                <div className="row">
                  <div className="col-md-6 dashboard-forms-input-content-col-1">
                    <DateInput
                      name={formDetails.input_six}
                      placeholderText={formDetails.input_six_placeholder}
                      dateFormat="yyyy/MM/dd"
                      minDate={new Date()}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      disabled={submitAuthDisabled}
                      className="product-name-input"
                      required
                    />
                  </div>
                  <div className="col-md-6 dashboard-forms-input-content-col-3">
                    <DateInput
                      name={formDetails.input_seven}
                      placeholderText={formDetails.input_seven_placeholder}
                      dateFormat="yyyy/MM/dd"
                      minDate={new Date()}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      disabled={submitAuthDisabled}
                      className="last-date-input"
                      required
                    />
                  </div>
                </div>
              )}
              <div className="row">
                <div className="col-md-6 dashboard-forms-input-content-col-1">
                  <Textarea
                    name={formDetails.input_eight}
                    placeholder={formDetails.input_eight_placeholder}
                    disabled={submitAuthDisabled}
                    className="product-name-input"
                    required
                  />
                </div>
                <div className="col-md-6 dashboard-forms-input-content-col-3">
                  {selected === 'products' && (
                    <MultiImageInput
                      name="product_images"
                      dropbox_label="Click or drag-and-drop to upload one or more images"
                      disabled={submitAuthDisabled}
                      className="product-images"
                      required
                    />
                  )}
                  {selected === 'services' && (
                    <MultiImageInput
                      name="service_images"
                      dropbox_label="Click or drag-and-drop to upload one or more images"
                      disabled={submitAuthDisabled}
                      className="product-images"
                      required
                    />
                  )}
                </div>
              </div>
              <div className="first-row">
                <button type="submit" className="dashboard-forms-add-btn">
                  Save
                </button>
                <button onClick={() => nextStep()} className="vendor-details-button">
                  {' '}
                  Skip
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VendorProductsAndServices;
