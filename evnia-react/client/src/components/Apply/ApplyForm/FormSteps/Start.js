// Libraries
import React, { useEffect } from 'react';
import styled from 'styled-components';

// Styling
import stepOne from '../../../../assets/images/step-one.png';
import stepTwo from '../../../../assets/images/step-two.png';
import stepThree from '../../../../assets/images/step-three.png';

const Start = (props) => {
  const { nextStep } = props;

  const lmOptions = [
    {
      name: 'Professionals',
      image:
        'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/professionals.jpg',
      link: '/professionals',
    },
    {
      name: 'Government Employees',
      image:
        'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/government-employees.jpg',
      link: '/government-employees',
    },
    {
      name: 'Office Workers',
      image:
        'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/office-workers.jpg',
      link: '/office-workers',
    },
    {
      name: 'Students',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/students.jpg',
      link: '/students',
    },
    {
      name: 'Tourists',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/tourists.jpg',
      link: '/tourists',
    },
    {
      name: 'Families',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/families.jpg',
      link: '/families',
    },
  ];

  const Container = styled.div`
    margin-top: 80px;
  `;

  const BoxImage = styled.img`
    height: 317px;
    width: 100%;
    display: block;
    background-position: center;
    background-size: cover;
  `;

  const Label = styled.h4`
    font-size: 20px;
    height: 50px;
    overflow: hidden;
  `;

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <section className="jumbotron text-center mb-0">
        <Container className="container p-0">
          <h1 className="jumbotron-heading">Add Your Restaurant</h1>
          <p className="mb-3 lead text-muted">
            Restaurants that participate in Tasttlig Festival attracts new customers to their
            business. On average, Tasttlig registers a large number of visitors per week during the
            festival. We send this traffic directly to restaurants participating the festival. Our
            visitors are people looking to experience food from around the world. The number of our
            visitors are increasing every day in the festival. This is your chance to showcase your
            food in the best light.
          </p>
          <button onClick={nextStep} className="get-started-btn">
            Add Your Restaurant
          </button>
        </Container>
      </section>
      <div className="album py-5 bg-light">
        <div className="container p-0">
          <h1 className="mb-5 text-center">Our Guests Are</h1>
          <div className="row">
            {lmOptions.map((lmo) => (
              <div key={lmo.name} className="col-lg-4 px-3">
                <div className="card mb-4 box-shadow rounded-0">
                  <BoxImage
                    className="card-img-top rounded-0"
                    style={{ backgroundImage: `url(${lmo.image})` }}
                  />
                  <div className="p-4 card-body">
                    <Label className="text-center" title={lmo.name}>
                      {lmo.name}
                    </Label>
                    {/* <div className="d-flex justify-content-between align-items-center">
                      <div className="btn-group btn-block">
                        <Link to={lmo.link} className="btn btn-sm btn-secondary">Learn More</Link>
                      </div>
                    </div> */}
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 px-3 text-center">
              <p className="lead text-gray-700 mt-6 mb-3">
                Restaurants benefit by being part of Tasttlig as they get to list samples of their
                food for the general public to taste and experience the authentic flavours created
                by the business. We understand your business is unique, and we want to tell your
                story in the best way possible.
              </p>
              <h1 className="mt-6 mb-3">How It Works</h1>
            </div>
          </div>
          <div className="row text-center">
            <div className="col-lg-4 px-3">
              <div className="card mb-4 box-shadow rounded-0">
                <div className="p-4 card-body">
                  <div className="mb-3">
                    <img src={stepOne} alt="Step One" />
                  </div>
                  <h1 className="mb-0 card-title">Add Your Restaurant</h1>
                </div>
              </div>
            </div>
            <div className="col-lg-4 px-3">
              <div className="card mb-4 box-shadow rounded-0">
                <div className="p-4 card-body">
                  <div className="mb-3">
                    <img src={stepTwo} alt="Step Two" />
                  </div>
                  <h1 className="mb-0 card-title">Add Food Samples</h1>
                </div>
              </div>
            </div>
            <div className="col-lg-4 px-3">
              <div className="card mb-4 box-shadow rounded-0">
                <div className="p-4 card-body">
                  <div className="mb-3">
                    <img src={stepThree} alt="Step Three" />
                  </div>
                  <h1 className="mb-0 card-title">Connect With Guests</h1>
                </div>
              </div>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 mt-6 px-0 text-center">
              <button onClick={nextStep} className="get-started-btn">
                Add Your Restaurant
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Start;
