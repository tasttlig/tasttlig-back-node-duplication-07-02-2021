// Libraries
import React, { useState, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { Checkbox, Form, Input } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const VendorApplicationPackage = (props) => {
  const { nextStep, update, values, readMode } = props;

  // Set initial state
  const [existingUser, setExistingUser] = useState(null);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const onSubmit = (data) => {
    if (data.accept_checkbox) {
      nextStep();
    }
  };

  // Render Welcome Paragraph part of multi-step form
  return (
    <div>
      {!readMode && (
        <div className="apply-to-host">
          <div className="apply-to-host-step-name">Vendor Application Package</div>
          <div className="welcome-paragraph">
            <ul>
              <li> Food Samples must be five bits</li>
              <li> At least 3 food samples per vendor</li>
              <li> Must list out dietary restrictions</li>
              <li> Must be accepted as an official restaurant</li>
              <li> Have an entertainment section in your vending station</li>
            </ul>
          </div>
          <Form data={values} onSubmit={onSubmit}>
            <div>
              <Checkbox name="accept_checkbox" label="Do you agree?" required={true} />
            </div>
            <div>
              {!readMode && (
                <div className="apply-to-host-navigation">
                  <span className="apply-to-host-navigation-spacing"></span>
                  <input
                    type="submit"
                    value="Continue"
                    disabled={values.submitAuthDisabled}
                    className="continue-btn"
                  />
                </div>
              )}
            </div>
          </Form>
        </div>
      )}
    </div>
  );
};

export default VendorApplicationPackage;
