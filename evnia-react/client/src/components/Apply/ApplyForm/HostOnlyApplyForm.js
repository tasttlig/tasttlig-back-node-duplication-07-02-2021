// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import Start from '../../MultiStepForm/ApplyForm/FormSteps/Start';
import PersonalInfoForm from './FormSteps/PersonalInfoForm';
import ApplyToHostForm from './FormSteps/ApplyToHostForm';
import FormReview from './FormSteps/FormReview';

toast.configure();

export default class HostOnlyApplyForm extends Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);
    const currentTime = new Date();
    currentTime.setHours(currentTime.getHours() + 2);

    // Set initial state
    this.state = {
      step: 0,

      // personal information form fields
      first_name: '',
      last_name: '',
      email: '',
      phone_number: '',

      // host form fields
      is_host: 'yes',
      host_selection: '',
      host_selection_resume: '',
      host_selection_video: '',
      host_youtube_link: '',

      submitAuthDisabled: false,
    };
  }

  // Proceed to next step
  nextStep = () => {
    window.scrollTo(0, 0);

    this.setState({
      step: this.state.step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    window.scrollTo(0, 0);

    this.setState({ step: this.state.step - 1 });
  };

  update = (data) => {
    const hostFormData = JSON.parse(localStorage.getItem('hostFormData')) || {};

    localStorage.setItem('hostFormData', JSON.stringify({ ...hostFormData, ...data }));

    Object.keys(data).forEach((key) => {
      this.setState({ [key]: data[key] });
    });
  };

  // Submit Become a Food Provider form helper function
  handleSubmitApplication = async (event) => {
    event.preventDefault();

    const url = '/user/host';

    let { submitAuthDisabled, use_residential, ...data } = this.state;
    data = {
      ...data,
      email: this.context.state.signedInStatus ? this.context.state.user.email : this.state.email,
    };

    try {
      const response = await axios({ method: 'POST', url, data });

      if (response.data && response.data.success) {
        localStorage.removeItem('hostFormData');

        setTimeout(() => {
          window.location.href = '/';
        }, 2000);

        toast(`Success! Thank you for submitting your application!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  getSteps = () => {
    let steps = [Start];

    if (!this.context.state.signedInStatus) {
      steps.push(PersonalInfoForm);
    }

    steps = [...steps, ApplyToHostForm, FormReview];

    return steps;
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  UNSAFE_componentWillMount() {
    const hostFormData = JSON.parse(localStorage.getItem('hostFormData'));

    if (hostFormData) {
      Object.keys(this.state).forEach((key) => {
        const value = hostFormData[key];

        if (value) {
          this.setState({
            [key]: value,
          });
        }
      });
    }
  }

  render = () => {
    const { step, ...values } = this.state;
    const Step = this.getSteps()[step];

    return (
      <Step
        nextStep={this.nextStep}
        prevStep={this.prevStep}
        update={this.update}
        values={values}
        handleSubmitApplication={this.handleSubmitApplication}
      />
    );
  };
}
