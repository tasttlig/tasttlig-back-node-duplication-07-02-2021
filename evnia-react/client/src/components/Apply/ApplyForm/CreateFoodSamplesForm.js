// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import ProductForm from './FormSteps/ProductForm';
import FormReview from './FormSteps/FormReview';

toast.configure();

export default class CreateFoodSamplesForm extends Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);

    // Set initial state
    this.state = {
      step: 0,
      isCreateFoodSamples: true,
      foodSampleList: [],
      successMessage: '',
      submitAuthDisabled: false,
    };
  }

  UNSAFE_componentWillMount = () => {
    const foodSamplesFormData = JSON.parse(localStorage.getItem('foodSamplesFormData'));

    if (foodSamplesFormData) {
      Object.keys(this.state).forEach((key) => {
        const value = foodSamplesFormData[key];

        if (value) {
          this.setState({
            [key]: value,
          });
        }
      });
    }
  };

  // Proceed to next step
  nextStep = () => {
    window.scrollTo(0, 0);

    this.setState({ step: this.state.step + 1 });
  };

  // Go back to prev step
  prevStep = () => {
    window.scrollTo(0, 0);

    this.setState({ step: this.state.step - 1 });
  };

  // Update food samples information
  update = (data) => {
    const foodSamplesFormData = JSON.parse(localStorage.getItem('foodSamplesFormData')) || {};

    localStorage.setItem(
      'foodSamplesFormData',
      JSON.stringify({ ...foodSamplesFormData, ...data }),
    );

    Object.keys(data).forEach((key) => {
      this.setState({ [key]: data[key] });
    });
  };

  // Submit Food Samples form helper function
  handleSubmitApplication = async (event) => {
    event.preventDefault();

    const url = '/food-sample/add';

    const { submitAuthDisabled, foodSampleList, ...data } = this.state;

    try {
      const response = await axios({
        method: 'POST',
        url,
        data: foodSampleList,
      });

      if (response && response.data && response.data.success) {
        localStorage.removeItem('foodSamplesFormData');

        setTimeout(() => {
          window.location.href = '/dashboard';
        }, 2000);

        toast(`Success! Thank you for creating food samples!`, {
          type: 'success',
          autoClose: 2000,
        });

        if (this.context.state.user.role.includes('RESTAURANT_PENDING')) {
          this.setState({
            successMessage:
              'Your food sample will be shown as soon as your application is approved.',
          });
        }

        this.setState({
          submitAuthDisabled: true,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  getSteps = () => {
    let steps = [];

    steps = [...steps, ProductForm, FormReview];

    return steps;
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  render = () => {
    const { step, ...values } = this.state;
    const Step = this.getSteps()[step];

    return (
      <Step
        nextStep={this.nextStep}
        prevStep={this.prevStep}
        update={this.update}
        values={values}
        handleSubmitApplication={this.handleSubmitApplication}
      />
    );
  };
}
