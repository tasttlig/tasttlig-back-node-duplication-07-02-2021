// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import {
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
  Checkbox,
  CheckboxGroup,
} from '../../../EasyForm';
// Components
import Nav from '../../../Navbar/Nav';

// Styling
import './GuestAmbassadorApplication.scss';
import 'react-toastify/dist/ReactToastify.css';
toast.configure();

const GuestAmbassadorApplication = (props) => {
  // Set initial state

  const [explainSampleOrigins, setExplainSampleOrigins] = useState('');
  const [noexplainSampleOriginsButton, setNoexplainSampleOriginsButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [explainSampleOriginsButton, setexplainSampleOriginsButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [isInfluencer, setIsInfluencer] = useState(false);

  let data = {
    linkedin_link: '',
    facebook_link: '',
    youtube_link: '',
    instagram_link: '',
    twitter_link: '',
    ambassador_intent_description: '',
  };

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const submitApplication = async (data) => {
    data.is_influencer = isInfluencer;
    data.resume = 'G_AMB';
    data.is_guest_amb = true;
    console.log('dta', data);
    const url = '/guest-ambassador-application';
    try {
      const response = await axios({ method: 'POST', url, data });
      if (response && response.data && response.data.success) {
        setTimeout(() => {
          // window.location.reload();
          window.location.href = `/`;
        }, 2000);

        toast(`Success! Thank you for sending in your application!`, {
          type: 'success',
          autoClose: 2000,
        });
      }
      return response;
    } catch (error) {
      return error.response;
    }
  };

  const onSubmit = (data) => {
    data.is_influencer = isInfluencer;
    console.log('dta', data);
    submitApplication(data);
  };
  // Mount Become a Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Become a Host page
  return (
    <div className="row">
      <Nav />
      <div className="col-lg-8 col-xl-6 px-0 sign-up-background-image-one" />
      <div className="col-lg-4 col-xl-4 px-0 become-a-host">
        <div className="become-a-host-title">Become a Guest Ambassador</div>

        <Form data={data} onSubmit={onSubmit}>
          <div className="">
            <h6>are you an influencer?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setNoexplainSampleOriginsButton('do-you-have-a-restaurant-no1-btn');
                      setexplainSampleOriginsButton('do-you-have-a-restaurant-yes1-btn');
                      setIsInfluencer(true);
                    }}
                    className={explainSampleOriginsButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setexplainSampleOriginsButton('do-you-have-a-restaurant-no1-btn');
                      setNoexplainSampleOriginsButton('do-you-have-a-restaurant-yes1-btn');
                      setIsInfluencer(false);
                    }}
                    className={noexplainSampleOriginsButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <Input
                name="linkedin_link"
                placeholder="share your linkedin profile"
                className="linkedin"
                required
              />
            </div>
            <div className="col-md-6">
              <Input
                name="facebook_link"
                placeholder="share your facebook profile"
                className="facebook"
                required
              />
            </div>
            <div className="col-md-6">
              <Input
                name="youtube_link"
                placeholder="share your youtube channel"
                className="youtube"
                required
              />
            </div>
            <div className="col-md-6">
              <Input
                name="instagram_link"
                placeholder="share your instagram handle"
                className="instagram"
                required
              />
            </div>
            <div className="col-md-6">
              <Input
                name="twitter_link"
                placeholder="share your twitter handle"
                className="twitter"
                required
              />
            </div>
            <div className="col-md-8">
              <Input
                name="ambassador_intent_description"
                label="why do you want to be an ambassador of Tasttlig?"
                className="ambassador_intent_description"
                required
              />
            </div>
          </div>

          <div>
            <button type="submit" className="submit-button">
              Submit
            </button>
          </div>
        </Form>
      </div>
      <div className="col-xl-2 px-0 sign-up-background-image-two" />
    </div>
  );
};

export default GuestAmbassadorApplication;
