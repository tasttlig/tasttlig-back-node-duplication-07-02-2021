// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import CreateFoodSamplesForm from './ApplyForm/CreateFoodSamplesForm';

// Styling
import './Apply.scss';

const CreateFoodSamples = (props) => {
  // Mount Create Food Samples page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Create Food Samples page
  return (
    <div>
      <Nav />
      <CreateFoodSamplesForm history={props.history} />
    </div>
  );
};

export default CreateFoodSamples;
