// Libraries
import React, { useEffect, useContext, Fragment } from 'react';
import { AppContext } from '../../ContextProvider/AppProvider';
// Components
import UserPassportPreferenceForm from './ApplyForm/UserPassportPreferenceForm/UserPassportPreferenceForm';
import Nav from '../Navbar/Nav';
import Footer from '../Footer/Footer';
import GoTop from '../Shared/GoTop';
// Styling
import './Apply.scss';

const Apply = (props) => {
  const appContext = useContext(AppContext);
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Host page
  return (
    <Fragment>
      <Nav/>
        <div>
          <div className="become-host-form">
            {/* <Nav /> */}
            <UserPassportPreferenceForm user={appContext.state.user} update={true} />
          </div>
        </div>
      <Footer />
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </Fragment>
  );
};

export default Apply;
