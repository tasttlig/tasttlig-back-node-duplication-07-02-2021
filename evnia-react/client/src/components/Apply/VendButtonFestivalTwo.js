import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import HostButtonApplyForm from './ApplyForm/HostButtonApplyForm';
import VendButtonForm from './ApplyForm/VendButtonApplyForm';
import VendButtonFestivalForm from './ApplyForm/VendButtonFestivalForm';
import VendButtonFestivalFormTwo from './ApplyForm/VendButtonFestivalFormTwo';

// Styling
import './Apply.scss';

const VendButtonFestivalTwo = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Host page
  return (
    <div>
      <div>
        <Nav />
        <VendButtonFestivalFormTwo history={props.history} festivalId={props.location.festivalId} />
      </div>
    </div>
  );
};

export default VendButtonFestivalTwo;
