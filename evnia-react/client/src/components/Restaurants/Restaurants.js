// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import Nav from '../Navbar/Nav';
import PassportCard from '../Passport-Old/PassportCard/PassportCard';
import BannerFooter from '../Home/BannerFooter/BannerFooter';
import GoTop from '../Shared/GoTop';

// Styling
import '../Passport/Passport.scss';
import defaultRestaurantImage from '../../assets/images/default-restaurant-image.png';

const Restaurants = () => {
  // Set initial state
  const [loading, setLoading] = useState(false);
  const [restaurants, setRestaurants] = useState([]);
  const [keyword, setKeyword] = useState([]);
  const [filterSearch, setFilterSearch] = useState('');

  // Render restaurants helper function
  const renderRestaurants = (arr) => {
    return arr.map((restaurant, index) => (
      <PassportCard
        key={index}
        businessDetailsId={restaurant.business_details_id}
        userId={restaurant.business_details_user_id}
        business_name={restaurant.business_name}
        foodSampleId={null}
        passportId={null}
        images={[defaultRestaurantImage]}
        title={null}
        price={null}
        address={restaurant.business_address_1}
        city={restaurant.city}
        provinceTerritory={restaurant.state}
        postalCode={restaurant.postal_code}
        startDate={null}
        endDate={null}
        startTime={null}
        endTime={null}
        description={null}
        sample_size={null}
        foodSampleOwnerId={restaurant.user_id}
        email={null}
        quantity={null}
        disableButton={true}
      />
    ));
  };

  // Get restaurants in current festival helper function
  const restaurantsInCurrentFestival = async () => {
    setLoading(true);
    await axios({
      method: 'GET',
      url: '/business/all/restaurants',
      params: {
        keyword: filterSearch,
      },
    })
      .then((response) => {
        setLoading(false);
        console.log(response.data);
        setRestaurants(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Mount Restaurants page
  useEffect(() => {
    window.scrollTo(0, 0);

    restaurantsInCurrentFestival();
  }, []);
  useEffect(() => {
    window.scrollTo(0, 0);

    restaurantsInCurrentFestival();
  }, [filterSearch]);

  // No restaurants found message
  const RestaurantEmpty = () => {
    return <strong className="no-restaurants-found">No restaurants found.</strong>;
  };

  const handleInputChange = (event) => {
    //event.preventDefault();
    console.log(event.target.value);
    setKeyword(event.target.value);
  };

  const _handleKeyDown = (event) => {
    //event.preventDefault();
    if (event.key === 'Enter') {
      event.preventDefault();
      setFilterSearch(keyword);
    }
  };
  // Render Restaurants page
  return (
    <div>
      <Nav />

      <div>
        <div className="passport">
          <div className="row">
            <div className="col-md-12 passport-restaurant-container">
              <>
                <div className="passport-sub-title">Festival's Restaurants</div>
                <div className="row search-field">
                  <input
                    name="card_search"
                    placeholder="Search..."
                    type="text"
                    value={keyword}
                    onChange={handleInputChange}
                    onKeyDown={_handleKeyDown}
                  ></input>
                </div>
                <div className="passport-cards mb-5">
                  {restaurants.length !== 0 ? renderRestaurants(restaurants) : <RestaurantEmpty />}
                </div>
              </>
            </div>
          </div>
        </div>
      </div>

      {/* Footer Area */}
      <BannerFooter />
      {/* Back to Top */}
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default Restaurants;
