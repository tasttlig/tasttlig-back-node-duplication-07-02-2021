// Libraries
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as shoppingCartActions from '../../redux/shoppingCart/actions';

// Styling
import './ShoppingCart.scss';

const ShoppingCartHoverList = (props) => {
  const cartItemList = (cartItem) => {
    return (
      <div className="row pb-3" key={cartItem.itemId}>
        <div
          className="col-sm-1 row p-0"
          onClick={() => props.removeFromCart(cartItem.itemType, cartItem.itemId)}
        >
          <div className="fas fa-trash p-2" />
        </div>
        <div className="col-sm-6 row p-0">
          <span className="item-name col-sm-12">{cartItem.itemTitle}</span>
          <span className="item-price col-sm-12">${cartItem.price}</span>
        </div>
        <div className="col-sm-5 row p-0">
          <div className="col p-0">
            <div
              className={
                cartItem.quantity > 1
                  ? 'border border-dark bg-dark text-white p-2 fas fa-minus'
                  : 'border border-dark bg-light text-dark p-2 fas fa-minus'
              }
              onClick={() =>
                cartItem.quantity > 1
                  ? props.subtractQuantity(cartItem.itemType, cartItem.itemId)
                  : null
              }
            />
          </div>
          <div className="col p-1 text-center">{cartItem.quantity}</div>
          <div className="col p-0">
            <div
              className="border border-dark bg-dark text-white p-2 fas fa-plus"
              onClick={() => props.addQuantity(cartItem.itemType, cartItem.itemId)}
            />
          </div>
        </div>
      </div>
    );
  };

  const cartBox = () => {
    return props.cartVisibility ? (
      <div className="cart-container" onClick={() => props.alternateCartVisibility()}>
        <div
          className="border shadow shopping-cart"
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <div className="shopping-cart-header">
            <i className="fa fa-shopping-cart cart-icon" />
            <span className="badge">{props.totalQuantity}</span>
            <div
              className="shopping-cart-total ml-3"
              onClick={() => props.alternateCartVisibility()}
            >
              <span className="lighter-text far fa-2x fa-window-close" />
            </div>
            <div className="shopping-cart-total">
              <span className="lighter-text">&nbsp;Total:&nbsp;</span>
              <span className="main-color-text">${props.totalPrice.toFixed(2)}</span>
            </div>
          </div>
          {props.cartItems.length === 0 && <div className="mt-3">Cart is currently Empty</div>}
          <div className="shopping-cart-items">
            {props.cartItems.map((cartItem) => (cartItem ? cartItemList(cartItem) : ''))}
          </div>
          {props.cartItems.length !== 0 && (
            <Link to="/cart/checkout" className="col-sm-12 checkout-btn">
              Checkout
            </Link>
          )}
        </div>
      </div>
    ) : null;
  };

  return <div>{cartBox()}</div>;
};

const mapStateToProps = (state) => {
  return {
    cartVisibility: state.cartVisibility,
    cartItems: state.cartItems,
    totalQuantity: state.totalQuantity,
    totalPrice: state.totalPrice,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addQuantity: (type, id) => {
      dispatch(shoppingCartActions.addQuantity(type, id));
    },
    subtractQuantity: (type, id) => {
      dispatch(shoppingCartActions.subtractQuantity(type, id));
    },
    removeFromCart: (type, id) => {
      dispatch(shoppingCartActions.removeFromCart(type, id));
    },
    alternateCartVisibility: () => {
      dispatch(shoppingCartActions.alternateCartVisibility());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartHoverList);
