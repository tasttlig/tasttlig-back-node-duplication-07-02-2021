// Libraries
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

// Components
import Nav from '../Navbar/Nav';
import CartStripePaymentCard from './CartStripePaymentCard';
import StripePaymentCard from './StripePaymentCard';

// Styling
import './Payment.scss';

const CartPayment = (props) => {
  // Set initial state
  const [price, setPrice] = useState(0.0);
  const [tax, setTax] = useState(0.0);
  const [total, setTotal] = useState(0.0);

  // Get payment details
  useEffect(() => {
    setPrice(parseFloat(props.totalPrice).toFixed(2));
    setTax((Math.round(parseFloat(props.totalPrice) * 13) / 100).toFixed(2));
    setTotal(
      (parseFloat(props.totalPrice) + Math.round(parseFloat(props.totalPrice) * 13) / 100).toFixed(
        2,
      ),
    );
  }, []);

  const orderSummary = () => {
    return (
      <div>
        <label className="title">Order Summary</label>
        <hr />
        <div className="form-group col-12 px-0">
          <span className="col-md-4 px-0">Price</span>
          <span className="col-md-2 px-0 float-right">${price}</span>
        </div>
        <div className="form-group col-12 px-0">
          <span className="col-md-4 px-0">Tax</span>
          <span className="col-md-2 px-0 float-right">${tax}</span>
        </div>

        <div className="form-group col-12 mt-4 px-0">
          <span className="col-md-4 px-0">Total</span>
          <span className="col-md-2 px-0 float-right">${total}</span>
        </div>
      </div>
    );
  };

  return (
    <div>
      <Nav />

      <div className="container-fluid payment-content">
        <div className="row">
          <div id="userinfo" className="col-sm-12 col-md-6 offset-lg-3 col-lg-3 payment-user-info">
            <div className="row">
              <div className="col px-0 mb-5" id="guest_checkout">
                <div className="form-group col-12 px-0">
                  <CartStripePaymentCard />
                </div>
              </div>
            </div>
          </div>
          <div id="checkout" className="col-sm-12 col-md-6 col-lg-3 payment-user-info">
            {orderSummary()}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartItems,
    totalPrice: state.totalPrice,
  };
};

export default connect(mapStateToProps)(CartPayment);
