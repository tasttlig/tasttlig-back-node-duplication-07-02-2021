// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { loadStripe } from '@stripe/stripe-js';
import { Elements, CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { connect } from 'react-redux';
import * as shoppingCartActions from '../../redux/shoppingCart/actions';
import { AppContext } from '../../ContextProvider/AppProvider';

const CheckoutForm = ({ props }) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Set initial state
  const [errorMessage, setErrorMessage] = useState('');
  const [userEmail, setUserEmail] = useState(
    appContext.state.user.email ? appContext.state.user.email : '',
  );
  const [userEmailError, setUserEmailError] = useState('');
  const stripe = useStripe();
  const elements = useElements();

  // Submit payment helper function
  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    if (userEmail.length === 0) {
      setUserEmailError('Email is required.');
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(userEmail)) {
      setUserEmailError('Please enter a valid email.');
    } else {
      setUserEmailError('');
    }

    try {
      const request_data = {
        cartItems: props.cartItems,
        email: userEmail,
      };
      const { data } = await axios.post('/payment/stripe/cart', request_data);

      if (data.success) {
        const clientSecret = data.client_secret;
        const result = await stripe.confirmCardPayment(clientSecret, {
          payment_method: {
            card: elements.getElement(CardElement),
            billing_details: {
              email: userEmail,
            },
          },
        });

        if (result.error) {
          setErrorMessage(result.error.message);
        } else {
          // The payment has been processed!
          if (result.paymentIntent.status === 'succeeded') {
            request_data.payment_id = result.paymentIntent.id;
            request_data.email = userEmail;

            const response = await axios.post('/payment/stripe/cart/success', request_data);

            if (response.data.success) {
              props.emptyCart();
              window.location.href = '/payment/success';
            }
          }
        }
      }
    } catch (error) {}
  };

  // Mount Payment page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <form onSubmit={handleSubmit} noValidate>
      <div className="title mb-3">Where should we email you your receipt?</div>
      <div className="mb-3">
        <label htmlFor="inputEmail4" className="inputLabel font-weight-bold">
          Email*
        </label>
        <input
          type="text"
          value={userEmail}
          onChange={(e) => setUserEmail(e.target.value)}
          disabled={appContext.state.user.email && true}
          className="form-control"
          required
        />
        {userEmailError && <div className="error-message">{userEmailError}</div>}
        {/*<label htmlFor="inputEmail4" className="inputLabel font-weight-bold">Email*</label>*/}
        {/*<input type="email" className="form-control mt-1" id="inputEmail4"/>*/}
      </div>
      <div className="mb-3">
        <CardElement />
        {errorMessage && <div className="error-message">{errorMessage}</div>}
      </div>
      <div className="mb-3">
        <button type="submit" disabled={!stripe} className="pay-btn">
          Pay
        </button>
      </div>
      <button
        type="button"
        data-toggle="tooltip"
        data-placement="bottom"
        title="Your payment and personal information are encrypted."
        className="secure-checkout"
      >
        <span className="fas fa-lock"></span>&nbsp;Secure Checkout
      </button>
    </form>
  );
};

const CartStripePaymentCard = (props) => {
  // Environment
  let stripePromise;

  if (process.env.REACT_APP_ENVIRONMENT == 'production') {
    stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PROD_PUBLISHABLE_KEY);
  } else {
    stripePromise = loadStripe(process.env.REACT_APP_STRIPE_TEST_PUBLISHABLE_KEY);
  }

  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm success={() => {}} props={props} />
    </Elements>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartItems,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    emptyCart: () => {
      dispatch(shoppingCartActions.emptyCart());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartStripePaymentCard);
