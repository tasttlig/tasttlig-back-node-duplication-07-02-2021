// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';

// Styling
import './Payment.scss';

const PaymentSuccess = () => {
  // Mount Payment Success page
  useEffect(() => {
    window.scrollTo(0, 0);

    setTimeout(() => {
      window.location.href = '/';
    }, 2000);
  }, []);

  // Render Payment Success page
  return (
    <div>
      <Nav />

      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12 text-center title">
            Congratulations, your payment went through successfully!
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaymentSuccess;
