// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';

// Components
import Nav from '../Navbar/Nav';
import StripePaymentCard from './StripePaymentCard';
import { AppContext } from '../../ContextProvider/AppProvider';

// Styling
import './Payment.scss';

const Payment = (props) => {
  console.log("props from payment part:", props)
  // Set initial state
  const [item_type] = useState(props.match.params.item_type);
  const [item_id] = useState(props.match.params.item_id);
  const [price, setPrice] = useState(0.0);
  const [tax, setTax] = useState(0.0);
  const [total, setTotal] = useState(0.0);
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [discount, setDiscount] = useState(1);
  const [festivalDiscount, setFestivalDiscount] = useState(1);

  // To use the JWT credentials
 const appContext = useContext(AppContext);

  let festivalDetailsArray = []
  let vendingFestivalPrice = 0;
  let vendingFestivalTaxPrice; 
  let totalVendingFestivalPrice = 0;
  let festivalPayments;
  if (localStorage.getItem('festivalPayments')) {
    festivalPayments = localStorage.getItem('festivalPayments').split(',');
  }

  console.log("festival items from local storage:", festivalPayments)

  // Fetch festival details helper function
  const fetchFestivalDetails = async (festivalPayment) => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/festival/${festivalPayment}`,
      });
      
      return response;
    } catch (error) {
      return error.response;
    }
  };

     // fetch user's subscription list and filter them, if user has level 1 or 2 subsciption set discount accordingly
     const fetchSubscriptionList = async () => {
      try {
        const response = await axios({
          method: 'GET',
          url: `/valid-user-subscriptions/${appContext.state.user.id}`,
        });
        const sub1 = response.data.user.filter(subscription => subscription.subscription_code === 'V_MOD')
        const sub2 = response.data.user.filter(subscription => subscription.subscription_code === 'V_ULTRA')
        const sub3 = response.data.user.filter(subscription => subscription.subscription_code === 'G_MSHIP2')
        const sub4 = response.data.user.filter(subscription => subscription.subscription_code === 'G_MSHIP3')

        if (sub2.length > 0 && (sub2[0].suscribed_festivals === null || sub2[0].suscribed_festivals.length < 4) ){
          setDiscount(0.95);
        } else if (sub1.length > 0 && sub1[0].suscribed_festivals === null) {
          setDiscount(0.95);
        } 
        if (sub4.length > 0 && !festivalPayments){
        setFestivalDiscount(0.85);

      } else if (sub3.length > 0 && !festivalPayments){

        setFestivalDiscount(0.85);
      }
        
        return response;
      } catch (error) {
        return error.response;
      }
    };

    useEffect(() => {
      fetchSubscriptionList();
  
    }, []);
  
  // Get payment details
  useEffect(() => {
    window.scrollTo(0, 0);

    (async function () {
    

      if(festivalPayments) {

        //fetching fetival details to know the price
          for(let festivalPayment = 0; festivalPayment < festivalPayments.length; festivalPayment++) {
           const festivalRespponse = await fetchFestivalDetails(festivalPayments[festivalPayment])
           festivalDetailsArray[festivalPayment] = festivalRespponse.data.details[0]

          }

      }

      const data = {
        item_type,
        item_id,
      };

      const response = await axios.get('/subscription/details', {
        params: data,
      });

      console.log("response from payment part:", response)

      let discount = 1;
      if (localStorage.getItem('discount')) {
        discount = localStorage.getItem('discount');
        localStorage.removeItem('discount');
      }

      if (response.data.item.festival_price) {
        if(festivalDiscount < 1) {
          setPrice(parseFloat(response.data.item.festival_price * festivalDiscount).toFixed(2));
          setTax((Math.round(parseFloat(response.data.item.festival_price * festivalDiscount) * 13) / 100).toFixed(2));
          setTotal(
            (
              parseFloat(response.data.item.festival_price * festivalDiscount) +
              Math.round(parseFloat(response.data.item.festival_price) * 13) / 100
            ).toFixed(2),
          );
        } else {
          setPrice(parseFloat(response.data.item.festival_price).toFixed(2));
          setTax((Math.round(parseFloat(response.data.item.festival_price) * 13) / 100).toFixed(2));
          setTotal(
            (
              parseFloat(response.data.item.festival_price) +
              Math.round(parseFloat(response.data.item.festival_price) * 13) / 100
            ).toFixed(2),
          );
        }


      } else if (response.data.item.product_price) {


        if(props.location.price && props.location.price === 2) {
         
          setPrice(parseFloat(props.location.price * discount).toFixed(2));
          setTax((Math.round(parseFloat(props.location.price * discount) * 13) / 100).toFixed(2));
          setTotal(
            (
              parseFloat(props.location.price * discount) +
              Math.round(parseFloat(props.location.price * discount) * 13) / 100
            ).toFixed(2),
          );
        } else {
 
          setPrice(parseFloat(response.data.item.product_price * discount).toFixed(2));
          setTax((Math.round(parseFloat(response.data.item.product_price * discount) * 13) / 100).toFixed(2));
         setTotal(
          (
            parseFloat(response.data.item.price * discount) +
            Math.round(parseFloat(response.data.item.product_price * discount) * 13) / 100
          ).toFixed(2),
        );
        }

      } else if (response.data.item.service_price) {

        if(props.location.price && props.location.price === 2) {
         
          setPrice(parseFloat(props.location.price * discount).toFixed(2));
          setTax((Math.round(parseFloat(props.location.price * discount) * 13) / 100).toFixed(2));
          setTotal(
            (
              parseFloat(props.location.price * discount) +
              Math.round(parseFloat(props.location.price * discount) * 13) / 100
            ).toFixed(2),
          );
        } else {
 
          setPrice(parseFloat(response.data.item.service_price * discount).toFixed(2));
          setTax((Math.round(parseFloat(response.data.item.service_price * discount) * 13) / 100).toFixed(2));
          setTotal(
            (
              parseFloat(response.data.item.service_price * discount) +
              Math.round(parseFloat(response.data.item.service_price * discount) * 13) / 100
            ).toFixed(2),
          );
        }


      } else if (response.data.item.experience_price) {

        if(props.location.price && props.location.price === 2) {
         
          setPrice(parseFloat(props.location.price * discount).toFixed(2));
          setTax((Math.round(parseFloat(props.location.price * discount) * 13) / 100).toFixed(2));
          setTotal(
            (
              parseFloat(props.location.price * discount) +
              Math.round(parseFloat(props.location.price * discount) * 13) / 100
            ).toFixed(2),
          );
        } else {
 
          setPrice(parseFloat(response.data.item.experience_price * discount).toFixed(2));
          setTax((Math.round(parseFloat(response.data.item.experience_price * discount) * 13) / 100).toFixed(2));
          setTotal(
            (
              parseFloat(response.data.item.experience_price * discount) +
              Math.round(parseFloat(response.data.item.experience_price * discount) * 13) / 100
            ).toFixed(2),
          );
        }

      } else {

        if (festivalPayments) {
          //bringing all the vending prices from the fesitval details array to perform calculations
          for(let array of festivalDetailsArray) {
            if(discount < 1) {
              vendingFestivalPrice += Number(array.festival_vendor_price);  
              vendingFestivalPrice = (vendingFestivalPrice * discount)
            } else  {
              vendingFestivalPrice += Number(array.festival_vendor_price);
            }

          } 
           vendingFestivalTaxPrice = ((Math.round(vendingFestivalPrice * 13) / 100
          ).toFixed(2));
          totalVendingFestivalPrice += vendingFestivalPrice + (Math.round(vendingFestivalPrice * 13) / 100
          );

          setPrice(vendingFestivalPrice.toFixed(2));
          setTax(vendingFestivalTaxPrice);
          setTotal(totalVendingFestivalPrice.toFixed(2) );

        } else {

          if(props.location.price && props.location.price === 2) {
         
            setPrice(parseFloat(props.location.price * discount).toFixed(2));
            setTax((Math.round(parseFloat(props.location.price * discount) * 13) / 100).toFixed(2));
            setTotal(
              (
                parseFloat(props.location.price * discount) +
                Math.round(parseFloat(props.location.price * discount) * 13) / 100
              ).toFixed(2),
            );
          } else {
   
            setPrice(parseFloat(response.data.item.price).toFixed(2));
            setTax((Math.round(parseFloat(response.data.item.price) * 13) / 100).toFixed(2));
            setTotal(
              (
                parseFloat(response.data.item.price) +
                Math.round(parseFloat(response.data.item.price) * 13) / 100
              ).toFixed(2),
            );
          }
          
        }
      }
    })();
  }, []);



  const orderSummary = () => {
    return (
      <div>
        <label className="title mb-2">Order Summary</label>
        <hr />
        <div className="form-group col-12">
          <span className="col-md-4">Price</span>
          <span className="col-md-2 float-right">{discount < 1 ? '$' +( price * discount).toFixed(2) : festivalDiscount < 1 ? '$' +( price * festivalDiscount).toFixed(2) : '$' + (price)}</span>
        </div>
        <div className="form-group col-12">
          <span className="col-md-4">Tax</span>
          <span className="col-md-2 float-right">{discount < 1 ? '$' +( tax * discount).toFixed(2) : festivalDiscount < 1 ? '$' +( tax * festivalDiscount).toFixed(2) : '$' + (tax)}</span>
        </div>

        <div className="form-group col-12 mt-4">
          <span className="font-weight-bold">Total</span>
          <span className="col-md-2 float-right">{discount < 1 ? '$' +( total * discount).toFixed(2) : festivalDiscount < 1 ? '$' +( total * festivalDiscount).toFixed(2) : '$' + (total)}</span>
        </div>
      </div>
    );
  };

  return (
    <div>
      <Nav />

      <div className="container-fluid payment-content">
        <div className="row">
          <div id="userinfo" className="col-sm-12 col-md-6 offset-lg-3 col-lg-3 payment-user-info">
            <div className="row">
              <div className="col" id="guest_checkout">
                <div className="form-group col-12">
                  <StripePaymentCard props={props} totalPrice={discount < 1 ? Number(total * discount) : festivalDiscount < 1 ? Number(total * festivalDiscount) : Number(total)} discount={discount} festivalDiscount={festivalDiscount}/>
                </div>
              </div>
            </div>
          </div>
          <div id="checkout" className="col-sm-12 col-md-6 col-lg-3">
            {orderSummary()}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Payment;
