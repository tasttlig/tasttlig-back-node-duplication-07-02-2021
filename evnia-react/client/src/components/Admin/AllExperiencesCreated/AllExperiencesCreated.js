// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Components
import Nav from '../../Navbar/Nav';
import AllExperiencesCreatedCard from './AllExperiencesCreatedCard/AllExperiencesCreatedCard';

// Styling
import './AllExperiencesCreated.scss';

export default class AllExperiencesCreated extends Component {
  // Set initial state
  state = {
    allExperienceCreatedItems: [],
  };

  // Render All Experiences Created helper function
  renderAllExperiencesCreated = (arr) => {
    return arr.map((card, index) => (
      <AllExperiencesCreatedCard
        key={index}
        id={card.id}
        experienceImage={card.img_url_1}
        title={card.title}
        price={card.price}
        capacity={card.capacity}
        firstName={card.first_name}
        lastName={card.last_name}
        email={card.email}
        phoneNumber={card.phone_number}
        addressLine1={card.address_line_1}
        addressLine2={card.address_line_2}
        city={card.city}
        provinceTerritory={card.province_territory}
        postalCode={card.postal_code}
        startDate={card.start_date}
        endDate={card.end_date}
        accepted={card.accepted}
        createdAt={card.created_at}
      />
    ));
  };

  // Mount All Experiences Created page
  componentDidMount = () => {
    window.scrollTo(0, 0);

    const url = '/experiences';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          allExperienceCreatedItems: [
            ...this.state.allExperienceCreatedItems,
            ...response.data.experiences.reverse(),
          ],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render All Experiences Created page
  render = () => {
    const { allExperienceCreatedItems } = this.state;

    return (
      <div>
        <Nav />

        <div className="all-experiences-created">
          <div className="all-experiences-created-navigation">
            <Link to="/admin" className="all-experiences-created-navigation-content">
              Admin
            </Link>
            <span className="arrow">&nbsp;&gt;&nbsp;</span>All Experiences Created
          </div>

          {this.renderAllExperiencesCreated(allExperienceCreatedItems)}
        </div>
      </div>
    );
  };
}
