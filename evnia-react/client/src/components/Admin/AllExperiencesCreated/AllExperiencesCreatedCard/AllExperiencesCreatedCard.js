// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import { toast } from 'react-toastify';

// Components
import { formatDate, formatTime } from '../../../Functions/Functions';

// Styling
import './AllExperiencesCreatedCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class AllExperiencesCreatedCard extends Component {
  // Set initial state
  state = {
    load: false,
    rejectExperienceCreatedDisabled: false,
    rejectExperienceCreatedNote: '',
    experienceCreatedSubmitDisabled: false,
  };

  // Load image helper function
  onLoad = () => {
    this.setState({ load: true });
  };

  // Toggle reject to experience created button helper function
  toggleRejectExperienceCreated = (event) => {
    event.preventDefault();

    this.setState((prevState) => {
      return {
        rejectExperienceCreatedDisabled: !prevState.rejectExperienceCreatedDisabled,
      };
    });
  };

  // Handle reject to experience created change helper function
  handleRejectExperienceCreated = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate reject to experience created from admin helper function
  validateRejectExperienceCreated = () => {
    let rejectExperienceCreatedNoteError = '';

    // Render reject to experience created error message
    if (!this.state.rejectExperienceCreatedNote) {
      rejectExperienceCreatedNoteError = 'Note is required.';
    }

    // Set reject to experience created error message state
    if (rejectExperienceCreatedNoteError) {
      this.setState({ rejectExperienceCreatedNoteError });
      return false;
    }

    return true;
  };

  // Accept experience created helper function
  handleSubmitAcceptExperienceCreated = async (event) => {
    event.preventDefault();

    try {
      const url = `/experiences/${this.props.id}`;

      const data = {
        title: this.props.title,
        first_name: this.props.firstName,
        last_name: this.props.lastName,
        email: this.props.email,
        accepted: true,
        reject_note: '',
      };

      const response = await axios({ method: 'PUT', url, data });

      if (response) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(
          `Success! Thank you for accepting ${this.props.firstName} ${this.props.lastName}'s experience!`,
          {
            type: 'success',
            autoClose: 2000,
          },
        );

        this.setState({
          rejectExperienceCreatedNoteError: '',
          experienceCreatedSubmitDisabled: true,
        });
      }
    } catch (error) {
      console.log(error);

      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Reject experience created helper function
  handleSubmitRejectExperienceCreated = async (event) => {
    event.preventDefault();

    const isValid = this.validateRejectExperienceCreated();

    if (isValid) {
      const url = `/experiences/${this.props.id}`;

      const data = {
        title: this.props.title,
        first_name: this.props.firstName,
        last_name: this.props.lastName,
        email: this.props.email,
        accepted: false,
        reject_note: this.state.rejectExperienceCreatedNote,
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(
            `Success! Thank you for rejecting ${this.props.firstName} ${this.props.lastName}'s experience!`,
            {
              type: 'success',
              autoClose: 2000,
            },
          );

          this.setState({
            rejectExperienceCreatedNoteError: '',
            experienceCreatedSubmitDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render All Experiences Created card
  render = () => {
    const {
      experienceImage,
      title,
      price,
      capacity,
      firstName,
      lastName,
      email,
      phoneNumber,
      addressLine1,
      addressLine2,
      city,
      provinceTerritory,
      postalCode,
      startDate,
      endDate,
      accepted,
      createdAt,
    } = this.props;

    const {
      load,
      rejectExperienceCreatedDisabled,
      rejectExperienceCreatedNote,
      rejectExperienceCreatedNoteError,
      experienceCreatedSubmitDisabled,
    } = this.state;

    return (
      <div>
        <LazyLoad once>
          <div className="mt-3 experience-created">
            <div className="row form-group">
              <div className="col-sm-3 experience-created-image-section">
                <img
                  src={experienceImage}
                  alt={title}
                  onLoad={this.onLoad}
                  className={load ? 'experience-created-image' : 'loading-image'}
                />
              </div>
              <div className="col-sm-9 experience-created-details-section">
                <div>{`Title: ${title}`}</div>
                <div>{`Price: $${price}`}</div>
                <div>{`Capacity: ${capacity}`}</div>
                <div>{`Address: ${addressLine1}, ${
                  addressLine2 ? addressLine2 + ', ' : ''
                } ${city}, ${provinceTerritory} ${postalCode}`}</div>
                <div>{`Start Date: ${formatDate(startDate)}`}</div>
                <div>{`End Date: ${formatDate(endDate)}`}</div>
                <div>{`Creator Name: ${firstName} ${lastName}`}</div>
                <div>
                  Email:{' '}
                  {
                    <a href={`mailto:${email}`} className="experience-created-contact">
                      {email}
                    </a>
                  }
                </div>
                <div>
                  Phone Number:{' '}
                  {
                    <a href={`tel:${phoneNumber}`} className="experience-created-contact">
                      {phoneNumber}
                    </a>
                  }
                </div>
                <div>{`Submitted: ${formatDate(createdAt)} at ${formatTime(createdAt)}`}</div>
              </div>
            </div>
            {accepted === null ? (
              <div>
                <div className="row">
                  <div className="col experience-created-accept">
                    <form onSubmit={this.handleSubmitAcceptExperienceCreated} noValidate>
                      <button
                        type="submit"
                        disabled={experienceCreatedSubmitDisabled}
                        className="experience-created-accept-btn"
                      >
                        Accept
                      </button>
                    </form>
                  </div>
                  <div className="col experience-created-reject">
                    <form onSubmit={this.toggleRejectExperienceCreated} noValidate>
                      <button
                        type="submit"
                        disabled={experienceCreatedSubmitDisabled}
                        className="experience-created-reject-btn"
                      >
                        Reject
                      </button>
                    </form>
                  </div>
                </div>
                <div>
                  {rejectExperienceCreatedDisabled ? (
                    <div>
                      <form onSubmit={this.handleSubmitRejectExperienceCreated} noValidate>
                        <div className="form-group">
                          <div>
                            <textarea
                              name="rejectExperienceCreatedNote"
                              placeholder="Add your note"
                              value={rejectExperienceCreatedNote}
                              onChange={this.handleRejectExperienceCreated}
                              disabled={experienceCreatedSubmitDisabled}
                              className="experience-created-reject-note"
                            />
                            <span className="fas fa-pencil-alt experience-created-reject-note-icon"></span>
                          </div>
                          {rejectExperienceCreatedNoteError ? (
                            <div className="error-message">{rejectExperienceCreatedNoteError}</div>
                          ) : null}
                        </div>

                        <div>
                          <button
                            type="submit"
                            disabled={experienceCreatedSubmitDisabled}
                            className="experience-created-reject-note-submit-btn"
                          >
                            Submit
                          </button>
                        </div>
                      </form>
                    </div>
                  ) : null}
                </div>
              </div>
            ) : accepted ? (
              <div className="text-center experience-created-replied">Accepted</div>
            ) : (
              <div className="text-center experience-created-replied">Rejected</div>
            )}
          </div>
        </LazyLoad>
      </div>
    );
  };
}
