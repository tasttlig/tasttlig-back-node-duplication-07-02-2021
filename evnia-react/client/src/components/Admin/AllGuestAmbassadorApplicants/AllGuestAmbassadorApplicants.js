// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import moment from 'moment';

// Components
import Nav from '../../Navbar/Nav';
//import AllGuestAmbassadorApplicantsCard from './AllGuestAmbassadorApplicantsCard/AllGuestAmbassadorApplicantsCard';
// Styling
import './AllGuestAmbassadorApplicants.scss';

export default class AllGuestAmbassadorApplicants extends Component {
  // Set initial state
  state = {
    allGuestAmbassadorApplicantItems: [],
  };

  // Mount All Host Applicants page
  componentDidMount = () => {
    window.scrollTo(0, 0);

    const url = '/hosts/guest/amb/applications';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          allGuestAmbassadorApplicantItems: [
            ...this.state.allGuestAmbassadorApplicantItems,
            ...response.data.applications.reverse(),
          ],
        });
        console.log('all apps', response.data.applications);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render All Host Applicants page
  render = () => {
    const { allGuestAmbassadorApplicantItems } = this.state;

    return (
      <div>
        <Nav />

        <div className="all-host-applicants">
          <div className="all-host-applicants-navigation">
            <Link exact="true" to="/admin" className="all-host-applicants-navigation-content">
              Admin
            </Link>
            <span className="arrow">&nbsp;&gt;&nbsp;</span>All Guest Ambassador Applicants
          </div>

          {allGuestAmbassadorApplicantItems && allGuestAmbassadorApplicantItems.length ? (
            <div className="table-responsive mt-5">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Date Submitted</th>
                    <th>Influencer</th>
                    <th>Linkedin</th>
                    <th>Facebook</th>
                    <th>Youtube</th>
                    <th>Instagram</th>
                    <th>Twitter</th>
                    <th>Ambassador Intent</th>
                  </tr>
                </thead>
                <tbody>
                  {allGuestAmbassadorApplicantItems.map((a) => (
                    <tr key={a.tasttlig_user_id}>
                      <td>
                        <Link
                          exact="true"
                          to={`/admin/all-guest-ambassador-applicants/${a.application_id}`}
                        >
                          {`${a.email}`}
                        </Link>
                      </td>
                      <td className="all-host-applicants-business-type">{a.type}</td>
                      <td>{moment(a.updated_at).toLocaleString()}</td>
                      <td>{a.is_influencer ? 'Yes' : 'No'}</td>
                      <td>{a.linkedin_link}</td>
                      <td>{a.facebook_link}</td>
                      <td>{a.youtube_link}</td>
                      <td>{a.instagram_link}</td>
                      <td>{a.twitter_link}</td>
                      <td>{a.ambassador_intent_description}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div className="mt-5">
              <strong>No applications available.</strong>
            </div>
          )}
        </div>
      </div>
    );
  };
}
