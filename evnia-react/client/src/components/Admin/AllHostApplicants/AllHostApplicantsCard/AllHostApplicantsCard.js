// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import { toast } from 'react-toastify';

// Components
import { formatDate, formatTime } from '../../../Functions/Functions';

// Styling
import './AllHostApplicantsCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class AllHostApplicantsCard extends Component {
  // Set initial state
  state = {
    load: false,
    userItems: [],
    verified: false,
    rejectHostApplicantDisabled: false,
    rejectHostApplicantNote: '',
    hostApplicantSubmitDisabled: false,
  };

  // Load image helper function
  onLoad = () => {
    this.setState({ load: true });
  };

  // Toggle reject to host applicant button helper function
  toggleRejectHostApplicant = (event) => {
    event.preventDefault();

    this.setState((prevState) => {
      return {
        rejectHostApplicantDisabled: !prevState.rejectHostApplicantDisabled,
      };
    });
  };

  // Handle reject to host applicant change helper function
  handleRejectHostApplicant = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate reject to host applicant from admin helper function
  validateRejectHostApplicant = () => {
    let rejectHostApplicantNoteError = '';

    // Render reject to host applicant error message
    if (!this.state.rejectHostApplicantNote) {
      rejectHostApplicantNoteError = 'Note is required.';
    }

    // Set reject to host applicant error message state
    if (rejectHostApplicantNoteError) {
      this.setState({ rejectHostApplicantNoteError });
      return false;
    }

    return true;
  };

  // Accept host applicant helper function
  handleSubmitAcceptHostApplicant = async (event) => {
    event.preventDefault();

    try {
      const url = `/applications/${this.props.id}`;

      const data = {
        first_name: this.props.firstName,
        last_name: this.props.lastName,
        email: this.props.email,
        is_host: true,
        reject_note: '',
      };

      const response = await axios({ method: 'PUT', url, data });

      if (response) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(
          `Success! Thank you for accepting ${this.props.firstName} ${this.props.lastName}'s host application!`,
          {
            type: 'success',
            autoClose: 2000,
          },
        );

        this.setState({
          rejectHostApplicantNoteError: '',
          hostApplicantSubmitDisabled: true,
        });
      }
    } catch (error) {
      console.log(error);

      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Reject host applicant helper function
  handleSubmitRejectHostApplicant = async (event) => {
    event.preventDefault();

    const isValid = this.validateRejectHostApplicant();

    if (isValid) {
      const url = `/applications/${this.props.id}`;

      const data = {
        first_name: this.props.firstName,
        last_name: this.props.lastName,
        email: this.props.email,
        is_host: false,
        reject_note: this.state.rejectHostApplicantNote,
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(
            `Success! Thank you for rejecting ${this.props.firstName} ${this.props.lastName}'s host application!`,
            {
              type: 'success',
              autoClose: 2000,
            },
          );

          this.setState({
            rejectHostApplicantNoteError: '',
            hostApplicantSubmitDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Mount users
  componentDidMount = () => {
    const url = '/users';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          userItems: [...this.state.userItems, ...response.data.users.reverse()],
        });
      })
      .then(() => {
        this.state.userItems.map((item) => {
          if (item.id.toString() === this.props.userId) {
            this.setState({ verified: true });
          }
          return null;
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render All Host Applicants card
  render = () => {
    const {
      profileImage,
      firstName,
      lastName,
      email,
      phoneNumber,
      businessName,
      businessType,
      foodHandlerCertificate,
      voidCheque,
      onlineEmail,
      paypalEmail,
      stripeAccount,
      hostSelectionVideo,
      isHost,
      createdAt,
    } = this.props;

    const {
      load,
      verified,
      rejectHostApplicantDisabled,
      rejectHostApplicantNote,
      rejectHostApplicantNoteError,
      hostApplicantSubmitDisabled,
    } = this.state;

    return (
      <div>
        {verified ? (
          <LazyLoad once>
            <div className="host-applicants">
              <div className="row host-applicants-content">
                <div className="col-sm-3 host-applicants-picture-section">
                  {profileImage ? (
                    <img
                      src={profileImage}
                      alt={`${firstName} ${lastName}`}
                      onLoad={this.onLoad}
                      className={load ? 'host-applicants-user-picture' : 'loading-image'}
                    />
                  ) : (
                    <span className="fas fa-user-circle fa-7x host-applicants-default-picture"></span>
                  )}
                </div>
                <div className="col-sm-9 host-applicants-details-section">
                  <div>{`First Name: ${firstName}`}</div>
                  <div>{`Last Name: ${lastName}`}</div>
                  <div>
                    Email:{' '}
                    {
                      <a href={`mailto:${email}`} className="host-applicants-contact">
                        {email}
                      </a>
                    }
                  </div>
                  <div>
                    Phone Number:{' '}
                    {
                      <a href={`tel:${phoneNumber}`} className="host-applicants-contact">
                        {phoneNumber}
                      </a>
                    }
                  </div>
                  <div>{`Business Name: ${businessName}`}</div>
                  <div>{`Business Type: ${businessType}`}</div>
                  <div>
                    Food Handler Certificate:{' '}
                    {
                      <a
                        href={foodHandlerCertificate}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="host-applicants-contact"
                      >
                        Link
                      </a>
                    }
                  </div>
                  {voidCheque ? (
                    <div>
                      Void Cheque:{' '}
                      {
                        <a
                          href={voidCheque}
                          target="_blank"
                          rel="noopener noreferrer"
                          className="host-applicants-contact"
                        >
                          Link
                        </a>
                      }
                    </div>
                  ) : onlineEmail ? (
                    <div>{`Online Email: ${onlineEmail}`}</div>
                  ) : paypalEmail ? (
                    <div>{`PayPal Email: ${paypalEmail}`}</div>
                  ) : (
                    <div>{`Stripe Account: ${stripeAccount}`}</div>
                  )}
                  <div>
                    Host Selection Video:{' '}
                    {
                      <a
                        href={hostSelectionVideo}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="host-applicants-contact"
                      >
                        Link
                      </a>
                    }
                  </div>
                  <div>{`Submitted: ${formatDate(createdAt)} at ${formatTime(createdAt)}`}</div>
                </div>
              </div>
              {isHost === null ? (
                <div>
                  <div className="row">
                    <div className="col host-applicants-accept">
                      <form onSubmit={this.handleSubmitAcceptHostApplicant} noValidate>
                        <button
                          type="submit"
                          disabled={hostApplicantSubmitDisabled}
                          className="host-applicants-accept-btn"
                        >
                          Accept
                        </button>
                      </form>
                    </div>
                    <div className="col host-applicants-reject">
                      <form onSubmit={this.toggleRejectHostApplicant} noValidate>
                        <button
                          type="submit"
                          disabled={hostApplicantSubmitDisabled}
                          className="host-applicants-reject-btn"
                        >
                          Reject
                        </button>
                      </form>
                    </div>
                  </div>
                  <div>
                    {rejectHostApplicantDisabled ? (
                      <div>
                        <form onSubmit={this.handleSubmitRejectHostApplicant} noValidate>
                          <div className="form-group">
                            <div>
                              <textarea
                                name="rejectHostApplicantNote"
                                placeholder="Add your note"
                                value={rejectHostApplicantNote}
                                onChange={this.handleRejectHostApplicant}
                                disabled={hostApplicantSubmitDisabled}
                                className="host-applicants-reject-note"
                              />
                              <span className="fas fa-pencil-alt host-applicants-reject-note-icon"></span>
                            </div>
                            {rejectHostApplicantNoteError ? (
                              <div className="error-message">{rejectHostApplicantNoteError}</div>
                            ) : null}
                          </div>

                          <div>
                            <button
                              type="submit"
                              disabled={hostApplicantSubmitDisabled}
                              className="host-applicants-reject-note-submit-btn"
                            >
                              Submit
                            </button>
                          </div>
                        </form>
                      </div>
                    ) : null}
                  </div>
                </div>
              ) : isHost ? (
                <div className="host-applicants-replied">Accepted</div>
              ) : (
                <div className="host-applicants-replied">Rejected</div>
              )}
            </div>
          </LazyLoad>
        ) : null}
      </div>
    );
  };
}
