// // Libraries
// import axios from "axios";
// import React, { useEffect, useContext, useState } from "react";
// import { Link } from "react-router-dom";
// import { AppContext } from "../../ContextProvider/AppProvider";
// import { AuthModalContext } from "../../ContextProvider/AuthModalProvider";
// import { toast } from "react-toastify";

// // Styling
// import "./UserPreferences.scss";
// import tasttligLogoBlack from "../../assets/images/tasttlig-logo-black.png";
// import { typesOfCusines } from "../Functions/Functions";
// import { Checkbox, CheckboxGroup } from "../EasyForm";

// const UserPreferences = (props) => {
//   // Source tracking on new sign ups
//   const source = new URLSearchParams(props.location.search).get("source");
//   if (source) {
//     if (!localStorage.getItem("source")) {
//       localStorage.setItem("source", source);
//     }
//   }

//   // To use the JWT credentials
//   const appContext = useContext(AppContext);
//   const authModalContext = useContext(AuthModalContext);

//   const [typesOfCuisines, setTypesOfCuisines] = useState([
//     { name: "Indian", isChecked: false },
//     { name: "Greek", isChecked: false },
//     { name: "Italian", isChecked: false },
//     { name: "Mexican", isChecked: false },
//     { name: "Chinese", isChecked: false },
//     { name: "Japanese", isChecked: false },
//     { name: "Korean", isChecked: false },
//     { name: "Thai", isChecked: false },
//     { name: "Vietnamese", isChecked: false },
//     { name: "Indonesian", isChecked: false },
//     { name: "Spanish", isChecked: false },
//     { name: "Turkish", isChecked: false },
//     { name: "English", isChecked: false },
//     { name: "Lebanon", isChecked: false },
//     { name: "Jamaican", isChecked: false },
//     { name: "Nigerian", isChecked: false },
//   ]);

//   const userPreference = [];

//   const handleCheckChange = ((checked, value) => {
//     var cuisineList = typesOfCuisines.slice();
//     cuisineList.map((cuisine) => {
//       if (cuisine.name === value) {
//         cuisine.isChecked = checked;
//       }
//     })
//     setTypesOfCuisines(cuisineList);
//   });

//   const onSubmitProfileForm = async () => {
//     const url = `/user/update-preferences/${props.user.id}`;
//     var data = []
//     typesOfCuisines.map((cuisine) => {
//       if(cuisine.isChecked == true) {
//         data.concat([cuisine])
//       }
//     })
//     try {
//       const response = await axios({ method: "PUT", url, data });

//       if (response && response.data && response.data.success) {
//         setTimeout(() => {
//           window.location.href = "/";
//         }, 2000);
//         console.log("here")
//         toast("Success! Your taste preferences has been updated!", {
//           type: "success",
//           autoClose: 2000,
//         });

//         // setSubmitAuthDisabled(true);
//       }
//     } catch (error) {
//       toast("Error! Something went wrong!", {
//         type: "error",
//         autoClose: 2000,
//       });
//     }
//   }

//   // Mount Login page
//   useEffect(() => {
//     window.scrollTo(0, 0);
//   }, []);

//   // Render User Preferences page
//   return (
//     <div className="row">
//       <div className="col-lg-8 col-xl-6 px-0 login-background-image-one" />
//       <div className="col-lg-4 col-xl-4 px-0 login-background">
//         <div className="login-content">
//           <div className="mb-3 text-center">
//             <Link exact="true" to="/">
//               <img
//                 src={tasttligLogoBlack}
//                 alt="Tasttlig"
//                 className="main-navbar-logo"
//               />
//             </Link>
//           </div>

//           {appContext.state.errorMessage && (
//             <div className="mb-3 text-center invalid-message">
//               {appContext.state.errorMessage}
//             </div>
//           )}

//           <form onSubmit={onSubmitProfileForm} noValidate>
//             <div className="text-center">
//               <span className="preferences-content">
//                 What type of cuisines do you prefer?</span>&nbsp;
//             </div>
//             <div>
//               <div className="checkbox-preference">
//                 {typesOfCuisines.map((cuisine) => {
//                   return (
//                     <label className = "checkbox-preference">
//                       <span className="checkbox-text">
//                       {cuisine.name}
//                       </span>

//                       <input

//                         name="types"
//                         type="checkbox"
//                         checked={cuisine.isChecked}
//                         value={cuisine.name}
//                         onChange={(e) => handleCheckChange(e.target.checked, e.target.value)}
//                       ></input>
//                     </label>
//                   );

//                 })}</div>

//             </div>
//             <div className="mb-3">
//               <button
//                 type="submit"
//                 disabled={authModalContext.state.submitAuthDisabled}
//                 className="log-in-btn"
//               >
//                 Continue
//               </button>
//             </div>
//           </form>
//         </div>
//       </div>
//       <div className="col-xl-2 px-0 login-background-image-two" />
//     </div>
//   );
// };

// export default UserPreferences;
