import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { AppContext } from '../../ContextProvider/AppProvider';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import Swal from 'sweetalert2';
import { DateInput, Form, MultiImageInput } from '../EasyForm';
import { toast } from 'react-toastify';
import { Checkbox, CheckboxGroup } from 'rsuite';
import Radio from '../Shared/Radio';
import SponsoringConditions from '../LandingPage/TermsAndConditions/SponsoringConditions';
import Modal from 'react-modal';
import './CreateSampleSaleDonation.scss';

const animatedComponents = makeAnimated();

toast.configure();

const CreateSampleSaleDonation = (props) => {
  const methods = useForm();
  const [form, setForm] = useState('productForm');
  
 
  const [festivalList, setFestivalList] = useState([]);
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [festivalPreference, setFestivalPreference] = useState([]);
  const [images, setImages] = useState([]);
  const [formValues, setFormValues] = useState([]);
  const [productType, setProductType] = useState();
  const [category, setCategory] = useState();
  const [loading, setLoading] = useState(true);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [successMessage, setSuccessMessage] = useState('');
  const [nationalities, setNationalities] = useState([]);
  const [selectedDate, setselectedDate] = useState(null);
  const [radioSelected, setRadioSelected] = useState('food');
  const [radioSelected2, setRadioSelected2] = useState('host');
  const [services, setServices] = useState([]);
  const [experiences, setExperiences] = useState([]);
  const [products, setProducts] = useState([]);
  const [priceValue, setPriceValue] = useState('');
  const [experienceType, setExperienceType] = useState('');
  const [ownerItemType, setOwnerItemType] = useState(['Vendor'])
  const [sponsoringConditions, setSponsoringConditions] = useState(false);
  const [submissionConditions, setSubmissionConditions] = useState(false);

  const refFrom = useRef()

  const { addToFestivalsDropdown, festivalId } = props;
  const toggleTab = (index) => {
    setToggleState(index);
  };
  const toggleService = (service, index) => {
    setRadioSelected2(service);
    setToggleState(index);
  };
  
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const new_user_email = localStorage.getItem('new_user_email');
  const user = appContext.state.user;
  const userRole = appContext.state.user.role;
  const userId = appContext.state.user.id;
  const user_CRA_Number = appContext.state.user.CRA_business_number;
  const [toggleState, setToggleState] = useState(props.sponsorType || userRole.includes("SPONSOR") &&  !userRole.includes("VENDOR") && !userRole.includes("HOST")? 3 : userRole.includes("VENDOR") && !userRole.includes("HOST") ? 2 : 1);


  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });
    setNationalities(response.data.nationalities);
  };

  const fetchFestivalDetails = async () => {
    const url = '/festival/allFestival';

    try {
      const response = await axios({ method: 'GET', url });
      return response;
    } catch (error) {
      return error.response;
    }
  };

  const loadServices = async () => {
    const url = `/services/details/${userId}`;

    const response = await axios({ method: 'GET', url });
    if (response.data.success) {
      setServices(response.data.details);
    }
  };

  const loadExperiences = async () => {
    const url = `/experience/user/all`;

    const response = await axios({ method: 'GET', url });
    if (response.data.success) {
      setExperiences(response.data.details.data);
    }
  };

  const loadAllUserProducts = async () => {
    const response = await axios({
      method: 'GET',
      url: `/all-products/user/all`,
      data: {
        user_id: userId,
      },
    });
    setProducts(response.data.details.data);
    return response;
  };


  const fetchAll = async () => {
    await fetchNationalities();
    await fetchFestivalDetails().then((response) => {
      setFestivalDetails(response.data.festival_list);
    });
    await loadServices();
    await loadExperiences();
    await loadAllUserProducts();
    // setLoading(false);
  };

  useEffect(() => {
    fetchAll();
  }, []);
  // form select component options

  const productTypeOptions = [
    { value: 'food', label: 'Food' },
    { value: 'other', label: 'Other' },
  ];

  const experienceTypeOptions = [
    { value: 'takeout', label: 'Takeout' },
    { value: 'dine-in', label: 'Dine-In' },
    { value: 'outdoor', label: 'Outdoor' },
    { value: 'Indoor', label: 'Indoor' },
  ];

  const userProducts = products.map((product) => ({
    value: product.product_id,
    label: product.title,
  }));

  //service_id: 34 service_name: "dd"
  const userServices = services.map((service) => ({
    value: service.service_id,
    label: service.service_name,
  }));

  const userExperiences = experiences.map((experience) => ({
    value: experience.experience_id,
    label: experience.experience_name,
  }));

  /* const userExperiences = [
    { value: 'experience1', label: 'Experience1' },
    { value: 'experience2', label: 'Experience2' },
  ]; */

  const categoryOptions = [
    { value: 'product', label: 'Product' },
    { value: 'service', label: 'Service' },
  ];

  const categoryOptionsWithExperience = [
    { value: 'product', label: 'Product' },
    { value: 'service', label: 'Service' },
    { value: 'experience', label: 'Experience' },
  ];

  const dietaryRestrictionsOptions = [
    { value: 'vegetarian', label: 'Vegetarian' },
    { value: 'vegan', label: 'Vegan' },
    { value: 'glutenFree', label: 'Gluten-Free' },
    { value: 'halal', label: 'Halal' },
  ];

  const spiceOptions = [
    { value: 'mild', label: 'Mild' },
    { value: 'medium', label: 'Medium' },
    { value: 'hot', label: 'Hot' },
  ];

  const daysAvailableOptions = [
    { value: 'available_on_monday', label: 'Monday' },
    { value: 'available_on_tuesday', label: 'Tuesday' },
    { value: 'available_on_wednesday', label: 'Wednesday' },
    { value: 'available_on_thursday', label: 'Thursday' },
    { value: 'available_on_friday', label: 'Friday' },
    { value: 'available_on_saturday', label: 'Saturday' },
    { value: 'available_on_sunday', label: 'Sunday' },
  ];

  const sizeOptions = [
    { value: 'biteSize', label: 'Bite Size' },
    { value: 'quarter', label: 'Quarter' },
    { value: 'half', label: 'Half' },
    { value: 'full', label: 'Full' },
  ];

  const otherSizeOptions = [
    { value: 'small', label: 'Small' },
    { value: 'medium', label: 'Medium' },
    { value: 'Large', label: 'Large' },
  ];

  const usersHostedFestivals = festivalDetails.map((n) => ({
    value: n.festival_id,
    label: n.festival_name,
  }));

  const productNationalities = nationalities.map((n) => ({
    value: n.id,
    label: n.nationality,
  }));

  console.log("ownerItem type:", ownerItemType)
  // end form select component options
  const { register, handleSubmit, control, setValue, errors } = useForm({
  });

  // Submit food sample helper function
  const submitFoodSample = async (data) => {
    window.scrollTo(0, 0);
    // if (!new_user_email) {
    //   data.userEmail = JSON.parse(localStorage.getItem('dataFromBecomeHost')).email;
    // }
    data.sponsorType = props.sponsorType ? props.sponsorType : null; 
    let userExist;
    const url = '/all-products/add';

    try {
      const response = await axios({
        method: 'POST',
        url,
        data: [data],
      });

      if (userRole && userRole.includes('ADMIN')) {
        userExist = await axios({
          method: 'GET',
          url: `/user/${data.userEmail}`,
        });
      }
      if (
          response &&
          response.data &&
          response.data.success 
 
      ) {
        defaultOnCreatedAction();
      }
    } catch (error) {
      toast('Error! Please fill all the fields with *!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const submitExperience = async (data) => {
    window.scrollTo(0, 0);
    const url = '/experiences/add';
    data.sponsorType = props.sponsorType ? props.sponsorType : null; 
    try {
      const response = await axios({
        method: 'POST',
        url,
        data,
      });

      if (response && response.data && response.data.success) {
        toast(`Experience created successfully!`, {
          type: 'success',
          autoClose: 2000,
        });

        setTimeout(() => {
          window.location.href = '/dashboard';
        }, 3000);
      }
    } catch (error) {
      toast('Error! Please fill all the fields with *!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const submitService = async (data) => {
    window.scrollTo(0, 0);
    const url = '/services/add';
    data.sponsorType = props.sponsorType ? props.sponsorType : null; 
    try {
      const response = await axios({
        method: 'POST',
        url,
        data,
      });

      if (response && response.data && response.data.success) {
        toast(`Service created successfully!`, {
          type: 'success',
          autoClose: 2000,
        });

        setTimeout(() => {
          window.location.href = '/dashboard';
        }, 3000);
      }
    } catch (error) {
      toast('Error! Please fill all the fields with *!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values, submissionConditions) => {

    setFormValues(values);
    const {
      productType,
      size,
      provinceTerritory,
      dietaryRestrictions,
      daysAvailable,
      addToFestivals,
      productNationalities,
      spiceLevel,
      custom,

      ...rest
    } = values;
    let numOfFestival = [];
    if (addToFestivals) {
      for (let festival of addToFestivals) {
        numOfFestival.push(festival.value);
      }
      /*  addToFestivals.map((festival) => {
          return festival.value;
        }), */
    } else {
      numOfFestival.push(festivalId);
    }


    if (form === 'experienceForm') {
      let products_selected = null;
      let services_selected = null;
      if (values.addProductsToExperience && values.addProductsToExperience.length > 0) {
        products_selected = [
          ...values.addProductsToExperience.map((p) => {
            return p.value;
          }),
        ];
      }
      if (values.addServicesToExperience && values.addServicesToExperience.length > 0) {
        services_selected = [
          ...values.addServicesToExperience.map((s) => {
            return s.value;
          }),
        ];
      }

      let data = {
        experience_images: images,
        experience_name: values.name,
        experience_price: values.price,
        experience_capacity: values.capacity,
        experience_size_scope: values.sizeScope.value,
        experience_description: values.description,
        experience_type: values.experienceTypeOptions ? values.experienceTypeOptions.value : null,
        experience_offering_type:  ownerItemType && ownerItemType.length !== 0 ? ownerItemType : null,

        start_date: values.start_date,
        end_date: values.end_date,
        start_time: values.start_time.toString().substring(16, 21),
        end_time: values.end_time.toString().substring(16, 21),
        additional_pricing_info: `${values.taxInformation ? values.taxInformation.value : null}, ${
          values.shippingInformation ? values.shippingInformation.value : null
        }`,
        additional_information: custom
          ? custom
          : null,
        festival_selected: props.selectedVendingFestival && props.selectedVendingFestival !== null ? Array.from(String(props.selectedVendingFestival), Number) : numOfFestival,
        products_selected,
        services_selected,
      };

      if(submissionConditions && submissionConditions === true ) {
        data.experience_offering_type = ["Sponsor"]
        await submitExperience(data);
      }
  
      if(Number(values.price) === 0) {
        setSponsoringConditions(true)
      } else {
        await submitExperience(data);
      }
      return;
    }

    if (form === 'serviceForm') {
      let products_selected = null;
      let experiences_selected = null;
      if (values.addProductsToService && values.addProductsToService.length > 0) {
        products_selected = [
          ...values.addProductsToService.map((p) => {
            return p.value;
          }),
        ];
      }
      if (values.addExperiencesToService && values.addExperiencesToService.length > 0) {
        experiences_selected = [
          ...values.addExperiencesToService.map((s) => {
            return s.value;
          }),
        ];
      }

      let data = {
        service_images: images,
        service_name: values.name,
        service_price: values.price,
        service_capacity: values.capacity,
        service_size_scope: values.sizeScope.value,
        service_description: values.description,
        service_type: radioSelected,
        start_date: values.start_date,
        end_date: values.end_date,
        start_time: values.start_time.toString().substring(16, 21),
        end_time: values.end_time.toString().substring(16, 21),
        products_selected,
        experiences_selected,
        tax_included_or_not: values.taxInformation ? values.taxInformation.value : null,
        shipping_included_or_not: values.shippingInformation
          ? values.shippingInformation.value
          : null,
        // additional_pricing_info: `${values.taxInformation.value}, ${values.shippingInformation.value}`,
        service_offering_type:  ownerItemType && ownerItemType.length !== 0 ? ownerItemType : null,

        additional_information: custom
          ? custom
          : null,
        festival_selected:props.selectedVendingFestival && props.selectedVendingFestival !== null ? Array.from(String(props.selectedVendingFestival), Number) : numOfFestival,
      };

      if(submissionConditions && submissionConditions === true ) {
        data.service_offering_type = ["Sponsor"]
        await submitService(data);
      }
  
      if(Number(values.price) === 0) {
        setSponsoringConditions(true)
      } else {
        await submitService(data);
      }
      return;
    }

    const dietaryRestrictionValue = function (requiredValue) {
      let boolValue = false;
      if(dietaryRestrictions) {
        for (let restriction of dietaryRestrictions) {
          if (restriction.value === requiredValue) {
            boolValue = true;
          }
        }
        return boolValue;
      } else {
        return null
      }
    };

    

    // let dietaryRestrictionValue;
    
    // if(radioSelected=='food'){
    //   dietaryRestrictionValue = function (requiredValue) {
    //     let boolValue = false;
    //     for (let restriction of dietaryRestrictions) {
    //       if (restriction.value === requiredValue) {
    //         boolValue = true;
    //       }
    //     }
    //     return boolValue;
    //   };
    // }
    // else {
    //   dietaryRestrictionValue = ''; 
    // }


    const daysAvailableValue = function (requiredValue) {
      let bool = false;
      if(daysAvailable) {
        for (let value of daysAvailable) {
          if (value.value === requiredValue) {
            bool = true;
          }
        }
        return bool;
      } else {
        return null
      }
    };

    // let address = addressLine1;

    // if (addressLine2 && addressLine2.length > 0) {
    //   address = `${address}, ${addressLine2}`;
    // }
    let data = {
      ...rest,
      // address,
      festivals: props.selectedVendingFestival && props.selectedVendingFestival !== null ? Array.from(String(props.selectedVendingFestival), Number) : numOfFestival,
      sample_size: size ? size.label : null,
      spice_level: spiceLevel,
      images: images,
      additional_information: custom,
      nationality_id:  productNationalities ? productNationalities.value : null,
      // is_vegetarian: dietaryRestrictionValue('vegetarian'),
      // is_vegan: dietaryRestrictionValue('vegan'),
      // is_gluten_free: dietaryRestrictionValue('glutenFree'),
      // is_halal: dietaryRestrictionValue('halal'),
      is_vegetarian: dietaryRestrictionValue ? dietaryRestrictionValue('vegetarian') : false,
      is_vegan: dietaryRestrictionValue ? dietaryRestrictionValue('vegan') : false,
      is_gluten_free: dietaryRestrictionValue ? dietaryRestrictionValue('glutenFree'): false,
      is_halal: dietaryRestrictionValue ? dietaryRestrictionValue('halal'): false,
      is_available_on_monday: daysAvailableValue('available_on_monday'),
      is_available_on_tuesday: daysAvailableValue('available_on_tuesday'),
      is_available_on_wednesday: daysAvailableValue('available_on_wednesday'),
      is_available_on_thursday: daysAvailableValue('available_on_thursday'),
      is_available_on_friday: daysAvailableValue('available_on_friday'),
      is_available_on_saturday: daysAvailableValue('available_on_saturday'),
      is_available_on_sunday: daysAvailableValue('available_on_sunday'),
      // state: provinceTerritory,
      country: 'Canada',
    };

    data.start_time = values.start_time ? values.start_time.toString().substring(16, 21) : null;
    data.end_time = values.end_time ? values.end_time.toString().substring(16, 21) : null;
    // data.product_owner_type = toggleState && toggleState === 1 ? "Host" : toggleState === 2 ? "Vendor" : toggleState === 3 ? "Sponsor" : null
    data.product_offering_type = ownerItemType && ownerItemType.length !== 0 ? ownerItemType : null

    if (props.update) {
      data = {
        food_sample_update_data: data,
      };
    }
    
    if(submissionConditions && submissionConditions === true ) {
      data.product_offering_type = ["Sponsor"]
      await submitFoodSample(data);
    }

    if(Number(values.price) === 0) {
      setSponsoringConditions(true)
    } else {
      await submitFoodSample(data);
    }
  };

  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      window.location.href = '/dashboard';
    }, 2000);

    toast(`Success! Thank you for creating a Product!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSuccessMessage('Your food sample will be shown as soon as your application is approved.');
    setSubmitAuthDisabled(true);
  };

  let value = {};

  const handleChange = (selector, event) => {
    if (selector === 'category') {
      handleCategoryChange(event);
    }
  };

  const handleCategoryChange = (event) => {
    if (event.value === 'product') {
      // setRadioSelected('food');
      // setForm('productForm');
      if (userRole.includes('Vendor')&&!userRole.includes('Host'))
      {
        setRadioSelected('other');
        setForm('productForm');
      } else
      {
        setRadioSelected('food');
        setForm('productForm');
      }
    } else if (event.value === 'service') {
      setRadioSelected('other');
      setForm('serviceForm');
    } else if (event.value === 'experience') {
      setRadioSelected('other');
      setForm('experienceForm');
    }
  };

  

  useEffect(() => {
    if (userRole.includes('VENDOR')&&!userRole.includes('HOST'))
      {
        setRadioSelected('other');
        setRadioSelected2('vend');
        setForm('productForm');
      }
    else if (userRole.includes('HOST'))
      {
        setRadioSelected('other');
        setRadioSelected2('host');
        setForm('productForm');
      }
    else if (!userRole.includes('VENDOR')&&!userRole.includes('HOST'))
      {
        setRadioSelected('other');
        setRadioSelected2('sponsor');
        setForm('productForm');
      }
  }, []);


  return (
    <Fragment>
      <Form data={{ images: images, values: formValues }} formRef={refFrom} className="container" onSubmit={handleSubmit(onSubmit)} >
        <div className="form-row">
          <div className="col">
            <Controller
              control={control}
              name="category"
              ref={register({ required: true })}
              render={(props) => (
                <Select
                  value={category}
                  onChange={(event) => handleChange('category', event)}
                  // selected={props.value}
                  // options={toggleState === 1 ? categoryOptions : categoryOptionsWithExperience}
                  options={categoryOptionsWithExperience}
                  placeholder="Create"
                />
              )}
            />
          </div>
        </div>
      
        {form === 'productForm' || form === 'serviceForm' || form === 'experienceForm' ? (
          <Fragment>
            <div className="form-row">
              {form === 'experienceForm' ? (
                <div className="col">
                  <Controller
                    options={experienceTypeOptions}
                    as={Select}
                    name="experienceType"
                    type="text"
                    control={control}
                    placeholder="Experience Type"
                    ref={register({ required: true })}
                  />

                </div>
              ) : ( <div className="col radio-product-type">
              <Radio
                value="food"
                selected={radioSelected}
                text="Food"
                onChange={setRadioSelected}
              />
              <Radio
                value="other"
                selected={radioSelected}
                text="Other"
                onChange={setRadioSelected}
              />
              </div> )                  
              }
            </div>
            <div className="form-row">
              <div className="col image-upload">
                <MultiImageInput
                  name="images"
                  dropbox_label="Click or drag-and-drop to upload one or more images"
                  onChange={(data) => setImages(data)}
                  required
                />
              </div>
            </div>
            <div className="form-row">
              <div className="col">
                <input
                  name="name"
                  type="text"
                  className={errors.name ? 'form-control invalid-input' : 'form-control'}
                  placeholder="Name*"
                  ref={register({ required: true })}
                />
              </div>
              {/* {toggleState === 1 || toggleState === 3 ? null : ( */}
                <div className="col">
                  <input
                    name="price"
                    type="text"
                    inputMode="numeric"
                    className={errors.price ? 'form-control invalid-input' : 'form-control'}
                    placeholder="Price*"
                    onChange={(e) => setPriceValue(e.target.value)}
                    ref={register({ required: true })}
                  />
                </div>
              {/* )} */}
            </div>
            <div className="form-row">
              <div className="col">
                {form === 'serviceForm' || form === 'experienceForm' ? (
                  <input
                    name="capacity"
                    type="text"
                    inputMode="numeric"
                    className={errors.quantity ? 'form-control invalid-input' : 'form-control'}
                    placeholder="Capacity*"
                    ref={register({ required: true })}
                  />
                ) : (
                  <input
                    name="quantity"
                    type="text"
                    inputMode="numeric"
                    className={errors.quantity ? 'form-control invalid-input' : 'form-control'}
                    placeholder="Quantity*"
                    ref={register({ required: true })}
                  />
                )}
              </div>
              <div className="col">
                {form === 'serviceForm' || form === 'experienceForm' ? (
                  <Controller
                    options={radioSelected === 'food' ? sizeOptions : otherSizeOptions}
                    as={Select}
                    name="sizeScope"
                    type="text"
                    control={control}
                    placeholder="Size Scope*"
                    ref={register({ required: true })}
                  />
                ) : (
                  <Controller
                    options={radioSelected === 'food' ? sizeOptions : otherSizeOptions}
                    as={Select}
                    name="size"
                    type="text"
                    control={control}
                    placeholder="Size*"
                    ref={register({ required: true })}
                  />
                )}
              </div>
            </div>
            <div className="form-row">
              <div className="col">
                <input
                  name="description"
                  type="text"
                  className={errors.description ? 'form-control invalid-input' : 'form-control'}
                  placeholder="Description*"
                  ref={register({ required: true })}
                />
              </div>
                <div className="col">
                  <input
                  name="custom"
                  type="text"
                  className='form-control'
                  placeholder="Custom Information about Item"
                  ref={register({ required: false })}
                />
                </div>
            </div>
            {radioSelected === 'food' && form !== 'serviceForm' && form !== 'experienceForm' ? (
              <div className="form-row">
                <div className="col">
                  <Controller
                    closeMenuOnSelect={false}
                    as={Select}
                    components={animatedComponents}
                    isMulti
                    options={dietaryRestrictionsOptions}
                    name="dietaryRestrictions"
                    type="text"
                    control={control}
                    // className={errors.dietaryRestrictions ? 'form-control invalid-input' : 'form-control'}
                    placeholder="Dietary Restrictions"
                    ref={register({ required: true })}
                  />
                </div>
                <div className="col">
                  <Controller
                    options={spiceOptions}
                    as={Select}
                    name="spiceLevel"
                    type="text"
                    control={control}
                    // className={errors.spiceLevel ? 'form-control invalid-input' : 'form-control'}
                    placeholder="Spice Level"
                    ref={register({ required: true })}
                  />
                </div>
              </div>
            ) : null}
            <div className="form-row">
              {form !== 'experienceForm' && form !== 'serviceForm' ? (
                <div className="col">
                  <Controller
                    closeMenuOnSelect={false}
                    components={animatedComponents}
                    isMulti
                    as={Select}
                    options={daysAvailableOptions}
                    name="daysAvailable"
                    type="text"
                    control={control}
                    // className={errors.dietaryRestrictions ? 'form-control invalid-input' : 'form-control'}
                    placeholder="Days Available"
                    ref={register({ required: true })}
                  />
                </div>
              ) : (
                <div className="col start-end-time-inputs">
                  <Controller
                    control={control}
                    name="start_date"
                    ref={register({ required: true })}
                    render={(props) => (
                      <DatePicker
                        className="form-control"
                        dateFormat="yyyy/MM/dd"
                        placeholderText="Start Date*"
                        onChange={(e) => props.onChange(e)}
                        selected={props.value}
                      />
                    )}
                  />
                  <Controller
                    control={control}
                    name="end_date"
                    ref={register({ required: true })}
                    render={(props) => (
                      <DatePicker
                        className="form-control"
                        dateFormat="yyyy/MM/dd"
                        placeholderText="End Date*"
                        onChange={(e) => props.onChange(e)}
                        selected={props.value}
                      />
                    )}
                  />
                </div>
              )}
              <div className="col start-end-time-inputs">
                <Controller
                  control={control}
                  name="start_time"
                  ref={register({ required: true })}
                  render={(props) => (
                    <DatePicker
                      className="form-control"
                      placeholderText="Start Time*"
                      showTimeSelect
                      showTimeSelectOnly
                      dateFormat="h:mm aa"
                      onChange={(e) => props.onChange(e)}
                      selected={props.value}
                    />
                  )}
                />
                <Controller
                  control={control}
                  name="end_time"
                  ref={register({ required: true })}
                  render={(props) => (
                    <DatePicker
                      className="form-control"
                      placeholderText="End Time*"
                      showTimeSelect
                      showTimeSelectOnly
                      dateFormat="h:mm aa"
                      onChange={(e) => props.onChange(e)}
                      selected={props.value}
                    />
                  )}
                />
              </div>
            </div>
            <div className="form-row">
              {form === 'experienceForm' ? (
                <Fragment>
                  <div className="col">
                    <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      control={control}
                      options={userProducts}
                      name="addProductsToExperience"
                      type="text"
                      placeholder="Add Your Products"
                      ref={register({ required: false })}
                    />
                  </div>
                  <div className="col">
                    <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      control={control}
                      options={userServices}
                      name="addServicesToExperience"
                      type="text"
                      placeholder="Add Your Services"
                      ref={register({ required: false })}
                    />
                  </div>
                </Fragment>
              ) : form === 'serviceForm' ? (
                <Fragment>
                  <div className="col">
                    <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      control={control}
                      options={userProducts}
                      name="addProductsToService"
                      type="text"
                      placeholder="Add Your Products"
                      ref={register({ required: false })}
                    />
                  </div>
                  <div className="col">
                    <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      control={control}
                      options={userExperiences}
                      name="addExperiencesToService"
                      type="text"
                      placeholder="Add Your Experiences"
                      ref={register({ required: false })}
                    />
                  </div>
                </Fragment>
              ) : (
                <div className="col">
                  <Controller
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    isMulti
                    as={Select}
                    control={control}
                    options={productNationalities}
                    name="productNationalities"
                    type="text"
                    placeholder="Country of Origin"
                    ref={register({ required: true })}
                  />
                </div>
              )}
            </div>

            {form === 'experienceForm' || form === 'serviceForm' ? (
              <Fragment>
                <div className="form-row">
                  <div className="col">
                    <Controller
                      closeMenuOnSelect={true}
                      as={Select}
                      control={control}
                      options={[
                        { value: 'taxes_included', label: 'Taxes Included' },
                        { value: 'taxes_not_included', label: 'Taxes Not Included' },
                      ]}
                      name="taxInformation"
                      type="text"
                      placeholder="Tax Information"
                      ref={register({ required: true })}
                    />
                  </div>
                  <div className="col">
                    <Controller
                      closeMenuOnSelect={true}
                      as={Select}
                      control={control}
                      options={[
                        { value: 'shipping_included', label: 'Shipping Included' },
                        { value: 'shipping_not_included', label: 'Shipping Not Included' },
                      ]}
                      name="shippingInformation"
                      type="text"
                      placeholder="Shipping Information"
                      ref={register({ required: true })}
                    />
                  </div>
                </div>
              </Fragment>
            ) : null}

            {addToFestivalsDropdown && userRole.includes("HOST") ? (
              <div className="form-row">
                <div className="col">
                  <Controller
                    closeMenuOnSelect={false}
                    components={animatedComponents}
                    isMulti
                    as={Select}
                    options={usersHostedFestivals}
                    name="addToFestivals"
                    type="text"
                    control={control}
                    placeholder="Add to Festival(s)"
                    ref={register({ required: true })}
                  />
                </div>
              </div>
            ) : null}

            {/* <div className="row">
              <div className="tab-row">
                <div className="service-switcher">

                { userRole && (userRole.includes('HOST') && !props.sponsorType ) ? (
                  <Radio
                    selected={radioSelected2}
                    value="host"
                    text="Host"
                    onChange={() => toggleService("host", 1) }
                  />
                ) : null} 

                 { userRole && (userRole.includes('VENDOR')  && !props.sponsorType)? (
                  <Radio
                    selected={radioSelected2}
                    value="vend"
                    text="Vend"
                    onChange={() => toggleService("vend", 2)}
                  />
                ) : null}  

                {userRole && (userRole.includes('SPONSOR') || props.sponsorType) ? (
                  <Radio
                    selected={radioSelected2}
                    
                    value="sponsor"
                    text="Sponsor"
                    onChange={() => toggleService("sponsor",3)}
                  />
                ) :null} 

                </div>
              </div>
            </div> */}
             <CheckboxGroup name="ownerItemType" value={ownerItemType} onChange={setOwnerItemType}>
                {/* {(Checkbox) => ( */}
                  {/* <> */}
                    {userRole && userRole.includes("HOST") ?    
                        <Checkbox value="Host" > Promote this Item for free sampling.
                      </Checkbox> : null}
                      {userRole && userRole.includes("SPONSOR") ?  
                      <Checkbox value="Sponsor" > This is a Sponsored Item.
                    </Checkbox> : null}
                  {/* </> */}
                {/* )} */}
              </CheckboxGroup>

              {sponsoringConditions ? (
                <Modal
                  className="terms-conditions-modal"
                  isOpen={sponsoringConditions}
                  onRequestClose={() => setSponsoringConditions(false)}
                >
                  <div className="terms-conditions-background-image" />
                  <SponsoringConditions
                   user_id={props.user_id}
                   viewHostingConditionsState={sponsoringConditions}
                   changeBusinessConditions={(e) => {setSponsoringConditions(e)
                    setSubmissionConditions(e)
                  }}
                   changeSubmissionConditions={(e) => 
                    {
                      // setOwnerItemType(["Sponsor"])
                      onSubmit(formValues, e);
                    
                    }}
                  />
                </Modal>
              ) : null}
             

            <div className="row save-changes-btn">
              {userRole && userRole.includes('HOST') || userRole.includes('Vendor')? (  <button type="submit" className="profile-button-primary">
                  Add to Festival
                </button>) : (
                  <button type="submit" className="profile-button-primary">
                  Save
                </button>
                )}
            </div>
          </Fragment>
        ) : null}
      </Form>
    </Fragment>
  );
};

export default CreateSampleSaleDonation;
