// Libraries
import React, { useState, useEffect } from 'react';
import { Slide } from 'react-slideshow-image';
//import 'react-slideshow-image/dist/styles.css'

const ImageSlider = (props) => {
  const { images } = props;
  const [className, setClassName] = useState('');
  const properties = {
    transitionDuration: 500,
    indicators: false,
    arrows: true,
    autoplay: false,
  };
  let card = props.isCard;
  useEffect(() => {
    if (card) {
      setClassName('lazy festival-card-image');
    } else {
      setClassName('lazy festival-details-images');
    }
  }, []);

  return (
    <div className="image-slider-container">
      <Slide {...properties}>
        {images.map((m) => (
          <div key={m} className="each-slide">
            <img className={className} src={m} alt={m} />
          </div>
        ))}
      </Slide>
    </div>
  );
};

export default ImageSlider;
