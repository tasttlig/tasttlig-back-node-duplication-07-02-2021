// Libraries
import React from 'react';

// Styling
import './Backdrop.scss';

// Backdrop when clicking on toggle
const Backdrop = (props) => <div className="backdrop" onClick={props.backdropClickHandler} />;

export default Backdrop;
