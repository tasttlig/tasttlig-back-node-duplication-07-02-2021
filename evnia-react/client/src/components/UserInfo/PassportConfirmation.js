// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
//import countryList from "react-select-country-list";

//components
// import NationalitySelector from "../Passport/NationalitySelector";
import AsyncSelect from 'react-select/async';
import Select from '../EasyForm/Controls/Select';
// import { canadaProvincesTerritories } from "../../../Functions/Functions";

// Styling
//import "./UserInfo.scss";
import './PassportConfirmation.scss';
import confirmationImage from '../../assets/images/passportConfirmation.png';
import vendingImage from '../../assets/images/vending-logo.png';
import hostingImage from '../../assets/images/hosting-logo.png';
import sponsoringImage from '../../assets/images/sponsoring-logo.png';
import attendingImage from '../../assets/images/attending-logo.png';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import UserBackgroundImage from '../../assets/images/free-food.jpg';
import checkMarkImage from '../../assets/images/check-mark.jpg';
import { nationalities } from '../Apply/nationality';
// import { response } from "express";

const UserInfo = (props) => {
  const userData = {
    age: '',
    occupation: '',
    maritalStatus: '',
    nationalities: [],
    location: '',
    zipcode: '',
    streetName: '',
    streetNumber: '',
    unit: '',
  };

  //set initial state
  const { update, handleSubmit } = useForm({ defaultValues: userData });
  const [selectedItem, setSelectedItem] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Mount Login page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render User Info page
  return (
    <div className="row">
      <div className="col-lg-1 col-xl-4 px-0 user-background-image-one" />
      <div className="col-lg-8 col-xl-5 px-0 login-background">
        <div className="login-content">
          {/* <row>
                    <div className="tasttlig-image">
                        <Link exact="true" to="/">
                            <img
                                src={tasttligLogoBlack}
                                // width = "800"
                                // height = "300"
                                alt="Tasttlig"
                                //className="main-navbar-logo"
                            />
                        </Link>
                    </div>  
                    </row> */}
          {/* <row>
                    <div className="check-mark-image">
                        <img src={checkMarkImage}
                        width = "400"
                        height = "300"
                                alt="Tasttlig"/>
                    </div>
                    </row> */}
          <div className="passport-confirmation-image">
            <img src={confirmationImage} width="700" height="241" alt="Tasttlig" />
            {/* <Link exact="true" to="/">
                            <img
                                src={CheckMarkImage}
                                alt="Tasttlig"
                                className="main-navbar-logo"
                            />
                        </Link> */}
          </div>
          <div>
            <row className="input-style-5">I am here to:</row>
          </div>
          <div className="logos-styling">
            <row>
              <Link
                className="logos-styling1"
                exact="true"
                to={{
                  pathname: '/business-passport',
                }}
              >
                <img
                  src={hostingImage}
                  // width = "800"
                  // height = "300"
                  alt="Tasttlig"
                  //className="main-navbar-logo"
                />
              </Link>
              <Link
                className="logos-styling1"
                exact="true"
                to={{
                  pathname: '/business-passport',
                  params: 'vendor-path',
                }}
              >
                <img
                  src={vendingImage}
                  // width = "800"
                  // height = "300"
                  //alt="Tasttlig"
                  //className="main-navbar-logo"
                />
              </Link>
              <Link
                className="logos-styling1"
                exact="true"
                to={{
                  pathname: '/business-passport',
                  is_sponsor: true,
                }}
              >
                <img
                  src={sponsoringImage}
                  // width = "800"
                  // height = "300"
                  //alt="Tasttlig"
                  //className="main-navbar-logo"
                />
              </Link>
              <Link className="logos-styling1" exact="true" to="/">
                <img
                  src={attendingImage}
                  // width = "800"
                  // height = "300"
                  //alt="Tasttlig"
                  //className="main-navbar-logo"
                />
              </Link>
            </row>
          </div>
        </div>
      </div>
      <div className="col-lg-1 col-xl-3 px-0 user-background-image-two" />
    </div>
  );
};

export default UserInfo;
