// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';
import Footer from '../Home/Footer/Footer';

// Styling
import '../Home/Home.scss';

export default class BlogThree extends Component {
  render = () => {
    return (
      <div>
        <Nav />

        <div className="page-title-area item-bg3">
          <div className="container">
            <h1>Previous Experiences</h1>
            <span>Our Previous Experiences</span>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>Previous Experiences</li>
            </ul>
          </div>
        </div>

        <section className="blog-area ptb-120">
          <div className="blog-container">
            <div className="widget widget_search">
              <form>
                <input type="text" className="search-form" placeholder="Search here..." />
                <button type="submit">
                  <i className="icofont-search"></i>
                </button>
              </form>
            </div>

            <input id="check01" type="checkbox" name="menu" />
            <label className="dropdown-Blog" for="check01">
              Categories
            </label>
            <ul class="submenu">
              <li>
                <Link to="#">Dining Experiences</Link>
              </li>
              <li>
                <Link to="#">City Tours</Link>
              </li>
              <li>
                <Link to="#">Boat Tours</Link>
              </li>
              <li>
                <Link to="#">Music Experiences</Link>
              </li>
              <li>
                <Link to="#">Arts Exhibitions</Link>
              </li>
              <li>
                <Link to="#">Sports Games</Link>
              </li>
            </ul>

            <div className="row">
              <div className="col-lg-13">
                <div className="row">
                  <div className="col-lg-14 col-md-6">
                    <div className="single-blog-post">
                      <div className="blog-image">
                        <Link to="#">
                          <img src={require('../../assets/images/Filipino.jpg')} alt="blog" />
                        </Link>
                      </div>

                      <div className="blog-post-content">
                        <span className="date">25 Feb, 2020</span>
                        <h3>
                          <Link to="#">The Authentic Filipino Dinning Experience.</Link>
                        </h3>
                        <p>
                          On the menu of this popular authentic Filipino Kamayan Feast comprises
                          Garlic Rice, Grilled Milkfish,Grilled Tilapia, Grilled Squid, Mussels,
                          Shrimp, BBQ Chicken or Pork,Pork Belly, Vegetables and Fruit Salad. This's
                          traditionally served on large plantain leaves and the feast is eaten
                          entirely by hand.{' '}
                        </p>
                        <Link to="#" className="read-more-btn">
                          Read More <i className="icofont-double-right"></i>
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-14 col-md-6">
                    <div className="single-blog-post">
                      <div className="blog-image">
                        <Link to="#">
                          <img src={require('../../assets/images/JollofRice.jpg')} alt="blog" />
                        </Link>
                      </div>

                      <div className="blog-post-content">
                        <span className="date">27 Feb, 2020</span>
                        <h3>
                          <Link to="#">Authentic West African Jollof Rice Experience</Link>
                        </h3>
                        <p>
                          Authentic Jollof rice is prepared from a tomato base sauce made up of
                          blended tomatoes, onions, sweet bell peppers and chilli peppers usually
                          scotch bonnet peppers, seasoned with Maggi cubes.{' '}
                        </p>
                        <Link to="#" className="read-more-btn">
                          Read More <i className="icofont-double-right"></i>
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-14 col-md-6">
                    <div className="single-blog-post">
                      <div className="blog-image">
                        <Link to="#">
                          <img src={require('../../assets/images/Kelewele.jpeg')} alt="blog" />
                        </Link>
                      </div>

                      <div className="blog-post-content">
                        <span className="date">28 Feb, 2020</span>
                        <h3>
                          <Link to="#">Kelewele - Original Appetizer</Link>
                        </h3>
                        <p>
                          Kelewele is a popular Ghanaian Street Food. It comprises of diced ripe
                          plantain lightly spiced & deep fried, traditionally served with roasted
                          peanuts for chewing contrast. The plantains are peeled and may be cut into
                          chunks or cubes. Ginger, cayenne pepper, and salt are the typical spices
                          used to make kelewele.
                        </p>
                        <Link to="#" className="read-more-btn">
                          Read More <i className="icofont-double-right"></i>
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-14 col-md-6">
                    <div className="single-blog-post">
                      <div className="blog-image">
                        <Link to="#">
                          <img src={require('../../assets/images/Chroma.jpeg')} alt="blog" />
                        </Link>
                      </div>

                      <div className="blog-post-content">
                        <span className="date">28 Feb, 2020</span>
                        <h3>
                          <Link to="#">Kanya's Kachumbari with Choma</Link>
                        </h3>
                        <p>
                          Kachumbari is an East African vegetable dish. It comprises of tomatoes,
                          onions, celery, lemon or lime juice, and chili pepper to make a salsa-like
                          mixture. It is traditionally served with Nyama Choma and Ugali, They are
                          roasted grilled goat meat and maize made into a thick porridge
                          respectively.{' '}
                        </p>
                        <Link to="#" className="read-more-btn">
                          Read More <i className="icofont-double-right"></i>
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-14 col-md-6">
                    <div className="single-blog-post">
                      <div className="blog-image">
                        <Link to="#">
                          <img src={require('../../assets/images/Italian.jpeg')} alt="blog" />
                        </Link>
                      </div>

                      <div className="blog-post-content">
                        <span className="date">25 Feb, 2020</span>
                        <h3>
                          <Link to="#">Italian Food Experience</Link>
                        </h3>
                        <p>
                          Join us Friday, August 23, for a 4 course Authentic Italian Meal and
                          Experience with the Funky Bunch of Marky Mark and the Funky Bunch! In
                          addition to great food and fun, discover New Music from emerging Artists,
                          Olenka and Andre with new music Album releases
                        </p>
                        <Link to="#" className="read-more-btn">
                          Read More <i className="icofont-double-right"></i>
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-14 col-md-6">
                    <div className="single-blog-post">
                      <div className="blog-image">
                        <Link to="#">
                          <img src={require('../../assets/images/Oxtail.jpeg')} alt="blog" />
                        </Link>
                      </div>

                      <div className="blog-post-content">
                        <span className="date">27 Feb, 2020</span>
                        <h3>
                          <Link to="#">Southern African Oxtail Stew</Link>
                        </h3>
                        <p>
                          Originally from Lesotho, Oxtail Stew is full of succulent pieces of oxtail
                          slow-cooked in an aromatic sauce. It is traditionally served on a bed of
                          rice. At its best, it's marinated in red wine and slowly cooked.
                        </p>
                        <Link to="#" className="read-more-btn">
                          Read More <i className="icofont-double-right"></i>
                        </Link>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-12 col-md-12">
                    <div className="pagination-area">
                      <nav aria-label="Page navigation">
                        <ul className="pagination justify-content-center">
                          <li className="page-item">
                            <Link className="page-link" to="#">
                              <i className="icofont-double-left"></i>
                            </Link>
                          </li>
                          <li className="page-item">
                            <Link className="page-link active" to="#">
                              1
                            </Link>
                          </li>
                          <li className="page-item">
                            <Link className="page-link" to="#">
                              2
                            </Link>
                          </li>
                          <li className="page-item">
                            <Link className="page-link" to="#">
                              3
                            </Link>
                          </li>
                          <li className="page-item">
                            <Link className="page-link" to="#">
                              <i className="icofont-double-right"></i>
                            </Link>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-12">
                <div className="sidebar">
                  <div className="widget widget_tag_cloud">
                    <h3 className="widget-title">Tags</h3>

                    <div className="tagcloud">
                      <Link to="#">Error</Link>
                      <Link to="#">Cake Bake</Link>
                      <Link to="#">Dromzone</Link>
                      <Link to="#">File</Link>
                      <Link to="#">Yii</Link>
                      <Link to="#">Yii2</Link>
                      <Link to="#">UUID</Link>
                      <Link to="#">Setup</Link>
                      <Link to="#">Error</Link>
                      <Link to="#">Cake Bake</Link>
                      <Link to="#">Dromzone</Link>
                      <Link to="#">File</Link>
                      <Link to="#">Yii</Link>
                      <Link to="#">Yii2</Link>
                      <Link to="#">UUID</Link>
                      <Link to="#">Setup</Link>
                    </div>
                  </div>

                  <div className="widget widget_archive">
                    <h3 className="widget-title">Archives</h3>

                    <ul>
                      <li>
                        <Link to="#">December 2018</Link>
                      </li>
                      <li>
                        <Link to="#">January 2020</Link>
                      </li>
                      <li>
                        <Link to="#">February 2020</Link>
                      </li>
                      <li>
                        <Link to="#">March 2020</Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  };
}
