// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import 'isomorphic-fetch';

// Components
import Nav from '../Navbar/Nav';
import Footer from '../Home/Footer/Footer';
import GoTop from '../Shared/GoTop';

// Styling
import '../Home/Home.scss';

export default class Contact extends Component {
  // Set initial state
  state = {
    submitting: false,
    submitted: false,
    buttonState: '',
    formFields: {
      name: '',
      email: '',
      phone: '',
      text: '',
    },
  };

  // Validate message helper function
  validateMessage = () => {
    let nameError = '';
    let emailError = '';
    let phoneError = '';
    let textError = '';

    // Render name error message
    if (!this.state.formFields.name) {
      nameError = 'Name is required.';
    }

    // Render email error message
    if (!this.state.formFields.email) {
      emailError = 'Email is required.';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(this.state.formFields.email)) {
      emailError = 'Please enter a valid email.';
    }

    // Render phone error message
    if (!this.state.formFields.phone) {
      phoneError = 'Phone is required.';
    }

    // Render text error message
    if (!this.state.formFields.text) {
      textError = 'Message is required.';
    }

    // Set validation error state
    if (nameError || emailError || phoneError || textError) {
      this.setState({
        nameError,
        emailError,
        phoneError,
        textError,
      });
      return false;
    }

    return true;
  };

  // Submit message helper function
  onSubmit = async (e) => {
    e.preventDefault();
    const isValid = this.validateMessage();

    if (isValid) {
      const url = '/tasttlig-messages';

      const data = this.state.formFields;

      try {
        const response = await axios({ method: 'POST', url, data });

        if (response) {
          let formFields = Object.assign({}, this.state.formFields);
          formFields.name = '';
          formFields.email = '';
          formFields.phone = '';
          formFields.text = '';

          this.setState({
            formFields,
            nameError: '',
            emailError: '',
            phoneError: '',
            textError: '',
            submitted: true,
          });
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  // Name change helper function
  nameChangeHandler = (e) => {
    let formFields = Object.assign({}, this.state.formFields);
    formFields.name = e.target.value;
    this.setState({ formFields });
  };

  // Email change helper function
  emailChangeHandler = (e) => {
    let formFields = Object.assign({}, this.state.formFields);
    formFields.email = e.target.value;
    this.setState({ formFields });
  };

  // Phone change helper function
  phoneChangeHandler = (e) => {
    let formFields = Object.assign({}, this.state.formFields);
    formFields.phone = e.target.value;
    this.setState({ formFields });
  };

  // Text change helper function
  textChangeHandler = (e) => {
    let formFields = Object.assign({}, this.state.formFields);
    formFields.text = e.target.value;
    this.setState({ formFields });
  };

  // On hide success helper function
  onHideSuccess = () => {
    this.setState({ submitted: false });
  };

  // Success message helper function
  successMessage = () => {
    if (this.state.submitted) {
      return (
        <div
          className="alert alert-success alert-dismissible fade show"
          style={{ marginTop: '14px' }}
        >
          <strong>Thank you!</strong> Your message is sent to the owner.
          <button type="button" className="close" onClick={this.onHideSuccess}>
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      );
    }
  };

  // Mount Contact page
  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  // Render contact page
  render = () => {
    const { nameError, emailError, phoneError, textError } = this.state;

    return (
      <div>
        <Nav />

        <div className="page-title-area item-bg1">
          <div className="container">
            <h1>Contact Us</h1>
            <span>Send Us your Message</span>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>Contact</li>
            </ul>
          </div>
        </div>

        <section className="contact-area ptb-120">
          <div className="container">
            <div className="row">
              <div className="col-lg-4">
                <div className="contact-box">
                  <div className="icon">
                    <i className="icofont-phone"></i>
                  </div>

                  <div className="content">
                    <h4>Phone</h4>
                    <p>(437) 882-3663</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4">
                <div className="contact-box">
                  <div className="icon">
                    <i className="icofont-email"></i>
                  </div>

                  <div className="content">
                    <h4>Email</h4>
                    <p>hello@tasttlig.com</p>
                  </div>
                </div>
              </div>

              <div className="col-lg-4">
                <div className="contact-box">
                  <div className="icon">
                    <i className="icofont-world"></i>
                  </div>

                  <div className="content">
                    <h4>Location</h4>
                    <p>585 Dundas St E, 3rd Floor, Toronto, ON M5A 2B7, Canada</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-12">
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2886.435623603926!2d-79.36419128413367!3d43.659909179121016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d4cb413dded43b%3A0x426f86c431460664!2s585%20Dundas%20St%20E%2C%20Toronto%2C%20ON%20M5A%202B7!5e0!3m2!1sen!2sca!4v1588871314526!5m2!1sen!2sca"
                  className="map"
                ></iframe>
              </div>
            </div>

            <div className="row h-100 align-items-center contact-form">
              <div className="col-lg-4 col-md-12">
                <div className="leave-your-message">
                  <h3>Leave Your Message</h3>
                  <p>
                    If you have any questions about the services we provide, simply use the form
                    below. We try and respond to all queries and comments within 24 hours.
                  </p>

                  <div className="stay-connected">
                    <h3>Stay Connected</h3>
                    <ul>
                      <li>
                        <a
                          href="https://www.facebook.com/tasttlig"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="facebook"
                        >
                          {' '}
                          <i className="icofont-facebook"></i>
                          <span>Facebook</span>
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.linkedin.com/company/tasttlig/about"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="linkedin"
                        >
                          {' '}
                          <i className="icofont-linkedin"></i>
                          <span>LinkedIn</span>
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.instagram.com/tasttlig"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="instagram"
                        >
                          {' '}
                          <i className="icofont-instagram"></i>
                          <span>Instagram</span>
                        </a>
                      </li>

                      <li>
                        <a
                          href="https://www.youtube.com/channel/UCMlJcL7dEwAkGBbMvgbjYVQ"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="youtube"
                        >
                          {' '}
                          <i className="icofont-youtube"></i>
                          <span>YouTube</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div className="col-lg-8 col-md-12">
                <form onSubmit={this.onSubmit} id="contactForm" noValidate>
                  <div className="row">
                    <div className="col-lg-19 col-md-6">
                      <div className="form-group">
                        <label htmlFor="name">Name*</label>
                        <input
                          type="text"
                          className="form-control"
                          name="name"
                          id="name"
                          required={true}
                          data-error="Please enter your name"
                          value={this.state.formFields.name}
                          onChange={this.nameChangeHandler}
                        />
                        <div className="help-block with-errors"></div>
                        {nameError ? <div className="error-message">{nameError}</div> : null}
                      </div>
                    </div>

                    <div className="col-lg-19 col-md-6">
                      <div className="form-group">
                        <label htmlFor="email">Email*</label>
                        <input
                          type="email"
                          className="form-control"
                          name="email"
                          id="email"
                          required={true}
                          data-error="Please enter your email"
                          value={this.state.formFields.email}
                          onChange={this.emailChangeHandler}
                        />
                        <div className="help-block with-errors"></div>
                        {emailError ? <div className="error-message">{emailError}</div> : null}
                      </div>
                    </div>

                    <div className="col-lg-19 col-md-12">
                      <div className="form-group">
                        <label htmlFor="number">Phone Number*</label>
                        <input
                          type="text"
                          className="form-control"
                          name="number"
                          id="number"
                          required={true}
                          data-error="Please enter your number"
                          value={this.state.formFields.phone}
                          onChange={this.phoneChangeHandler}
                        />
                        <div className="help-block with-errors"></div>
                        {phoneError ? <div className="error-message">{phoneError}</div> : null}
                      </div>
                    </div>

                    <div className="col-lg-19 col-md-12">
                      <div className="form-group">
                        <label htmlFor="message">Message*</label>
                        <textarea
                          name="message"
                          className="form-control"
                          id="message"
                          cols="30"
                          rows="4"
                          required={true}
                          data-error="Write your message"
                          value={this.state.formFields.text}
                          onChange={this.textChangeHandler}
                        />
                        <div className="help-block with-errors"></div>
                        {textError ? <div className="error-message">{textError}</div> : null}
                      </div>
                    </div>

                    <div className="col-lg-12 col-md-12">
                      <button type="submit" className="btn btn-primary">
                        Send Message
                      </button>
                      <div id="msgSubmit" className="h3 text-center hidden">
                        {this.successMessage()}
                      </div>
                      <div className="clearfix"></div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>

        <Footer />

        <GoTop scrollStepInPx="50" delayInMs="16.66" />
      </div>
    );
  };
}
