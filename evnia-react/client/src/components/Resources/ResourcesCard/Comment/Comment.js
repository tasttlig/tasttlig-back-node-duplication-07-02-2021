// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import CommentCard from './CommentCard/CommentCard';

// Styling
import './Comment.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class Comment extends Component {
  // Set initial state
  state = {
    commentResourcesOpen: false,
    comment: '',
    submitAuthDisabled: false,
    commentItems: [],
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // Toggle comment to resources post button helper function
  toggleComment = (event) => {
    event.preventDefault();

    this.setState((prevState) => {
      return {
        commentResourcesOpen: !prevState.commentResourcesOpen,
      };
    });
  };

  // Handle comment change helper function
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate comment helper function
  validateComment = () => {
    let commentError = '';

    // Render comment error message
    if (!this.state.comment) {
      commentError = 'Comment is required.';
    }

    // Set comment error message state
    if (commentError) {
      this.setState({ commentError });
      return false;
    }

    return true;
  };

  // Submit Comment helper function
  handleSubmitComment = async (event) => {
    event.preventDefault();

    const isValid = this.validateComment();

    if (isValid) {
      const url = '/comments';

      const acc_token = localStorage.getItem('access_token');

      const headers = {
        Authorization: `Bearer ${acc_token}`,
      };

      const data = {
        post_id: this.props.postId.toString(),
        profile_img_url: this.context.state.user.profile_img_url,
        first_name: this.context.state.user.first_name,
        body: this.state.comment,
      };

      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for commenting ${this.props.firstName}'s resources post!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            commentError: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Comment card helper function
  renderCommentCard = (arr) => {
    return arr.map((card, index) => (
      <CommentCard
        key={index}
        id={card.id}
        resourcesPostId={this.props.postId}
        commentPostId={card.post_id}
        profileImage={card.profile_img_url}
        firstName={card.first_name}
        commentBody={card.body}
        createdAt={card.created_at}
      />
    ));
  };

  // Mount All Comments
  componentDidMount = () => {
    const url = '/comments';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          commentItems: [...this.state.commentItems, ...response.data.comments.reverse()],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render comment component
  render = () => {
    const { commentResourcesOpen, commentError, submitAuthDisabled, commentItems } = this.state;

    return (
      <div>
        <form onSubmit={this.toggleComment} noValidate>
          <button type="submit" className="comment-toggle-btn">
            Comment
          </button>
        </form>

        <div>
          {commentResourcesOpen ? (
            <div>
              {this.context.state.user.verified ? (
                <form onSubmit={this.handleSubmitComment} noValidate>
                  <div className="form-group">
                    <div>
                      <textarea
                        name="comment"
                        placeholder="Comment..."
                        value={this.state.comment}
                        onChange={this.handleChange}
                        disabled={submitAuthDisabled}
                        className="comment-note"
                      />
                      <span className="fas fa-pencil-alt comment-note-icon"></span>
                    </div>
                    {commentError ? <div className="error-message">{commentError}</div> : null}
                  </div>

                  <div>
                    <button
                      type="submit"
                      disabled={submitAuthDisabled}
                      className="comment-submit-btn"
                    >
                      Submit
                    </button>
                  </div>
                </form>
              ) : null}

              <div>{this.renderCommentCard(commentItems)}</div>
            </div>
          ) : null}
        </div>
      </div>
    );
  };
}
