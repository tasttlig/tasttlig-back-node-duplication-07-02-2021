// Libraries
import React from 'react';
import LazyLoad from 'react-lazyload';

// Components
import { formatDate } from '../../../../Functions/Functions';

// Styling
import './CommentCard.scss';

const CommentCard = (props) => {
  const { resourcesPostId, commentPostId, profileImage, firstName, commentBody, createdAt } = props;

  // Render comment card
  return (
    <div>
      {resourcesPostId === parseInt(commentPostId) ? (
        <LazyLoad once>
          <div className="pt-3">
            <div className="comment-card-content">
              <div className="row text-left">
                {profileImage ? (
                  <img src={profileImage} alt={firstName} className="comment-card-user-picture" />
                ) : (
                  <span className="fas fa-user-circle fa-3x comment-card-default-picture"></span>
                )}
                <span>
                  <div className="comment-card-user-name">{firstName}</div>
                  <div className="comment-card-user-date">{`${formatDate(createdAt)}`}</div>
                </span>
              </div>
              <div className="pt-3">
                <div className="row comment-card-body">{commentBody}</div>
              </div>
            </div>
          </div>
        </LazyLoad>
      ) : null}
    </div>
  );
};

export default CommentCard;
