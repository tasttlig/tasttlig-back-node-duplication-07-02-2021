// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import S3 from 'react-aws-s3';
import { toast } from 'react-toastify';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import ResourcesCard from './ResourcesCard/ResourcesCard';
import Services from '../Services/Services';

// Styling
import './Resources.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class Resources extends Component {
  // Set initial state
  state = {
    term: '',
    resourcesPostCategory: '',
    resourcesPostMethodOfTransportation: '',
    resourcesPostTitle: '',
    resourcesPostBody: '',
    resourcesPostImage: '',
    submitAuthDisabled: false,
    resourcesPostItems: [],
    filteredData: [],
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // Resources post search submit helper function
  resourcesPostSearchSubmit = (term, arr = this.state.resourcesPostItems) => {
    const lowercasedFilter = term.toLowerCase();
    const searchedData = arr.filter((item) => {
      return Object.keys(item).some((key) =>
        item[key].toString().toLowerCase().includes(lowercasedFilter),
      );
    });

    this.setState({ filteredData: searchedData });
  };

  // Resources post search input change helper function
  handleResourcesPostSearchChange = (event) => {
    const currentTerm = event.target.value;

    this.setState({ term: currentTerm });
    this.resourcesPostSearchSubmit(currentTerm);
  };

  // User input change helper function
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Resources post image upload helper function
  handleResourcesPostImage = async (event) => {
    const config = {
      bucketName: process.env.REACT_APP_AWS_BUCKET_NAME,
      dirName: 'resources-post-images',
      region: process.env.REACT_APP_AWS_REGION,
      accessKeyId: process.env.REACT_APP_AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.REACT_APP_AWS_SECRET_ACCESS_KEY,
    };

    const ReactS3Client = new S3(config);

    await ReactS3Client.uploadFile(event.target.files[0])
      .then((data) => {
        this.setState({ resourcesPostImage: data.location });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Validate user input for Resources Post helper function
  validateResourcesPost = () => {
    let resourcesPostError = '';

    // Render resources post error message
    if (
      !this.state.resourcesPostCategory ||
      (this.state.resourcesPostCategory === 'Delivery' &&
        !this.state.resourcesPostMethodOfTransportation) ||
      !this.state.resourcesPostTitle ||
      !this.state.resourcesPostBody
    ) {
      resourcesPostError = 'Category, title, and description are required.';
    }

    // Set validation error state
    if (resourcesPostError) {
      this.setState({ resourcesPostError });

      return false;
    }

    return true;
  };

  // Submit Resources Post helper function
  handleSubmitResourcesPost = async (event) => {
    event.preventDefault();

    const isValid = this.validateResourcesPost();

    if (isValid) {
      const url = '/posts';

      const acc_token = localStorage.getItem('access_token');

      const headers = {
        Authorization: `Bearer ${acc_token}`,
      };

      const data = {
        category: this.state.resourcesPostCategory,
        method_of_transportation: this.state.resourcesPostMethodOfTransportation,
        title: this.state.resourcesPostTitle,
        body: this.state.resourcesPostBody,
        post_img_url: this.state.resourcesPostImage,
        profile_img_url: this.context.state.user.profile_img_url
          ? this.context.state.user.profile_img_url
          : '',
        first_name: this.context.state.user.first_name,
        last_name: this.context.state.user.last_name,
        email: this.context.state.user.email,
        phone_number: this.context.state.user.phone_number,
        verified: this.context.state.user.verified,
      };

      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for submitting your resources post!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            resourcesPostError: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Resources Post card helper function
  renderResourcesPostCard = (arr) => {
    return arr.map((card, index) => (
      <ResourcesCard
        key={index}
        id={card.id}
        publisher={card.user_id}
        profileImage={card.profile_img_url}
        firstName={card.first_name}
        lastName={card.last_name}
        email={card.email}
        phoneNumber={card.phone_number}
        verified={card.verified}
        resourcesPostCategory={card.category}
        resourcesPostMethodOfTransportation={card.method_of_transportation}
        resourcesPostTitle={card.title}
        resourcesPostBody={card.body}
        resourcesPostImage={card.post_img_url}
        createdAt={card.created_at}
        history={this.props.history}
      />
    ));
  };

  // Mount Resources page
  componentDidMount = () => {
    window.scrollTo(0, 0);

    const url = '/posts';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          resourcesPostItems: [...this.state.resourcesPostItems, ...response.data.posts.reverse()],
          filteredData: [...this.state.filteredData, ...response.data.posts.reverse()],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render Resources page
  render = () => {
    const {
      term,
      resourcesPostCategory,
      resourcesPostMethodOfTransportation,
      resourcesPostTitle,
      resourcesPostBody,
      resourcesPostImage,
      resourcesPostError,
      submitAuthDisabled,
      filteredData,
    } = this.state;

    return (
      <div>
        <Nav />

        <div className="resources">
          <div className="resources-search-section">
            <div className="text-center resources-title">Resources</div>
            <div>
              <input
                type="text"
                name="term"
                placeholder="Search"
                value={term}
                onChange={this.handleResourcesPostSearchChange}
                className="main-search-bar"
              />
              <span className="fa fa-search search-icon"></span>
            </div>
          </div>

          <div className="resources-card-section">
            {this.context.state.user.verified ? (
              <form onSubmit={this.handleSubmitResourcesPost} noValidate>
                <div className="form-group">
                  <select
                    name="resourcesPostCategory"
                    value={resourcesPostCategory}
                    onChange={this.handleChange}
                    disabled={submitAuthDisabled}
                    className="custom-select"
                    required
                  >
                    <option value="">Category</option>
                    <option value="Delivery">Delivery</option>
                    <option value="Commercial Kitchen">Commercial Kitchen</option>
                  </select>
                </div>
                {resourcesPostCategory === 'Delivery' ? (
                  <div className="form-group">
                    <select
                      name="resourcesPostMethodOfTransportation"
                      value={resourcesPostMethodOfTransportation}
                      onChange={this.handleChange}
                      disabled={submitAuthDisabled}
                      className="custom-select"
                    >
                      <option value="">Method of Transportation</option>
                      <option value="Bicycle">Bicycle</option>
                      <option value="E-Bike">E-Bike</option>
                      <option value="Motorized Vehicle">Motorized Vehicle</option>
                    </select>
                  </div>
                ) : null}
                <div>
                  <input
                    type="text"
                    name="resourcesPostTitle"
                    placeholder="Title"
                    value={resourcesPostTitle}
                    onChange={this.handleChange}
                    disabled={submitAuthDisabled}
                    className="resources-title-input"
                    required
                  />
                  <span className="fas fa-pencil-alt resources-title-input-icon"></span>
                </div>
                <div>
                  <textarea
                    name="resourcesPostBody"
                    placeholder="Start a post"
                    value={resourcesPostBody}
                    onChange={this.handleChange}
                    disabled={submitAuthDisabled}
                    className="resources-post-input"
                    required
                  />
                  <span className="fas fa-pencil-alt resources-post-input-icon"></span>
                </div>
                <div className="form-group">
                  {resourcesPostImage ? (
                    <div className="resources-post-image-content">
                      <img
                        src={resourcesPostImage}
                        alt={resourcesPostTitle}
                        className="resources-post-image"
                      />
                    </div>
                  ) : null}
                  <label htmlFor="file-upload-post-image" className="resources-post-file-upload">
                    <span className="fas fa-image fa-2x"></span>
                  </label>
                  <input
                    id="file-upload-post-image"
                    type="file"
                    accept="image/x-png,image/gif,image/jpeg"
                    name="resourcesPostImage"
                    onChange={this.handleResourcesPostImage}
                    disabled={submitAuthDisabled}
                  />
                  {resourcesPostError ? (
                    <div className="error-message">{resourcesPostError}</div>
                  ) : null}
                </div>

                <div>
                  <button
                    type="submit"
                    disabled={submitAuthDisabled}
                    className="resources-post-submit-btn"
                  >
                    Submit
                  </button>
                </div>
              </form>
            ) : null}

            {this.renderResourcesPostCard(filteredData)}
          </div>
        </div>

        <Services />
      </div>
    );
  };
}
