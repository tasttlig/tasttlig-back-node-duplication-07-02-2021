// Libraries
import React from 'react';
// import moment from "moment";

// Components
import ImageSlider from '../ImageSlider/ImageSlider';

const MenuItemDetails = (props) => {
  return (
    <div className="row product-details-owner-information">
      <div className="col-25">
        {props.foodSampleOwnerPicture ? (
          <img
            src={props.foodSampleOwnerPicture}
            alt={`${props.first_name} ${props.last_name}`}
            className="product-details-owner-profile-picture"
          />
        ) : (
          <span className="fas fa-user-circle fa-3x product-details-owner-default-picture" />
        )}
      </div>
      <div className="col-75">
        <div className="product-details-title">{props.title}</div>

        {props.business_name ? (
          <div className="text-left">
            <span className="product-details-owner-by">by </span>
            <span className="product-details-owner-name">{props.business_name}</span>
          </div>
        ) : (
          ''
        )}
      </div>
      <div className="product-details-image">
        {props.image_urls.length > 1 ? (
          <ImageSlider images={props.image_urls} />
        ) : (
          <img src={props.image_urls[0]} alt={props.title} />
        )}
      </div>

      <div className="mb-3 product-text-default">
        <div className="pb-2">
          <div className="product-text-sub-title">Address</div>
          <div>{`${props.address}, ${props.city}, ${props.state} ${props.postal_code}`}</div>
        </div>
        {/*<div className="pb-2">
          <div className="product-text-sub-title">Date</div>
          <div>
            {`${formatDate(props.start_date)} to ${formatDate(props.end_date)}`}
          </div>
        </div>
        <div className="pb-2">
          <div className="product-text-sub-title">Time</div>
          <div>
            {`${formatMilitaryToStandardTime(
              props.start_time
            )} to ${formatMilitaryToStandardTime(props.end_time)}`}
          </div>
        </div>*/}
        {/* <div>{`Frequency: ${props.frequency} until ${moment(
          new Date(props.end_date).toISOString()
        ).format("MMM Do")}`}</div> */}
        {/*<div>{`Quantity Available: ${quantityAvailable}`}</div>*/}
        {props.price && (
          <div className="pb-2">
            <div className="product-text-sub-title">Price</div>
            <div>{`$${props.price}`}</div>
          </div>
        )}
        <div className="passport-details-description">{props.description}</div>
      </div>
    </div>
  );
};

export default MenuItemDetails;
