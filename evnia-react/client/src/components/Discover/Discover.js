// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { usePosition } from '../../hooks';

// Components
import Nav from '../Navbar/Nav';
import ShoppingCartHoverList from '../ShoppingCart/ShoppingCartHoverList';
import SearchBar from '../Navbar/SearchBar';
import MenuItemCard from './MenuItemCard';
import BannerFooter from '../Home/BannerFooter/BannerFooter';
import GoTop from '../Shared/GoTop';

// Styling
import './Discover.scss';

const Discover = (props) => {
  // Set initial state
  const [menuItems, setMenuItems] = useState([]);
  const [availableNationalities, setAvailableNationalities] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);

  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  const [filterRadius, setFilterRadius] = useState(25000000);
  const { latitude, longitude, geoError } = usePosition();

  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Render Discover Cards helper function
  const renderDiscoverCards = (arr) => {
    return arr.map((card, index) => <MenuItemCard key={card.menu_item_id} {...card} />);
  };

  const loadNextPage = async (page) => {
    const url = '/menu_items?';

    return axios({
      method: 'GET',
      url: url,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
        nationalities: selectedNationalities,
        radius: filterRadius,
        latitude,
        longitude,
      },
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);

    const fetchMenuItems = async () => {
      try {
        const { data } = await axios({
          method: 'GET',
          url: '/menu_items',
        });

        setMenuItems(data.details.data);
      } catch (error) {
        return error.response;
      }
    };

    fetchMenuItems();
  }, []);

  const handleLoadMore = (page = currentPage, discover = menuItems) => {
    if (latitude || geoError) {
      setLoading(true);
      loadNextPage(page).then((newPage) => {
        setLoading(false);

        const pagination = newPage.data.details.pagination;

        if (page < pagination.lastPage) {
          setCurrentPage(page + 1);
        }

        setHasNextPage(currentPage < pagination.lastPage);
        setMenuItems(discover.concat(newPage.data.details.data));
      });
    }
  };

  useEffect(() => {
    handleLoadMore(0, []);
  }, [props.location.state.keyword, selectedNationalities, filterRadius, latitude, geoError]);

  useEffect(() => {
    const fetchDistinctNationalities = async () => {
      const { data } = await axios({
        method: 'GET',
        url: '/menu_items/nationalities',
      });

      setAvailableNationalities(data.nationalities);
    };

    fetchDistinctNationalities();
  }, []);

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  const toggleNationality = (event) => {
    const nationality = event.target.value;

    if (selectedNationalities.includes(nationality)) {
      setSelectedNationalities(selectedNationalities.filter((c) => c !== nationality));
    } else {
      setSelectedNationalities([nationality].concat(selectedNationalities));
    }
  };

  const Empty = () => {
    return <strong className="mt-3 ml-3">No discover items found.</strong>;
  };

  const Filters = () => (
    <div className="passport-filters">
      <div className="filter-section-categories">
        <ul>
          {availableNationalities &&
            availableNationalities.map((c) => (
              <li key={c}>
                <input
                  type="checkbox"
                  defaultChecked={selectedNationalities.includes(c)}
                  value={c || ''}
                  onClick={toggleNationality}
                />
                {c}
              </li>
            ))}
        </ul>
      </div>
      {latitude && (
        <div className="filter-section-categories">
          <select
            onChange={(e) => setFilterRadius(parseInt(e.target.value))}
            value={filterRadius}
            className="custom-select"
          >
            <option value="25000000">--Select---</option>
            <option value="5000">5km</option>
            <option value="10000">10km</option>
            <option value="25000">25km</option>
            <option value="50000">50km</option>
          </select>
        </div>
      )}
    </div>
  );

  return (
    <div>
      <Nav />

      <ShoppingCartHoverList />

      <div className="discover">
        <div className="row">
          <div className="col-lg-3 page-title-content">
            <div className="page-title">Discover</div>
          </div>
          <div className="col-lg-9 search-bar-content">
            <SearchBar keyword={props.location.state.keyword} url="/discover" />
          </div>
        </div>

        <div className="row">
          <div className="col-lg-3 filter-content">
            <Filters />
          </div>

          <div className="col-lg-9 discover-content">
            <div className="row menu_items" ref={infiniteRef}>
              {menuItems.length !== 0 ? renderDiscoverCards(menuItems) : <Empty />}
            </div>
          </div>
        </div>
      </div>

      {/* Footer Area */}
      <BannerFooter />

      {/* Back to Top */}
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default Discover;
