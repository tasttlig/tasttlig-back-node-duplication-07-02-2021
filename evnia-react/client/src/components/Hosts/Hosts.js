// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Components
import Nav from '../Navbar/Nav';
import BannerFooter from '../Home/BannerFooter/BannerFooter';

// Styling
import './Hosts.scss';
import hostsFestivals from '../../assets/images/hosts-festivals.png';
import hostsExperiences from '../../assets/images/hosts-experiences.png';

const Hosts = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Mount Hosts page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Hosts page
  return (
    <div>
      <Nav />

      <div className="text-center hosts">
        <h1 className="hosts-title">Tasttlig Hosts Create</h1>

        <div className="row">
          <div className="col-lg-6 hosts-option-content-festivals">
            <Link exact="true" to="/host" className="hosts-option">
              <img src={hostsFestivals} alt="Host Festival" />
              <div className="mt-3">Festivals</div>
            </Link>
          </div>
          <div className="col-lg-6 hosts-option-content-experiences">
            <Link exact="true" to="/apply" className="hosts-option">
              <img src={hostsExperiences} alt="Host Experiences" />
              <div className="mt-3">Experiences</div>
            </Link>
          </div>
        </div>

        {appContext.state.signedInStatus ? (
          <div>
            <Link exact="true" to="/create-experience" className="hosts-create-your-own">
              Create Your Own
            </Link>
          </div>
        ) : (
          <div onClick={authModalContext.openModal('log-in')} className="hosts-create-your-own">
            Create Your Own
          </div>
        )}
      </div>

      <BannerFooter />
    </div>
  );
};

export default Hosts;
