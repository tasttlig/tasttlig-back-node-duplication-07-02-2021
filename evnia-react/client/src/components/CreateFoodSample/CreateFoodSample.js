// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Navbar';
import CreateFoodSampleForm from './CreateFoodSampleForm/CreateFoodSampleForm';

const CreateFoodSample = (props) => {
  console.log('props >>> ', props);

  // Mount Create Food Sample page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Create Food Sample page
  return (
    <div>
      <Nav />
      <CreateFoodSampleForm
        update={props.location.state}
        heading={props.heading}
        editObj={props.editObj}
      />
    </div>
  );
};

export default CreateFoodSample;
