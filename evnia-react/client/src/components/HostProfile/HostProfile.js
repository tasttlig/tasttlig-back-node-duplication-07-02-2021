// Libraries
import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import { useForm, Controller } from 'react-hook-form';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../ContextProvider/AppProvider';
import CreateFoodSampleForm from '../../components/CreateFoodSample/CreateFoodSampleForm/CreateFoodSampleForm';
import Modal from 'react-modal';
import LoadingBar from 'react-top-loading-bar';
import { Link } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';
import FestivalCard from '../Festivals/FestivalCard/FestivalCard.js';
import ExpiredFestivalCard from '../Festivals/FestivalCard/ExpiredFestivalCard.js';

// Styling
import './HostProfile.scss';
import defaultProfilePicture from '../../assets/images/default-profile-picture.png';

const HostProfile = (props) => {
 
  // Set initial state
  const [load, setLoad] = useState(false);
  const [host, setHost] = useState([]);
  const [hostID, setHostID] = useState('1');
  const [navBackground, setNavBackground] = useState('nav-transparent');
  const [festivalItems, setFestivalItems] = useState([]);
  // const [festivalItemsNow, setFestivalItemsNow] = useState([]);
  // const [festivalItemsPast, setFestivalItemsPast] = useState([]);
  // const [festivalItemsFuture, setFestivalItemsFuture] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [dayOfWeek, setDayOfWeek] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectMultipleFestivals, setSelectMultipleFestivals] = useState(false);
  const [hostFestivalList, setHostFestivalList] = useState([]);
  const [festivalList, setFestivalList] = useState([]);
  const [emailVerifyOpened, setEmailVerifyOpened] = useState(false);
  const [width, setWidth] = useState(window.innerWidth);
  const breakpoint = 780;
  const ref = useRef(null);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  const handleLoadMore = (page = currentPage, festivals = festivalItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);
      if (!newPage) {
        return false;
      }
      const pagination = newPage.data.details.pagination;
      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }
      setHasNextPage(currentPage < pagination.lastPage);
      setFestivalItems(festivals.concat(newPage.data.details.data));
    });
  };
  console.log("festival items from the festivals", festivalItems)

  // set up d as current date
  const d = new Date();

  // create a new array with ongoing festivals (started before current time and didn't end)
  const festivalItemsNow = festivalItems.filter((card) => 
     
    new Date(card.festival_start_date) < d && 
    new Date(card.festival_end_date) > d
  );

  // create a new array with incoming festivals (starts later than current time)
  const festivalItemsFuture = festivalItems.filter((card) => 
     
    new Date(card.festival_start_date) > d
  );

  // create a new array with incoming festivals (ended before current time)
  const festivalItemsPast = festivalItems.filter((card) => 
    
    new Date(card.festival_end_date) < d
  );

  // Load next set of festivals helper functions
  const loadNextPage = async (page) => {
    const url = `/festivals/${props.match.params.id}`;
    return axios({
      method: 'GET',
      url,
      params: {
        user_id: props.match.params.id,
        keyword: props.location.state.keyword,
        page: page + 1,
        nationalities: selectedNationalities,
        radius: filterRadius,
        startDate,
        startTime,
        cityLocation,
        dayOfWeek,
      },
    });
  };

  // render future and present festivals
  const renderFestivalCards = (arr) => {
    
    return arr.map((card, index) => (
  //     card.festival_host_admin_id[0] === parseInt(props.match.params.id) && new Date(card.festival_start_date) < d3 ?(
      <FestivalCard
        key={index}
        festivalId={card.festival_id}
        images={card.image_urls}
        title={card.festival_name}
        type={card.festival_type}
        price={card.festival_price}
        city={card.festival_city}
        startDate={card.festival_start_date}
        endDate={card.festival_end_date}
        startTime={card.festival_start_time}
        endTime={card.festival_end_time}
        description={card.festival_description}
        selectMultipleFestivals={selectMultipleFestivals}
        // hostFestivalList={card.festival_restaurant_host_id}
        festivalSponsorList={card.festival_business_sponsor_id}
        hostFestivalList={card.festival_vendor_id}
        guestFestivalList={card.festival_user_guest_id}
        history={props.history}
      />
      // ) : null
    ));
  };

  // render expired festivals
  const renderExpiredFestivalCards = (arr) => {
    
    return arr.map((card, index) => (
  //     card.festival_host_admin_id[0] === parseInt(props.match.params.id) && new Date(card.festival_start_date) < d3 ?(
      <ExpiredFestivalCard
        key={index}
        festivalId={card.festival_id}
        images={card.image_urls}
        title={card.festival_name}
        type={card.festival_type}
        price={card.festival_price}
        city={card.festival_city}
        startDate={card.festival_start_date}
        endDate={card.festival_end_date}
        startTime={card.festival_start_time}
        endTime={card.festival_end_time}
        description={card.festival_description}
        selectMultipleFestivals={selectMultipleFestivals}
        // hostFestivalList={card.festival_restaurant_host_id}
        festivalSponsorList={card.festival_business_sponsor_id}
        hostFestivalList={card.festival_vendor_id}
        guestFestivalList={card.festival_user_guest_id}
        history={props.history}
      />
      // ) : null
    ));
  };


  // Render empty festival page
  const FestivalEmpty = () => <strong className="no-festivals-found">No festivals found.</strong>;

  const renderFestivalItems = function () {
    return festivalItems.map((item) => {
      return item.festival_name;
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 200,
  });


  const fetchAllFestivals = async () => {
    const url = `/festival/allFestival`;
    const response = await axios({ method: 'GET', url });
    //setHost(response.data.owner_user);
    //setHostItems(response.data.food_samples);
  };

  const fetchUserDetails = async () => {
    const response = await axios({
      method: 'GET',
      url: `/food-sample/owner/${props.match.params.id}`,
    });
    setHost(response.data.owner_user);
    return response;
  };

  // Mount HostProfile page
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchAllFestivals();
    fetchUserDetails();
  }, []);


  // Host Banner Component
  const Banner = () => (
    <div>
      <div className="host-profile-banner">
        <div className="row host-profile-banner-simple">
          <div className="col-md-5 banner-content">
            <div className="mb-4">
              {host.profile_image_link ? (
                <img
                  src={host.profile_image_link}
                  alt={`${host.first_name} ${host.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'host-profile-picture' : 'loading-image'}
                />
              ) : (
                <img
                  src={defaultProfilePicture}
                  alt={`${host.first_name} ${host.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'host-default-picture' : 'loading-image'}
                />
              )}
            </div>
            <div className="host-profile-name">
              {`${host.first_name} ${host.last_name}`}
            </div>
            <div className="host-profile-promo">
              {host.profile_tag_line && (
                <div className="pro mb-4 host-profile-tag-line">
                  {host.profile_tag_line}
                </div>
              )}
              <address>
                {host.business_name && (
                  <div>
                    <strong>{host.business_name}</strong>
                    <br />
                  </div>
                )}
                {host.business_address_1 && host.business_address_1}
                {host.business_address_2 && (
                  <span>, {host.business_address_2}</span>
                )}
                <br />
                {host.city && `${host.city},`}{' '}
                {host.state && host.state}{' '}
                {host.zip_postal_code && host.zip_postal_code}
                <br />
                <div title="Email">E: {host.email}</div>
                <div title="Phone">P: {host.phone_number}</div>
              </address>
            </div>
            {/* {appContext.state.user.id === host.tasttlig_user_id && (
              <div>
                <Link to="/account" className="edit-profile-link">
                  Edit Profile
                </Link>
              </div>
            )} */}
          </div>
          <div className="col-md-7 banner-content">
            <div className="banner-images">
              <div className="banner-images-wrapper">
                <div className="banner-color-block-container">
                  <div className="banner-color-block"></div>
                </div>
                {host.banner_image_link ? (
                  <img
                    src={host.banner_image_link}
                    alt="Banner"
                    onLoad={() => setLoad(true)}
                    className={load ? 'main-banner-image' : 'loading-image'}
                  />
                ) : (
                  <div className="main-banner-image"></div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="fade-rule" />
    </div>
  );

  // Render Host page
  return (
    <div>
      <Nav />

      {/* Host section */}
      <div className="host">
        <Banner />

        {/* Host Items */}
          <h2> Ongoing Festivals </h2>
        <div className="festivals__explore-cards festivals-page__cards" ref={infiniteRef}>
                  {festivalItemsNow.length !== 0 ? (
                    renderFestivalCards(festivalItemsNow)
                  ) : (
                    <FestivalEmpty />
                  )}
                </div>


        <h2> Incoming Festivals </h2>
        <div className="festivals__explore-cards festivals-page__cards" ref={infiniteRef}>
                  {festivalItemsFuture.length !== 0 ? (
                    renderFestivalCards(festivalItemsFuture)
                  ) : (
                    <FestivalEmpty />
                  )}
                </div>

        <h2> Past Festivals </h2>
        <div className="festivals__explore-cards festivals-page__cards" ref={infiniteRef}>
                  {festivalItems.length !== 0 ? (
                    renderExpiredFestivalCards(festivalItemsPast)
                  ) : (
                    <FestivalEmpty />
                  )}
                </div>

      </div>
    </div>
  );
};

export default HostProfile;
