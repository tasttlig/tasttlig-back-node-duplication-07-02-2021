// import React, { useEffect, useContext, useState } from "react";
// import { Link } from "react-router-dom";
// import { useForm } from "react-hook-form";
// import { AuthModalContext } from "../../ContextProvider/AuthModalProvider";
// import { AppContext } from "../../ContextProvider/AppProvider";
// import axios from "axios";
// import useInfiniteScroll from "react-infinite-scroll-hook";
// import { Radio } from 'semantic-ui-react';

// //Components
// import VendorFestivalCard from "./VendorFestivalCard";

// //styling
// import pricingBackgroung from "../../assets/images/pricingBackground.jpg";
// import { toast } from "react-toastify";
// import "./VendorPricing.scss";

// const VendorPricingData = (props) => {
//     const userData = {

//       vendorPrice: "",
//       vendorPackage: "",

//     };

//     //Set intial state
//     const [festivalItems, setFestivalItems] = useState([]);
//     const [loading, setLoading] = useState(false);
//     const [hasNextPage, setHasNextPage] = useState(true);
//     const [currentPage, setCurrentPage] = useState(0);
//     const [startDate, setFilterStartDate] = useState("");
//     const [startTime, setFilterStartTime] = useState("");
//     const [cityLocation, setCityLocation] = useState("");
//     const [cashOrKind, setCashOrKind] = useState("");
//     const [vendorSubscriptionDetails, setVendorSubscriptionDetails] = useState([]);

//     // Render festival cards helper function
//     const renderFestivalCards = (arr) => {

//         return arr.map((card, index) => (
//         <VendorFestivalCard
//             key={index}
//             festivalId={card.festival_id}
//             images={card.image_urls}
//             title={card.festival_name}
//             type={card.festival_type}
//             price={card.festival_price}
//             city={card.festival_city}
//             startDate={card.festival_start_date}
//             endDate={card.festival_end_date}
//             startTime={card.festival_start_time}
//             endTime={card.festival_end_time}
//             description={card.festival_description}
//             //selectMultipleFestivals={selectMultipleFestivals}
//             hostFestivalList={card.festival_restaurant_host_id}
//             festivalSponsorList={card.festival_business_sponsor_id}
//             history={props.history}
//         />
//         ));
//     };

//     // Load next set of festivals helper functions
//     const loadNextPage = async (page) => {
//         const url = "/festival/all?";

//         return axios({
//         method: "GET",
//         url,
//         params: {
//             //keyword: props.location.state.keyword,
//             page: page + 1,
//             //nationalities: selectedNationalities,
//             //radius: filterRadius,
//             startDate,
//             startTime,
//             cityLocation,
//         },
//         });
//     };

//     const loadDetails = async () => {
//       try {
//         const response = await axios({
//           method: "GET",
//           url: "/vendor-subscription-details",
//           params: {},
//         });

//         // console.log("response>>>>>>", response);

//         return response.data;
//       } catch (error) {
//         return error.response;
//       }
//     }

//     // Load next set of festivals helper functions
//     const vendorSubscriptionPrices =  () => {
//       const url = "/vendor-subscription-details";
//       loadDetails().then((respData) => {
//         setVendorSubscriptionDetails(respData)
//       })
//     };

//     const handleLoadMore = (page = currentPage, festivals = festivalItems) => {
//         setLoading(true);
//         loadNextPage(page).then((newPage) => {
//           setLoading(false);

//           if (!newPage) {
//             return false;
//           }

//           const pagination = newPage.data.details.pagination;

//           if (page < pagination.lastPage) {
//             setCurrentPage(page + 1);
//           }

//           setHasNextPage(currentPage < pagination.lastPage);
//           setFestivalItems(festivals.concat(newPage.data.details.data));
//         });
//       };

//       const infiniteRef = useInfiniteScroll({
//         loading,
//         hasNextPage,
//         onLoadMore: handleLoadMore,
//         scrollContainer: "window",
//         threshold: 50,
//     });

//       // Render empty festival page
//         const FestivalEmpty = () => (
//             <strong className="no-festivals-found">No festivals found.</strong>
//         );

//         const handleCheckRadio = (e) => {
//           setCashOrKind(e);
//         };

//         useEffect(() => {
//           window.scrollTo(0, 0);

//           vendorSubscriptionPrices();
//         }, []);

//         //vendorSubscriptionPrices()

//         console.log(vendorSubscriptionDetails)
//     return(
//         <div className="row">
//                 <div className="col-lg-10 col-xl-4 px-20 pricingBackground" />
//                 <div className = "col-lg-10 col-xl-8 px-20">
//                     <div className = "title">
//                     Vending & Plan
//                     </div>

//                     <div
//                       className="row"
//                       onChange={(e) => handleCheckRadio(e.target.value)}
//                     >
//                       <div className="radio-style">
//                         <input type="radio" value="Yes" name="registration" /> Vending Price
//                       </div>
//                       <div className="radio-style">
//                         <input type="radio" value="No" name="registration" /> Vending Subscription Plan
//                       </div>
//                       </div>

//                   <div className="row">
//                     {cashOrKind === "Yes" ?
//                     <div className="row">
//                     <div>
//                       <div className="landing-page-cards" ref={infiniteRef}>
//                           {festivalItems.length !== 0 ? (
//                               renderFestivalCards(festivalItems)) : (
//                               <FestivalEmpty />
//                           )}
//                         </div>
//                       </div>
//                     </div>
//                     : <div >Select the subscription plan that fits your business
//                           {/* <table className="table table-striped passport-table">
//                             <thead>
//                               <tr>
//                                 <th scope="col">Subscription Name</th>
//                               </tr>
//                             </thead>
//                             <tbody>
//                               {passportItems.length !== 0 ? (
//                                 renderFoodTypeRows(passportItems[0].food_preferences)
//                               ) : (
//                                 <tr>
//                                   <td>No packages available</td>
//                                 </tr>
//                               )}
//                             </tbody>
//                           </table>
//                           <table className="table table-striped passport-table">
//                             <thead>
//                               <tr>
//                                 <th scope="col">ubscription Price</th>
//                               </tr>
//                             </thead>
//                             <tbody>
//                               {passportItems.length !== 0 ? (
//                                 renderAllergiesRows(passportItems[0].food_allergies)
//                               ) : (
//                                 <tr>
//                                   <td>No pricing available</td>
//                                 </tr>
//                               )}
//                             </tbody>
//                           </table> */}

//                     </div> }

//                   </div>

//               </div>

//         </div>
//     )

// }

// export default VendorPricingData;
