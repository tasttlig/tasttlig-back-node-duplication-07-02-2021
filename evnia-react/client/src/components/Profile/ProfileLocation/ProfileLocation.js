// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import './ProfileLocation.scss';

export default class ProfileLocation extends Component {
  // Set initial state
  state = {
    updateBusinessStreetAddress: '',
    updateBusinessCity: '',
    updateBusinessProvinceTerritory: '',
    updateBusinessPostalCode: '',
    updateFacebook: '',
    updateTwitter: '',
    updateInstagram: '',
    updateYouTube: '',
    updateLinkedIn: '',
    updateWebsite: '',
    updateDisabled: false,
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // User basic info change helper function
  handleUserBasicInfoChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate user input for update profile helper function
  validateProfileUpdate = () => {
    let updateBusinessStreetAddressError = '';
    let updateBusinessCityError = '';
    let updateBusinessProvinceTerritoryError = '';
    let updateBusinessPostalCodeError = '';
    let updateFacebookError = '';
    let updateTwitterError = '';
    let updateInstagramError = '';
    let updateYouTubeError = '';
    let updateLinkedInError = '';
    let updateWebsiteError = '';

    // Render business address error message
    if (
      !this.state.updateBusinessStreetAddress &&
      (this.state.updateBusinessCity ||
        this.state.updateBusinessProvinceTerritory ||
        this.state.updateBusinessPostalCode)
    ) {
      updateBusinessStreetAddressError = 'Business address is required.';
    }

    // Render business city error message
    if (
      !this.state.updateBusinessCity &&
      (this.state.updateBusinessStreetAddress ||
        this.state.updateBusinessProvinceTerritory ||
        this.state.updateBusinessPostalCode)
    ) {
      updateBusinessCityError = 'Business city is required.';
    }

    // Render business Province or Territory error message
    if (
      !this.state.updateBusinessProvinceTerritory &&
      (this.state.updateBusinessStreetAddress ||
        this.state.updateBusinessCity ||
        this.state.updateBusinessPostalCode)
    ) {
      updateBusinessProvinceTerritoryError = 'Business Province or Territory is required.';
    }

    // Render business postal code error message
    if (
      !this.state.updateBusinessPostalCode &&
      (this.state.updateBusinessStreetAddress ||
        this.state.updateBusinessCity ||
        this.state.updateBusinessProvinceTerritory)
    ) {
      updateBusinessPostalCodeError = 'Business postal code is required.';
    }

    // Render Facebook error message
    if (
      this.state.updateFacebook &&
      !/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(
        this.state.updateFacebook,
      )
    ) {
      updateFacebookError = 'Please enter a valid URL.';
    }

    // Render Twitter error message
    if (
      this.state.updateTwitter &&
      !/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(
        this.state.updateTwitter,
      )
    ) {
      updateTwitterError = 'Please enter a valid URL.';
    }

    // Render Instagram error message
    if (
      this.state.updateInstagram &&
      !/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(
        this.state.updateInstagram,
      )
    ) {
      updateInstagramError = 'Please enter a valid URL.';
    }

    // Render YouTube error message
    if (
      this.state.updateYouTube &&
      !/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(
        this.state.updateYouTube,
      )
    ) {
      updateYouTubeError = 'Please enter a valid URL.';
    }

    // Render LinkedIn error message
    if (
      this.state.updateLinkedIn &&
      !/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(
        this.state.updateLinkedIn,
      )
    ) {
      updateLinkedInError = 'Please enter a valid URL.';
    }

    // Render website error message
    if (
      this.state.updateWebsite &&
      !/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi.test(
        this.state.updateWebsite,
      )
    ) {
      updateWebsiteError = 'Please enter a valid URL.';
    }

    // Set validation error state
    if (
      updateBusinessStreetAddressError ||
      updateBusinessCityError ||
      updateBusinessProvinceTerritoryError ||
      updateBusinessPostalCodeError ||
      updateFacebookError ||
      updateTwitterError ||
      updateInstagramError ||
      updateYouTubeError ||
      updateLinkedInError ||
      updateWebsiteError
    ) {
      this.setState({
        updateBusinessStreetAddressError,
        updateBusinessCityError,
        updateBusinessProvinceTerritoryError,
        updateBusinessPostalCodeError,
        updateFacebookError,
        updateTwitterError,
        updateInstagramError,
        updateYouTubeError,
        updateLinkedInError,
        updateWebsiteError,
      });

      return false;
    }

    return true;
  };

  // Submit update profile form helper function
  handleSubmitProfileUpdate = (event) => {
    event.preventDefault();
    const isValid = this.validateProfileUpdate();

    if (isValid) {
      const url = `/user/location/${this.context.state.user.id}`;

      const data = {
        business_street_address: this.state.updateBusinessStreetAddress,
        business_city: this.state.updateBusinessCity,
        business_province_territory: this.state.updateBusinessProvinceTerritory,
        business_postal_code: this.state.updateBusinessPostalCode,
        facebook: this.state.updateFacebook,
        twitter: this.state.updateTwitter,
        instagram: this.state.updateInstagram,
        youtube: this.state.updateYouTube,
        linkedin: this.state.updateLinkedIn,
        website: this.state.updateWebsite,
      };

      try {
        const response = axios({ method: 'PUT', url, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          this.setState({
            updateBusinessStreetAddressError: '',
            updateBusinessCityError: '',
            updateBusinessProvinceTerritoryError: '',
            updateBusinessPostalCodeError: '',
            updateFacebookError: '',
            updateTwitterError: '',
            updateInstagramError: '',
            updateYouTubeError: '',
            updateLinkedInError: '',
            updateWebsiteError: '',
            updateDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  // Render Profile Location modal
  render = () => {
    const {
      updateBusinessStreetAddress,
      updateBusinessStreetAddressError,
      updateBusinessCity,
      updateBusinessCityError,
      updateBusinessProvinceTerritory,
      updateBusinessProvinceTerritoryError,
      updateBusinessPostalCode,
      updateBusinessPostalCodeError,
      updateFacebook,
      updateFacebookError,
      updateTwitter,
      updateTwitterError,
      updateInstagram,
      updateInstagramError,
      updateYouTube,
      updateYouTubeError,
      updateLinkedIn,
      updateLinkedInError,
      updateWebsite,
      updateWebsiteError,
      updateDisabled,
    } = this.state;

    return (
      <div>
        <div className="your-location">Your Location</div>

        {this.context.state.user.verified ? (
          <div>
            <form onSubmit={this.handleSubmitProfileUpdate} noValidate>
              <div className="form-group">
                <input
                  type="text"
                  name="updateBusinessStreetAddress"
                  placeholder="Business Address"
                  value={updateBusinessStreetAddress}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="business-street-address"
                />
                <span className="fas fa-map-marker-alt input-icon"></span>
                {updateBusinessStreetAddressError ? (
                  <div className="error-message">{updateBusinessStreetAddressError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateBusinessCity"
                  placeholder="Business City"
                  value={updateBusinessCity}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="business-city"
                />
                <span className="fas fa-map-marker-alt input-icon"></span>
                {updateBusinessCityError ? (
                  <div className="error-message">{updateBusinessCityError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateBusinessProvinceTerritory"
                  placeholder="Business Province or Territory"
                  value={updateBusinessProvinceTerritory}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="business-province-territory"
                />
                <span className="fas fa-map-marker-alt input-icon"></span>
                {updateBusinessProvinceTerritoryError ? (
                  <div className="error-message">{updateBusinessProvinceTerritoryError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateBusinessPostalCode"
                  placeholder="Business Postal Code"
                  value={updateBusinessPostalCode}
                  onChange={this.handleUserBasicInfoChange}
                  maxLength="7"
                  disabled={updateDisabled}
                  className="business-postal-code"
                />
                <span className="fas fa-map-marker-alt input-icon"></span>
                {updateBusinessPostalCodeError ? (
                  <div className="error-message">{updateBusinessPostalCodeError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateFacebook"
                  placeholder="Facebook"
                  value={updateFacebook}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="facebook"
                />
                <span className="fab fa-facebook input-icon"></span>
                {updateFacebookError ? (
                  <div className="error-message">{updateFacebookError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateTwitter"
                  placeholder="Twitter"
                  value={updateTwitter}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="twitter"
                />
                <span className="fab fa-twitter input-icon"></span>
                {updateTwitterError ? (
                  <div className="error-message">{updateTwitterError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateInstagram"
                  placeholder="Instagram"
                  value={updateInstagram}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="instagram"
                />
                <span className="fab fa-instagram input-icon"></span>
                {updateInstagramError ? (
                  <div className="error-message">{updateInstagramError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateYouTube"
                  placeholder="YouTube"
                  value={updateYouTube}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="youtube"
                />
                <span className="fab fa-youtube input-icon"></span>
                {updateYouTubeError ? (
                  <div className="error-message">{updateYouTubeError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateLinkedIn"
                  placeholder="LinkedIn"
                  value={updateLinkedIn}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="linkedin"
                />
                <span className="fab fa-linkedin input-icon"></span>
                {updateLinkedInError ? (
                  <div className="error-message">{updateLinkedInError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateWebsite"
                  placeholder="Website"
                  value={updateWebsite}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="website"
                />
                <span className="fas fa-globe input-icon"></span>
                {updateWebsiteError ? (
                  <div className="error-message">{updateWebsiteError}</div>
                ) : null}
              </div>

              <div>
                <button type="submit" disabled={updateDisabled} className="modal-btn">
                  Update Profile
                </button>
              </div>
            </form>
          </div>
        ) : (
          <div>Please verify your email.</div>
        )}
      </div>
    );
  };
}
