import React, { useState, useEffect, useContext } from 'react';

import './PassportProfile.scss';

const PassportProfile = (props) => {
  const { appContext } = props;

  console.log('INFORMATION', props.appContext);

  return (
    <div className="container">
      <div className="passport-profile__content">
        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Type</h5>
            <div className="passport-profile__user-data-values">
              {props.hasBusinessPassport && props.hasBusinessPassport ? 'B' : 'P'}
            </div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Issuing Country</h5>
            <div className="passport-profile__user-data-values">Canada</div>
          </div>
        </div>

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Surname</h5>
            <div className="passport-profile__user-data-values">{props.appContext.last_name}</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Business Name</h5>
            <div className="passport-profile__user-data-values">
              {props.appContext.business_name}
            </div>
          </div>
        </div>

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Given Name</h5>
            <div className="passport-profile__user-data-values">{props.appContext.first_name}</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Business Address</h5>
            <div className="passport-profile__user-data-values">{`${props.appContext.business_street_number}, ${props.appContext.business_street_name}`}</div>
          </div>
        </div>

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Nationality</h5>
            <div className="passport-profile__user-data-values">{props.appContext.country}</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Place of Registration</h5>
            <div className="passport-profile__user-data-values">
              {props.appContext.business_registered_location}
            </div>
          </div>
        </div>

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Date of Birth</h5>
            <div className="passport-profile__user-data-values">{props.birthdate !== null ? (props.birthdate) : null}</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Business Type</h5>
            <div className="passport-profile__user-data-values">
              {props.appContext.business_type}
            </div>
          </div>
        </div>

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Sex</h5>
            <div className="passport-profile__user-data-values">{props.appContext.sex}</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Place of Birth</h5>
            <div className="passport-profile__user-data-values">{props.appContext.country}</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Business Document</h5>
            <div className="passport-profile__user-data-values">
              {props.appContext.food_business_type}
            </div>
          </div>
        </div>
        <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Business CRA Number</h5>
            <div className="passport-profile__user-data-values">
              {props.appContext.CRA_business_number}
            </div>
        </div>
        

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Date of Issue</h5>
            <div className="passport-profile__user-data-values">30 MAR 21</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Business Issue Date</h5>
            <div className="passport-profile__user-data-values">30 MAR 21</div>
          </div>
        </div>

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Date of Expiry</h5>
            <div className="passport-profile__user-data-values">30 MAR 25</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Business Expiry Date</h5>
            <div className="passport-profile__user-data-values">30 MAR 22</div>
          </div>
        </div>

        <div className="row passport-info-row">
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Issuing Authority</h5>
            <div className="passport-profile__user-data-values">TASTTLIG CORPORATION</div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle"></h5>
            <div className="passport-profile__user-data-values"></div>
          </div>
          <div className="col passport-profile__info-block">
            <h5 className="passport-profile__subtitle">Place of Issue</h5>
            <div className="passport-profile__user-data-values">TASTTLIG HEAD OFFICE</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PassportProfile;
