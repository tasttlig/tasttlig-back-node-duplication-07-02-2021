// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../../ContextProvider/AppProvider';
import Swal from 'sweetalert2';

// Components
import MyActivity from '../../Passport/Passport';
import MyStamps from '../../Dashboard/FoodSamplesDashboard/FoodSamplesDashboard';
import EditPersonalPassport from './EditPersonalPassport';

// Styling
import './PersonalPassport.scss';

const PersonalPassport = (props) => {
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const user = appContext.state.user;

  return (
    <div className="container">
      <div className="row">
        <div className="tab-row">
          <div className="tab-switcher personal-switcher">
            <button
              onClick={() => toggleTab(1)}
              className={toggleState === 1 ? 'tab active-tab' : 'tab'}
            >
              My Activity
            </button>
            {/* <button
              onClick={() => toggleTab(2)}
              className={toggleState === 2 ? "tab active-tab" : "tab"}
            >
              My Stamps
            </button> */}
            <button
              onClick={() => toggleTab(3)}
              className={toggleState === 3 ? 'tab active-tab' : 'tab'}
            >
              Edit Passport
            </button>
          </div>
        </div>
      </div>
      {toggleState && toggleState === 1 ? (
        <section className="passport-sections personal-passport">
          <MyActivity />
        </section>
      ) : toggleState === 2 ? (
        <section className="passport-sections personal-passport">
          <MyStamps />
        </section>
      ) : toggleState === 3 ? (
        <EditPersonalPassport />
      ) : null}
    </div>
  );
};

export default PersonalPassport;
