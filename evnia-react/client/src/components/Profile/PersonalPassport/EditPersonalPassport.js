// Libraries
import React, {useContext, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {AppContext} from '../../../ContextProvider/AppProvider';
import {formatPhoneNumber} from '../../Functions/Functions';
import axios from 'axios';
import Swal from 'sweetalert2';
import DatePicker from 'react-datepicker';

import './PersonalPassport.scss';
import {fileUploadAsync} from '../../../functions/FileUpload';

const EditPersonalPassport = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const user = appContext.state.user;

  console.log('user Info', user);

  const [state, setState] = useState({
    firstName: user.first_name,
    lastName: user.last_name,
    email: user.email,
    phoneNumber: user.phone_number,
    birthdate: user.date_of_birth,
    occupation: user.occupation,
    streetNumber: user.apartment_no,
    streetName: user.street_name,
    unitNumber: user.apartment_no,
    postalCode: user.postal_code,
    city: user.city,
    region: user.state,
    country: user.country,
    profileImage: user.profile_image

  });

  const {register, handleSubmit, setValue, errors, control} = useForm({

    defaultValues: {
      firstName: state.firstName,
      lastName: state.lastName,
      email: state.email,
      phoneNumber: state.phoneNumber,
      birthdate: state.birthdate,
      occupation: state.occupation,
      streetNumber: state.streetNumber,
      streetName: state.streetName,
      unitNumber: state.unitNumber,
      postalCode: state.postalCode,
      city: state.city,
      region: state.region,
      country: state.country,
      profileImage: state.profileImage
    },
    
  });

  var [dateOfBirth, setDateOfBirth] = useState(user.birthdate);
  var [profileImage, setProfileImage] = useState(user.profile_image);
  // var src = URL.createObjectURL(profileImage);
  console.log("present image", profileImage);


  const normalizeBirthdate = (val, prevVal) => {
    if (isNaN(parseInt(val[val.length - 1], 10))) {
      return val.slice(0, -1);
    }
    if (prevVal && prevVal.length >= val.length) {
      return val;
    }
    if (val.length === 2 || val.length === 5) {
      val += '/';
    }
    if (val.length >= 10) {
      return val.slice(0, 10);
    }
    return val;
  };

  const handleChange = async (e) => {
    console.log("upload", e.target.files[0])
    var result;
    try {
      result = await fileUploadAsync(e.target.files[0], 'dir');
    } catch (error) {
      Swal.fire({
        title: 'Error Saving Image Changes',
        text: 'Something went wrong, please try again.',
        icon: 'error',
        confirmButtonColor: '#88171a ',
      });
      console.log(error);
    }

    console.log("updated result", result)
    setProfileImage(!result ? '' : result.url);

  };

  const onSubmit = async (data) => {
    try {
      const url = `/user/update-profile/${user.id}`;

      data.birthdate = dateOfBirth;
      data.profileImage = profileImage; //if result is not null, then give result url;

      const response = await axios({
        method: 'PUT',
        url,
        data,
      });

      console.log('submit data', data);

      if (response && response.data && response.data.success) {
        Swal.fire({
          title: 'Changes Saved',
          text: 'Your personal passport has been updated with your new information.',
          icon: 'success',
          confirmButtonColor: '#88171a ',
        });
        console.log('submit data', response.data);
        //window.location.reload();
      }
    } catch (error) {
      Swal.fire({
        title: 'Error Saving Changes',
        text: 'Something went wrong, please try again.',
        icon: 'error',
        confirmButtonColor: '#88171a ',
      });
    }
  };

  return (
    <section className="passport-sections personal-passport">
      {errors.address ||
      errors.birthdate ||
      errors.city ||
      errors.country ||
      errors.email ||
      errors.firstName ||
      errors.lastName ||
      errors.phoneNumber ||
      errors.postalCode ||
      errors.region ? (
        <span className="invalid-submit-info">Invalid Input</span>
      ) : null}

      <form className="container" onSubmit={handleSubmit(onSubmit)}>
        <div>

          <label htmlFor="upload-button">
            {profileImage ? (
              <img src={profileImage} alt="dummy" width="400" height="300"/>
            ) : (
              <>

                   <span className="fa-stack fa-2x mt-3 mb-2">
                   {/* <img src={profileImage.preview} alt="dummy" width="400" height="300"> Upload new profile picture </img>  */}

                     <i className="fas fa-circle fa-stack-2x"/>
                    <i className="fas fa-store fa-stack-1x fa-inverse"/>
                  </span>
                <h6 className="text-center">Upload new profile picture</h6>
              </>
            )}
          </label>
          <input
            type="file"
            id="upload-button"
            style={{display: 'none'}}
            onChange={handleChange}
          />
          <br/>

        </div>
        {/* <div className="row">

            <label htmlFor="upload-button">
              {profileImage.preview? (
                <img src={profileImage.preview} alt="dummy" width="100" height="100" />
              ) : (
                <>
                  <span className="fa-stack fa-2x mt-3 mb-2">
                    <i className="fas fa-circle fa-stack-2x" />
                    <i className="fas fa-store fa-stack-1x fa-inverse" />
                  </span>
                  <h6 className="text-center">Upload new profile picture</h6>
                </>
              )}
            </label>
            <input
              type="file"
              id="upload-button"
              style={{ display: 'none' }}
              onChange={handleChange}
            />
            <br />
          </div> */}
        <div className="form-row">
          <div className="col">
            <input
              name="firstName"
              type="text"
              className={errors.firstName ? 'form-control invalid-input' : 'form-control'}
              placeholder="First Name"
              ref={register({required: true})}
            />
          </div>
          <div className="col">
            <input
              name="lastName"
              type="text"
              className={errors.lastName ? 'form-control invalid-input' : 'form-control'}
              placeholder="Last Name"
              ref={register({required: true})}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="col">
            <input
              name="email"
              type="email"
              className={errors.email ? 'form-control invalid-input' : 'form-control'}
              placeholder="Email Address"
              ref={register({required: true})}
            />
          </div>
          <div className="col">
            <input
              name="phoneNumber"
              type="tel"
              className={errors.phoneNumber ? 'form-control invalid-input' : 'form-control'}
              placeholder="Phone Number"
              maxLength="14"
              ref={register({required: true, minLength: 14, maxLength: 14})}
              onChange={(e) => setValue('phoneNumber', formatPhoneNumber(e.target.value))}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="col">
            {/* <input
              name="birthdate"
              type="text"
              inputMode="numeric"
              autoComplete="birthdate"
              className={errors.birthdate ? 'form-control invalid-input' : 'form-control'}
              placeholder="Date of Birth (mm/dd/yyyy)"
              onChange={(e) => setValue('birthdate', normalizeBirthdate(e.target.value))}
              ref={register({ required: true })}
            /> */}

            <Controller
              control={control}
              name="test"
              render={({onChange, value, ref}) => (
                <DatePicker
                  className={errors.birthdate ? 'form-control invalid-input' : 'form-control'}
                  placeholderText={user.date_of_birth ? user.date_of_birth.substring(0, 10) : "Enter Date of Birth"}
                  maxDate={new Date()}
                  name="birthdate"
                  // onChange={(e) => setValue('birthdate', normalizeBirthdate(e.target.value))}
                  onChange={onChange}
                  selected={setDateOfBirth(value)}
                  inputRef={ref}
                  rules={{required: true}}
                  showMonthDropdown
                  showYearDropdown
                  scrollableYearDropdown
                  yearDropdownItemNumber={50}
                />
              )}
            />

          </div>
          <div className="col">
            <input
              name="occupation"
              type="text"
              className="form-control"
              placeholder="Occupation"
              ref={register}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="col">
            <input
              name="streetNumber"
              type="text"
              inputMode="numeric"
              className={errors.streetNumber ? 'form-control invalid-input' : 'form-control'}
              placeholder="Street Number"
              // ref={register({required: true})}
              ref={register}
            />
          </div>
          <div className="col">
            <input
              name="streetName"
              type="text"
              className={errors.streetName ? 'form-control invalid-input' : 'form-control'}
              placeholder="Street Name"
              // ref={register({required: true})}
              ref={register}
            />
          </div>
          <div className="col">
            <input
              name="unitNumber"
              type="text"
              className="form-control"
              placeholder="Unit/Apartment Number"
              ref={register}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="col">
            <input
              name="city"
              type="text"
              className={errors.city ? 'form-control invalid-input' : 'form-control'}
              placeholder="City"
              // ref={register({required: true})}
              ref={register}
            />
          </div>
          <div className="col">
            <input
              name="region"
              type="text"
              className={errors.region ? 'form-control invalid-input' : 'form-control'}
              placeholder="Province"
              // ref={register({required: true})}
              ref={register}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="col">
            <input
              name="country"
              type="text"
              className={errors.country ? 'form-control invalid-input' : 'form-control'}
              placeholder="Country"
              // ref={register({required: true})}
              ref={register}
            />
          </div>
          <div className="col">
            <input
              name="postalCode"
              type="text"
              className={errors.postalCode ? 'form-control invalid-input' : 'form-control'}
              placeholder="Postal Code"
              // ref={register({required: true})}
              ref={register}
            />
          </div>
        </div>
        <div className="row save-changes-btn">
          <button type="submit" className="profile-button-primary">
            Save Changes
          </button>
        </div>
      </form>
    </section>
  );
};

export default EditPersonalPassport;
