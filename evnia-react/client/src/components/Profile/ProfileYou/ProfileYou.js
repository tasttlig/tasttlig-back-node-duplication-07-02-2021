// Libraries
import React, { Component } from 'react';
import S3 from 'react-aws-s3';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import { formatPhoneNumber } from '../../Functions/Functions';

// Styling
import './ProfileYou.scss';

export default class ProfileYou extends Component {
  // Set initial state
  state = {
    updateProfilePhoto: '',
    updateFirstName: '',
    updateLastName: '',
    updateEmail: '',
    updatePassword: '',
    updatePasswordType: 'password',
    updatePhoneNumber: '',
    updateBio: '',
    updateDisabled: false,
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // User profile photo file upload helper function
  handleProfilePhotoUpload = (event) => {
    const config = {
      bucketName: process.env.REACT_APP_AWS_BUCKET_NAME,
      dirName: 'profile-photos',
      region: process.env.REACT_APP_AWS_REGION,
      accessKeyId: process.env.REACT_APP_AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.REACT_APP_AWS_SECRET_ACCESS_KEY,
    };

    const ReactS3Client = new S3(config);

    ReactS3Client.uploadFile(event.target.files[0])
      .then((data) => {
        this.setState({
          updateProfilePhoto: data.location,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // User basic info change helper function
  handleUserBasicInfoChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    let updatePasswordLessChar = '';
    let updatePasswordMinChar = '';

    /* When the user is updating their profile password, display error 
    message when there are less than 8 characters */
    if (nam === 'updatePassword' && val.length < 8) {
      updatePasswordLessChar = <span className="fas fa-times"> At least 8 characters.</span>;
    }

    /* When the user is updating their profile password, display approval 
    message when there are at least 8 characters */
    if (nam === 'updatePassword' && val.length >= 8) {
      updatePasswordMinChar = <span className="fas fa-check">At least 8 characters.</span>;
    }

    // Set at least 8 characters validation state
    if (updatePasswordLessChar || updatePasswordMinChar) {
      this.setState({ updatePasswordLessChar, updatePasswordMinChar });
    }

    this.setState({ [nam]: val });
  };

  // Show/Hide update profile password helper function
  handleClickUpdate = () =>
    this.setState(({ updatePasswordType: type }) => ({
      updatePasswordType: type === 'password' ? 'text' : 'password',
    }));

  // Validate user input for update profile helper function
  validateProfileUpdate = () => {
    let updateFirstNameError = '';
    let updateLastNameError = '';
    let updateEmailError = '';
    let updatePasswordError = '';
    let updatePhoneNumberError = '';

    // Render first name error message
    if (!this.state.updateFirstName) {
      updateFirstNameError = 'First name is required.';
    }

    // Render last name error message
    if (!this.state.updateLastName) {
      updateLastNameError = 'Last name is required.';
    }

    // Render email error message
    if (!this.state.updateEmail) {
      updateEmailError = 'Email is required.';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(this.state.updateEmail)) {
      updateEmailError = 'Please enter a valid email.';
    }

    // Render password error message
    if (!this.state.updatePassword) {
      updatePasswordError = 'Password is required.';
    } else if (this.state.updatePassword.length < 8) {
      updatePasswordError = 'Your password must be at least 8 characters. Please try again.';
    }

    // Render phone number error message
    if (!this.state.updatePhoneNumber) {
      updatePhoneNumberError = 'Phone number is required.';
    } else if (this.state.updatePhoneNumber.length < 14) {
      updatePhoneNumberError = 'Please enter a valid phone number.';
    }

    // Set you error state
    if (
      updateFirstNameError ||
      updateLastNameError ||
      updateEmailError ||
      updatePasswordError ||
      updatePhoneNumberError
    ) {
      this.setState({
        updateFirstNameError,
        updateLastNameError,
        updateEmailError,
        updatePasswordError,
        updatePhoneNumberError,
      });
      return false;
    }

    return true;
  };

  // Submit update profile form helper function
  handleSubmitProfileUpdate = (event) => {
    event.preventDefault();

    const isValid = this.validateProfileUpdate();

    if (isValid) {
      try {
        // JWT update profile (you)
        const userData = {
          profile_img_url: this.state.updateProfilePhoto,
          first_name: this.state.updateFirstName,
          last_name: this.state.updateLastName,
          email: this.state.updateEmail,
          password: this.state.updatePassword,
          phone_number: this.state.updatePhoneNumber,
          bio: this.state.updateBio,
        };

        this.context.jwtUpdateProfile(userData);

        this.setState({
          updateFirstNameError: '',
          updateLastNameError: '',
          updateEmailError: '',
          updatePasswordError: '',
          updatePasswordLessChar: '',
          updatePasswordMinChar: '',
          updatePhoneNumberError: '',
        });

        if (!this.context.state.errorMessage) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          this.setState({ updateDisabled: true });
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  // Render Profile You modal
  render = () => {
    const {
      updateProfilePhoto,
      updateFirstName,
      updateFirstNameError,
      updateLastName,
      updateLastNameError,
      updateEmail,
      updateEmailError,
      updatePassword,
      updatePasswordType,
      updatePasswordError,
      updatePasswordLessChar,
      updatePasswordMinChar,
      updatePhoneNumber,
      updatePhoneNumberError,
      updateBio,
      updateDisabled,
    } = this.state;

    return (
      <div>
        <div className="your-profile">Your Profile</div>

        {this.context.state.user.verified ? (
          <div>
            {this.context.state.errorMessage ? (
              <div className="form-group invalid-message">{this.context.state.errorMessage}</div>
            ) : null}

            <form onSubmit={this.handleSubmitProfileUpdate} noValidate>
              <div className="form-group">
                {updateProfilePhoto ? (
                  <img
                    src={updateProfilePhoto}
                    alt={`${this.context.state.user.first_name} ${this.context.state.user.last_name}`}
                    className="profile-photo"
                  />
                ) : (
                  <span className="fas fa-user-circle fa-7x default-profile-photo"></span>
                )}
              </div>
              <div className="form-group text-left">
                <label htmlFor="file-upload-profile-photo" className="file-upload">
                  Upload Profile Photo
                </label>
                <input
                  id="file-upload-profile-photo"
                  type="file"
                  accept="image/x-png,image/gif,image/jpeg"
                  name="updateProfilePhoto"
                  onChange={this.handleProfilePhotoUpload}
                  disabled={updateDisabled}
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateFirstName"
                  placeholder="First Name*"
                  value={updateFirstName}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="first-name"
                  required
                />
                <span className="far fa-user input-icon"></span>
                {updateFirstNameError ? (
                  <div className="error-message">{updateFirstNameError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="updateLastName"
                  placeholder="Last Name*"
                  value={updateLastName}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="last-name"
                  required
                />
                <span className="far fa-user input-icon"></span>
                {updateLastNameError ? (
                  <div className="error-message">{updateLastNameError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <input
                  type="email"
                  name="updateEmail"
                  placeholder="Email*"
                  value={updateEmail}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="email"
                  required
                />
                <span className="far fa-envelope input-icon"></span>
                {updateEmailError ? <div className="error-message">{updateEmailError}</div> : null}
              </div>
              <div className="form-group">
                <input
                  type={updatePasswordType}
                  name="updatePassword"
                  placeholder="Password*"
                  value={updatePassword}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="password"
                  required
                />
                <span className="password-icon" onClick={this.handleClickUpdate}>
                  {updatePasswordType === 'password' ? (
                    <span className="far fa-eye-slash"></span>
                  ) : (
                    <span className="far fa-eye"></span>
                  )}
                </span>
                <div className="error-message">
                  <div>{updatePasswordError}</div>
                  <div>
                    <div className="password-less-char">{updatePasswordLessChar}</div>
                    <div className="password-min-char">{updatePasswordMinChar}</div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <input
                  type="tel"
                  name="updatePhoneNumber"
                  placeholder="Phone Number*"
                  value={formatPhoneNumber(updatePhoneNumber)}
                  onChange={this.handleUserBasicInfoChange}
                  maxLength="14"
                  disabled={updateDisabled}
                  className="phone-number"
                  required
                />
                <span className="fas fa-phone-alt input-icon"></span>
                {updatePhoneNumberError ? (
                  <div className="error-message">{updatePhoneNumberError}</div>
                ) : null}
              </div>
              <div className="form-group">
                <textarea
                  name="updateBio"
                  placeholder="Please tell us about yourself"
                  value={updateBio}
                  onChange={this.handleUserBasicInfoChange}
                  disabled={updateDisabled}
                  className="bio"
                />
                <span className="fas fa-pencil-alt bio-icon"></span>
              </div>

              <div>
                <button type="submit" disabled={updateDisabled} className="modal-btn">
                  Update Profile
                </button>
              </div>
            </form>
          </div>
        ) : null}
      </div>
    );
  };
}
