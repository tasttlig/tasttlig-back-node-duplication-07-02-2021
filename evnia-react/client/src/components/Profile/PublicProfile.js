// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import FoodSampleOwnerItems from '../FoodSampleOwner/FoodSampleOwnerItems/FoodSampleOwnerItems';
import ExperiencesCard from '../Experiences/ExperiencesCard/ExperiencesCard';
import MenuItemCard from '../Discover/MenuItemCard';

// Styling
import '../FoodSampleOwner/FoodSampleOwner.scss';

const PublicProfile = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [foodSampleOwnerItems, setFoodSampleOwnerItems] = useState([]);
  const [experienceOwnerItems, setExperienceOwnerItems] = useState([]);
  const [menuOwnerItems, setMenuOwnerItems] = useState([]);
  const [foodSamplesCount, setFoodSamplesCount] = useState(0);
  const [experienceCount, setExperienceCount] = useState(0);
  const [MenuItemsCount, setMenuItemsCount] = useState(0);
  const [activeTab, setActiveTab] = useState('food samples');
  const [loading, setLoading] = useState(false);
  const [hasExperienceNextPage, setHasExperienceNextPage] = useState(true);
  const [currentExperiencePage, setCurrentExperiencePage] = useState(0);
  const [hasMenuItemNextPage, setHasMenuItemNextPage] = useState(true);
  const [currentMenuItemPage, setCurrentMenuItemPage] = useState(0);
  const [hasFoodSamplesNextPage, setHasFoodSamplesNextPage] = useState(true);
  const [currentFoodSamplesPage, setCurrentFoodSamplesPage] = useState(0);
  const [ownerDetails, setOwnerDetails] = useState({});

  // Get current user
  const appContext = useContext(AppContext);

  // Render Food Sample Owner Items helper function
  const renderFoodSampleOwnerItems = (arr) => {
    return arr.map((card, index) => (
      <FoodSampleOwnerItems
        business_name={ownerDetails.business_name}
        key={index}
        passportId={card.food_sample_id}
        images={card.image_urls}
        title={card.title}
        nationality={card.nationality}
        alpha_2_code={card.alpha_2_code}
        frequency={card.frequency}
        price={card.price}
        quantity={card.quantity}
        numOfClaims={card.num_of_claims}
        address={card.address}
        city={card.city}
        provinceTerritory={card.state}
        postalCode={card.postal_code}
        startDate={card.start_date}
        endDate={card.end_date}
        startTime={card.start_time}
        endTime={card.end_time}
        description={card.description}
        sample_size={card.sample_size}
        is_available_on_monday={card.is_available_on_monday}
        is_available_on_tuesday={card.is_available_on_tuesday}
        is_available_on_wednesday={card.is_available_on_wednesday}
        is_available_on_thursday={card.is_available_on_thursday}
        is_available_on_friday={card.is_available_on_friday}
        is_available_on_saturday={card.is_available_on_saturday}
        is_available_on_sunday={card.is_available_on_sunday}
        foodSampleOwnerId={card.food_sample_creater_user_id}
        foodSampleOwnerPicture={ownerDetails.profile_image_link}
        isEmailVerified={ownerDetails.is_email_verified}
        firstName={ownerDetails.first_name}
        lastName={ownerDetails.last_name}
        email={ownerDetails.email}
        phoneNumber={ownerDetails.phone_number}
        facebookLink={ownerDetails.facebook_link}
        twitterLink={ownerDetails.twitter_link}
        instagramLink={ownerDetails.instagram_link}
        youtubeLink={ownerDetails.youtube_link}
        linkedinLink={ownerDetails.linkedin_link}
        websiteLink={ownerDetails.website_link}
        bioText={ownerDetails.bio_text}
        history={props.history}
      />
    ));
  };

  // Render Experiences Items helper function
  const renderExperienceCards = (arr) => {
    return arr.map((card, index) => (
      <ExperiencesCard key={card.experience_id} experiencesCard={card} isFullPageView="true" />
    ));
  };

  // Render Menu Items Items helper function
  const renderMenuItemsCards = (arr) => {
    return arr.map((card, index) => (
      <MenuItemCard key={card.menu_item_id} {...card} isFullPageView="true" />
    ));
  };

  // Load next set of user experiences helper functions
  const loadExperienceNextPage = async (page) => {
    const url = `/experience/business/${props.match.params.business_name}`;

    return axios({
      method: 'GET',
      url: url,
      params: {
        page: page + 1,
      },
    });
  };

  // Load next set of user food samples helper functions
  const loadFoodSamplesNextPage = async (page) => {
    const url = `/food-sample/business/${props.match.params.business_name}`;

    return axios({
      method: 'GET',
      url: url,
      params: {
        page: page + 1,
      },
    });
  };

  // Load next set of user food samples helper functions
  const loadMenuItemsNextPage = async (page) => {
    const url = `/menu_items/business/${props.match.params.business_name}`;

    return axios({
      method: 'GET',
      url: url,
      params: {
        page: page + 1,
      },
    });
  };

  const handleExperienceLoadMore = (
    page = currentExperiencePage,
    experiences = experienceOwnerItems,
  ) => {
    setLoading(true);
    loadExperienceNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentExperiencePage(page + 1);
      }

      setHasExperienceNextPage(currentExperiencePage < pagination.lastPage);
      setExperienceOwnerItems(experiences.concat(newPage.data.details.data));
    });
  };

  const handleFoodSamplesLoadMore = (
    page = currentFoodSamplesPage,
    foodSamples = foodSampleOwnerItems,
  ) => {
    setLoading(true);
    loadFoodSamplesNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.food_samples.pagination;

      if (page < pagination.lastPage) {
        setCurrentFoodSamplesPage(page + 1);
      }

      setHasFoodSamplesNextPage(currentFoodSamplesPage < pagination.lastPage);
      setFoodSampleOwnerItems(foodSamples.concat(newPage.data.food_samples.data));
    });
  };

  const handleMenuItemsLoadMore = (page = currentMenuItemPage, menuItems = menuOwnerItems) => {
    setLoading(true);
    loadMenuItemsNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.menu_items.pagination;

      if (page < pagination.lastPage) {
        setCurrentMenuItemPage(page + 1);
      }

      setHasMenuItemNextPage(currentMenuItemPage < pagination.lastPage);
      setMenuOwnerItems(menuItems.concat(newPage.data.menu_items.data));
    });
  };

  const infiniteExperienceRef = useInfiniteScroll({
    loading,
    hasExperienceNextPage,
    onLoadMore: handleExperienceLoadMore,
  });

  const infiniteFoodSamplesRef = useInfiniteScroll({
    loading,
    hasFoodSamplesNextPage,
    onLoadMore: handleFoodSamplesLoadMore,
  });

  const infiniteMenuItemRef = useInfiniteScroll({
    loading,
    hasMenuItemNextPage,
    onLoadMore: handleMenuItemsLoadMore,
  });

  // Mount Profile page
  useEffect(() => {
    window.scrollTo(0, 0);
    const foodSampleUrl = `/food-sample/business/${props.match.params.business_name}`;
    const experienceUrl = `/experience/business/${props.match.params.business_name}`;
    const menuItemUrl = `/menu_items/business/${props.match.params.business_name}`;
    const fetchFoodSamples = async () => {
      const response = await axios({ method: 'GET', url: foodSampleUrl });
      setFoodSampleOwnerItems(response.data.food_samples.data);
      setFoodSamplesCount(response.data.food_samples.pagination.total);
      setOwnerDetails(response.data.owner_user);
    };
    const fetchExperiences = async () => {
      const response = await axios({ method: 'GET', url: experienceUrl });
      setExperienceOwnerItems(response.data.details.data);
      setExperienceCount(response.data.details.pagination.total);
    };
    const fetchMenuItems = async () => {
      const response = await axios({ method: 'GET', url: menuItemUrl });
      setMenuOwnerItems(response.data.menu_items.data);
      setMenuItemsCount(response.data.menu_items.pagination.total);
    };

    fetchFoodSamples();
    fetchExperiences();
    fetchMenuItems();
  }, []);

  // Profile Banner Component
  const Banner = () => (
    <div>
      <div className="food-sample-owner-profile-banner">
        <div className="row food-sample-owner-profile-banner-simple">
          <div className="col-lg-5 banner-content">
            <div className="mb-4">
              {ownerDetails.profile_image_link ? (
                <img
                  src={ownerDetails.profile_image_link}
                  alt={`${ownerDetails.first_name} ${ownerDetails.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'food-sample-owner-profile-picture' : 'loading-image'}
                />
              ) : (
                <span className="fas fa-user-circle fa-5x food-sample-owner-default-picture"></span>
              )}
            </div>
            <div className="food-sample-owner-profile-name">
              {`${ownerDetails.first_name} ${ownerDetails.last_name}`}
            </div>
            <div className="food-sample-owner-profile-promo">
              {ownerDetails.profile_tag_line && (
                <div className="pro mb-4 food-sample-owner-profile-tag-line">
                  {ownerDetails.profile_tag_line}
                </div>
              )}
              <address>
                {ownerDetails.business_name && (
                  <div>
                    <strong>{ownerDetails.business_name}</strong>
                    <br />
                  </div>
                )}
                {ownerDetails.business_address_1 && ownerDetails.business_address_1}
                {ownerDetails.business_address_2 && (
                  <span>, {ownerDetails.business_address_2}</span>
                )}
                <br />
                {ownerDetails.city && `${ownerDetails.city},`}{' '}
                {ownerDetails.state && ownerDetails.state}{' '}
                {ownerDetails.postal_code && ownerDetails.postal_code}
                <br />
                <div
                  id="profile-email"
                  className="btn btn-primary p-2"
                  title="Email"
                  onClick={() => {
                    window.location.href = 'mailto:' + ownerDetails.email;
                  }}
                >
                  <span className="far fa-envelope p-2" />
                  {ownerDetails.email}
                </div>
                {/* <div title="Phone">P: {ownerDetails.phone_number}</div> */}
              </address>
              {ownerDetails.created_by_admin && (
                <Link exact="true" to="/forgot-password">
                  {' '}
                  Is this your restaurant? Claim it here!
                </Link>
              )}
            </div>
            {/* <div>
              <Link to="/account" className="edit-profile-link">
                Edit Profile
              </Link>
            </div> */}
          </div>
          <div className="col-lg-7 banner-content">
            <div className="banner-images">
              <div className="banner-images-wrapper">
                <div className="banner-color-block-container">
                  <div className="banner-color-block"></div>
                </div>
                {ownerDetails.banner_image_link ? (
                  <img
                    src={ownerDetails.banner_image_link}
                    alt="Banner"
                    onLoad={() => setLoad(true)}
                    className={load ? 'main-banner-image' : 'loading-image'}
                  />
                ) : (
                  <div className="main-banner-image"></div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="fade-rule" />
    </div>
  );

  return (
    <div>
      <Nav />

      {/* Food Sample Owner section */}
      <div className="food-sample-owner">
        <Banner />

        {/*  <div className="row m-5">
          <div
            onClick={() => {
              setActiveTab('food samples');
            }}
            className={
              activeTab === 'food samples'
                ? 'col-sm-auto m-3 pt-2 pb-2 pl-3 pr-3 badge-pill text-light border bg-dark text-sm-center cursor-pointer'
                : 'col-sm-auto m-3 pt-2 pb-2 pl-3 pr-3 badge-pill text-dark border bg-light text-sm-center cursor-pointer'
            }
          >
            Food Samples ({foodSamplesCount})
          </div>
          <div
            onClick={() => {
              setActiveTab('experiences');
            }}
            className={
              activeTab === 'experiences'
                ? 'col-sm-auto m-3 pt-2 pb-2 pl-3 pr-3 badge-pill text-light border bg-dark text-sm-center cursor-pointer'
                : 'col-sm-auto m-3 pt-2 pb-2 pl-3 pr-3 badge-pill text-dark border bg-light text-sm-center cursor-pointer'
            }
          >
            Experiences ({experienceCount})
          </div>
          <div
            onClick={() => {
              setActiveTab('menu items');
            }}
            className={
              activeTab === 'menu items'
                ? 'col-sm-auto m-3 pt-2 pb-2 pl-3 pr-3 badge-pill text-light border bg-dark text-sm-center cursor-pointer'
                : 'col-sm-auto m-3 pt-2 pb-2 pl-3 pr-3 badge-pill text-dark border bg-light text-sm-center cursor-pointer'
            }
          >
            Menu Items ({MenuItemsCount})
          </div>
        </div> */}

        {/*Food Sample Owner Items section*/}
        {/* <div className="food-sample-owner-items" ref={infiniteFoodSamplesRef}>
          {activeTab === 'food samples' && foodSampleOwnerItems && foodSampleOwnerItems.length !== 0
            ? renderFoodSampleOwnerItems(foodSampleOwnerItems)
            : null}
        </div> */}

        {/*Experiences Owner Items section*/}
        {activeTab === 'experiences' ? (
          <div className="food-sample-owner-items" ref={infiniteExperienceRef}>
            {experienceOwnerItems &&
              experienceOwnerItems.length !== 0 &&
              renderExperienceCards(experienceOwnerItems)}
          </div>
        ) : null}

        {/*Menu Items Owner Items section*/}
        {activeTab === 'menu items' ? (
          <div className="food-sample-owner-items" ref={infiniteMenuItemRef}>
            {menuOwnerItems && menuOwnerItems.length !== 0 && renderMenuItemsCards(menuOwnerItems)}
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default PublicProfile;
