// Libraries
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { Checkbox } from './../../EasyForm';
import { useHistory } from 'react-router-dom';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';

// Styling
import './SponsorPackageCashCard.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../assets/images/live.png';

toast.configure();

const SponsorPackageCashCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const history = useHistory();

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;

  // Set date and time
  const startDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.startTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.endTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  const pay = async () => {
    await history.push({
      pathname: `/payment/package/S_KMIN`,
      title: props.title,
      festivalId: props.festivalId,
    });
  };

  return (
    <div className="col-md-6 festival-card">
      <LazyLoad once>
        <div className="d-flex flex-column h-100 card-box">
          {props.images.length > 1 ? (
            <div>
              <ImageSlider images={props.images} isCard={true} />
            </div>
          ) : (
            <img
              src={props.images[0]}
              alt={props.title}
              onLoad={() => setLoad(true)}
              className={load ? 'festival-card-image' : 'loading-image'}
            />
          )}

          {upcomingDays > 0 ? (
            <div className="festival-card-date-upcoming">
              <span className="festival-card-date-upcoming-text">Upcoming</span>
              <span className="festival-card-date-upcoming-days">{`in ${upcomingDays} day${
                upcomingDays > 1 ? 's' : ''
              }`}</span>
            </div>
          ) : upcomingDays === 0 ? (
            <div className="festival-card-date-live">
              <img src={liveFestival} alt="Live Festival" />
            </div>
          ) : endingDays > 0 ? (
            <div className="festival-card-date-ending">
              <span className="festival-card-date-ending-text">Ending</span>
              <span className="festival-card-date-ending-days">{`in ${endingDays} day${
                endingDays > 1 ? 's' : ''
              }`}</span>
            </div>
          ) : null}

          <div className="col-12 pt-5 mt-5 position-absolute">
            <div className=" col-4 mx-auto bg-danger py-1 price-box">
              <p className="text-white text-center">Sponsor Price</p>
              <h5 className="text-white text-center">{`$30`}</h5>
            </div>
          </div>

          <div className="festival-card-text">
            <div className="festival-card-title">{props.title}</div>
            <div className="festival-card-details">
              <div className="row">
                <div className="col-md-8 festival-card-time">
                  <span className="fas fa-clock pr-1 festival-card-time-icon"></span>
                  Starts At:{' '}
                  {`${moment(startDateTime).format('h:mm a')} - ${moment(endDateTime).format(
                    'h:mm a',
                  )}`}
                </div>
                <div className="col-md-4 pr-0 pl-1 festival-card-location">
                  <span className="fas fa-map-marker-alt pr-1 festival-card-location-icon"></span>
                  {props.city}
                </div>
              </div>
            </div>
            <div className="festival-card-details">{props.description}</div>
            <div className="row">
              <input
                onClick={pay}
                type="submit"
                className="mx-auto text-white btn btn-danger px-5 py-3 rounded-pill"
                value="Pay"
              />
            </div>
          </div>

          <div className="festival-card-footer">
            <div className="row">
              <div className="col-md-8 festival-card-category">{props.type}</div>
            </div>
          </div>
          {/* </Link> */}
        </div>
      </LazyLoad>
    </div>
  );
};

export default SponsorPackageCashCard;
