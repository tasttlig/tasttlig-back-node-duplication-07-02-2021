// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import { AppContext } from './../../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';

// Styling
import './SponsorProductsAndServices.scss';
import 'react-toastify/dist/ReactToastify.css';

// Components
import Footer from './../../../Home/BannerFooter/BannerFooter';
import GoTop from './../../../Shared/GoTop';
import Nav from './../../../Navbar/Nav';
import SponsorProductOrServiceCard from './../../../FestivalPage/SponsorProductOrServiceCard/SponsorProductOrServiceCard';
import {
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
  Checkbox,
  CheckboxGroup,
} from './../../../EasyForm';
import {
  formatDate,
  formattedTimeToDateTime,
  formatMilitaryToStandardTime,
} from './../../../Functions/Functions';

toast.configure();

const SponsorProductsAndServices = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  if (!userRole) {
    window.location.href = '/';
  }
  // Check that user is signed in to access user specific pages
  //if (!appContext.state.signedInStatus) window.location.href = "/login";

  // Set initial state
  const [productsInFestival, setProductsInFestival] = useState([]);
  const [products, setProducts] = useState([]);
  const [services, setServices] = useState([]);
  const [servicesInFestival, setServicesInFestival] = useState([]);
  const [loading, setLoading] = useState(true);
  const [keyword, setKeyword] = useState('');
  const [createFormsOpened, setCreateFormsOpened] = useState(false);
  const [nationalities, setNationalities] = useState([]);
  const [festivalList, setFestivalList] = useState([]);
  const [formDetails, setFormDetails] = useState({
    input_one: 'product_name',
    input_one_placeholder: 'Name Your Product',
    input_two: 'product_made_in_nationality_id',
    input_three: 'product_price',
    input_three_placeholder: 'Price',
    input_four: 'product_quantity',
    input_four_placeholder: 'Quantity',
    input_five: 'product_size',
    input_five_placeholder: 'Size',
    input_six: 'product_description',
    input_six_placeholder: 'Description',
    input_seven: 'product_expiry_date',
    input_seven_placeholder: 'Expiry Date',
    input_eight: 'product_manufacture_date',
    input_eight_placeholder: 'Manufacture Date',
    input_nine: 'product_images',
    input_ten: 'product_festival_id',
    input_eleven: 'product_creator_type',
  });
  const [selected, setSelected] = useState('products');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [amountOfVendorSubscriptions, setAmountOfVendorSubscriptions] = useState(0);
  const [subscriptionId, setSubscriptionId] = useState(0);
  let data = {};

  //will contain logic to count sponsor's number allowed festivals
  const handleChange = () => {};

  // Fetch products in festival helper function
  const fetchProductsInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/products/festival/${props.location.festivalId}`,
      });

      setProductsInFestival(response.data.details);
      /* setProductsInFestival(
        response.data.details.filter(
          (product) => product.product_user_id === appContext.state.user.id
        )
      ); */
      return response;
    } catch (error) {
      return error.response;
    }
  };
  // Fetch user products
  const fetchUserProducts = async () => {
    const response = await axios({
      method: 'GET',
      url: `/products/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
        keyword: keyword,
      },
    });
    setProducts(response.data.details);
    return response;
  };
  // Fetch user services
  const fetchUserServices = async () => {
    const response = await axios({
      method: 'GET',
      url: `/services/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
        keyword: keyword,
      },
    });
    setServices(response.data.details);
    return response;
  };

  // Fetch services in festival helper function
  const fetchServicesInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/services/festival/${props.location.festivalId}`,
      });

      setServicesInFestival(response.data.details);
      /* setServicesInFestival(
        response.data.details.filter(
          (service) => service.service_user_id === appContext.state.user.id
        )
      ); */

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Get all fetches helper function
  const getAllFetches = async () => {
    await fetchUserProducts();
    await fetchUserServices();
    await fetchProductsInFestival();
    await fetchServicesInFestival();
    await fetchSubscriptions();
    setLoading(false);
  };

  // Render product cards helper function
  const renderProductCards = (arr) => {
    return arr.map((product, index) => (
      <div className="col-md-6 col-xl-4">
        <div className="col-md-12 col-xl-8 restaurant-card">
          <SponsorProductOrServiceCard
            key={index}
            productId={product.product_id}
            images={product.image_urls}
            title={product.product_name}
            price={product.product_price}
            address={product.business_address_1}
            restaurant_name={product.business_name}
            city={product.city}
            startDate={formatDate(product.product_expiry_date)}
            startTime={
              product.product_expiry_time &&
              formatMilitaryToStandardTime(product.product_expiry_time)
            }
            description={product.product_description}
            classNameTrue={false}
            claim={false}
            itemType="product"
            isSponsor={userRole && userRole.includes('SPONSOR')}
          />
        </div>
        <div className="button-checkmark">
          <ButtonOrCheckmark
            product={product}
            className="button-checkmark"
            festivalId={props.location.festivalId}
            isProduct={true}
          ></ButtonOrCheckmark>
        </div>
      </div>
    ));
  };

  // Render service cards helper function
  const renderServiceCards = (arr) => {
    return arr.map((service, index) => (
      <div className="col-md-6 col-xl-4">
        <div className="col-md-12 col-xl-8 restaurant-card">
          <SponsorProductOrServiceCard
            key={index}
            serviceId={service.service_id}
            images={service.image_urls}
            title={service.service_name}
            price={service.service_price}
            address={service.business_address_1}
            restaurant_name={service.business_name}
            description={service.service_description}
            itemType="service"
            claim={false}
            isSponsor={userRole && userRole.includes('SPONSOR')}
          />
        </div>
        <div className="button-checkmark">
          <ButtonOrCheckmark
            product={service}
            className="button-checkmark"
            festivalId={props.location.festivalId}
            isProduct={false}
          ></ButtonOrCheckmark>
        </div>
      </div>
    ));
  };

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(false);
    }
  };

  // Select product form helper function
  const productSelected = () => {
    setFormDetails({
      input_one: 'product_name',
      input_one_placeholder: 'Name Your Product',
      input_two: 'product_made_in_nationality_id',
      input_three: 'product_price',
      input_three_placeholder: 'Price',
      input_four: 'product_quantity',
      input_four_placeholder: 'Quantity',
      input_five: 'product_size',
      input_five_placeholder: 'Size',
      input_six: 'product_description',
      input_six_placeholder: 'Description',
      input_seven: 'product_expiry_date',
      input_seven_placeholder: 'Expiry Date',
      input_eight: 'product_manufacture_date',
      input_eight_placeholder: 'Manufacture Date',
      input_nine: 'product_images',
      input_ten: 'product_festival_id',
      input_eleven: 'product_creator_type',
    });
  };

  // Select service form helper function
  const serviceSelected = () => {
    setFormDetails({
      input_one: 'service_name',
      input_one_placeholder: 'Name Your Service',
      input_two: 'service_nationality_id',
      input_three: 'service_price',
      input_three_placeholder: 'Price',
      input_four: 'service_capacity',
      input_four_placeholder: 'Capacity',
      input_five: 'service_size_scope',
      input_five_placeholder: 'Scope',
      input_six: 'service_description',
      input_six_placeholder: 'Description',
      input_seven: null,
      input_seven_placeholder: null,
      input_eight: null,
      input_eight_placeholder: null,
      input_nine: 'service_images',
      input_ten: 'service_festival_id',
    });
  };

  //select multiple festivals
  const selectFestivals = () => {
    return festivalList.map((fest) => (
      <Checkbox name={fest.festival_id} label={fest.festival_name} />
    ));
  };

  // Change form helper function
  const changeForm = (event) => {
    if (event.target.value === 'services') {
      setSelected('services');
      serviceSelected();
    } else {
      setSelected('products');
      productSelected();
    }
  };

  // Submit product, service helper functions
  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      //window.location.href = `/festival/${props.location.festivalId}`;
      window.location.href = '/';
    }, 2000);

    toast(`Success! Thank you for creating ${selected}!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSubmitAuthDisabled(true);
  };

  const submitProductOrService = async (data) => {
    window.scrollTo(0, 0);
    if (
      amountOfVendorSubscriptions !== 0 ||
      data.product_festival_id === null ||
      data.service_festival_id === null
    ) {
      let url = '';
      if (selected === 'products') {
        if (userRole) data.product_creator_type = userRole[0];
        url = '/products/add';
      } else if (selected === 'services') {
        if (userRole) data.service_creator_type = userRole[0];
        url = '/services/add';
      }
      try {
        const response = await axios({
          method: 'POST',
          url,
          data,
        });

        if (response && response.data && response.data.success && userRole) {
          defaultOnCreatedAction();
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    } else {
      toast('Subscription has been used up/invalid!', {
        type: 'error',
        autoClose: 4000,
      });
    }
  };

  const onSubmit = async (values) => {
    const { ...rest } = values;

    let data = {
      ...rest,
    };

    if (selected === 'products') {
      //actions based on festival selection
      if (
        data.product_festival_id === 'all_by_subscription' &&
        data.selectedFestivals.length >= 1
      ) {
        if (
          data.selectedFestivals < amountOfVendorSubscriptions ||
          amountOfVendorSubscriptions === -1
        ) {
          data.product_festival_id = data.selectedFestivals;
        } else {
          toast('Subscription has been used up/invalid!', {
            type: 'error',
            autoClose: 4000,
          });
          return;
        }
      } else if (
        data.product_festival_id === 'all_by_subscription' &&
        data.selectedFestivals.length < 1
      ) {
        toast('Please select festival(s)!', {
          type: 'warn',
          autoClose: 4000,
        });
      } else if (
        data.product_festival_id !== 'all_by_subscription' &&
        data.selectedFestivals.length >= 1
      ) {
        toast('Please select upcoming(created) festivals to select other festivals!', {
          type: 'warn',
          autoClose: 4000,
        });
        return;
      } else if (data.product_festival_id === 'uncreated') {
        data.product_festival_id = null;
      }

      /*       data.product_expiry_time = formattedTimeToDateTime(
        values.product_expiry_time.toString().substring(16, 21),
      ); */
      data.product_expiry_date = formatDate(values.product_expiry_date);

      await submitProductOrService(data);
    } else {
      //actions based on festival selection
      if (
        data.service_festival_id === 'all_by_subscription' &&
        data.selectedFestivals.length >= 1
      ) {
        if (
          data.selectedFestivals < amountOfVendorSubscriptions ||
          amountOfVendorSubscriptions === -1
        ) {
          data.service_festival_id = data.selectedFestivals;
        } else {
          toast('Subscription has been used up/invalid!', {
            type: 'error',
            autoClose: 4000,
          });
          return;
        }
      } else if (
        data.service_festival_id === 'all_by_subscription' &&
        data.selectedFestivals.length < 1
      ) {
        toast('Please select festival(s)!', {
          type: 'error',
          autoClose: 4000,
        });
        return;
      } else if (
        data.service_festival_id !== 'all_by_subscription' &&
        data.selectedFestivals.length >= 1
      ) {
        toast('Please select upcoming(created) festivals to select other festivals!', {
          type: 'error',
          autoClose: 4000,
        });
        return;
      } else if (data.service_festival_id === 'uncreated') {
        data.service_festival_id = null;
      }
      await submitProductOrService(data);
    }
  };

  // Fetch nationalities helper function
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
  };

  const fetchSubscriptions = async () => {
    const response = await axios({
      method: 'GET',
      url: `/subscription/${appContext.state.user.id}`,
    });
    console.log(response);
    for (let subscription of response.data.user) {
      if (subscription.subscription_code === 'V_MIN') {
        if (!subscription.suscribed_festivals) {
          setAmountOfVendorSubscriptions(1);
          setSubscriptionId(subscription.user_subscription_id);
          return;
        } else {
          setAmountOfVendorSubscriptions(0);
        }
      } else if (subscription.suscribed_festivals && subscription.subscription_code === 'V_MOD') {
        if (subscription.suscribed_festivals.length < 4) {
          setAmountOfVendorSubscriptions(4 - subscription.suscribed_festivals.length);
          setSubscriptionId(subscription.user_subscription_id);
          return;
        } else {
          setAmountOfVendorSubscriptions(0);
        }
      } else if (subscription.suscribed_festivals && subscription.subscription_code === 'V_ULTRA') {
        setAmountOfVendorSubscriptions(-1);
        setSubscriptionId(subscription.user_subscription_id);
        return;
      }
    }
  };

  // Mount Dashboard page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchNationalities();
    fetchFestivalList();
    getAllFetches();
  }, []);

  let ButtonOrCheckmark = (props) => {
    const { values, readMode, product } = props;
    const [button, setButton] = useState(true);
    const [checkmark, setCheckmark] = useState(false);

    const addToFestival = async (data) => {
      let url;
      let ps;
      // if (amountOfVendorSubscriptions > 0 || amountOfVendorSubscriptions === -1) {
      if (props.isProduct) {
        url = `/products/festival/${props.festivalId}`;
        ps = product.product_id;
      } else {
        url = `/services/festival/${props.festivalId}`;
        ps = product.service_id;
      }
      const response = await axios({
        method: 'POST',
        url,
        data: {
          festivalId: props.festivalId,
          ps,
        },
      });
      if (response.data.details === 'Success.') {
        /*  const response = await axios({
            method: 'POST',
            url: `/subscriptions/addFestival/${appContext.state.user.id}`,
            data: {
              festivalId: props.festivalId,
              user_subscription_id: subscriptionId,
            },
          }); */
        console.log(response);
        //setAmountOfVendorSubscriptions((prev) => prev - 1);
        setButton(false);
        setCheckmark(true);
      }
      /*     if (products[0].product_festivals_id.includes(values.festivalId)) {
              setButton(false)
              setCheckmark(true);
            } */
      return response;
      //  } else {
      // toast('Subscription has been used up/invalid!', {
      // type: 'error',
      // autoClose: 4000,
      // });
      //}
    };
    useEffect(() => {
      if (
        (product.product_festivals_id && product.product_festivals_id.includes(props.festivalId)) ||
        (product.service_festival_id && product.service_festival_id.includes(props.festivalId))
      ) {
        setButton(false);
        setCheckmark(true);
      }
    }, []);
    return (
      <Form onSubmit={addToFestival}>
        {button && <input type="submit" className="" value="Add to Festival"></input>}

        {checkmark && (
          <span className="checkmark">
            <div className="checkmark_circle"></div>
            <div className="checkmark_stem"></div>
            <div className="checkmark_kick"></div>
          </span>
        )}
      </Form>
    );
  };

  return (
    <div>
      <Nav />
      <Modal
        isOpen={createFormsOpened}
        onRequestClose={closeModal('create-forms')}
        ariaHideApp={false}
        className="dashboard-forms-modal"
      >
        <div>
          <div className="row">
            <div className="col-md-6 my-auto dashboard-forms-input-content-col-1">
              <div className="dashboard-forms-title mb-3">Add</div>
            </div>
            <div className="col-md-6 dashboard-forms-input-content-col-3">
              <select
                name="select-form"
                selected="products"
                onChange={changeForm}
                disabled={submitAuthDisabled}
                className="custom-select mb-3"
                required
              >
                <option value="products">Products</option>
                <option value="services">Services</option>
              </select>
            </div>
            <div className="dashboard-forms-close">
              <button
                aria-label={`Close Forms Modal`}
                onClick={closeModal('create-forms')}
                className="fas fa-times fa-2x close-modal"
              ></button>
            </div>
          </div>
          <Form data={data} onSubmit={onSubmit}>
            <div className="row">
              <div className="col-md-6 dashboard-forms-input-content-col-1">
                <Input
                  name={formDetails.input_one}
                  placeholder={formDetails.input_one_placeholder}
                  disabled={submitAuthDisabled}
                  className="product-name-input"
                  required
                  label={null}
                />
              </div>
              <div className="col-md-6 dashboard-forms-input-content-col-3">
                {nationalities.length && (
                  <Select
                    name={formDetails.input_two}
                    placeholder={formDetails.input_two_placeholder}
                    className="product-name-input"
                    disabled={submitAuthDisabled}
                    required
                  >
                    <option value="">--Nationality--</option>
                    {nationalities.map((n) => (
                      <option key={n.id} value={n.id}>
                        {n.nationality}
                      </option>
                    ))}
                  </Select>
                )}
              </div>
            </div>
            <div className="row">
              <div className="col-md-4 dashboard-forms-input-content-col-1">
                <Input
                  name={formDetails.input_three}
                  step="0.01"
                  placeholder={formDetails.input_three_placeholder}
                  className="product-name-input"
                  disabled={submitAuthDisabled}
                  required
                />
              </div>
              <div className="col-md-4 dashboard-forms-input-content-col-2">
                <Input
                  name={formDetails.input_four}
                  type="number"
                  placeholder={formDetails.input_four_placeholder}
                  min="1"
                  className="product-name-input"
                  disabled={submitAuthDisabled}
                  required
                />
              </div>
              <div className="col-md-4 dashboard-forms-input-content-col-3">
                <Select
                  name={formDetails.input_five}
                  placeholder={formDetails.input_five_placeholder}
                  className="product-name-input"
                  disabled={submitAuthDisabled}
                  required
                >
                  <option value="">{`--${
                    formDetails.input_five === 'product_size' ? 'Size' : 'Scope'
                  }--`}</option>
                  <option value="Bite Size">Bite Size</option>
                  <option value="Quarter">Quarter</option>
                  <option value="Half">Half</option>
                  <option value="Full">Full</option>
                </Select>
              </div>
            </div>
            {selected === 'products' && (
              <div className="row">
                <div className="col-md-6 dashboard-forms-input-content-col-1">
                  <DateInput
                    name={formDetails.input_seven}
                    placeholderText={formDetails.input_seven_placeholder}
                    dateFormat="yyyy/MM/dd"
                    minDate={new Date()}
                    peekNextMonth
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                    disabled={submitAuthDisabled}
                    className="product-name-input"
                    required
                  />
                </div>
                <div className="col-md-6 dashboard-forms-input-content-col-3">
                  <DateInput
                    name={formDetails.input_eight}
                    placeholderText={formDetails.input_eight_placeholder}
                    dateFormat="yyyy/MM/dd"
                    minDate={new Date()}
                    peekNextMonth
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                    disabled={submitAuthDisabled}
                    className="last-date-input"
                    required
                  />
                </div>
              </div>
            )}
            <div className="first-row">
              <Textarea
                name={formDetails.input_six}
                placeholder={formDetails.input_six_placeholder}
                disabled={submitAuthDisabled}
                className="product-name-input"
                required
              />
            </div>
            <div className="first-row">
              {selected === 'products' && (
                <MultiImageInput
                  name="product_images"
                  dropbox_label="Click or drag-and-drop to upload one or more images"
                  disabled={submitAuthDisabled}
                  className="product-images"
                  required
                />
              )}
              {selected === 'services' && (
                <MultiImageInput
                  name="service_images"
                  dropbox_label="Click or drag-and-drop to upload one or more images"
                  disabled={submitAuthDisabled}
                  className="product-images"
                  required
                />
              )}
            </div>
            {festivalList.length ? (
              <>
                <div>{`Which festival does this ${
                  selected === 'products' ? 'product' : 'service'
                } belong to?`}</div>
                <div>
                  {props.location.festivalId ? (
                    <Select
                      name={formDetails.input_ten}
                      className="product-name-input"
                      disabled={submitAuthDisabled}
                      required
                    >
                      <option value="">--Select--</option>
                      <option value={props.location.festivalId}>
                        Selected Festival &#40;{props.location.title}&#41;
                      </option>
                      <option value="uncreated">Future&#40;Uncreated&#41; Festivals</option>
                      <option value="all_by_subscription">
                        Upcoming&#40;Created&#41; Festivals
                      </option>
                    </Select>
                  ) : (
                    <Select
                      name={formDetails.input_ten}
                      className="product-name-input"
                      disabled={submitAuthDisabled}
                      required
                    >
                      <option value="">--Select--</option>
                      {festivalList.map((f) => (
                        <option key={f.festival_id} value={f.festival_id}>
                          {f.festival_name}
                        </option>
                      ))}
                    </Select>
                  )}
                </div>
                <div>
                  {/* {festivalList.map((fest) => (
                    <Checkbox
                      name={fest.festival_id}
                      label={fest.festival_name}
                    />
                  ))} */}

                  <CheckboxGroup
                    name="selectedFestivals"
                    label={`Choose the ${selected} for multiple festivals?`}
                    options={festivalList.map((fest) => [fest.festival_name, fest.festival_id])}
                  />
                </div>
              </>
            ) : null}

            <div className="first-row">
              <button type="submit" className="dashboard-forms-add-btn">
                Add
              </button>
            </div>
          </Form>
        </div>
      </Modal>

      <div className="dashboard-products-and-services">
        <div className="row">
          <div className="col-lg-8 px-0">
            <div className="dashboard-title">Add Products and services</div>
          </div>
          {userRole &&
          (userRole.includes('ADMIN') ||
            userRole.includes('SPONSOR') ||
            userRole.includes('VENDOR')) ? (
            <div className="col-lg-4 px-0 dashboard-section dashboard-forms-btn-content">
              <button onClick={openModal('create-forms')} className="dashboard-forms-btn">
                <span className="fas fa-3x fa-plus-circle" />
              </button>
            </div>
          ) : null}
        </div>
      </div>

      <div className="mb-5">
        <div className="dashboard-title text-center"> Products </div>
        <div className="row">{!loading && products && renderProductCards(products)}</div>
        {products < 1 && <div className="text-center">No product added yet</div>}
      </div>

      <div>
        <div className="dashboard-title text-center"> Services </div>
        <div className="row">{!loading && services && renderServiceCards(services)}</div>
        {services < 1 && <div className="text-center">No service added yet</div>}
      </div>

      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default SponsorProductsAndServices;
