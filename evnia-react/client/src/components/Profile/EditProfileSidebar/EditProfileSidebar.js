// Libraries
import React from 'react';
import { useHistory, Link } from 'react-router-dom';

// Styling
import './EditProfileSidebar.scss';

const EditProfileSidebar = () => {
  const history = useHistory();

  return (
    <div>
      <div
        className="nav flex-column nav-pills edit-profile-sidebar"
        id="v-pills-tab"
        role="tablist"
        aria-orientation="vertical"
      >
        <Link className="edit-profile-sidebar-link" data-toggle="pill" to="/account" role="tab">
          Account
        </Link>
        <Link
          className="edit-profile-sidebar-link"
          data-toggle="pill"
          to="/account/profile"
          role="tab"
        >
          Profile
        </Link>
      </div>

      {/* Responsive Design */}
      <div className="edit-profile-sidebar-dropdown">
        <select onChange={(e) => history.push(e.target.value)} className="custom-select">
          <option>Select</option>
          <option value="/account">Account</option>
          <option value="/account/profile">Profile</option>
        </select>
      </div>
    </div>
  );
};

export default EditProfileSidebar;
