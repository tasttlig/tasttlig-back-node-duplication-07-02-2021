import React, { useState, useEffect, useContext } from 'react';

import './Preferences.scss';

const Preferences = (props) => {
  console.log('props from preferences:', props.appContext);
  return (
    <div
      onClick={() => props.toggleFullView('preferences')}
      className="passport-activities__card preferences-card"
    >
      <h3 className="passport-activities__card-title">Preferences</h3>
      <div onClick={(e) => window.location.href='/complete-profile/preference'}
      className="passport-activites__preference-btn">Edit</div>
      <div className="passport-activities__card-festival-list">
        <div className="row preferences__card-content">
          <h5 className="preferences__subtitle">Cuisine Preferences</h5>
          <div className="preferences__tags">
            {props.appContext.preferred_country_cuisine &&
            props.appContext.preferred_country_cuisine.length !== 0 ? (
              props.appContext.preferred_country_cuisine.map((cuisine) => {
                return <span className="red-tag">{cuisine}</span>;
              })
            ) : (
              <span className="grey-tag">None</span>
            )}
          </div>
        </div>
        <div className="row preferences__card-content">
          <h5 className="preferences__subtitle">Food Preferences</h5>
          <div className="preferences__tags">
            {props.appContext.food_preferences && props.appContext.food_preferences.length !== 0 ? (
              props.appContext.food_preferences.map((preferences) => {
                return <span className="red-tag">{preferences}</span>;
              })
            ) : (
              <span className="grey-tag">None</span>
            )}
          </div>
        </div>
        <div className="row preferences__card-content">
          <h5 className="preferences__subtitle">Allergies</h5>
          <div className="preferences__tags">
            {props.appContext.food_allergies && props.appContext.food_allergies.length !== 0 ? (
              props.appContext.food_allergies.map((allergies) => {
                if (allergies !== 'No allergies') {
                  return <span className="red-tag">{allergies}</span>;
                } else {
                  return <span className="grey-tag">None</span>;
                }
              })
            ) : (
              <span className="grey-tag">None</span>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Preferences;
