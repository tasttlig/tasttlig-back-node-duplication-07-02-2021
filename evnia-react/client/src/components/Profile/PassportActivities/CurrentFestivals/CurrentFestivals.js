import React, { useState, useEffect, useContext } from 'react';

import './CurrentFestivals.scss';

const CurrentFestivals = (props) => {
  return (
    <div
      onClick={() => props.toggleFullView('currentFestivals')}
      className="passport-activities__card current-festivals-card"
    >
      <h3 className="passport-activities__card-title">Current Festivals</h3>
      <div className="passport-activites__card-count">{props.currentFestivals.length}</div>
      <div className="passport-activities__card-festival-list">
        {props.currentFestivals && props.currentFestivals.length !== 0 ? (
          props.currentFestivals.map((festival) => {
            return <span>{festival.festival_name}</span>;
          })
        ) : (
          <span>You are currently not attending any festivals!</span>
        )}
      </div>
    </div>
  );
};

export default CurrentFestivals;
