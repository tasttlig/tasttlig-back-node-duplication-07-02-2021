import React, { useState, useEffect, useContext, Fragment } from 'react';
import { AppContext } from '../../../ContextProvider/AppProvider';
import CurrentFestivals from './CurrentFestivals/CurrentFestivals';
import PastFestivals from './PastFestivals/PastFestivals';
import MyStamps from './MyStamps/MyStamps';
import Preferences from './Preferences/Preferences';
import Rewards from './Rewards/Rewards';

import './PassportActivities.scss';

const PassportActivities = (props) => {
  const appContext = useContext(AppContext);

  const {
    toggleFullCardView,
    setToggleFullCardView,
    currentFestivals,
    expiredFestivals,
    stampList,
    infiniteRef,
  } = props;

  return (
    <Fragment>
      <div className="row" ref={infiniteRef}>
        <div className="col">
          <CurrentFestivals
            currentFestivals={currentFestivals}
            toggleFullView={props.toggleFullView}
          />
        </div>
        <div className="col">
          <PastFestivals
            expiredFestivals={expiredFestivals}
            toggleFullView={props.toggleFullView}
          />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <MyStamps stampList={stampList} toggleFullView={props.toggleFullView} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <Preferences appContext={appContext.state.user} toggleFullView={props.toggleFullView} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <Rewards toggleFullView={props.toggleFullView} />
        </div>
      </div>
    </Fragment>
  );
};

export default PassportActivities;
