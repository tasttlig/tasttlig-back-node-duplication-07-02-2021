import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';

import './MyStamps.scss';

const MyStamps = (props) => {
  console.log('props from my stamps:', props.stampList);
  return (
    <div
      onClick={() => props.toggleFullView('myStamps')}
      className="passport-activities__card my-stamps-card"
    >
      <h3 className="passport-activities__card-title">My Stamps</h3>

      <div className="passport-activities__card-festival-list">
        {props.stampList.length !== 0 ? (
          props.stampList.map((stamp) => {
            return <span>{stamp.title}</span>;
          })
        ) : (
          <span>You dont have any stamps yet!</span>
        )}
      </div>
    </div>
  );
};

export default MyStamps;
