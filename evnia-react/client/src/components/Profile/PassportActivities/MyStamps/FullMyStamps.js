import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';

import './MyStamps.scss';

const FullMyStamps = (props) => {
  return (
    <div className="passport-activities__full-card my-stamps-card">
      <h3 className="passport-activities__card-title">My Stamps</h3>
      <div onClick={() => props.toggleFullView('')} className="passport-activites__full-card-exit">
        <i className="fas fa-times-circle"></i>
      </div>
      <div className="container">
        <div className="row passport-activities__full-card-list">
          {props.stampList.length !== 0 ? (
            props.stampList.map((stamp) => {
              return (
                <div className="col-md-6">
                  <div className="passport-full-view-festival-card">
                    <div className="passport-festival-card__left">
                      <div className="passport-festival-card__name">{stamp.title}</div>
                      <div className="passport-festival-card__misc-info">
                        <div className="passport-festival-card__location">{stamp.description}</div>
                      </div>
                    </div>
                    <div className="passport-festival-card__right">
                      <Link
                        exact="true"
                        to={`/festival/${stamp.festival_selected[0]}`}
                        className="festival-card-link"
                      >
                        <button className="passport-festival-card__view-button">View</button>
                      </Link>
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <span>You haven't collected any stamps yet!</span>
          )}
        </div>
      </div>
    </div>
  );
};

export default FullMyStamps;
