import React, { useState, useEffect, useContext } from 'react';

import './PastFestivals.scss';

const PastFestivals = (props) => {
  console.log("AAAAA", props);
  return (
    <div
      onClick={() => props.toggleFullView('pastFestivals')}
      className="passport-activities__card past-festivals-card"
    >
      <h3 className="passport-activities__card-title">Past Festivals</h3>
      <div className="passport-activites__card-count">{props.expiredFestivals.length}</div>
      <div className="passport-activities__card-festival-list">
        {props.expiredFestivals && props.expiredFestivals.length !== 0 ? (
          props.expiredFestivals.map((festival) => {
            return <span>{festival.festival_name}</span>;
          })
        ) : (
          <span>You haven't attended any festivals yet!</span>
        )}
      </div>
    </div>
  );
};

export default PastFestivals;
