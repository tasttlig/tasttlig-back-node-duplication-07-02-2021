import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';

import './PastFestivals.scss';

const FullPastFestivals = (props) => {
  const { formatDate } = props;

  return (
    <div className="passport-activities__full-card past-festivals-card">
      <h3 className="passport-activities__card-title">Past Festivals</h3>
      <div onClick={() => props.toggleFullView('')} className="passport-activites__full-card-exit">
        <i className="fas fa-times-circle"></i>
      </div>
      <div className="container">
        <div className="row passport-activities__full-card-list">
          {props.expiredFestivals && props.expiredFestivals.length !== 0 ? (
            props.expiredFestivals.map((festival) => {
              return (
                <div className="col-md-6">
                  <div className="passport-full-view-festival-card">
                    <div className="passport-festival-card__left">
                      <div className="passport-festival-card__name">{festival.festival_name}</div>
                      <div className="passport-festival-card__misc-info">
                        <div className="passport-festival-card__location">
                          <i className="fas fa-map-marker-alt"></i>
                          {festival.festival_city}
                        </div>
                        <div className="passport-festival-card__date">
                          <i className="fas fa-calendar"></i>
                          {`${formatDate(festival.festival_start_date)} -
                          ${formatDate(festival.festival_end_date)}`}
                        </div>
                      </div>
                    </div>
                    <div className="passport-festival-card__right">
                      <Link
                        exact="true"
                        to={`/ticket/${festival.ticket_id}`}
                        className="festival-card-link"
                      >
                        <button className="passport-festival-card__view-button">View</button>
                      </Link>
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <span>You haven't attended any festivals yet!</span>
          )}
        </div>
      </div>
    </div>
  );
};

export default FullPastFestivals;
