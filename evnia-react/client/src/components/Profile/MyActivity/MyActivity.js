// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import PastFestivals from './PastFestivals/PastFestivals';
import CurrentFestivals from './CurrentFestivals/CurrentFestivals';

// Styling
import './MyActivity.scss';

const MyActivity = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  return (
    <section className="passport-sections my-activity">
      <div className="container">
        <CurrentFestivals />
        <PastFestivals />
      </div>
    </section>
  );
};

export default MyActivity;
