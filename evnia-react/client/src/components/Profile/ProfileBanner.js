// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
// import { Link } from "react-router-dom";
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import FoodSampleOwnerItems from '../FoodSampleOwner/FoodSampleOwnerItems/FoodSampleOwnerItems';
import ExperiencesCard from '../Experiences/ExperiencesCard/ExperiencesCard';
import MenuItemCard from '../Discover/MenuItemCard';

// Styling
import '../FoodSampleOwner/FoodSampleOwner.scss';

const Profile = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [foodSampleOwnerItems, setFoodSampleOwnerItems] = useState([]);
  const [experienceOwnerItems, setExperienceOwnerItems] = useState([]);
  const [menuOwnerItems, setMenuOwnerItems] = useState([]);
  const [foodSamplesCount, setFoodSamplesCount] = useState(0);
  const [experienceCount, setExperienceCount] = useState(0);
  const [MenuItemsCount, setMenuItemsCount] = useState(0);
  const [activeTab, setActiveTab] = useState('food samples');
  const [loading, setLoading] = useState(false);
  const [hasExperienceNextPage, setHasExperienceNextPage] = useState(true);
  const [currentExperiencePage, setCurrentExperiencePage] = useState(0);
  const [hasMenuItemNextPage, setHasMenuItemNextPage] = useState(true);
  const [currentMenuItemPage, setCurrentMenuItemPage] = useState(0);
  const [hasFoodSamplesNextPage, setHasFoodSamplesNextPage] = useState(true);
  const [currentFoodSamplesPage, setCurrentFoodSamplesPage] = useState(0);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Render Food Sample Owner Items helper function
  const renderFoodSampleOwnerItems = (arr) => {
    return arr.map((card, index) => (
      <FoodSampleOwnerItems
        key={index}
        passportId={card.food_sample_id}
        images={card.image_urls}
        title={card.title}
        nationality={card.nationality}
        alpha_2_code={card.alpha_2_code}
        frequency={card.frequency}
        price={card.price}
        quantity={card.quantity}
        numOfClaims={card.num_of_claims}
        address={card.address}
        city={card.city}
        provinceTerritory={card.state}
        postalCode={card.postal_code}
        startDate={card.start_date}
        endDate={card.end_date}
        startTime={card.start_time}
        endTime={card.end_time}
        description={card.description}
        sample_size={card.sample_size}
        is_available_on_monday={card.is_available_on_monday}
        is_available_on_tuesday={card.is_available_on_tuesday}
        is_available_on_wednesday={card.is_available_on_wednesday}
        is_available_on_thursday={card.is_available_on_thursday}
        is_available_on_friday={card.is_available_on_friday}
        is_available_on_saturday={card.is_available_on_saturday}
        is_available_on_sunday={card.is_available_on_sunday}
        foodSampleOwnerId={card.food_sample_creater_user_id}
        foodSampleOwnerPicture={appContext.state.user.profile_image_link}
        isEmailVerified={appContext.state.user.is_email_verified}
        firstName={appContext.state.user.first_name}
        lastName={appContext.state.user.last_name}
        email={appContext.state.user.email}
        phoneNumber={appContext.state.user.phone_number}
        facebookLink={appContext.state.user.facebook_link}
        twitterLink={appContext.state.user.twitter_link}
        instagramLink={appContext.state.user.instagram_link}
        youtubeLink={appContext.state.user.youtube_link}
        linkedinLink={appContext.state.user.linkedin_link}
        websiteLink={appContext.state.user.website_link}
        bioText={appContext.state.user.bio_text}
        disableButton={true}
        history={props.history}
      />
    ));
  };

  // Render Experiences Items helper function
  const renderExperienceCards = (arr) => {
    return arr.map((card, index) => (
      <ExperiencesCard key={card.experience_id} experiencesCard={card} isFullPageView="true" />
    ));
  };

  // Render Menu Items Items helper function
  const renderMenuItemsCards = (arr) => {
    return arr.map((card, index) => (
      <MenuItemCard key={card.menu_item_id} {...card} isFullPageView="true" />
    ));
  };

  // Load next set of user experiences helper functions
  const loadExperienceNextPage = async (page) => {
    const url = `/experience/owner/${props.match.params.id}`;

    return axios({
      method: 'GET',
      url,
      params: {
        page: page + 1,
      },
    });
  };

  // Load next set of user food samples helper functions
  const loadFoodSamplesNextPage = async (page) => {
    const url = `/food-sample/owner/${props.match.params.id}`;

    return axios({
      method: 'GET',
      url,
      params: {
        page: page + 1,
      },
    });
  };

  // Load next set of user food samples helper functions
  const loadMenuItemsNextPage = async (page) => {
    const url = `/menu_items/owner/${props.match.params.id}`;

    return axios({
      method: 'GET',
      url,
      params: {
        page: page + 1,
      },
    });
  };

  const handleExperienceLoadMore = (
    page = currentExperiencePage,
    experiences = experienceOwnerItems,
  ) => {
    setLoading(true);
    loadExperienceNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentExperiencePage(page + 1);
      }

      setHasExperienceNextPage(currentExperiencePage < pagination.lastPage);
      setExperienceOwnerItems(experiences.concat(newPage.data.details.data));
    });
  };

  const handleFoodSamplesLoadMore = (
    page = currentFoodSamplesPage,
    foodSamples = foodSampleOwnerItems,
  ) => {
    setLoading(true);
    loadFoodSamplesNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.food_samples.pagination;

      if (page < pagination.lastPage) {
        setCurrentFoodSamplesPage(page + 1);
      }

      setHasFoodSamplesNextPage(currentFoodSamplesPage < pagination.lastPage);
      setFoodSampleOwnerItems(foodSamples.concat(newPage.data.food_samples.data));
    });
  };

  const handleMenuItemsLoadMore = (page = currentMenuItemPage, menuItems = menuOwnerItems) => {
    setLoading(true);
    loadMenuItemsNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.menu_items.pagination;

      if (page < pagination.lastPage) {
        setCurrentMenuItemPage(page + 1);
      }

      setHasMenuItemNextPage(currentMenuItemPage < pagination.lastPage);
      setMenuOwnerItems(menuItems.concat(newPage.data.menu_items.data));
    });
  };

  const infiniteExperienceRef = useInfiniteScroll({
    loading,
    hasExperienceNextPage,
    onLoadMore: handleExperienceLoadMore,
  });

  const infiniteFoodSamplesRef = useInfiniteScroll({
    loading,
    hasFoodSamplesNextPage,
    onLoadMore: handleFoodSamplesLoadMore,
  });

  const infiniteMenuItemRef = useInfiniteScroll({
    loading,
    hasMenuItemNextPage,
    onLoadMore: handleMenuItemsLoadMore,
  });

  // Profile Banner Component
  const Banner = () => (
    <div>
      <div className="food-sample-owner-profile-banner">
        <div className="row food-sample-owner-profile-banner-simple">
          <div className="col-lg-5 banner-content">
            <div className="mb-4">
              {appContext.state.user.profile_image_link ? (
                <img
                  src={appContext.state.user.profile_image_link}
                  alt={`${appContext.state.user.first_name} ${appContext.state.user.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'food-sample-owner-profile-picture' : 'loading-image'}
                />
              ) : (
                <span className="fas fa-user-circle fa-5x food-sample-owner-default-picture"></span>
              )}
            </div>
            <div className="food-sample-owner-profile-name">
              {`${appContext.state.user.first_name} ${appContext.state.user.last_name}`}
            </div>
            <div className="food-sample-owner-profile-promo">
              {appContext.state.user.profile_tag_line && (
                <div className="pro mb-4 food-sample-owner-profile-tag-line">
                  {appContext.state.user.profile_tag_line}
                </div>
              )}
              <address>
                {appContext.state.user.business_name && (
                  <div>
                    <strong>{appContext.state.user.business_name}</strong>
                    <br />
                  </div>
                )}
                {appContext.state.user.address_line_1 && appContext.state.user.address_line_1}
                {appContext.state.user.address_line_2 && (
                  <span>, {appContext.state.user.address_line_2}</span>
                )}
                <br />
                {appContext.state.user.city && `${appContext.state.user.city},`}{' '}
                {appContext.state.user.state && appContext.state.user.state}{' '}
                {appContext.state.user.postal_code && appContext.state.user.postal_code}
                <br />
                <div
                  id="profile-email"
                  className="btn btn-primary p-2"
                  title="Email"
                  onClick={() => {
                    window.location.href = `mailto${appContext.state.user.email}`;
                  }}
                >
                  <span className="far fa-envelope p-2" />
                  {appContext.state.user.email}
                </div>
                {/* <div title="Phone">P: {appContext.state.user.phone_number}</div> */}
              </address>
            </div>
            {/* <div>
              <Link to="/account" className="edit-profile-link">
                Edit Profile
              </Link>
            </div> */}
          </div>
          <div className="col-lg-7 banner-content">
            <div className="banner-images">
              <div className="banner-images-wrapper">
                <div className="banner-color-block-container">
                  <div className="banner-color-block"></div>
                </div>
                {appContext.state.user.banner_image_link ? (
                  <img
                    src={appContext.state.user.banner_image_link}
                    alt="Banner"
                    onLoad={() => setLoad(true)}
                    className={load ? 'main-banner-image' : 'loading-image'}
                  />
                ) : (
                  <div className="main-banner-image"></div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="fade-rule" />
    </div>
  );

  return (
    <div className="food-sample-owner">
      <Banner />
    </div>
  );
};

export default Profile;
