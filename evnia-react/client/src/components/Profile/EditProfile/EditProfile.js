// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import Nav from '../../Navbar/Nav';
import EditProfileSidebar from '../EditProfileSidebar/EditProfileSidebar';
import ProfileForm from './ProfileForm/ProfileForm';

// Styling
import '../EditAccount/EditAccount.scss';

const EditProfile = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Mount Edit Profile page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Edit Profile page
  return (
    <div className="edit-profile">
      <Nav />

      <div className="mt-5">
        <div className="edit-profile-title">
          <span>
            <Link
              to={`/profile/${appContext.state.user.id}`}
              className="profile-link"
            >{`${appContext.state.user.first_name} ${appContext.state.user.last_name}`}</Link>
          </span>
          <span className="separate">/</span>
          <span>Edit Profile</span>
        </div>

        <div className="row">
          <div className="col-lg-3 profile-form">
            <EditProfileSidebar />
          </div>
          <div className="col-lg-9 profile-form">
            <ProfileForm user={appContext.state.user} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditProfile;
