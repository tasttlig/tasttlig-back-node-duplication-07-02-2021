// Libraries
import React, { Component } from 'react';
import axios from 'axios';

// Components
import ProfileOrdersCard from './ProfileOrdersCard/ProfileOrdersCard';

// Styling
import './ProfileOrders.scss';

export default class ProfileOrders extends Component {
  // Profile Orders constructor
  constructor(props) {
    super(props);

    this.state = {
      orderItems: [],
    };
  }

  url = '/purchase/user';

  access_token = localStorage.getItem('access_token');

  headers = {
    Authorization: `Bearer ${this.access_token}`,
  };

  // Render profile orders helper function
  renderOrders = (arr) => {
    return arr.map((card, index) => (
      <ProfileOrdersCard
        key={index}
        quantity={card.quantity}
        description={card.description}
        orderCode={card.order_code}
        cost={card.cost}
        createdAt={card.created_at}
        receiptUrl={card.receipt_url}
        accept={card.accept}
        rejectNote={card.reject_note}
      />
    ));
  };

  // Mount profile orders
  componentDidMount = () => {
    axios({ method: 'GET', url: this.url, headers: this.headers })
      .then((response) => {
        this.setState({
          orderItems: [...this.state.orderItems, ...response.data.purchases],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render Profile Orders modal
  render = () => {
    const { orderItems } = this.state;

    return (
      <div>
        <div className="your-orders">Your Orders</div>
        {orderItems.length === 0 ? (
          <div className="your-orders-list">None.</div>
        ) : (
          this.renderOrders(orderItems.reverse())
        )}
      </div>
    );
  };
}
