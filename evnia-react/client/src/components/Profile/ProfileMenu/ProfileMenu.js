// Libraries
import React, { Component } from 'react';
import axios from 'axios';

// Components
import ProfileMenuCard from './ProfileMenuCard/ProfileMenuCard';

// Styling
import './ProfileMenu.scss';

export default class ProfileMenu extends Component {
  // Profile Menu constructor
  constructor(props) {
    super(props);

    this.state = {
      menuPublishedItems: [],
    };
  }

  url = '/api/experiences';

  access_token = localStorage.getItem('access_token');

  headers = {
    Authorization: `Bearer ${this.access_token}`,
  };

  // Render profile menu published helper function
  renderMenuPublished = (arr) => {
    return arr.map((card, index) => (
      <ProfileMenuCard
        key={index}
        image={card.Experience_img_url}
        name={card.name}
        ExperienceEthnicity={card.Experience_ethnicity}
        price={card.price}
        ExperienceCode={card.Experience_code}
        createdAt={card.created_at}
      />
    ));
  };

  // Mount profile menu published
  componentDidMount = () => {
    axios({ method: 'GET', url: this.url, headers: this.headers })
      .then((response) => {
        this.setState({
          menuPublishedItems: [...this.state.menuPublishedItems, ...response.data.Experiences],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render Profile Menu modal
  render = () => {
    const { menuPublishedItems } = this.state;

    return (
      <div>
        <div className="your-menu-published">Your Menu Published</div>
        {menuPublishedItems.length === 0 ? (
          <div className="your-menu-published-list">
            You can publish menu if you have a valid Experience Handler Certificate and access to
            Commercial Kitchen.
          </div>
        ) : (
          this.renderMenuPublished(menuPublishedItems.reverse())
        )}
      </div>
    );
  };
}
