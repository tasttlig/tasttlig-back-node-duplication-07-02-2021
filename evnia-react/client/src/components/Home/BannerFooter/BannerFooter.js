// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

// Styling
import facebook from '../../../assets/images/facebook.png';
import twitter from '../../../assets/images/twitter.png';
import instagram from '../../../assets/images/instagram.png';
import linkedin from '../../../assets/images/linkedin.png';
import youtube from '../../../assets/images/youtube.png';

const BannerFooter = () => {
  return (
    <div className="banner-footer">
      <div className="row">
        <div className="col-md-6 col-xl-3 banner-footer-col">
          <div className="banner-footer-title">Contact</div>
          <div className="row">
            <div className="col-1 pr-2 pl-0">
              <span className="fas fa-map-marker-alt banner-footer-icon" />
            </div>
            <div className="col-11 px-0">
              <a
                href="https://www.google.com/maps/place/Tasttlig/@43.6599131,-79.3641913,17z/data=!3m1!4b1!4m5!3m4!1s0x89d4cbb168337463:0xc48ec33e02ee9ed0!8m2!3d43.6599092!4d-79.3620026"
                target="_blank"
                rel="noopener noreferrer"
                className="banner-footer-link"
              >
                585 Dundas Street East, 3rd Floor, Toronto, Ontario M5A 2B7
              </a>
            </div>
          </div>
          <div className="row">
            <div className="col-1 pr-2 pl-0">
              <span className="fas fa-envelope banner-footer-icon" />
            </div>
            <div className="col-11 px-0">
              <a href="mailto:hello@tasttlig.com" className="banner-footer-link">
                hello@tasttlig.com
              </a>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-xl-3 banner-footer-col">
          <div className="banner-footer-title">Products</div>
          <div className="banner-footer-coming-soon">Coming Soon</div>
        </div>
        <div className="col-md-6 col-xl-3 banner-footer-col">
          <div className="banner-footer-title">Services</div>
          <div className="banner-footer-coming-soon">Coming Soon</div>
        </div>
        <div className="col-md-6 col-xl-3 banner-footer-col">
          <div className="banner-footer-title">Follow Us</div>
          <a
            href="https://www.facebook.com/tasttlig"
            target="_blank"
            rel="noopener noreferrer"
            className="pl-0 banner-footer-social-media-link"
          >
            <img src={facebook} alt="Facebook" />
          </a>
          <a
            href="https://www.twitter.com/tasttlig1"
            target="_blank"
            rel="noopener noreferrer"
            className="banner-footer-social-media-link"
          >
            <img src={twitter} alt="Twitter" />
          </a>
          <a
            href="https://www.instagram.com/tasttlig"
            target="_blank"
            rel="noopener noreferrer"
            className="banner-footer-social-media-link"
          >
            <img src={instagram} alt="Instagram" />
          </a>
          <a
            href="https://www.linkedin.com/company/tasttlig/about"
            target="_blank"
            rel="noopener noreferrer"
            className="banner-footer-social-media-link"
          >
            <img src={linkedin} alt="LinkedIn" />
          </a>
          <a
            href="https://www.youtube.com/channel/UCMlJcL7dEwAkGBbMvgbjYVQ"
            target="_blank"
            rel="noopener noreferrer"
            className="pr-0 banner-footer-social-media-link"
          >
            <img src={youtube} alt="YouTube" />
          </a>
        </div>
      </div>

      <div className="row">
        <div className="col-xl-9 col-md-6 banner-footer-col">
          <div className="copyright">&copy; {new Date().getFullYear()} Tasttlig Corporation</div>
        </div>
        {/* <div className="col-xl-3 col-md-6 banner-footer-col">
          <div className="privacy-and-terms">
            <Link exact="true" to="/privacy" className="privacy">
              Privacy
            </Link>
            <span className="pr-4"></span>
            <Link exact="true" to="/terms" className="terms">
              Terms
            </Link>
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default BannerFooter;
