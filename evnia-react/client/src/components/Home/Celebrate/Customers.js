// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import celebrateCustomers from '../../../assets/images/celebrate-customers.jpg';

const Customers = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Render Celebrate Customers component
  return (
    <div className="landing-page-customers-section">
      <div className="row">
        <div className="col-lg-6 landing-page-customers-content">
          <img
            src={celebrateCustomers}
            alt="Celebrate To Attract More Customers"
            className="landing-page-customers-image border shadow"
          />
        </div>
        <div className="col-lg-6 landing-page-customers-content">
          <div className="landing-page-sub-title-right">Celebrate To Attract More Customers</div>
          {!appContext.state.user.id ||
          (userRole &&
            !userRole.includes('RESTAURANT') &&
            !userRole.includes('RESTAURANT_PENDING')) ? (
            <div className="landing-page-btn-right-content">
              <Link exact="true" to="/add-restaurant" className="landing-page-add-restaurant-btn">
                Add Your Restaurant
              </Link>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Customers;
