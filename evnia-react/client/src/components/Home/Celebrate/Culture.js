// Libraries
import React, { useContext } from 'react';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import celebrateCulture from '../../../assets/images/celebrate-culture.jpg';

const Culture = () => {
  // To use the JWT credentials
  const authModalContext = useContext(AuthModalContext);
  const appContext = useContext(AppContext);

  // Render Celebrate Culture component
  return (
    <div className="landing-page-culture-section">
      <div className="row flex-column-reverse flex-lg-row">
        <div className="col-lg-6 landing-page-culture-content">
          <div className="landing-page-sub-title-left">Celebrate Every Culture With Us</div>
          {!appContext.state.user.id && (
            <div className="landing-page-btn-left-content">
              <span
                onClick={authModalContext.openModal('sign-up')}
                className="landing-page-btn-left"
              >
                Join
              </span>
            </div>
          )}
        </div>
        <div className="col-lg-6 landing-page-culture-content">
          <img
            src={celebrateCulture}
            alt="Celebrate Every Culture With Us"
            className="landing-page-culture-image border shadow"
          />
        </div>
      </div>
    </div>
  );
};

export default Culture;
