// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

// Styling
import celebrateCreate from '../../../assets/images/celebrate-create.jpg';

const Create = () => {
  // Render Celebrate Create component
  return (
    <div className="landing-page-create-section">
      <div className="row">
        <div className="col-lg-6 landing-page-create-content">
          <img
            src={celebrateCreate}
            alt="Create Your Own Celebrations"
            className="landing-page-create-image border shadow"
          />
        </div>
        <div className="col-lg-6 landing-page-create-content">
          <div className="landing-page-sub-title-right">Create Your Own Celebrations</div>
          <div className="landing-page-btn-right-content">
            <Link exact="true" to="/host" className="landing-page-host-btn">
              Host
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Create;
