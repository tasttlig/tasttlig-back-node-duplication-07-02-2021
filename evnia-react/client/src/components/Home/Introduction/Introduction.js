// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

const Introduction = () => {
  // Render Landing Page Introduction component
  return (
    <div className="banner-slide festival-banner-toronto">
      <div className="festival-banner-value-proposition">
        <div>Showcasing the world in the best light</div>
        <div className="festival-banner-statement">
          Taste Every Culture At Our Free Food Festival
        </div>
        <Link exact="true" to="/festival" className="landing-page-get-started-btn">
          Get Started
        </Link>
      </div>
      <span
        onClick={() => window.scrollTo({ top: window.innerHeight, behavior: 'smooth' })}
        className="fas fa-chevron-down fa-3x festival-banner-down-arrow"
      ></span>
    </div>
  );
};

export default Introduction;
