// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

const Faq = () => {
  return (
    <div>
      <button type="button" data-toggle="tooltip" data-placement="left" title="FAQ" className="faq">
        <Link exact="true" to="/faq" className="faq">
          <span className="fas fa-2x fa-lightbulb"></span>
        </Link>
      </button>
    </div>
  );
};

export default Faq;
