// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

// Styling
import '../Restaurants/Restaurants.scss';

const FeaturedEvent = () => {
  return (
    <div>
      <div className="restaurants">
        <div className="restaurants-title">Experience Argentina</div>
        <div>
          <a href="https://www.eventbrite.ca/e/experience-argentina-tickets-119369482335">
            <img
              src={require('../../../assets/images/argentina_experience.jpg')}
              alt="argentina_experience"
            />
          </a>
        </div>
        <div className="text-center mt-5">
          <Link to="/payment/experience/4">
            <div className="btn btn-primary">Buy Now</div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default FeaturedEvent;
