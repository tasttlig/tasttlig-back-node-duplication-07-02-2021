// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

const Join = () => {
  return (
    <div className="join">
      <div className="join-title">Experience The World On Tasttlig</div>
      <Link to="/join" className="join-btn">
        Join
      </Link>
    </div>
  );
};

export default Join;
