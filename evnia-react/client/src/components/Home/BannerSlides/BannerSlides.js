// Libraries
import React from 'react';
import { Slide } from 'react-slideshow-image';

// Components
import MainBannerSlide from './MainBannerSlide/MainBannerSlide';
import DiscoverBannerSlide from './DiscoverBannerSlide/DiscoverBannerSlide';
import ExperienceBannerSlide from './ExperienceBannerSlide/ExperienceBannerSlide';
import PassportBannerSlide from './PassportBannerSlide/PassportBannerSlide';

const slideImages = [
  <MainBannerSlide />,
  <DiscoverBannerSlide />,
  <ExperienceBannerSlide />,
  <PassportBannerSlide />,
];

const properties = {
  transitionDuration: 500,
  arrows: true,
  autoplay: false,
};

const BannerSlides = () => {
  // Render Banner Slides Component
  return (
    <div>
      <Slide {...properties}>
        <div>{slideImages[0]}</div>
        <div>{slideImages[1]}</div>
        <div>{slideImages[2]}</div>
        <div>{slideImages[3]}</div>
      </Slide>
    </div>
  );
};

export default BannerSlides;
