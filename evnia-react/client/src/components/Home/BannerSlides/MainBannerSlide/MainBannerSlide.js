// Libraries
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import ModalVideo from 'react-modal-video';

// Styling
import '../../../../../node_modules/react-modal-video/scss/modal-video.scss';

const MainBannerSlide = () => {
  // Set initial state
  const [isVideoOpen, setIsVideoOpen] = useState(false);

  // Render Main Banner Slide
  return (
    <div className="banner-slide main-banner-image">
      <ModalVideo
        channel="youtube"
        isOpen={isVideoOpen}
        videoId="GYmmxMF5mUQ"
        onClose={() => setIsVideoOpen(false)}
      />

      <div className="value-proposition">
        <div className="value-proposition-statement">
          Showcasing<br></br>The World In The Best Light
        </div>
        <div className="row">
          <div className="col-sm-6 watch-video-btn-content">
            <Link onClick={() => setIsVideoOpen(true)} to="#" className="watch-video-btn">
              Watch Video
            </Link>
          </div>
          <div className="col-sm-6 learn-about-our-mission-btn-content">
            <Link to="/about" className="learn-about-our-mission-btn">
              Learn About Our Mission
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainBannerSlide;
