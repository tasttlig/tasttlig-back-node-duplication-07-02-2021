// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

const ExperienceBannerSlide = () => {
  // Render Experience Banner Slide
  return (
    <div className="banner-slide experience-banner-image">
      <div className="banner-section">
        <div className="banner-section-statement">
          Experience<br></br>The World With Food, Music, Arts And Games
        </div>
        {/* <div className="row">
          <div className="col-sm-12 taste-btn-content">
            <Link to="/experiences" className="taste-btn">
              Taste
            </Link>
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default ExperienceBannerSlide;
