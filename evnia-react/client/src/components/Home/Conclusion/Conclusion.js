// Libraries
import React, { useContext } from 'react';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';

const Conclusion = () => {
  // To use the JWT credentials
  const authModalContext = useContext(AuthModalContext);
  const appContext = useContext(AppContext);

  // Render Landing Page Conclusion component
  return (
    <div className="landing-page-conclusion-section">
      <div className="landing-page-sub-title-center">Showcase The World In The Best Light</div>
      {!appContext.state.user.id && (
        <div
          onClick={authModalContext.openModal('sign-up')}
          className="mx-auto landing-page-btn-center"
        >
          Join Our Celebrations
        </div>
      )}
    </div>
  );
};

export default Conclusion;
