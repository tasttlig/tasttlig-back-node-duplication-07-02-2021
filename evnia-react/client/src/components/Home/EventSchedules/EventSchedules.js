// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { scroller } from 'react-scroll';

// Styling
import '../Home.scss';
import JoinNow from '../Banner/JoinNow';

const EventSchedules = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Open schedule tab section helper function
  const openTabSection = (evt, tabName) => {
    let i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName('tabs_item');
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none';
    }

    tablinks = document.getElementsByTagName('li');
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace('current', '');
    }

    document.getElementById(tabName).style.display = 'block';
    evt.currentTarget.className += 'current';
  };

  // Render Schedules Component page
  return (
    <section className="schedule-area ptb-120">
      <div className="container">
        <div className="section-title">
          <span>Schedule Plan</span>
          <h2>
            Information of Experience
            <br /> Schedules
          </h2>

          <JoinNow text={'Be Our Guest'} color={'secondary'} />
          {/*{appContext.state.signedInStatus ? (*/}
          {/*  <div*/}
          {/*    onClick={authModalContext.handleSubmitTasttligFestivalGuest}*/}
          {/*    disabled={authModalContext.state.submitAuthDisabled}*/}
          {/*    className="btn btn-primary rsvp"*/}
          {/*  >*/}
          {/*    Join*/}
          {/*  </div>*/}
          {/*) : (*/}
          {/*  <div*/}
          {/*    onClick={authModalContext.openModal("sign-up")}*/}
          {/*    className="btn btn-primary join-now"*/}
          {/*  >*/}
          {/*    Host Now!*/}
          {/*  </div>*/}
          {/*)}*/}

          <div className="bar"></div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className="tab">
              <ul className="tabs active">
                {/*<li*/}
                {/*  onClick={e => openTabSection(e, "tab1")}*/}
                {/*  className="current"*/}
                {/*>*/}
                {/*  <Link to="#">June</Link>*/}
                {/*</li>*/}

                {/*<li onClick={e => openTabSection(e, "tab2")}>*/}
                {/*  <Link to="#">July</Link>*/}
                {/*</li>*/}

                <li onClick={(e) => openTabSection(e, 'tab3')}>
                  <Link to="#">August</Link>
                </li>

                <li onClick={(e) => openTabSection(e, 'tab4')}>
                  <Link to="#">September</Link>
                </li>
              </ul>

              <div className="tab_content">
                {/*  <div id="tab1" className="tabs_item">*/}
                {/*    <ul className="accordion">*/}
                {/*      <li className="accordion-item">*/}
                {/*        <Link className="accordion-title" to="#">*/}
                {/*          <div className="schedule-info">*/}
                {/*            <h3>App Release</h3>*/}

                {/*            <ul>*/}
                {/*              <li>*/}
                {/*                Open the app for the restaurants to register for*/}
                {/*                the event.*/}
                {/*              </li>*/}
                {/*            </ul>*/}
                {/*          </div>*/}
                {/*        </Link>*/}
                {/*      </li>*/}
                {/*    </ul>*/}
                {/*  </div>*/}

                {/*  <div id="tab2" className="tabs_item">*/}
                {/*    <ul className="accordion">*/}
                {/*      <li className="accordion-item">*/}
                {/*        <Link className="accordion-title" to="#">*/}
                {/*          <div className="schedule-info">*/}
                {/*            <h3>Selection Begin</h3>*/}

                {/*            <ul>*/}
                {/*              <li>*/}
                {/*                The selection for the restaurants begin for the*/}
                {/*                event.*/}
                {/*              </li>*/}
                {/*            </ul>*/}
                {/*          </div>*/}
                {/*        </Link>*/}
                {/*      </li>*/}
                {/*    </ul>*/}
                {/*  </div>*/}

                <div id="tab3" className="tabs_item">
                  <ul className="accordion">
                    <li className="accordion-item">
                      <Link className="accordion-title" to="#">
                        <div className="schedule-info">
                          <h3>Applications</h3>

                          <ul>
                            <li>Restaurant Apply to host their food at the festival.</li>
                          </ul>
                        </div>
                      </Link>
                    </li>
                  </ul>
                </div>

                <div id="tab4" className="tabs_item">
                  <ul className="accordion">
                    <li className="accordion-item">
                      <Link className="accordion-title" to="#">
                        <div className="schedule-info">
                          <h3>Let's Go</h3>

                          <ul>
                            <li>Month Long celebration of food from around the world.</li>
                          </ul>
                        </div>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-12">
            <div className="btn-box">
              {/* <Link to="#" className="btn btn-primary">Download Passport</Link> */}
              <div className="btn btn-secondary">
                <a
                  className="tasttlig-instagram"
                  href="https://www.instagram.com/tasttlig"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Follow on Instagram
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="shape1">
        <img src={require('../../../assets/images/shapes/1.png')} alt="shape1" />
      </div>
      <div className="shape2 rotateme">
        <img src={require('../../../assets/images/shapes/2.png')} alt="shape2" />
      </div>
      <div className="shape3 rotateme">
        <img src={require('../../../assets/images/shapes/3.png')} alt="shape3" />
      </div>
      <div className="shape4">
        <img src={require('../../../assets/images/shapes/4.png')} alt="shape4" />
      </div>
    </section>
  );
};

export default EventSchedules;
