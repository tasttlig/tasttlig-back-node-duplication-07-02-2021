// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Styling
import './FreeFoodSamples.scss';
import '../Restaurants/Restaurants.scss';
import '../FeaturedFoodSamples/FeaturedFoodSamples.scss';

const FreeFoodSamples = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  return (
    <div>
      {!appContext.state.signedInStatus && (
        <div className="free-food-samples">
          <div className="free-food-samples-title">Grow your Business</div>
          <div className="text-center mb-4">Reach more people</div>
          <Link to="/host" className="btn btn-secondary get-a-passport-btn">
            Join
          </Link>
        </div>
      )}
    </div>
  );
};

export default FreeFoodSamples;
