// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

// Styling
import './HowItWorks.scss';

// Render How it Works Component
const HowItWorks = () => {
  return (
    <div className="how-it-works">
      <div className="how-it-works-title">How it Works</div>

      <div className="how-it-works-section">
        <div className="how-it-works-timeline"></div>
        <div className="row">
          <div className="col text-right how-it-works-content">
            <div className="fas fa-7x fa-ticket-alt"></div>
            <span className="how-it-works-timeline-circle-right"></span>
          </div>
          <div className="col text-left how-it-works-content">
            <div className="how-it-works-text">Explore the Experiences</div>
          </div>
        </div>

        <div className="row">
          <div className="col text-right how-it-works-content">
            <div className="how-it-works-text">Make Reservations</div>
          </div>
          <div className="col text-left how-it-works-content">
            <span className="how-it-works-timeline-circle-left"></span>
            <div className="fas fa-7x fa-compass"></div>
          </div>
        </div>

        <div className="row">
          <div className="col text-right how-it-works-content">
            <div className="fas fa-7x fa-check-circle"></div>
            <span className="how-it-works-timeline-circle-right"></span>
          </div>
          <div className="col text-left how-it-works-content">
            <div className="how-it-works-text">Experience The World Locally</div>
          </div>
        </div>
      </div>

      {/* Responsive Design */}
      <div className="how-it-works-responsive">
        <div className="mb-5">
          <div className="fas fa-7x fa-ticket-alt"></div>
          <div className="how-it-works-text">Explore the Experiences</div>
        </div>

        <div className="mb-5">
          <div className="fas fa-7x fa-compass"></div>
          <div className="how-it-works-text">Make Reservations</div>
        </div>

        <div className="mb-5">
          <div className="fas fa-7x fa-check-circle"></div>
          <div className="how-it-works-text">Experience The World Locally</div>
        </div>
      </div>

      <Link to="/experiences" className="btn btn-primary buy-a-festival-pass-btn">
        Explore The Experiences
      </Link>
    </div>
  );
};

export default HowItWorks;
