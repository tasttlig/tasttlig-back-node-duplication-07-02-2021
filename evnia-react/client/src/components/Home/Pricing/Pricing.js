// Libraries
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import lax from 'lax.js';

// Components
import LaxDiv from '../../Shared/LaxDiv';

// Styling
import '../Home.scss';

export default class Pricing extends Component {
  constructor(props) {
    super(props);
    lax.setup();

    document.addEventListener(
      'scroll',
      function (x) {
        lax.update(window.scrollY);
      },
      false,
    );

    lax.update(window.scrollY);
  }

  // Render Pricing Component page
  render = () => {
    return (
      <section className="pricing-area ptb-120 bg-image">
        <div className="container">
          <div className="section-title">
            <span>Pricing Plan</span>
            <h2>
              Get Your <b>Tickets</b>
            </h2>
            <span>Get Points with Every purchase</span>

            <LaxDiv text="Pricing" dataPreset="driftLeft" />

            <div className="bar"></div>
          </div>

          <div className="row">
            <div className="col-lg-4 col-md-6">
              <div className="pricing-table-box">
                <div className="pricingTable-header">
                  <h3 className="title">1 Experience</h3>
                  <div className="price-value">
                    <sup>$</sup>25
                  </div>
                </div>

                <ul className="pricing-content">
                  <li>You can go and eat 1 experience</li>
                  <li>Get 25 Points</li>
                </ul>

                <Link to="#" className="btn btn-primary">
                  Buy Ticket Now
                </Link>
              </div>
            </div>
            <div className="col-lg-4 col-md-6">
              <div className="pricing-table-box">
                <div className="pricingTable-header">
                  <h3 className="title">10 Experiences</h3>
                  <div className="price-value">
                    <sup>$</sup>250
                  </div>
                </div>

                <ul className="pricing-content">
                  <li>You can go and eat 10 experiences</li>
                  <li>Get 300 Points</li>
                </ul>

                <Link to="#" className="btn btn-primary">
                  Buy Ticket Now
                </Link>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div className="pricing-table-box">
                <div className="pricingTable-header">
                  <h3 className="title">50 Experiences</h3>
                  <div className="price-value">
                    <sup>$</sup>1250
                  </div>
                </div>

                <ul className="pricing-content">
                  <li>You can go and eat 50 experiences</li>
                  <li>Get 1500 Points</li>
                </ul>

                <Link to="#" className="btn btn-primary">
                  Buy Ticket Now
                </Link>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
              <div className="pricing-table-box">
                <div className="pricingTable-header">
                  <h3 className="title">100 Experiences</h3>
                  <div className="price-value">
                    <sup>$</sup>2500
                  </div>
                </div>

                <ul className="pricing-content">
                  <li>You can go and eat 100 experiences</li>
                  <li>Get 3000 Points</li>
                </ul>

                <Link to="#" className="btn btn-primary">
                  Buy Ticket Now
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  };
}
