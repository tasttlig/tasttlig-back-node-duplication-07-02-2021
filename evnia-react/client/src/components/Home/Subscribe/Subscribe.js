// Libraries
import React, { useContext } from 'react';
import Modal from 'react-modal';
import { toast } from 'react-toastify';
import { Element } from 'react-scroll';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Styling
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

// Render Be Our Guest Modal
const Subscribe = () => {
  const authModalContext = useContext(AuthModalContext);

  return (
    <Modal
      isOpen={authModalContext.state.beOurGuestOpened}
      onRequestClose={authModalContext.closeModal('be-our-guest')}
      ariaHideApp={false}
      className="be-our-guest-modal"
    >
      <section className="subscribe-area">
        <Element name="subscribe_section">
          <div>
            <div>
              <span
                onClick={authModalContext.closeModal('be-our-guest')}
                disabled={authModalContext.state.submitAuthDisabled}
                className="fas fa-times fa-2x close-modal"
              ></span>
            </div>
            <div className="subscribe-inner">
              <span>Be Our Guest</span>
              <h2>Your Adventure begins now</h2>

              <form
                className="newsletter-form"
                data-toggle="validator"
                onSubmit={authModalContext.handleSubmitBeOurGuest}
                noValidate
              >
                <input
                  type="email"
                  name="beOurGuestEmail"
                  placeholder="Enter your email address"
                  value={authModalContext.state.beOurGuestEmail}
                  onChange={authModalContext.handleChange}
                  disabled={authModalContext.state.submitAuthDisabled}
                  className="form-control"
                  required
                />
                <span className="far fa-envelope input-icon"></span>
                {authModalContext.state.beOurGuestEmailError && (
                  <div className="error-message">{authModalContext.state.beOurGuestEmailError}</div>
                )}
                <button className="btn btn-primary" type="submit">
                  Be Our Guest
                </button>

                <div id="validator-newsletter" className="form-result"></div>
              </form>
            </div>
          </div>
        </Element>
      </section>
    </Modal>
  );
};

export default Subscribe;
