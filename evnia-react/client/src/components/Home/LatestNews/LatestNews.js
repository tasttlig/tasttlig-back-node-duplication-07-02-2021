// Libraries
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel3';

// Styling
import '../Home.scss';

const options = {
  loop: true,
  nav: false,
  dots: true,
  autoplayHoverPause: true,
  autoplay: true,
  navText: ["<i class='icofont-rounded-left'></i>", "<i class='icofont-rounded-right'></i>"],
  responsive: {
    0: {
      items: 1,
    },
    768: {
      items: 2,
    },
    1200: {
      items: 3,
    },
  },
};

export default class LatestNews extends Component {
  // Render Latest News component page
  render = () => {
    return (
      <section className="blog-area ptb-120 bg-image">
        <div className="container">
          <div className="section-title">
            <span>Info Update!</span>
            <h2>Our Previous Experiences</h2>

            {/*<Link to="#" className="btn btn-primary">*/}
            {/*  View Previous Experiences*/}
            {/*</Link>*/}

            <div className="bar"></div>
          </div>

          <div className="row">
            <OwlCarousel className="blog-slides owl-carousel owl-theme" {...options}>
              <div className="col-lg-18 col-md-12">
                <div className="single-blog-post">
                  <div className="blog-image">
                    <Link to="#">
                      <img src={require('../../../assets/images/Chroma.jpeg')} alt="blog" />
                    </Link>
                  </div>

                  <div className="blog-post-content">
                    <h3>Kanya's Kachumbari with Choma</h3>
                    <p>
                      Kachumbari is an East African vegetable dish. It comprises of tomatoes,
                      onions, celery, lemon or lime juice, and chili pepper to make a salsa-like
                      mixture. It is traditionally served with Nyama Choma and Ugali. They are
                      roasted grilled goat meat and maize made into a thick porridge, respectively.
                    </p>
                    {/*<Link to="#" className="read-more-btn">*/}
                    {/*  Read More <i className="icofont-double-right"></i>*/}
                    {/*</Link>*/}
                  </div>
                </div>
              </div>

              <div className="col-lg-18 col-md-12">
                <div className="single-blog-post">
                  <div className="blog-image">
                    <img
                      className="italian-image"
                      src={require('../../../assets/images/Italian.jpeg')}
                      alt="blog"
                    />
                  </div>

                  <div className="blog-post-content">
                    <h3>
                      <Link to="#">Italian Food Experience</Link>
                    </h3>
                    <p>
                      Join us Friday, August 23rd, for a 4-course Authentic Italian Meal and
                      Experience with the Funky Bunch of Marky Mark and the Funky Bunch! In addition
                      to great food and fun, discover new music from emerging artists, Olenka and
                      Andre, with new music album releases.
                    </p>
                    {/*<Link to="#" className="read-more-btn">*/}
                    {/*  Read More <i className="icofont-double-right"></i>*/}
                    {/*</Link>*/}
                  </div>
                </div>
              </div>

              <div className="col-lg-18 col-md-12">
                <div className="single-blog-post">
                  <div className="blog-image">
                    <img src={require('../../../assets/images/Kelewele.jpeg')} alt="blog" />
                  </div>

                  <div className="blog-post-content">
                    <h3>Kelewele - Original Appetizer</h3>
                    <p>
                      Kelewele is a popular Ghanaian street food. It comprises of diced ripe
                      plantain lightly spiced and deep-fried, traditionally served with roasted
                      peanuts for chewing contrast. The plantains are peeled and may be cut into
                      chunks or cubes. Ginger, cayenne pepper, and salt are the typical spices used
                      to make kelewele.
                    </p>
                    {/*<Link to="#" className="read-more-btn">*/}
                    {/*  Read More <i className="icofont-double-right"></i>*/}
                    {/*</Link>*/}
                  </div>
                </div>
              </div>
            </OwlCarousel>
          </div>
        </div>
      </section>
    );
  };
}
