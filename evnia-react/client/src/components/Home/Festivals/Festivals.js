// Libraries
import React, { useContext } from 'react';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import freeFood from '../../../assets/images/free-food.jpg';

const Festivals = () => {
  // To use the JWT credentials
  const authModalContext = useContext(AuthModalContext);
  const appContext = useContext(AppContext);

  // Render Festivals Component
  return (
    <div className="landing-page-free-food">
      <div className="row">
        <div className="col-lg-6">
          <img src={freeFood} alt="Taste Free Food From Around The World" />
        </div>
        <div className="col-lg-6">Taste Free Food From Around The World</div>
      </div>
    </div>
  );
};

export default Festivals;
