// Libraries
import React from 'react';
// import { Link } from "react-router-dom";
import ReactPlayer from 'react-player/youtube';

const AboutVideo = () => {
  return (
    <div>
      <div className="react-player-content">
        <ReactPlayer
          url="https://www.youtube.com/watch?v=GYmmxMF5mUQ"
          controls={true}
          className="react-player"
        />
      </div>
      {/* <div className="text-center">
        <Link to="/about" className="about-video-learn-btn">
          Learn About Our Mission
        </Link>
      </div> */}
    </div>
  );
};

export default AboutVideo;
