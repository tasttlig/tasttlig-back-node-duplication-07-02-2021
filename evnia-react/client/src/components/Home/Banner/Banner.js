// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import ModalVideo from 'react-modal-video';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Components
import BeOurGuest from '../Subscribe/Subscribe';
import JoinNow from './JoinNow';

// Styling
import '../Home.scss';
import '../../../../node_modules/react-modal-video/scss/modal-video.scss';

const Banner = (props) => {
  // Set initial state
  const [days, setDays] = useState('');
  const [hours, setHours] = useState('');
  const [minutes, setMinutes] = useState('');
  // const [seconds, setSeconds] = useState("");
  const [isVideoOpen, setIsVideoOpen] = useState(false);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Countdown helper function
  const makeTimer = () => {
    let endTime = new Date('September 2, 2020 7:00:00 PDT');
    let endTimeParse = Date.parse(endTime) / 1000;
    let now = new Date();
    let nowParse = Date.parse(now) / 1000;
    let timeLeft = endTimeParse - nowParse;
    let days = Math.floor(timeLeft / 86400);
    let hours = Math.floor((timeLeft - days * 86400) / 3600);
    let minutes = Math.floor((timeLeft - days * 86400 - hours * 3600) / 60);
    let seconds = Math.floor(timeLeft - days * 86400 - hours * 3600 - minutes * 60);
    if (hours < '10') {
      hours = '0' + hours;
    }
    if (minutes < '10') {
      minutes = '0' + minutes;
    }
    if (seconds < '10') {
      seconds = '0' + seconds;
    }
    setDays(days);
    setHours(hours);
    setMinutes(minutes);
    // setSeconds(seconds);
  };

  // Mount countdown
  useEffect(() => {
    const myInterval = setInterval(() => {
      makeTimer();
    }, 1000);

    return () => clearInterval(myInterval);
  }, [days, hours, minutes]);

  // Render Main Banner Component page
  return (
    <div className="main-banner item-bg1">
      <div className="d-table">
        <div className="d-table-cell">
          <div className="container main-banner-area">
            <div className="main-banner-content">
              <p>
                Get <span>ready</span> to:
              </p>
              <h1>
                Experience the<br></br> World Locally {/*<b>2</b>*/}
                {/*<b>0</b>*/}
                {/*<b>2</b>*/}
                {/*<b>0</b>*/}
              </h1>

              <ul>
                <li>
                  <i className="icofont-compass"></i> Toronto, ON, Canada
                </li>
                {/*<li>*/}
                {/*  <i className="icofont-calendar"></i> Wednesday, September 2nd*/}
                {/*  to 30th, 2020*/}
                {/*</li>*/}
              </ul>
              {props.noButtons ? null : (
                <div>
                  {!appContext.state.signedInStatus ? (
                    <div className="row button-box">
                      <div className="col-lg-4 banner-btn-content">
                        <Link to="/experiences" className="btn btn-primary banner-btn">
                          Explore the Experiences
                        </Link>
                      </div>
                      <div className="col-lg-4 video-btn-content">
                        <Link
                          onClick={() => setIsVideoOpen(true)}
                          to="#"
                          className="video-btn popup-youtube"
                        >
                          <i className="icofont-ui-play"></i> Watch Promo Video
                        </Link>
                      </div>
                      <div className="col-lg-4 banner-btn-content">
                        <Link to="/host" className="btn btn-secondary">
                          Join
                        </Link>
                      </div>
                    </div>
                  ) : (
                    <div className="row button-box">
                      {/*<div className="col-lg-4 banner-btn-content">*/}
                      {/*  <Link*/}
                      {/*    to="/become-a-food-provider"*/}
                      {/*    className="btn btn-primary join-now"*/}
                      {/*  >*/}
                      {/*    Become a Food Provider*/}
                      {/*  </Link>*/}
                      {/*</div>*/}
                      <div className="col-lg-4 video-btn-content">
                        <Link
                          onClick={() => setIsVideoOpen(true)}
                          to="#"
                          className="video-btn popup-youtube"
                        >
                          <i className="icofont-ui-play"></i> Watch Promo Video
                        </Link>
                      </div>
                      {/* <div
                        onClick={authModalContext.openModal("be-our-guest")}
                        className={"ml-4"}
                      >
                        <JoinNow text={"Be Our Guest"} color={"secondary"} />
                      </div> */}
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <div>
        <ModalVideo
          channel="youtube"
          isOpen={isVideoOpen}
          videoId="GYmmxMF5mUQ"
          onClose={() => setIsVideoOpen(false)}
        />
      </div>
      {!props.noTimer ? null : (
        <div className="event-countdown countdown1">
          <div id="timer">
            <div id="days">
              {days} <span>Days</span>
            </div>
            <div id="hours">
              {hours} <span>Hours</span>
            </div>
            <div id="minutes">
              {minutes} <span>Minutes</span>
            </div>
            {/* <div id="seconds">
              {seconds} <span>Seconds</span>
            </div> */}
          </div>
        </div>
      )}
      <div className="shape1">
        <img src={require('../../../assets/images/shapes/1.png')} alt="shape1" />
      </div>

      <div className="shape2 rotateme">
        <img src={require('../../../assets/images/shapes/2.png')} alt="shape2" />
      </div>

      <div className="shape3 rotateme">
        <img src={require('../../../assets/images/shapes/3.png')} alt="shape3" />
      </div>

      <div className="shape4">
        <img src={require('../../../assets/images/shapes/4.png')} alt="shape4" />
      </div>

      {/* Be Our Guest Modal */}
      <BeOurGuest />
    </div>
  );
};

export default Banner;
