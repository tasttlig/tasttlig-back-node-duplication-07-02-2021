// Libraries
import React, { useState } from 'react';
import Modal from 'react-modal';
import Flag from 'react-world-flags';

// Components
import PassportDetails from '../../../Passport/PassportDetails/PassportDetails';

// Styling
import '../FeaturedFoodSamples.scss';

const FeaturedFoodSamplesCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(false);
    }
  };

  return (
    <div className="col-lg-4 featured-food-sample-card">
      <Modal
        isOpen={passportDetailsOpened}
        onRequestClose={closeModal('passport-details')}
        ariaHideApp={false}
        className="passport-details-modal"
      >
        <div className="form-group">
          <span
            onClick={closeModal('passport-details')}
            className="fas fa-times fa-2x close-modal"
          ></span>
        </div>

        <PassportDetails
          business_name={props.business_name}
          passportId={props.passportId}
          foodSampleId={props.foodSampleId}
          image={props.image}
          title={props.title}
          address={props.address}
          city={props.city}
          provinceTerritory={props.provinceTerritory}
          postalCode={props.postalCode}
          startDate={props.startDate}
          endDate={props.endDate}
          startTime={props.startTime}
          endTime={props.endTime}
          sample_size={props.sample_size}
          is_available_on_monday={props.is_available_on_monday}
          is_available_on_tuesday={props.is_available_on_tuesday}
          is_available_on_wednesday={props.is_available_on_wednesday}
          is_available_on_thursday={props.is_available_on_thursday}
          is_available_on_friday={props.is_available_on_friday}
          is_available_on_saturday={props.is_available_on_saturday}
          is_available_on_sunday={props.is_available_on_sunday}
          description={props.description}
          foodSampleOwnerId={props.foodSampleOwnerId}
          foodSampleOwnerPicture={props.foodSampleOwnerPicture}
          isEmailVerified={props.isEmailVerified}
          firstName={props.firstName}
          lastName={props.lastName}
          email={props.email}
          phoneNumber={props.phoneNumber}
          facebookLink={props.facebookLink}
          twitterLink={props.twitterLink}
          instagramLink={props.instagramLink}
          youtubeLink={props.youtubeLink}
          linkedinLink={props.linkedinLink}
          websiteLink={props.websiteLink}
          bioText={props.bioText}
          history={props.history}
          nationality={props.nationality}
          alpha_2_code={props.alpha_2_code}
          frequency={props.frequency}
          foodSampleType={props.foodSampleType}
          price={props.price}
          quantity={props.quantity}
          numOfClaims={props.numOfClaims}
        />

        <div className="close-modal-bottom-content">
          <span onClick={closeModal('passport-details')} className="close-modal-bottom">
            Close
          </span>
        </div>
      </Modal>

      <div onClick={openModal('passport-details')} className="passport-card-section">
        <img
          src={props.image}
          alt={props.title}
          onLoad={() => setLoad(true)}
          className={load ? 'passport-image' : 'loading-image'}
        />
        <div className="passport-card-flag-container">
          <Flag code={props.alpha_2_code} className="passport-card-flag" />
          <div className="passport-card-flag-country">{props.nationality}</div>
        </div>
        <div className="passport-title">{props.title}</div>
        {/* <div>{`$${props.price}`}</div> */}
      </div>
    </div>
  );
};

export default FeaturedFoodSamplesCard;
