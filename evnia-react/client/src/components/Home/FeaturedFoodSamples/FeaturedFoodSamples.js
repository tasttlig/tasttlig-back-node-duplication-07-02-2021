// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Components
import FeaturedFoodSamplesCard from './FeaturedFoodSamplesCard/FeaturedFoodSamplesCard';

// Styling
import './FeaturedFoodSamples.scss';

const FeaturedFoodSamples = () => {
  // Set initial state
  const [featuredFoodSampleItems, setFeaturedFoodSampleItems] = useState([]);

  // Render Featured Food Samples Cards helper function
  const renderFeaturedFoodSampleCards = (arr) => {
    return arr
      .slice(0, 3)
      .map((card, index) => (
        <FeaturedFoodSamplesCard
          key={index}
          foodSampleId={card.food_sample_id}
          image={card.image_urls[0]}
          title={card.title}
          address={card.address}
          city={card.city}
          provinceTerritory={card.state}
          postalCode={card.postal_code}
          startDate={card.start_date}
          endDate={card.end_date}
          startTime={card.start_time}
          endTime={card.end_time}
          description={card.description}
          sample_size={card.sample_size}
          is_available_on_monday={card.is_available_on_monday}
          is_available_on_tuesday={card.is_available_on_tuesday}
          is_available_on_wednesday={card.is_available_on_wednesday}
          is_available_on_thursday={card.is_available_on_thursday}
          is_available_on_friday={card.is_available_on_friday}
          is_available_on_saturday={card.is_available_on_saturday}
          is_available_on_sunday={card.is_available_on_sunday}
          foodSampleOwnerId={card.food_sample_creater_user_id}
          foodSampleOwnerPicture={card.profile_image_link}
          isEmailVerified={card.is_email_verified}
          firstName={card.first_name}
          lastName={card.last_name}
          email={card.email}
          phoneNumber={card.phone_number}
          facebookLink={card.facebook_link}
          twitterLink={card.twitter_link}
          instagramLink={card.instagram_link}
          youtubeLink={card.youtube_link}
          linkedinLink={card.linkedin_link}
          websiteLink={card.website_link}
          bioText={card.bio_text}
          nationality={card.nationality}
          alpha_2_code={card.alpha_2_code}
          frequency={card.frequency}
          foodSampleType={card.food_sample_type}
          price={card.price}
          quantity={card.quantity}
          numOfClaims={card.num_of_claims}
        />
      ));
  };

  // Fetch Featured Food Samples helper function
  const fetchFeaturedFoodSamples = async () => {
    const url = '/food-sample/all';
    const response = await axios({ method: 'GET', url });

    setFeaturedFoodSampleItems(response.data.details.data);
  };

  // Mount Featured Food Samples Component page
  useEffect(() => {
    fetchFeaturedFoodSamples();
  }, []);

  // Render Featured Food Samples Component page
  return (
    <div className="featured-food-samples">
      <div className="featured-food-samples-title">Featured Food Samples</div>

      <div className="featured-food-sample-cards">
        {renderFeaturedFoodSampleCards(featuredFoodSampleItems)}
      </div>

      <Link to="/festival" className="btn btn-warning see-all-food-samples">
        Show All
      </Link>
    </div>
  );
};

export default FeaturedFoodSamples;
