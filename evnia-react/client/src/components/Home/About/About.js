// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Slide } from 'react-slideshow-image';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import moment from 'moment';

// Components
import JoinNow from '../Banner/JoinNow';

// Styling
import '../Home.scss';

const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrows: true,
  pauseOnHover: true,
};

const About = () => {
  // Set initial state
  const [load, setLoad] = useState(false);
  // const [foodSampleItems, setFoodSampleItems] = useState([]);
  const [experienceItems, setExperienceItems] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // // Fetch Food Samples helper function
  // const fetchFoodSamples = async () => {
  //   const url = "/food-sample/all";
  //   const response = await axios({ method: "GET", url });
  //   setFoodSampleItems(response.data.details.data);
  // };

  // Fetch Food Samples helper function
  const fetchExperiences = async () => {
    const url = '/experience/all';
    const response = await axios({ method: 'GET', url });

    setExperienceItems(response.data.details.data);
  };

  // Mount About Component page
  useEffect(() => {
    fetchExperiences();
  }, []);

  // Render About Component page
  return (
    <section className="about-area ptb-120 bg-image">
      <div className="container">
        <div className="row h-100 align-items-center">
          <div className="col-lg-6">
            <div className="about-content">
              <span>Join The Experience</span>
              <h2>About Tasttlig</h2>
              <p className="text-justify">
                Tasttlig showcases the world in the best light by connecting people to the world
                through culinary experiences hosted in a distributed network of restaurants. The
                culinary experiences contain prix fixe meals, games and entertainment for guests to
                enjoy immersive cultural experiences. Certain experiences feature celebrities, VIP
                personalities and artists. Every Tasttlig experience is accompanied by games and
                prizes ranging from flights, travel packages, dinners and more.
              </p>
              <p className="text-justify">
                Tasttlig helps restaurants grow their businesses by hosting experiences at their
                location. They become hosts by joining Tasttlig
              </p>
              <div>
                {!appContext.state.signedInStatus ? (
                  <div className="d-flex row button-box">
                    <div className="col-lg-12 get-a-passport-btn-content">
                      <Link to="/experiences" className="btn btn-primary banner-btn">
                        Explore the Experiences
                      </Link>
                    </div>
                    <div className="col-sm-12 get-a-passport-btn-content">
                      <Link to="/host" className="btn btn-secondary banner-btn">
                        Join
                      </Link>
                    </div>
                    {/*<div className="col-lg-12 read-more-btn-content">*/}
                    {/*  <Link to="/about" className="btn btn-warning banner-btn">*/}
                    {/*    Read More*/}
                    {/*  </Link>*/}
                    {/*</div>*/}
                  </div>
                ) : (
                  <div className="d-flex row button-box">
                    <div className="col-lg-12 get-a-passport-btn-content">
                      <Link to="/experiences" className="btn btn-primary banner-btn">
                        Explore the Experiences
                      </Link>
                    </div>
                    {/*<div className="col-lg-12 become-a-food-provider-btn-content">*/}
                    {/*  <Link*/}
                    {/*    to="/become-a-food-provider"*/}
                    {/*    className="btn btn-primary banner-btn"*/}
                    {/*  >*/}
                    {/*    Become a Food Provider*/}
                    {/*  </Link>*/}
                    {/*</div>*/}
                    {/* <div
                      onClick={authModalContext.openModal("be-our-guest")}
                      className={"ml-4"}
                    >
                      <JoinNow text={"Be Our Guest"} color={"secondary"} />
                    </div> */}
                    {/*<div className="col-lg-12 read-more-btn-content">*/}
                    {/*  <Link to="/about" className="btn btn-warning banner-btn">*/}
                    {/*    Read More*/}
                    {/*  </Link>*/}
                    {/*</div>*/}
                  </div>
                )}
              </div>
            </div>
          </div>

          <div className="slide-container">
            <Slide {...properties}>
              {/*{foodSampleItems.slice(0, 5).map((foodSampleItem, index) => (*/}
              {/*  <div key={index} className="text-center">*/}
              {/*    <img*/}
              {/*      src={foodSampleItem.image_urls[0]}*/}
              {/*      alt={foodSampleItem.title}*/}
              {/*      onLoad={() => setLoad(true)}*/}
              {/*      className={load ? "passport-image" : "loading-image"}*/}
              {/*    />*/}
              {/*    /!* <div className="passport-card-flag-container">*/}
              {/*          <img*/}
              {/*            src={`https://www.countryflags.io/${foodSampleItem.alpha_2_code}/shiny/64.png`}*/}
              {/*            alt={foodSampleItem.nationality}*/}
              {/*            className="passport-card-flag"*/}
              {/*          />*/}
              {/*          <div className="passport-card-flag-country">*/}
              {/*            {foodSampleItem.nationality}*/}
              {/*          </div>*/}
              {/*        </div> *!/*/}
              {/*    <div className="passport-title">{foodSampleItem.title}</div>*/}
              {/*    <div className="passport-card-details">*/}
              {/*      {foodSampleItem.address}, {foodSampleItem.city},{" "}*/}
              {/*      {foodSampleItem.state} {foodSampleItem.postal_code}*/}
              {/*    </div>*/}
              {/*    <div className="passport-card-details">*/}
              {/*      {moment(*/}
              {/*        moment(*/}
              {/*          new Date(*/}
              {/*            foodSampleItem.start_date*/}
              {/*          ).toISOString().split("T")[0] +*/}
              {/*          "T" +*/}
              {/*          foodSampleItem.start_time + ".000Z"*/}
              {/*        ).add(new Date().getTimezoneOffset(), "m")*/}
              {/*      ).format("MMM Do")}{" "}*/}
              {/*      {moment(*/}
              {/*        moment(*/}
              {/*          new Date(*/}
              {/*            foodSampleItem.start_date*/}
              {/*          ).toISOString().split("T")[0] +*/}
              {/*          "T" +*/}
              {/*          foodSampleItem.start_time + ".000Z"*/}
              {/*        ).add(new Date().getTimezoneOffset(), "m")*/}
              {/*      ).format("hh:mm a")}{" "}*/}
              {/*      -{" "}*/}
              {/*      {moment(*/}
              {/*        moment(*/}
              {/*          new Date(*/}
              {/*            foodSampleItem.start_date*/}
              {/*          ).toISOString().split("T")[0] +*/}
              {/*          "T" +*/}
              {/*          foodSampleItem.end_time + ".000Z"*/}
              {/*        ).add(new Date().getTimezoneOffset(), "m")*/}
              {/*      ).format("hh:mm a")}*/}
              {/*      <br />*/}
              {/*      {foodSampleItem.frequency} until{" "}*/}
              {/*      {moment(*/}
              {/*        new Date(foodSampleItem.end_date).toISOString()*/}
              {/*      ).format("MMM Do")}*/}
              {/*    </div>*/}
              {/*  </div>*/}
              {/*))}*/}
              {experienceItems.slice(0, 5).map((experienceItem, index) => (
                <div key={index} className="text-center">
                  <img
                    src={experienceItem.image_urls[0]}
                    alt={experienceItem.title}
                    onLoad={() => setLoad(true)}
                    className={load ? 'passport-image' : 'loading-image'}
                  />
                  <div className="passport-title">{experienceItem.title}</div>
                  <div className="passport-card-details">
                    {experienceItem.address}, {experienceItem.city}, {experienceItem.state}{' '}
                    {experienceItem.postal_code}
                  </div>
                  <div className="passport-card-details">
                    {moment(
                      moment(
                        new Date(experienceItem.start_date).toISOString().split('T')[0] +
                          'T' +
                          experienceItem.start_time +
                          '.000Z',
                      ).add(new Date().getTimezoneOffset(), 'm'),
                    ).format('MMM Do')}{' '}
                    {moment(
                      moment(
                        new Date(experienceItem.start_date).toISOString().split('T')[0] +
                          'T' +
                          experienceItem.start_time +
                          '.000Z',
                      ).add(new Date().getTimezoneOffset(), 'm'),
                    ).format('hh:mm a')}{' '}
                    -{' '}
                    {moment(
                      moment(
                        new Date(experienceItem.start_date).toISOString().split('T')[0] +
                          'T' +
                          experienceItem.end_time +
                          '.000Z',
                      ).add(new Date().getTimezoneOffset(), 'm'),
                    ).format('hh:mm a')}
                  </div>
                </div>
              ))}
            </Slide>

            {/* <div className="col-lg-12 text-center">
              <Link to="/festival" className="btn btn-secondary">
                Explore All Festival Experiences
              </Link>
            </div> */}
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
