// Libraries
import React, { useContext } from 'react';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Styling
import '../Home.scss';

const BuyTicket = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Render Buy Ticket Component page
  return (
    <section className="buy-tickets-area ptb-120">
      <div className="buy-tickets">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6">
              <div className="section-title">
                <span>Hurry Up!</span>
                <h2>Host Tasttlig at your Restaurant</h2>

                <p>Attract more customers</p>
              </div>
            </div>

            <div className="col-lg-6">
              <div className="buy-ticket-btn">
                <div
                  onClick={authModalContext.openModal('sign-up')}
                  className="btn btn-primary join-now"
                >
                  Host
                </div>
                {/*{appContext.state.signedInStatus ? (*/}
                {/*  <div*/}
                {/*    onClick={authModalContext.handleSubmitTasttligFestivalGuest}*/}
                {/*    disabled={authModalContext.state.submitAuthDisabled}*/}
                {/*    className="btn btn-primary rsvp"*/}
                {/*  >*/}
                {/*    Join*/}
                {/*  </div>*/}
                {/*) : (*/}
                {/*  <div*/}
                {/*    onClick={authModalContext.openModal("sign-up")}*/}
                {/*    className="btn btn-primary join-now"*/}
                {/*  >*/}
                {/*    Host Now!*/}
                {/*  </div>*/}
                {/*)}*/}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default BuyTicket;
