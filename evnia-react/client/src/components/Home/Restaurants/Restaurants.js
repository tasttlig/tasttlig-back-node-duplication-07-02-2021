// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

// Components
import restaurantData from '../../../data/RestaurantData';
import RestaurantTimelineItem from './RestaurantTimelineItem/RestaurantTimelineItem';
import JoinNow from '../Banner/JoinNow';

// Styling
import './Restaurants.scss';

// Render Restaurants Component
const Restaurants = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  return (
    <div className="restaurants">
      <div className="restaurants-title">How it Works</div>

      <div className="row restaurants-steps">
        <div className="col-sm-4">Get your festival pass</div>
        <div className="col-sm-4 border-left border-right border-dark">
          Explore the festival samples
        </div>
        <div className="col-sm-4">Claim free samples</div>
      </div>

      <div className="restaurants-timeline-container mt-3">
        {restaurantData.map((data, index) => (
          <RestaurantTimelineItem data={data} key={index} />
        ))}
      </div>

      <Link to="/passport/subscription" className="btn btn-primary get-your-passport-btn">
        Explore the Festival
      </Link>

      <div className="restaurants-steps mt-5 mb-5">
        Tasttlig Passport holders get 20% off festival admission.
      </div>

      {!appContext.state.signedInStatus && (
        <Link to="/host" className="btn btn-secondary get-a-passport-btn">
          Join
        </Link>
      )}
    </div>
  );
};

export default Restaurants;
