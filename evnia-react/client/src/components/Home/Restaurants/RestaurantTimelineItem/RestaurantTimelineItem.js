// Libraries
import React, { useState } from 'react';

// Styling
import '../Restaurants.scss';

const RestaurantTimelineItem = ({ data }) => {
  // Set initial state
  const [load, setLoad] = useState(false);

  return (
    <div className="restaurants-timeline-item">
      <div className="restaurants-timeline-item-content">
        <img
          src={data.image}
          alt={data.nationality}
          onLoad={() => setLoad(true)}
          className={load ? 'restaurants-timeline-item-content-image' : 'loading-image'}
        />
        <div className="restaurants-timeline-item-content-restaurant-underline">
          <div className="restaurants-timeline-item-content-restaurant">{data.restaurant}</div>
        </div>
        <div className="restaurants-timeline-item-content-food-items">{data.food_items}</div>
        <div className="restaurants-timeline-item-content-address">{data.address}</div>
        <div className="restaurants-timeline-item-content-frequency">{data.frequency}</div>
        <div className="restaurants-timeline-item-content-time">{`${data.start_time} to ${data.end_time} EDT`}</div>

        <span className="circle" />
      </div>
    </div>
  );
};

export default RestaurantTimelineItem;
