// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import Nav from '../../Navbar/Nav';

// Styling
import '../Dashboard.scss';
import septemberFestivalImage from '../../../assets/images/food-festival-passport-sep-2020.jpg';
import decemberFestivalImage from '../../../assets/images/food-festival-passport-dec-2020.jpg';

const FestivalDashboard = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Mount Dashboard page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <Nav />

      <div className="dashboard">
        <div className="row">
          <div className="col-lg-12">
            <div className="dashboard-title text-center">Festivals</div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-3 col-sm-12 col-md-4 dashboard-section">
            <Link exact="true" to="/09-2020-festival">
              <div className="dashboard-content-text border shadow">
                <img
                  src={septemberFestivalImage}
                  alt="Current Experiences"
                  className="card-image"
                />
                <div className="text-center col-sm-12 p-3">September 2020 Festival</div>
              </div>
            </Link>
          </div>
          <div className="col-lg-3 col-sm-12 col-md-4 dashboard-section">
            <Link exact="true" to="/festival">
              <div className="dashboard-content-text border shadow">
                <img src={decemberFestivalImage} alt="Create Experience" className="card-image" />
                <div className="text-center col-sm-12 p-3">December 2020 Festival</div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FestivalDashboard;
