// Libraries
import React, { useState, useContext, useEffect, Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import Flag from 'react-world-flags';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';
// import {
//   Select,
//   Input
// } from "../../../EasyForm";

// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import {
  formatDate,
  formatMilitaryToStandardTime,
  formattedTimeToDateTime,
} from '../../../Functions/Functions';

// Styling
import '../../../Passport-Old/PassportCard/PassportCard.scss';
// import './BusinessCreatedCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const BusinessCreatedCard = (props) => {
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  //

  console.log('props from BusinessCreatedCard: ', props);
  // Set initial state
  const [load, setLoad] = useState(false);
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const history = useHistory();
  const [errorMessage, setErrorMessage] = useState('');
  const [canClaim, setCanClaim] = useState(true);
  const [claimQuantity, setClaimQuantity] = useState(0);
  const [userSubscriptions, setUserSubscriptions] = useState([]);
  const [claimReservationList, setClaimReservationList] = useState([]);
  const idOfCard =
    props.foodSampleProps.business_details_id ||
    props.foodSampleProps.service_id ||
    props.foodSampleProps.experience_id;

    localStorage.removeItem('festivalId');

  const getUserSubscriptions = async () => {
    const response = await axios({
      method: 'GET',
      url: `/valid-user-subscriptions/${appContext.state.user.id}`,
    });
    console.log('r u', response.data.user);
    setUserSubscriptions(response.data.user);
    //return response;
  };

  useEffect(() => {
    getUserSubscriptions();
  }, []);

  const getSelectedServices = (ids) => {
    if (ids.length > 0) {
      return ids && ids.map((i) => '' + i);
    }
    return [];
  };

  // Fetch user festival reservations helper function
  const fetchUserClaimFestivalReservations = async () => {
    try {
      const url = '/all-product-claim/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };
  console.log("claimReservationList from festival details: ",claimReservationList)

  // Mount Festival Reservations page
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoad(true);

    fetchUserClaimFestivalReservations().then(({ data }) => {
      console.log(data.details);
      setClaimReservationList(data.details);
    });
    setLoad(false);
  }, []);

  const hasSubscriptionAndValid = () => {
    let result = false;
    userSubscriptions.length > 0 &&
      userSubscriptions.map((sub) => {
        if (sub.subscription_code.startsWith('G')) {
          if (
            sub.user_subscription_status === 'ACTIVE' &&
            (sub.suscribed_festivals == null ||
              getSelectedServices(sub.suscribed_festivals).includes(props.festivalId))
          ) {
            console.log('sub has fest or null', props.festivalId);
            result = true;
          }
        } else if (sub.subscription_code.startsWith('H')) {
          result = true;
        }
      });
    if (!result) {
      toast(`You have no valid subscription to attend this festival!`, {
        type: 'info',
        autoClose: 2000,
      });
    }
    console.log('result', result);
    return result;
  };


  let passportIdOrEmail = null;

  const validatePassportIdOrEmail = () => {
    passportIdOrEmail = appContext.state.user.passport_id;

    if (!passportIdOrEmail) {
      setErrorMessage('Passport Id or Email is required.');

      return false;
    } else {
      setErrorMessage('');

      return true;
    }
  };
  let isSelected;
  if (props.selected) {
    isSelected = props.selected.includes(idOfCard);
  }

  const handleCheckboxChange = (event) => {
    //setIsSelected((prev) => !prev);
    //props.setSelected(value);
    //console.log(idOfCard);
    if (isSelected) {
      props.setSelected((prev) => prev.filter((id) => id !== idOfCard));
    } else {
      props.setSelected((prev) => [...prev, idOfCard]);
    }
  };

  const getClaimReservationstatus = function (claimReservationListItem) {

    let status = false;
    for (let claimedItem of claimReservationListItem ) {
      if(claimedItem.product_user_id === props.foodSampleProps.tasttlig_user_id && claimedItem.current_stamp_status === "Redeemed") {
        // status = true; 
        // console.log("claimedItem from true side ", props.foodSampleProps.tasttlig_user_id)
        return true
        // break;
       } 
    }
    console.log("claimReservationListItem from getclaim reservation function:", claimReservationListItem)
    return false;
  };

  console.log('appContext.state.user.id:', appContext.state.user.id);

  // Render Current Food Samples Card
  return (
    <div className="col-lg-6 col-md-12">
      {props.isSelectable && (
        <input type="checkbox" value={isSelected} onChange={handleCheckboxChange}></input>
      )}
      <LazyLoad once>


        <div className="d-flex flex-column main-user-item-card">
        <Link exact="true" to={`/festival/${props.festivalId}/business-owner/${props.foodSampleProps.tasttlig_user_id}`} onClick={() => localStorage.setItem('festivalId', props.festivalId)}>

          <div >
            <div className="food-sample-card-photo">
              <span className="item-host-name">
                <i class="fas fa-map-marker-alt"></i>
                <strong>{props.foodSampleProps.city}</strong>
              </span>
              <span className="days-remaining">
                <strong>{props.foodSampleProps.business_type}</strong>
              </span>
              
              <img
                src={props.foodSampleProps.business_image_urls[0]}
                alt={props.foodSampleProps.business_name}
                onLoad={() => setLoad(true)}
                className={load ? "passport-image" : "loading-image"}
              />
            </div>
          </div>
          </Link>

             
          <div className="user-item-card-footer">
            <div className="user-item-footer-title">
              {props.foodSampleProps.business_name
                ? props.foodSampleProps.business_name
                // : props.foodSampleProps.title
                // ? props.foodSampleProps.title
                // : props.foodSampleProps.experience_name
               : null }
            </div>
            <div className="user-item-footer-buttons">
            {!userRole ? ( null ) :(
                // {/* {(userRole && userRole.includes('ADMIN')) ||
                // props.foodSampleProps.tasttlig_user_id === appContext.state.user.id ? (
                // null
                // ) : ( */}
                  <div>

                  {claimReservationList &&  claimReservationList.length > 0
                     && getClaimReservationstatus(claimReservationList) === true ? (
                     <button>visited </button>
                    // <> 
                    // {console.log("status from here")}
                    //  </>
                     ) : null}

                  </div>
                // {/* )} */}
              )}
            </div>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default BusinessCreatedCard;
