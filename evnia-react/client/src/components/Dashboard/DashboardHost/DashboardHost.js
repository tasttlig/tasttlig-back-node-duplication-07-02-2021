// Libraries
import React, {Fragment, useContext, useEffect, useState} from 'react';
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import Modal from 'react-modal';
import {AppContext} from '../../../ContextProvider/AppProvider';
import {toast} from 'react-toastify';
import {useForm} from 'react-hook-form';
import { Link } from 'react-router-dom';
import {formatDate, formatMilitaryToStandardTime, formattedTimeToDateTime,} from '../../Functions/Functions';
import {CheckboxGroup, DateInput, Form, Input, MultiImageInput, Select, Textarea,} from '../../EasyForm';
import './DashboardHost.scss';
import ApplyForBusinessPassport from '../../ApplyForBusinessPassport/ApplyForBusinessPassport';
import HostRedeemDetails from '../HostRedeemFood/HostRedeemDetails/HostRedeemDetails';
import TicketDetails from '../Ticket/TicketDetails/TicketDetails';
import ClaimsDetails from '../MemberClaimedFood/ClaimsDetails/ClaimsDetails';
import SponsorPackageKindCard from '../../Profile/Sponsor/SponsorPackageKindCard';
import ProductDetails from '../BusinessDashboard/productAndServiceDetails/ProductDetails';
import EditExperience from '../BusinessDashboard/productAndServiceDetails/EditExperience';
import ServiceDetails from '../BusinessDashboard/productAndServiceDetails/ServiceDetails';
import PromotionDetails from '../BusinessDashboard/productAndServiceDetails/PromotionDetails';
import Admin from '../../Admin/Admin';
import HostPanel from '../HostPanel/HostPanel';
import CreateFoodSampleForm from '../../CreateFoodSample/CreateFoodSampleForm/CreateFoodSampleForm';
import CreateSampleSaleDonation from '../../CreateSampleSaleDonation/CreateSampleSaleDonation';

const DashboardHost = () => {
  const [createFormsOpened, setCreateFormsOpened] = useState(false);
  const [currentForm, setCurrentForm] = useState('tickets');
  const [services, setServices] = useState([]);
  const [fetchedExperiences, setFetchedExperiences] = useState([]);
  const [foodSamples, setFoodSamples] = useState([]);
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [promotions, setPromotions] = useState([]);
  const [tickets, setTickets] = useState([]);
  const [sponsorships, setSponsorships] = useState([]);
  const [sponsorshipForm, setSponsorshipForm] = useState('sponsorInCash');
  const history = useHistory();
  const [inKindSponsorships, setInKindSponsorships] = useState([]);
  const [myOrders, setMyOrders] = useState([]);
  const [currentOrders, setCurrentOrders] = useState([]);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [selectedServices, setSelectedServices] = useState([]);
  const [selectedPromotions, setSelectedPromotions] = useState('');
  const [selectedTickets, setSelectedTickets] = useState([]);
  const [selectedFoodSamples, setSelectedFoodSamples] = useState([]);
  const [refreshUserProducts, setRefreshUserProducts] = useState(false);
  const [refreshUserServices, setRefreshUserServices] = useState(false);
  const [refreshUserTickets, setRefreshUserTickets] = useState(false);
  const [refreshUserFoodSamples, setRefreshUserFoodSamples] = useState(false);
  const [refreshUserExperience, setrefreshUserExperience] = useState(false);
  const [foodSampleFestivalOpened, setFoodSampleFestivalOpened] = useState(false);
  const [reviewInformation, setReviewInformation] = useState([]);
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [festivalPreference, setFestivalPreference] = useState([]);
  const [promotionOpened, setPromotionOpened] = useState(false);

  const [festivalList, setFestivalList] = useState([]);
  const [productToEdit, setProductToEdit] = useState({});
  const [claimReservationList, setClaimReservationList] = useState([]);
  const [redeemReservationList, setRedeemReservationList] = useState([]);
  const [experienceRedeemReservationList, setExperienceRedeemReservationList] = useState([]);
  const [serviceRedeemReservationList, setServiceRedeemReservationList] = useState([]);
  const [focus, setFocus] = useState('view');
  const [selectedExperiences, setSelectedExperiences] = useState([]);
  const [selectedVendingFestival, setSelectedVendingFestival] = useState(null);
  const [filterSearch, setFilterSearch] = useState('');
  const [keyword, setKeyword] = useState('');
  const [nationalities, setNationalities] = useState([]);
  const [formDetails, setFormDetails] = useState({
    input_one: 'product_name',
    input_one_placeholder: 'Name Your Product',
    input_two: 'product_made_in_nationality_id',
    input_three: 'product_price',
    input_three_placeholder: 'Price',
    input_four: 'product_quantity',
    input_four_placeholder: 'Quantity',
    input_five: 'product_size',
    input_five_placeholder: 'Size',
    input_six: 'product_description',
    input_six_placeholder: 'Description',
    input_seven: 'product_expiry_date',
    input_seven_placeholder: 'Expiry Date',
    input_eight: 'product_expiry_time',
    input_eight_placeholder: 'Expiry Time',
    input_nine: 'product_images',
    input_ten: 'product_festival_id',
  });
  const [selected, setSelected] = useState('products');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [toggleState, setToggleState] = useState(1);
  const [toggleRedemptionState, setToggleRedemptionState] = useState(1);

  // Close modal type helper function
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  const userRolesString = userRole && userRole.join(', ').toLowerCase();
  const user = appContext.state.user;
  const [editExperience, setEditExperience] = useState([]);
  const [addToFestivalSelected, setAddToFestivalSelected] = useState(false);
  let experienceData = {festival_selected: []};
  let count = 0;
  let ids = [];
  
  const formMethods = useForm();

  const closeModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(false);
    }
    if (modalType === 'passport-details') {
      setFoodSampleFestivalOpened(false);
    }
    if (modalType === 'apply-promotion') {
      setPromotionOpened(false);
      fetchUserFoodSamples();
      setRefreshUserFoodSamples(false);
    }
  };

  const toggleTab = (index) => {
    if (index === 1) {
      setCurrentForm('tickets');
    } else if (index === 2) {
      setCurrentForm('home');
    }
    setToggleState(index);
  };

  const toggleRedemptionTab = (index) => {
    // if (index === 1) {
    //   setCurrentForm('tickets');
    // } else if (index === 2) {
    //   setCurrentForm('home');
    // }
    setToggleRedemptionState(index);
  };
  console.log('data cpming from food sample:', selectedFoodSamples);
  let data = {};
  //handle for search box
  const handleInputChange = (event) => {
    //event.preventDefault();
    setKeyword(event.target.value);
  };
  const _handleKeyDown = (event) => {
    //event.preventDefault();
    if (event.key === 'Enter') {
      event.preventDefault();
      setFilterSearch(keyword);
    }
  };
  const Empty = () => {
    return <strong className="no-food-samples-found">No food samples found.</strong>;
  };

  const renderClaimsRows = (arr) => {
    return arr.map((row, index) => (
      <ClaimsDetails
        key={index}
        productId={row.product_id}
        foodSampleId={row.claim_viewable_id}
        claimantFullName={row.first_name + ' ' + row.last_name}
        dateTimeofClaim={row.reserved_on}
        numberofClaims={row.claimed_quantity}
        foodName={row.title ? row.title : row.service_name ? row.service_name : row.experience_name ? row.experience_name : null}
        festivalName={row.festival_name}
        claimStatus={row.current_stamp_status}
        //history={props.history}
      />
    ));
  };
  const renderRedeemsRows = (arr, itemType) => {
    return arr.map((row, index) => (
      <HostRedeemDetails
        key={index}
        productId={row.claim_viewable_id}
        claimantFullName={row.first_name + ' ' + row.last_name}
        dateTimeofClaim={row.reserved_on}
        numberofClaims={row.claimed_quantity}
        foodName={row.title ? row.title : row.service_name ? row.service_name : row.experience_name ? row.experience_name : null}
        festivalName={row.festival_name}
        claimStatus={row.current_stamp_status}
        totalQuantity={row.quantity ? row.quantity : row.service_capacity ? row.service_capacity : row.experience_capacity}
        totalRedeemQuantity={row.redeemed_total_quantity}
        itemType={itemType}
        //history={props.history}
      />
    ));
  };

   // Edit current food sample helper function
   const handleEditExperience = (editingExperiences) => {
    history.push({
      pathname: '/edit-experience',
      state: {
        experienceId: editingExperiences.experience_id,
        experience_images: editingExperiences.image_urls,
        experience_name: editingExperiences.experience_name,
        nationality_id: editingExperiences.nationality_id,
        start_date: editingExperiences.start_date,
        end_date: editingExperiences.end_date,
        festival_selected: editingExperiences.festival_selected,
        start_time: formattedTimeToDateTime(editingExperiences.start_time),
        end_time: formattedTimeToDateTime(editingExperiences.end_time),
        experience_size_scope: editingExperiences.experience_size_scope,
        experience_price: editingExperiences.experience_price,
        experience_capacity: editingExperiences.experience_capacity,
        spice_level: editingExperiences.spice_level,
        experience_description: editingExperiences.experience_description,
        // actual_price: props.foodSampleProps.price,
        // promotional_price: props.foodSampleProps.discounted_price
      },
    });
  };


  

  const addExperienceId = (id) => {
    const stringValue = '' + id;

    if (ids.length === 0) {
      ids[count] = stringValue;
      count++;
    } else if (ids.length > 0 && ids.includes(stringValue)) {
      ids = ids.filter((id) => id !== stringValue);
      count--;
    } else {
      ids[count] = stringValue;
      count++;
    }
    localStorage.setItem('experienceIds', JSON.stringify(ids));
  };

  const getSelectedExperiences = () => {
    return ids.map((i) => Number(i));
  };

  const addExperienceToFestival = async (data) => {
    if (data.festival_selected && data.festival_selected.length < 1) {
      toast('Please select a festival!', {
        type: 'info',
        autoClose: 2000,
      });
      return;
    }
    data.experience_id = JSON.parse(localStorage.getItem('experienceIds'));
    ids = JSON.parse(localStorage.getItem('experienceIds'));
    let url = `/experience/update/${data.experience_id}`;
    try {
      const response = await axios({
        method: 'PUT',
        url,
        data,
      });
      if (response && response.data && response.data.success && userRole) {
        setAddToFestivalSelected(false);
        ids = JSON.parse(localStorage.getItem('experienceIds'));
        setTimeout(() => {
          setCurrentForm('time-out');
          fetchUserExperiences();
        }, 2000);
        setTimeout(() => {
          setCurrentForm('experiences');
        }, 4000);
        toast('Experience added to selected festival', {
          type: 'success',
          autoClose: 2000,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  console.log("fetchedExperiences", fetchedExperiences)
  const renderFetchedExperiences = (arr) => {
    // return arr.map((row, index) => (
    //   <ExperienceDetails
    //     key={index}
    //     experienceId={row.experience_id}
    //     experienceName={row.experience_name}
    //     experienceDetails={row.experience_description}
    //     experienceNationality={row.nationality}
    //     // festivalName={row.festival_name}
    //     experienceSize={row.experience_size_scope}
    //     experienceCapacity={row.experience_capacity}
    //     experiencePrice={row.experience_price}
    //   />
    // ));

    return arr.map((experience) => {
      // console.log("experience from experience render", experience)
      const isSelected = !!selectedExperiences.find(
        (selectedExperience) => experience.experience_id === selectedExperience.experience_id,
      );
      // console.log("experience from experience render", experience)
      return (
        <tr>
          <td scope="row">
            <input
              type="checkbox"
              name="foodSample_checkbox"
              value={isSelected}
              onClick={() => {
                addExperienceId(experience.experience_id);
              }}
              /* onChange={() => {
                if (isSelected) {
                  setSelectedExperiences((selectedExperiences) => {
                    const newExperience = selectedExperiences.filter(
                      (selectedExperience) =>
                        selectedExperience.experience_id !== experience.experience_id,
                    );
                    return newExperience;
                  });
                } else {
                  setSelectedExperiences((selectedExperiences) =>
                    selectedExperiences.concat(experience),
                  );
                }
              }} */
            ></input>
          </td>
          <td
            class="fas fa-edit"
            value={isSelected}
            onClick={(e) => {
              e.preventDefault();
              // setCurrentForm('edit-experience');
              // setEditExperience(experience);
              handleEditExperience(experience);
            }}
          ></td>
          <td scope="row">{experience.experience_id}</td>
          <td>{experience.experience_name}</td>
          <td>{experience.experience_capacity}</td>
          <td>{experience.experience_size_scope}</td>
          {/* <td>{experience.nationality}</td> */}
          <td>{experience.experience_description}</td>
          <td>${experience.experience_price}</td>
        </tr>
      );
    });
  };

  // Select product form helper function
  const productSelected = () => {
    setFormDetails({
      input_one: 'product_name',
      input_one_placeholder: 'Name Your Product',
      input_two: 'product_made_in_nationality_id',
      input_three: 'product_price',
      input_three_placeholder: 'Price',
      input_four: 'product_quantity',
      input_four_placeholder: 'Quantity',
      input_five: 'product_size',
      input_five_placeholder: 'Size',
      input_six: 'product_description',
      input_six_placeholder: 'Description',
      input_seven: 'product_expiry_date',
      input_seven_placeholder: 'Expiry Date',
      input_eight: 'product_expiry_time',
      input_eight_placeholder: 'Expiry Time',
      input_nine: 'product_images',
      input_ten: 'product_festivals_id',
    });
  };

  // Select service form helper function
  const serviceSelected = () => {
    setFormDetails({
      input_one: 'service_name',
      input_one_placeholder: 'Name Your Service',
      input_two: 'service_nationality_id',
      input_three: 'service_price',
      input_three_placeholder: 'Price',
      input_four: 'service_capacity',
      input_four_placeholder: 'Capacity',
      input_five: 'service_size_scope',
      input_five_placeholder: 'Scope',
      input_six: 'service_description',
      input_six_placeholder: 'Description',
      input_seven: null,
      input_seven_placeholder: null,
      input_eight: null,
      input_eight_placeholder: null,
      input_nine: 'service_images',
      input_ten: 'service_festival_id',
    });
  };

  // Select promotion form helper function
  const promotionSelected = () => {
    setFormDetails({
      input_one: 'promotion_name',
      input_one_placeholder: 'Name Your Promotion',
      input_two: 'promotion_discount_percent',
      input_two_placeholder: 'Promotion discount percentage',
      input_three: 'promotion_start_date',
      input__three_placeholder: 'Promotion Start Date',
      input_four: 'promotion_end_date',
      input_four_placeholder: 'Promotion End Date',
    });
  };

  // Select experience form helper function
  const experienceSelected = () => {
    setFormDetails({
      input_one: 'experience_name',
      input_one_placeholder: 'Experience Name',
      input_two: 'experience_nationality_id',
      input_three: 'experience_price',
      input_three_placeholder: 'Price',
      input_four: 'experience_capacity',
      input_four_placeholder: 'Capacity',
      input_five: 'experience_size_scope',
      input_five_placeholder: 'Scope',
      input_six: 'experience_description',
      input_six_placeholder: 'Description',
      input_seven: null,
      input_seven_placeholder: null,
      input_eight: null,
      input_eight_placeholder: null,
      input_nine: 'experience_images',
      input_ten: 'festival_selected',
    });
  };

  // Change form helper function
  const changeForm = (event) => {
    if (event.target.value === 'experiences') {
      setSelected('experiences');
      experienceSelected();
    } else if (event.target.value === 'services') {
      setSelected('services');
      serviceSelected();
    } else if (event.target.value === 'promotions') {
      setSelected('promotions');
      promotionSelected();
    } else {
      setSelected('products');
      productSelected();
    }
  };

  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      window.location.href = '/dashboard';
      //window.location.href = `/festival/${festivalId}`;
    }, 2000);

    toast(`Success! Thank you for creating ${selected}!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSubmitAuthDisabled(true);
  };

  const submitProductServiceOrExperience = async (data) => {
    window.scrollTo(0, 0);

    let url = '';
    if (selected === 'products') {
      data.product_creator_type = userRole[0];
      url = '/products/add';
    } else if (selected === 'experiences') {
      url = '/experiences/add';
    } else if (selected === 'services') {
      data.product_creator_type = userRole[0];
      url = '/services/add';
    }

    try {
      const response = await axios({
        method: 'POST',
        url,
        data,
      });

      if (
        response &&
        response.data &&
        response.data.success &&
        userRole &&
        (userRole.includes('VENDOR') || userRole.includes('SPONSOR') || userRole.includes('ADMIN'))
      ) {
        //defaultOnCreatedAction(data.product_festival_id);
        defaultOnCreatedAction();
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    const {...rest} = values;

    let data = {
      ...rest,
    };

    if (selected === 'products') {
      data.product_expiry_time = formattedTimeToDateTime(
        values.product_expiry_time.toString().substring(16, 21),
      );
      data.product_expiry_date = formatDate(values.product_expiry_date);

      await submitProductServiceOrExperience(data);
    } else {
      await submitProductServiceOrExperience(data);
    }
  };

  // Fetch nationalities helper function
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };
  //fetch all festivals
  const fetchFestivalDetails = async () => {
    const url = '/festival/allFestival';

    try {
      const response = await axios({method: 'GET', url});
      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });

    setFestivalList(response.data.festival_list);
  };
  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(true);
    }
  };
  // this localstorage item host_id will be removed at festival page
  localStorage.setItem('host_id', appContext.state.user.id);
  const fetchUserServices = async () => {
    const response = await axios({
      method: 'GET',
      url: `/services/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setServices(response.data.details);
    return response;
  };
  console.log('service appcontext for users', appContext.state.user.id)
  console.log('service from dashboard database for users', services)

  const fetchUserExperiences = async () => {
    const response = await axios({
      method: 'GET',
      url: `/experiences/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setFetchedExperiences(response.data.details);
    return response;
  };

  const fetchUserProducts = async () => {
    const response = await axios({
      method: 'GET',
      url: `/products/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setProducts(response.data.details);
    return response;
  };
  const fetchUserPromotions = async () => {
    const response = await axios({
      method: 'GET',
      url: `/promotions/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setPromotions(response.data.promotions_all);
    return response;
  };
  const fetchUserFoodSamples = async () => {
    const response = await axios({
      method: 'GET',
      url: `/all-products/user/all`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setFoodSamples(response.data.details.data);
    return response;
  };
  const fetchUserTickets = async () => {
    const response = await axios({
      method: 'GET',
      url: `/ticket/all`,
      data: {
        ticket_user_id: appContext.state.user.id,
      },
    });
    setTickets(response.data.details.data);
    return response;
  };
  const fetchSponsorships = async () => {
    const response = await axios({
      method: 'GET',
      /* url: `/sponsor/user/${appContext.state.user.id}`, */
      url: '/user-subscription',
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setSponsorships(response.data.user);
    return response;
  };
  const fetchSponsorshipsInKind = async () => {
    const response = await axios({
      method: 'GET',
      url: `/sponsor/inKind/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setInKindSponsorships(response.data.details);
    return response;
  };

  const fetchMyOrders = async () => {
    const response = await axios({
      method: 'GET',
      url: `/orders/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setMyOrders(response.data.details);
    return response;
  };
  const fetchCurrentOrders = async () => {
    const response = await axios({
      method: 'GET',
      url: `/orders/current/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setCurrentOrders(response.data.details);
    return response;
  };
  // Fetch user product reservations helper function
  const fetchUserRedeemFestivalReservations = async () => {
    try {
      const url = '/all-product-redeem/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = {Authorization: `Bearer ${acc_token}`};
      return await axios({
        method: 'GET',
        url,
        headers,
        params: {
          keyword: filterSearch,
        },
      });
    } catch (error) {
      return error.response;
    }
  };

  // Fetch user experiences reservations helper function
  const fetchUserExperienceRedeemFestivalReservations = async () => {
    try {
      const url = '/all-experience-redeem/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = {Authorization: `Bearer ${acc_token}`};
      return await axios({
        method: 'GET',
        url,
        headers,
        params: {
          keyword: filterSearch,
        },
      });
    } catch (error) {
      return error.response;
    }
  };

  // Fetch user experiences reservations helper function
  const fetchUserServiceRedeemFestivalReservations = async () => {
    try {
      const url = '/all-service-redeem/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = {Authorization: `Bearer ${acc_token}`};
      return await axios({
        method: 'GET',
        url,
        headers,
        params: {
          keyword: filterSearch,
        },
      });
    } catch (error) {
      return error.response;
    }
  };

  // Fetch user festival reservations helper function
  const fetchUserClaimFestivalReservations = async () => {
    try {
      const url = '/all-product-claim/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = {Authorization: `Bearer ${acc_token}`};

      return await axios({method: 'GET', url, headers});
    } catch (error) {
      return error.response;
    }
  };
  // Mount Festival Reservations page
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoading(true);
    fetchUserRedeemFestivalReservations().then(({data}) => {
      setRedeemReservationList(data.details);
    });
    fetchUserClaimFestivalReservations().then(({data}) => {
      console.log(data.details);
      setClaimReservationList(data.details);
    });
    setLoading(false);
  }, [filterSearch]);

  console.log('claim list', claimReservationList);

  const renderUserServicesTable = (arr) => {
    return arr.map((service) => {
      const isSelected = !!selectedServices.find(
        (selectedServices) => service.service_id === selectedServices.service_id,
      );
      return (
        <tr>
          <th scope="row">
            <input
              type="checkbox"
              name="service_checkbox"
              value={isSelected}
              onChange={() => {
                if (isSelected) {
                  setSelectedServices((selectedServices) => {
                    const newService = selectedServices.filter(
                      (selectedService) => selectedService.service_id !== service.service_id,
                    );
                    return newService;
                  });
                } else {
                  setSelectedServices((selectedServices) => selectedServices.concat(service));
                }
              }}
            ></input>
            {service.service_id}
          </th>
          <td>{service.service_name}</td>
          <td>{service.service_price}</td>
          <td>{service.service_description}</td>
          <td>{service.nationality}</td>
          <td>{service.service_size_scope}</td>
          <td>{service.service_capacity}</td>
        </tr>
      );
    });
  };

  // Edit current food sample helper function
  const handleEditFoodSample = (sample) => {
    setCurrentForm('edit-food-sample');

    let daysAvailable = [];
    sample.is_available_on_monday && daysAvailable.push('available_on_monday');
    sample.is_available_on_tuesday && daysAvailable.push('available_on_tuesday');
    sample.is_available_on_wednesday && daysAvailable.push('available_on_wednesday');
    sample.is_available_on_thursday && daysAvailable.push('available_on_thursday');
    sample.is_available_on_friday && daysAvailable.push('available_on_friday');
    sample.is_available_on_saturday && daysAvailable.push('available_on_saturday');
    sample.is_available_on_sunday && daysAvailable.push('available_on_sunday');

    // Set dietary restrictions
    let dietaryRestrictions = [];
    sample.is_vegetarian && dietaryRestrictions.push('vegetarian');
    sample.is_vegan && dietaryRestrictions.push('vegan');
    sample.is_gluten_free && dietaryRestrictions.push('glutenFree');
    sample.is_halal && dietaryRestrictions.push('halal');

    // Split address into address lines 1 and 2
    // let addressParts = sample.address.split(', ');
    // let addressLine1 = addressParts[0];
    // let addressLine2 = addressParts[1];

    // const prefilledValues = {
    //   foodSampleId: sample.food_sample_id,
    //   images: sample.image_urls,
    //   title: sample.title,
    //   nationality_id: sample.nationality_id,
    //   start_date: sample.start_date,
    //   end_date: sample.end_date,
    //   start_time: formattedTimeToDateTime(sample.start_time),
    //   end_time: formattedTimeToDateTime(sample.end_time),
    //   sample_size: sample.sample_size,
    //   quantity: sample.quantity,
    //   dietaryRestrictions,
    //   daysAvailable,
    //   spice_level: sample.spice_level,
    //   addressLine1,
    //   addressLine2,
    //   city: sample.city,
    //   provinceTerritory: sample.state,
    //   postal_code: sample.postal_code,
    //   description: sample.description,
    // };

    history.push({
      pathname: '/edit-food-sample',
      state: {
        foodSampleId: sample.product_id,
        images: sample.image_urls,
        title: sample.title,
        nationality_id: sample.nationality_id,
        start_date: sample.start_date,
        end_date: sample.end_date,
        start_time: formattedTimeToDateTime(sample.start_time),
        end_time: formattedTimeToDateTime(sample.end_time),
        sample_size: sample.sample_size,
        quantity: sample.quantity,
        dietaryRestrictions,
        daysAvailable,
        spice_level: sample.spice_level,
        // addressLine1,
        // addressLine2,
        city: sample.city,
        provinceTerritory: sample.state,
        postal_code: sample.postal_code,
        description: sample.description,
      },
    });

    // return <CreateFoodSample editObj={prefilledValues} />;
  };
  const renderUserFoodSampleTable = (arr) => {
    return arr.map((foodSample) => {
      const isSelected = selectedFoodSamples && selectedFoodSamples.includes(foodSample);
      return (
        <tr>
          <td scope="row">
            <input
              type="checkbox"
              name="foodSample_checkbox"
              onChange={() => {
                if (isSelected) {

                  if (selectedFoodSamples.length === 1) {
                    setSelectedFoodSamples([]);
                  } else {
                    selectedFoodSamples.splice(selectedFoodSamples.indexOf(foodSample), 1);
                    setSelectedFoodSamples(selectedFoodSamples);
                  }
                } else {
                  setSelectedFoodSamples((selectedFoodSamples) =>
                    selectedFoodSamples.concat(foodSample),
                  );
                }
              }}
            ></input>
          </td>
          <td
            class="fas fa-edit"
            value={isSelected}
            onClick={(e) => {
              e.preventDefault();
              handleEditFoodSample(foodSample);
            }}
          ></td>
          <td>{foodSample.product_id}</td>
          <td>{foodSample.title}</td>
          {foodSample.price && foodSample.price !== null ? (<td>$ {foodSample.price}</td>) : (<td>$ 0.00</td>)}
          <td>{foodSample.status}</td>
          {foodSample.discounted_price ? (
            <td>${foodSample.discounted_price}.00</td>
          ) : (
            <td>None </td>
          )}
          <td>{foodSample.quantity}</td>
          <td>{foodSample.nationality}</td>
        </tr>
      );
    });

    // console.log("array from fooddis", arr)
    // return arr.map((row, index) => (
    //   <FoodSamplesDetails
    //     key={index}
    //     foodSampleId={row.food_sample_id}
    //     title={row.title}
    //     price={row.price}
    //     image_urls={row.image_urls}
    //     type={row.food_sample_type}
    //     size={row.sample_size}
    //     spice={row.spice_level}
    //     status={row.status}
    //     quantity={row.quantity}
    //     description={row.description}
    //     nationality={row.nationality}
    //     history={row.history}
    //     address={row.address}
    //     city={row.city}
    //     state={row.state}
    //     postal_code={row.postal_code}
    //     start_date={row.start_date}
    //     end_date={row.end_date}
    //     start_time={row.start_time}
    //     end_time={row.end_time}
    //     is_available_on_monday={row.is_available_on_monday}
    //     is_available_on_tuesday={row.is_available_on_tuesday}
    //     is_available_on_wednesday={row.is_available_on_wednesday}
    //     is_available_on_thursday={row.is_available_on_thursday}
    //     is_available_on_friday={row.is_available_on_friday}
    //     is_available_on_saturday={row.is_available_on_saturday}
    //     is_available_on_sunday={row.is_available_on_sunday}
    //   />
    // ));
  };
  const renderUserProductsTable = (arr) => {
    return arr.map((product) => {
      const isSelected = !!selectedProducts.find(
        (selectedProduct) => product.product_id === selectedProduct.product_id,
      );
      return (
        <tr>
          <th
            className="fa fa-edit"
            scope="row"
            onClick={() => {
              setCurrentForm('edit');
              setProductToEdit(product);
            }}
          >
            {/* <input type="checkbox" name="product_checkbox" value={isSelected} onChange={() => {
              if (isSelected) {
                setSelectedProducts((selectedProducts) => {
                  const newProducts = selectedProducts.filter(selectedProduct => selectedProduct.product_id !== product.product_id);
                  return newProducts;
                });
              } else {
                setSelectedProducts((selectedProducts) => selectedProducts.concat(product));
              }
            }}></input> */}

            {product.product_id}
          </th>
          <td>{product.product_id}</td>
          <td>{product.product_name}</td>
          <td>{product.product_price}</td>
          <td>{product.product_description}</td>
          <td>{product.nationality}</td>
          <td>{product.product_size}</td>
          <td>{product.product_quantity}</td>
        </tr>
      );
    });
  };

  console.log("tickets rendered from dashboard host:", tickets)
  const renderUserTicketsTable = (arr) => {
    return arr.map((row, index) => (
      <TicketDetails
        key={index}
        ticketId={row.ticket_id}
        ticketBookingId={row.ticket_booking_confirmation_id}
        festivalId={row.festival_id}
        images={row.image_urls}
        title={row.festival_name}
        type={row.festival_type}
        price={row.festival_price}
        city={row.festival_city}
        startDate={row.festival_start_date}
        endDate={row.festival_end_date}
        startTime={row.festival_start_time}
        endTime={row.festival_end_time}
        description={row.festival_description}
        //history={props.history}
      />
    ));
  };
  const renderVendingFestivals = (arr) => {
    return arr.map((ticket, index) => {
      console.log("ticket coming from vend ticket:", ticket)
      if (ticket.ticket_type === 'Vendor') {
        return (
          <tr key={index}>
             <td scope="row">
                {' '}
                <Link exact="true" to={`/ticket/${ticket.ticket_id}`} className="ticket-card-link">
                  {ticket.ticket_booking_confirmation_id}
                </Link>
              </td>
              <td>
                {' '}
                <Link
                  exact="true"
                  to={`/festival/${ticket.festival_id}`}
                  onClick={localStorage.setItem('valueFromTicketDashboard', true)}
                  className="ticket-card-link"
                >
                  {ticket.festival_name}
                </Link>
              </td>
              <td>{ticket.festival_type}</td>
              <td>{`${formatDate(ticket.festival_start_date)} at ${ticket.festival_start_time}`}</td>
              <td>{`${formatDate(ticket.festival_end_date)} at ${ticket.festival_end_time}`}</td>
              <td>${ticket.festival_vendor_price}</td>
              <td>
                <strong>Paid</strong>
              </td>
              <td> 
                <button
                  className="nav-vend-btn"
                  onClick={() => {
                    setCurrentForm('add-product-service-experience');
                    setSelectedVendingFestival(ticket.festival_id);
                  }}
                >
                  Add
                </button>
              </td>
          </tr>
        );
        } 
  
      });
  };

  const renderMyOrdersTable = (arr) => {
    return arr.map((order) => {
      if (order.item_type === 'product') {
        order.item_name = order.product_name;
        order.festival_name = order.product_festival;
      } else if (order.item_type === 'service') {
        order.item_name = order.service_name;
        order.festival_name = order.service_festival;
      }
      return (
        <tr>
          <th scope="row">{order.order_datetime}</th>
          <td>{order.item_id}</td>
          <td>{order.item_name}</td>
          <td>{order.quantity}</td>
          <td>{order.total_tax}</td>
          <td>{order.total_amount_after_tax}</td>
          <td>{order.festival_name}</td>
        </tr>
      );
    });
  };
  const renderCurrentOrdersTable = (arr) => {
    return arr.map((order) => {
      if (order.item_type === 'product') {
        order.item_name = order.product_name;
        order.festival_name = order.product_festival;
      } else if (order.item_type === 'service') {
        order.item_name = order.service_name;
        order.festival_name = order.service_festival;
      }

      return (
        <tr key={order.item_id}>
          <th scope="row">{order.order_datetime}</th>
          <td>{order.item_id}</td>
          <td>{order.item_name}</td>
          <td>{order.quantity}</td>
          <td>{order.total_tax}</td>
          <td>{order.total_amount_after_tax}</td>
          <td>{order.festival_name}</td>
        </tr>
      );
    });
  };

  const StarRating = (props) => {
    const [stars, setStars] = useState([1, 2, 3, 4, 5]);
    //const [rating, setRating] = useState(0);
    const [hovered, setHovered] = useState(0);
    const [selectedIcon, setSelectedIcon] = useState(<i className="fas fa-star"></i>);
    const [deselectedIcon, setDeselectedIcon] = useState(<i className="far fa-star"></i>);

    let outOf = 5;

    const changeRating = (newRating) => {
      props.setRating(newRating);

      /* if (props.onChange(newRating)) {
        props.onChange(newRating);
      } */
    };
    const hoverRating = (rating) => {
      setHovered(rating);
    };

    return (
      <div>
        <div className="rating" style={{fontSize: '1em'}}>
          {stars.map((star, index) => {
            return (
              <span
                key={index}
                style={{cursor: 'pointer'}}
                onClick={() => {
                  changeRating(star);
                }}
                onMouseEnter={() => {
                  hoverRating(star);
                }}
                onMouseLeave={() => {
                  hoverRating(0);
                }}
              >
                {props.rating < star
                  ? hovered < star
                    ? deselectedIcon
                    : selectedIcon
                  : selectedIcon}
              </span>
            );
          })}
        </div>
      </div>
    );
  };

  const RenderUserReviews = (props) => {
    //props.reviews.map((review) => {

    const [tasteRating, setTasteRating] = useState(0);
    const [authenticityRating, setAuthenticityRating] = useState(0);
    const [serviceRating, setServiceRating] = useState(0);
    const [transitRating, setTransitRating] = useState(0);
    const [environmentRating, setEnvironmentRating] = useState(0);
    const [finalScore, setFinalScore] = useState(0);
    const [additionalInfo, setAdditionalInfo] = useState('');
    let data = {};

    const handleSubmitUserReview = async (event) => {
      event.preventDefault();
      //console.log(event);
      //api call to backend
      let data = {
        tasteRating,
        authenticityRating,
        serviceRating,
        transitRating,
        environmentRating,
        finalScore,
        additionalInfo,
        review_id: props.review.review_id,
      };
      console.log(data);
      console.log(props.review);

      const response = await axios({
        method: 'POST',
        url: `/reviews/add`,
        data: data,
        params: {review_id: props.review.review_id},
      });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          // window.location.reload();
          window.location.href = `/dashboard`;
        }, 2000);

        toast(`Success! Thank you for submitting a review!`, {
          type: 'success',
          autoClose: 2000,
        });
      } else {
        toast(`Error! Something went wrong!`, {
          type: 'error',
          autoClose: 2000,
        });
      }
    };

    const calculateFinalScore = () => {
      let average =
        (Number(tasteRating) +
          Number(authenticityRating) +
          Number(serviceRating) +
          Number(transitRating) +
          Number(environmentRating)) /
        5;
      setFinalScore(average);
    };

    useEffect(() => {
      calculateFinalScore();
    }, [tasteRating, authenticityRating, serviceRating, transitRating, environmentRating]);

    return (
      <div className="home-tab-container">
        <div className="row">
          <div className="home-tab-title">Reviews</div>
        </div>
        <div className="container reviews__container">
          <div className="row reviews__card">
            <div className="col-md-3 reviews__image-container">
              <img
                className="reviews__product-img"
                src={
                  props.review.image_urls ? props.review.image_urls[0] : 'https://picsum.photos/500'
                }
              ></img>
            </div>
            <div className="col-md-9 reviews__main-info">
              <div className="reviews__name-and-rating">
                <div className="reviews__product-name">{props.review.title}</div>
                <div className="reviews__product-rating">
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                  <span className="reviews__rating-quantity">(46)</span>
                </div>
              </div>
              <div className="reviews__restaurant-name">
                <i className="fas fa-map-marker-alt"></i>
                {props.review.business_name}
              </div>
              <div className="reviews__restaurant-scores">
                <div className="reviews__category">
                  Taste
                  <span className="reviews__category-rating">
                    {/* <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i> */}
                    <StarRating rating={tasteRating} setRating={setTasteRating}></StarRating>
                  </span>
                </div>
                <div className="reviews__category">
                  Authenticity
                  <span className="reviews__category-rating">
                    {/* <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i> */}
                    <StarRating
                      rating={authenticityRating}
                      setRating={setAuthenticityRating}
                    ></StarRating>
                  </span>
                </div>
                <div className="reviews__category">
                  Service
                  <span className="reviews__category-rating">
                    {/* <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i> */}
                    <StarRating rating={serviceRating} setRating={setServiceRating}></StarRating>
                  </span>
                </div>
                <div className="reviews__category">
                  Transit
                  <span className="reviews__category-rating">
                    {/* <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i> */}
                    <StarRating rating={transitRating} setRating={setTransitRating}></StarRating>
                  </span>
                </div>
                <div className="reviews__category">
                  Environment
                  <span className="reviews__category-rating">
                    {/* <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i>
                    <i className="far fa-star"></i> */}
                    <StarRating
                      rating={environmentRating}
                      setRating={setEnvironmentRating}
                    ></StarRating>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div className="row reviews__additional">
            <div className="col-md-12 reviews__additional-info-input">
              <input
                placeholder="Additional Comments"
                value={additionalInfo}
                onChange={(event) => {
                  setAdditionalInfo(event.target.value);
                }}
              />
            </div>
          </div>

          <div className="row reviews__final-score-container">
            <div className="col-md-12 reviews__final-score">
              Your TASTE score for this restaurant is
              <span className="reviews__category-rating">
                {/* <i className="far fa-star"></i>
                <i className="far fa-star"></i>
                <i className="far fa-star"></i>
                <i className="far fa-star"></i>
                <i className="far fa-star"></i> */}
                {finalScore}
              </span>
            </div>
            <div className="col-md-2 reviews__final-submission">
              <button className="reviews__submit-button" onClick={handleSubmitUserReview}>
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    );
    //});
  };
  const renderInKindSponsorshipTable = (arr) => {
    return arr.map((sponsor) => (
      <tr>
        <th scope="row">{sponsor.festival_id}</th>
        <td>{sponsor.product_name}</td>
        <td>{sponsor.festival_name}</td>
        <td>
          {sponsor.festival_start_date ? (
            formatDate(sponsor.festival_start_date)
          ) : (
            <div>No date</div>
          )}{' '}
          {sponsor.festival_start_time
            ? formatMilitaryToStandardTime(sponsor.festival_start_time)
            : null}
        </td>
        <td>
          {sponsor.festival_end_date ? formatDate(sponsor.festival_end_date) : null}{' '}
          {sponsor.festival_end_time
            ? formatMilitaryToStandardTime(sponsor.festival_end_time)
            : null}
        </td>
        <td>${sponsor.festival_price}</td>
        <td>Sponsored</td>
      </tr>
    ));
  };
  const renderSponsorshipTable = (arr) => {
    return arr.map((sponsor) => {
      let inCash;
      if (sponsor.subscription_code === 'S_C1') {
        inCash = 1500.0;
      } else if (sponsor.subscription_code === 'S_C2') {
        inCash = 5000.0;
      } else if (sponsor.subscription_code === 'S_C3') {
        inCash = 20000.0;
      }
      return (
        <tr>
          <th scope="row">{sponsor.festival_id}</th>
          <td>${inCash}</td>
          <td>{sponsor.festival_name}</td>
          <td>
            {sponsor.festival_start_date ? (
              formatDate(sponsor.festival_start_date)
            ) : (
              <div>No date</div>
            )}{' '}
            {sponsor.festival_start_time
              ? formatMilitaryToStandardTime(sponsor.festival_start_time)
              : null}
          </td>
          <td>
            {sponsor.festival_start_date ? formatDate(sponsor.festival_end_date) : null}{' '}
            {sponsor.festival_end_time
              ? formatMilitaryToStandardTime(sponsor.festival_end_time)
              : null}
          </td>
          <td>{sponsor.festival_price}</td>
          <td>Sponsored</td>
        </tr>
      );
    });
  };

  const handleHomeClick = () => {
    setCurrentForm('home');
  };
  const handleSponsorClick = () => {
    setCurrentForm('sponsor');
  };
  const handleExperienceClick = () => {
    setCurrentForm('experiences');
  };
  const handleOrdersClick = () => {
    setCurrentForm('orders');
  };
  const handleRedemptionsClick = () => {
    setCurrentForm('redemptions');
  };
  const handleMyClaimsClick = () => {
    setCurrentForm('claims');
  };
  const handleMyTicketsClick = () => {
    setCurrentForm('tickets');
  };
  const handleReviewsClick = () => {
    setCurrentForm('reviews');
  };
  const handleMyProductsClick = () => {
    setCurrentForm('products');
  };
  const handleMyServicesClick = () => {
    setCurrentForm('services');
  };
  const handleMyPromotionsClick = () => {
    setCurrentForm('promotions');
  };
  const handleMyFoodSamplesClick = () => {
    setCurrentForm('food-samples');
  };
  const handleMyVendingFestivalsClick = () => {
    setCurrentForm('vending-festivals');
  };
  const handleProductServiceExperienceClick = () => {
    setSelectedVendingFestival(null);
    setCurrentForm('add-product-service-experience');
  };
  const sponsorInKind = () => {
    setSponsorshipForm('sponsorInKind');
  };
  const sponsorInCash = () => {
    setSponsorshipForm('sponsorInCash');
  };

  const handleFoodSampleFestival = async (event) => {
    event.preventDefault();

    setFoodSampleFestivalOpened(true);
  };

  const handlePromotionOpened = async (event) => {
    event.preventDefault();
    setPromotionOpened(true);
  };

  const handlePromotionApplyToProduct = async () => {
    const url = '/promotions/apply/product';
    const acc_token = localStorage.getItem('access_token');
    const headers = {Authorization: `Bearer ${acc_token}`};
    console.log(selectedFoodSamples);
    const data = {
      promotion: selectedPromotions,
      products: selectedFoodSamples,
    };
    let response;
    try {
      response = await axios({method: 'PUT', url, headers, data});

      if (response && response.data && response.data.success) {
        // setTimeout(() => {
        //   closeModal('apply-promotion')
        // }, 2000);

        closeModal('apply-promotion');
        await fetchUserFoodSamples();
        setCurrentForm('food-samples');
        toast(`Success! Promotions applied to product!`, {
          type: 'success',
          autoClose: 2000,
        });
        // setSubmitAuthDisabled(true);
      } else if (!response.data.success) {
        toast('Error! '.concat(response.data.message), {
          type: 'error',
          autoClose: 2000,
        });
      }
      return response;
      setRefreshUserFoodSamples(true);
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const handleRemovePromotion = async () => {
    const data = {
      products: selectedFoodSamples,
    };
    let response;
    try {
      response = await axios({method: 'PUT', url: `/promotions/remove-from-product`, data});
      if (response && response.data && response.data.success) {
        await fetchUserFoodSamples();
        setCurrentForm('food-samples');
        toast(`Success! Promotions removed to product!`, {
          type: 'success',
          autoClose: 2000,
        });
      } else if (!response.data.success) {
        toast('Error! '.concat(response.data.message), {
          type: 'error',
          autoClose: 2000,
        });
      }
      return response;
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const handleFoodSampleSubmitFestival = async () => {
    const url = '/host-festival';
    const acc_token = localStorage.getItem('access_token');
    const headers = {Authorization: `Bearer ${acc_token}`};
    const data = {
      festival_id: festivalPreference.map((festival) => {
        return festival.festival_id;
      }),
      festival_restaurant_host_id: appContext.state.user.id,
      foodSamplePreference: selectedFoodSamples.map((sample) => {
        return sample.product_id;
      }),
    };

    try {
      const response = await axios({method: 'POST', url, headers, data});
      if (response && response.data && response.data.success) {
        setTimeout(() => {
          // window.location.reload();
          window.location.href = `/`;
        }, 2000);

        toast(`Success! Thank you for hosting!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onChangeFestival = function (event) {
    //takes the file and reads the file from buffer of array

    if (festivalPreference.includes(event.target.value)) {
    } else {
      const matchedFestival = festivalDetails.find((value) => {
        return value.festival_id === Number(event.target.value);
      });
      setFestivalPreference(festivalPreference.concat(matchedFestival));
      //setFestivalPreferenceName(festivalPreferenceName.concat(matchedFestival.festival_name))
    }
  };

  const onPromotionSelect = function (event) {
    const selectedPromo = promotions.find((value) => {
      return value.promotion_name === event.target.value;
    });
    setSelectedPromotions(selectedPromo);
  };

  const getSelectedPromotions = function (promotions) {
    console.log(promotions);
    return (
      <button className="festival-card-festival-attend-btn">{promotions.promotion_name}</button>
    );
  };

  //function to render over the selected options from dropdown and print them
  const getOptions = function (festivals) {
    return festivals.map((festival) => (
      <button
        className="festival-card-festival-attend-btn"
        onClick={(event) => {
          setFestivalPreference((stateFestivals) => {
            return stateFestivals.filter((stateFestival) => {
              if (stateFestival.festival_id === festival.festival_id) {
                return false;
              } else {
                return true;
              }
            });
          });
        }}
      >
        {festival.festival_name}
      </button>
    ));
  };

  const fetchAll = async () => {
    await fetchUserServices();
    await fetchUserReviews();
    await fetchUserFoodSamples();
    await fetchUserProducts();
    await fetchUserPromotions();
    await fetchUserTickets();
    await fetchSponsorships();
    await fetchSponsorshipsInKind();
    await fetchMyOrders();
    await fetchCurrentOrders();
    await fetchNationalities();
    await fetchFestivalList();
    await fetchUserExperiences();
    await fetchFestivalDetails().then((response) => {
      setFestivalDetails(response.data.festival_list);
    });
    await fetchUserRedeemFestivalReservations().then(({data}) => {
      setRedeemReservationList(data.details);
    });
    await fetchUserClaimFestivalReservations().then(({data}) => {
      console.log(data.details);
      setClaimReservationList(data.details);
    });
    await fetchUserExperienceRedeemFestivalReservations().then(({data}) => {
      setExperienceRedeemReservationList(data.details);
    });
    await fetchUserServiceRedeemFestivalReservations().then(({data}) => {
      setServiceRedeemReservationList(data.details);
    });

    setLoading(false);
  };
  console.log("redeemReservationList", redeemReservationList)
  console.log("serviceRedeemReservationList", serviceRedeemReservationList)
  useEffect(() => {
    // setTimeout(() => {
    //   setCurrentForm('time-out');
    //   // window.location.href='/dashboard'
    //   setSelectedFoodSamples([]);
    //   fetchUserFoodSamples();
    // }, 2000);
    // setTimeout(() => {
    //   setCurrentForm('tickets');
    // }, 4000);
    fetchAll();
  }, []);
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchFestivalDetails().then((response) => {
      setFestivalDetails(response.data.festival_list);
    });
  }, []);

  const deleteProducts = async (data) => {
    const response = await axios({
      method: 'DELETE',
      url: `/products/delete/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
        delete_items: selectedProducts,
      },
    });
    setRefreshUserProducts(true);
    //setSponsorships(response.data.details)
    return response;
  };
  useEffect(() => {
    fetchUserProducts();
    setRefreshUserProducts(false);
  }, [refreshUserProducts]);

  const fetchUserReviews = async () => {
    const response = await axios({
      method: 'GET',
      url: `/reviews/user/${appContext.state.user.id}`,
      params: {
        //productId: props.productId,
      },
    });
    console.log(response);
    if (response && response.data && response.data.success && response.data.details) {
      setReviewInformation(response.data.details);
    }
    console.log(response);
  };

  /* useEffect(() => {
    fetchUserReviews();
  }, []); */

  const deleteServices = async (data) => {
    const response = await axios({
      method: 'DELETE',
      url: `/services/delete/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
        delete_items: selectedServices,
      },
    });
    setRefreshUserServices(true);
    //setSponsorships(response.data.details)
    return response;
  };
  const deleteTickets = async (data) => {
    const response = await axios({
      method: 'DELETE',
      url: `/tickets/delete/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
        delete_items: selectedTickets,
      },
    });
    setRefreshUserTickets(true);
    //setSponsorships(response.data.details)
    //return response;
  };

  const deleteExperiences = async (data) => {
    const response = await axios({
      method: 'DELETE',
      url: `/experience/delete/user/${appContext.state.user.id}`,
      data: {
        user_id: appContext.state.user.id,
        delete_items: JSON.parse(localStorage.getItem('experienceIds')),
      },
    });
    setrefreshUserExperience(true);
    setTimeout(() => {
      setCurrentForm('time-out');
      fetchUserExperiences();
    }, 2000);
    setTimeout(() => {
      setCurrentForm('experiences');
    }, 4000);
    //setSponsorships(response.data.details)
    return response;
  };
  useEffect(() => {
    fetchUserExperiences();
    setrefreshUserExperience(false);
  }, [refreshUserExperience]);

  const deleteFoodSamples = async (data) => {
    try {
      const response = await axios({
        method: 'DELETE',
        url: `/food-sample/delete/user/${appContext.state.user.id}`,
        data: {
          user_id: appContext.state.user.id,
          delete_items: selectedFoodSamples,
        },
      });

      setTimeout(() => {
        setCurrentForm('time-out');
        // window.location.href='/dashboard'
        setSelectedFoodSamples([]);
        fetchUserFoodSamples();
      }, 2000);
      setTimeout(() => {
        setCurrentForm('food-samples');
      }, 4000);
      toast('Product removed successfully!', {
        type: 'success',
        autoClose: 2000,
      });
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
    // return response;
  };
  useEffect(() => {
    fetchUserServices();
    setRefreshUserServices(false);
  }, [refreshUserServices]);

  useEffect(() => {
    fetchUserTickets();
    setRefreshUserTickets(false);
  }, [refreshUserTickets]);

  useEffect(() => {
    fetchUserFoodSamples();
    setRefreshUserFoodSamples(false);
  }, [refreshUserFoodSamples]);

  return (
    <div className="dashboard">
      <Modal
        isOpen={foodSampleFestivalOpened}
        onRequestClose={closeModal('passport-details')}
        ariaHideApp={false}
        className="passport-details-modal"
      >
        <div className="mb-3">
          <h5>Which festival would you like to add your products to ?</h5>
          <div className="input-title">Select all that apply</div>
          <select
            name="festival"
            label="List festival"
            onChange={onChangeFestival}
            disabled={submitAuthDisabled}
            // required
          >
            <option value="" name="">
              --Select--
            </option>
            {festivalDetails.map((n) => (
              <option key={n.festival_id} value={n.festival_id} name={n.festival_name}>
                {n.festival_name}
              </option>
            ))}
          </select>
          {festivalPreference.length === 0 ? (
            <div className="col-md-4 p-2">
              <p></p>
            </div>
          ) : (
            <div className="col-md-4">{getOptions(festivalPreference)}</div>
          )}
        </div>

        <div
          onClick={handleFoodSampleSubmitFestival}
          disabled={submitAuthDisabled}
          className={'mt-3 call-to-action-btn'}
        >
          Add to Festival
        </div>
        <div className="close-modal-bottom-content">
          <span onClick={closeModal('passport-details')} className="close-modal-bottom">
            Close
          </span>
        </div>
      </Modal>
      <Modal
        isOpen={promotionOpened}
        onRequestClose={closeModal('apply-promotion')}
        ariaHideApp={false}
        className="passport-details-modal"
      >
        <div className="mb-3">
          <h5>Which promotion would you like to apply?</h5>
          <div className="input-title">Select only one</div>
          <select
            name="festival"
            label="List festival"
            onChange={onPromotionSelect}
            disabled={submitAuthDisabled}
            // required
          >
            <option value="" name="">
              --Select--
            </option>
            {promotions.map((n) => (
              <option key={n.promotion_id} value={n.promotion_name} name={n.promotion_name}>
                {n.promotion_name}
              </option>
            ))}
          </select>
          {/* {selectedPromotions.length === 0 ? (
            <div className="col-md-4 p-2">
              <p></p>
            </div>
          ) : (
            <div className="col-md-4">{getSelectedPromotions(selectedPromotions)}</div>
          )} */}
        </div>
        <div
          onClick={handlePromotionApplyToProduct}
          disabled={submitAuthDisabled}
          className={'mt-3 call-to-action-btn'}
        >
          Apply Promotion
        </div>
        <div className="close-modal-bottom-content">
          <span onClick={closeModal('apply-promotion')} className="close-modal-bottom">
            Close
          </span>
        </div>
      </Modal>
      <Modal
        isOpen={createFormsOpened}
        onRequestClose={closeModal('create-forms')}
        ariaHideApp={false}
        className="dashboard-forms-modal"
      >
        <div>
          <div className="row">
            <div className="col-md-6 my-auto dashboard-forms-input-content-col-1">
              <div className="dashboard-forms-title mb-3">Add</div>
            </div>
            <div className="col-md-6 dashboard-forms-input-content-col-3">
              <select
                name="select-form"
                selected="products"
                onChange={changeForm}
                disabled={submitAuthDisabled}
                className="custom-select mb-3"
                required
              >
                <option value="products">Products</option>
                <option value="services">Services</option>
                <option value="experiences">Experiences</option>
              </select>
            </div>
            <div className="dashboard-forms-close">
              <button
                aria-label={`Close Forms Modal`}
                onClick={closeModal('create-forms')}
                className="fas fa-times fa-2x close-modal"
              ></button>
            </div>
          </div>
          <Form data={data} onSubmit={onSubmit}>
            <div className="row">
              <div className="col-md-6 dashboard-forms-input-content-col-1">
                <Input
                  name={formDetails.input_one}
                  placeholder={formDetails.input_one_placeholder}
                  disabled={submitAuthDisabled}
                  className="product-name-input"
                  required
                  label={null}
                />
              </div>
              <div className="col-md-6 dashboard-forms-input-content-col-3">
                {nationalities.length && (
                  <Select
                    name={formDetails.input_two}
                    placeholder={formDetails.input_two_placeholder}
                    className="product-name-input"
                    disabled={submitAuthDisabled}
                    required
                  >
                    <option value="">--Nationality--</option>
                    {nationalities.map((n) => (
                      <option key={n.id} value={n.id}>
                        {n.nationality}
                      </option>
                    ))}
                  </Select>
                )}
              </div>
            </div>
            <div className="row">
              <div className="col-md-4 dashboard-forms-input-content-col-1">
                <Input
                  name={formDetails.input_three}
                  step="0.01"
                  placeholder={formDetails.input_three_placeholder}
                  className="product-name-input"
                  disabled={submitAuthDisabled}
                  required
                />
              </div>
              <div className="col-md-4 dashboard-forms-input-content-col-2">
                <Input
                  name={formDetails.input_four}
                  type="number"
                  placeholder={formDetails.input_four_placeholder}
                  min="1"
                  className="product-name-input"
                  disabled={submitAuthDisabled}
                  required
                />
              </div>
              <div className="col-md-4 dashboard-forms-input-content-col-3">
                <Select
                  name={formDetails.input_five}
                  placeholder={formDetails.input_five_placeholder}
                  className="product-name-input"
                  disabled={submitAuthDisabled}
                  required
                >
                  <option value="">{`--${
                    formDetails.input_five === 'product_size' ? 'Size' : 'Scope'
                  }--`}</option>
                  <option value="Bite Size">Bite Size</option>
                  <option value="Quarter">Quarter</option>
                  <option value="Half">Half</option>
                  <option value="Full">Full</option>
                </Select>
              </div>
            </div>
            {selected === 'products' && (
              <div className="row">
                <div className="col-md-6 dashboard-forms-input-content-col-1">
                  <DateInput
                    name={formDetails.input_seven}
                    placeholderText={formDetails.input_seven_placeholder}
                    dateFormat="yyyy/MM/dd"
                    minDate={new Date()}
                    peekNextMonth
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                    disabled={submitAuthDisabled}
                    className="product-name-input"
                    required
                  />
                </div>
                <div className="col-md-6 dashboard-forms-input-content-col-3">
                  <DateInput
                    name={formDetails.input_eight}
                    placeholderText={formDetails.input_eight_placeholder}
                    showTimeSelect
                    showTimeSelectOnly
                    dateFormat="h:mm aa"
                    disabled={submitAuthDisabled}
                    className="last-date-input"
                    required
                  />
                </div>
              </div>
            )}
            <div className="first-row">
              <Textarea
                name={formDetails.input_six}
                placeholder={formDetails.input_six_placeholder}
                disabled={submitAuthDisabled}
                className="product-name-input"
                required
              />
            </div>
            <div className="first-row">
              {selected === 'products' && (
                <MultiImageInput
                  name="product_images"
                  dropbox_label="Click or drag-and-drop to upload one or more images"
                  disabled={submitAuthDisabled}
                  className="product-images"
                  required
                />
              )}
              {selected === 'services' && (
                <MultiImageInput
                  name="service_images"
                  dropbox_label="Click or drag-and-drop to upload one or more images"
                  disabled={submitAuthDisabled}
                  className="product-images"
                  required
                />
              )}
              {selected === 'experiences' && (
                <MultiImageInput
                  name="experience_images"
                  dropbox_label="Click or drag-and-drop to upload one or more images"
                  disabled={submitAuthDisabled}
                  className="product-images"
                  required
                />
              )}
            </div>
            {festivalList.length ? (
              <>
                <div>{`Which festival does this ${
                  selected === 'products'
                    ? 'product'
                    : selected === 'services'
                    ? 'service'
                    : 'experience'
                } belong to?`}</div>
                <div>
                  <Select
                    name={formDetails.input_ten}
                    className="product-name-input"
                    disabled={submitAuthDisabled}
                    required
                  >
                    <option value="">--Select--</option>
                    {festivalList.map((f) => (
                      <option key={f.festival_id} value={f.festival_id}>
                        {f.festival_name}
                      </option>
                    ))}
                  </Select>
                </div>
              </>
            ) : null}

            <div className="first-row">
              <button type="submit" className="dashboard-forms-add-btn">
                Add
              </button>
            </div>
          </Form>
        </div>
      </Modal>
      {!loading && (
        <div>
          <section className="passport-sections dashboard-tab-switcher ">
            <div className="container dashboard-host-container">
              <div className="row">
                <div className="tab-row">
                  <div className="tab-switcher">
                    <button
                      onClick={() => toggleTab(1)}
                      className={toggleState === 1 ? 'tab active-tab' : 'tab'}
                    >
                      Personal Dashboard
                    </button>
                    <button
                      onClick={() => toggleTab(2)}
                      className={toggleState === 2 ? 'tab active-tab' : 'tab'}
                    >
                      Restaurant Dashboard
                    </button>
                    {userRole && userRole.includes('ADMIN') ? (
                      <button
                        onClick={() => toggleTab(3)}
                        className={toggleState === 3 ? 'tab active-tab' : 'tab'}
                      >
                        Admin Panel
                      </button>
                    ) : null}
                    {userRole && userRole.includes('HOST')  ? (
                      <button
                      onClick={() => toggleTab(4)}
                      className={toggleState === 4 ? 'tab active-tab' : 'tab'}
                     >
                      Host Panel
                     </button>
                    ) : null}
                    {/* {toggleState === 2 ? (
                      <div
                        onClick={() => {
                          setCurrentForm('add-remove');
                        }}
                        className="add-product-service-button"
                      >
                        <button className="tab">
                          <i className="fas fa-plus"></i>
                        </button>
                      </div>
                    ) : null} */}
                  </div>
                  {toggleState && toggleState === 2 && userRole && (userRole.includes('HOST') ||
                    userRole.includes('HOST_PENDING') ||
                    userRole.includes('RESTAURANT') ||
                    userRole.includes('RESTAURANT_PENDING') ||
                    userRole.includes('SPONSOR') ||
                    userRole.includes('SPONSOR_PENDING') ||
                    userRole.includes('VENDOR') ||
                    userRole.includes('BUSINESS_MEMBER_PENDING') ||
                    userRole.includes('BUSINESS_MEMBER') ||
                    userRole.includes('VENDOR_PENDING')) ? (
                    <div className="col add-remove-action-buttons">
                      <button
                        className="add-action-button"
                        onClick={handleProductServiceExperienceClick}
                      >
                        {'Add New Products, Services or Experiences  '}
                        {/* <i class="fas fa-plus add-credits"></i>{' '} */}
                      </button>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </section>
          {toggleState && toggleState === 1 && userRole ? (
            <div className="row">
              <div className="col sticky-dash-nav">
                
                <button
                  className={currentForm === 'tickets' ? 'button-current-form' : null}
                  onClick={handleMyTicketsClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Tickets
                </button>
                {/* <div className="home-tab" onClick={handleHomeClick}>
                  <img src={homeImage} />
                  <div>Home</div>
                </div> */}
                <button
                  className={currentForm === 'claims' ? 'button-current-form' : null}
                  onClick={handleMyClaimsClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Claims
                </button>
                <button
                  className={currentForm === 'orders' ? 'button-current-form' : null}
                  onClick={handleOrdersClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Orders
                </button>
                <button
                  className={currentForm === 'reviews' ? 'button-current-form' : null}
                  onClick={handleReviewsClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  Reviews
                </button>
              </div>
              <div className="col-md-10">
                {currentForm && currentForm === 'tickets' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="home-tab-title">My Tickets</div>
                    </div>
                    <form onSubmit={formMethods.handleSubmit(deleteTickets)}>
                      <table className="table">
                        <thead>
                        <tr>
                          <th scope="col">Ticket ID</th>
                          <th scope="col">Festival Name</th>
                          <th scope="col">Festival Type</th>
                          <th scope="col">Ticket Start Date & Time</th>
                          <th scope="col">Ticket End Date & Time</th>
                          <th scope="col">Ticket Price</th>
                          <th scope="col">Ticket Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {tickets.length !== 0 ? (
                          renderUserTicketsTable(tickets)
                        ) : (
                          <tr>
                            <td>You don't have any tickets yet!</td>
                          </tr>
                        )}
                        </tbody>
                      </table>
                    </form>
                  </div>
                )}

                {currentForm && currentForm === 'orders' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="home-tab-title">My Orders</div>
                    </div>
                    <div className="row">
                      {/* <div className="home-tab-subtitle">Current Orders </div> */}
                    </div>
                    <table className="table">
                      <thead>
                      <tr>
                        <th scope="col">Date & Time</th>
                        <th scope="col">Product ID</th>
                        <th scope="col">Product name</th>
                        <th scope="col">Quantities</th>
                        <th scope="col">Unit Price</th>
                        <th scope="col">Price Sold</th>
                        <th scope="col">Festival name</th>
                      </tr>
                      </thead>
                      <tbody>{renderMyOrdersTable(myOrders)}</tbody>
                    </table>
                  </div>
                )}

                {currentForm && currentForm === 'claims' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="home-tab-title">My Claims</div>
                    </div>
                    <table className="table">
                      <thead>
                      <tr>
                        <th scope="col">Food Claim Id</th>
                        <th scope="col">Claimant Full name</th>
                        <th scope="col">Festival Name</th>
                        <th scope="col">Name of Item</th>
                        <th scope="col">Date of Claim</th>
                        <th scope="col">Quantity of claims</th>
                        <th scope="col">Claim Status</th>
                      </tr>
                      </thead>
                      <tbody>
                      {claimReservationList.length !== 0 ? (
                        renderClaimsRows(claimReservationList)
                      ) : (
                        <tr>
                          <td>You haven't claimed any sample yet!</td>
                        </tr>
                      )}
                      </tbody>
                    </table>
                  </div>
                )}

                {currentForm &&
                currentForm === 'reviews' &&
                reviewInformation &&
                reviewInformation.map((review) => (
                  <RenderUserReviews review={review}></RenderUserReviews>
                ))}

                {/* {currentForm && currentForm === 'reviews' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="home-tab-title">Reviews</div>
                    </div>
                    <div className="container reviews__container">
                      <div className="row reviews__card">
                        <div className="col-md-3 reviews__image-container">
                          <img
                            className="reviews__product-img"
                            src="https://picsum.photos/500"
                          ></img>
                        </div>
                        <div className="col-md-9 reviews__main-info">
                          <div className="reviews__name-and-rating">
                            <div className="reviews__product-name">Example Product</div>
                            <div className="reviews__product-rating">
                              <i className="fas fa-star"></i>
                              <i className="fas fa-star"></i>
                              <i className="fas fa-star"></i>
                              <i className="fas fa-star"></i>
                              <i className="fas fa-star"></i>
                              <span className="reviews__rating-quantity">(46)</span>
                            </div>
                          </div>
                          <div className="reviews__restaurant-name">
                            <i className="fas fa-map-marker-alt"></i>
                            Example Restaurant
                          </div>
                          <div className="reviews__restaurant-scores">
                            <div className="reviews__category">
                              Taste
                              <span className="reviews__category-rating">
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                              </span>
                            </div>
                            <div className="reviews__category">
                              Authenticity
                              <span className="reviews__category-rating">
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                              </span>
                            </div>
                            <div className="reviews__category">
                              Service
                              <span className="reviews__category-rating">
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                              </span>
                            </div>
                            <div className="reviews__category">
                              Environment
                              <span className="reviews__category-rating">
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                              </span>
                            </div>
                            <div className="reviews__category">
                              Transit
                              <span className="reviews__category-rating">
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                                <i className="far fa-star"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="row reviews__additional">
                        <div className="col-md-12 reviews__additional-info-input">
                          <input placeholder="Additional Comments" />
                        </div>
                      </div>

                      <div className="row reviews__final-score-container">
                        <div className="col-md-10 reviews__final-score">
                          Your TASTE score for this restaurant is
                          <span className="reviews__category-rating">
                            <i className="far fa-star"></i>
                            <i className="far fa-star"></i>
                            <i className="far fa-star"></i>
                            <i className="far fa-star"></i>
                            <i className="far fa-star"></i>
                          </span>
                        </div>
                        <div className="col-md-2 reviews__final-submission">
                          <button className="reviews__submit-button">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                )} */}
              </div>
            </div>
          ) : toggleState === 2 &&
          userRole &&
          !userRole.includes('HOST') &&
          !userRole.includes('HOST_PENDING') &&
          !userRole.includes('RESTAURANT') &&
          !userRole.includes('RESTAURANT_PENDING') &&
          !userRole.includes('SPONSOR') &&
          !userRole.includes('SPONSOR_PENDING') &&
          !userRole.includes('VENDOR') &&
          !userRole.includes('BUSINESS_MEMBER_PENDING') &&
          !userRole.includes('BUSINESS_MEMBER') &&
          !userRole.includes('VENDOR_PENDING') ? (
            <ApplyForBusinessPassport/>
          ) : toggleState === 2 && userRole ? (
            <div className="row">
              <div className="col sticky-dash-nav">
                <button
                  className={currentForm === 'home' ? 'button-current-form' : null}
                  onClick={handleHomeClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  Current Orders
                </button>
                {/* <div className="home-tab" onClick={handleHomeClick}>
                  <img src={homeImage} />
                  <div>Home</div>
                </div> */}
                {/* <button
                  className={currentForm === 'add-remove' ? 'button-current-form' : null}
                  onClick={() => {
                    setCurrentForm('add-remove');
                  }}
                >
                  Create
                </button> */}
                {/* <button
                  className={
                    currentForm === 'products' || currentForm === 'add-remove-product'
                      ? 'button-current-form'
                      : null
                  }
                  onClick={handleMyProductsClick}
                > */}
                {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                {/*  My Products
                </button> */}
                <button
                  className={
                    currentForm === 'food-samples' || currentForm === 'add-food-sample'
                      ? 'button-current-form'
                      : null
                  }
                  onClick={handleMyFoodSamplesClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Products
                </button>

                <button
                  className={
                    currentForm === 'vending-festivals' || currentForm === 'add-remove-vending-festivals'
                      ? 'button-current-form'
                      : null
                  }
                  onClick={handleMyVendingFestivalsClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Vending Festivals
                </button>

                <button
                  className={
                    currentForm === 'promotions' || currentForm === 'add-remove-promotion'
                      ? 'button-current-form'
                      : null
                  }
                  onClick={handleMyPromotionsClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Promotions
                </button>
                <button
                  className={
                    currentForm === 'services' || currentForm === 'add-remove-service'
                      ? 'button-current-form'
                      : null
                  }
                  onClick={handleMyServicesClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Services
                </button>
                {/* <button
                  className={currentForm === 'sponsor' ? 'button-current-form' : null}
                  onClick={handleSponsorClick}
                >
                  <i className="fas fa-ticket-alt tab-icons"></i>
                  My Sponsorships
                </button> */}

                <button
                  className={currentForm === 'experiences' ? 'button-current-form' : null}
                  onClick={handleExperienceClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Experiences
                </button>

                <button
                  className={currentForm === 'redemptions' ? 'button-current-form' : null}
                  onClick={handleRedemptionsClick}
                >
                  {/* <i className="fas fa-ticket-alt tab-icons"></i> */}
                  My Redemptions
                </button>
              </div>
              <div className="col-md-10">
                {currentForm && currentForm === 'add-remove-service' && (
                  <SponsorPackageKindCard type={'services'}/>
                )}
                {currentForm && currentForm === 'add-remove-product' && (
                  <SponsorPackageKindCard type={'products'}/>
                )}

                {/* {currentForm && currentForm === 'edit-food-sample' && (
                  <CreateFoodSampleForm sampleObject={sampleObj} />
                )} */}

                {currentForm && currentForm === 'add-food-sample' && <CreateFoodSampleForm/>}
                {currentForm && currentForm === 'add-product-service-experience' && (
                  <CreateSampleSaleDonation addToFestivalsDropdown={true} selectedVendingFestival={selectedVendingFestival}/>
                )}

                {currentForm && currentForm === 'home' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="home-tab-title">Current Orders</div>
                    </div>
                    <table className="table">
                      <thead>
                      <tr>
                        <th scope="col">Date & Time</th>
                        <th scope="col">Product ID</th>
                        <th scope="col">Product name</th>
                        <th scope="col">Quantities</th>
                        <th scope="col">Unit Price</th>
                        <th scope="col">Price Sold</th>
                        <th scope="col">Festival name</th>
                      </tr>
                      </thead>
                      <tbody>{renderCurrentOrdersTable(currentOrders)}</tbody>
                    </table>
                  </div>
                )}
                {
                  currentForm && currentForm === 'services' && (
                    <ServiceDetails setCurrentForm={setCurrentForm}/>
                  )
                  /*             <div className="home-tab-container">
              <div className="row">
                <div className="home-tab-title">My Services</div>
              </div>
              <form onSubmit={formMethods.handleSubmit(deleteServices)}>
                <div className="row">
                  <button type="submit">Delete Button</button>
                </div>
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Service ID</th>
                      <th scope="col">Service name</th>
                      <th scope="col">Service price</th>
                      <th scope="col">Service types</th>
                      <th scope="col">Made in</th>
                      <th scope="col">Size</th>
                      <th scope="col">Quantity</th>
                    </tr>
                  </thead>
                  <tbody>
                    {renderUserServicesTable(services)}
                  </tbody>
                </table>
              </form>
            </div> */
                }
                {currentForm && currentForm === 'promotions' && <PromotionDetails/>}
                {currentForm && currentForm === 'food-samples' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="col">
                        <div className="home-tab-title">My Products</div>
                      </div>
                      <div className="col add-remove-action-buttons">
               
                        {userRole && userRole.includes('HOST') || userRole.includes('VENDOR') || userRole.includes('ADMIN') ? (
                          <button
                            className="add-action-button"
                            onClick={handleFoodSampleFestival}
                            disabled={submitAuthDisabled}
                          >
                            Add to Festival
                          </button>) : null}
                        <button
                          className="add-action-button"
                          onClick={handlePromotionOpened}
                          disabled={submitAuthDisabled}
                        >
                          Enable Promotion
                        </button>
                        <button
                          className="add-action-button"
                          onClick={handleRemovePromotion}
                          disabled={submitAuthDisabled}
                        >
                          Disable Promotion
                        </button>
                        <button
                          className="remove-action-button"
                          onClick={formMethods.handleSubmit(deleteFoodSamples)}
                          disabled={submitAuthDisabled}
                        >
                          Remove
                        </button>
                      </div>
                    </div>
                    <form onSubmit={formMethods.handleSubmit(deleteFoodSamples)}>
                      <table className="table">
                        <thead>
                        <tr>
                          <th></th>
                          <th></th>
                          <th scope="col">Product ID</th>
                          <th scope="col">Product Name</th>
                          <th scope="col">Product Price</th>
                          {/* <th scope="col">Promotional Price</th> */}
                          <th scope="col">Product Status</th>
                          {/* <th scope="col">Size</th> */}
                          <th scope="col">Promotional Price</th>
                          <th scope="col">Quantity</th>
                          {/* <th scope="col">Description</th> */}
                          <th scope="col">Cuisine</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foodSamples.length !== 0 ? (
                          renderUserFoodSampleTable(foodSamples)
                        ) : (
                          <tr>
                            <Empty/>
                          </tr>
                        )}
                        {/* {renderUserFoodSampleTable(foodSamples)} */}
                        </tbody>
                      </table>
                    </form>
                  </div>
                )}
                {
                  currentForm && currentForm === 'products' && (
                    <ProductDetails setCurrentForm={setCurrentForm}/>
                  )
                  /* <div className="home-tab-container">
              <div className="row">
                <div className="home-tab-title">My Products</div>
              </div>
              <form onSubmit={formMethods.handleSubmit(deleteProducts)}>
                <div className="row">
                  <button type="submit">Delete Button</button>
                </div>
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Product ID</th>
                      <th scope="col">Product name</th>
                      <th scope="col">Product price</th>
                      <th scope="col">Product types</th>
                      <th scope="col">Made in</th>
                      <th scope="col">Size</th>
                      <th scope="col">Quantity</th>
                    </tr>
                  </thead>
                  <tbody>
                    {renderUserProductsTable(products)}
                  </tbody>
                </table>
              </form>
            </div> */
                }
                {currentForm && currentForm === 'sponsor' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="col">
                        <div className="home-tab-title">Sponsorships</div>
                      </div>
                      <div className="col add-remove-action-buttons">
                        <button
                          className={
                            sponsorshipForm === 'sponsorInCash'
                              ? 'sponsor-active-button'
                              : 'sponsor-button'
                          }
                          onClick={sponsorInCash}
                        >
                          Sponsor In Cash
                        </button>
                        <button
                          className={
                            sponsorshipForm === 'sponsorInKind'
                              ? 'sponsor-active-button'
                              : 'sponsor-button'
                          }
                          onClick={sponsorInKind}
                        >
                          Sponsor In Kind
                        </button>
                      </div>
                    </div>
                    {sponsorshipForm === 'sponsorInCash' && (
                      <table className="table">
                        <thead>
                        <tr>
                          <th scope="col">Festival ID</th>
                          <th scope="col">Sponsor / In Cash</th>
                          <th scope="col">Festival sponsored</th>
                          <th scope="col">Festival start date & time</th>
                          <th scope="col">Festival end date & time</th>
                          <th scope="col">Festival ticket</th>
                          <th scope="col">Sponsor Status</th>
                        </tr>
                        </thead>
                        <tbody>{renderSponsorshipTable(sponsorships)}</tbody>
                      </table>
                    )}
                    {sponsorshipForm === 'sponsorInKind' && (
                      <table className="table">
                        <thead>
                        <tr>
                          <th scope="col">Festival ID</th>
                          <th scope="col">Product Sponsored</th>
                          <th scope="col">Festival sponsored</th>
                          <th scope="col">Festival start date & time</th>
                          <th scope="col">Festival end date & time</th>
                          <th scope="col">Festival ticket</th>
                          <th scope="col">Sponsor Status</th>
                        </tr>
                        </thead>
                        <tbody>{renderInKindSponsorshipTable(inKindSponsorships)}</tbody>
                      </table>
                    )}
                  </div>
                )}
                {currentForm && currentForm === 'redemptions' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="col">
                        <div className="home-tab-title">My Redemptions</div>
                      </div>
                      <div className="col">
                        <div className="redemptions-container">
                          <label className="search-label" htmlFor="search-input">
                            <input
                              type="text"
                              value={keyword}
                              id="search-input"
                              onChange={handleInputChange}
                              onKeyDown={_handleKeyDown}
                              placeholder="Food Claim ID..."
                            />
                            <i className="fa fa-search search-icon-redemptions"/>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div className="tab-switcher">
                      <button
                        onClick={() => toggleRedemptionTab(1)}
                        className={toggleRedemptionState === 1 ? 'tab active-tab' : 'tab'}
                      >
                        Prodcuts
                      </button>
                      <button
                        onClick={() => toggleRedemptionTab(2)}
                        className={toggleRedemptionState === 2 ? 'tab active-tab' : 'tab'}
                      >
                        Experiences
                      </button>

                      <button
                        onClick={() => toggleRedemptionTab(3)}
                        className={toggleRedemptionState === 3 ? 'tab active-tab' : 'tab'}
                      >
                        Services
                      </button>
                    </div>
                    <table className="table">
                      <thead>
                      <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Claimant Full name</th>
                        <th scope="col">Festival Name</th>
                        <th scope="col">Name of Item</th>
                        <th scope="col">Date of Claim</th>
                        <th scope="col">Quantity of claims</th>
                        <th scope="col">Claim Status</th>
                        {/* <th scope="col">Description</th> */}
                      </tr>
                      </thead>
                      {/*
                        {toggleState && toggleState === 1 && redeemReservationList.length !== 0 ? (
                          renderRedeemsRows(redeemReservationList, "Product")
                        ) : toggleState && toggleState === 2 &&  experienceRedeemReservationList && experienceRedeemReservationList.length !== 0 ? (
                          renderRedeemsRows(experienceRedeemReservationList, "Experience")
                        ) : toggleState && toggleState === 3 &&  serviceRedeemReservationList && serviceRedeemReservationList.length !== 0 ? (
                          renderRedeemsRows(serviceRedeemReservationList, "Service")
                        ) : (
                          <tr>
                            <td>No Items redeemed yet!</td>
                          </tr>
                        )} */}
                      {toggleRedemptionState && toggleRedemptionState === 1 ? (
                        <tbody>
                        {redeemReservationList && redeemReservationList.length !== 0 ? (
                            renderRedeemsRows(redeemReservationList, "Product")


                          ) :
                          (<tr>
                            <td>No Items redeemed yet!</td>
                          </tr>)}
                        </tbody>
                      ) : toggleRedemptionState && toggleRedemptionState === 2 ? (
                        <tbody>
                        {experienceRedeemReservationList && experienceRedeemReservationList.length !== 0 ? (
                            renderRedeemsRows(experienceRedeemReservationList, "Experience")


                          ) :
                          (<tr>
                            <td>No Items redeemed yet!</td>
                          </tr>)}
                        </tbody>
                      ) : toggleRedemptionState && toggleRedemptionState === 3 ? (
                        <tbody>
                        {serviceRedeemReservationList && serviceRedeemReservationList.length !== 0 ? (
                            renderRedeemsRows(serviceRedeemReservationList, "Service")


                          ) :
                          (<tr>
                            <td>No Items redeemed yet!</td>
                          </tr>)}
                        </tbody>
                      ) : null}


                    </table>
                  </div>
                )}

              {currentForm && currentForm === 'vending-festivals' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="col">
                        <div className="home-tab-title">My Vending Festivals</div>
                      </div>
                      <div className="col add-remove-action-buttons">
              
                        {addToFestivalSelected && (
                          <Form data={experienceData} onSubmit={addExperienceToFestival}>
                            <div className="col">
                              <CheckboxGroup
                                name="festival_selected"
                                options={festivalList.map((fest) => [
                                  fest.festival_name,
                                  fest.festival_id,
                                ])}
                              />
                            </div>
                            <div className="col">
                              <button type="submit" className="remove-action-button">
                                Save
                              </button>
                            </div>
                          </Form>
                        )}
                      </div>
                    </div>
                    <table className="table">
                      <thead>
                      <tr>
                        <th scope="col">Confirmation Id</th>
                        <th scope="col">Festival Name</th>
                        <th scope="col">Festival Type</th>
                        <th scope="col">Festival Start Date & Time</th>
                        <th scope="col">Festival End Date & Time</th>
                        <th scope="col">Festival Price</th>
                        <th scope="col">Festival Status</th>
                        <th scope="col">Add Product/Service/Experience</th>
                      </tr>
                      </thead>
                      <tbody>
                      {tickets && tickets.length > 0 ? (
                        renderVendingFestivals(tickets)
                      ) : (
                        <tr>
                          <td>No Festivals Vending yet!</td>
                        </tr>
                      )}
                      </tbody>
                    </table>
                  </div>
                )}

                {currentForm && currentForm === 'experiences' && (
                  <div className="home-tab-container">
                    <div className="row">
                      <div className="col">
                        <div className="home-tab-title">My Experiences</div>
                      </div>
                      <div className="col add-remove-action-buttons">
                        {/* <button
                          className="add-action-button"
                          onClick={() => setCurrentForm('add-food-sample')}
                          disabled={submitAuthDisabled}
                        >
                          <i class="fas fa-plus add-credits"></i>
                        </button> */}
                        {userRole && userRole.includes('HOST') || userRole.includes('VENDOR') ? (<button
                          className="add-action-button"
                          onClick={() => {
                            setAddToFestivalSelected(true);
                          }}
                          disabled={submitAuthDisabled}
                        >
                          Add to Festival
                        </button>) : null}
                        <button
                          className="remove-action-button"
                          onClick={formMethods.handleSubmit(deleteExperiences)}
                          disabled={submitAuthDisabled}
                        >
                          Remove
                        </button>
                        {addToFestivalSelected && (
                          <Form data={experienceData} onSubmit={addExperienceToFestival}>
                            <div className="col">
                              <CheckboxGroup
                                name="festival_selected"
                                options={festivalList.map((fest) => [
                                  fest.festival_name,
                                  fest.festival_id,
                                ])}
                              />
                            </div>
                            <div className="col">
                              <button type="submit" className="remove-action-button">
                                Save
                              </button>
                            </div>
                          </Form>
                        )}
                      </div>
                    </div>
                    <table className="table">
                      <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th scope="col">Experience Id</th>
                        <th scope="col">Experience Name</th>
                        <th scope="col">Capacity of experience</th>
                        <th scope="col">Size of experience</th>
                        {/* <th scope="col">Experience Nationality</th> */}
                        <th scope="col">Experience Details</th>
                        <th scope="col">Experience Price</th>
                        {/* <th scope="col">Description</th> */}
                      </tr>
                      </thead>
                      <tbody>
                      {fetchedExperiences && fetchedExperiences.length > 0 ? (
                        renderFetchedExperiences(fetchedExperiences)
                      ) : (
                        <tr>
                          <td>No Experiences available yet!</td>
                        </tr>
                      )}
                      </tbody>
                    </table>
                  </div>
                )}
                {currentForm && currentForm === 'edit-experience' && (
                  <EditExperience experience={editExperience}/>
                )}
              </div>
            </div>
          ) : toggleState === 3 && userRole.includes('ADMIN') ? (
            <Fragment>
              <Admin/>
            </Fragment>
          ) : toggleState === 4 && userRole.includes('HOST') ? (
            <Fragment>
              <HostPanel/>
            </Fragment>
          ) : null}
        </div>
      )}
    </div>
  );
};

export default DashboardHost;
