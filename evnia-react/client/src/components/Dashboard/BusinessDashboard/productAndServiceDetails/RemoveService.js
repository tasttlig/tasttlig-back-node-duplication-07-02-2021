// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form } from '../../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';

// Styling
import './RemoveService.scss';
import 'react-datepicker/dist/react-datepicker.css';

const RemoveService = (props) => {
  // Set initial state
  const [services, setServices] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userId = appContext.state.user.id;
  //const { latitude, longitude, geoError } = usePosition();

  const loadServices = async () => {
    const url = `/Services/details/${userId}`;

    const response = await axios({ method: 'GET', url });
    if (response.data.success) {
      setServices(response.data.details);
    }
  };

  const removeService = async (serviceId) => {
    const url = `/service/delete/${serviceId}`;

    try {
      const response = await axios({ method: 'DELETE', url });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          loadServices();
        }, 2000);

        toast('Service removed successfully!', {
          type: 'success',
          autoClose: 2000,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
    loadServices();
  }, []);

  // Render empty festival page
  const Empty = () => <strong className="">There is no content at the moment</strong>;

  return (
    <div>
      {services.length > 0 ? (
        services.map((item) => (
          <div className="row border p-2 my-1">
            <div className="col-3 px-2">
              <img
                src={item.image_urls && item.image_urls.length > 0 ? item.image_urls[0] : ''}
                className="logo-img"
              />
            </div>
            <div className="col-6 px-2">
              <span>
                <strong>{item.service_name}</strong>
              </span>
              <br />
              <span>{item.service_price}</span>
            </div>
            <div
              className="col-1 px-2 bg-danger text-white text-center btn rounded-pill"
              onClick={() => {
                removeService(item.service_id, item.service_image_id);
              }}
            >
              <strong>Remove</strong>
            </div>
          </div>
        ))
      ) : (
        <Empty />
      )}
    </div>
  );
};

export default RemoveService;
