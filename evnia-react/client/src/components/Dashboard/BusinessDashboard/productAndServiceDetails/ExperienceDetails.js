// Libraries
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';
// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import { formatDate } from '../../../Functions/Functions';
import EditExperience from './EditExperience';

// Styling
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../../assets/images/live.png';

toast.configure();

const ExperienceDetails = (props) => {
  console.log('props from Experience details:', props);
  // Calculate the number of days between start/end and current date

  // Set initial state
  const [confirmed, setConfirmed] = useState(false);
  const [error, setError] = useState(null);
  const [selectedExperiences, setSelectedExperiences] = useState([]);
  const history = useHistory();

  const isSelected = !!selectedExperiences.find(
    (selectedExperience) => props.experienceId === selectedExperience.experienceId,
  );

  // Edit current food sample helper function
  const handleEditExperience = (sample) => {
    // setCurrentForm('edit-food-sample');

    history.push({
      // pathname: '/edit-food-sample',
      state: {
        // foodSampleId: sample.food_sample_id,
        // images: sample.image_urls,
        // title: sample.title,
        // nationality_id: sample.nationality_id,
        // start_date: sample.start_date,
        // end_date: sample.end_date,
        // start_time: formattedTimeToDateTime(sample.start_time),
        // end_time: formattedTimeToDateTime(sample.end_time),
        // sample_size: sample.sample_size,
        // quantity: sample.quantity,
        // dietaryRestrictions,
        // daysAvailable,
        // spice_level: sample.spice_level,
        // addressLine1,
        // addressLine2,
        // city: sample.city,
        // provinceTerritory: sample.state,
        // postal_code: sample.postal_code,
        // description: sample.description,
      },
    });

    // return <CreateFoodSample editObj={prefilledValues} />;
  };
  console.log('selectedExperiences', selectedExperiences);

  return (
    <tr>
      <td scope="row">
        <input
          type="checkbox"
          name="foodSample_checkbox"
          value={isSelected}
          onChange={() => {
            if (isSelected) {
              setSelectedExperiences((selectedExperiences) => {
                const newExperience = selectedExperiences.filter(
                  (selectedExperience) => selectedExperience.experienceId !== props.experienceId,
                );
                return newExperience;
              });
            } else {
              setSelectedExperiences((selectedExperiences) => selectedExperiences.concat(props));
            }
          }}
        ></input>
      </td>
      <td
        class="fas fa-edit"
        value={isSelected}
        onClick={(e) => {
          e.preventDefault();
          //handleEditExperience(props);
          return <EditExperience experience={props} />;
        }}
      ></td>
      <td scope="row">{props.experienceId}</td>
      <td>{props.experienceName}</td>
      <td>{props.experienceCapacity}</td>
      <td>{props.experienceSize}</td>
      <td>{props.experienceNationality}</td>
      <td>{props.experienceDetails}</td>
      <td>{props.experiencePrice}</td>
    </tr>
  );
};

export default ExperienceDetails;
