// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form, CheckboxGroup } from '../../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import EditService from './EditService';
import {
  formatDate,
  formatMilitaryToStandardTime,
  formattedTimeToDateTime,
} from '../../../Functions/Functions';

// Styling
import './ServiceDetails.scss';
import 'react-datepicker/dist/react-datepicker.css';

const ServiceDetails = (props) => {
  // Set initial state
  const [services, setServices] = useState([]);
  const [focus, setFocus] = useState('view');
  const [serviceToEdit, setServiceToEdit] = useState({});
  const [selectedService, setSelectedService] = useState({});
  const [addToFestivalSelected, setAddToFestivalSelected] = useState(false);
  const history = useHistory();
  const [festivalList, setFestivalList] = useState([]);
  let data = { service_festival_id: [] };
  let count = 0;
  let ids = [];

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userId = appContext.state.user.id;
  const userRole = appContext.state.user.role;
  //const { latitude, longitude, geoError } = usePosition();

  // Render festival cards helper function
  const renderServices = (arr) => {
    return arr.map((item) => (
      <tr
      /* onClick={() => {
          setSelectedService(item);
        }} */
      >
        <td scope="row">
          <input
            onClick={() => {
              addServiceId(item.service_id);
            }}
            type="checkbox"
            name="service_id"
            value={item.service_id}
          ></input>
        </td>
        <th
          className="fa fa-edit"
          onClick={() => {
            setFocus('edit');
            setServiceToEdit(item);
          }}
          scope="row"
        ></th>
        <td>{item.service_id}</td>
        <td>{item.service_name}</td>
        <td>{item.service_price}</td>
        {/* <td>{item.service_type}</td> */}
        <td>{item.country}</td>
        <td>{item.service_size_scope}</td>
        <td>{item.service_capacity}</td>
      </tr>
    ));
  };

  const addServiceId = (id) => {
    const stringValue = '' + id;

    if (ids.length === 0) {
      ids[count] = stringValue;
      count++;
    } else if (ids.length > 0 && ids.includes(stringValue)) {
      ids = ids.filter((id) => id !== stringValue);
      count--;
    } else {
      ids[count] = stringValue;
      count++;
    }
    localStorage.setItem('serviceIds', JSON.stringify(ids));
  };
  const getSelectedServices = () => {
    return ids.map((i) => Number(i));
  };

  const removeItem = async () => {
    if (ids.length < 1) {
      toast('Select an item to remove!', {
        type: 'info',
        autoClose: 2000,
      });
      return;
    }
    const url = '/service/delete';
    data.service_id = ids;
    try {
      const response = await axios({ method: 'DELETE', url, data });
      if (response && response.data && response.data.success) {
        setTimeout(() => {
          setFocus('time-out');
          loadServices();
        }, 2000);
        setTimeout(() => {
          setFocus('view');
        }, 4000);
        toast('Service removed successfully!', {
          type: 'success',
          autoClose: 2000,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const addServiceToFestival = async (data) => {
    if (data.service_festival_id && data.service_festival_id.length < 1) {
      toast('Please select a festival!', {
        type: 'info',
        autoClose: 2000,
      });
      return;
    }
    data.service_id = JSON.parse(localStorage.getItem('serviceIds'));
    ids = JSON.parse(localStorage.getItem('serviceIds'));
    let url = '/service/update';
    try {
      const response = await axios({
        method: 'PUT',
        url,
        data,
      });
      if (response && response.data && response.data.success && userRole) {
        setAddToFestivalSelected(false);
        ids = JSON.parse(localStorage.getItem('serviceIds'));
        // setTimeout(() => {
        //   setFocus('time-out');
        //   loadServices();
        // }, 2000);
        setTimeout(() => {
          window.location.href= '/dashboard';
        }, 2000);
        toast('Service added to selected festival', {
          type: 'success',
          autoClose: 2000,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const loadServices = async () => {
    const url = `/services/details/${userId}`;

    const response = await axios({ method: 'GET', url });
    if (response.data.success) {
      setServices(response.data.details);
    }
  };

  // Edit current food sample helper function
  const handleEditService = (editingService) => {
    history.push({
      pathname: '/edit-service',
      state: {
        service_id: editingService.service_id,
        service_images: editingService.image_urls,
        service_name: editingService.service_name,
        service_nationality_id: editingService.service_nationality_id,
        start_date: editingService.start_date,
        end_date: editingService.end_date,
        festival_selected: editingService.festival_selected,
        start_time: formattedTimeToDateTime(editingService.start_time),
        end_time: formattedTimeToDateTime(editingService.end_time),
        service_size_scope: editingService.service_size_scope,
        service_price: editingService.service_price,
        service_capacity: editingService.service_capacity,
        service_description: editingService.service_description,
        // actual_price: props.foodSampleProps.price,
        // promotional_price: props.foodSampleProps.discounted_price
      },
    });
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
  };

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
    loadServices();
    fetchFestivalList();
  }, []);

  // Render empty festival page
  const ServiceEmpty = () => <strong className="">You have not added services yet</strong>;

  return (
    <div>
      {focus === 'view' ? (
        <div className="home-tab-container">
          <div className="row">
            <div className="col">
              <div className="home-tab-title">My Services</div>
            </div>
            <div className="col add-remove-action-buttons">
              {userRole && userRole.includes('HOST') || userRole.includes('VENDOR') ? (
                <button
                onClick={() => {
                  setAddToFestivalSelected(true);
                }}
                className="add-action-button"
              >
                Add to Festival
              </button>): null}
              <button onClick={removeItem} className="remove-action-button">
                Remove
              </button>
            </div>
          </div>
          <div className="row">
            <div className="col add-to-festivals-checkboxes add-remove-action-buttons">
              {addToFestivalSelected && (
                <Form data={data} onSubmit={addServiceToFestival}>
                  <div className="col">
                    <CheckboxGroup
                      name="service_festival_id"
                      options={festivalList.map((fest) => [fest.festival_name, fest.festival_id])}
                    />
                  </div>
                  <div className="col">
                    <button type="submit" className="remove-action-button">
                      Save
                    </button>
                  </div>
                </Form>
              )}
            </div>
          </div>
          <form>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col"></th>
                  <th scope="col">Service ID</th>
                  <th scope="col">Service Name</th>
                  <th scope="col">Service Price</th>
                  <th scope="col">Made in</th>
                  <th scope="col">Size</th>
                  <th scope="col">Quantity</th>
                </tr>
              </thead>
              <tbody>{services.length !== 0 ? renderServices(services) : <ServiceEmpty />}</tbody>
            </table>
          </form>
        </div>
      ) : (
        <div className="edit-service-container">
          <div className="row edit-heading-container">
            <div className="edit-heading-title">Edit Service</div>
            <div
              className="edit-close-button"
              onClick={() => {
                setFocus('view');
              }}
            >
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="row">
            {/* <EditService service={serviceToEdit} /> */}
            {handleEditService(serviceToEdit)}
          </div>
        </div>
      )}
    </div>
  );
};

export default ServiceDetails;
