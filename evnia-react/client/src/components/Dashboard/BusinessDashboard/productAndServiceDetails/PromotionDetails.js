// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form, CheckboxGroup } from '../../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import Modal from 'react-modal';
import AddPromotion from './AddPromotion';
import EditPromotion from './EditPromotion';


// Styling
import './PromotionDetails.scss';
import 'react-datepicker/dist/react-datepicker.css';

const PromotionDetails = (props) => {
  // Set initial state
  const [promotions, setPromotions] = useState([]);
  const [focus, setFocus] = useState('view');
  const [promotionToEdit, setPromotionToEdit] = useState({});
  const [selectedPromotion, setSelectedPromotion] = useState(false);
  const [addToFestivalSelected, setAddToFestivalSelected] = useState(false);
  const [addNewPromotion, setAddNewPromotion] = useState(false);
  const [festivalList, setFestivalList] = useState([]);
  let data = { promotion_festivals_id: [] };
  let count = 0;
  let ids = [];

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const user_id = appContext.state.user.id;
  const userRole = appContext.state.user.role;
  

  
  const renderPromotions = (arr) => {
    return arr.map((item, index) => (
      <tr
      //  key={index}
      //  data-item={item}
      //  onClick={fetchRowDetails}
      // onClick={() => {
      //     setSelectedPromotion(item);
      //   }}
      >
        <th scope="row">
          <input
            onClick={() => {
              addPromotionId(item.promotion_id);
            }}
            type="checkbox"
            name="promotion_id"
            value={item.promotion_id}
          ></input>
        </th>
        <td
          className="fa fa-edit"
          onClick={() => {
            setFocus('edit');
            setPromotionToEdit(item);
          }}
        ></td>
        <td data-title="promotion_id">{item.promotion_id}</td>
        <td data-title="promotion_name">{item.promotion_name}</td>
        <td data-title="promotion_discount_percentage">{item.promotion_discount_percentage ? item.promotion_discount_percentage: "NA" }</td>
        <td data-title="promotion_discount_price">{item.promotion_discount_price ? item.promotion_discount_price: "NA" }</td>
        {/* <td data-title="country">{item.country}</td> */}
        <td data-title="promotion_start_date">{item.promotion_start_date_time ? 
        item.promotion_start_date_time.substr(0,10): "NA"}</td>
        <td data-title="product_end_date">{item.promotion_end_date_time ?
        item.promotion_end_date_time.substr(0,10): "NA"}</td>
      </tr>
    ));
  };

  const addPromotionId = (id) => {
    const stringValue = '' + id;

    if (ids.length === 0) {
      ids[count] = stringValue;
      count++;
    } else if (ids.length > 0 && ids.includes(stringValue)) {
      ids = ids.filter((id) => id !== stringValue);
      count--;
    } else {
      ids[count] = stringValue;
      count++;
    }
    localStorage.setItem('promotionIds', JSON.stringify(ids));
  };

  const getSelectedPromotions = () => {
    return ids.map((i) => Number(i));
  };

  const loadPromotions = async () => {
    const url = `/promotions/user/${user_id}`;

    const response = await axios({ method: 'GET', url });
    if (response.data.success) {
      setPromotions(response.data.promotions_all);
    }
  };

  const removeItem = async () => {
    if (ids.length < 1) {
      toast('Select an item to remove!', {
        type: 'info',
        autoClose: 2000,
      });
      return;
    }
    const url = `/promotions/delete/user/${user_id}`;
    data.delete_items = ids;
    try {
      const response = await axios({ method: 'DELETE', url, data });
      if (response && response.data && response.data.success) {
        setTimeout(() => {
          loadPromotions();
          setFocus('time-out');
        }, 2000);
        setTimeout(() => {
          setFocus('view');
        }, 4000);
        toast('Promotion removed successfully!', {
          type: 'success',
          autoClose: 2000,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
  };

  const addPromotionToFestival = async (data) => {};


  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
    loadPromotions();
    fetchFestivalList();
  }, []);

  // Render empty festival page
  const PromotionsEmpty = () => <strong className="">You have not added any promotions yet</strong>;

  
  return (
    <div>
      {focus === 'view' ? (
        <form className="bg-white">
          <div className="p-4">
            <span className="h2 border-bottom border-danger">My Promotional Offers</span>
          </div>
          <div>
            <h6>This feature isn't ready yet. Our team is working hard to bring this feature to you.
              Very soon, you will be able to apply promotional offers to your products. Please stay in touch.
            </h6>
          </div>
          <div>
            <>
              <span
                onClick={removeItem}
                className="btn btn-danger text-center rounded-pill py-2 px-3 mr-2"
              >
                Remove
              </span>
              
              <span
                onClick={() => setAddNewPromotion(true)}
                className="btn btn-danger text-center rounded-pill py-2 px-3"
              >
                Add new Promotional Offer
              </span>
              
              {addNewPromotion ? (
                <Modal className="passport-details-modal" 
                isOpen ={addNewPromotion} onRequestClose={() => setAddNewPromotion(false) }>
                  
                  <AddPromotion
                    user_id={props.user_id}
                    addNewPromotionState={addNewPromotion}
                    changeAddNewPromotion={(e) => setAddNewPromotion(e)}
                    />
              </Modal>
            ) : null}
               
            </>
          </div>
          <table className="table table-striped pt-5 mt-5">
            <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col">Promotion ID</th>
                <th scope="col">Promotio Name</th>
                <th scope="col">Product Discount Percentage</th>
                <th scope="col">Product Discount Price</th>
                <th scope="col">Promotion Start Date</th>
                <th scope="col">Promotion End Date</th>
            </tr>
            </thead>
            <tbody>{promotions && promotions.length !== 0 ? renderPromotions(promotions) : <PromotionsEmpty />}</tbody>
          </table>
        </form>
      ) : focus === 'time-out' ? null : (
        <div>
          <div className="d-flex align-items-end flex-column">
            <span
              className=" btn btn-dark fas fa-times-circle mr-5 mt-3 p-3 rounded-circle"
              onClick={() => {
                setFocus('view');
              }}
            >
              <i class="fas fa-times"></i>
            </span>
          </div>
          <div className="row">
            <EditPromotion promotion={promotionToEdit} /> 
          </div>
        </div>
      )}
    </div>
  );
};

export default PromotionDetails;
