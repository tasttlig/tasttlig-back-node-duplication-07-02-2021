// Libraries
import React, { useState, useEffect, useContext, Fragment } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import { useForm, Controller } from 'react-hook-form';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';
import Radio from '../../../Shared/Radio';

// Styling
import './EditService.scss';
import 'react-toastify/dist/ReactToastify.css';

// Components
import {
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
  Checkbox,
  CheckboxGroup,
} from '../../../EasyForm';
import {
  formatDate,
  formattedTimeToDateTime,
  formatMilitaryToStandardTime,
} from '../../../Functions/Functions';

toast.configure();

const EditPromotion = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  const { errors } = useForm({});
  // Check that user is signed in to access user specific pages
  //if (!appContext.state.signedInStatus) window.location.href = "/login";

  // Set initial state
  const [loading, setLoading] = useState(true);
  const [nationalities, setNationalities] = useState([]);
  const [festivalList, setFestivalList] = useState([]);
  const [discountRadioSelected, setDiscountRadioSelected] = useState('');
  const [formDetails, setFormDetails] = useState({
    input_one: 'promotion_name',
    input_one_placeholder: 'Promotional Offer Name',
    input_two: 'promotion_description',
    input_three: 'discount_percent_price',
    // input_three_placeholder: 'Price',
    input_four: 'discount_percentage',
    input_four_placeholder: 'Discount Percentage',
    input_five: 'discount_price',
    input_five_placeholder: 'Discount Price',
    input_six: 'start_date',
    input_six_placeholder: props.promotion.promotion_start_date_time,
    input_seven: 'end_date',
    input_seven_placeholder: props.promotion.promotion_end_date_time,
  });
  const [selected, setSelected] = useState('promotions');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  let data = {
    promotion_name: props.promotion.promotion_name,
    promotion_description: props.promotion.promotion_description,
    // discount_percentage_price: props.promotion.,
    discount_percentage: props.promotion.promotion_discount_percentage,
    discount_price: props.promotion.promotion_discount_price,
    promotion_start_date: props.promotion.promotion_start_date_time,
    promotion_end_date: props.promotion.promotion_end_date_time,
  };

  // Get all fetches helper function
  const getAllFetches = async () => {
    setDiscountRadioSelected(
      data && data.discount_price ? 'discount_price' : 'discount_percentage',
    );
    setLoading(false);
  };

  // Select promotion form helper function
  const promotionSelected = () => {
    setFormDetails({
      input_one: 'promotion_name',
      input_one_placeholder: 'Promotional Offer Name',
      input_two: 'promotion_description',
      input_two_placeholder: 'Promotional Offer Description',
      input_three: 'discount_percent_price',
      // input_three_placeholder: 'Price',
      input_four: 'discount_percentage',
      input_four_placeholder: 'Discount Percentage',
      input_five: 'discount_price',
      input_five_placeholder: 'Discount Price',
      input_six: 'start_date',
      input_six_placeholder: 'Start Date',
      input_seven: 'end_date',
      input_seven_placeholder: 'End Date',
    });
  };

  // Submit product, service helper functions
  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      // window.location.href = `/festival/${props.location.festivalId}`;
      window.location.href = '/dashboard';
    }, 2000);

    toast(`Update was successful!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSubmitAuthDisabled(true);
  };

  const updatePromotion = async (dataPut) => {
    window.scrollTo(0, 0);

    let url = '';
    // if (selected === 'promotions') {
    //   if (userRole) data.product_creator_type = userRole[0];
    url = `/promotion/update/${props.promotion.promotion_id}`;
    // }
    console.log('sending data', dataPut);
    console.log('data var', data)
    if(!dataPut['start_date']) {
      dataPut['start_date'] = data.promotion_start_date
    } else {
      dataPut['start_date'] = dataPut['start_date'].toString()
    }
    if(!dataPut['end_date']) {
      dataPut['end_date'] = data.promotion_end_date
    } else {
      dataPut['end_date'] = dataPut['end_date'].toString()
    }
    if(!dataPut['discount_price']) {
      dataPut['discount_price'] = null
    }
    if(!dataPut['discount_percentage']) {
      dataPut['discount_percentage'] = null
    }
    dataPut['promotion_id'] = props.promotion.promotion_id
    try {
      const response = await axios({
        method: 'PUT',
        url,
        data: dataPut,
      });

      if (response && response.data && response.data.success && userRole) {
        defaultOnCreatedAction();
      } else {
        if (response && response.data && response.data.success === false) {
          toast('Error! Something went wrong!', {
            type: 'error',
            autoClose: 2000,
          });
        }
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    const { ...rest } = values;

    console.log('values',values)
    let data = {
      ...rest,
    };

    if (selected === 'promotions') {
      await updatePromotion(data);
    }
  };

  // Mount Dashboard page
  useEffect(() => {
    window.scrollTo(0, 0);
    getAllFetches();
  }, []);

  console.log(props);
  console.log('data', data);
  return (
    <Fragment>
      <Form data={data} onSubmit={onSubmit} className="edit-service-form">
        <div className="row text-cen">
          <div className="col-md-6 dashboard-forms-input-content-col-1">
            <Input
              name={formDetails.input_one}
              placeholder={formDetails.input_one_placeholder}
              disabled={submitAuthDisabled}
              className="form-control"
              required
              label={null}
            />
          </div>
          <div className="col-md-6 dashboard-forms-input-content-col-1">
            <Textarea
              name={formDetails.input_two}
              placeholder={formDetails.input_two_placeholder}
              disabled={submitAuthDisabled}
              className="form-control"
              required
              label={null}
            />
          </div>
        </div>
        <h6>
          Do you want to add a discount percentage on the price or flat discount on the price?
        </h6>
        <div className="col radio-product-type">
          <Radio
            value="discount_percentage"
            selected={discountRadioSelected}
            text="Discount Percentage"
            onChange={setDiscountRadioSelected}
          />
          <Radio
            value="discount_price"
            selected={discountRadioSelected}
            text="Discount Price"
            onChange={setDiscountRadioSelected}
          />
        </div>
        <div className="row">
          <div className="col-md-6 dashboard-forms-input-content-col-1">
            <Input
              name={formDetails.input_four}
              placeholder={formDetails.input_four_placeholder}
              disabled={discountRadioSelected !== 'discount_percentage'}
              className={
                errors.percentageSelected && discountRadioSelected === 'discount_percentage'
                  ? 'form-control invalid-input'
                  : 'form-control'
              }
              className="edit-form-control"
              required={discountRadioSelected === 'discount_percentage'}
              label={null}
            />
          </div>
          <div className="col-md-6 dashboard-forms-input-content-col-2">
            <Input
              name={formDetails.input_five}
              placeholder={formDetails.input_five_placeholder}
              disabled={discountRadioSelected !== 'discount_price'}
              className={
                errors.priceSelected && discountRadioSelected === 'discount_price'
                  ? 'form-control invalid-input'
                  : 'form-control'
              }
              className="edit-form-control"
              required={discountRadioSelected === 'discount_price'}
              label={null}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6 dashboard-forms-input-content-col-1">
            <DateInput
              name={formDetails.input_six}
              placeholder={formDetails.input_six_placeholder}
              dateFormat="yyyy/MM/dd, h:mm:ss a"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              showTimeSelect
              dropdownMode="select"
              disabled={submitAuthDisabled}
              className="form-control"
              default
              // selected={formDetails.input_six && new Date(formDetails.input_six)}
              // handleChange={handleStartDateChange}
              // onChange={handleStartDateChange}
            />
          </div>
          <div className="col-md-6 dashboard-forms-input-content-col-3">
            <DateInput
              name={formDetails.input_seven}
              placeholder={formDetails.input_seven_placeholder}
              dateFormat="yyyy/MM/dd, h:mm:ss a"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              showTimeSelect
              dropdownMode="select"
              disabled={submitAuthDisabled}
              className="form-control"
              // required
              // selected={data.promotion_end_date && new Date(data.promotion_end_date)}
            />
          </div>
        </div>

        <div className="first-row">
          <button type="submit" className="dashboard-forms-add-btn">
            Update
          </button>
        </div>
      </Form>
    </Fragment>
  );
};

export default EditPromotion;
