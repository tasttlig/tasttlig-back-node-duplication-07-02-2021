// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form } from '../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import EditProduct from './productAndServiceDetails/EditProduct';

// Styling
import './RedemptionDetails.scss';
import 'react-datepicker/dist/react-datepicker.css';

const RedemptionDetails = (props) => {
  // Set initial state
  const [orders, setRedemptions] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userId = appContext.state.user.id;
  //const { latitude, longitude, geoError } = usePosition();

  // Render festival cards helper function
  const renderRedemptions = (arr) => {
    return arr.map((item) => (
      <tr>
        <th scope="row">
          {' '}
          <input
            type="checkbox"
            name={item.food_sample_claim_id}
            value={item.food_sample_claim_id}
          ></input>
        </th>
        <td>{item.reserved_on}</td>
        <td>{item.food_sample_id}</td>
        <td>{item.status}</td>
      </tr>
    ));
  };

  const loadRedemptions = async () => {
    const url = '/food-sample-claim/user/reservations';

    const response = await axios({ method: 'GET', url });
    console.log('redempt', response);
    if (response.data.success) {
      setRedemptions(response.data.details);
    }
  };

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
    loadRedemptions();
  }, []);

  // Render empty festival page
  const RedemptionEmpty = () => <strong className="">You have no redemptions</strong>;

  return (
    <form className="bg-white">
      <div className="p-4">
        <span className="h2 border-bottom border-danger">My Redemptions</span>
      </div>
      <table className="table table-striped pt-5 mt-5">
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Date</th>
            <th scope="col">Food sample ID</th>
            <th scope="col">Redemption status</th>
          </tr>
        </thead>
        <tbody>{orders.length !== 0 ? renderRedemptions(orders) : <RedemptionEmpty />}</tbody>
      </table>
    </form>
  );
};

export default RedemptionDetails;
