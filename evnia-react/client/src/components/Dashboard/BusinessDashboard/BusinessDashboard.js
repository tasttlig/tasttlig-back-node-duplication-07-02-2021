// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

// Styling
import './BusinessDashboard.scss';
import tasttligLogoBlack from '../../../assets/images/tasttlig-logo-black.png';

import Sponsorships from './Sponsorships/Sponsorships';
import ProductDetails from './productAndServiceDetails/ProductDetails';
import ServiceDetails from './productAndServiceDetails/ServiceDetails';
import ExperienceDetails from './productAndServiceDetails/ExperienceDetails';
import PromotionDetails from './productAndServiceDetails/PromotionDetails';
import RemoveProductsAndServices from './productAndServiceDetails/RemoveProductsAndServices';
import OrderDetails from './OrderDetails';
import RedemptionDetails from './RedemptionDetails';
import GuestOrders from './GuestOrders';

const BusinessDashboard = (props) => {
  const history = useHistory();

  const [businessDetails, setBusinessDetails] = useState();

  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  const [selectedView, setSelectedView] = useState('home');

  const gotoPasssport = () => {
    window.location.href = `/passport/${appContext.state.user.id}`;
  };

  const handleHostApply = () => {
    window.location.href = '/become-a-host';
  };

  const handleVendApply = () => {
    window.location.href = '/complete-profile/festival-vendor';
  };

  const handleSponsorApply = async () => {
    if (
      userRole &&
      !userRole.includes('BUSINESS') &&
      !userRole.includes('VENDOR') &&
      !userRole.includes('HOST')
    ) {
      window.location.href = '/get-business-passport';
    }
    const url = '/become-in-kind-sponsor';
    const data = { is_sponsor: true };
    const response = await axios({ method: 'POST', url, data });
    if (response && response.data && response.data.success) {
      await history.push({
        pathname: '/sponsor-in-kind-confirmation',
      });
    } else {
      toast(`Something went wrong`, {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const gotoTicket = () => {
    window.location.href = '/ticket';
  };

  const gotoSponsorPlan = () => {
    window.location.href = '/sponsor-plan';
  };

  const gotoTasttlig = () => {
    window.location.href = '/';
  };

  useEffect(() => {
    const getBusinessDetails = async () => {
      const userId = appContext.state.user.id;
      const url = '/get-business-details-all';

      const response = await axios({ method: 'GET', url, userId });
      if (response.data.success) {
        setBusinessDetails(response.data.business_details_all);
      }
    };
    getBusinessDetails();
  }, []);

  return (
    <div className="navbar-content">
      <div className="navbar-layer-a">
        <div className="logo overlap-inline">
          <img
            src={
              businessDetails && businessDetails.business_details_logo
                ? businessDetails.business_details_logo
                : ''
            }
            className="logo-img rounded-circle"
          />
        </div>

        <div onClick={gotoPasssport} className="overlap-inline">
          <span>
            <b>
              {businessDetails && businessDetails.business_name
                ? businessDetails.business_name
                : ''}
            </b>
          </span>
          <br />
          <span className="text-dark btn-link">View my passport</span>
        </div>

        <div className="roles overlap-inline rounded-pill">
          {/* <span className="fas fa-gifts mr-1"></span> */}
          <span className="role role-text mr-2 py-1 px-1 rounded-pill">
            <b>Role(s): </b>
          </span>
          {userRole.map((role) => (
            <span className="role role-text mr-2 py-1 px-2 rounded-pill">
              <b>{role}</b>
            </span>
          ))}
          {/* {userRole.includes("SPONSOR") && <span className="role role-text mr-2 py-1 px-1 rounded-pill"><b>Sponsor</b></span>}
{userRole.includes("VENDOR") && <span className="role role-text mr-2 py-1 px-1 rounded-pill"><b>Vendor</b></span>}
{userRole.includes("HOST") && <span className="role role-text mr-2 py-1 px-1 rounded-pill"><b>Host</b></span>}
{userRole.includes("BUSINESS") && <span className="role role-text mr-2 py-1 px-1 rounded-pill"><b>Business</b></span>} */}
        </div>

        {userRole && !userRole.includes('SPONSOR') && (
          <div
            onClick={handleSponsorApply}
            className="host-apply-btn overlap-inline rounded-pill btn btn-light"
          >
            <span>
              <b>Apply to Sponsor</b>
            </span>
          </div>
        )}
        {userRole && !userRole.includes('HOST') && (
          <div
            onClick={handleHostApply}
            className="host-apply-btn overlap-inline rounded-pill btn btn-light"
          >
            <span>
              <b>Apply to Host</b>
            </span>
          </div>
        )}

        {userRole && !userRole.includes('VENDOR') && (
          <div
            onClick={handleVendApply}
            className="vend-apply-btn overlap-inline rounded-pill btn btn-light"
          >
            <span>
              <b>Apply to Vend</b>
            </span>
          </div>
        )}

        <div className="credit-balance-label overlap-inline">
          <span>
            <b>Credit Balance</b>
          </span>
        </div>

        <div className="credit-balance overlap-inline rounded-pill">
          <span>
            <b>$100.00</b>
          </span>
        </div>

        <div className="top-up overlap-inline rounded-pill btn btn-dark">
          <span>
            <b>Top Up</b>
          </span>
        </div>
      </div>

      <div className="navbar-layer-b">
        <div className="overlap-inline"></div>

        <div onClick={gotoTicket} className="tickets overlap-inline rounded-pill btn btn-light">
          <span>
            <b>My Tickets</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('product-details');
          }}
          className="products overlap-inline rounded-pill btn btn-light"
        >
          <span>
            <b>My Products</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('service-details');
          }}
          className="services overlap-inline rounded-pill btn btn-light"
        >
          <span>
            <b>My Services</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('experience-details');
          }}
          className="services overlap-inline rounded-pill btn btn-light"
        >
          <span>
            <b>My experiences</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('promotion-details');
          }}
          className="promotions overlap-inline rounded-pill btn btn-light"
        >
          <span>
            <b>My Promotions</b>
          </span>
        </div>

        <div className="tasttlig-logo overlap-inline">
          <img src={tasttligLogoBlack} className="tasttlig-logo-img" />
        </div>

        <div className="bar overlap-inline"></div>

        <div className="title overlap-inline">
          <span>
            <b>Business</b>
          </span>
        </div>

        <div onClick={gotoTasttlig} className="goto-tasttlig overlap-inline btn btn-link">
          <span>
            <b>Goto Tasttlig</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('add-remove');
          }}
          className="add-btn overlap-inline rounded-circle fas fa-3x fa-plus-circle"
        ></div>
      </div>

      <div className="navbar-layer-c">
        <div
          onClick={() => {
            setSelectedView('orders');
          }}
          className="home-btn overlap-inline btn btn-light rounded-pill"
        >
          <span className="fas fa-home mr-1"></span>
          <span>
            <b>Home</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('sponsorships');
          }}
          className="sponsorship-btn overlap-inline btn btn-light rounded-pill"
        >
          <span className="fas fa-gifts mr-1"></span>
          <span>
            <b>My Sponsorhips</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('orders');
          }}
          className="order-btn overlap-inline btn btn-light rounded-pill"
        >
          <span className="fas fa-shopping-bag mr-1"></span>
          <span>
            <b>My Orders</b>
          </span>
        </div>

        <div
          onClick={() => {
            setSelectedView('redemptions');
          }}
          className="redemption-btn overlap-inline btn btn-light rounded-pill"
        >
          <span className="fas fa-concierge-bell mr-1"></span>
          <span>
            <b>My Redemptions</b>
          </span>
        </div>
      </div>

      <div>
        {selectedView === 'sponsorships' ? (
          <Sponsorships />
        ) : selectedView === 'home' ? (
          <GuestOrders />
        ) : selectedView === 'orders' ? (
          <OrderDetails />
        ) : selectedView === 'redemptions' ? (
          <RedemptionDetails />
        ) : selectedView === 'product-details' ? (
          <ProductDetails />
        ) : selectedView === 'service-details' ? (
          <ServiceDetails />
        ) : selectedView === 'experience-details' ? (
          <ExperienceDetails />
        ) : selectedView === 'promotion-details' ? (
          <PromotionDetails />
        ) : selectedView === 'add-remove' ? (
          <RemoveProductsAndServices />
        ) : (
          <div />
        )}
      </div>
    </div>
  );
};

export default BusinessDashboard;
