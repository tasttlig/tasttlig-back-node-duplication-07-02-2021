// Libraries
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import UserReview from '../../../Reviews/UserReviews';

// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import { formatDate } from '../../../Functions/Functions';

// Styling
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../../assets/images/live.png';

toast.configure();

const MemberTicketClaimed = (props) => {
  console.log('props from tickets:', props);
  // Calculate the number of days between start/end and current date
  const appContext = useContext(AppContext);

  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const [orderList, setOrderList] = useState([]);
  const [, setLoading] = useState(false);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
  const [detailsOpened, toggleDetailsOpened] = useState(false);
  const [userReviewOpened, setUserReviewOpened] = useState(false);
  const [modalInformation, setModalInformation] = useState([]);

  // Render claimed items Row
  return (
    <tr>
      <td scope="row">{props.foodSampleId}</td>

      <td>{props.claimantFullName}</td>
      <td>{props.festivalName}</td>
      <td>{props.foodName}</td>
      <td>{formatDate(props.dateTimeofClaim)}</td>
      <td>{props.numberofClaims}</td>
      <td>
        <button type="button" className="btn btn-primary view-paid">
          {props.claimStatus}
        </button>
      </td>
    </tr>
  );
};

export default MemberTicketClaimed;
