// Libraries
import React, { useState, useEffect, useContext, Fragment } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import { AppContext } from '../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';
import { DateInput, Form, Input, MultiImageInput, Select, Textarea } from '../EasyForm';
import { formatDate, formattedTimeToDateTime } from '../Functions/Functions';
import DashboardHost from './DashboardHost/DashboardHost';

// Styling
import './Dashboard.scss';
import 'react-toastify/dist/ReactToastify.css';
import createNewUser from '../../assets/images/create-new-user.png';
import currentExperiences from '../../assets/images/current-experiences.png';
import createExperience from '../../assets/images/create-experience.png';
import archivedExperiences from '../../assets/images/archived-experiences.png';
import currentFoodSamples from '../../assets/images/current-food-samples.png';
import createFoodSample from '../../assets/images/create-food-sample.png';
import archivedFoodSamples from '../../assets/images/archived-food-samples.png';
import myMenuItems from '../../assets/images/my-menu-items.png';
import Ticket from './Ticket/Ticket';
import createMenuItems from '../../assets/images/create-menu-items.png';
import festivalIcon from '../../assets/images/festival-icon.png';
// import specialsIcon from "../../assets/images/star.png";
import experienceOrders from '../../assets/images/experience-orders.png';
import festivalReservations from '../../assets/images/festival-reservations.png';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import BusinessPassportStart from '../Festivals/FestivalCard/BusinessPassportStart';
import BusinessDashboard from './BusinessDashboard/BusinessDashboard';
import Footer from '../Footer/Footer';

toast.configure();

const Dashboard = () => {
  // Set initial state
  const [navBackground, setNavBackground] = useState('nav-white');
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  // To use the JWT credentials
  const history = useHistory();
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  const userRolesString = userRole && userRole.join(', ').toLowerCase();
  const user = appContext.state.user;

  const userRolesStringEdited = userRolesString &&userRolesString.split('_').join(' ')

// console.log("user role string from dashboard: ", userRolesStringEdited)
  if (!userRole) {
    window.location.href = '/';
  }

  let isHost = false;

  if (userRole && userRole.includes('HOST')) {
    isHost = true;
  }

  console.log("user roles:", userRole)

  // console.log("user role from dashboard", userRole[0])
  // console.log("props from dashboard", appContext)
  // Set initial state
  const [createFormsOpened, setCreateFormsOpened] = useState(false);
  const [nationalities, setNationalities] = useState([]);
  const [festivalList, setFestivalList] = useState([]);
  const [formDetails, setFormDetails] = useState({
    input_one: 'product_name',
    input_one_placeholder: 'Name Your Product',
    input_two: 'product_made_in_nationality_id',
    input_three: 'product_price',
    input_three_placeholder: 'Price',
    input_four: 'product_quantity',
    input_four_placeholder: 'Quantity',
    input_five: 'product_size',
    input_five_placeholder: 'Size',
    input_six: 'product_description',
    input_six_placeholder: 'Description',
    input_seven: 'product_expiry_date',
    input_seven_placeholder: 'Expiry Date',
    input_eight: 'product_expiry_time',
    input_eight_placeholder: 'Expiry Time',
    input_nine: 'product_images',
    input_ten: 'product_festival_id',
  });
  const [selected, setSelected] = useState('products');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [businessDetails, setBusinessDetails] = useState();
  let data = {};
  //const [festivalId, setFestivalId] = useState(null);

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'create-forms') {
      setCreateFormsOpened(false);
    }
  };

  // Select product form helper function
  const productSelected = () => {
    setFormDetails({
      input_one: 'product_name',
      input_one_placeholder: 'Name Your Product',
      input_two: 'product_made_in_nationality_id',
      input_three: 'product_price',
      input_three_placeholder: 'Price',
      input_four: 'product_quantity',
      input_four_placeholder: 'Quantity',
      input_five: 'product_size',
      input_five_placeholder: 'Size',
      input_six: 'product_description',
      input_six_placeholder: 'Description',
      input_seven: 'product_expiry_date',
      input_seven_placeholder: 'Expiry Date',
      input_eight: 'product_expiry_time',
      input_eight_placeholder: 'Expiry Time',
      input_nine: 'product_images',
      input_ten: 'product_festivals_id',
    });
  };

  // Select service form helper function
  const serviceSelected = () => {
    setFormDetails({
      input_one: 'service_name',
      input_one_placeholder: 'Name Your Service',
      input_two: 'service_nationality_id',
      input_three: 'service_price',
      input_three_placeholder: 'Price',
      input_four: 'service_capacity',
      input_four_placeholder: 'Capacity',
      input_five: 'service_size_scope',
      input_five_placeholder: 'Scope',
      input_six: 'service_description',
      input_six_placeholder: 'Description',
      input_seven: null,
      input_seven_placeholder: null,
      input_eight: null,
      input_eight_placeholder: null,
      input_nine: 'service_images',
      input_ten: 'service_festival_id',
    });
  };

  // Select experience form helper function
  const experienceSelected = () => {
    setFormDetails({
      input_one: 'experience_name',
      input_one_placeholder: 'Experience Name',
      input_two: 'experience_nationality_id',
      input_three: 'experience_price',
      input_three_placeholder: 'Price',
      input_four: 'experience_capacity',
      input_four_placeholder: 'Capacity',
      input_five: 'experience_size_scope',
      input_five_placeholder: 'Scope',
      input_six: 'experience_description',
      input_six_placeholder: 'Description',
      input_seven: null,
      input_seven_placeholder: null,
      input_eight: null,
      input_eight_placeholder: null,
      input_nine: 'experience_images',
      input_ten: 'experience_festival_id',
    });
  };

  // Change form helper function
  const changeForm = (event) => {
    if (event.target.value === 'experiences') {
      setSelected('experiences');
      experienceSelected();
    } else if (event.target.value === 'services') {
      setSelected('services');
      serviceSelected();
    } else {
      setSelected('products');
      productSelected();
    }
  };

  // Submit product, service or experience helper functions
  //const defaultOnCreatedAction = (festivalId) => {
  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      window.location.href = '/dashboard';
      //window.location.href = `/festival/${festivalId}`;
    }, 2000);

    toast(`Success! Thank you for creating ${selected}!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSubmitAuthDisabled(true);
  };

  const submitProductServiceOrExperience = async (data) => {
    window.scrollTo(0, 0);

    let url = '';
    if (selected === 'products') {
      data.product_creator_type = userRole[0];
      url = '/products/add';
    } else if (selected === 'experiences') {
      url = '/experiences/add';
    } else if (selected === 'services') {
      data.product_creator_type = userRole[0];
      url = '/services/add';
    }

    try {
      const response = await axios({
        method: 'POST',
        url,
        data,
      });

      if (
        response &&
        response.data &&
        response.data.success &&
        userRole &&
        (userRole.includes('VENDOR') || userRole.includes('SPONSOR') || userRole.includes('ADMIN'))
      ) {
        //defaultOnCreatedAction(data.product_festival_id);
        defaultOnCreatedAction();
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    const { ...rest } = values;

    let data = {
      ...rest,
    };

    if (selected === 'products') {
      data.product_expiry_time = formattedTimeToDateTime(
        values.product_expiry_time.toString().substring(16, 21),
      );
      data.product_expiry_date = formatDate(values.product_expiry_date);

      await submitProductServiceOrExperience(data);
    } else {
      await submitProductServiceOrExperience(data);
    }
  };

  // Fetch nationalities helper function
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });

    setFestivalList(response.data.festival_list);
  };

  // Mount Dashboard page
  useEffect(() => {
    // window.location.reload();
    window.scrollTo(0, 0);
    const getBusinessDetails = async () => {
      const userId = appContext.state.user.id;
      const userRole = appContext.state.user.role;
      // const userRolesString = userRole && userRole.join(', ').toLowerCase();
      
      const url = '/get-business-details-all';

      const response = await axios({ method: 'GET', url, userId });
      if (response.data.success) {
        setBusinessDetails(response.data.business_details_all);
      }
    };
    getBusinessDetails();
    fetchNationalities();
    fetchFestivalList();

  }, []);


  

  

  // Render Dashboard page
  let businessMembershipButton = '';
  // let becomeHostButton = "";
  let becomeRestaurantButton = '';
  let becomeSponsorButton = '';

  if (userRole && userRole.includes('MEMBER_BUSINESS_PRO')) {
    businessMembershipButton = (
      <div className="offset-lg-8 col-lg-4 dashboard-header">
        <button className="business-pro-member-btn">Business Pro Member</button>
      </div>
    );
  } else if (userRole && userRole.includes('MEMBER_BUSINESS_ADVANCED')) {
    businessMembershipButton = (
      <div className="offset-lg-8 col-lg-4 dashboard-header">
        <button className="business-advanced-member-btn">Business Advanced Member</button>
      </div>
    );
  } else if (userRole && userRole.includes('MEMBER_BUSINESS_INTERMEDIATE')) {
    businessMembershipButton = (
      <div className="offset-lg-8 col-lg-4 dashboard-header">
        <button className="business-intermediate-member-btn">Business Intermediate Member</button>
      </div>
    );
  } else if (userRole && userRole.includes('MEMBER_BUSINESS_BASIC')) {
    businessMembershipButton = (
      <div className="offset-lg-8 col-lg-4 dashboard-header">
        <button className="business-basic-member-btn">Business Basic Member</button>
      </div>
    );
  } else if (userRole && userRole.includes('MEMBER')) {
    businessMembershipButton = '';
  }

  if (userRole && userRole.includes('RESTAURANT_PENDING')) {
    becomeRestaurantButton = (
      <div className="offset-lg-8 col-lg-4 dashboard-header">
        <button className="pending-btn">Restaurant Application Pending</button>
      </div>
    );
  } else if (userRole && userRole.includes('RESTAURANT')) {
    becomeRestaurantButton = '';
  } else if (userRole && userRole.includes('HOST')) {
    becomeRestaurantButton = (
      <div className="offset-lg-8 col-lg-4 dashboard-header">
        <Link to="/add-restaurant">
          <button className="call-to-action-btn">Add Your Restaurant</button>
        </Link>
      </div>
    );
  }

  if (userRole && userRole.includes('SPONSOR_PENDING')) {
    becomeSponsorButton = (
      <div className="offset-lg-8 col-lg-4 dashboard-header">
        <button className="pending-btn">Sponsor Application Pending</button>
      </div>
    );
  } else if (userRole && userRole.includes('SPONSOR')) {
    becomeSponsorButton = '';
  }

  // if (userRole && userRole.includes("HOST_PENDING")) {
  //   becomeHostButton = (
  //     <div className="offset-lg-8 col-lg-4 dashboard-header">
  //       <button className="pending-btn">Host Application Pending</button>
  //     </div>
  //   );
  // } else if (userRole && userRole.includes("HOST")) {
  //   becomeHostButton = "";
  // } else if (
  //   userRole &&
  //   !userRole.includes("RESTAURANT") &&
  //   !userRole.includes("RESTAURANT_PENDING") &&
  //   !userRole.includes("ADMIN")
  // ) {
  //   becomeHostButton = (
  //     <div className="offset-lg-8 col-lg-4 dashboard-header">
  //       <Link to="/apply">
  //         <button className="call-to-action-btn">Become a Tasttlig Host</button>
  //       </Link>
  //     </div>
  //   );
  // }
  const gotoPasssport = () => {
    window.location.href = `/passport/${appContext.state.user.id}`;
  };
  const handleHostApply = () => {
    window.location.href = '/become-a-host';
  };
  const handleVendApply = () => {
    window.location.href = '/complete-profile/festival-vendor';
  };

  const handleSponsorApply = async () => {
    const url = '/become-in-kind-sponsor';
    const data = { is_sponsor: true };
    const response = await axios({ method: 'POST', url, data });
    if (response && response.data && response.data.success) {
      await history.push({
        pathname: '/sponsor-in-kind-confirmation',
      });
    } else {
      toast(`Something went wrong`, {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const getUserRole = () => {
    return userRole.length > 0 && userRole;
  };

  return (
    // <div className="mt-5 pt-5">
    //   {/* <Nav /> */}
    //   <div className="navbar-layer-a">
    //     <div className="logo overlap-inline">
    //       {businessDetails && businessDetails.business_details_logo ? (
    //         <img src={businessDetails.business_details_logo} className="logo-img rounded-circle" />
    //       ) : (
    //         <span className="fas fa-user-circle fa-3x main-navbar-account-default-picture"></span>
    //       )}
    //     </div>

    //     <div onClick={gotoPasssport} className="overlap-inline">
    //       <span>
    //         <b>
    //           {businessDetails && businessDetails.business_name
    //             ? businessDetails.business_name
    //             : ''}
    //         </b>
    //       </span>
    //       <br />
    //       <span className="text-dark btn-link">View my passport</span>
    //     </div>

    //     <div className="role overlap-inline rounded-pill">
    //       {userRole.length > 0 && userRole.includes('SPONSOR') && (
    //         <span className="role role-text mr-2 py-1 px-1 rounded-pill">
    //           <b>Sponsor</b>
    //         </span>
    //       )}
    //       {userRole.length > 0 && userRole.includes('VENDOR') && (
    //         <span className="role role-text mr-2 py-1 px-1 rounded-pill">
    //           <b>Vendor</b>
    //         </span>
    //       )}
    //       {userRole.length > 0 && userRole.includes('HOST') && (
    //         <span className="role role-text mr-2 py-1 px-1 rounded-pill">
    //           <b>Host</b>
    //         </span>
    //       )}
    //       {userRole.length > 0 && userRole.includes('BUSINESS') && (
    //         <span className="role role-text mr-2 py-1 px-1 rounded-pill">
    //           <b>Business</b>
    //         </span>
    //       )}
    //     </div>

    //     {userRole.length > 0 && !userRole.includes('SPONSOR') && (
    //       <div
    //         onClick={handleSponsorApply}
    //         className="host-apply-btn overlap-inline rounded-pill btn btn-light"
    //       >
    //         <span>
    //           <b>Apply to Sponsor</b>
    //         </span>
    //       </div>
    //     )}
    //     {userRole.length > 0 && !userRole.includes('HOST') && (
    //       <div
    //         onClick={handleHostApply}
    //         className="host-apply-btn overlap-inline rounded-pill btn btn-light"
    //       >
    //         <span>
    //           <b>Apply to Host</b>
    //         </span>
    //       </div>
    //     )}

    //     {userRole && !userRole.includes('VENDOR') && (
    //       <div
    //         onClick={handleVendApply}
    //         className="vend-apply-btn overlap-inline rounded-pill btn btn-light"
    //       >
    //         <span>
    //           <b>Apply to Vend</b>
    //         </span>
    //       </div>
    //     )}

    //     <div className="credit-balance-label overlap-inline">
    //       <span>
    //         <b>Credit Balance</b>
    //       </span>
    //     </div>

    //     <div className="credit-balance overlap-inline rounded-pill">
    //       <span>
    //         <b>$100.00</b>
    //       </span>
    //     </div>

    //     <div className="top-up overlap-inline rounded-pill btn btn-dark">
    //       <span>
    //         <b>Top Up</b>
    //       </span>
    //     </div>
    //   </div>

    //   {/* Product, Service or Experience Modal Form */}
    //   <Modal
    //     isOpen={createFormsOpened}
    //     onRequestClose={closeModal('create-forms')}
    //     ariaHideApp={false}
    //     className="dashboard-forms-modal"
    //   >
    //     <div>
    //       <div className="row">
    //         <div className="col-md-6 my-auto dashboard-forms-input-content-col-1">
    //           <div className="dashboard-forms-title mb-3">Add</div>
    //         </div>
    //         <div className="col-md-6 dashboard-forms-input-content-col-3">
    //           <select
    //             name="select-form"
    //             selected="products"
    //             onChange={changeForm}
    //             disabled={submitAuthDisabled}
    //             className="custom-select mb-3"
    //             required
    //           >
    //             <option value="products">Products</option>
    //             <option value="services">Services</option>
    //             <option value="experiences">Experiences</option>
    //           </select>
    //         </div>
    //         <div className="dashboard-forms-close">
    //           <button
    //             aria-label={`Close Forms Modal`}
    //             onClick={closeModal('create-forms')}
    //             className="fas fa-times fa-2x close-modal"
    //           ></button>
    //         </div>
    //       </div>
    //       <Form data={data} onSubmit={onSubmit}>
    //         <div className="row">
    //           <div className="col-md-6 dashboard-forms-input-content-col-1">
    //             <Input
    //               name={formDetails.input_one}
    //               placeholder={formDetails.input_one_placeholder}
    //               disabled={submitAuthDisabled}
    //               className="product-name-input"
    //               required
    //               label={null}
    //             />
    //           </div>
    //           <div className="col-md-6 dashboard-forms-input-content-col-3">
    //             {nationalities.length && (
    //               <Select
    //                 name={formDetails.input_two}
    //                 placeholder={formDetails.input_two_placeholder}
    //                 className="product-name-input"
    //                 disabled={submitAuthDisabled}
    //                 required
    //               >
    //                 <option value="">--Nationality--</option>
    //                 {nationalities.map((n) => (
    //                   <option key={n.id} value={n.id}>
    //                     {n.nationality}
    //                   </option>
    //                 ))}
    //               </Select>
    //             )}
    //           </div>
    //         </div>
    //         <div className="row">
    //           <div className="col-md-4 dashboard-forms-input-content-col-1">
    //             <Input
    //               name={formDetails.input_three}
    //               step="0.01"
    //               placeholder={formDetails.input_three_placeholder}
    //               className="product-name-input"
    //               disabled={submitAuthDisabled}
    //               required
    //             />
    //           </div>
    //           <div className="col-md-4 dashboard-forms-input-content-col-2">
    //             <Input
    //               name={formDetails.input_four}
    //               type="number"
    //               placeholder={formDetails.input_four_placeholder}
    //               min="1"
    //               className="product-name-input"
    //               disabled={submitAuthDisabled}
    //               required
    //             />
    //           </div>
    //           <div className="col-md-4 dashboard-forms-input-content-col-3">
    //             <Select
    //               name={formDetails.input_five}
    //               placeholder={formDetails.input_five_placeholder}
    //               className="product-name-input"
    //               disabled={submitAuthDisabled}
    //               required
    //             >
    //               <option value="">{`--${
    //                 formDetails.input_five === 'product_size' ? 'Size' : 'Scope'
    //               }--`}</option>
    //               <option value="Bite Size">Bite Size</option>
    //               <option value="Quarter">Quarter</option>
    //               <option value="Half">Half</option>
    //               <option value="Full">Full</option>
    //             </Select>
    //           </div>
    //         </div>
    //         {selected === 'products' && (
    //           <div className="row">
    //             <div className="col-md-6 dashboard-forms-input-content-col-1">
    //               <DateInput
    //                 name={formDetails.input_seven}
    //                 placeholderText={formDetails.input_seven_placeholder}
    //                 dateFormat="yyyy/MM/dd"
    //                 minDate={new Date()}
    //                 peekNextMonth
    //                 showMonthDropdown
    //                 showYearDropdown
    //                 dropdownMode="select"
    //                 disabled={submitAuthDisabled}
    //                 className="product-name-input"
    //                 required
    //               />
    //             </div>
    //             <div className="col-md-6 dashboard-forms-input-content-col-3">
    //               <DateInput
    //                 name={formDetails.input_eight}
    //                 placeholderText={formDetails.input_eight_placeholder}
    //                 showTimeSelect
    //                 showTimeSelectOnly
    //                 dateFormat="h:mm aa"
    //                 disabled={submitAuthDisabled}
    //                 className="last-date-input"
    //                 required
    //               />
    //             </div>
    //           </div>
    //         )}
    //         <div className="first-row">
    //           <Textarea
    //             name={formDetails.input_six}
    //             placeholder={formDetails.input_six_placeholder}
    //             disabled={submitAuthDisabled}
    //             className="product-name-input"
    //             required
    //           />
    //         </div>
    //         <div className="first-row">
    //           {selected === 'products' && (
    //             <MultiImageInput
    //               name="product_images"
    //               dropbox_label="Click or drag-and-drop to upload one or more images"
    //               disabled={submitAuthDisabled}
    //               className="product-images"
    //               required
    //             />
    //           )}
    //           {selected === 'services' && (
    //             <MultiImageInput
    //               name="service_images"
    //               dropbox_label="Click or drag-and-drop to upload one or more images"
    //               disabled={submitAuthDisabled}
    //               className="product-images"
    //               required
    //             />
    //           )}
    //           {selected === 'experiences' && (
    //             <MultiImageInput
    //               name="experience_images"
    //               dropbox_label="Click or drag-and-drop to upload one or more images"
    //               disabled={submitAuthDisabled}
    //               className="product-images"
    //               required
    //             />
    //           )}
    //         </div>
    //         {festivalList.length ? (
    //           <>
    //             <div>{`Which festival does this ${
    //               selected === 'products'
    //                 ? 'product'
    //                 : selected === 'services'
    //                 ? 'service'
    //                 : 'experience'
    //             } belong to?`}</div>
    //             <div>
    //               <Select
    //                 name={formDetails.input_ten}
    //                 className="product-name-input"
    //                 disabled={submitAuthDisabled}
    //                 required
    //               >
    //                 <option value="">--Select--</option>
    //                 {festivalList.map((f) => (
    //                   <option key={f.festival_id} value={f.festival_id}>
    //                     {f.festival_name}
    //                   </option>
    //                 ))}
    //               </Select>
    //             </div>
    //           </>
    //         ) : null}

    //         <div className="first-row">
    //           <button type="submit" className="dashboard-forms-add-btn">
    //             Add
    //           </button>
    //         </div>
    //       </Form>
    //     </div>
    //   </Modal>
    //   {userRole ? (
    //     <DashboardHost />
    //   ) : (
    //     <div className="dashboard">
    //       <div className="row">
    //         {/* <div className="col-lg-8 px-0">
    //         <div className="dashboard-title">Dashboard</div>
    //       </div> */}
    //         {userRole && (userRole.includes('ADMIN') || userRole.includes('VENDOR')) ? (
    //           <div className="col-lg-4 px-0 dashboard-section dashboard-forms-btn-content">
    //             <button onClick={openModal('create-forms')} className="dashboard-forms-btn">
    //               <span className="fas fa-3x fa-plus-circle" />
    //             </button>
    //           </div>
    //         ) : null}
    //         {/* <div className="col-lg-4 dashboard-header">
    //         <div>
    //           <strong>Passport ID: </strong>
    //           {appContext.state.user.passport_id}
    //         </div>
    //         {userRole &&
    //           !userRole.includes("HOST") &&
    //           !userRole.includes("HOST_PENDING") &&
    //           !userRole.includes("ADMIN") &&
    //           !userRole.includes("RESTAURANT") &&
    //           !userRole.includes("RESTAURANT_PENDING") && (
    //             <div>
    //               <strong>Stamps Collected: </strong>
    //               {appContext.state.user.points}
    //             </div>
    //           )}
    //       </div> */}
    //       </div>
    //       <div className="row">{businessMembershipButton}</div>
    //       {/* <div className="row">{becomeHostButton}</div> */}
    //       <div className="row">{becomeRestaurantButton}</div>
    //       <div className="row">{becomeSponsorButton}</div>

    //       {userRole && userRole.includes('ADMIN') && (
    //         <div>
    //           <div className="row">
    //             <div className="col-lg-4 dashboard-section">
    //               <Link
    //                 exact="true"
    //                 to="/complete-profile/business"
    //                 className="dashboard-content-create-from-admin"
    //               >
    //                 <div className="dashboard-content-text">
    //                   <img src={createNewUser} alt="Create New User" className="dashboard-icon" />
    //                   <div>Create New User</div>
    //                 </div>
    //               </Link>
    //             </div>
    //             <div className="col-lg-4 dashboard-section">
    //               <Link
    //                 exact="true"
    //                 to="/create-festival"
    //                 className="dashboard-content-create-from-admin"
    //               >
    //                 <div className="dashboard-content-text">
    //                   <img src={festivalIcon} alt="Create Festival" className="dashboard-icon" />
    //                   <div>Create Festival</div>
    //                 </div>
    //               </Link>
    //             </div>
    //           </div>
    //         </div>
    //       )}

    //       {/* {userRole &&
    //       (userRole.includes("HOST") ||
    //         userRole.includes("HOST_PENDING") ||
    //         userRole.includes("ADMIN") ||
    //         userRole.includes("RESTAURANT") ||
    //         userRole.includes("RESTAURANT_PENDING")) && (
    //         <div> */}
    //       {/* <div className="row">
    //             <div className="col-lg-4 dashboard-section">
    //               <Link
    //                 exact="true"
    //                 to="/my-current-experiences"
    //                 className="dashboard-content-experiences"
    //               >
    //                 <div className="dashboard-content-text">
    //                   <img
    //                     src={currentExperiences}
    //                     alt="Current Experiences"
    //                     className="dashboard-icon"
    //                   />
    //                   <div>My Current Experiences</div>
    //                 </div>
    //               </Link>
    //             </div>
    //             <div className="col-lg-4 dashboard-section">
    //               <Link
    //                 exact="true"
    //                 to="/create-experience"
    //                 className="dashboard-content-experiences"
    //               >
    //                 <div className="dashboard-content-text">
    //                   <img
    //                     src={createExperience}
    //                     alt="Create Experience"
    //                     className="dashboard-icon"
    //                   />
    //                   <div>Create Experience</div>
    //                 </div>
    //               </Link>
    //             </div>
    //             <div className="col-lg-4 dashboard-section">
    //               <Link
    //                 exact="true"
    //                 to="/my-archived-experiences"
    //                 className="dashboard-content-experiences"
    //               >
    //                 <div className="dashboard-content-text">
    //                   <img
    //                     src={archivedExperiences}
    //                     alt="My Archived Experiences"
    //                     className="dashboard-icon"
    //                   />
    //                   <div>My Archived Experiences</div>
    //                 </div>
    //               </Link>
    //             </div>
    //           </div> */}

    //       {userRole &&
    //         (userRole.includes('ADMIN') ||
    //           userRole.includes('RESTAURANT') ||
    //           userRole.includes('RESTAURANT_PENDING') ||
    //           userRole.includes('HOST') ||
    //           userRole.includes('HOST_PENDING')) && (
    //           <div>
    //             <div className="row">
    //               <div className="col-lg-4 dashboard-section">
    //                 <Link
    //                   exact="true"
    //                   to="/my-current-food-samples"
    //                   className="dashboard-content-food-samples"
    //                 >
    //                   <div className="dashboard-content-text">
    //                     <img
    //                       src={currentFoodSamples}
    //                       alt="Current Food Samples"
    //                       className="dashboard-icon"
    //                     />
    //                     <div>My Current Food Samples</div>
    //                   </div>
    //                 </Link>
    //               </div>
    //               <div className="col-lg-4 dashboard-section">
    //                 <Link
    //                   exact="true"
    //                   to="/create-food-samples"
    //                   className="dashboard-content-food-samples"
    //                 >
    //                   <div className="dashboard-content-text">
    //                     <img
    //                       src={createFoodSample}
    //                       alt="Create Food Sample"
    //                       className="dashboard-icon"
    //                     />
    //                     <div>Create Food Samples</div>
    //                   </div>
    //                 </Link>
    //               </div>
    //               <div className="col-lg-4 dashboard-section">
    //                 <Link
    //                   exact="true"
    //                   to="/my-archived-food-samples"
    //                   className="dashboard-content-food-samples"
    //                 >
    //                   <div className="dashboard-content-text">
    //                     <img
    //                       src={archivedFoodSamples}
    //                       alt="My Archived Food Samples"
    //                       className="dashboard-icon"
    //                     />
    //                     <div>My Archived Food Samples</div>
    //                   </div>
    //                 </Link>
    //               </div>
    //             </div>
    //             {/* <div className="row">
    //                   <div className="col-lg-4 dashboard-section">
    //                     <Link
    //                       exact="true"
    //                       to="/my-menu-items"
    //                       className="dashboard-content-menu-items"
    //                     >
    //                       <div className="dashboard-content-text">
    //                         <img
    //                           src={myMenuItems}
    //                           alt="My Menu Items"
    //                           className="dashboard-icon"
    //                         />
    //                         <div>My Menu Items</div>
    //                       </div>
    //                     </Link>
    //                   </div>
    //                   <div className="col-lg-4 dashboard-section">
    //                     <Link
    //                       exact="true"
    //                       to="/create-menu-items"
    //                       className="dashboard-content-menu-items"
    //                     >
    //                       <div className="dashboard-content-text">
    //                         <img
    //                           src={createMenuItems}
    //                           alt="Create Menu Items"
    //                           className="dashboard-icon"
    //                         />
    //                         <div>Create Menu Items</div>
    //                       </div>
    //                     </Link>
    //                   </div> */}
    //           </div>
    //           // </div>
    //         )}
    //       {/* </div>
    //        )} */}

    //       {/* {userRole &&
    //       (userRole.includes("ADMIN") ||
    //         userRole.includes("RESTAURANT") ||
    //         userRole.includes("RESTAURANT_PENDING")) && (
    //         <div className="row">
    //           <div className="col-lg-4 dashboard-section">
    //             <Link
    //               exact="true"
    //               to="/dashboard/festivals"
    //               className="dashboard-content-consumers"
    //             >
    //               <div className="dashboard-content-text">
    //                 <img
    //                   src={festivalIcon}
    //                   alt="All Festivals"
    //                   className="dashboard-icon"
    //                 />
    //                 <div>Festivals</div>
    //               </div>
    //             </Link>
    //           </div>
    //           <div className="col-lg-4 dashboard-section">
    //             <Link
    //               exact="true"
    //               to="/dashboard/specials"
    //               className="dashboard-content-consumers"
    //             >
    //               <div className="dashboard-content-text">
    //                 <img
    //                   src={specialsIcon}
    //                   alt="All Festivals"
    //                   className="dashboard-icon"
    //                 />
    //                 <div>Specials</div>
    //               </div>
    //             </Link>
    //           </div>
    //         </div>
    //       )} */}

    //       {/* {userRole &&
    //       !userRole.includes("HOST") &&
    //       !userRole.includes("HOST_PENDING") &&
    //       !userRole.includes("ADMIN") &&
    //       !userRole.includes("RESTAURANT") &&
    //       !userRole.includes("RESTAURANT_PENDING") &&
    //       !userRole.includes("SPONSOR") &&
    //       !userRole.includes("SPONSOR_PENDING") &&
    //       !userRole.includes("VENDOR") &&
    //       !userRole.includes("VENDOR_PENDING") && (
    //         <div >
    //           <Ticket /> */}
    //       {/* <div className="col-lg-4 dashboard-section">
    //             <Link
    //               exact="true"
    //               to="/dashboard/orders"
    //               className="dashboard-content-consumers"
    //             >
    //               <div className="dashboard-content-text">
    //                 <img
    //                   src={experienceOrders}
    //                   alt="My Experience Orders"
    //                   className="dashboard-icon"
    //                 />
    //                 <div>My Experience Orders</div>
    //               </div>
    //             </Link>
    //           </div> */}
    //       {/* <div className="col-lg-4 dashboard-section">
    //             <Link
    //               exact="true"
    //               to="/dashboard/reservations"
    //               className="dashboard-content-consumers"
    //             >
    //               <div className="dashboard-content-text">
    //                 <img
    //                   src={festivalReservations}
    //                   alt="My Festival Reservations"
    //                   className="dashboard-icon"
    //                 />
    //                 <div>My Festival Reservations</div>
    //               </div>
    //             </Link>
    //           </div> */}
    //       {/* <div className="col-lg-4 dashboard-section">
    //             <Link
    //               exact="true"
    //               to="/dashboard/tickets"
    //               className="dashboard-content-consumers"
    //             >
    //               <div className="dashboard-content-text">
    //                 <img
    //                   src={festivalReservations}
    //                   alt="My Ticket Reservations"
    //                   className="dashboard-icon"
    //                 />
    //                 <div>My Ticket Reservations</div>
    //               </div>
    //             </Link>
    //           </div> */}
    //       {/*  </div> */}
    //       {/* ) */}
    //     </div>
    //   )}
    // </div>
    <Fragment>
      <Nav background={navBackground} />
      <div className="container dashboard-container">
        <section className="passport-sections passport-header">
          <div className="container passport-header-content">
            <div className="passport-profile-left">
              <div className="passport-profile-pic">
                <img src={appContext.state.user.profile_image ? appContext.state.user.profile_image : "/img/passport-profile-pic.png"} alt="Passport Display" 
                width="80" height="90"
                />
              </div>
              <div className="passport-display-name-roles">
                <strong>
                  <span className="passport-name">{`${appContext.state.user.first_name} ${appContext.state.user.last_name}`}</span>
                </strong>
                <span className="passport-roles">{userRolesStringEdited}</span>
              </div>
            </div>
            {/* {!appContext.state.user.id ||
          (window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile` &&
            window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile` &&
            window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile` &&
            userRole &&
            userRole.includes('BUSINESS_MEMBER_PENDING') ||
            userRole.includes('BUSINESS_MEMBER')) ? (
            <div>
              <Link exact="true" to="/guest-host-plan" className="main-navbar-item nav-host-btn">
                View Packages
              </Link>
            </div>
          ) : null} */}
          {/* <div className="passport-credits-right">
             {userRole ?
                  <div>
                  <Link exact="true" to="/guest-vendor-plan" className="modal-button2">
                    View Subscription Packages
                  </Link>
                </div>

              : null}
          </div> */}

          <div className="passport-credits-right">
           {/* {userRole &&
            userRole.includes('VENDOR') || userRole.includes('HOST')  ? ( */}
            <div>
              <Link exact="true" to="/sponsor-plan" className="modal-button2">
                Become a Sponsor
              </Link>
            </div>
          {/* ) : null} */}
          </div>
            {/* <div className="passport-credits-right">
                    <button
                      type="button"
                      className="modal-button2"
                      onClick={() => {
                        window.location.href = '/';
                      }}
                    >
                      Go to Festivals{' '}
                    </button>
              </div> */}
            {/* <div className="passport-credits-right">
                            <span className="credits">
                <strong>Credits:</strong> 250
              </span>
              <i class="fas fa-plus add-credits"></i>
            </div> */}
          </div>
        </section>
        <DashboardHost />
      </div>
      <Footer />
    </Fragment>
  );
};

export default Dashboard;
