// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';

// Components
import Nav from '../../Navbar/Nav';
import SearchBar from '../../Navbar/SearchBar';
import ExperiencesArchivedCard from './ExperiencesArchivedCard/ExperiencesArchivedCard';

// Styling
import '../Dashboard.scss';

const ExperiencesArchived = (props) => {
  // Set initial state
  const [experiences, setExperiences] = useState([]);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Fetch user experiences archived helper function
  const fetchExperiences = async () => {
    try {
      const url = '/experience/user/archived';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Load next set of user experiences archived helper functions
  const loadNextPage = async (page) => {
    const url = '/experience/user/archived?';
    const acc_token = localStorage.getItem('access_token');
    const headers = { Authorization: `Bearer ${acc_token}` };

    return axios({
      method: 'GET',
      url,
      headers,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
      },
    });
  };

  const handleLoadMore = (page = currentPage, experience = experiences) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);
      setExperiences(experience.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Your Archived Experiences page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchExperiences().then(({ data }) => {
      setExperiences(data.details.data);
    });
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [props.location.state.keyword]);

  // Render empty page
  const Empty = () => {
    return <strong className="no-experiences-found">No experiences found.</strong>;
  };

  // Render search bar
  const Filters = () => (
    <div className="page-filters">
      <section className="mb-4">
        <SearchBar keyword={props.location.state.keyword} url="/my-archived-experiences" />
      </section>
      <hr />
    </div>
  );

  // Render Your Archived Experiences page
  return (
    <div>
      <Nav />

      <div className="your-archived-food-experiences">
        <div className="your-archived-experiences-title">Your Archived Experiences</div>
        <div className="row">
          <div className="col-md-3 p-0">
            <Filters />
          </div>
          <div className="col-md-9 p-0">
            <div className="experience-cards" ref={infiniteRef}>
              {experiences.length !== 0 ? (
                experiences.map((experience) => (
                  <ExperiencesArchivedCard
                    key={experience.experience_id}
                    experienceProps={experience}
                  />
                ))
              ) : (
                <Empty />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExperiencesArchived;
