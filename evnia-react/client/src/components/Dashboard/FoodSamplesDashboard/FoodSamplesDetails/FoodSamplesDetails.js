// Libraries
import React, { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { Input, Form, DateInput, MultiImageInput, Select, Textarea,   Checkbox,CheckboxGroup } from '../../../EasyForm';

// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import {
  formattedTimeToDateTime,
  formatMilitaryToStandardTime,
  formatDate,
} from '../../../Functions/Functions';

// Styling
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../../assets/images/live.png';

toast.configure();

const FoodSamplesDetails = (props) => {
  console.log('props from food sample details: ', props);

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const [orderList, setOrderList] = useState([]);
  const [, setLoading] = useState(false);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);
  const history = useHistory();
  const [foodSamplePreference, setFoodSamplePreference] = useState([]);
  const [foodSamplePreferenceArray, setFoodSamplePreferenceArray] = useState(foodSamplePreference);
  
  const [detailsOpened, toggleDetailsOpened] = useState("your-orders-item");

  let daysAvailable = [];
  props.is_available_on_monday && daysAvailable.push('available_on_monday');
  props.is_available_on_tuesday && daysAvailable.push('available_on_tuesday');
  props.is_available_on_wednesday && daysAvailable.push('available_on_wednesday');
  props.is_available_on_thursday && daysAvailable.push('available_on_thursday');
  props.is_available_on_friday && daysAvailable.push('available_on_friday');
  props.is_available_on_saturday && daysAvailable.push('available_on_saturday');
  props.is_available_on_sunday && daysAvailable.push('available_on_sunday');

  // Set dietary restrictions
  let dietaryRestrictions = [];
  props.is_vegetarian && dietaryRestrictions.push('vegetarian');
  props.is_vegan && dietaryRestrictions.push('vegan');
  props.is_gluten_free && dietaryRestrictions.push('glutenFree');
  props.is_halal && dietaryRestrictions.push('halal');

  // Split address into address lines 1 and 2
  let addressParts = props.address.split(', ');
  let addressLine1 = addressParts[0];
  let addressLine2 = addressParts[1];

  // Edit current food sample helper function
  const handleEditFoodSample = () => {
    history.push({
      pathname: '/edit-food-sample',
      state: {
        foodSampleId: props.food_sample_id,
        images: props.image_urls,
        title: props.title,
        nationality_id: props.nationality_id,
        start_date: props.start_date,
        end_date: props.end_date,
        start_time: formattedTimeToDateTime(props.start_time),
        end_time: formattedTimeToDateTime(props.end_time),
        sample_size: props.sample_size,
        quantity: props.quantity,
        dietaryRestrictions,
        daysAvailable,
        spice_level: props.spice_level,
        addressLine1,
        addressLine2,
        city: props.city,
        provinceTerritory: props.state,
        postal_code: props.postal_code,
        description: props.description,
      },
    });
  };

  const onChangeFoodSample = function () {
  
    setFoodSamplePreference([...foodSamplePreference].concat(props.foodSampleId));
    console.log("food sample preffee", ...foodSamplePreference)
    // console.log("event from change: ", props.foodSampleId)
    // if (foodSamplePreference.includes(props.foodSampleId)) {
    //   setFoodSamplePreference(foodSamplePreference.filter((item) => {
    //     console.log("im in ifff", item)
    //     return item !== props.foodSampleId
    // }))
      
    
    // } else {
    //   console.log("im in elseee!!")
    //   setFoodSamplePreference(foodSamplePreference.concat(props.foodSampleId));
    //   // const matchedFestival = festivalDetails.find((value) => {
    //   //   return value.festival_id === Number(event.target.value);
    //   // });
     
    //   //setFestivalPreferenceName(festivalPreferenceName.concat(matchedFestival.festival_name))
    // }
  };
  // console.log("food sample preference>>>>>>>>>>>>>>", foodSamplePreferenceArray)


  const openModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(true);
    }
  };
  console.log("food sample preference>>>>>>>>>>>>>>", foodSamplePreference)

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(false);
    }
  };
  // Render Past Festival Table Row
  return (
    <LazyLoad once>
      <Modal
        isOpen={passportDetailsOpened}
        onRequestClose={closeModal('passport-details')}
        ariaHideApp={false}
        className="passport-details-modal"
      >
        <div className="text-right">
          <button
            aria-label={`Close ${props.title} Details Modal`}
            onClick={closeModal('passport-details')}
            className="fas fa-times fa-2x close-modal"
          ></button>
        </div>

        <div className="text-left">
          <div className="passport-item-title">{props.title}</div>
          <div className="mb-3 passport-image__full-width">
            <ImageSlider images={props.image_urls} />
          </div>

          <div>
            <div className="mb-3">
              <div className="passport-details-sub-title">Address</div>
              <div>{`${props.address}, ${props.city}, ${props.state} ${props.postal_code}`}</div>
            </div>
            <div className="mb-3">
              <div className="passport-details-sub-title">Date</div>
              <div>{`${formatDate(props.start_date)} to ${formatDate(props.end_date)}`}</div>
            </div>
            <div className="mb-3">
              <div className="passport-details-sub-title">Time</div>
              <div>{`${formatMilitaryToStandardTime(
                props.start_time,
              )} to ${formatMilitaryToStandardTime(props.end_time)}`}</div>
            </div>
            <div className="mb-3">
              <div className="passport-details-sub-title">Sample Size</div>
              <div>{`${props.size}`}</div>
            </div>
            <div className="mb-3">
              <div className="passport-details-sub-title">Days Available</div>
              {props.is_available_on_monday && <div>Monday</div>}
              {props.is_available_on_tuesday && <div>Tuesday</div>}
              {props.is_available_on_wednesday && <div>Wednesday</div>}
              {props.is_available_on_thursday && <div>Thursday</div>}
              {props.is_available_on_friday && <div>Friday</div>}
              {props.is_available_on_saturday && <div>Saturday</div>}
              {props.is_available_on_sunday && <div>Sunday</div>}
            </div>
            <div className="mb-3">
              <div className="passport-details-sub-title">Description</div>
              <div>{props.description}</div>
            </div>
          </div>
        </div>

        <div className="close-modal-bottom-content">
          <span onClick={closeModal('passport-details')} className="close-modal-bottom">
            Close
          </span>
        </div>
      </Modal>
      {/* {() => toggleDetailsOpened("do-you-have-a-restaurant-yes1-btn")} */}
      <tr 
        onClick={onChangeFoodSample}
        data-toggle="collapse"
        // data-target={`#detail_${props.foodSampleId}`}
        className={detailsOpened}
      >
        <td scope="row">{props.foodSampleId}</td>

        <td>
          {' '}
          <Link exact="true" onClick={openModal('passport-details')} className="ticket-card-link">
            {props.title}
          </Link>
        </td>
        <td>${props.price}</td>
        <td>${props.discounted_price}</td>
        <td>{props.status}</td>
        <td>{props.size}</td>
        <td>{props.spice}</td>
        <td>{props.quantity}</td>
        <td>{props.nationality}</td>

        <i class="fas fa-edit" onClick={handleEditFoodSample}></i>
      </tr>
    </LazyLoad>
  );
};

export default FoodSamplesDetails;
