// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../../ContextProvider/AppProvider';
import moment, { now } from 'moment';
import { formatDate } from '../../Functions/Functions';
import autoTable from 'jspdf-autotable';
import dropDownArrow from '../../../assets/images/shapes/6.png';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer';
import FoodSamplesDetails from './FoodSamplesDetails/FoodSamplesDetails';
// Styling
import './FoodSamplesDashboard.scss';
import 'react-datepicker/dist/react-datepicker.css';
import GoTop from '../../Shared/GoTop';

const FoodSamples = (props) => {
  // Set initial state
  const [passportItems, setPassportItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [ticketItems, setTicketItems] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  // let expiredFestivals = [];

  // // check if festival has passed, if it did add it to expiredFestivals
  // passportItems.forEach((row) => {
  //   if (moment(row.festival_end_date).isBefore()) {
  //     expiredFestivals.push(row);
  //   }
  // });

  // Render past festival table rows helper
  const renderFoodSampleRows = (arr) => {
    return arr.map((row, index) => (
      <FoodSamplesDetails
        key={index}
        foodSampleId={row.food_sample_id}
        title={row.title}
        price={row.price}
        image_urls={row.image_urls}
        type={row.food_sample_type}
        size={row.sample_size}
        spice={row.spice_level}
        status={row.status}
        quantity={row.quantity}
        description={row.description}
        nationality={row.nationality}
        history={row.history}
        address={row.address}
        city={row.city}
        state={row.state}
        postal_code={row.postal_code}
        start_date={row.start_date}
        end_date={row.end_date}
        start_time={row.start_time}
        end_time={row.end_time}
        is_available_on_monday={row.is_available_on_monday}
        is_available_on_tuesday={row.is_available_on_tuesday}
        is_available_on_wednesday={row.is_available_on_wednesday}
        is_available_on_thursday={row.is_available_on_thursday}
        is_available_on_friday={row.is_available_on_friday}
        is_available_on_saturday={row.is_available_on_saturday}
        is_available_on_sunday={row.is_available_on_sunday}
      />
    ));
  };

  const [foodSamples, setFoodSamples] = useState([]);

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Fetch user food samples created helper function
  const fetchFoodSamples = async () => {
    try {
      const url = '/food-sample/user/all';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Load next set of user food samples helper functions
  const loadNextPage = async (page) => {
    const url = '/food-sample/user/all?';
    const acc_token = localStorage.getItem('access_token');
    const headers = { Authorization: `Bearer ${acc_token}` };

    return axios({
      method: 'GET',
      url,
      headers,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
      },
    });
  };

  const handleLoadMore = (page = currentPage, foodSample = foodSamples) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);
      setFoodSamples(foodSample.concat(newPage.data.details.data));
    });
  };
  console.log('food samples: ', foodSamples);

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Your Current Food Samples page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchFoodSamples().then(({ data }) => {
      setFoodSamples(data.details.data);
    });
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [props.location.state.keyword]);

  // Render empty page
  const Empty = () => {
    return <strong className="no-food-samples-found">No food samples found.</strong>;
  };

  // Render Passport component
  return (
    <div>
      <Nav />
      <div className="dashboard">
        {userRole &&
        userRole.includes('HOST') &&
        !userRole.includes('HOST_PENDING') &&
        !userRole.includes('ADMIN') &&
        !userRole.includes('RESTAURANT') &&
        !userRole.includes('RESTAURANT_PENDING') &&
        !userRole.includes('SPONSOR') &&
        !userRole.includes('SPONSOR_PENDING') &&
        !userRole.includes('VENDOR') &&
        !userRole.includes('VENDOR_PENDING') ? (
          <Link exact="true" to="/dashboard/my-redeem-food">
            <h4 className="ticket-sub-image">My Redemptions</h4>
          </Link>
        ) : (
          <h7>Once you are approved for hosting then you can redeem!</h7>
        )}

        <span className="ticket-table-head">
          <h2 className="ticket-sub-title">My Food Samples</h2>
          <Link exact="true" to="/">
            <h4 className="ticket-sub-image">
              <i className="fas fa-running ticket-icon"></i>
              Go to Tasttlig
            </h4>
          </Link>
        </span>
      </div>

      <div className="ticket-container">
        <div className="row ticket-row">
          <table className="table table-striped ticket-table">
            <thead>
              <tr>
                <th scope="col">Product ID</th>
                <th scope="col">Product Name</th>
                <th scope="col">Product Price</th>
                <th scope="col">Product Status</th>
                <th scope="col">Size</th>
                <th scope="col">Spice Level</th>
                <th scope="col">Quantity</th>
                {/* <th scope="col">Description</th> */}
                <th scope="col">Made in</th>
              </tr>
            </thead>
            <tbody>
              {foodSamples.length !== 0 ? renderFoodSampleRows(foodSamples) : <Empty />}
            </tbody>
          </table>
        </div>
      </div>
      {/* <Footer/>
    <GoTop scrollStepInPx="50" delayInMs="16.66" /> */}
    </div>
  );
};

export default FoodSamples;
