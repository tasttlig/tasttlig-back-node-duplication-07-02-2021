// Libraries
import React, {useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';

// Components
import Nav from '../../Navbar/Nav';

// Styling
import './HostPanel.scss';
import allHostApplicants from '../../../assets/images/all-host-applicants.png';
import { AppContext } from '../../../ContextProvider/AppProvider';
import addApplication from '../../../assets/images/add-application.png';
import createNewUser from '../../../assets/images/create-new-user.png';
import createFoodSample from '../../../assets/images/create-food-sample.png';
import axios from 'axios';
import festivalIcon from '../../../assets/images/festival-icon.png';

const HostPanel = () => {

  const appContext = useContext(AppContext);
  const [festivalItems, setFestivalItems] = useState([]);


  const fetchHostFestivals = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/festivals/${appContext.state.user.id}`,
        params: {
          user_id: appContext.state.user.id,
        },
      });
       console.log('fud sm', response.data.details.data);

       setFestivalItems(response.data.details.data);
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  console.log("fesitval")
 
  // Mount Admin page
  useEffect(() => {
    window.scrollTo(0, 0);
     fetchHostFestivals();
  }, []);

  // Render Admin page
  return (
    <div>
      <Nav />

      <div className="admin">
        <div className="admin-title">Host</div>

        <div className="row">
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/host-review/all-vendor-applicants" className="admin-content">
              <div>
                <div>
                  {/* <img src={allHostApplicants} alt="All Host Applicants" className="admin-icon" /> */}
                  <img src={createNewUser} alt="Create New User" className="dashboard-icon" />
                </div>
                <div className="admin-sub-title">All Vendor Applicants</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/hosts/all-host-sponsors" className="admin-content">
              <div>
                <div>
                  <i alt="All Host Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">Sponsors List</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/hosts/all-host-vendors" className="admin-content">
              <div>
                <div>
                  <i alt="All Member Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">Vendors List</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link
              exact="true"
              to="/hosts/all-host-guests"
              className="admin-content"
            >
              <div>
                <div>
                  <i alt="All Member Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">Guests List</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/hosts/revenue" className="admin-content">
              <div className="dashboard-content-text">
                <img src={festivalIcon} alt="Create New User" className="dashboard-icon" />
                <div className="admin-sub-title">Revenue</div>
              </div>
            </Link>
          </div>
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/hosts/host-festivals" className="admin-content">
              <div className="dashboard-content-text">
                <img src={createFoodSample} alt="Create Food Sample" className="dashboard-icon" />
                <div className="admin-sub-title">My Festivals</div>
              </div>
            </Link>
          </div>
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/create-festival" className="admin-content">
              <div className="dashboard-content-text">
                <img src={festivalIcon} alt="Create Festival" className="dashboard-icon" />
                <div>Create Festival</div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HostPanel;
