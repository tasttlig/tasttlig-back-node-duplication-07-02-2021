// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../ContextProvider/AppProvider';
import moment, { now } from 'moment';
import { formatDate } from '../Functions/Functions';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';

// Components
import Nav from '../Navbar/Nav';
import PastFestivalsRow from './PastFestivalsRow/PastFestivalsRow';
import CuisineRow from './CuisineRow/CuisineRow';
import FoodTypeRow from './FoodTypeRow/FoodTypeRow';
import AllergiesRow from './AllergiesRow/AllergiesRow';
import GoTop from '../Shared/GoTop';
// Styling
import './Passport.scss';
import '../Profile/Profile.scss';
import 'react-datepicker/dist/react-datepicker.css';

const Passport = (props) => {
  // Source tracking on new sign ups
  // const source = new URLSearchParams(props.location.search).get("source");
  // if (source) {
  //   if (!localStorage.getItem("source")) {
  //     localStorage.setItem("source", source);
  //   }
  // }

  // Set initial state
  const [passportItems, setPassportItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  let expiredFestivals = [];
  let currentFestivals = [];

  // check if festival has passed, if it did add it to expiredFestivals
  // if not add it to current festivals
  passportItems.forEach((row) => {
    if (moment(row.festival_end_date).isBefore()) {
      expiredFestivals.push(row);
    } else {
      currentFestivals.push(row);
    }
  });

  // Render past festival table rows helper
  const renderPastFestivalRows = (arr) => {
    return arr.map((row, index) => (
      <PastFestivalsRow
        key={index}
        ticketId={row.ticket_id}
        festivalId={row.festival_id}
        images={row.image_urls}
        title={row.festival_name}
        type={row.festival_type}
        price={row.festival_price}
        city={row.festival_city}
        startDate={row.festival_start_date}
        endDate={row.festival_end_date}
        startTime={row.festival_start_time}
        endTime={row.festival_end_time}
        description={row.festival_description}
        history={props.history}
      />
    ));
  };

  // Render passport table rows helper
  const renderCusineRows = (arr) => {
    console.log('array from render food', arr);
    return arr.map((row, index) => <CuisineRow key={index} cuisine={row} />);
  };

  // Render passport table rows helper
  const renderFoodTypeRows = (arr) => {
    return arr.map((row, index) => <FoodTypeRow key={index} foodType={row} />);
  };

  // Render passport table rows helper
  const renderAllergiesRows = (arr) => {
    return arr.map((row, index) => <AllergiesRow key={index} allergies={row} />);
  };

  const generatePDF = () => {
    const doc = new jsPDF();

    const cuisineColumn = ['Cuisine Preferences'];
    const cuisineRows = [];

    const foodPreferencesColumn = ['Food Preferences'];
    const foodPreferencesRows = [];

    const allergiesColumn = ['Food Allergies'];
    const allergiesRows = [];

    const expiredFestivalsColumn = [
      'Past Festivals',
      'Date',
      'Location',
      'Price',
      'Ticket Confirmation #',
    ];

    const currentFestivalsColumn = [
      'Current Festivals',
      'Date',
      'Location',
      'Price',
      'Ticket Confirmation #',
    ];

    const expiredFestivalsRows = [];
    const currentFestivalsRows = [];

    if (!passportItems[0]) {
      return false;
    }

    passportItems[0].preferred_country_cuisine.forEach((cuisine) => {
      const cuisineData = [cuisine];
      cuisineRows.push(cuisineData);
    });

    passportItems[0].food_preferences.forEach((foodPreference) => {
      const foodPreferenceData = [foodPreference];
      foodPreferencesRows.push(foodPreferenceData);
    });

    passportItems[0].food_allergies.forEach((allergy) => {
      const allergiesData = [allergy];
      allergiesRows.push(allergiesData);
    });

    currentFestivals.forEach((festival) => {
      const currentFestivalsData = [
        festival.festival_name,
        formatDate(festival.festival_start_date),
        festival.festival_city,
        festival.festival_price,
        festival.ticket_booking_confirmation_id,
      ];
      currentFestivalsRows.push(currentFestivalsData);
    });

    expiredFestivals.forEach((festival) => {
      const expiredFestivalsData = [
        festival.festival_name,
        formatDate(festival.festival_start_date),
        festival.festival_city,
        festival.festival_price,
        festival.ticket_booking_confirmation_id,
      ];
      expiredFestivalsRows.push(expiredFestivalsData);
    });

    doc.text(
      20,
      20,
      `${appContext.state.user.first_name} ${appContext.state.user.last_name}'s Tasttlig Passport.pdf`,
    );

    doc.autoTable(currentFestivalsColumn, currentFestivalsRows, {
      startY: 40,
    });
    doc.autoTable(expiredFestivalsColumn, expiredFestivalsRows, {
      startY: doc.lastAutoTable.finalY + 15,
    });
    doc.autoTable(cuisineColumn, cuisineRows, {
      startY: doc.lastAutoTable.finalY + 15,
    });
    doc.autoTable(foodPreferencesColumn, foodPreferencesRows, {
      startY: doc.lastAutoTable.finalY + 15,
    });
    doc.autoTable(allergiesColumn, allergiesRows, {
      startY: doc.lastAutoTable.finalY + 15,
    });

    doc.save(
      `${appContext.state.user.first_name}_${appContext.state.user.last_name}_tasttlig_passport.pdf`,
    );
  };

  const loadNextPage = async (page) => {
    try {
      const response = await axios({
        method: 'GET',
        url: '/passport',
        params: {
          userId: appContext.state.user.id,
          page: page + 1,
        },
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  const handleLoadMore = (page = currentPage, passport = passportItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);
      if (!newPage) {
        return false;
      }

      const pagination = newPage.data.details.pagination;

      if (pagination && page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }
      setHasNextPage(pagination && currentPage < pagination.lastPage);
      setPassportItems(passport.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Festivals page
  // useEffect(() => {
  //   window.scrollTo(0, 0);

  //   handleLoadMore();
  // }, []);

  // useEffect(() => {
  //   handleLoadMore(0, []);
  // }, [selectedNationalities, startDate, startTime, cityLocation, filterRadius]);

  // Render Passport component
  return (
    <div className="container">
      <div className="row passport-row" ref={infiniteRef}>
        <h3 className="passport-sub-title">Current Festivals</h3>
        <table className="table table-striped passport-table">
          <thead>
            <tr>
              <th scope="col">Festivals</th>
              <th scope="col">Date</th>
              <th scope="col">Location</th>
              <th scope="col">Price</th>
              <th scope="col">Ticket</th>
            </tr>
          </thead>
          <tbody>
            {currentFestivals.length !== 0 ? (
              renderPastFestivalRows(currentFestivals)
            ) : (
              <tr>
                <td>You are not attending any active festivals.</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      <div className="row passport-row" ref={infiniteRef}>
        <h3 className="passport-sub-title">Past Festivals</h3>
        <table className="table table-striped passport-table">
          <thead>
            <tr>
              <th scope="col">Festivals</th>
              <th scope="col">Date</th>
              <th scope="col">Location</th>
              <th scope="col">Price</th>
              <th scope="col">Ticket</th>
            </tr>
          </thead>
          <tbody>
            {expiredFestivals.length !== 0 ? (
              renderPastFestivalRows(expiredFestivals)
            ) : (
              <tr>
                <td>You haven't attended a festival yet</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
      <div className="row passport-row" ref={infiniteRef}>
        <span className="passport-table-head">
          <h3 className="passport-sub-title">Preferences</h3>
          {/* <h3 className="passport-sub-title">
            <i className="fas fa-user-edit passport-icon"></i>
          </h3> */}
        </span>
        <span className="table-caption">
          Your specialized preferences from past festivals and from registration.
        </span>
        <div className="preferences-columns">
          <table className="table table-striped passport-table">
            <thead>
              <tr>
                <th scope="col">Cuisine</th>
              </tr>
            </thead>
            <tbody>
              {passportItems.length !== 0 ? (
                renderCusineRows(passportItems[0].preferred_country_cuisine)
              ) : (
                <tr>
                  <td>No cuisine preferences</td>
                </tr>
              )}
            </tbody>
          </table>
          <table className="table table-striped passport-table">
            <thead>
              <tr>
                <th scope="col">Food Preferences</th>
              </tr>
            </thead>
            <tbody>
              {passportItems.length !== 0 ? (
                renderFoodTypeRows(passportItems[0].food_preferences)
              ) : (
                <tr>
                  <td>No food preferences</td>
                </tr>
              )}
            </tbody>
          </table>
          <table className="table table-striped passport-table">
            <thead>
              <tr>
                <th scope="col">Allergies</th>
              </tr>
            </thead>
            <tbody>
              {passportItems.length !== 0 ? (
                renderAllergiesRows(passportItems[0].food_allergies)
              ) : (
                <tr>
                  <td>No allergies</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
      <div className="row save-pdf-row">
        <button onClick={() => generatePDF()} className="profile-button-primary">
          Save as PDF <i class="far fa-file-pdf"></i>
        </button>
      </div>
    </div>
  );
};

export default Passport;
