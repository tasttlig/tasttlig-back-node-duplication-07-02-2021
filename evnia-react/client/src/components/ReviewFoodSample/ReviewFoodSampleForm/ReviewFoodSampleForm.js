// Libraries
import React, { useState } from 'react';
import axios from 'axios';
import Flag from 'react-world-flags';
import { toast } from 'react-toastify';

// Components
import { formatDate, formatMilitaryToStandardTime } from '../../Functions/Functions';

// Styling
import './ReviewFoodSampleForm.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const ReviewFoodSampleForm = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [reason, setReason] = useState('');
  const [reasonError, setReasonError] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Validate user input for reason/suggestions helper function
  const validateReviewFoodSample = () => {
    if (!reason) {
      setReasonError('Reason/suggestions are required.');
    } else {
      setReasonError('');
    }

    if (!reason) {
      return false;
    }

    return true;
  };

  // Accept food sample helper function
  const handleSubmitAcceptFoodSample = async (event) => {
    event.preventDefault();

    const isValid = validateReviewFoodSample();

    if (isValid) {
      const url = '/food-sample/review';

      const data = {
        token: props.token,
        id: props.reviewFoodSample.food_sample_id,
        food_sample_update_data: {
          status: 'ACTIVE',
          review_food_sample_reason: reason,
        },
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);

          toast('Success! Thank you for accepting this food sample!', {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Reject food-sample helper function
  const handleSubmitRejectFoodSample = async (event) => {
    event.preventDefault();

    const isValid = validateReviewFoodSample();

    if (isValid) {
      const url = '/food-sample/review';

      const data = {
        token: props.token,
        id: props.reviewFoodSample.food_sample_id,
        food_sample_update_data: {
          status: 'ARCHIVED',
          review_food_sample_reason: reason,
        },
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);

          toast('Success! Thank you for rejecting this food sample!', {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Destructuring
  const {
    image_urls,
    title,
    alpha_2_code,
    nationality,
    address,
    city,
    state,
    postal_code,
    start_date,
    end_date,
    start_time,
    end_time,
    sample_size,
    is_available_on_monday,
    is_available_on_tuesday,
    is_available_on_wednesday,
    is_available_on_thursday,
    is_available_on_friday,
    is_available_on_saturday,
    is_available_on_sunday,
    description,
  } = props.reviewFoodSample;

  return (
    <div className="review-food-sample">
      <div className="review-food-sample-title">Review Food Sample</div>

      <div className="row">
        <div className="col-md-6 review-food-sample-image-content">
          <img
            src={image_urls[0]}
            alt={title}
            onLoad={() => setLoad(true)}
            className={load ? 'review-food-sample-image' : 'loading-image'}
          />
          <div className="passport-card-flag-container">
            <Flag code={alpha_2_code} className="passport-card-flag" />
            <div className="passport-card-flag-country">{nationality}</div>
          </div>
        </div>
        <div className="col-md-6 review-food-sample-text-content">
          <div className="mb-3">
            <div className="review-food-sample-sub-title">Title</div>
            <div>{title}</div>
          </div>
          <div className="mb-3">
            <div className="review-food-sample-sub-title">Address</div>
            <div>{`${address}, ${city}, ${state} ${postal_code}`}</div>
          </div>
          <div className="mb-3">
            <div className="review-food-sample-sub-title">Date</div>
            <div>{`${formatDate(start_date)} to ${formatDate(end_date)}`}</div>
          </div>
          <div className="mb-3">
            <div className="review-food-sample-sub-title">Time</div>
            <div>{`${formatMilitaryToStandardTime(start_time)} to ${formatMilitaryToStandardTime(
              end_time,
            )}`}</div>
          </div>
          <div className="mb-3">
            <div className="review-food-sample-sub-title">Sample Size</div>
            <div>{sample_size}</div>
          </div>
          <div className="mb-3">
            <div className="review-food-sample-sub-title">Days Available</div>
            {is_available_on_monday && <div>Monday</div>}
            {is_available_on_tuesday && <div>Tuesday</div>}
            {is_available_on_wednesday && <div>Wednesday</div>}
            {is_available_on_thursday && <div>Thursday</div>}
            {is_available_on_friday && <div>Friday</div>}
            {is_available_on_saturday && <div>Saturday</div>}
            {is_available_on_sunday && <div>Sunday</div>}
          </div>
          <div className="mb-3">
            <div className="review-food-sample-sub-title">Description</div>
            <div>{description}</div>
          </div>
        </div>
      </div>

      <div className="review-food-sample-reason-suggestions-content">
        <div className="input-title">Reason/Suggestions*</div>
        <textarea
          value={reason}
          onChange={(e) => setReason(e.target.value)}
          disabled={submitAuthDisabled}
          className="review-food-sample-reason-suggestions"
          required
        />
        {reasonError && <div className="error-message">{reasonError}</div>}
      </div>

      <div className="row">
        <div className="col-md-6 review-food-sample-accept-content">
          <form onSubmit={handleSubmitAcceptFoodSample} noValidate>
            <button
              type="submit"
              disabled={submitAuthDisabled}
              className="btn btn-success review-food-sample-accept-btn"
            >
              Accept
            </button>
          </form>
        </div>
        <div className="col-md-6 review-food-sample-reject-content">
          <form onSubmit={handleSubmitRejectFoodSample} noValidate>
            <button
              type="submit"
              disabled={submitAuthDisabled}
              className="btn btn-danger review-food-sample-reject-btn"
            >
              Reject
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ReviewFoodSampleForm;
