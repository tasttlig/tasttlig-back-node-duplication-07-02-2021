// Libraries
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

// Components
import Footer from '../Home/BannerFooter/BannerFooter';

// Styling
import './ApplyToHost.scss';
import '../Home/HowItWorks/HowItWorks.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import applyToHostBanner from '../../assets/images/apply-to-host-banner.png';
import documents from '../../assets/images/documents.png';
import video from '../../assets/images/video.png';
import profile from '../../assets/images/profile.png';
import connect from '../../assets/images/connect.png';
import select from '../../assets/images/select.png';
import entertainment from '../../assets/images/entertainment.png';
import group from '../../assets/images/group.png';
import building from '../../assets/images/building.png';
import money from '../../assets/images/money.png';

const ApplyToHost = () => {
  // Mount Apply to Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Apply to Host page
  return (
    <div>
      <div className="navbar">
        <Link exact="true" to="/">
          <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
        </Link>
        <Link to="/apply" className="apply-to-host-navbar-btn">
          Get Started
        </Link>
      </div>

      <div className="row apply-to-host-header">
        <div className="col-lg-8 apply-to-host-header-image-content">
          <img src={applyToHostBanner} alt="Apply to Host" className="apply-to-host-header-image" />
        </div>
        <div className="col-lg-4 apply-to-host-header-text-content">
          <div className="apply-to-host-header-text">
            Tasttlig Hosts make money hosting Tasttlig experiences to showcase the world in the best
            light.
          </div>
          <Link to="/apply" className="apply-to-host-header-btn">
            Get Started
          </Link>
        </div>
      </div>

      <div className="text-center apply-to-host-body">
        <div className="apply-to-host-body-title">Tasttlig Host</div>

        <div className="apply-to-host-body-section">
          <div className="apply-to-host-body-timeline"></div>

          <div className="apply-to-host-body-sub-title">Apply to Host</div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <img src={documents} alt="Documents" />
              <span className="apply-to-host-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <div>Submit documents</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <div>Have a video Interview</div>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <span className="apply-to-host-body-timeline-circle-left"></span>
              <img src={video} alt="Video" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <img src={profile} alt="Profile" />
              <span className="apply-to-host-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <div>Become a Tasttlig Host</div>
            </div>
          </div>

          <div className="apply-to-host-body-sub-title">Create an Experience</div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <div>Connect to local restaurants serving food with cultural identities</div>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <span className="apply-to-host-body-timeline-circle-left"></span>
              <img src={connect} alt="Connect" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <img src={select} alt="Select" />
              <span className="apply-to-host-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <div>Select 3 items on restaurant menu representing the taste of the culture</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <div>Add Entertainment including music, arts and games to your experience</div>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <span className="apply-to-host-body-timeline-circle-left"></span>
              <img src={entertainment} alt="Entertainment" />
            </div>
          </div>

          <div className="apply-to-host-body-sub-title">Host the Experience</div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <img src={group} alt="Group" />
              <span className="apply-to-host-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <div>Promote the experience to guests</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <div>Meet guests at the restaurant</div>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <span className="apply-to-host-body-timeline-circle-left"></span>
              <img src={building} alt="Building" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-host-body-text">
              <img src={money} alt="Money" />
              <span className="apply-to-host-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-host-body-text">
              <div>Host the party</div>
            </div>
          </div>

          <Link exact="true" to="/apply" className="apply-to-host-body-btn">
            Get Started
          </Link>
        </div>

        {/* Responsive Design */}
        <div className="apply-to-host-body-section-responsive">
          <div className="apply-to-host-body-sub-title-responsive">Apply to Host</div>

          <div className="mb-4">
            <img src={documents} alt="Documents" />
            <div className="apply-to-host-body-text-responsive">Submit documents</div>
          </div>

          <div className="mb-4">
            <img src={video} alt="Video" />
            <div className="apply-to-host-body-text-responsive">Have a video Interview</div>
          </div>

          <div className="mb-4">
            <img src={profile} alt="Profile" />
            <div className="apply-to-host-body-text-responsive">Become a Tasttlig Host</div>
          </div>

          <div className="apply-to-host-body-sub-title-responsive">Create an Experience</div>

          <div className="mb-4">
            <img src={connect} alt="Connect" />
            <div className="apply-to-host-body-text-responsive">
              Connect to local restaurants serving food with cultural identities
            </div>
          </div>

          <div className="mb-4">
            <img src={select} alt="Select" />
            <div className="apply-to-host-body-text-responsive">
              Select 3 items on restaurant menu representing the taste of the culture
            </div>
          </div>

          <div className="mb-4">
            <img src={entertainment} alt="Entertainment" />
            <div className="apply-to-host-body-text-responsive">
              Add Entertainment including music, arts and games to your experience
            </div>
          </div>

          <div className="apply-to-host-body-sub-title-responsive">Host the Experience</div>

          <div className="mb-4">
            <img src={group} alt="Group" />
            <div className="apply-to-host-body-text-responsive">
              Promote the experience to guests
            </div>
          </div>

          <div className="mb-4">
            <img src={building} alt="Building" />
            <div className="apply-to-host-body-text-responsive">Meet guests at the restaurant</div>
          </div>

          <div className="mb-4">
            <img src={money} alt="Money" />
            <div className="apply-to-host-body-text-responsive">Host the party</div>
          </div>

          <Link exact="true" to="/apply" className="apply-to-host-body-btn-responsive">
            Get Started
          </Link>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default ApplyToHost;
