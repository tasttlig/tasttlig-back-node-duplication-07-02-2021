// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AppContext } from '../../ContextProvider/AppProvider';

// Styling
import './EnterNewPassword.scss';
import logo from '../../assets/images/tasttlig-logo-black.png';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class EnterNewPassword extends Component {
  // Set initial state
  state = {
    load: false,
    enterNewPassword: '',
    enterNewPasswordType: 'password',
    submitAuthDisabled: false,
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // Load image helper function
  onLoad = () => {
    this.setState({ load: true });
  };

  // User input change helper function
  handleChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    let enterNewPasswordLessChar = '';
    let enterNewPasswordMinChar = '';

    /* When the user is entering new password, display error 
    message when there are less than 8 characters */
    if (nam === 'enterNewPassword' && val.length < 8) {
      enterNewPasswordLessChar = <span className="fas fa-times"> At least 8 characters.</span>;
    }

    /* When the user is entering new password, display approval 
    message when there are at least 8 characters */
    if (nam === 'enterNewPassword' && val.length >= 8) {
      enterNewPasswordMinChar = <span className="fas fa-check">At least 8 characters.</span>;
    }

    // Set at least 8 characters validation state
    if (enterNewPasswordLessChar || enterNewPasswordMinChar) {
      this.setState({
        enterNewPasswordLessChar,
        enterNewPasswordMinChar,
      });
    }

    this.setState({
      [nam]: val,
    });
  };

  // Show/Hide Enter New Password helper function
  handleClickEnterNewPassword = () =>
    this.setState(({ enterNewPasswordType: type }) => ({
      enterNewPasswordType: type === 'password' ? 'text' : 'password',
    }));

  // Validate user input for Enter New Password helper function
  validateEnterNewPassword = () => {
    let enterNewPasswordError = '';

    // Render enter new password error message
    if (!this.state.enterNewPassword) {
      enterNewPasswordError = 'Password is required.';
    } else if (this.state.enterNewPassword.length < 8) {
      enterNewPasswordError = 'Your password must be at least 8 characters. Please try again.';
    }

    // Set validation error state
    if (enterNewPasswordError) {
      this.setState({ enterNewPasswordError });
      return false;
    }

    return true;
  };

  // Submit New Password form helper function
  handleSubmitNewPassword = async (event) => {
    event.preventDefault();

    const isValid = this.validateEnterNewPassword();

    if (isValid) {
      const token = this.props.match.params.token;

      const url = `/user/update-password/${token}`;

      const headers = { Authorization: `Bearer ${token}` };

      const data = {
        email: this.props.match.params.email,
        password: this.state.enterNewPassword,
        tasttlig: true,
      };

      try {
        const response = await axios({ method: 'PUT', url, headers, data });

        if (response) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);

          toast(`Success! Your password has been updated!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            enterNewPasswordError: '',
            enterNewPasswordLessChar: '',
            enterNewPasswordMinChar: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Mount Enter New Password page
  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  // Render Enter New Password page
  render = () => {
    const {
      load,
      enterNewPassword,
      enterNewPasswordType,
      enterNewPasswordError,
      enterNewPasswordLessChar,
      enterNewPasswordMinChar,
      submitAuthDisabled,
    } = this.state;

    return (
      <div className="enter-new-password">
        <div>
          <Link to="/">
            <img
              src={logo}
              alt="Tasttlig"
              onLoad={this.onLoad}
              className={load ? 'enter-new-password-tasttlig-logo' : 'loading-image'}
            />
          </Link>
        </div>

        <form onSubmit={this.handleSubmitNewPassword} noValidate>
          <div className="mb-3">
            <input
              type={enterNewPasswordType}
              name="enterNewPassword"
              placeholder="Password"
              value={enterNewPassword}
              onChange={this.handleChange}
              disabled={submitAuthDisabled}
              className="password"
              required
            />
            <span className="password-icon" onClick={this.handleClickEnterNewPassword}>
              {enterNewPasswordType === 'password' ? (
                <span className="far fa-eye-slash"></span>
              ) : (
                <span className="far fa-eye"></span>
              )}
            </span>
            {enterNewPasswordError && <div className="error-message">{enterNewPasswordError}</div>}
            <div>
              <div className="password-less-char">{enterNewPasswordLessChar}</div>
              <div className="password-min-char">{enterNewPasswordMinChar}</div>
            </div>
          </div>

          <div className="mb-3">
            <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  };
}
