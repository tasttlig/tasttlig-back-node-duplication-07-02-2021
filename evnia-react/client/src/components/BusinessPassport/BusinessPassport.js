// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../ContextProvider/AppProvider';
import { useForm } from 'react-hook-form';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import Form from 'react-bootstrap/Form';
import { fileUploadAsync } from '../../functions/FileUpload';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import Nav from '../Navbar/Nav';
import Footer from '../Footer/Footer';
import GoTop from '../Shared/GoTop';
//components
import AsyncSelect from 'react-select/async';

// Styling
import './BusinessPassport.scss';
import BusinessPassportColumn1 from '../../assets/images/BusinessPassportColumn1.JPG';
import BusinessPassportColumn2 from '../../assets/images/BusinessPassportColumn3.JPG';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import BusinessPassport2 from './BusinessPassport2';
import { Col, Row } from 'react-bootstrap';

const BusinessPassport = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;
  const history = useHistory();
  // console.log(userRole);

  const userData = {
    businessName: '',
    businessStreetNumber: '',
    businessStreetName: '',
    maritalStatus: '',
    nationalities: [],
    location: '',
    zipcode: '',
    streetName: '',
    streetNumber: '',
    unit: '',
    businessPhoneNumber: '',
  };

  //intial state  

  var prevData;
  if (localStorage.getItem('businessPassport1Data')) {
    var businessPassport1Data = localStorage.getItem('businessPassport1Data');
    prevData = JSON.parse(businessPassport1Data);
  }

  const { update, handleSubmit } = useForm({ defaultValues: userData });
  const [nationalities, setNationalities] = useState(
    prevData && prevData['user_business_country'] ? prevData['user_business_country'] : '',
  );
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [image, setImage] = useState({ preview: '', raw: '' });
  const [businessName, setBusinessName] = useState(
    prevData && prevData['user_business_name'] ? prevData['user_business_name'] : '',
  );
  const [businessStreetNumber, setBusinessStreetNumber] = useState(
    prevData && prevData['user_business_street_number']
      ? prevData['user_business_street_number']
      : '',
  );
  const [businessStreetName, setBusinessStreetName] = useState(
    prevData && prevData['user_business_street_name'] ? prevData['user_business_street_name'] : '',
  );
  const [businessUnit, setBusinessUnit] = useState(
    prevData && prevData['user_business_unit'] ? prevData['user_business_unit'] : '',
  );
  const [businessCity, setBusinessCity] = useState(
    prevData && prevData['user_business_city'] ? prevData['user_business_city'] : '',
  );
  const [businessProvince, setBusinessProvince] = useState(
    prevData && prevData['user_business_province'] ? prevData['user_business_province'] : '',
  );
  const [businessPostalCode, setBusinessPostalCode] = useState(
    prevData && prevData['user_business_postal_code'] ? prevData['user_business_postal_code'] : '',
  );
  const [businessRegistered, setBusinessRegistered] = useState(
    prevData && prevData['user_business_registered'] ? prevData['user_business_registered'] : '',
  );
  const [businessPhoneNumber, setBusinessPhoneNumber] = useState(
    prevData && prevData['user_business_phone_number']
      ? prevData['user_business_phone_number']
      : '',
  );

  const businessPreference = localStorage.getItem('business-preference');
  console.log('business preference', businessPreference);

  const toggleNationality = (nationalities) => {
    console.log(nationalities['label']);
    setNationalities(nationalities['label']);
  };

  const handleChange = (e) => {
    setImage({
      preview: URL.createObjectURL(e.target.files[0]),
      raw: e.target.files[0],
    });
  };

  const handleUpload = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('image', image.raw);
    const config = { headers: { 'content-type': 'multipart/form-data' } };
    //await uploadToBackend('endpoint', {image: image.raw}, config)
  };

  const handleCheckRadio = (e) => {
    setBusinessRegistered(e);
  };

  const validateFields = () => {
    if (!businessName) {
      toast('Error! Restaurant Name cannot be null!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    if (!businessStreetName) {
      toast('Error! Restaurant Street Name cannot be null!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    if (!businessCity) {
      toast('Error! Restaurant City cannot be null!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    if (!businessProvince) {
      toast('Error! Restaurant Province cannot be null!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    if (!nationalities) {
      toast('Error! Restaurant Country cannot be null!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    // if (!businessPostalCode) {
    //   toast('Error! Business Postal Code cannot be null!', {
    //     type: 'error',
    //     autoClose: 2000,
    //   });
    //   return false;
    // }

    if (!businessPhoneNumber) {
      toast('Error! Please specify the phone number of your Restaurant!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    var regex = /^[0-9]+$/;
    if (!businessPhoneNumber.match(regex)) {
      toast('Error! Restaurant Phone Number must be a number!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    if (!businessStreetNumber.match(regex)) {
      toast('Error! Restaurant Street Number must be a number!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }
    

    if (!businessRegistered) {
      toast('Error! Restaurant regitration cannot be null!', {
        type: 'error',
        autoClose: 2000,
      });
      return false;
    }

    // if (!image) {
    //   toast('Error! Upload business logo!', {
    //     type: 'error',
    //     autoClose: 2000,
    //   });
    //   return false;
    // }
    // if nothing null return true
    return true;
  };

  const onSubmitProfileForm = async () => {
    // const url = `/business-passport`;
    if (validateFields()) {
      var result;
      try {
        if (image) {
          result = await fileUploadAsync(image.raw, 'dir');
        }
      } catch (error) {
        // toast('Error in Image Upload!', {
        //   type: 'error',
        //   autoClose: 2000,
        // });
      }

      const data = {
        user_business_logo: !result ? '' : result.url, //if result is not null, then give result url
        user_business_name: businessName,
        user_business_street_number: businessStreetNumber,
        user_business_street_name: businessStreetName,
        user_business_unit: businessUnit,
        user_business_country: nationalities,
        user_business_city: businessCity,
        user_business_province: businessProvince,
        user_business_postal_code: businessPostalCode,
        user_business_registered: businessRegistered,
        user_business_phone_number: businessPhoneNumber,
        user_business_preference: businessPreference,
      };
      console.log(data);
      localStorage.setItem('businessPassport1Data', JSON.stringify(data));
      props.history.push({
        pathname: '/business-passport2',
        //is_sponsor: props.location.is_sponsor,
        // data: data // your data array of objects
      });
    }
  };

  const onPostalCodeSelected = (value) => {
    // console.log(value)
    if (value.value.terms) {
      if (value.value.terms[0]) {
        console.log('here ', value.value.terms[0]);
        setBusinessCity(value.value.terms[0].value);
      }
      if (value.value.terms[1]) {
        setBusinessProvince(value.value.terms[1].value);
      }
      if (value.value.terms[2]) {
        setBusinessPostalCode(value.value.terms[2].value);
      }
      if (value.value.terms[3]) {
        setNationalities(value.value.terms[3].value);
      }
    }
  };

  // Mount Login page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  console.log(userRole);
  return (
    <Fragment>
      <Nav />
      <div className="">
        <div className="row">
          <div className="col-lg-1 col-xl-3 px-0 user-background-image1" />
          <div className="col-lg-8 col-xl-6 px-0 login-background">
            <div className="business-passport-content1">
              <row>
                <div className="tasttlig-image">
                  <Link exact="true" to="/">
                    <img src={tasttligLogoBlack} alt="Tasttlig" />
                  </Link>
                </div>
              </row>
              <span>Restaurant Application</span>&nbsp;
            </div>

            <div className="business-passport-content2">
              <span>
                Thank You for your interest in vending! Let's start by adding your restaurant to
                Tasttlig
              </span>
              &nbsp;
            </div>

            <form onSubmit={handleSubmit(onSubmitProfileForm)} noValidate>
              <div className="row">
                <label htmlFor="upload-button">
                  {image.preview ? (
                    <img src={image.preview} alt="dummy" width="100" height="100" />
                  ) : (
                    <>
                      <span className="fa-stack fa-2x mt-3 mb-2">
                        <i className="fas fa-circle fa-stack-2x" />
                        <i className="fas fa-store fa-stack-1x fa-inverse" />
                      </span>
                      <h5 className="text-center">Upload your photo</h5>
                    </>
                  )}
                </label>
                <input
                  type="file"
                  id="upload-button"
                  style={{ display: 'none' }}
                  onChange={handleChange}
                />
                <br />
                {/* <button onClick={handleUpload}></button> */}
              </div>

              <Form.Row>
                <Form.Group as={Col} controlId="formGridBusinessName">
                  <Form.Control
                    className="formGridStyle"
                    type="email"
                    placeholder="Restaurant Name"
                    value={businessName}
                    onChange={(e) => setBusinessName(e.target.value)}
                  />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridBusinessPhoneNUmber">
                  <Form.Control
                    className="formGridStyle"
                    type="text"
                    placeholder="Restaurant Phone Number"
                    value={businessPhoneNumber}
                    onChange={(e) => setBusinessPhoneNumber(e.target.value)}
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridBusinessUnit">
                  <Form.Control
                    className="formGridStyle"
                    type="text"
                    placeholder="Restaurant Unit/Suite/Floor"
                    value={businessUnit}
                    onChange={(e) => setBusinessUnit(e.target.value)}
                  />
                </Form.Group>
                <Form.Group as={Col} controlId="formBusinessStreetNumber">
                  <Form.Control
                    className="formGridStyle"
                    type="text"
                    placeholder="Restaurant Street Number"
                    value={businessStreetNumber}
                    onChange={(e) => setBusinessStreetNumber(e.target.value)}
                  />
                </Form.Group>
                <Form.Group as={Col} controlId="formBusinessStreetName">
                  <Form.Control
                    className="formGridStyle"
                    type="email"
                    placeholder="Restaurant Street Name"
                    value={businessStreetName}
                    onChange={(e) => setBusinessStreetName(e.target.value)}
                  />
                </Form.Group>
              </Form.Row>

              {/* <div className="row">
            <div className="business-passport-input-style">
              <input
                type="text"
                name="businessName"
                placeholder="Business Name"
                value={businessName}
                onChange={(e) => setBusinessName(e.target.value)}
                disabled={authModalContext.state.submitAuthDisabled}
                className="email-2"
                required
              />
            </div>
            <div className="business-passport-input-style">
              <input
                type="text"
                name="businessStreetNumber"
                placeholder="Business Street Number"
                value={businessStreetNumber}
                onChange={(e) => setBusinessStreetNumber(e.target.value)}
                disabled={authModalContext.state.submitAuthDisabled}
                className="email-2"
                required
              />
            </div>
          </div> */}
              {/* <div className="row">
            <div className="business-passport-input-style">
              <input
                type="text"
                name="businessStreetName"
                placeholder="Business Street Name"
                value={businessStreetName}
                onChange={(e) => setBusinessStreetName(e.target.value)}
                disabled={authModalContext.state.submitAuthDisabled}
                className="email-2"
                required
              />
            </div>
            <div className="business-passport-input-style">
              <input
                type="text"
                name="businessUnit"
                placeholder="Business Unit/Suite/Floor"
                value={businessUnit}
                onChange={(e) => setBusinessUnit(e.target.value)}
                disabled={authModalContext.state.submitAuthDisabled}
                className="email-2"
                required
              />
            </div>
          </div> */}
              <Form.Row>
                <Form.Group as={Col} className="mapsStyle">
                  {/* <Form.Control className="formGridStyle" type="text" placeholder="Business Postal Code" */}
                  {/* <Form.Label> Type Postal Code: </Form.Label> */}
                  <GooglePlacesAutocomplete
                    className="formGridStyle"
                    placeholder="Type your Restaurant Postal Code"
                    styles={{
                      textInputContainer: {
                        backgroundColor: '#fff0',
                        borderWidth: 1,
                        borderRadius: 3,
                        padding: 0,
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        width: '50%',
                      },
                      textInput: {
                        color: '#000',
                        padding: 0,
                        margin: 0,
                      },
                    }}
                    selectProps={{
                      styles: {
                        // input: (provided) => ({
                        //   ...provided,
                        //   color: 'blue',
                        // }),
                        // option: (provided) => ({
                        //   ...provided,
                        //   color: 'blue',
                        // }),
                        dropdownIndicator: (provided) => ({
                          ...provided,
                          visibility: 'hidden',
                        }),
                      },
                      // placeholder: 'Type your postal code',
                      // businessPostalCode,
                      placeholder: businessPostalCode
                        ? businessPostalCode
                        : 'Type in your Restaurant Postal Code',
                      onChange: onPostalCodeSelected,
                      value: 'location',
                    }}
                  ></GooglePlacesAutocomplete>
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridBusinessCity">
                  <Form.Control
                    className="formGridStyle"
                    type="text"
                    placeholder="Restaurant City"
                    value={businessCity}
                    onChange={(e) => setBusinessCity(e.target.value)}
                  />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridBusinessProvince">
                  <Form.Control
                    className="formGridStyle"
                    type="text"
                    placeholder="Restaurant Province"
                    value={businessProvince}
                    onChange={(e) => setBusinessProvince(e.target.value)}
                  />
                </Form.Group>

                {/* <Form.Group as={Col} controlId="formGridBusinessZipCode">
                        <Form.Control className="formGridStyle" type="text" placeholder="Business Zip/Postal Code" 
                        value={businessPostalCode} onChange={(e) => setBusinessPostalCode(e.target.value)}/>
                        </Form.Group> */}

                <Form.Group as={Col} controlId="formGridBusinessCountry">
                  <Form.Control
                    className="formGridStyle"
                    type="text"
                    placeholder="Restaurant Country"
                    value={nationalities}
                    onChange={(e) => setNationalities(e.target.value)}
                  />
                </Form.Group>
              </Form.Row>

              {/* <div className="row">
            <div className="business-passport-input-style-2">
              <select
                className="text-border"
                onChange={(e) => setBusinessCity(e.target.value)}
              >
                <option selected disabled>
                  {" "}
                  Business City
                </option>
                <option value="Other">Other</option>
                <option value="Toronto">Toronto</option>
                <option value="Montreal">Montreal</option>
                <option value="Vancouver">Vancouver</option>
                <option value="Ottawa">Ottawa</option>
                <option value="Calgary">Calgary</option>
                <option value="Ottawa">Ottawa</option>
                <option value="Edmonton">Edmonton</option>
                <option value="Québec City">Québec City</option>
                <option value="Winnipeg">Winnipeg</option>
                <option value="Hamilton">Hamilton</option>
                <option value="Kitchener">Kitchener</option>
                <option value="London">London</option>
                <option value="St. Catharines">St. Catharines</option>
                <option value="Halifax">Halifax</option>
                <option value="Oshawa">Oshawa</option>
                <option value="Victoria">Victoria</option>
                <option value="Windsor">Windsor</option>
                <option value="Saskatoon">Saskatoon</option>
                <option value="Regina">Regina</option>
                <option value="Saskatoon">Saskatoon</option>
                <option value="Sherbrooke">Sherbrooke</option>
                <option value="Barrie">Barrie</option>
                <option value="Kelowna">Kelowna</option>
                <option value="Kingston">Kingston</option>
                <option value="Guelph">Guelph</option>
                <option value="Peterborough">Peterborough</option>
                value = {businessCity}
                disabled={authModalContext.state.submitAuthDisabled}
              </select>
            </div>
            <div className="business-passport-input-style-3">
              <select
                className="text-border"
                onChange={(e) => setBusinessProvince(e.target.value)}
              >
                <option selected disabled>
                  {" "}
                  Business Province
                </option>
                <option value="Other">Other</option>
                <option value="Alberta">Alberta</option>
                <option value="British Columbia">British Columbia</option>
                <option value="Manitoba">Manitoba</option>
                <option value="New Brunswick">New Brunswick</option>
                <option value="Newfoundland">Newfoundland</option>
                <option value="Nova Scotia">Nova Scotia</option>
                <option value="Ontario">Ontario</option>
                <option value="Prince Edward Island">
                  Prince Edward Island
                </option>
                <option value="Quebec">Quebec</option>
                <option value="Saskatchewan">Saskatchewan</option>
                <option value="Yukon">Yukon</option>
                <option value="Nunavat">Nunavat</option>
                <option value="Northwest Territories">
                  Northwest Territories
                </option>
                value = {businessProvince}
                disabled={authModalContext.state.submitAuthDisabled}
              </select>
            </div>
          </div>
          <div className="row">
            <div className="business-passport-input-style-2">
              <NationalitySelector
                toggleNationality={toggleNationality}
                selectedItem={nationalities}
                setSelectedItem={setNationalities}
              />
                  <div input-style-3>
                        <CountryDropdown  value={nationalities} className="custom-select1 festivals-filter-location"
                                    
                        onChange={(e) => setNationalities(e)} /> 
                  </div>
            </div>
            <div className="business-passport-input-style-3">
              <input
                type="text"
                name="businessPostalCode"
                placeholder="Business Postal/Zip Code"
                value={businessPostalCode}
                onChange={(e) => setBusinessPostalCode(e.target.value)}
                disabled={authModalContext.state.submitAuthDisabled}
                className="email-2"
                required
              />
            </div>
          </div> */}
              <div className="row">
                <div className="business-registered-title">Is your restaurant registered?</div>
              </div>
              <div className="row" onChange={(e) => handleCheckRadio(e.target.value)}>
                <div className="radio-style">
                  <input type="radio" value="Yes" name="registration" /> Yes
                </div>
                <div className="radio-style">
                  <input type="radio" value="No" name="registration" /> No
                </div>
              </div>

              <div className="submit-button-style">
                <button
                  type="submit"
                  disabled={authModalContext.state.submitAuthDisabled}
                  className="log-in-btn"
                >
                  Next
                </button>
              </div>
            </form>
          </div>

          <div className="col-xl-3 px-0 user-background-image2" />
        </div>
      </div>
      <Footer />

      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </Fragment>
  );
};

export default BusinessPassport;
