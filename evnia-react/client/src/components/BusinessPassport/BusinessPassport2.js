import React, { useEffect, useContext, useState, Fragment  } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../ContextProvider/AppProvider';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import { fileUploadAsync } from '../../functions/FileUpload';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import Nav from '../Navbar/Nav';
import Footer from '../Footer/Footer';
import GoTop from '../Shared/GoTop';
//styling
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import thumbsUpLogo from '../../assets/images/tick.png';

import { toast } from 'react-toastify';

//components

import { DateInput } from '../EasyForm';
import { Modal, Button, Row, Col } from 'react-bootstrap';

const BusinessPassport2 = (props) => {
  const userData = {
    is_business: true,
    businessRetail: '',
    businessRegisteredLocation: '',
    businessType: '',
    businessFoodType: '',
    businessFoodHandling: '',
    //startDate: "",
  };

  //intial state
  const { update, handleSubmit } = useForm({ defaultValues: userData });
  const [businessRetail, setBusinessRetail] = useState();
  const [image, setImage] = useState({ preview: '', raw: '' });
  const [businessType, setBusinessType] = useState();
  const [businessFoodType, setBusinessFoodType] = useState();
  const [businessRegisteredLocation, setBusinessRegisteredLocation] = useState();
  const [businessFoodHandling, setBusinessFoodHandling] = useState();
  const [businessNumber, setBusinessNumber] = useState();
  const [isDropdownDisabled, setIsDropDownDisabled] = useState(true);
  const [startDate, setStartDate] = useState(new Date());
  const [showModal, setShowModal] = useState(false);
  const [defaultBusinessTypeValue, setdefaultBusinessTypeValue] = useState('Business Type');
  const [defaultFoodTypeValue, setdefaultFoodTypeValue] = useState('Type of Food Business');
  const [subscriptionResponse, setSubscriptionResponse] = useState([]);
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  const businessPreference = localStorage.getItem('business-preference');
  const applicationType = localStorage.getItem('application-type');


  const handleChange = (e) => {
    setImage({
      preview: URL.createObjectURL(e.target.files[0]),
      raw: e.target.files[0],
    });
  };
  // console.log("user data from the form", userData)

  const handleUpload = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('image', image.raw);
    const config = { headers: { 'content-type': 'multipart/form-data' } };
    //await uploadToBackend('endpoint', {image: image.raw}, config)
  };

  const setBusinessTypeValue = (e) => {
    // console.log("e from here man", typeof e)
    setBusinessType(e);
    if (e === 'Food') {
      setIsDropDownDisabled(false);
    } else {
      setIsDropDownDisabled(true);
    }
  };
  // setBusinessTypeValue('Food')

  const handleCheckRadio = (e) => {
    setBusinessRetail(e);
  };
  const hostFromLocalStorage = localStorage.getItem('dataFromFestival')

  const validateFields = () => {
    if (!businessRetail) {
      toast('Error! Please specify if business has a storefront or not!', {
        type: 'error',
        autoClose: 4000,
      });
      return false;
    }

    // if (!businessRegisteredLocation) {
    //   toast('Error! Location of business registeration cannot be null!', {
    //     type: 'error',
    //     autoClose: 2000,
    //   });
    //   return false;
    // }

    
    // if (businessPreference=='Host'&&businessFoodType=='Restaurant') {
    //   if(!businessNumber){
    //   toast('You have to submit your CRA number to be able to host!', {
    //     type: 'error',
    //     autoClose: 2000,
    //   });
    //   return false;
    // }
    // }

    if (!businessType) {
      toast('Error! Business Type cannot be null!', {
        type: 'error',
        autoClose: 4000,
      });
      return false;
    }

    return true;
  };
  // console.log("businessType", businessType);
  // console.log("businessFoodType", businessFoodType);

  const onSubmitProfileForm = async () => {
    if (validateFields()) {
      var result;

      try {
        result = await fileUploadAsync(image.raw, 'dir');
      } catch (error) {}

      const url = `/business-passport`;
      var businessPassport1Data = localStorage.getItem('businessPassport1Data');
      var data = JSON.parse(businessPassport1Data);
      data['is_business'] = true;
      data['user_business_food_handling'] = !result ? '' : result.url;
      data['user_business_retail'] = businessRetail;
      data['user_business_registered_location'] = businessRegisteredLocation;
      data['user_business_type'] = businessType;
      data['user_business_number'] = businessNumber;
      data['user_id'] = appContext.state.user.id;
      data['start_date'] = startDate;
      data['member_status'] = 'Pending';
      data['subscriptionResponse'] = subscriptionResponse
      if (businessType == 'Food') {
        data['user_business_food_type'] = businessFoodType;
      } else {
        data['user_business_food_type'] = '';
      }

      const acc_token = await localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };
      var response;

      try {
        response = await axios({ method: 'POST', url, headers, data });

        setTimeout(1000);
        if (response && response.data && response.data.success) {

          toast('Success! Your details have been sent to the admin to review!', {
            type: 'success',
            autoClose: 4000,
          });
           
          console.log('Application Type', applicationType);
          
          if (applicationType === "HostApplication")
               {
                window.location.href = '/become-a-host';
               }
          else{
                    setTimeout(async() => {
                      await appContext.getCurrentUserData();
                      setShowModal(true);
                    }, 7000);
               }
          // localStorage.setItem('business-preference', 'GUEST');
          // localStorage.removeItem('businessPassport1Data');
          localStorage.removeItem('business-preference');
          
          
         
          // if (localStorage.getItem('dataFromFestival')) {
          //   // setTimeout(() => {
          //   //   props.history.push({ pathname: '/dashboard' });
          //   // }, 4000);
          //   setShowModal(true);
          // } else {
          //   setShowModal(true);
          // }
        } else {
          toast(response.data.message, {
            type: 'error',
            autoClose: 4000,
          });
        }
      } catch (error) {
        toast('Error! Something went wrong!'.concat(error.message), {
          type: 'error',
          autoClose: 7000,
        });
      }
    }
  };

  const fetchSubscriptions = async () => {
    const response = await axios({
      method: 'GET',
      url: `/subscription/details`,
      params: {
        item_type: 'package',
        item_id: 'H_BASIC'
      },
    });
    setSubscriptionResponse(response.data.item);
    return response;
  };

  // console.log("subscription reponse coming from business passport2: ", subscriptionResponse)


  const setLocation = (value) => {
    if (value && value.label) {
      setBusinessRegisteredLocation(value.label);
    }
  };

  useEffect(() => {
   if(hostFromLocalStorage === "Host") {
    setBusinessTypeValue('Food')
    setBusinessFoodType('Restaurant')
    setdefaultFoodTypeValue('Restaurant')
    setdefaultBusinessTypeValue('Food')
   }
   fetchSubscriptions(); 
  }, []);


  return (

    <Fragment>
    <Nav/>
    <div className="row">
      <div className="col-lg-1 col-xl-3 px-0 user-background-image1" />
      <div className="col-lg-8 col-xl-6 px-0 login-background">
        <div className="business-passport-content1">
          <row>
            <div className="tasttlig-image">
              <Link exact="true" to="/">
                <img src={tasttligLogoBlack} alt="Tasttlig" />
              </Link>
            </div>
          </row>
        </div>
        {showModal === false ? ( 
          <container>
          <form onSubmit={handleSubmit(onSubmitProfileForm)} noValidate>
            <div className="row">
              <div className="business-registered-title">Do you have a store front?</div>
            </div>
            <div className="row" onChange={(e) => handleCheckRadio(e.target.value)}>
              <div className="radio-style">
                <input type="radio" value="Yes" name="registration" /> Yes
              </div>
              <div className="radio-style">
                <input type="radio" value="No" name="registration" /> No
              </div>
              {/* value={businessRegistered}                         */}
            </div>
            <div className="business-registered-title">
              Which city is your restaurant registered in?
            </div>
            <GooglePlacesAutocomplete
              placeholder=" Type the City"
              styles={{
                textInputContainer: {
                  backgroundColor: '#fff0',
                  borderWidth: 1,
                  borderRadius: 3,
                  padding: 0,
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  width: '100%',
                },
                textInput: {
                  color: '#000',
                  padding: 0,
                  margin: 0,
                },
              }}
              
              selectProps={{
                styles: {
                  // input: (provided) => ({
                  //   ...provided,
                  //   color: 'blue',
                  // }),
                  // option: (provided) => ({
                  //   ...provided,
                  //   color: 'blue',
                  // }),
                  dropdownIndicator: (provided) => ({
                    ...provided,
                    visibility: 'hidden',
                  }),
                },
                placeholder: businessRegisteredLocation
                  ? businessRegisteredLocation
                  : 'Type in your city',
                businessRegisteredLocation,
                onChange: setLocation,
                value: 'businessRegisteredLocation',
              }}
            >
              {' '}
            </GooglePlacesAutocomplete>
            
            <div className="business-passport-input-style">
              <div className="business-registered-title">What type of business is it?</div>
              <select
                className="custom-select1"
                onChange={(e) => setBusinessTypeValue(e.target.value)}
                defaultValue={defaultBusinessTypeValue}
              >
               {/* {localStorage.getItem('dataFromFestival') ? (setIsDropDownDisabled(false)):(  */}
               {/* <option selected disabled>
                  {' '}
                  Business Type
                </option> */}
                 {/* )} */}
                 {hostFromLocalStorage === "Host" ? (
                   <option value="Food" >Food</option>
                 ):  (
                   <>
                  <option value="" >--Select--</option>
                  <option value="Food" >Food</option>
                  </>
                 ) 
                 }
                
                <option value="Non-Profit">Non-Profit</option>
                <option value="Government">Government</option>
                <option value="Other Business">Other Business</option>
                value = {businessType}
                disabled={authModalContext.state.submitAuthDisabled}
              </select>
            </div>
            { businessType && businessType == 'Food' ? (
              <div>
                      <div className="business-passport-input-style">
              <div className="business-registered-title">What type of food business is it?</div>
                    <select
                      className="custom-select1"
                      disabled={isDropdownDisabled}
                      onChange={(e) => setBusinessFoodType(e.target.value)}
                      defaultValue={defaultFoodTypeValue}
                    >
                      {/* <option selected disabled>
                        {' '}
                        Type of Food Business
                      </option> */}
                        {hostFromLocalStorage === "Host" ? (
                            <option value="Restaurant">Restaurant</option>
                        ): (
                          <>
                          <option value="" >--Select--</option>
                          <option value="Restaurant">Restaurant</option>
                          </>
                        )
                        }
               
                      <option value="Food-Truck">Food-Truck</option>
                      <option value="Catering">Catering</option>
                      <option value="Chef">Chef</option>
                      <option value="Chef">Independant Home Based Food Business</option>
                    </select>
            </div>
              </div>
            ):  null 
            }
            

            <div className="business-passport-input-style">
              <div className="business-registered-title">If applicable, when was your restaurant registered?</div>
              <DatePicker
                className="custom-select1"
                selected={startDate}
                onChange={(date) => setStartDate(date)}
                maxDate={new Date()}
                showMonthDropdown
                showYearDropdown
                yearDropdownItemNumber={50}
                scrollableYearDropdown
                />
            </div>
            <div className="business-passport-input-style">
            <div className="business-registered-title">What is your CRA Business Number?</div>
              <input
                type="text"
                name="businessNumber"
                placeholder="Enter Your CRA Business Number"
                value={businessNumber}
                onChange={(e) => setBusinessNumber(e.target.value)}
                disabled={authModalContext.state.submitAuthDisabled}
                className="CRA-number"
                required
              />
            </div>
            { businessType && businessType == 'Food' ? ( <div className="row">
              <label htmlFor="upload-button">
                {image.preview ? (
                  <img src={image.preview} alt="dummy" width="100" height="100" />
                ) : (
                  <>
                    <span className="fa-stack fa-2x mt-3 mb-2">
                      <i className="fas fa-circle fa-stack-2x" />
                      <i className="fas fa-store fa-stack-1x fa-inverse" />
                    </span>
                    <h5 className="text-center">Upload your food handlers certificate</h5>
                  </>
                )}
              </label>
              <input
                type="file"
                id="upload-button"
                style={{ display: 'none' }}
                onChange={handleChange}
              />
              <br />
              {/* <button onClick={handleUpload}></button> */}
            </div>): null}
            
            
              <row>
              <div className="submit-button-style">
              <button
                type="submit"
                disabled={authModalContext.state.submitAuthDisabled}
                className="log-in-btn"
              >
                Complete Restaurant Application
              </button>
              </div>
              </row>
            
          </form>

              <div className="submit-button-back">
              <button 
                type="button"
                disabled={authModalContext.state.submitAuthDisabled}
                className="log-in-btn"
                onClick={() => {
                  props.history.push({
                    pathname: '/business-passport',
                  });
                }}
                
              >
                Back
              </button> 
              </div>
          </container>
              
        ) : (
          <div>
            
             {/* {hostFromLocalStorage ? (
                <Modal show={true}>
                <Modal.Header modal-header className="modal-header">
                   <div className="modal-div">
                     <img className="modal-img1" src={tasttligLogoBlack} alt="Tasttlig" />
                   </div>
                   <div className="modal-div-2">
                     <img className="modal-img" src={thumbsUpLogo} alt="Tasttlig" />
                   </div>
                   <div className="modal-div-3">
                    <h3>
                    Congratulations You have applied for Business Passport!
                    </h3> 
                    <p>We are looking forward to working with you. We will notify you to begin posting to our festival as soon as your business application is verified.</p>
                   </div>
                 </Modal.Header>
                 <Modal.Footer className="modal-footer">
                   <button
                     type="button"
                     className="modal-button"
                     onClick={() => {
                       props.history.push({
                         pathname: '/dashboard',
                       });
                     }}
                   >
                     Go To Dashboard{' '}
                   </button>
                   <button
                     type="button"
                     className="modal-button1"
                     onClick={() => {
                       props.history.push({
                         pathname: '/',
                       });
                     }}
                   >
                     Explore Festivals{' '}
                   </button>
                 </Modal.Footer>
               </Modal>
             ):(  */}
               <Modal show={true}>
             <Modal.Header modal-header className="modal-header">
                {/* <div className="modal-div">
                  <img className="modal-img1" src={tasttligLogoBlack} alt="Tasttlig" />
                </div> */}
                <div className="modal-div-2">
                  <img className="modal-img" src={thumbsUpLogo} alt="Tasttlig" />
                </div>
                { businessPreference === "Vend" ? 
                    <div className="modal-div-3">
                      Thank you for applying to Vend at Tasttlig. 
                      We will review your application to vend and get back to you as soon as possible.
                    </div>
                    : businessPreference === "Host" ? 
                    <div className="modal-div-3">
                      Thank you for applying to Host at Tasttlig. 
                      We will review your application to host and get back to you as soon as possible.
                    </div>
                    : <div className="modal-div-3">
                    Thank you for adding your Restaurant with Tasttlig, we will get back to you as
                    soon as possible
                  </div>
                }
                
              </Modal.Header>
              {localStorage.getItem('VendFromSignOut') ?
                   <Modal.Footer className="modal-footer">
                      <Link exact = 'true' to = '/payment/package/V_MIN'    onClick={() => {
                          localStorage.removeItem('businessPassport1Data');
                          localStorage.removeItem('business-preference');
                      }}>
                      <button
                        type="button"
                        className="modal-button1"
                      >
                        Complete your Payment{' '}
                      </button>
                      </Link>
                   </Modal.Footer>
                    : <Modal.Footer className="modal-footer">
                        <Link exact = 'true' to = '/dashboard'    onClick={() => {
                            localStorage.removeItem('businessPassport1Data');
                            localStorage.removeItem('business-preference');
                        }}>
                        <button
                          type="button"
                          className="modal-button1"
                        >
                          Go to your Restaurant Dashboard{' '}
                        </button>
                        </Link>
                      </Modal.Footer>}
         
            </Modal>
              {/* )} */}
          </div>
        )}
      </div>
      <div className="col-xl-3 px-0 user-background-image2" />
    </div>
    <Footer />

  <GoTop scrollStepInPx="50" delayInMs="16.66" />
  </Fragment>
  );
};

export default BusinessPassport2;
