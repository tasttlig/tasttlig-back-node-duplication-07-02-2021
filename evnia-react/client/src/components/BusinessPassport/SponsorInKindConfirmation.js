// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Styling
import './SponsorInKindConfirmation.scss';

const SponsorInKindConfirmation = (props) => {
  return (
    <div
      className="business-modal row justify-content-md-center 
        align-items-center text-center"
    >
      <div
        className="col-4 bg-danger text-center
        border border-bottom-0 border-danger rounded-top rounded py-5 text-white"
      >
        <div className="float-left">
          <img
            src={require('./../../assets/images/tasttlig-logo-black.png')}
            className="tasttlig-logo"
            alt="Tasttlig"
          />
        </div>
        Thank you for your application to sponsor Tasttlig! Your application will be reviewed
      </div>
      <div class="w-100 d-none d-md-block"></div>
      <div
        className="col-4 bg-white border border-top-0 
        border-danger py-5"
      >
        <div className="px-5 btn btn-danger rounded-pill">
          <Link
            exact="true"
            to={{
              pathname: '/dashboard',
            }}
            className="text-white"
          >
            Goto Dashboard
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SponsorInKindConfirmation;
