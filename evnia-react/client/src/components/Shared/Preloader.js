// Libraries
import React, { Component } from 'react';

export default class Preloader extends Component {
  render = () => {
    return (
      <div className="preloader">
        <div className="loader"></div>
      </div>
    );
  };
}
