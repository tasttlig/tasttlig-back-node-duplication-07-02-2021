import React from 'react';
import { scroller } from 'react-scroll';

const GoTop = (props) => {
  return (
    <button
      className={props.visible ? 'show-scroll-to-top' : 'hide-scroll-to-top'}
      onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}
    >
      <i className="fas fa-chevron-up"></i>
    </button>
  );
};

export default GoTop;
