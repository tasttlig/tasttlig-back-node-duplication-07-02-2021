// Libraries
import React, { useState, useContext } from 'react';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import './AllergiesRow.scss';

toast.configure();

const AllergiesRow = (props) => {
  // Render Food Type Table Row
  return (
    <tr>
      <td>{props.allergies}</td>
    </tr>
  );
};

export default AllergiesRow;
