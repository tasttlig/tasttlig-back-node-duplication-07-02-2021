// Libraries
import React, { useState, useContext } from 'react';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import './FoodTypeRow.scss';

toast.configure();

const FoodTypeRow = (props) => {
  // Render Food Type Table Row
  return (
    <tr>
      <td>{props.foodType}</td>
    </tr>
  );
};

export default FoodTypeRow;
