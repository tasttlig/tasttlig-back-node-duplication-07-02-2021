// Libraries
import React, { useState, useContext } from 'react';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import './CuisineRow.scss';

toast.configure();

const CuisineRow = (props) => {
  // Render Cuisine Table Row
  return (
    <tr>
      <td>{props.cuisine}</td>
    </tr>
  );
};

export default CuisineRow;
