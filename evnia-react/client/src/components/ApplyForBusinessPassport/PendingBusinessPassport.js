// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../ContextProvider/AppProvider';
import { Link } from 'react-router-dom';

// Styling
import './ApplyForBusinessPassport.scss';

const PendingBusinessPassport = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  return (
    <div className="container">
      <div className="row apply-for-business-passport">
        <h1>Your Business Passport application is currently pending.</h1>
      </div>
    </div>
  );
};

export default PendingBusinessPassport;
