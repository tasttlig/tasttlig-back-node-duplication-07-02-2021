import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../../ContextProvider/AppProvider';
import axios from 'axios';
import { DateInput, Form, MultiImageInput } from '../../EasyForm';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer'

import './Subscriptions.scss';
import bulletPointIcon from '../../../assets/images/bullet-point-icon.jpg';

toast.configure();

const Subscriptions = (props) => {
  const history = useHistory();
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  let role = userRole && userRole.includes('vendor') ? 'vendor' : 'guest';
  const [userSubscription, setUserSubscription] = useState(role);
  const [userSubscriptions, setUserSubscriptions] = useState([]);

  const membershipPaymentFirst = async () => {
    await history.push({
      pathname: `/payment/package/G_MSHIP2`,
      /* title: props.title,
      festivalId: props.festivalId, */
    });
  };

  const membershipPaymentSecond = async () => {
    await history.push({
      pathname: `/payment/package/KG_MSHIP2`,
      /* title: props.title,
      festivalId: props.festivalId, */
    });
  };

  const membershipPaymentThird = async () => {
    await history.push({
      pathname: `/payment/package/G_MSHIP3`,
      /* title: props.title,
      festivalId: props.festivalId, */
    });
  };

  const membershipPaymentFourth = async () => {
    await history.push({
      pathname: `/payment/package/KG_MSHIP3`,
      /* title: props.title,
      festivalId: props.festivalId, */
    });
  };

  const vendingHostPaymentFirst = async () => {
    await history.push({
      pathname: `/payment/package/V_MOD`,
    });
  };

  const vendingHostPaymentSecond = async () => {
    await history.push({
      pathname: `/payment/package/KV_MOD`,
    });
  };

  const vendingHostPaymentThird = async () => {
    await history.push({
      pathname: `/payment/package/V_ULTRA`,
    });
  };

  const vendingHostPaymentFourth = async () => {
    await history.push({
      pathname: `/payment/package/KV_ULTRA`,
    });
  };

  const getFreeGuest = async () => {
    await history.push({
      pathname: `/payment/package/G_BASIC`,
    });
  };
  const getFreeHost = async () => {
    await history.push({
      pathname: `/payment/package/H_BASIC`,
    });
  };
  const getAmbassadorGuest = async () => {
    await history.push({
      pathname: '/guest-ambassador-form',
      //pathname: `/payment/package/G_AMB`,
    });
  };
  // const getAmbassadorHost = async () => {
  //   await history.push({
  //     pathname: `/payment/package/H_AMB`,
  //   });
  // };

  // const handlePayment = () => {
  //   if (userSubscription === 'host') {
  //     vendingHostPayment();
  //   } else {
  //     membershipPayment();
  //   }
  // };
  const hasSubscriptionAndValid = (subCode) => {
    let result = false;
    userSubscriptions.length > 0 &&
      userSubscriptions.map((sub) => {
        if (sub.subscription_code === subCode) {
          if (sub.user_subscription_status === 'ACTIVE') {
            result = true;
          }
        }
      });
    if (result) {
      toast(`You already have a valid subscription for this package!`, {
        type: 'info',
        autoClose: 2000,
      });
    }
    return result;
  };

  const handlePaymentFirstPackage = () => {
    if (userSubscription === 'vendor') {
      if (hasSubscriptionAndValid('V_MOD')) {
        return;
      }
      vendingHostPaymentFirst();
    } else {
      if (hasSubscriptionAndValid('G_MSHIP2')) {
        return;
      }
      membershipPaymentFirst();
    }
  };
  const handlePaymentSecondPackage = () => {
    if (userSubscription === 'vendor') {
      if (hasSubscriptionAndValid('KV_MOD')) {
        return;
      }
      vendingHostPaymentSecond();
    } else {
      if (hasSubscriptionAndValid('KG_MSHIP2')) {
        return;
      }
      membershipPaymentSecond();
    }
  };
  const handlePaymentThirdPackage = () => {
    if (userSubscription === 'vendor') {
      if (hasSubscriptionAndValid('V_ULTRA')) {
        return;
      }
      vendingHostPaymentThird();
    } else {
      if (hasSubscriptionAndValid('G_MSHIP3')) {
        return;
      }
      membershipPaymentThird();
    }
  };

  const handlePaymentFourthPackage = () => {
    if (userSubscription === 'vendor') {
      if (hasSubscriptionAndValid('KV_ULTRA')) {
        return;
      }
      vendingHostPaymentFourth();
    } else {
      if (hasSubscriptionAndValid('KG_MSHIP3')) {
        return;
      }
      membershipPaymentFourth();
    }
  };

  const handleFree = () => {
    if (userSubscription === 'vendor') {
      getFreeHost();
    } else {
      getFreeGuest();
    }
  };
  // const handleAmbassador = () => {
  //   if (userSubscription === 'host') {
  //     getAmbassadorHost();
  //   } else {
  //     getAmbassadorGuest();
  //   }
  // };

  const getUserSubscriptions = async () => {
    const response = await axios({
      method: 'GET',
      url: `/valid-user-subscriptions/${appContext.state.user.id}`,
    });
    console.log('r u', response.data.user);
    setUserSubscriptions(response.data.user);
    //return response;
  };

  useEffect(() => {
    getUserSubscriptions();
  }, []);

  return (
    <>
      <Nav />
      <div className="nav-space">
        <div className="pricing-image bg-image"></div>
        <div className=" bg-color">
          <div className="py-5 text-center">
            <span
              className={userSubscription === 'vendor' ? 'tab active-tab' : 'tab'}
              onClick={() => {
                // if (
                //   (userRole && userRole.includes('HOST')) ||
                //   userRole.includes('BUSINESS_MEMBER_PENDING') ||
                //   userRole.includes('BUSINESS_MEMBER')
                // ) {
                  setUserSubscription('vendor');
                // }
              }}
            >
              Vending Plans
            </span>
            <span
              className={userSubscription === 'guest' ? 'tab active-tab' : 'tab'}
              onClick={() => {
                setUserSubscription('guest');
              }}
            >
              Guest Plans
            </span>
          </div>

          <div className="row text-justify">
            {/* <div className="col-3 card p-2 mx-auto mb-5"> */}
            
              {/* {userSubscription === 'host' ? (
                <div>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Attend Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Free Food Tasting.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Put all your Business contents for 30 promotions.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    25% Commission.
                  </p>
                </div>
              ) : (
                <div>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Attend Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    30 Days Renewal.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                   1 Festival.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                   15% Discount During Festivals.
                  </p>
                </div>
              )} */}

              {/* <div className="pt-5">
                <div className="tab active-tab absolute-div text-center">Default Free</div>
              </div> */}
            {/* </div> */}

            <div className="col-3 card p-2 mx-auto mb-5">
              {userSubscription === 'vendor' ? (
                <div>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Attend Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Free Food Tasting.
                  </p>
                  {/* <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Put all your Business contents for 30 promotions.
                  </p> */}
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Vend 1 Festival with discount.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    5% Commission During Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    5% Discount on vending Festivals.
                  </p>
                </div>
              ) : (
                <div>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Attend Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    30 Days Renewal.
                  </p>
                  {/* <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                  1 Festival.
                  </p> */}
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                  15% Discount During Festivals.
                  </p>
               </div>
              )}
              <div className="pt-5">
                <div
                  className="tab active-tab absolute-div text-center"
                  onClick={handlePaymentFirstPackage}
                >
                  {userSubscription === 'vendor' ? '$50.00/ Festival' : '$25.00'}
                </div>
              </div>
            </div>

            <div className="col-3 card p-2 mx-auto mb-5">
              {/* <div>{userSubscription === 'host' ? 'Vending Host' : 'Members'}</div> */}
              <div>{userSubscription === 'vendor' ? 'Kodidi' : 'Kodidi'}</div>
              {userSubscription === 'vendor' ? (
                <div >
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Kodidi
                  </p>
                  {/* <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    15% Discounts After Festivals.
                  </p> */}
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                   365 Days Renewal.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    5% Discounts After Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    5% Discount on vending Festivals.
                  </p>
                </div>
              ) : (
                <div>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Kodidi
                  </p>
                  {/* <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    15% Discounts After Festivals.
                  </p> */}
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                   365 Days Renewal.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    15% Discounts After Festivals.
                  </p>
                </div>
              )}
              <div className="pt-5">
                <div
                  className="tab active-tab absolute-div text-center"
                  onClick={handlePaymentSecondPackage}
                >
                  {userSubscription === 'vendor' ? '$50.00 /month' : '$10.00 per month'}
                </div>
              </div>
            </div>

            <div className="col-3 card p-2 mx-auto mb-5">
              {/* <div>{userSubscription === 'vendor' ? 'Vending vendor' : 'Members'}</div> */}
              {userSubscription && userSubscription === 'vendor' ? (
                <div>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Attend Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Free Food Tasting.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Vend 4 Festivals with discount.
                  </p>
                  
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    20% Commission.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    5% Discount on vending Festivals.
                  </p>
                </div>
              ) : (
                <div>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Attend Festivals.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    365 Days Renewal.
                  </p>
                  {/* <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                  4 Festival.
                  </p> */}
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                  15% Discount During Festivals.
                  </p>
                </div>
              )}
              <div className="pt-5">
                <div
                  className="tab active-tab absolute-div text-center"
                  onClick={handlePaymentThirdPackage}
                >
                  {userSubscription === 'vendor' ? '$150.00 / 4 Festivals' : '$80.00'}
                </div>
              </div>
            </div>

            <div className="col-3 card p-2 mx-auto mb-5">
              <div>{userSubscription === 'vendor' ? 'Kodidi' : 'Kodidi'}</div>
              {userSubscription === 'vendor' ? (
                <div>
                <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Kodidi
                  </p>
                  {/* <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    15% Discounts After Festivals.
                  </p> */}
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                   365 Days Renewal.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    5% Discounts After Festivals.
                  </p>
                </div>
              ) : (
                <div>
                 <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    Kodidi
                  </p>
                  {/* <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    15% Discounts After Festivals.
                  </p> */}
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                   365 Days Renewal.
                  </p>
                  <p>
                    <img src={bulletPointIcon} alt="#" className="mr-2" />
                    15% Discounts After Festivals.
                  </p>
                </div>
              )}

              <div className="pt-5">
                <div
                  className="tab active-tab absolute-div text-center"
                  onClick={handlePaymentFourthPackage}
                >
                  {userSubscription === 'vendor' ? '$500.00/Year' : '$100.00 per year'}
                </div>
              </div>

              {/* <div className="pt-5"> */}
                {/* {userSubscription === 'vendor' ? (
                  (userRole && userRole.includes('vendor')) ||
                  (userRole && userRole.includes('vendor_PENDING')) ? (
                    <div className="tab active-tab absolute-div text-center">Already Applied!</div>
                  ) : (
                    <Link exact="true" to="/become-a-vendor">
                      <div className="pt-5">
                        <div className="tab active-tab absolute-div text-center">
                          Apply now to become Ambassador
                        </div>
                      </div>
                    </Link>
                  )
                ) : (
                  <div
                    className="tab active-tab absolute-div text-center"
                    onClick={() => {
                      if (hasSubscriptionAndValid('G_AMB')) {
                        return;
                      } else {
                        getAmbassadorGuest();
                      }
                    }}
                  >
                    Apply now to become Ambassador
                  </div>
                )} */}
              {/* </div> */}
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Subscriptions;
