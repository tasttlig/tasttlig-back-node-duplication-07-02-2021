// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import Footer from '../Home/BannerFooter/BannerFooter';

// Styling
import './PassportPricing.scss';

const PaymentPricing = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Mount Pricing page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <Nav />

      <div className="passport-pricing-content">
        {!appContext.state.signedInStatus ? null : (
          <div>
            <div className="passport-pricing-title">Festival Admission</div>
            <div className="row">
              <div className="col-md-6 passport-pricing-card">
                <div className="col-sm-12 border rounded shadow p-2 mb-5">
                  <div className="col-sm-12 festival-img-bg-sep-2020" />
                  <div className="title">Multi-National Food Festival 2020</div>
                  <div>
                    <a>Sep 2nd to 30th</a>
                  </div>
                  <div className="single-entry-festival-pass">Single-Entry Festival Pass</div>
                  <div>Claim each food sample once</div>
                  <Link
                    to="/payment/subscription/M_F_SEP_2020_S"
                    className="btn btn-primary mt-3 mb-3"
                  >
                    Buy&nbsp;&nbsp;$20
                  </Link>
                </div>
              </div>
              <div className="col-md-6 passport-pricing-card">
                <div className="col-sm-12 border rounded shadow p-2 mb-5 passport-pricing-card-content">
                  <div className="col-sm-12 festival-img-bg-sep-2020" />
                  <div className="title">Multi-National Food Festival 2020</div>
                  <div>
                    <a>Sep 2nd to 30th</a>
                  </div>
                  <div className="multiple-entry-festival-pass">Multiple-Entry Festival Pass</div>
                  <div>Claim each food sample multiple times</div>
                  <Link
                    to="/payment/subscription/M_F_SEP_2020_M"
                    className="btn btn-primary mt-3 mb-3"
                  >
                    Buy&nbsp;&nbsp;$40
                  </Link>
                </div>
              </div>
              {/* <div className="col-sm-12 col-md-6 p-3">
                <div className="col-sm-12 border rounded shadow p-2 mb-5">
                  <div className="col-sm-12 festival-img-bg-dec-2020" />
                  <div className="title text-left">
                    Multi-National
                    <br />
                    Food Festival
                    <br />
                    2020
                  </div>
                  <div className="text-left">
                    <a>Dec 2nd to 31st</a>
                    <br />
                  </div>
                  <Link
                    to="/payment/subscription/NM_FP_DEC_2020"
                    className="btn btn-primary mt-3 mb-3"
                  >
                    $25&nbsp;&nbsp;Buy Now
                  </Link>
                </div>
              </div> */}
            </div>
            <Link to="/festival" className="btn btn-secondary">
              Explore Festival
            </Link>
          </div>
        )}
        {appContext.state.signedInStatus ? null : (
          <div>
            <div className="passport-pricing-title">Festival Admission</div>
            <div className="row">
              <div className="col-md-6 passport-pricing-card">
                <div className="col-sm-12 border rounded shadow p-2 mb-5">
                  <div className="col-sm-12 festival-img-bg-sep-2020" />
                  <div className="title">Multi-National Food Festival 2020</div>
                  <div>
                    <a>Sep 2nd to 30th</a>
                  </div>
                  <div className="single-entry-festival-pass">Single-Entry Festival Pass</div>
                  <div>Claim each food sample once</div>
                  <Link
                    to="/payment/subscription/F_SEP_2020_S"
                    className="btn btn-primary mt-3 mb-3"
                  >
                    Buy&nbsp;&nbsp;$25
                  </Link>
                </div>
              </div>
              <div className="col-md-6 passport-pricing-card">
                <div className="col-sm-12 border rounded shadow p-2 mb-5 passport-pricing-card-content">
                  <div className="col-sm-12 festival-img-bg-sep-2020" />
                  <div className="title">Multi-National Food Festival 2020</div>
                  <div>
                    <a>Sep 2nd to 30th</a>
                  </div>
                  <div className="multiple-entry-festival-pass">Multiple-Entry Festival Pass</div>
                  <div>Claim each food sample multiple times</div>
                  <Link
                    to="/payment/subscription/F_SEP_2020_M"
                    className="btn btn-primary mt-3 mb-3"
                  >
                    Buy&nbsp;&nbsp;$50
                  </Link>
                </div>
              </div>
              {/* <div className="col-sm-12 col-md-6 p-3">
              <div className="col-sm-12 border rounded shadow p-2 mb-5">
                <div className="col-sm-12 festival-img-bg-dec-2020" />
                <div className="title text-left">
                  Multi-National
                  <br />
                  Food Festival
                  <br />
                  2020
                </div>
                <div className="text-left">
                  <a>Dec 2nd to 31st</a>
                  <br />
                </div>
                <Link
                  to="/payment/subscription/NM_FP_DEC_2020"
                  className="btn btn-primary mt-3 mb-3"
                >
                  $25&nbsp;&nbsp;Buy Now
                </Link>
              </div>
            </div> */}
            </div>
            <Link to="/festival" className="btn btn-secondary">
              Explore Festival
            </Link>
          </div>
        )}
      </div>

      <Footer />
    </div>
  );
};

export default PaymentPricing;
