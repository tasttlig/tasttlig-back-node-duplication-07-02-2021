// Libraries
import React, { useState } from 'react';

// Components
import Navbar from './Navbar';
import SideDrawer from '../SideDrawer/SideDrawer';
import Backdrop from '../Backdrop/Backdrop';
import { Fragment } from 'react';

const Nav = (props) => {
  // Set initial state
  const [sideDrawerOpen, setSideDrawerOpen] = useState(false);

  let backdrop;
  if (sideDrawerOpen) {
    backdrop = <Backdrop backdropClickHandler={() => setSideDrawerOpen(false)} />;
  }

  return (
    <Fragment>
      <Navbar
        background={props.background}
        drawerToggleClickHandler={() => setSideDrawerOpen(true)}
        navbarEffect={props.navbarEffect}
        keyword={props.keyword}
        url={props.url}
        toggleNationality={props.toggleNationality}
        selectedItem={props.selectedItem}
        setSelectedItem={props.setSelectedItem}
        latitude={props.latitude}
        filterRadius={props.filterRadius}
        setFilterRadius={props.setFilterRadius}
        filterStartDate={props.filterStartDate}
        setFilterStartDate={props.setFilterStartDate}
        filterEndDate={props.filterEndDate}
        setFilterEndDate={props.setFilterEndDate}
        filterQuantity={props.filterQuantity}
        setFilterQuantity={props.setFilterQuantity}
        handleHostMultipleFestivals={props.handleHostMultipleFestivals}
        selectMultipleFestivals={props.selectMultipleFestivals}
        hostFestivalList={props.hostFestivalList}
        history={props.history}
      />
      <SideDrawer
        sideDrawerOpen={sideDrawerOpen}
        drawerToggleClickHandler={() => setSideDrawerOpen(true)}
        handleHostMultipleFestivals={props.handleHostMultipleFestivals}
        selectMultipleFestivals={props.selectMultipleFestivals}
        hostFestivalList={props.hostFestivalList}
        history={props.history}
      />
      {backdrop}
    </Fragment>
  );
};

export default Nav;
