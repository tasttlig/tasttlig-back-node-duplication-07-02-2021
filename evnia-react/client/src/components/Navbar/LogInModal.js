// Libraries
import React, { useContext } from 'react';
import Modal from 'react-modal';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Styling
import './Navbar.scss';

const LogInModal = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Render Login Modal
  return (
    <Modal
      isOpen={authModalContext.state.logInOpened}
      onRequestClose={authModalContext.closeModal('log-in')}
      ariaHideApp={false}
      className="log-in-modal"
    >
      <div className="text-right">
        <button
          aria-label="Close Login Modal"
          onClick={authModalContext.closeModal('log-in')}
          disabled={authModalContext.state.submitAuthDisabled}
          className="fas fa-times fa-2x close-modal"
        ></button>
      </div>

      <div className="modal-title">Login to Tasttlig</div>

      {appContext.state.errorMessage && (
        <div className="mb-3 invalid-message">{appContext.state.errorMessage}</div>
      )}

      <form onSubmit={authModalContext.handleSubmitLogIn} noValidate>
        {/* <div className="mb-3">
          <input
            type="email"
            name="logInEmail"
            placeholder="Email"
            value={authModalContext.state.logInEmail}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="email"
            required
          />
          <span className="far fa-envelope input-icon"></span>
          {authModalContext.state.logInEmailError && (
            <div className="error-message">
              {authModalContext.state.logInEmailError}
            </div>
          )}
        </div> */}
        <div className="mb-3">
          <input
            type="email"
            name="logInPassportIdOrEmail"
            placeholder="Email Address"
            value={authModalContext.state.logInPassportIdOrEmail}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="email"
            required
          />
          <span className="far fa-envelope input-icon"></span>
          {authModalContext.state.logInPassportIdOrEmailError && (
            <div className="error-message">
              {authModalContext.state.logInPassportIdOrEmailError}
            </div>
          )}
        </div>
        <div className="mb-3">
          <input
            type={authModalContext.state.logInPasswordType}
            name="logInPassword"
            placeholder="Password"
            value={authModalContext.state.logInPassword}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="password"
            required
          />
          <span onClick={authModalContext.handleClickLogIn} className="password-icon">
            {authModalContext.state.logInPasswordType === 'password' ? (
              <i className="far fa-eye-slash"></i>
            ) : (
              <i className="far fa-eye"></i>
            )}
          </span>
          {authModalContext.state.logInPasswordError && (
            <div className="error-message">{authModalContext.state.logInPasswordError}</div>
          )}
        </div>

        <div className="mb-3">
          <button
            type="submit"
            disabled={authModalContext.state.submitAuthDisabled}
            className="log-in-btn"
          >
            Login
          </button>
        </div>
      </form>

      <div className="mb-3">
        <span onClick={authModalContext.openModal('forgot-password')} className="option">
          Forgot Password?
        </span>
      </div>

      <div>
        <span>Don't have a Tasttlig account?</span>&nbsp;
        <span onClick={authModalContext.openModal('sign-up')} className="option">
          Sign Up
        </span>
      </div>
    </Modal>
  );
};

export default LogInModal;
