// Libraries
import React from 'react';
import DatePicker from 'react-datepicker';

// Components
import NationalitySelector from '../Passport-Old/NationalitySelector';

// Styling
import './Navbar.scss';

const FilterBar = (props) => {
  // Set initial state
  const filterRadius = props.filterRadius;
  const setFilterRadius = props.setFilterRadius;
  const filterStartDate = props.filterStartDate;
  const setFilterStartDate = props.setFilterStartDate;
  const filterEndDate = props.filterEndDate;
  const setFilterEndDate = props.setFilterEndDate;
  const filterQuantity = props.filterQuantity;
  const setFilterQuantity = props.setFilterQuantity;

  const LocationSelector = () => (
    <div className="filter-section-categories">
      <div className="mb-1">Distance (km)</div>
      <select
        onChange={(e) => setFilterRadius(parseInt(e.target.value))}
        value={filterRadius}
        className="mb-3 custom-select"
      >
        <option value="25000000">--Select---</option>
        <option value="5000">5km</option>
        <option value="10000">10km</option>
        <option value="25000">25km</option>
        <option value="50000">50km</option>
      </select>
    </div>
  );

  const StartDateSelector = () => (
    <>
      {props.latitude && (
        <div className="filter-section-categories">
          <div className="mb-1">From Date / Time</div>
          <DatePicker
            selected={filterStartDate}
            onChange={(date) => setFilterStartDate(date)}
            minDate={new Date()}
            showTimeSelect
            dateFormat="MMMM d, yyyy h:mm aa"
            className="mb-3 form-control"
          />
        </div>
      )}
    </>
  );

  const EndDateSelector = () => (
    <>
      {props.latitude && (
        <div className="filter-section-categories">
          <div className="mb-1">To Date / Time</div>
          <DatePicker
            selected={filterEndDate}
            onChange={(date) => setFilterEndDate(date)}
            minDate={new Date()}
            showTimeSelect
            dateFormat="MMMM d, yyyy h:mm aa"
            className="mb-3 form-control"
          />
        </div>
      )}
    </>
  );

  const QuantitySelector = () => (
    <div className="filter-section-categories">
      <div className="mb-1">Quantity Available</div>
      <select
        onChange={(e) => setFilterQuantity(parseInt(e.target.value))}
        value={filterQuantity}
        className="mb-3 custom-select"
      >
        <option value="0">--Select--</option>
        <option value="1">1 or more</option>
        <option value="10">10 or more</option>
        <option value="25">25 or more</option>
        <option value="50">50 or more</option>
        <option value="100">100 or more</option>
      </select>
    </div>
  );

  // Render Filter Bar
  return (
    props.openFilter && (
      <div className="row main-navbar-filter">
        <div className="col-lg-4 main-navbar-filter-content">
          <div className="mb-1">Nationality</div>
          <NationalitySelector
            toggleNationality={props.toggleNationality}
            selectedItem={props.selectedItem}
            setSelectedItem={props.setSelectedItem}
          />
        </div>
        <div className="col-lg-4 main-navbar-filter-content">
          <LocationSelector />
        </div>
        {/* <div className="col-lg-4 main-navbar-filter-content">
          <StartDateSelector />
        </div>
        <div className="col-lg-4 main-navbar-filter-content">
          <EndDateSelector />
        </div> */}
        <div className="col-lg-4 main-navbar-filter-content">
          <QuantitySelector />
        </div>
      </div>
    )
  );
};

export default FilterBar;
