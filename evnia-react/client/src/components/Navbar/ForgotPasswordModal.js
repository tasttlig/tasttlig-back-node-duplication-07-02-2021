// Libraries
import React, { useContext } from 'react';
import Modal from 'react-modal';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Styling
import './Navbar.scss';

const ForgotPasswordModal = () => {
  // To use the JWT credentials
  const authModalContext = useContext(AuthModalContext);

  // Render Forgot Password Modal
  return (
    <Modal
      isOpen={authModalContext.state.forgotPasswordOpened}
      onRequestClose={authModalContext.closeModal('forgot-password')}
      ariaHideApp={false}
      className="forgot-password-modal"
    >
      <div className="text-right">
        <button
          aria-label="Close Forgot Password Modal"
          onClick={authModalContext.closeModal('forgot-password')}
          disabled={authModalContext.state.submitAuthDisabled}
          className="fas fa-times fa-2x close-modal"
        ></button>
      </div>

      <div className="modal-title">Forgot Password on Tasttlig?</div>

      <form onSubmit={authModalContext.handleSubmitForgotPassword} noValidate>
        <div className="mb-3">
          <input
            type="email"
            name="forgotPasswordEmail"
            placeholder="Email Address"
            value={authModalContext.state.forgotPasswordEmail}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="email"
            required
          />
          <span className="far fa-envelope input-icon"></span>
          {authModalContext.state.forgotPasswordEmailError && (
            <div className="error-message">{authModalContext.state.forgotPasswordEmailError}</div>
          )}
        </div>

        <div className="mb-3">
          <button
            type="submit"
            disabled={authModalContext.state.submitAuthDisabled}
            className="forgot-password-btn"
          >
            Find Account
          </button>
        </div>
      </form>

      <div>
        <span onClick={authModalContext.openModal('log-in')} className="option log-in-switch">
          Login
        </span>
        &nbsp;
        <span onClick={authModalContext.openModal('sign-up')} className="option sign-up-switch">
          Sign Up
        </span>
      </div>
    </Modal>
  );
};

export default ForgotPasswordModal;
