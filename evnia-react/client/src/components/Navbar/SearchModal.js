// Libraries
import React, { useContext } from 'react';
import Modal from 'react-modal';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Components
import SearchBar from './SearchBar';

// Styling
import './Navbar.scss';

const SearchModal = (props) => {
  // To use the JWT credentials
  const authModalContext = useContext(AuthModalContext);

  // Render Search Modal
  return (
    <>
      {window.innerWidth < 992 && (
        <Modal
          isOpen={authModalContext.state.searchOpened}
          onRequestClose={authModalContext.closeModal('search')}
          ariaHideApp={false}
          className="search-modal"
        >
          <div className="text-right">
            <button
              aria-label="Close Search Modal"
              onClick={authModalContext.closeModal('search')}
              disabled={authModalContext.state.submitAuthDisabled}
              className="fas fa-times fa-2x close-modal"
            ></button>
          </div>
          <div className="search-modal-content">
            <SearchBar keyword={props.keyword} url={props.url} />
          </div>
        </Modal>
      )}
    </>
  );
};

export default SearchModal;
